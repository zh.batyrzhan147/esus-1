<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=1280, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Сертификат</title>
</head>
<body style="font-family: 'Times New Roman'; background: url('{{ $background }}') no-repeat; position: relative; background-size: cover; height: 888px;">
<main style="display:flex; flex-direction: column; height: 460px; width:1280px; margin: 0 auto;">
    <div style="position: absolute; top: 55%; font-size: 30px; left: 0; width: 100%; text-align: center;font-family: 'Times New Roman';">{{ $title }}</div>
    <div style="position: absolute; top: 96.9%; font-size: 18px; left: 18.5%; width: 300px; text-align: left;font-family: 'Times New Roman';">{{ $date }}</div>
    <div style="position: absolute; top: 96.9%; font-size: 18px; left: 61%; width: 300px; text-align: left;font-family: 'Times New Roman';">{{ $date }}</div>
</main>
<div style="position: absolute;display: flex;justify-content: right;align-items: end; right:50px;bottom:0px; z-index: 100">
    <div style="margin: 0 auto;width:100%;text-align:center;">{!! \SimpleSoftwareIO\QrCode\Facades\QrCode::size(90)->generate($url) !!}</div>
    <div style="margin: 10px auto;text-align: center;font-weight: bold; font-size: 16px;text-transform: uppercase;">{{ $uuid }}</div>
</div>
</body>
</html>
