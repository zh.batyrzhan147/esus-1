<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=1280, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Сертификат</title>
</head>
<body style="font-family: 'Times New Roman', sans-serif; background: url('{{ $background }}') no-repeat; position: relative; background-size: cover; height: 888px;">
<main style="display:flex; flex-direction: column; height: 460px; width:1280px; margin: 0 auto;">
    <div style="position: absolute; top: 38.5%; font-size: 30px; left: 0; width: 100%; text-align: center;font-family: 'Times New Roman', serif;">{{ $title }}</div>
    <div style="position: absolute; top: 94.85%; font-size: 18px; left: 19.8%; width: 300px; text-align: left;font-family: 'Times New Roman', serif;">{{ date('d.m.Y', strtotime($date)) }}</div>
    <div style="position: absolute; top: 94.85%; font-size: 18px; left: 59.2%; width: 300px; text-align: left;font-family: 'Times New Roman', serif;">{{ date('d.m.Y', strtotime($date)) }}</div>
</main>
<div style="position: absolute; font-size: 190px; top: 0; left: 0; width: 100%; height: 888px; text-align: center;z-index: 100;font-family: 'Times New Roman', serif;">
    <div style="display:flex; justify-content:center; align-items:center;height: 888px;width: 100%;">
        <div style="color: rgba(255, 0, 0, .4);padding-top: 270px;">КӨШІРМЕ</div>
        <div style="color: rgba(255, 0, 0, .4);">КОПИЯ</div>
    </div>
</div>
<div style="position: absolute;display: flex;justify-content: right;align-items: end; right:100px;bottom:100px; z-index: 100">
    {!! \SimpleSoftwareIO\QrCode\Facades\QrCode::size(100)->generate($url) !!}
    <div style="margin: 10px auto;text-align: center;font-weight: bold; font-size: 16px;text-transform: uppercase;font-family: 'Times New Roman', serif;">{{ $uuid }}</div>
</div>
</body>
</html>
