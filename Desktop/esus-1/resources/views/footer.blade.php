<footer class="hidden lg:flex w-full bg-white relative z-40">
    <div class="flex max-w-screen-xl w-full mx-auto py-8 px-2 justify-between items-start">
        <div class="w-full flex flex-col space-y-4 justify-start">
            <div class="flex text-2xl font-bold justify-start items-center space-x-2">
                <svg width="32" height="32" viewBox="0 0 32 32" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path fill-rule="evenodd" clip-rule="evenodd" d="M0 16C4.84192e-07 18.5263 2.03301 20.5593 6.09902 24.6253L7.37469 25.901C11.4407 29.967 13.4737 32 16 32C18.5263 32 20.5593 29.967 24.6253 25.901L25.901 24.6253C29.967 20.5593 32 18.5263 32 16C32 13.4737 29.967 11.4407 25.901 7.37468L24.6253 6.09901C20.5593 2.033 18.5263 0 16 0C13.4737 1.1079e-06 11.4407 2.03301 7.37468 6.09902L6.09901 7.37469C2.033 11.4407 -4.25901e-07 13.4737 0 16ZM8.74933 27.2713C11.5932 30.0868 13.3664 31.645 15.2976 31.946L20.2129 26.844L15.9291 25.5371L14.84 21.1806L8.74933 27.2713Z" fill="url(#paint0_linear_259_399)"/>
                    <defs>
                        <linearGradient id="paint0_linear_259_399" x1="10.6654" y1="-18.5891" x2="46.8224" y2="9.9351" gradientUnits="userSpaceOnUse">
                            <stop stop-color="#3B82F6"/>
                            <stop offset="1" stop-color="#1B55B5"/>
                        </linearGradient>
                    </defs>
                </svg>
                <span class="text-logo-blue">ESUS</span>
            </div>
            <div class="text-base font-normal text-main-black w-52">{{ __('footer.description') }}</div>
        </div>
        <div class="flex w-full flex-col items-end">
            <nav class="flex space-x-8">
                <a href="{{ route('user.profile') }}" class="flex {{ request()->routeIs('user.profile') ? 'text-main-black' : 'text-soft-gray' }} font-semibold hover:text-main-black">Обзор</a>
                <a href="{{ route('messages.list') }}" class="relative flex {{ request()->routeIs('messages.list') ? 'text-main-black' : 'text-soft-gray' }} font-semibold hover:text-main-black">
                    <span>Сообщения</span>
                    @if (auth()->user()->unreadNotifications->first())
                        <span class="absolute top-0 -right-2 inline-block w-2 h-2 transform translate-x-1/2 -translate-y-1/2 bg-red-600 rounded-full"></span>
                    @endif
                </a>
                <a href="{{ route('user.groups.active') }}" class="flex {{ (request()->routeIs('user.groups.*') or request()->routeIs('user.group.view')) ? 'text-main-black' : 'text-soft-gray' }} font-semibold hover:text-main-black">Мои курсы</a>
                <a href="{{ route('user.orders.list') }}" class="flex {{ (request()->routeIs('user.orders.*') or request()->routeIs('user.orders.list')) ? 'text-main-black' : 'text-soft-gray' }} font-semibold hover:text-main-black">Мои заказы</a>
                @can(\App\Models\Permission::PERMISSION_TRAINER)
                    <a href="{{ route('trainer.groups.active') }}" class="flex {{ request()->routeIs('trainer.*') ? 'text-main-black' : 'text-soft-gray' }} font-semibold hover:text-main-black">Тренер</a>
                @endcan
            </nav>
            <div class="text-sm font-normal text-soft-gray mt-10">{{ date('Y') }}. {{ __('footer.copyright') }}</div>
        </div>
    </div>
</footer>
