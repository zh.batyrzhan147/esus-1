<div class="w-full" wire:ignore>
    <input id="{{ $attributes['id'] }}" type="hidden" name="content" value="{{ $value }}">
    <trix-editor class="" input="{{ $attributes['id'] }}"></trix-editor>
</div>
<script>
    var trixEditor{{ $attributes['id'] }} = document.getElementById("{{ $attributes['id'] }}")
    addEventListener("trix-blur", function(event) {
        @this.set('{{ $attributes['id'] }}', trixEditor{{ $attributes['id'] }}.getAttribute('value'))
    })

</script>
