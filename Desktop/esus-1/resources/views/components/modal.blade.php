@props(['id' => null])
<div
    x-data="{
    show: true
    }"
    x-show="show"

    class="min-w-screen h-screen animated fadeIn faster fixed left-0 top-0 flex justify-center items-center inset-0 z-50 outline-none focus:outline-none bg-no-repeat bg-center bg-cover"
    {{ $attributes }}>
   	<div class="absolute bg-black opacity-80 inset-0 z-0"></div>
    <div class="w-full max-w-5xl p-5 relative mx-auto my-auto rounded-xl shadow-lg bg-white ">
        <div class="">
        {{ $header }}
        </div>
      <!--content-->
      <div class="">
          {{ $slot }}
      </div>
    </div>
</div>
