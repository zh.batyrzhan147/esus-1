<div>
    <x-slot name="header"></x-slot>

    <div class="bg-white mx-auto py-6 px-4 sm:px-6 lg:px-8">
        <div class="flex justify-between items-center">
            <h2 class="font-semibold text-xl text-gray-800 leading-tight">
                {{ __('admin.schedule.list.title') }}
            </h2>

            <div class="flex justify-end items-center mb-4">
                @include('admin.common.secondmenu', [
                    'tabname'=>'groups',
                    'popup'=>true,
                ])
            </div>
        </div>
    </div>

    <div class="flex flex-col mx-auto py-2 px-4 sm:px-6 lg:px-8">
        <div class="w-full flex justify-between space-x-4">
            <div class="flex flex-col mb-4 w-full">
                <label class="flex flex-col text-left">
                    <span class="text-gray-700 mb-2">{{ __('admin.schedule.list.filter.start') }}</span>
                </label>
                <x-inputs.date id="start" wire:model.defer="start_time" />
                @error('start_time')
                <p class="text-xs text-red-600">{{ $message }}</p>
                @enderror
            </div>
            <div class="flex flex-col mb-4 w-full">
                <label class="flex flex-col text-left">
                    <span class="text-gray-700 mb-2">{{ __('admin.schedule.list.filter.end') }}</span>
                </label>
                <x-inputs.date id="end" wire:model.defer="end_time" />
                @error('end_time')
                <p class="text-xs text-red-600">{{ $message }}</p>
                @enderror
            </div>
            <div class="flex flex-col mb-4 w-full">
                <label class="flex flex-col text-left">
                    <span class="text-gray-700 mb-2">{{ __('admin.schedule.list.filter.show') }}</span>
                </label>
                <select wire:model.defer="showby"
                        class="w-48 px-3 py-1 border rounded focus:border-blue-500 hover:border-blue-500 text-sm outline-none focus:input-shadow">
                    <option value="auto">{{ __('admin.schedule.list.filter.show.auto') }}</option>
                    <option value="byday">{{ __('admin.schedule.list.filter.show.day') }}</option>
                    <option value="byweek">{{ __('admin.schedule.list.filter.show.week') }}</option>
                </select>
                @error('start_time')
                <p class="text-xs text-red-600">{{ $message }}</p>
                @enderror
            </div>
            <div class="flex flex-col mb-4 w-full">
                <label class="flex flex-col text-left">
                    <span class="text-gray-700 mb-2">{{ __('admin.schedule.list.filter.affiliate') }}</span>
                </label>
                <select wire:model.defer="affiliate"
                        class="w-full px-3 py-1 border rounded focus:border-blue-500 hover:border-blue-500 text-sm outline-none focus:input-shadow">
                    <option value="">{{ __('admin.schedule.list.filter.affiliate.all') }}</option>
                    @foreach(\App\Models\Affiliates::all() as $affiliate)
                        <option value="{{ $affiliate->id }}">{{ $affiliate->title }}</option>
                    @endforeach
                </select>
                @error('start_time')
                <p class="text-xs text-red-600">{{ $message }}</p>
                @enderror
            </div>
        </div>
        <div class="flex justify-end">
            <button wire:click="applyFilter"
                    class="mb-2 text-sm flex justify-center items-center space-x-1.5 px-4 py-1 border-0 bg-blue-500 text-white rounded hover:bg-blue-600">{{ __('admin.buttons.accept.filters') }}</button>
        </div>
    </div>

    <style>
        .gantt {
            display: grid;
            border: 0;
            border-radius: 0px;
            position: relative;
            overflow-x: auto;
            box-sizing: border-box;
        }
        .gantt__row {
            display: grid;
            grid-template-columns: 1fr;
            background-color: #fff;
        }
        .gantt__row:nth-child(odd) {
            background-color: #f5f5f5;
        }
        .gantt__row:nth-child(odd) .gantt__row-first {
            background-color: #f5f5f5;
        }
        .gantt__row:nth-child(3) .gantt__row-bars {
            border-top: 0;
        }
        .gantt__row:nth-child(3) .gantt__row-first {
            border-top: 0;
        }
        .gantt__row--months {
            color: #000;
            background-color: #f2f2f2 !important;
            grid-template-columns: repeat({{ count($header) }}, {{ (count($header)>15) ? '80px' : '1fr' }});
        }
        .gantt__row--months .gantt__row-first {
            border-top: 0 !important;
            background-color: #0a3444 !important;
        }
        .gantt__row--months span, .gantt__paddings {
            text-align: center;
            font-size: 13px;
            align-self: center;
            font-weight: bold;
            padding: 20px 0;
        }
        .gantt__row-first {
            background-color: #fff;
            border-width: 1px 0 0 0;
            border-color: rgba(0, 0, 0, 0.1);
            border-style: solid;
            padding: 15px 0;
            font-size: 13px;
            font-weight: bold;
            text-align: center;
            height: 50px;
        }
        .gantt__row-bars {
            list-style: none;
            display: grid;
            padding: 9px 0;
            margin: 0;
            grid-template-columns: repeat({{ count($header) }}, {{ (count($header)>15) ? '80px' : '1fr' }});
            grid-gap: 8px 0;
            border-top: 1px solid rgba(221, 221, 221, 0.8);
            height: 50px;
        }
        .gantt__row-bars li {
            font-weight: 500;
            text-align: left;
            font-size: 14px;
            min-height: 15px;
            background-color: #55de84;
            padding: 5px 12px;
            color: #fff;
            overflow: hidden;
            position: relative;
            cursor: pointer;
            border-radius: 15px;
        }
        .gantt__row-bars li.stripes {
            background-image: repeating-linear-gradient(45deg, transparent, transparent 5px, rgba(255, 255, 255, 0.1) 5px, rgba(255, 255, 255, 0.1) 12px);
        }
        .gantt__row-bars li:before, .gantt__row-bars li:after {
            content: "";
            height: 100%;
            top: 0;
            z-index: 4;
            position: absolute;
            background-color: rgba(0, 0, 0, 0.3);
        }
        .gantt__row-bars li.start__false {
            border-top-left-radius: 0;
            border-bottom-left-radius: 0;
        }
        .gantt__row-bars li.end__false {
            border-top-right-radius: 0;
            border-bottom-right-radius: 0;
        }
        .gantt__row-bars li:hover {
            filter: brightness(125%);
        }
        .gantt__row-bars li:before {
            left: 0;
        }
        .gantt__row-bars li:after {
            right: 0;
        }

        .gantt__row--lines {
            position: absolute;
            height: 100%;
            width: 100%;
            background-color: transparent;
            grid-template-columns: repeat({{ count($header) }}, {{ (count($header)>15) ? '80px' : '1fr' }});
        }
        .gantt__row--lines span {
            display: block;
            border-right: 1px solid rgba(0, 0, 0, 0.1);
        }
        .gantt__row--lines span.marker {
            background-color: rgba(10, 52, 68, 0.13);
            z-index: 2;
        }
        .gantt__row--lines:after {
            grid-row: 1;
            grid-column: 0;
            background-color: #1688b345;
            z-index: 2;
            height: 100%;
        }
    </style>
    <div class="w-full flex flex-col mx-auto py-2 px-4 sm:px-6 lg:px-8">
        @if(!empty($results))
            <div class="w-full flex border border-gray-100">
                <div class="flex flex-col">
                    <div class="flex justify-between align-center">
                        <div class="gantt__paddings flex w-96">
                            <div class="px-2">{{ __('admin.schedule.table.head.trainer') }}</div>
                        </div>
                        <div class="flex w-32 px-2 text-sm font-bold items-center justify-center">
                            {{ __('admin.schedule.table.head.hours') }}
                        </div>
                    </div>
                    <div class="flex flex-col">
                        @foreach($results as $result)
                            <div class="gantt__row-first whitespace-nowrap sticky left-0 flex justify-between">
                                <div class="px-2 flex w-96">
                                    {{ $result['trainer'] }}
                                </div>
                                <div class="flex w-32 px-2 text-sm font-bold justify-center items-center">
                                    {{ $result['hours_count'] }} часов
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
                <div class="gantt w-full">
                    <div class="gantt__row gantt__row--months">
                        @foreach($header as $item)
                            <span>{{ $item['title'] }}</span>
                            {{--                        <span>{{ $loop->iteration }}</span>--}}
                        @endforeach
                    </div>
                    <div class="gantt__row gantt__row--lines">
                        @foreach($header as $item)
                            <span></span>
                        @endforeach
                    </div>
                    @foreach($results as $result)
                        <div class="gantt__row">
                            <ul class="gantt__row-bars">
                                @foreach($result['groups'] as $key => $item)
                                    {{--                                @dd($item['color'])--}}
                                    <li wire:click="showModal({{ $key }})" style="grid-column: {{ $item['time'] }}; background-color: rgb({{$item['color'][0]}},{{$item['color'][1]}},{{$item['color'][2]}}); @if($item['color'][3] > 155) color:black; @endif" class="stripes {{ ($item['start'])?'start__false':'' }} {{ ($item['end'])?'end__false':'' }}">{{ $item['slug']  }}</li>
                                @endforeach
                            </ul>
                        </div>

                    @endforeach
                </div>
            </div>
        @endif
    </div>
    @if($showEditableModal)
        @include('livewire.accountant.schedule.group_modal')
    @endif
</div>

@push('styles')
{{--    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/flatpickr/dist/flatpickr.min.css">--}}
{{--    <script src="https://cdn.jsdelivr.net/npm/flatpickr"></script>--}}
{{--    <script src="https://cdn.jsdelivr.net/npm/flatpickr/dist/l10n/ru.js"></script>--}}
@endpush
