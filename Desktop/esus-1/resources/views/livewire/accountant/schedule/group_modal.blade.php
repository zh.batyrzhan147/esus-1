<div>
    <x-modal>
        <x-slot name="header">
            <div class="flex justify-between">
                <span>{{ __('admin.schedule.modal.group.info.title') }}</span>
                <button wire:click="$set('showEditableModal',false)">
                    <svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="times" class="svg-inline--fa fa-times fa-w-11 w-4 h-4" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 352 512"><path fill="currentColor" d="M242.72 256l100.07-100.07c12.28-12.28 12.28-32.19 0-44.48l-22.24-22.24c-12.28-12.28-32.19-12.28-44.48 0L176 189.28 75.93 89.21c-12.28-12.28-32.19-12.28-44.48 0L9.21 111.45c-12.28 12.28-12.28 32.19 0 44.48L109.28 256 9.21 356.07c-12.28 12.28-12.28 32.19 0 44.48l22.24 22.24c12.28 12.28 32.2 12.28 44.48 0L176 322.72l100.07 100.07c12.28 12.28 32.2 12.28 44.48 0l22.24-22.24c12.28-12.28 12.28-32.19 0-44.48L242.72 256z"></path></svg>
                </button>
            </div>
        </x-slot>
        <div class="py-6 px-4 sm:px-6 lg:px-8 h-full">
            <div class="flex w-full justify-between items-end mb-5 space-x-1 px-4">
                <h2 class="text-2xl font-bold text-black text-center">{{ __('admin.schedule.modal.group.info.group', ['id' => $modalGroup->id]) }}</h2>
            </div>
            <div class="rounded w-full bg-[#FFFFFF] px-4 py-4 mb-5">
                <div class="w-full mb-5">
                    <div class="text-[14px] text-[#3F3F46] font-medium m-0 p-0 mb-1">{{ __('admin.schedule.modal.group.info.program') }}</div>
                    <div class="text-[14px] text-[#000000] font-normal m-0 p-0">{{ $modalGroup->coursesItem->ru_title }}</div>
                </div>
                <div class="w-full grid gap-x-[20px] gap-y-5 grid-cols-2">
                    <div class="w-full">
                        <div class="text-[14px] text-[#3F3F46] font-medium m-0 p-0 mb-1">{{ __('admin.schedule.modal.group.info.cifr') }}</div>
                        <div class="text-[14px] text-[#000000] font-normal m-0 p-0">{{ $modalGroup->slug }}</div>
                    </div>
                    <div class="w-full">
                        <div class="text-[14px] text-[#3F3F46] font-medium m-0 p-0 mb-1">{{ __('admin.schedule.modal.group.info.lang') }}</div>
                        <div class="text-[14px] text-[#000000] font-normal m-0 p-0">{{ $modalGroup->languageItem->title }}</div>
                    </div>
                    <div class="w-full">
                        <div class="text-[14px] text-[#3F3F46] font-medium m-0 p-0 mb-1">{{ __('admin.schedule.modal.group.info.affiliate') }}</div>
                        <div class="text-[14px] text-[#000000] font-normal m-0 p-0">{{ $modalGroup->affiliateItem->title }}</div>
                    </div>
                    <div class="w-full">
                        <div class="text-[14px] text-[#3F3F46] font-medium m-0 p-0 mb-1">{{ __('admin.schedule.modal.group.info.year') }}</div>
                        <div class="text-[14px] text-[#000000] font-normal m-0 p-0">{{ $modalGroup->year }}</div>
                    </div>
                    <div class="w-full">
                        <div class="text-[14px] text-[#3F3F46] font-medium m-0 p-0 mb-1">{{ __('admin.schedule.modal.group.info.date.start') }}</div>
                        <div class="text-[14px] text-[#000000] font-normal m-0 p-0">{{ date("d.m.Y", strtotime($modalGroup->start_time)) }}</div>
                    </div>
                    <div class="w-full">
                        <div class="text-[14px] text-[#3F3F46] font-medium m-0 p-0 mb-1">{{ __('admin.schedule.modal.group.info.date.end') }}</div>
                        <div class="text-[14px] text-[#000000] font-normal m-0 p-0">{{ date("d.m.Y", strtotime($modalGroup->end_time)) }}</div>
                    </div>
                    <div class="w-full">
                        <div class="text-[14px] text-[#3F3F46] font-medium m-0 p-0 mb-1">{{ __('admin.schedule.modal.group.info.trainer') }}</div>
                        <div class="text-[14px] text-[#000000] font-normal m-0 p-0">{{ $modalGroup->trainerItem->fio }}</div>
                    </div>
                    <div class="w-full">
                        <div class="text-[14px] text-[#3F3F46] font-medium m-0 p-0 mb-1">{{ __('admin.schedule.modal.group.info.type') }}</div>
                        <div class="text-[14px] text-[#000000] font-normal m-0 p-0">{{ $modalGroup->type }}</div>
                    </div>
                    <div class="w-full">
                        <div class="text-[14px] text-[#3F3F46] font-medium m-0 p-0 mb-1">{{ __('admin.schedule.modal.group.info.count.plan') }}</div>
                        <div class="text-[14px] text-[#000000] font-normal m-0 p-0">{{ $modalGroup->count_all }}</div>
                    </div>
                    <div class="w-full">
                        <div class="text-[14px] text-[#3F3F46] font-medium m-0 p-0 mb-1">{{ __('admin.schedule.modal.group.info.count.fact') }}</div>
                        <div class="text-[14px] text-[#000000] font-normal m-0 p-0">{{ $modalGroup->users()->count() }}</div>
                    </div>
                </div>
            </div>
            <div class="w-full flex justify-end items-center space-x-4">
                <a href="{{ route('accountant.groups.edit', $modalGroup->id) }}" target="_blank"
                   class="text-blue-500 hover:text-blue-900 m-0 font-normal">{{ __('admin.links.edit') }}</a>
                <button wire:click="$set('showEditableModal',false)"
                        class="text-blue-500 hover:text-blue-900 m-0 font-normal">{{ __('admin.buttons.close') }}</button>
            </div>
        </div>

    </x-modal>
</div>
