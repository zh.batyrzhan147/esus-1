<div>
    <x-slot name="header"></x-slot>

    <div class="bg-white mx-auto py-6 px-4 sm:px-6 lg:px-8">
        <div class="w-full flex justify-between items-center">
            <h2 class="font-semibold text-xl text-gray-800 leading-tight">
                {{ __('admin.groups.list.title') }}
            </h2>
            @include('admin.common.secondmenu', [
                'tabname'=>'groups',
                'popup'=>true,
            ])

{{--            <div class="flex">--}}
{{--                <button wire:click="add_new_group" class="text-sm flex justify-center items-center space-x-1.5 px-4 py-1 border-0 bg-blue-500 text-white rounded hover:bg-blue-600">--}}
{{--                    <svg width="14" height="14" viewBox="0 0 14 14" fill="none" xmlns="http://www.w3.org/2000/svg">--}}
{{--                        <path d="M7.71838 1.62408H6.78088C6.69755 1.62408 6.65588 1.66575 6.65588 1.74908V6.65533H2C1.91667 6.65533 1.875 6.697 1.875 6.78033V7.71783C1.875 7.80117 1.91667 7.84283 2 7.84283H6.65588V12.7491C6.65588 12.8324 6.69755 12.8741 6.78088 12.8741H7.71838C7.80172 12.8741 7.84338 12.8324 7.84338 12.7491V7.84283H12.5C12.5833 7.84283 12.625 7.80117 12.625 7.71783V6.78033C12.625 6.697 12.5833 6.65533 12.5 6.65533H7.84338V1.74908C7.84338 1.66575 7.80172 1.62408 7.71838 1.62408Z" fill="white"/>--}}
{{--                    </svg>--}}
{{--                    <span>{{ __('admin.buttons.create') }}</span>--}}
{{--                </button>--}}
{{--            </div>--}}
        </div>
    </div>

    <div class="flex flex-col mx-auto py-2 px-4 sm:px-6 lg:px-8">
        <div class="w-full flex justify-between space-x-4">
            <div class="flex flex-col mb-4 w-96">
                <label class="flex flex-col text-left">
                    <span class="text-gray-700 mb-2">{{ __('admin.groups.list.filter.search') }}</span>
                </label>
                <input type="text" wire:keydown.enter="applyFilter" wire:model.defer="search"
                       class="w-3/4 px-3 py-1 border rounded focus:border-blue-500 hover:border-blue-500 text-sm outline-none focus:input-shadow"
                       placeholder="Введите название"
                />
            </div>
            <div class="flex flex-col mb-4 w-96">
                <label class="flex flex-col text-left">
                    <span class="text-gray-700 mb-2">{{ __('admin.groups.list.filter.year') }}</span>
                </label>
                <select wire:model="year" class="w-full px-3 py-1 border rounded focus:border-blue-500 hover:border-blue-500 text-sm outline-none focus:input-shadow">
                    <option value>{{ __('admin.groups.list.filter.year.all') }}</option>
                    @foreach(\App\Models\Years::orderBy('year','ASC')->get() as $item)
                        <option value="{{ $item->year }}">{{ $item->year }}</option>
                    @endforeach
                </select>
            </div>
            <div class="flex flex-col mb-4 w-full">
                <label class="flex flex-col text-left">
                    <span class="text-gray-700 mb-2">{{ __('admin.groups.list.filter.program') }}</span>
                </label>
                <select wire:model.defer="course" class="w-full px-3 py-1 border rounded focus:border-blue-500 hover:border-blue-500 text-sm outline-none focus:input-shadow">
                    <option value>{{ __('admin.groups.list.filter.program.all') }}</option>
                    @foreach($yearItem->courses ?? [] as $courseItem)
                        <option value="{{ $courseItem->id }}">{{ $courseItem->ru_title }}</option>
                    @endforeach
                </select>
            </div>

            <div class="flex flex-col mb-4 w-1/3">
                <label class="flex flex-col text-left">
                    <span class="text-gray-700 mb-2">{{ __('admin.groups.list.filter.affiliates') }}</span>
                </label>
                <select wire:model.defer="affiliate" class="w-full px-3 py-1 border rounded focus:border-blue-500 hover:border-blue-500 text-sm outline-none focus:input-shadow">
                    <option value>{{ __('admin.groups.list.filter.affiliates.all') }}</option>
                    @foreach(\App\Models\Affiliates::select('id','title')->get()->pluck('title','id')->toArray() as $key => $stat)
                        <option value="{{ $key }}">{{ $stat }}</option>
                    @endforeach
                </select>
            </div>

            <div class="flex flex-col mb-4 w-1/3">
                <label class="flex flex-col text-left">
                    <span class="text-gray-700 mb-2">{{ __('admin.groups.list.filter.status') }}</span>
                </label>
                <select wire:model.defer="status" class="w-full px-3 py-1 border rounded focus:border-blue-500 hover:border-blue-500 text-sm outline-none focus:input-shadow">
                    <option value>{{ __('admin.groups.list.filter.status.all') }}</option>
                    @foreach(\App\Models\Groups::listGroupStatuses() as $key => $stat)
                        <option value="{{ $key }}">{{ $stat }}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="flex justify-end">
            <button wire:click="applyFilter"
                    class="text-sm flex justify-center items-center space-x-1.5 px-4 py-1 border-0 bg-blue-500 text-white rounded hover:bg-blue-600">
                <span>{{ __('admin.buttons.accept.filters') }}</span>
            </button>
        </div>
    </div>

    <div class="py-6 px-4 sm:px-6 lg:px-8">
        <div class="flex flex-col">
            <div class="-my-2 overflow-x-auto sm:-mx-6 lg:-mx-8">
                <div class="py-2 align-middle inline-block min-w-full sm:px-6 lg:px-8">
                    <div class="shadow overflow-hidden border-b border-gray-200 sm:rounded">
                        <table class="min-w-full divide-y divide-gray-200 w-full">
                            <thead>
                            <tr>
                                <th scope="col" class="px-2 py-3 bg-gray-50 text-left text-sm font-medium text-gray-800">
                                    <div class="pl-4">{{ __('admin.table.head.title.group') }}</div>
                                </th>
                                <th scope="col" class="px-2 py-3 bg-gray-50 text-left text-sm font-medium text-gray-800">
                                    <div class="pl-4 border-l border-gray-200">{{ __('admin.table.head.title.group.year') }}</div>
                                </th>
                                <th scope="col" class="px-2 py-3 bg-gray-50 text-left text-sm font-medium text-gray-800">
                                    <div class="pl-4 border-l border-gray-200">{{ __('admin.table.head.count.listener') }}</div>
                                </th>
                                <th scope="col" class="px-2 py-3 bg-gray-50 text-left text-sm font-medium text-gray-800">
                                    <div class="pl-4 border-l border-gray-200">{{ __('admin.table.head.program') }}</div>
                                </th>
                                <th scope="col" width="200" class="px-2 py-3 bg-gray-50 text-left text-sm font-medium text-gray-800">
                                    <div class="pl-4 border-l border-gray-200">{{ __('admin.table.head.action') }}</div>
                                </th>
                            </tr>
                            </thead>
                            <tbody class="bg-white divide-y divide-gray-200">
                            @foreach ($groups as $group)
                                <tr>
                                    <td class="px-6 py-1.5 whitespace-nowrap text-sm text-gray-900">
                                        {{ $group->slug }}
                                    </td>

                                    <td class="px-6 py-1.5 whitespace-nowrap text-sm text-gray-900">
                                        {{ $group->year  }}
                                    </td>

                                    <td class="px-6 py-1.5 whitespace-nowrap text-sm text-gray-900">
                                        {{ $group->count_all }}
                                    </td>

                                    <td class="px-6 py-1.5  text-sm text-gray-900">
                                        {!! $group->coursesItem->ru_title !!}
                                    </td>

                                    <td class="px-6 py-1.5 whitespace-nowrap text-sm font-medium space-x-2">
{{--                                        @if($group->status == \App\Models\Groups::GROUP_STATUS_FINISHED or $group->status == \App\Models\Groups::GROUP_STATUS_FINISHED_TESTING)--}}
{{--                                            <a href="{{ route('accountant.groups.certificates', $group->id) }}"--}}
{{--                                               class="text-blue-500 hover:text-blue-900 m-0 font-normal">{{ __('admin.links.give.certificates') }}</a>--}}
{{--                                        @endif--}}
                                            <a href="{{ route('accountant.groups.edit', $group->id) }}"
                                               class="text-blue-500 hover:text-blue-900 m-0 font-normal">{{ __('admin.links.show') }}</a>
{{--                                        <button wire:click="$emit('triggerDelete',{{ $group->id }})"--}}
{{--                                                class="text-red-500 hover:text-red-900 bg-none border-0 m-0 cursor-pointer bg-transparent font-normal ">{{ __('admin.links.delete') }}</button>--}}
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="mt-4">
                        {{ $groups->appends(request()->query())->links() }}
                    </div>
                </div>
            </div>

        </div>
    </div>

    @if($showAddPopup)
        @include('livewire.groups.group_add')
    @endif
    @push('scripts')
        <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>

        <script type="text/javascript">
            document.addEventListener('DOMContentLoaded', function () {
            @this.on('triggerDelete', itemID => {
                console.log(itemID);

                Swal.fire({
                    title: '{{ __("admin.swal.temp.title") }}',
                    text: '{{ __("admin.swal.groups.description") }}',
                    icon: "warning",
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#aaa',
                    cancelButtonText: '{{ __("admin.swal.temp.buttons.cancel") }}',
                    confirmButtonText: '{{ __("admin.swal.temp.buttons.approve") }}'
                }).then((result) => {
                    //if user clicks on delete
                    if (result.value) {
                        // calling destroy method to delete
                    @this.call('destroy',itemID);
                        // success response
                        Swal.fire({title: '{{ __("admin.swal.temp.deleted.title") }}', icon: 'success'});
                    } else {
                        Swal.fire({
                            title: '{{ __("admin.swal.temp.discarded.title") }}',
                            icon: 'success'
                        });
                    }
                });
            });
            })
        </script>

    @endpush

    @push('styles')
{{--        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/flatpickr/dist/flatpickr.min.css">--}}
{{--        <script src="https://cdn.jsdelivr.net/npm/flatpickr"></script>--}}
{{--        <script src="https://cdn.jsdelivr.net/npm/flatpickr/dist/l10n/ru.js"></script>--}}
    @endpush

</div>
