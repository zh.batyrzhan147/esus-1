<div>
    <div class="bg-white mx-auto py-6 px-4 sm:px-6 lg:px-8">
        <div class="flex w-full justify-between items-center">
            <div class="flex items-center">
                <h2 class="font-semibold text-xl text-gray-800 leading-tight">
                    {{ __('admin.groups.certificate.title') }}: {{ $group->slug }}
                </h2>
            </div>

            <div class="flex justify-end items-center">
                @if($group->status == \App\Models\Groups::GROUP_STATUS_FINISHED)
                    <button wire:click="$emit('triggerChangeStatus',{{ $group->status }})"
                            class="mb-2 text-sm flex justify-center items-center space-x-1.5 px-4 py-1 border-0 bg-blue-500 text-white rounded hover:bg-blue-600">
                        <span>{{ __('admin.groups.certificate.button') }}</span>
                    </button>
                @endif
                @if($group->status == \App\Models\Groups::GROUP_STATUS_CERTIFICATED)
                    <span>{{ __('admin.groups.certificate.button.done') }}</span>
                @endif
            </div>

        </div>
    </div>
    <div class="py-6 px-4 sm:px-6 lg:px-8">
        <div class="flex flex-col">
            @if($this->group->coursesItem->certificateTemplate)
                <div class="-my-2 overflow-x-auto sm:-mx-6 lg:-mx-8">
                    <div class="py-2 align-middle inline-block min-w-full sm:px-6 lg:px-8">
                        <div class="shadow overflow-hidden border-b border-gray-200 sm:rounded">
                            <table class="min-w-full divide-y divide-gray-200 w-full">
                                <thead>
                                <tr>
                                    <th scope="col"
                                        class="px-2 py-3 bg-gray-50 text-left text-sm font-medium text-gray-800">
                                        <div class="text-center">{{ __('admin.table.head.id') }}</div>
                                    </th>
                                    <th scope="col"
                                        class="px-2 py-3 bg-gray-50 text-left text-sm font-medium text-gray-800">
                                        <div class="pl-4 border-l border-gray-200">{{ __('admin.table.head.fio') }}</div>
                                    </th>
                                    <th scope="col"
                                        class="px-2 py-3 bg-gray-50 text-left text-sm font-medium text-gray-800">
                                        <div class="pl-4">{{ __('admin.table.head.group') }}</div>
                                    </th>
                                    <th scope="col" width="200"
                                        class="px-2 py-3 bg-gray-50 text-left text-sm font-medium text-gray-800">
                                        <div class="pl-4 border-l border-gray-200">{{ __('admin.table.head.action') }}</div>
                                    </th>
                                </tr>
                                </thead>
                                <tbody class="bg-white divide-y divide-gray-200">
                                @foreach($group->users_sb as $item)
                                    <tr>
                                        <td class="px-6 py-1.5 text-sm text-gray-900">
                                            {{ $item->id }}
                                        </td>
                                        <td class="px-6 py-1.5 whitespace-nowrap text-sm text-gray-900">
                                            {{ $item->fio }}
                                        </td>
                                        <td class="px-6 py-1.5 text-sm text-gray-900">
                                        </td>
                                        <td class="px-6 py-1.5 whitespace-nowrap text-sm font-medium space-x-2">
                                            @if($group->status != \App\Models\Groups::GROUP_STATUS_CERTIFICATED)
                                                <button wire:click="issue({{$item->id}})"
                                                        class="text-blue-500 hover:text-blue-900 m-0 font-normal">{{ __('admin.links.give') }}
                                                </button>
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>

                    <div class="py-2 align-middle inline-block min-w-full sm:px-6 lg:px-8">
                        <div class="shadow overflow-hidden border-b border-gray-200 sm:rounded">
                            <table class="min-w-full divide-y divide-gray-200 w-full">
                                <thead>
                                <tr>
                                    <th scope="col"
                                        class="px-2 py-3 bg-gray-50 text-left text-sm font-medium text-gray-800">
                                        <div class="text-center">{{ __('admin.table.head.id') }}</div>
                                    </th>
                                    <th scope="col"
                                        class="px-2 py-3 bg-gray-50 text-left text-sm font-medium text-gray-800">
                                        <div class="pl-4 border-l border-gray-200">{{ __('admin.table.head.fio') }}</div>
                                    </th>
                                    <th scope="col"
                                        class="px-2 py-3 bg-gray-50 text-left text-sm font-medium text-gray-800">
                                        <div class="pl-4">{{ __('admin.table.head.group') }}</div>
                                    </th>
                                    <th scope="col" width="200"
                                        class="px-2 py-3 bg-gray-50 text-left text-sm font-medium text-gray-800">
                                        <div class="pl-4 border-l border-gray-200">{{ __('admin.table.head.action') }}</div>
                                    </th>
                                </tr>
                                </thead>
                                <tbody class="bg-white divide-y divide-gray-200">
                                @forelse($certs as $item)
                                    <tr>
                                        <td class="px-6 py-1.5 text-sm text-gray-900">
                                            {{ $item->id }}
                                        </td>
                                        <td class="px-6 py-1.5 whitespace-nowrap text-sm text-gray-900">
                                            {{ $item->fio }}
                                        </td>
                                        <td class="px-6 py-1.5 text-sm text-gray-900">
                                            {{ $item->group_title }}
                                        </td>
                                        <td class="px-6 py-1.5 whitespace-nowrap text-sm font-medium space-x-2">
                                            @if($item->pdf)
                                                <a href="{{ $item->pdf }}" download="" target="_blank">{{ __('admin.links.download') }}</a>
                                                <button wire:click="$emit('triggerDelete',{{ $item->id }})"
                                                        class="text-red-500 hover:text-red-900 bg-none border-0 m-0 cursor-pointer bg-transparent font-normal ">{{ __('admin.links.delete') }}</button>
                                            @else
                                                <span>На обработке</span>
                                            @endif
                                        </td>
                                    </tr>
                                @empty
                                    <tr class="h-48 w-full"><td scope="col" class="h-48 w-full text-center text-xs text-gray-500" colspan="6">{{ __('admin.table.body.empty') }}</td></tr>
                                @endforelse
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            @else
                <div class="flex flex-col space-y-3">
                <span class="text-2xl text-red-600">{{ __('admin.groups.certificate.error.text') }} </span>
                <b>{{ $group->coursesItem->ru_title }}</b>
                <a href="{{ route('accountant.certificates-templates.index') }}" class="text-blue-600 text-xl hover:underline">{{ __('admin.groups.certificate.link.create') }}</a>
                </div>
            @endif
        </div>
    </div>
</div>

@push('scripts')
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    <script type="text/javascript">
        document.addEventListener('DOMContentLoaded', function () {
        @this.on('triggerChangeStatus', itemID => {
            Swal.fire({
                title: '{{ __("admin.swal.temp.title") }}',
                text: '',
                icon: "warning",
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#aaa',
                cancelButtonText: '{{ __("admin.swal.temp.buttons.cancel") }}',
                confirmButtonText: '{{ __("admin.swal.temp.buttons.approve") }}'
            }).then((result) => {
                //if user clicks on delete
                if (result.value) {
                    // calling destroy method to delete
                @this.call('changeGroupStatus', itemID);
                    // success response
                    Swal.fire({
                        title: 'Удачно!', icon: 'success',
                        width: 600,
                        position: 'top-end',
                        showConfirmButton: false,
                        timer: 1000
                    });
                } else {
                    Swal.fire({
                        title: '{{ __("admin.swal.temp.discarded.title") }}',
                        icon: 'success',
                        width: 600,
                        position: 'top-end',
                        showConfirmButton: false,
                        timer: 1000
                    });
                }
            });
        });

        @this.on('triggerDelete', itemID => {
            console.log(itemID);

            Swal.fire({
                title: '{{ __("admin.swal.temp.title") }}',
                text: 'Год обучения будет удален! Это может привести к необратимым последствиям!',
                icon: "warning",
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#aaa',
                confirmButtonText: 'Я уверен, удалить!'
            }).then((result) => {
                //if user clicks on delete
                if (result.value) {
                    // calling destroy method to delete
                @this.call('destroy',itemID);
                    // success response
                    Swal.fire({title: '{{ __("admin.swal.temp.deleted.title") }}', icon: 'success',
                        width: 600,
                        position: 'top-end',
                        showConfirmButton: false,
                        timer: 1000});
                } else {
                    Swal.fire({
                        title: '{{ __("admin.swal.temp.discarded.title") }}',
                        icon: 'success',
                        width: 600,
                        position: 'top-end',
                        showConfirmButton: false,
                        timer: 1000
                    });
                }
            });
        });
        })
    </script>

@endpush
