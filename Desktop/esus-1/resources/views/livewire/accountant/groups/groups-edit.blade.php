<div>
    <x-slot name="header">
        <div class="bg-white mx-auto py-6 px-4 sm:px-6 lg:px-8">
            <h2 class="font-semibold text-xl text-gray-800 leading-tight">
                {{ __('admin.groups.edit.title') }}
            </h2>
        </div>
    </x-slot>
    <p class="px-4 sm:px-6 lg:px-8 text-sm">{{ __('admin.groups.edit.description') }}</p>
    <div class="py-6 px-4 sm:px-6 lg:px-8 h-full">
        <div class="flex flex-col items-start md:items-center w-full md:w-3/4 mx-auto space-y-4">

            <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-center">
                <div class="w-full md:w-1/4">
                    <label for="affiliate" class="w-full text-left md:text-right block text-sm">{{ __('admin.groups.add.modal.affiliate') }}</label>
                </div>
                <div class="w-full md:w-3/4">
                    <select disabled wire:model="group.affiliate"
                            class="w-full md:w-3/4 px-3 py-1 border rounded focus:border-blue-500 hover:border-blue-500 text-sm outline-none focus:input-shadow">
                        <option value>{{ __('admin.groups.add.modal.chooseaffiliate') }}</option>
                        @foreach(\App\Models\Affiliates::all() ?? []  as $item)
                            <option value="{{ $item->id }}">{{ $item->title }}</option>
                        @endforeach
                    </select>
                    @error('group.affiliate')
                    <p class="text-xs text-red-600">{{ $message }}</p>
                    @enderror
                </div>
            </div>

            <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-center">
                <div class="w-full md:w-1/4">
                    <label for="year" class="w-full text-left md:text-right block text-sm">{{ __('admin.groups.add.modal.year') }}</label>
                </div>
                <div class="w-full md:w-3/4">
                    <select disabled wire:model="year"
                            class="w-full md:w-3/4 px-3 py-1 border rounded focus:border-blue-500 hover:border-blue-500 text-sm outline-none focus:input-shadow">
                        @foreach(\App\Models\Years::orderBy('year','ASC')->get() as $item)
                            <option value="{{ $item->year }}">{{ $item->year }}</option>
                        @endforeach
                    </select>
                    @error('year')
                    <p class="text-xs text-red-600">{{ $message }}</p>
                    @enderror
                </div>
            </div>

            <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-center">
                <div class="w-full md:w-1/4">
                    <label for="course" class="w-full text-left md:text-right block text-sm">{{ __('admin.groups.add.modal.program') }}</label>
                </div>
                <div class="w-full md:w-3/4">
                    <select disabled wire:model="courses"
                            class="w-full md:w-3/4 px-3 py-1 border rounded focus:border-blue-500 hover:border-blue-500 text-sm outline-none focus:input-shadow">
                        <option value>{{ __('admin.groups.add.modal.chooseprogram') }}</option>
                        @foreach($yearItem->courses ?? [] as $courseItem)
                            <option value="{{ $courseItem->id }}">{{ $courseItem->ru_title }}</option>
                        @endforeach
                    </select>
                    @error('courses')
                    <p class="text-xs text-red-600">{{ $message }}</p>
                    @enderror
                </div>
            </div>

            <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-center">
                <div class="w-full md:w-1/4">
                    <label for="language" class="w-full text-left md:text-right block text-sm">{{ __('admin.groups.add.modal.lang') }}</label>
                </div>
                <div class="w-full md:w-3/4">
                    <select disabled wire:model="group.language"
                            class="w-full md:w-3/4 px-3 py-1 border rounded focus:border-blue-500 hover:border-blue-500 text-sm outline-none focus:input-shadow">
                        <option value="">{{ __('admin.groups.add.modal.chooselang') }}</option>
                        @foreach(\App\Models\Languages::all() as $item)
                            <option value="{{ $item->id }}">{{ $item->title }}</option>
                        @endforeach
                    </select>
                    @error('group.language')
                    <p class="text-xs text-red-600">{{ $message }}</p>
                    @enderror
                </div>
            </div>

            <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-center">
                <div class="w-full md:w-1/4">
                    <label for="course" class="w-full text-left md:text-right block text-sm">{{ __('admin.groups.add.modal.trainer') }}</label>
                </div>
                <div class="w-full md:w-3/4">
                    <select disabled wire:model="group.trainer"
                            class="w-full md:w-3/4 px-3 py-1 border rounded focus:border-blue-500 hover:border-blue-500 text-sm outline-none focus:input-shadow">
                        <option value>{{ __('admin.groups.add.modal.choosetrainer') }}</option>
                        @foreach($trainers ?? []  as $item)
                            <option value="{{ $item->id }}">{{ $item->fio }}</option>
                        @endforeach
                    </select>
                    @error('group.trainer')
                    <p class="text-xs text-red-600">{{ $message }}</p>
                    @enderror
                </div>
            </div>


            <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-center">
                <div class="w-full md:w-1/4">
                    <label for="slug" class="w-full text-left md:text-right block text-sm">{{ __('admin.groups.add.modal.cifr') }}</label>
                </div>
                <div class="w-full md:w-3/4">
                    <input disabled wire:model="slug" type="text"
                           placeholder="{{ __('admin.groups.add.modal.cifr.placeholder') }}"
                           class="w-full md:w-3/4 px-3 py-1 border rounded focus:border-blue-500 hover:border-blue-500 text-sm outline-none focus:input-shadow"
                    />
                    @error('slug')
                    <p class="text-xs text-red-600">{{ $message }}</p>
                    @enderror
                </div>
            </div>

            <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-center">
                <div class="w-full md:w-1/4">
                    <label for="offer_agreement" class="w-full text-left md:text-right block text-sm">{{ __('admin.groups.add.modal.dogovor') }}</label>
                </div>
                @if($group->offer_agreement)
                    <a href="{{ $group->getFileUrl($group->offer_agreement) }}" download="{{ __('admin.groups.add.modal.dogovor') }}.{{ pathinfo($group->offer_agreement, PATHINFO_EXTENSION)}}" target="_blank">{{ __('admin.links.download') }}</a>
                @endif
                <div class="w-full md:w-3/4">
{{--                    <input disabled wire:model="offer_agreement" type="file"--}}
{{--                           class="w-full md:w-3/4 px-3 py-1 border rounded focus:border-blue-500 hover:border-blue-500 text-sm outline-none focus:input-shadow"--}}
{{--                    />--}}
                    @error('offer_agreement')
                    <p class="text-xs text-red-600">{{ $message }}</p>
                    @enderror
                </div>
            </div>

            <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-center">
                <div class="w-full md:w-1/4">
                    <label for="count_all" class="w-full text-left md:text-right block text-sm">{{ __('admin.groups.add.modal.listeners.count') }}</label>
                </div>
                <div class="w-full md:w-3/4">
                    <input disabled wire:model="group.count_all" type="text"
                           placeholder="{{ __('admin.groups.add.modal.listeners.count.placeholder') }}"
                           class="w-full md:w-3/4 px-3 py-1 border rounded focus:border-blue-500 hover:border-blue-500 text-sm outline-none focus:input-shadow"
                    />
                    @error('group.count_all')
                    <p class="text-xs text-red-600">{{ $message }}</p>
                    @enderror
                </div>
            </div>

            <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-center">
                <div class="w-full md:w-1/4">
                    <label for="start" placeholder="{{ __('admin.groups.add.modal.start.placeholder') }}" class="w-full text-left md:text-right block text-sm">{{ __('admin.groups.add.modal.start') }}</label>
                </div>
                <div class="w-full md:w-3/4">
                    <x-inputs.date disabled id="start" wire:model.lazy="group.start_time" class="w-full md:w-3/4 px-3 py-1 border rounded focus:border-blue-500 hover:border-blue-500 text-sm outline-none focus:input-shadow"/>
                    @error('group.start_time')
                    <p class="text-xs text-red-600">{{ $message }}</p>
                    @enderror
                </div>
            </div>

            <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-center">
                <div class="w-full md:w-1/4">
                    <label for="end" class="w-full text-left md:text-right block text-sm">{{ __('admin.groups.add.modal.end') }}</label>
                </div>
                <div class="w-full md:w-3/4">
                    <x-inputs.date disabled  id="end" wire:model.lazy="group.end_time" placeholder="{{ __('admin.groups.add.modal.end.placeholder') }}" class="w-full md:w-3/4 px-3 py-1 border rounded focus:border-blue-500 hover:border-blue-500 text-sm outline-none focus:input-shadow"/>
                    @error('group.end_time')
                    <p class="text-xs text-red-600">{{ $message }}</p>
                    @enderror
                </div>
            </div>

            <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-center">
                <div class="w-full md:w-1/4">
                    <label for="cert_day" placeholder="{{ __('Дата выдачи сертификата') }}"
                           class="w-full text-left md:text-right block text-sm">{{ __('Дата выдачи сертификата') }}</label>
                </div>
                <div class="w-full md:w-3/4">
                    <x-inputs.date disabled id="cert_day" wire:model.lazy="group.cert_day"
                                   class="w-full md:w-3/4 px-3 py-1 border rounded focus:border-blue-500 hover:border-blue-500 text-sm outline-none focus:input-shadow"/>
                    @error('group.cert_day')
                    <p class="text-xs text-red-600">{{ $message }}</p>
                    @enderror
                </div>
            </div>

            <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-center">
                <div class="w-full md:w-1/4">
                    <label for="status" class="w-full text-left md:text-right block text-sm">{{ __('admin.groups.add.modal.status') }}</label>
                </div>
                <div class="w-full md:w-3/4">
                    <select disabled wire:model="group.status"
                            class="w-full md:w-3/4 px-3 py-1 border rounded focus:border-blue-500 hover:border-blue-500 text-sm outline-none focus:input-shadow">
                        <option value>{{ __('admin.groups.add.modal.choosestatus') }}</option>
                        @foreach(\App\Models\Groups::listGroupStatuses() ?? []  as $key => $item)
                            <option value="{{ $key }}">{{ $item }}</option>
                        @endforeach
                    </select>
                    @error('group.status')
                    <p class="text-xs text-red-600">{{ $message }}</p>
                    @enderror
                </div>
            </div>

            <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-center">
                <div class="w-full md:w-1/4">
                </div>
                <div class="w-full md:w-3/4 space-x-2 flex">
{{--                    <button wire:click="save"--}}
{{--                            class="mb-2 text-sm flex justify-center items-center px-4 py-1 border-0 bg-blue-500 text-white rounded hover:bg-blue-600">--}}
{{--                        {{ __('admin.buttons.save') }}--}}
{{--                    </button>--}}
                    <a href="{{ route('accountant.groups.index') }}"
                       class="py-1 px-4 border inline-block m-0 mb-2 text-sm rounded hover:border-black">{{ __('admin.buttons.cancel') }}</a>
                </div>
            </div>
        </div>
    </div>
    <div class="bg-white mx-auto py-6 px-4 sm:px-6 lg:px-8 flex flex-row justify-between items-end">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Список слушателей') }}
        </h2>
        <button wire:click="getAllStatements()" class="font-medium text-[#FFFFFF] px-8 py-2.5 rounded bg-[#3B82F6] hover:bg-[#2563EB]">
            {{ __('trainer.buttons.table.download') }}
        </button>
    </div>
    <div class="py-6 px-4 sm:px-6 lg:px-8 h-full">
        <div class="py-2 align-middle inline-block min-w-full sm:px-6 lg:px-8">
            <div class="shadow overflow-hidden border-b border-gray-200 sm:rounded">
                <table class="min-w-full divide-y divide-gray-200 w-full">
                    <thead>
                    <tr>
                        <th scope="col"
                            class="px-2 py-3 bg-gray-50 text-left text-sm font-medium text-gray-800">
                            <div class="text-center">{{ __('admin.table.head.id') }}</div>
                        </th>
                        <th scope="col"
                            class="px-2 py-3 bg-gray-50 text-left text-sm font-medium text-gray-800">
                            <div class="pl-4 border-l border-gray-200">{{ __('admin.table.head.fio') }}</div>
                        </th>
                        <th scope="col"
                            class="px-2 py-3 bg-gray-50 text-left text-sm font-medium text-gray-800">
                            <div class="pl-4">{{ __('admin.table.head.group') }}</div>
                        </th>
                        <th scope="col" width="200"
                            class="px-2 py-3 bg-gray-50 text-left text-sm font-medium text-gray-800">
                            <div class="pl-4 border-l border-gray-200">{{ __('admin.table.head.action') }}</div>
                        </th>
                    </tr>
                    </thead>
                    <tbody class="bg-white divide-y divide-gray-200">
                    @foreach($group->users as $item)
                        <tr>
                            <td class="px-6 py-1.5 text-sm text-gray-900">
                                {{ $item->id }}
                            </td>
                            <td class="px-6 py-1.5 whitespace-nowrap text-sm text-gray-900">
                                {{ $item->fio }}
                            </td>
                            <td class="px-6 py-1.5 text-sm text-gray-900">
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

@push('styles')
{{--    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/flatpickr/dist/flatpickr.min.css">--}}
{{--    <script src="https://cdn.jsdelivr.net/npm/flatpickr"></script>--}}
{{--    <script src="https://cdn.jsdelivr.net/npm/flatpickr/dist/l10n/ru.js"></script>--}}
@endpush
