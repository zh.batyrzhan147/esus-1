<div>
    <x-slot name="header"></x-slot>
        <div class="bg-white mx-auto py-6 px-4 sm:px-6 lg:px-8">
            <div class="w-full flex justify-between items-center">
                <h2 class="font-semibold text-xl text-gray-800 leading-tight">
                    {{ __('admin.reports.list.title') }}
                </h2>
                <button wire:click="add_new"
                        class="text-sm flex justify-center items-center space-x-1.5 px-4 py-1 border-0 bg-blue-500 text-white rounded hover:bg-blue-600">
                    <svg width="14" height="14" viewBox="0 0 14 14" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path
                            d="M7.71838 1.62408H6.78088C6.69755 1.62408 6.65588 1.66575 6.65588 1.74908V6.65533H2C1.91667 6.65533 1.875 6.697 1.875 6.78033V7.71783C1.875 7.80117 1.91667 7.84283 2 7.84283H6.65588V12.7491C6.65588 12.8324 6.69755 12.8741 6.78088 12.8741H7.71838C7.80172 12.8741 7.84338 12.8324 7.84338 12.7491V7.84283H12.5C12.5833 7.84283 12.625 7.80117 12.625 7.71783V6.78033C12.625 6.697 12.5833 6.65533 12.5 6.65533H7.84338V1.74908C7.84338 1.66575 7.80172 1.62408 7.71838 1.62408Z"
                            fill="white"/>
                    </svg>
                    <span>Добавить</span>
                </button>
            </div>
        </div>



    <div class="flex flex-col mx-auto py-2 px-4 sm:px-6 lg:px-8">
{{--        <div class="w-full flex justify-between space-x-4">--}}
{{--            <div class="flex flex-col mb-4 w-full">--}}
{{--                <label class="flex flex-col text-left" for="iinfio">--}}
{{--                    <span class="text-gray-700 mb-2">{{ __('admin.statements-messages.list.filters.iin') }}</span>--}}
{{--                </label>--}}
{{--                <input type="text" wire:keydown.enter="applyFilter" wire:model.defer="search" id="iinfio"--}}
{{--                       class="w-3/4 px-3 py-1 border rounded focus:border-blue-500 hover:border-blue-500 text-sm outline-none focus:input-shadow"--}}
{{--                       placeholder="{{ __('admin.statements-messages.list.filters.iin.placeholder') }}"--}}
{{--                />--}}
{{--            </div>--}}
{{--            <div class="flex space-x-4">--}}
{{--                <div class="flex justify-between items-center mb-4">--}}
{{--                    <label class="flex flex-col text-left">--}}
{{--                        <span class="text-gray-700 mb-2">{{ __('admin.statements-messages.list.filters.affiliate') }}</span>--}}
{{--                        <select wire:model.defer="affiliate"--}}
{{--                                class="w-48 px-3 py-1 border rounded focus:border-blue-500 hover:border-blue-500 text-sm outline-none focus:input-shadow">--}}
{{--                            <option value>{{ __('admin.courses.index.filter.all') }}</option>--}}
{{--                            @foreach(\App\Models\Affiliates::all() as $item)--}}
{{--                                <option value="{{ $item->id }}">{{ $item->title }}</option>--}}
{{--                            @endforeach--}}
{{--                        </select>--}}
{{--                    </label>--}}
{{--                </div>--}}
{{--                <div class="flex justify-between items-center mb-4">--}}
{{--                    <label class="flex flex-col text-left">--}}
{{--                        <span class="text-gray-700 mb-2">{{ __('admin.statements-messages.list.filters.program') }}</span>--}}
{{--                        <select class="w-96 px-3 py-1 border rounded focus:border-blue-500 hover:border-blue-500 text-sm outline-none focus:input-shadow">--}}
{{--                            --}}{{--                    @foreach()--}}
{{--                            <option value="1">1</option>--}}
{{--                            --}}{{--                    @endforeach--}}
{{--                        </select>--}}
{{--                    </label>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--        <div class="flex justify-end">--}}
{{--            <button wire:click="applyFilter"--}}
{{--                    class="text-sm flex justify-center items-center space-x-1.5 px-4 py-1 border-0 bg-blue-500 text-white rounded hover:bg-blue-600">--}}
{{--                <span>{{ __('admin.buttons.accept.filters') }}</span>--}}
{{--            </button>--}}
{{--        </div>--}}
    </div>

    <div class="py-6 px-4 sm:px-6 lg:px-8">
        <div class="flex flex-col">
            <div class="-my-2 overflow-x-auto sm:-mx-6 lg:-mx-8">
                <div class="py-2 align-middle inline-block min-w-full sm:px-6 lg:px-8">
                    <div class="shadow overflow-hidden border-b border-gray-200 sm:rounded">
                        <table class="min-w-full divide-y divide-gray-200 w-full">
                            <thead>
                            <tr>
                                <th scope="col"
                                    class="px-2 py-3 bg-gray-50 text-left text-sm font-medium text-gray-800">
                                    <div class="pl-4">{{ __('admin.table.head.id') }}</div>
                                </th>
                                <th scope="col"
                                    class="px-2 py-3 bg-gray-50 text-left text-sm font-medium text-gray-800">
                                    <div class="pl-4 border-l border-gray-200">{{ __('Тип отчета') }}</div>
                                </th>
                                <th scope="col"
                                    class="px-2 py-3 bg-gray-50 text-left text-sm font-medium text-gray-800">
                                    <div class="pl-4 border-l border-gray-200">{{ __('Курс') }}</div>
                                </th>
                                <th scope="col"
                                    class="px-2 py-3 bg-gray-50 text-left text-sm font-medium text-gray-800">
                                    <div class="pl-4 border-l border-gray-200">{{ __('Начало') }}</div>
                                </th>
                                <th scope="col"
                                    class="px-2 py-3 bg-gray-50 text-left text-sm font-medium text-gray-800">
                                    <div class="pl-4 border-l border-gray-200">{{ __('Конец') }}</div>
                                </th>
                                <th scope="col"
                                    class="px-2 py-3 bg-gray-50 text-left text-sm font-medium text-gray-800">
                                    <div class="pl-4 border-l border-gray-200">{{ __('admin.table.head.status') }}</div>
                                </th>
                                <th scope="col" width="200"
                                    class="px-2 py-3 bg-gray-50 text-left text-sm font-medium text-gray-800">
                                    <div class="pl-4 border-l border-gray-200">{{ __('admin.table.head.action') }}</div>
                                </th>
                            </tr>
                            </thead>
                            <tbody class="bg-white divide-y divide-gray-200">
                            @foreach ($reports as $item)
                                <tr>
                                    <td class="px-6 py-1.5 whitespace-nowrap text-sm text-gray-900">
                                        {{ $item->id }}
                                    </td>

                                    <td class="px-6 py-1.5 whitespace-nowrap text-sm text-gray-900">
                                        {{ \App\Models\Reports::listTypes()[$item->type]  }}
                                    </td>

                                    <td class="px-6 py-1.5  text-sm text-gray-900">
                                        {!!  $item->courseItem->ru_title ?? ''  !!}
                                    </td>

                                    <td class="px-6 py-1.5 whitespace-nowrap text-sm text-gray-900">
                                        {{ date('d.m.Y',strtotime($item->starttime)) }}
                                    </td>

                                    <td class="px-6 py-1.5 whitespace-nowrap text-sm text-gray-900">
                                        {{ date('d.m.Y',strtotime($item->endtime)) }}
                                    </td>

                                    <td class="px-6 py-1.5 whitespace-nowrap text-sm text-gray-900">
                                        {{ \App\Models\Reports::listStatuses()[$item->status] }}
                                    </td>

                                    <td class="px-6 py-1.5 whitespace-nowrap text-sm font-medium space-x-2 text-right">
                                        @if($item->file)
                                            <a href="{{ $item->getFileUrl($item->file) }}"
                                               class="text-blue-500 hover:text-blue-900 m-0 font-normal">{{ __('admin.links.download') }}</a>
                                        @endif

                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="mt-4" wire:ignore>
                        {{ $reports->appends(request()->query())->links('admin.common.pagination') }}
                    </div>
                </div>
            </div>
        </div>
    </div>
    @if($modal_add_new)
        <x-modal>
            <x-slot name="header">
                Создать отчет:
                <br><br>
            </x-slot>
            <div class="space-y-2">
                <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-center">
                    <div class="w-full md:w-1/4">
                        <label for="type" class="w-full text-left md:text-right block text-sm">{{ __('admin.courses.create.form.course.type') }}</label>
                    </div>
                    <div class="w-full md:w-3/4">
                        <select wire:model="reporttype" id="type"
                                class="w-full md:w-3/4 px-3 py-1 border rounded focus:border-blue-500 hover:border-blue-500 text-sm outline-none focus:input-shadow">
                            <option value>Выберите тип отчета</option>
                            @foreach(\App\Models\Reports::listTypes() as $key => $type)
                                <option value="{{ $key }}">{{ $type }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>

                @if($reporttype == 1)
                    <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-center">
                        <div class="w-full md:w-1/4">
                            <label for="starttime" class="w-full text-left md:text-right block text-sm">{{ __('Начиная с:') }}</label>
                        </div>
                        <div class="w-full md:w-3/4">
                            <x-inputs.date id="starttime" wire:model.defer="starttime" placeholder="{{ __('admin.weekends.modal.date.placeholder') }}" class="w-full md:w-3/4 px-3 py-1 border rounded focus:border-blue-500 hover:border-blue-500 text-sm outline-none focus:input-shadow"/>
                            @error('starttime')
                            <p class="text-xs text-red-600">{{ $message }}</p>
                            @enderror
                        </div>
                    </div>

                    <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-center">
                        <div class="w-full md:w-1/4">
                            <label for="endtime" class="w-full text-left md:text-right block text-sm">{{ __('по:') }}</label>
                        </div>
                        <div class="w-full md:w-3/4">
                            <x-inputs.date id="endtime" wire:model.defer="endtime" placeholder="{{ __('admin.weekends.modal.date.placeholder') }}" class="w-full md:w-3/4 px-3 py-1 border rounded focus:border-blue-500 hover:border-blue-500 text-sm outline-none focus:input-shadow"/>
                            @error('endtime')
                            <p class="text-xs text-red-600">{{ $message }}</p>
                            @enderror
                        </div>
                    </div>

                    <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-center">
                        <div class="w-full md:w-1/4">
                            <label for="year"
                                   class="w-full text-left md:text-right block text-sm">{{ __('admin.groups.add.modal.year') }}</label>
                        </div>
                        <div class="w-full md:w-3/4">
                            <select wire:model="year"
                                    class="w-full md:w-3/4 px-3 py-1 border rounded focus:border-blue-500 hover:border-blue-500 text-sm outline-none focus:input-shadow">
                                <option value>{{ __('admin.groups.add.modal.chooseyear') }}</option>
                                @foreach(\App\Models\Years::orderBy('year','ASC')->get() as $item)
                                    <option value="{{ $item->year }}">{{ $item->year }}</option>
                                @endforeach
                            </select>
                            @error('year')
                            <p class="text-xs text-red-600">{{ $message }}</p>
                            @enderror
                        </div>
                    </div>

                    <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-center">
                        <div class="w-full md:w-1/4">
                            <label for="courses"
                                   class="w-full text-left md:text-right block text-sm">{{ __('admin.groups.add.modal.program') }}</label>
                        </div>
                        <div class="w-full md:w-3/4">
                            <select wire:model="courses"
                                    class="w-full md:w-3/4 px-3 py-1 border rounded focus:border-blue-500 hover:border-blue-500 text-sm outline-none focus:input-shadow">
                                <option value>{{ __('admin.groups.add.modal.chooseprogram') }}</option>
                                @foreach($yearItem->courses ?? [] as $courseItem)
                                    <option value="{{ $courseItem->id }}">{{ $courseItem->ru_title }}</option>
                                @endforeach
                            </select>
                            @error('courses')
                            <p class="text-xs text-red-600">{{ $message }}</p>
                            @enderror
                        </div>
                    </div>

                    <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-center">
                        <div class="w-full md:w-1/4">
                            <label for="language"
                                   class="w-full text-left md:text-right block text-sm">{{ __('admin.groups.add.modal.lang') }}</label>
                        </div>
                        <div class="w-full md:w-3/4">
                            <select wire:model="language"
                                    class="w-full md:w-3/4 px-3 py-1 border rounded focus:border-blue-500 hover:border-blue-500 text-sm outline-none focus:input-shadow">
                                <option value="">{{ __('admin.groups.add.modal.chooselang') }}</option>
                                @foreach(\App\Models\Languages::all() as $item)
                                    <option value="{{ $item->id }}">{{ $item->title }}</option>
                                @endforeach
                            </select>
                            @error('language')
                            <p class="text-xs text-red-600">{{ $message }}</p>
                            @enderror
                        </div>
                    </div>

                @endif

                <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-center">
                    <div class="w-full md:w-1/4">
                    </div>
                    <div class="w-full md:w-3/4 space-x-2 flex">
                        @if($reporttype)
                        <button wire:click="add_report"
                                class="mb-2 text-sm flex justify-center items-center px-4 py-1 border-0 bg-blue-500 text-white rounded hover:bg-blue-600">
                            Добавить
                        </button>
                        @endif
                        <button wire:click="$set('modal_add_new',false)"
                                class="py-1 px-4 border inline-block m-0 mb-2 text-sm rounded hover:border-black">
                            Отмена
                        </button>
                    </div>
                </div>
            </div>
        </x-modal>
    @endif
</div>

@push('styles')
{{--    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/flatpickr/dist/flatpickr.min.css">--}}
{{--    <script src="https://cdn.jsdelivr.net/npm/flatpickr"></script>--}}
{{--    <script src="https://cdn.jsdelivr.net/npm/flatpickr/dist/l10n/ru.js"></script>--}}
@endpush
