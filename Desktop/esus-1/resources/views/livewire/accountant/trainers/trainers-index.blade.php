<div>
    <x-slot name="header">
        <div class="bg-white mx-auto py-6 px-4 sm:px-6 lg:px-8">
            <div class="w-full flex justify-between items-center">
                <h2 class="font-semibold text-xl text-gray-800 leading-tight">
                    {{ __('admin.trainers.list.title') }}
                </h2>

                @include('admin.common.secondmenu', [
                    'tabname'=>'groups',
                    'popup'=>true,
                    'addlink'=>''
                ])
            </div>
        </div>
    </x-slot>


    <div class="flex flex-col mx-auto py-2 px-4 sm:px-6 lg:px-8">
        <div class="w-full flex justify-between space-x-4">
            <div class="flex flex-col mb-4 w-full">
                <label class="flex flex-col text-left" for="iinfio">
                    <span class="text-gray-700 mb-2">{{ __('admin.trainers.list.filters.iin') }}</span>
                </label>
                <input type="text" wire:keydown.enter="applyFilter" wire:model.defer="search" id="iinfio"
                       class="w-3/4 px-3 py-1 border rounded focus:border-blue-500 hover:border-blue-500 text-sm outline-none focus:input-shadow"
                       placeholder="{{ __('admin.trainers.list.filters.iin.placeholder') }}"
                />
            </div>
            <div class="flex space-x-4">
                <div class="flex justify-between items-center mb-4">
                    <label class="flex flex-col text-left">
                        <span class="text-gray-700 mb-2">{{ __('admin.trainers.list.filters.affiliate') }}</span>
                        <select wire:model.defer="affiliate"
                                class="w-48 px-3 py-1 border rounded focus:border-blue-500 hover:border-blue-500 text-sm outline-none focus:input-shadow">
                            <option value>{{ __('admin.courses.index.filter.all') }}</option>
                            @foreach(\App\Models\Affiliates::all() as $item)
                                <option value="{{ $item->id }}">{{ $item->title }}</option>
                            @endforeach
                        </select>
                    </label>
                </div>
                {{--                <div class="flex justify-between items-center mb-4">--}}
                {{--                    <label class="flex flex-col text-left">--}}
                {{--                        <span class="text-gray-700 mb-2">{{ __('admin.trainers.list.filters.program') }}</span>--}}

                {{--                        <select wire:model.defer="courses" class="w-96 px-3 py-1 border rounded focus:border-blue-500 hover:border-blue-500 text-sm outline-none focus:input-shadow" id="courses">--}}
                {{--                            --}}{{--                                                @foreach()--}}
                {{--                            <option value="1">1</option>--}}
                {{--                            --}}{{--                                                @endforeach--}}
                {{--                        </select>--}}
                {{--                    </label>--}}
                {{--                </div>--}}
                <div class="flex flex-col mb-4">
                    <label class="flex flex-col text-left">
                        <span class="text-gray-700 mb-2 whitespace-nowrap">{{ __('admin.trainers.list.filters.lang') }}</span>
                    </label>
                    <div class="flex items-center space-x-2">
                        <select wire:model.defer="language"
                                class="w-48 px-3 py-1 border rounded focus:border-blue-500 hover:border-blue-500 text-sm outline-none focus:input-shadow">
                            <option value>{{ __('admin.courses.index.filter.all') }}</option>
                            @foreach(\App\Models\Languages::all() as $item)
                                <option value="{{ $item->id }}">{{ $item->title }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
        </div>
        <div class="flex justify-end">
            <button wire:click="applyFilter"
                    class="text-sm flex justify-center items-center space-x-1.5 px-4 py-1 border-0 bg-blue-500 text-white rounded hover:bg-blue-600">
                <span>{{ __('admin.buttons.accept.filters') }}</span>
            </button>
        </div>
    </div>

    <div class="py-6 px-4 sm:px-6 lg:px-8">
        <div class="flex flex-col">
            <div class="-my-2 overflow-x-auto sm:-mx-6 lg:-mx-8">
                <div class="py-2 align-middle inline-block min-w-full sm:px-6 lg:px-8">
                    <div class="shadow overflow-hidden border-b border-gray-200 sm:rounded">
                        <table class="min-w-full divide-y divide-gray-200 w-full">
                            <thead>
                            <tr>
                                <th scope="col"
                                    class="px-2 py-3 bg-gray-50 text-left text-sm font-medium text-gray-800">
                                    <div class="pl-4">{{ __('admin.table.head.iin') }}</div>
                                </th>
                                <th scope="col"
                                    class="px-2 py-3 bg-gray-50 text-left text-sm font-medium text-gray-800">
                                    <div class="pl-4 border-l border-gray-200">{{ __('admin.table.head.fio') }}</div>
                                </th>
                                <th scope="col"
                                    class="px-2 py-3 bg-gray-50 text-left text-sm font-medium text-gray-800">
                                    <div class="pl-4 border-l border-gray-200">{{ __('admin.table.head.affiliate') }}</div>
                                </th>
                                <th scope="col"
                                    class="px-2 py-3 bg-gray-50 text-left text-sm font-medium text-gray-800">
                                    <div class="pl-4 border-l border-gray-200">{{ __('admin.table.head.hours') }}</div>
                                </th>
                                <th scope="col" width="200"
                                    class="px-2 py-3 bg-gray-50 text-left text-sm font-medium text-gray-800">
                                    <div class="pl-4 border-l border-gray-200">{{ __('admin.table.head.action') }}</div>
                                </th>
                            </tr>
                            </thead>
                            <tbody class="bg-white divide-y divide-gray-200">
                            {{--                            @dd($trainers)--}}
                            @foreach ($trainers as $trainer)
                                <tr>
                                    <td class="px-6 py-1.5 whitespace-nowrap text-sm text-gray-900">
                                        {{ $trainer->iin }}
                                    </td>

                                    <td class="px-6 py-1.5 whitespace-nowrap text-sm text-gray-900">
                                        {{ $trainer->fio  }}
                                    </td>

                                    <td class="px-6 py-1.5 whitespace-nowrap text-sm text-gray-900">
                                        {{ $trainer->affiliateItem->title ?? '' }}
                                    </td>

                                    <td class="px-6 py-1.5 whitespace-nowrap text-sm text-gray-900">
                                        {{ $trainer->hours_count }} {{ __('admin.table.body.hours') }}
                                    </td>

                                    <td class="px-6 py-1.5 whitespace-nowrap text-sm font-medium space-x-2 text-right">
                                        <a href="{{ route('accountant.trainers.show', $trainer->id) }}"
                                           class="text-blue-500 hover:text-blue-900 m-0 font-normal">{{ __('admin.links.show') }}</a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="mt-4">
                        {{ $trainers->appends(request()->query())->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
