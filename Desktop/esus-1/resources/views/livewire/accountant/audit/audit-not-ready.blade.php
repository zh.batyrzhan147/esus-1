<div>
    <div class="bg-white mx-auto py-6 px-4 sm:px-6 lg:px-8">
        <div class="flex justify-between items-center w-full">
            <h2 class="font-semibold text-xl text-gray-800 leading-tight">
                @if ($type == \App\Models\AuditReports::AUDIT_REPORT_TYPE_GROUPS)
                    Аудит групп за {{ $year }} год
                @elseif($type == \App\Models\AuditReports::AUDIT_REPORT_TYPE_COURSES)
                    Аудит курсов за {{ $year }} год
                @endif
            </h2>
        </div>
    </div>

    <div class="bg-white mx-auto py-6 px-4 sm:px-6 lg:px-8">
        <div class="flex justify-between items-center w-full">
            <p class="font-normal text-xl text-gray-800 leading-tight">
                Система в процессе обработки вашего запроса...
            </p>
        </div>
    </div>

</div>
