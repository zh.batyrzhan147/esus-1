<div>
    <x-slot name="header"></x-slot>

    <div class="bg-white mx-auto py-6 px-4 sm:px-6 lg:px-8">
        <div class="w-full flex justify-between items-center">
            <h2 class="font-semibold text-xl text-gray-800 leading-tight">
                {{ __('admin.groups.payments.title') }}
            </h2>
            @include('admin.common.secondmenu', [
                'tabname'=>'groups',
                'popup'=>true,
            ])

      
        </div>

        <button wire:click="downloadxlsx" class=" font-medium text-[#FFFFFF] px-8 py-2.5 rounded bg-[#3B82F6] hover:bg-[#2563EB]" style="float: right;">
                    {{ __('Скачать') }}
        </button>
    </div>

    <div class="flex flex-col mx-auto py-2 px-4 sm:px-6 lg:px-8">
        <div class="w-full flex justify-between space-x-4">


        <div class="flex flex-col mb-4 w-96">
                <label class="flex flex-col text-left">
                    <span class="text-gray-700 mb-2">{{ __('admin.schedule.list.filter.start') }}</span>
                </label>
                <x-inputs.date id="start" wire:model.defer="start_time" />
                @error('start_time')
                <p class="text-xs text-red-600">{{ $message }}</p>
                @enderror
            </div>
            <div class="flex flex-col mb-4 w-96">
                <label class="flex flex-col text-left">
                    <span class="text-gray-700 mb-2">{{ __('admin.schedule.list.filter.end') }} (Дейін)</span>
                </label>
                <x-inputs.date id="end" wire:model.defer="end_time" />
                @error('end_time')
                <p class="text-xs text-red-600">{{ $message }}</p>
                @enderror
            </div>

            <div class="flex flex-col mb-4 w-96">
                <label class="flex flex-col text-left">
                    <span class="text-gray-700 mb-2">{{ __('admin.groups.list.filter.search') }}</span>
                </label>
                <input type="text" wire:keydown.enter="applyFilter" wire:model.defer="search"
                       class="w-3/4 px-3 py-1 border rounded focus:border-blue-500 hover:border-blue-500 text-sm outline-none focus:input-shadow"
                       placeholder="Введите название"
                />
            </div> 
            <div class="flex flex-col mb-4 w-96">
                <label class="flex flex-col text-left">
                    <span class="text-gray-700 mb-2">{{ __('admin.groups.list.filter.year') }}</span>
                </label>
                <select wire:model="year" class="w-full px-3 py-1 border rounded focus:border-blue-500 hover:border-blue-500 text-sm outline-none focus:input-shadow">
                    <option value>{{ __('admin.groups.list.filter.year.all') }}</option>
                    @foreach(\App\Models\Years::orderBy('year','ASC')->get() as $item)
                        <option value="{{ $item->year }}">{{ $item->year }}</option>
                    @endforeach
                </select>
            </div>
            <div class="flex flex-col mb-4 w-full">
                <label class="flex flex-col text-left">
                    <span class="text-gray-700 mb-2">{{ __('admin.groups.list.filter.program') }}</span>
                </label>
                <select wire:model.defer="course" class="w-full px-3 py-1 border rounded focus:border-blue-500 hover:border-blue-500 text-sm outline-none focus:input-shadow">
                    <option value>{{ __('admin.groups.list.filter.program.all') }}</option>
                    @foreach($yearItem->courses ?? [] as $courseItem)
                        <option value="{{ $courseItem->id }}">@if(app()->getLocale() == 'kz') {{$courseItem->kz_title}} @else {{$courseItem->ru_title}} @endif</option>
                    @endforeach
                </select>
            </div>

         <div class="flex flex-col mb-4 w-1/3">
                <label class="flex flex-col text-left">
                    <span class="text-gray-700 mb-2">{{ __('admin.groups.list.filter.affiliates') }}</span>
                </label>
                <select wire:model.defer="affiliate" class="w-full px-3 py-1 border rounded focus:border-blue-500 hover:border-blue-500 text-sm outline-none focus:input-shadow">
                    <option value>{{ __('admin.groups.list.filter.affiliates.all') }}</option>
                    @foreach(\App\Models\Affiliates::select('id','title')->get()->pluck('title','id')->toArray() as $key => $stat)
                        <option value="{{ $key }}">{{ $stat }}</option>
                    @endforeach
                </select>
            </div>
 
        </div>
        <div class="flex justify-end">
            <button wire:click="applyFilter"
                    class="text-sm flex justify-center items-center space-x-1.5 px-4 py-1 border-0 bg-blue-500 text-white rounded hover:bg-blue-600">
                <span>{{ __('admin.buttons.accept.filters') }}</span>
            </button>
        </div>
       
    </div> 

    <div class="py-6 px-4 sm:px-6 lg:px-8">
        <div class="flex flex-col">
            <div class="-my-2 overflow-x-auto sm:-mx-6 lg:-mx-8">
                <div class="py-2 align-middle inline-block min-w-full sm:px-6 lg:px-8">
                    <div class="shadow overflow-hidden border-b border-gray-200 sm:rounded">
                        <table class="" style=" table-layout: auto;
    border-collapse: collapse;
    width: 100%;">
                            <thead>
                            <tr>
                                <th scope="col" class="px-2 py-3 bg-gray-50 text-left text-sm font-medium text-gray-800">
                                    <div class="pl-4">{{ __('ID') }}</div>
                                </th>
                                <th scope="col" width="200" class="px-2 py-3 bg-gray-50 text-left text-sm font-medium text-gray-800">
                                    <div class="pl-4 border-l border-gray-200">{{ __('ФИО') }}</div>
                                </th>

                                <th scope="col" width="200" class="px-2 py-3 bg-gray-50 text-left text-sm font-medium text-gray-800">
                                    <div class="pl-4 border-l border-gray-200">{{ __('Область') }}</div>
                                </th>

                                <th scope="col" width="200" class="px-2 py-3 bg-gray-50 text-left text-sm font-medium text-gray-800">
                                    <div class="pl-4 border-l border-gray-200">{{ __('Филиал') }}</div>
                                </th>
                                
                                <th scope="col" width="200" class="px-2 py-3 bg-gray-50 text-left text-sm font-medium text-gray-800">
                                    <div class="pl-4 border-l border-gray-200">{{ __('Программа') }}</div>
                                </th>
                                <th scope="col" class="px-2 py-3 bg-gray-50 text-left text-sm font-medium text-gray-800">
                                    <div class="pl-4 border-l border-gray-200">{{ __('Тренер') }}</div>
                                </th>

                                <th scope="col" class="px-2 py-3 bg-gray-50 text-left text-sm font-medium text-gray-800">
                                    <div class="pl-4 border-l border-gray-200">{{ __('Шифр') }}</div>
                                </th>

                                <th scope="col" class="px-2 py-3 bg-gray-50 text-left text-sm font-medium text-gray-800">
                                    <div class="pl-4 border-l border-gray-200">{{ __('Дата оплаты') }}</div>
                                </th>

                                <th scope="col" class="px-2 py-3 bg-gray-50 text-left text-sm font-medium text-gray-800">
                                    <div class="pl-4">{{ __('Сумма') }}</div>
                                </th>
                               
                            </tr>
                            </thead>
                            <tbody class="bg-white divide-y divide-gray-200" style="border: 1px solid;">
                            @foreach ($orders as $order)
                            @if($order->isPayed())
                                <tr style="border: 1px solid #BBCFF3;">
                                    <td class="px-6 py-1.5 whitespace-nowrap text-sm text-gray-900">
                                        {{ $order->id }}
                                    </td>

                                    <td class="px-6 py-1.5 whitespace-nowrap text-sm text-gray-900">
                                    {{ $order->getUserWithId( $order->user_id)}}
                                    </td>

                                    <td class="px-6 py-1.5 whitespace-nowrap text-sm text-gray-900">
                                    {{ $order->getOblastWithId( $order->user_id)}}
                                    </td>

                                    <td class="px-6 py-1.5 whitespace-nowrap text-sm text-gray-900">
                                    {{ $order->getAffilateWithId( $order->target_id)}}
                                    </td>
                            
                                    <td class="px-6 py-1.5 whitespace-nowrap text-sm text-gray-900">
                                        <div style="overflow-x:auto; width: 400px; white-space: normal;">{{ $order->getCourseNameWithTargetId( $order->target_id)}}
                                        </div>
                                   
                                    </td>

                                    <td class="px-6 py-1.5 whitespace-nowrap text-sm text-gray-900">
                                    {{ $order->getTrainerWithId( $order->target_id) }}
                                    </td>

                                    <td class="px-6 py-1.5 whitespace-nowrap text-sm text-gray-900">
                                    {{ $order->getSlugWithId( $order->target_id) }}
                                    </td>

                                    <td class="px-6 py-1.5  text-sm text-gray-900">
                                   {{ date('d.m.Y  H:i:s',strtotime($order->date_payed)) ?? "Пусто" }}
                                    </td>

                                    <td class="px-6 py-1.5 whitespace-nowrap text-sm text-gray-900">
                                    {{ $order->total  }} {{ $order->currency }}
                                    </td>                                
                                </tr>
                                @endif
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="mt-4 flex flex-row justify-between">
                        <select wire:model="paginationCount">
                            <option value="10">10</option>
                            <option value="50">50</option>
                            <option value="100">100</option>
                        </select>
                        <div class="mt-4">
                        {{ $orders->appends(request()->query())->links() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

