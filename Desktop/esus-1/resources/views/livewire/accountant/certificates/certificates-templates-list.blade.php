<div>
    <x-slot name="header">
        <div class="bg-white mx-auto py-6 px-4 sm:px-6 lg:px-8">
            <div class="flex justify-between items-center w-full">
                <h2 class="font-semibold text-xl text-gray-800 leading-tight">
                    {{ __('admin.certificates.template.index.title') }}
                </h2>

                @include('admin.common.secondmenu', [
                    'tabname'=>'courses',
                    'popup'=>false,
                    'addlink'=>'accountant.certificates-templates.create'
                ])
            </div>
        </div>
    </x-slot>

    <div class="py-6 px-4 sm:px-6 lg:px-8">
        <div class="flex flex-col">
            <div class="-my-2 overflow-x-auto sm:-mx-6 lg:-mx-8">
                <div class="py-2 align-middle inline-block min-w-full sm:px-6 lg:px-8">
                    <div class="shadow overflow-hidden border-b border-gray-200 sm:rounded">
                        <table class="min-w-full divide-y divide-gray-200 w-full">
                            <thead>
                            <tr>
                                <th scope="col"
                                    class="px-2 py-3 bg-gray-50 text-left text-sm font-medium text-gray-800">
                                    <div class="text-center">{{ __('admin.table.head.id') }}</div>
                                </th>
                                <th scope="col"
                                    class="px-2 py-3 bg-gray-50 text-left text-sm font-medium text-gray-800">
                                    <div class="pl-4">{{ __('admin.table.head.title') }}</div>
                                </th>
                                <th scope="col"
                                    class="px-2 py-3 bg-gray-50 text-left text-sm font-medium text-gray-800">
                                    <div class="pl-4 border-l border-gray-200">{{ __('admin.table.head.program') }}</div>
                                </th>
                                <th scope="col" width="200"
                                    class="px-2 py-3 bg-gray-50 text-left text-sm font-medium text-gray-800">
                                    <div class="pl-4 border-l border-gray-200">{{ __('admin.table.head.action') }}</div>
                                </th>
                            </tr>
                            </thead>
                            <tbody class="bg-white divide-y divide-gray-200">
                            @foreach ($templates as $item)
                                <tr>
                                    <td class="px-6 py-1.5 text-sm text-gray-900">
                                        {{ $item->id }}
                                    </td>
                                    <td class="px-6 py-1.5 text-sm text-gray-900">
                                        {{ $item->title }}
                                    </td>
                                    <td class="px-6 py-1.5 text-sm text-gray-900">

                                        {{ ($item->courseItem) ? $item->courseItem->ru_title : '' }}
                                    </td>
                                    <td class="px-6 py-1.5 whitespace-nowrap text-sm font-medium space-x-2">
                                        <a href="{{ route('accountant.certificates-templates.edit', $item->id) }}"
                                           class="text-blue-500 hover:text-blue-900 m-0 font-normal">{{ __('admin.links.edit') }}</a>
                                        <button wire:click="runtest({{ $item->id }})" class="text-blue-500 hover:text-blue-900 m-0 font-normal">Скачать пример</button>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="mt-4">
                        {{ $templates->appends(request()->query())->links('admin.common.pagination') }}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
