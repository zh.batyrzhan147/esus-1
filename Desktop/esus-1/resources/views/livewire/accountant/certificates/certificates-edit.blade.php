<div class="bg-white z-0">
    <div class="py-8">
        @if($certificate)
            <div class="flex w-[896px] mx-auto flex-col p-6 rounded-xl relative mb-6" style="background: linear-gradient(251.32deg, #E349C4 23.36%, #7748EF 83.76%);">
                @if($certificate->type == \App\Models\Certificates::CERTIFICATES_TYPE_CERTIFICATE)
                    <div class="text-lg text-white mb-8">{{ __('certificate.verify') }}</div>
                @elseif($certificate->type == \App\Models\Certificates::CERTIFICATES_TYPE_REFERENCE)
                    <div class="text-lg text-white mb-8">{{ __('reference.verify') }}</div>
                @endif
                <div class="text-white opacity-50 mb-8">№<span class="uppercase">{{ $certificate->uuid }}</span> от <span>{{ date('d', strtotime($certificate->created_at)) }}</span> <span class="lowercase">{{ __(date('F', strtotime($certificate->created_at))) }}</span> <span>{{ __(date('Y', strtotime($certificate->created_at))) }} года</span></div>
                <div class="text-5xl text-white font-bold mb-8">{{ $certificate->fio }}</div>
                <div class="text-2xl text-white mb-8">{{ $certificate->course_title }}</div>
                <div class="flex space-x-2">
                    <a href="{{ $certificate->pdf }}" class="text-sm rounded border border-white py-1 px-3 text-white w-min whitespace-nowrap">{{ __('certificate.verify.download.org') }}</a>
                    @if($certificate->type == \App\Models\Certificates::CERTIFICATES_TYPE_CERTIFICATE)
                    <a href="{{ $certificate->getPdfCopyUrl() }}" class="text-sm rounded border border-white py-1 px-3 text-white w-min whitespace-nowrap">{{ __('certificate.verify.download') }}</a>
                    @endif
                </div>
            </div>
            <div class="h-px bg-gray-200 mb-4 w-[880px] mx-auto mb-6"></div>
            <h2 class="w-[896px] mx-auto text-xl text-[#18181B] px-4 mb-4">{{ __('certificate.verify.more.title') }}</h2>
            <div class="flex w-[896px] mx-auto flex-col pt-4 relative bg-gray-50">
                <div class="w-full grid bg-gray-50 py-2.5 px-5 grid-cols-3 border-b-2 border-gray-300">
                    <div>{{ __('certificate.verify.more.course.title') }}</div>
                    <div class="col-span-2">{{ $certificate->course_title }}</div>
                </div>
                <div class="w-full grid bg-gray-50 py-2.5 px-5 grid-cols-3 border-b-2 border-gray-300">
                    <div>{{ __('certificate.verify.more.course.date') }}</div>
                    <div class="col-span-2">от <span>{{ date('d', strtotime($certificate->created_at)) }}</span> <span class="lowercase">{{ __(date('F', strtotime($certificate->created_at))) }}</span> <span>{{ __(date('Y', strtotime($certificate->created_at))) }} года</div>
                </div>
                <div class="w-full grid bg-gray-50 py-2.5 px-5 grid-cols-3 border-b-2 border-gray-300">
                    <div>{{ __('certificate.verify.more.course.number') }}</div>
                    <div class="col-span-2">№<span class="uppercase">{{ $certificate->uuid }}</div>
                </div>
                <div class="w-full grid bg-gray-50 py-2.5 px-5 grid-cols-3 border-b-2 border-gray-300">
                    <div>{{ __('certificate.verify.more.course.trainer') }}</div>
                    <div class="col-span-2">{{ $certificate->fio }}</div>
                </div>
            </div>
        @else
            <div class="flex justify-center items-center w-[500px] h-[300px] mx-auto flex-col p-6 rounded-xl relative bg-red-200">
                <div class="text-center">
                    <div class="text-2xl text-center">{!! __('certificate.notverify') !!}</div>
                    <div class="text-base text-center mb-8">{!! __('certificate.notverify2') !!}</div>
                    <a href="{{ route('verify-certificate-index') }}" class="mx-auto text-sm rounded border border-black py-1 px-3 text-black w-min whitespace-nowrap">{{ __('certificate.notverify.button') }}</a>
                </div>
            </div>
        @endif
    </div>
</div>
=
