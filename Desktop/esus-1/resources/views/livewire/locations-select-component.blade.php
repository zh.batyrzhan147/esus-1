<div class="flex w-full flex-col space-y-4">
    <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-center">
        <div class="w-full md:w-1/4">
            <label for="type" class="w-full text-left md:text-right block text-sm">{{ __('admin.locations.create.form.type') }}</label>
        </div>
        <div class="w-full md:w-3/4">
            <select wire:change="change($event.target.value)" name="type" id="type"
                    class="w-full md:w-3/4 px-3 py-1 border rounded focus:border-blue-500 hover:border-blue-500 text-sm outline-none focus:input-shadow">
                @foreach(\App\Models\Locations::listTypes() as $key=>$item)
                    <option value="{{$key}}" @if($type == $key) selected @endif >{{ $item }}</option>
                @endforeach
            </select>
            @error('type')
            <p class="text-xs text-red-600">{{ $message }}</p>
            @enderror
        </div>
    </div>
        @if($showparentList)
            <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-center">
                <div class="w-full md:w-1/4">
                    <label for="parent" class="w-full text-left md:text-right block text-sm">{{ __('admin.locations.create.form.type.obl') }}</label>
                </div>
                <div class="w-full md:w-3/4">
                    <select name="parent" id="parent"
                            class="w-full md:w-3/4 px-3 py-1 border rounded focus:border-blue-500 hover:border-blue-500 text-sm outline-none focus:input-shadow">
                            <option value="" @if( ($this->parent1->id ?? '') == null) selected @endif>{{ __('admin.locations.create.form.type.withoutobl') }}</option>
                        @foreach($parentList as $parent)
                            <option value="{{ $parent->id }}" @if( ($this->parent1->id ?? '') == $parent->id) selected @endif>{{ $parent->kz_title }}</option>
                        @endforeach
                    </select>
                    @error('type')
                    <p class="text-xs text-red-600">{{ $message }}</p>
                    @enderror
                </div>
            </div>
        @endif

        @if($showparentListobl)
            <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-center">
                <div class="w-full md:w-1/4">
                    <label for="parentobl" class="w-full text-left md:text-right block text-sm">{{ __('admin.locations.create.form.type.obl') }}</label>
                </div>
                <div class="w-full md:w-3/4">
                    <select wire:change="changeobl($event.target.value)" id="parentobl"
                            class="w-full md:w-3/4 px-3 py-1 border rounded focus:border-blue-500 hover:border-blue-500 text-sm outline-none focus:input-shadow">
                        <option value="">{{ __('admin.locations.create.form.type.chooseobl') }}</option>
                        @foreach($parentList as $parent)
                            <option value="{{ $parent->id }}" @if( ($this->parent2->id ?? '') == $parent->id) selected @endif>{{ $parent->kz_title }}</option>
                        @endforeach
                    </select>
                    @error('type')
                    <p class="text-xs text-red-600">{{ $message }}</p>
                    @enderror
                </div>
            </div>
        @endif

        @if($showparentList2)
            <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-center">
                <div class="w-full md:w-1/4">
                    <label for="parent" class="w-full text-left md:text-right block text-sm">{{ __('admin.locations.create.form.type.rai') }}</label>
                </div>
                <div class="w-full md:w-3/4">
                    <select name="parent" id="parent"
                            class="w-full md:w-3/4 px-3 py-1 border rounded focus:border-blue-500 hover:border-blue-500 text-sm outline-none focus:input-shadow">
                        <option value="">{{ __('admin.locations.create.form.type.chooserai') }}</option>
                        @foreach($parentList2 as $parent)
                            <option value="{{ $parent->id }}" @if( ($this->parent1->id ?? '') == $parent->id) selected @endif>{{ $parent->kz_title }}</option>
                        @endforeach
                    </select>
                    @error('type')
                    <p class="text-xs text-red-600">{{ $message }}</p>
                    @enderror
                </div>
            </div>
        @endif
{{--    @else--}}
{{--        <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-center">--}}
{{--            <div class="w-full md:w-1/4">--}}
{{--                <label for="parent" class="w-full text-left md:text-right block text-sm">Parent:</label>--}}
{{--            </div>--}}
{{--            <div class="w-full md:w-3/4">--}}
{{--                <select name="parent" id="parent"--}}
{{--                        class="w-full md:w-3/4 px-3 py-1 border rounded focus:border-blue-500 hover:border-blue-500 text-sm outline-none focus:input-shadow">--}}
{{--                    @foreach($parentList2 as $parent)--}}
{{--                        <option value="{{ $parent->id }}">{{ $parent->kz_title }} </option>--}}
{{--                    @endforeach--}}
{{--                </select>--}}
{{--            <!-- <input type="text" name="type" id="type" class="w-full md:w-3/4 px-3 py-1 border rounded focus:border-blue-500 hover:border-blue-500 text-sm outline-none focus:input-shadow" placeholder="Введите название Должность на русском языке" value="{{ old('type', $location->type) }}" /> -->--}}
{{--                @error('type')--}}
{{--                <p class="text-xs text-red-600">{{ $message }}</p>--}}
{{--                @enderror--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    @endif--}}
</div>


