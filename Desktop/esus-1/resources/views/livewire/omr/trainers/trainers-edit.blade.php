<div xmlns:wire="http://www.w3.org/1999/xhtml">
    <x-slot name="header">
        <div class="bg-white mx-auto py-6 px-4 sm:px-6 lg:px-8">
            <h2 class="font-semibold text-xl text-gray-800 leading-tight">
                {{ __('admin.trainers.edit.title') }}
            </h2>
        </div>
    </x-slot>
    @include('admin.common.secondmenu', [
        'tabname'=>'groups',
        'popup'=>true,
    ])
    @if (session()->has('user_for_trainer_found'))
        <p class="px-4 sm:px-6 lg:px-8 text-sm"> {!! session()->get('user_for_trainer_found') !!}</p>
    @endif



    <div class="py-6 px-4 sm:px-6 lg:px-8 h-full">
        <div class="flex flex-col items-start md:items-center w-full md:w-3/4 mx-auto space-y-4">
            <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-center">
                <div class="w-full md:w-1/4">
                    <label for="iin" class="w-full text-left md:text-right block text-sm">{{ __('admin.trainers.create.iin') }}</label>
                </div>
                <div class="w-full md:w-3/4">
                    <input wire:model="trainer.iin" type="text" id="iin" placeholder="{{ __('admin.trainers.create.iin.placeholder') }}"
                           class="w-full md:w-3/4 px-3 py-1 border rounded focus:border-blue-500 hover:border-blue-500 text-sm outline-none focus:input-shadow"/>
                    @if($new)
                        <button wire:click="search" class="@if (session()->has('user_not_found')) bg-red-600 @endif">
                            {{ __('admin.buttons.search') }}</button>
                    @endif
                    @error('trainer.iin')
                    <p class="text-xs text-red-600">{{ $message }}</p>
                    @enderror
                </div>
            </div>

            <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-center">
                <div class="w-full md:w-1/4">
                    <label for="last_name" class="w-full text-left md:text-right block text-sm">{{ __('admin.trainers.create.last_name') }}</label>
                </div>
                <div class="w-full md:w-3/4">
                    <input wire:model="trainer.last_name" type="text" id="last_name" placeholder="{{ __('admin.trainers.create.last_name.placeholder') }}"
                           class="w-full md:w-3/4 px-3 py-1 border rounded focus:border-blue-500 hover:border-blue-500 text-sm outline-none focus:input-shadow"/>
                    @error('trainer.last_name')
                    <p class="text-xs text-red-600">{{ $message }}</p>
                    @enderror
                </div>
            </div>
            <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-center">
                <div class="w-full md:w-1/4">
                    <label for="last_name" class="w-full text-left md:text-right block text-sm">{{ __('admin.trainers.edit.last_name.lat') }}</label>
                </div>
                <div class="w-full md:w-3/4">
                    <input wire:model="trainer.last_name_lat" type="text" id="last_name_lat" placeholder="{{ __('admin.trainers.edit.last_name.lat.placeholder') }}"
                           class="w-full md:w-3/4 px-3 py-1 border rounded focus:border-blue-500 hover:border-blue-500 text-sm outline-none focus:input-shadow"/>
                    @error('trainer.last_name_lat')
                    <p class="text-xs text-red-600">{{ $message }}</p>
                    @enderror
                </div>
            </div>


            <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-center">
                <div class="w-full md:w-1/4">
                    <label for="name" class="w-full text-left md:text-right block text-sm">{{ __('admin.trainers.create.name') }}</label>
                </div>
                <div class="w-full md:w-3/4">
                    <input wire:model="trainer.name" type="text" id="name" placeholder="{{ __('admin.trainers.create.name.placeholder') }}"
                           class="w-full md:w-3/4 px-3 py-1 border rounded focus:border-blue-500 hover:border-blue-500 text-sm outline-none focus:input-shadow"/>
                    @error('trainer.name')
                    <p class="text-xs text-red-600">{{ $message }}</p>
                    @enderror
                </div>
            </div>

            <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-center">
                <div class="w-full md:w-1/4">
                    <label for="name" class="w-full text-left md:text-right block text-sm">{{ __('admin.trainers.edit.name.lat') }}</label>
                </div>
                <div class="w-full md:w-3/4">
                    <input wire:model="trainer.name_lat" type="text" id="name_lat" placeholder="{{ __('admin.trainers.edit.name.lat.placeholder') }}"
                           class="w-full md:w-3/4 px-3 py-1 border rounded focus:border-blue-500 hover:border-blue-500 text-sm outline-none focus:input-shadow"/>
                    @error('trainer.name_lat')
                    <p class="text-xs text-red-600">{{ $message }}</p>
                    @enderror
                </div>
            </div>

            <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-center">
                <div class="w-full md:w-1/4">
                    <label for="patronymic" class="w-full text-left md:text-right block text-sm">{{ __('admin.trainers.create.patronymic') }}</label>
                </div>
                <div class="w-full md:w-3/4">
                    <input wire:model="trainer.patronymic" type="text" id="patronymic" placeholder="{{ __('admin.trainers.create.patronymic.placeholder') }}"
                           class="w-full md:w-3/4 px-3 py-1 border rounded focus:border-blue-500 hover:border-blue-500 text-sm outline-none focus:input-shadow"/>
                    @error('trainer.patronymic')
                    <p class="text-xs text-red-600">{{ $message }}</p>
                    @enderror
                </div>
            </div>

            <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-center">
                <div class="w-full md:w-1/4">
                    <label for="email" class="w-full text-left md:text-right block text-sm">{{ __('admin.trainers.create.email') }}</label>
                </div>
                <div class="w-full md:w-3/4">
                    <input wire:model="trainer.email" type="email" id="email" placeholder="{{ __('admin.trainers.create.email.placeholder') }}"
                           class="w-full md:w-3/4 px-3 py-1 border rounded focus:border-blue-500 hover:border-blue-500 text-sm outline-none focus:input-shadow"/>
                    @error('trainer.email')
                    <p class="text-xs text-red-600">{{ $message }}</p>
                    @enderror
                </div>
            </div>

            <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-center">
                <div class="w-full md:w-1/4">
                    <label for="phone" class="w-full text-left md:text-right block text-sm">{{ __('admin.trainers.create.phone') }}</label>
                </div>
                <div class="w-full md:w-3/4">
                    <input wire:model="trainer.phone" type="text" id="phone" placeholder="{{ __('admin.trainers.create.phone.placeholder') }}"
                           class="w-full md:w-3/4 px-3 py-1 border rounded focus:border-blue-500 hover:border-blue-500 text-sm outline-none focus:input-shadow"/>
                    @error('trainer.phone')
                    <p class="text-xs text-red-600">{{ $message }}</p>
                    @enderror
                </div>
            </div>

            <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-center">
                <div class="w-full md:w-1/4">
                    <label for="gender" class="w-full text-left md:text-right block text-sm">{{ __('admin.trainers.create.sex') }}</label>
                </div>
                <div class="w-full md:w-3/4">
                    <select wire:model="trainer.gender"
                            class="w-full md:w-3/4 px-3 py-1 border rounded focus:border-blue-500 hover:border-blue-500 text-sm outline-none focus:input-shadow">
                        <option value="" >{{ __('admin.trainers.create.choosesex') }}</option>
                        @foreach(\App\Models\Genders::all() ?? []  as $item)
                            <option value="{{ $item->id }}">{{ $item->title }}</option>
                        @endforeach
                    </select>
                    @error('trainer.gender')
                    <p class="text-xs text-red-600">{{ $message }}</p>
                    @enderror
                </div>
            </div>

            <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-center">
                <div class="w-full md:w-1/4">
                    <label for="nationality" class="w-full text-left md:text-right block text-sm">{{ __('admin.trainers.create.nationality') }}</label>
                </div>
                <div class="w-full md:w-3/4">
                    <select wire:model="trainer.nationality"
                            class="w-full md:w-3/4 px-3 py-1 border rounded focus:border-blue-500 hover:border-blue-500 text-sm outline-none focus:input-shadow">
                        <option value="" >{{ __('admin.trainers.create.choosenationality') }}</option>
                        @foreach(\App\Models\Nationalities::all() ?? []  as $item)
                            <option value="{{ $item->id }}">{{ $item->ru_title }}</option>
                        @endforeach
                    </select>
                    @error('trainer.nationality')
                    <p class="text-xs text-red-600">{{ $message }}</p>
                    @enderror
                </div>
            </div>

            <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-center">
                <div class="w-full md:w-1/4">
                    <label for="affiliate" class="w-full text-left md:text-right block text-sm">{{ __('admin.trainers.create.affiliate') }}</label>
                </div>
                <div class="w-full md:w-3/4">
                    <select wire:model="trainer.affiliate" id="affiliate"
                            class="w-full md:w-3/4 px-3 py-1 border rounded focus:border-blue-500 hover:border-blue-500 text-sm outline-none focus:input-shadow">
                        <option value="null">{{ __('admin.trainers.create.chooseaffiliate') }}</option>
                        @foreach(\App\Models\Affiliates::all() as $key=>$item)
                            <option value="{{$item->id}}">{{ $item->title }}</option>
                        @endforeach
                    </select>
                    @error('trainer.affiliate')
                    <p class="text-xs text-red-600">{{ $message }}</p>
                    @enderror
                </div>
            </div>


            <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-center">
                <div class="w-full md:w-1/4">
                    <label for="languages" class="w-full text-left md:text-right block text-sm">{{ __('admin.trainers.create.lang') }}</label>
                </div>
                <div class="w-full md:w-3/4" x-data x-init="langs = new SlimSelect({select: $refs.langs, placeholder:'Select langs'});" wire:ignore>
                    <select x-on:change="$wire.set('languages', langs.selected())" id="langs" x-ref="langs" multiple class="w-full md:w-3/4 px-3 py-1 border rounded focus:border-blue-500 hover:border-blue-500 text-sm outline-none focus:input-shadow">
                        @foreach(\App\Models\Languages::all()->pluck('title', 'id')->toArray() as $id => $lang)
                            <option value="{{ $id }}" @if(in_array($id, $languages)) selected @endif>
                                {{ $lang }}
                            </option>
                        @endforeach
                    </select>
                    @error('languages')
                    <p class="text-xs text-red-600">{{ $message }}</p>
                    @enderror
                </div>
            </div>
            <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-center">
                <div class="w-full md:w-1/4">
                    <label for="auto_generation" class="w-full text-left md:text-right block text-sm">{{ __('admin.trainers.create.generate') }}</label>
                </div>
                <div class="w-full md:w-3/4">
                    <select wire:model.defer="trainer.auto_generation" name="auto_generation" id="auto_generation"
                            class="w-full md:w-3/4 px-3 py-1 border rounded focus:border-blue-500 hover:border-blue-500 text-sm outline-none focus:input-shadow">
                        <option value="1" @if( $trainer->auto_generation == true) selected @endif>{{ __('admin.trainers.create.generate.true') }}</option>
                        <option value="0" @if( $trainer->auto_generation == false) selected @endif>{{ __('admin.trainers.create.generate.false') }}</option>
                    </select>
                    @error('trainer.auto_generation')
                    <p class="text-xs text-red-600">{{ $message }}</p>
                    @enderror
                </div>
            </div>

            <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-center">
                <div class="w-full md:w-1/4">
                    <label for="oblast" class="w-full text-left md:text-right block text-sm">{{ __('admin.trainers.create.obl') }}</label>
                </div>
                <div class="w-full md:w-3/4">
                    <select wire:model="oblast"
                            class="w-full md:w-3/4 px-3 py-1 border rounded focus:border-blue-500 hover:border-blue-500 text-sm outline-none focus:input-shadow">
                        <option value="">{{ __('admin.trainers.create.chooseobl') }}</option>
                        @foreach(\App\Models\Locations::where('type', \App\Models\Locations::LOCATION_OBLAST)->get() ?? []  as $item)
                            <option value="{{ $item->id }}">{{ $item->ru_title }}</option>
                        @endforeach
                    </select>
                    @error('oblast')
                    <p class="text-xs text-red-600">{{ $message }}</p>
                    @enderror
                </div>
            </div>

            @if($oblast)
                <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-center">
                    <div class="w-full md:w-1/4">
                        <label for="raion" class="w-full text-left md:text-right block text-sm">{{ __('admin.trainers.create.rai') }}</label>
                    </div>
                    <div class="w-full md:w-3/4">
                        <select wire:model="raion"
                                class="w-full md:w-3/4 px-3 py-1 border rounded focus:border-blue-500 hover:border-blue-500 text-sm outline-none focus:input-shadow">
                            <option value="">{{ __('admin.trainers.create.chooserai') }}</option>
                            @foreach(\App\Models\Locations::where('type', \App\Models\Locations::LOCATION_RAYON)->where('parent',$oblast)->get() ?? []  as $item)
                                <option value="{{ $item->id }}">{{ $item->ru_title }}</option>
                            @endforeach
                        </select>
                        @error('raion')
                        <p class="text-xs text-red-600">{{ $message }}</p>
                        @enderror
                    </div>
                </div>
                @if($raion)
                    <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-center">
                        <div class="w-full md:w-1/4">
                            <label for="gorod" class="w-full text-left md:text-right block text-sm">{{ __('admin.trainers.create.city') }}</label>
                        </div>
                        <div class="w-full md:w-3/4">
                            <select wire:model="gorod"
                                    class="w-full md:w-3/4 px-3 py-1 border rounded focus:border-blue-500 hover:border-blue-500 text-sm outline-none focus:input-shadow">
                                <option value="">{{ __('admin.trainers.create.choosecity') }}</option>
                                @foreach(\App\Models\Locations::where('type', \App\Models\Locations::LOCATION_AUL)->where('parent',$raion)->get() ?? []  as $item)
                                    <option value="{{ $item->id }}">{{ $item->ru_title }}</option>
                                @endforeach
                            </select>
                            @error('gorod')
                            <p class="text-xs text-red-600">{{ $message }}</p>
                            @enderror
                        </div>
                    </div>
                    <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-center">
                        <div class="w-full md:w-1/4">
                            <label for="institute" class="w-full text-left md:text-right block text-sm">{{ __('admin.trainers.create.job') }}</label>
                        </div>
                        <div class="w-full md:w-3/4" {{-- x-data x-init="institute = new SlimSelect({allowDeselect: true, select: $refs.institute, placeholder:'Select institute'});" wire:ignore --}} >
                            <select wire:model="trainer.institute"  {{--x-on:change="$wire.set('user.institute', institute.selected())" id="institute" x-ref="institute" --}}
                            class="w-full md:w-3/4 px-3 py-1 border rounded focus:border-blue-500 hover:border-blue-500 text-sm outline-none focus:input-shadow">
                                <option value="">{{ __('admin.trainers.create.choosejob') }}</option>
                                @foreach(\App\Models\Institutes::where('oblast', $oblast)->where('rayon',$raion)->get() ?? []  as $item)
                                    <option value="{{ $item->id }}">{{ $item->title }}</option>
                                @endforeach
                            </select>
                            @error('trainer.institute')
                            <p class="text-xs text-red-600">{{ $message }}</p>
                            @enderror
                        </div>
                    </div>
                @endif
            @endif
            @if(!$new)
            <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-start">
                <div class="w-full md:w-1/4">
                    <label class="w-full text-left md:text-right block text-sm">{{ __('admin.trainers.create.program') }}</label>
                </div>
                <div class="flex flex-col w-full md:w-3/4">
                    <button wire:click="add_courses_modal_open" class="mb-2 text-sm flex w-48 justify-center items-center space-x-1.5 px-4 py-1 border-0 bg-blue-500 text-white rounded hover:bg-blue-600">{{ __('admin.buttons.create.program') }}</button>

                    <div class=" flex flex-col w-full md:w-3/4">
                        @foreach($trainer->courses as $item)
                            <div class="flex justify-between">
                                - {{ $item->ru_title }}
                                <button wire:click="$emit('triggerDelete',{{ $item->id }})"
                                        class="text-red-500 hover:text-red-900 m-0 font-normal"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512" class="w-4 h-5 text-red-500 hover:text-red-600"><path d="M135.2 17.69C140.6 6.848 151.7 0 163.8 0H284.2C296.3 0 307.4 6.848 312.8 17.69L320 32H416C433.7 32 448 46.33 448 64C448 81.67 433.7 96 416 96H32C14.33 96 0 81.67 0 64C0 46.33 14.33 32 32 32H128L135.2 17.69zM394.8 466.1C393.2 492.3 372.3 512 346.9 512H101.1C75.75 512 54.77 492.3 53.19 466.1L31.1 128H416L394.8 466.1z" fill="currentColor"/></svg></button>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
            @endif

            <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-center">
                <div class="w-full md:w-1/4">
                    <label for="academic_category" class="w-full text-left md:text-right block text-sm">{{ __('admin.trainers.create.category') }}</label>
                </div>
                <div class="w-full md:w-3/4">
                    <select wire:model="trainer.academic_category"
                            class="w-full md:w-3/4 px-3 py-1 border rounded focus:border-blue-500 hover:border-blue-500 text-sm outline-none focus:input-shadow">
                        <option value="">{{ __('admin.trainers.create.choosecategory') }}</option>
                        @foreach(\App\Models\AcademicCategories::all() ?? []  as $item)
                            <option value="{{ $item->id }}">{{ $item->ru_title }}</option>
                        @endforeach
                    </select>
                    @error('trainer.academic_category')
                    <p class="text-xs text-red-600">{{ $message }}</p>
                    @enderror
                </div>
            </div>
            <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-center">
                <div class="w-full md:w-1/4">
                    <label for="education" class="w-full text-left md:text-right block text-sm">{{ __('admin.trainers.create.level') }}</label>
                </div>
                <div class="w-full md:w-3/4">
                    <select wire:model="trainer.education"
                            class="w-full md:w-3/4 px-3 py-1 border rounded focus:border-blue-500 hover:border-blue-500 text-sm outline-none focus:input-shadow">
                        <option value="">{{ __('admin.trainers.create.chooselevel') }}</option>
                        @foreach(\App\Models\Educations::all() ?? []  as $item)
                            <option value="{{ $item->id }}">{{ $item->ru_title }}</option>
                        @endforeach
                    </select>
                    @error('trainer.education')
                    <p class="text-xs text-red-600">{{ $message }}</p>
                    @enderror
                </div>
            </div>
            <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-center">
                <div class="w-full md:w-1/4">
                    <label for="academic_degree" class="w-full text-left md:text-right block text-sm">{{ __('admin.trainers.create.academic') }}</label>
                </div>
                <div class="w-full md:w-3/4">
                    <select wire:model="trainer.academic_degree"
                            class="w-full md:w-3/4 px-3 py-1 border rounded focus:border-blue-500 hover:border-blue-500 text-sm outline-none focus:input-shadow">
                        <option value="">{{ __('admin.trainers.create.chooseacademic') }}</option>
                        @foreach(\App\Models\AcademicDegrees::all() ?? []  as $item)
                            <option value="{{ $item->id }}">{{ $item->ru_title }}</option>
                        @endforeach
                    </select>
                    @error('trainer.academic_degree')
                    <p class="text-xs text-red-600">{{ $message }}</p>
                    @enderror
                </div>
            </div>

            <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-center">
                <div class="w-full md:w-1/4">
                    <label for="subject_teach" class="w-full text-left md:text-right block text-sm">{{ __('admin.users.edit.lesson') }}</label>
                </div>
                <div class="w-full md:w-3/4">
                    <select wire:model="trainer.teach_subject_id" id="teach_subject_id"
                            class="w-full md:w-3/4 px-3 py-1 border rounded focus:border-blue-500 hover:border-blue-500 text-sm outline-none focus:input-shadow">
                        <option value="">{{ __('admin.users.edit.lesson.placeholder') }}</option>
                        @foreach(\App\Models\TeachSubjects::all() ?? []  as $item)
                            <option value="{{ $item->id }}">{{ $item->ru_title }}</option>
                        @endforeach
                    </select>
                    @error('trainer.teach_subject_id')
                    <p class="text-xs text-red-600">{{ $message }}</p>
                    @enderror
                </div>
            </div>

{{--            <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-center">--}}
{{--                <div class="w-full md:w-1/4">--}}
{{--                    <label for="subject_teach" class="w-full text-left md:text-right block text-sm">{{ __('admin.trainers.create.lesson') }}</label>--}}
{{--                </div>--}}
{{--                <div class="w-full md:w-3/4">--}}
{{--                    <input wire:model="trainer.subject_teach" type="text" id="subject_teach" placeholder="{{ __('admin.trainers.create.lesson.placeholder') }}"--}}
{{--                           class="w-full md:w-3/4 px-3 py-1 border rounded focus:border-blue-500 hover:border-blue-500 text-sm outline-none focus:input-shadow"/>--}}
{{--                    @error('trainer.subject_teach')--}}
{{--                    <p class="text-xs text-red-600">{{ $message }}</p>--}}
{{--                    @enderror--}}
{{--                </div>--}}
{{--            </div>--}}

                    <!-- stazh -->

            <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-center">
                <div class="w-full md:w-1/4">
                    <label for="stazh" class="w-full text-left md:text-right block text-sm">{{ __('admin.trainers.create.staj') }}</label>
                </div>
                <div class="w-full md:w-3/4">
                    <input wire:model="trainer.stazh" type="text" id="stazh" placeholder="{{ __('admin.trainers.create.staj.placeholder') }}" class="w-full md:w-3/4 px-3 py-1 border rounded focus:border-blue-500 hover:border-blue-500 text-sm outline-none focus:input-shadow" />
                    @error('trainer.stazh')
                    <p class="text-xs text-red-600">{{ $message }}</p>
                    @enderror
                </div>
            </div>

            <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-center">
                <div class="w-full md:w-1/4">
                    <label for="iban" class="w-full text-left md:text-right block text-sm">{{ __('admin.trainers.create.iban') }}</label>
                </div>
                <div class="w-full md:w-3/4">
                    <input wire:model="trainer.iban" type="text" id="iban" placeholder="{{ __('admin.trainers.create.iban.placeholder') }}"
                           class="w-full md:w-3/4 px-3 py-1 border rounded focus:border-blue-500 hover:border-blue-500 text-sm outline-none focus:input-shadow"/>
                    @error('trainer.iban')
                    <p class="text-xs text-red-600">{{ $message }}</p>
                    @enderror
                </div>
            </div>

            <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-center">
                <div class="w-full md:w-1/4">
                    <label for="bank_name" class="w-full text-left md:text-right block text-sm">{{ __('admin.trainers.create.bank') }}</label>
                </div>
                <div class="w-full md:w-3/4">
                    <input wire:model="trainer.bank_name" type="text" id="bank_name" placeholder="{{ __('admin.trainers.create.bank.placeholder') }}"
                           class="w-full md:w-3/4 px-3 py-1 border rounded focus:border-blue-500 hover:border-blue-500 text-sm outline-none focus:input-shadow"/>
                    @error('trainer.bank_name')
                    <p class="text-xs text-red-600">{{ $message }}</p>
                    @enderror
                </div>
            </div>

            <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-center">
                <div class="w-full md:w-1/4">
                    <label for="attracted" class="w-full text-left md:text-right block text-sm">{{ __('admin.trainers.create.attracted') }}</label>
                </div>
                <div class="w-full md:w-3/4">
                    <select wire:model.defer="trainer.attractedcat" name="attracted" id="attracted"
                            class="w-full md:w-3/4 px-3 py-1 border rounded focus:border-blue-500 hover:border-blue-500 text-sm outline-none focus:input-shadow">
                        <option value="">Без категории</option>
                        @foreach(\App\Models\AttractedCats::all() ?? []  as $item)
                            <option value="{{ $item->id }}">{{ $item->ru_title }}</option>
                        @endforeach
                    </select>
                    @error('trainer.auto_generation')
                    <p class="text-xs text-red-600">{{ $message }}</p>
                    @enderror
                </div>
            </div>
            <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-center" >
                <div class="w-full md:w-1/4">
                    <label class="w-full text-left md:text-right block text-sm" for="trainer_awards">{{ __('Награды') }}</label>
                </div>
                <div class="w-full md:w-3/4">
                    <x-inputs.trix id="trainer_awards" :value="$trainer_awards" />
                </div>
            </div>
            <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-center" >
                <div class="w-full md:w-1/4">
                    <label class="w-full text-left md:text-right block text-sm" for="trainer_theses">{{ __('Опубликованные статьи') }}</label>
                </div>
                <div class="w-full md:w-3/4">
                    <x-inputs.trix id="trainer_theses" :value="$trainer_theses" />
                </div>
            </div>
            <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-center" >
                <div class="w-full md:w-1/4">
                    <label class="w-full text-left md:text-right block text-sm" for="trainer_certificates">{{ __('Пройденные курсы') }}</label>
                </div>
                <div class="w-full md:w-3/4">
                    <x-inputs.trix id="trainer_certificates" :value="$trainer_certificates" />
                </div>
            </div>

            <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-center">
                <div class="w-full md:w-1/4">
                </div>
                <div class="w-full md:w-3/4 space-x-2 flex">
                    <button wire:click="save"
                            class="mb-2 text-sm flex justify-center items-center px-4 py-1 border-0 bg-blue-500 text-white rounded hover:bg-blue-600">
                        {{ __('admin.buttons.save') }}
                    </button>
                    @if(!$new)
                    <button wire:click="delete"
                            class="mb-2 text-sm flex justify-center items-center px-4 py-1 border-0 bg-red-500 text-white rounded hover:bg-red-600">
                        {{ __('admin.links.delete') }}
                    </button>
                    @endif
                    <a href="{{ route('omr.trainers.index') }}"
                       class="py-1 px-4 border inline-block m-0 mb-2 text-sm rounded hover:border-black">{{ __('admin.buttons.cancel') }}</a>
                </div>
            </div>
        </div>
    </div>

    @if($showModalAddCourses)
        @include('livewire.trainers.courses_add')
    @endif

</div>

@push('styles')
    <link rel="stylesheet" href="https://unpkg.com/trix@2.0.0-alpha.1/dist/trix.css"/>
    <script src="https://unpkg.com/trix@2.0.0-alpha.1/dist/trix.umd.js"></script>

{{--    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/flatpickr/dist/flatpickr.min.css">--}}
{{--    <script src="https://cdn.jsdelivr.net/npm/flatpickr"></script>--}}
{{--    <script src="https://cdn.jsdelivr.net/npm/flatpickr/dist/l10n/ru.js"></script>--}}
    <link href="https://unpkg.com/slim-select@1.27.0/dist/slimselect.min.css" rel="stylesheet"/>

@endpush

@push('scripts')
    <script src="https://unpkg.com/slim-select@1.27.0/dist/slimselect.min.js"></script>
@endpush


@push('scripts')
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>

    <script type="text/javascript">
        document.addEventListener('DOMContentLoaded', function () {
        @this.on('triggerSuccessCreate', () => {
            Swal.fire({
                title: '{{ __("admin.swal.lang.title.success") }}',
                icon: 'success',
                width: 600,
                position: 'top-end',
                showConfirmButton: false,
                timer: 1000
            });
        });

        @this.on('triggerDelete', itemID => {
            console.log(itemID);

            Swal.fire({
                title: '{{ __("admin.swal.temp.title") }}',
                text: '',
                icon: "warning",
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#aaa',
                cancelButtonText: '{{ __("admin.swal.temp.buttons.cancel") }}',
                confirmButtonText: '{{ __("admin.swal.temp.buttons.approve") }}'
            }).then((result) => {
                //if user clicks on delete
                if (result.value) {
                    // calling destroy method to delete
                @this.call('removeCourse',itemID);
                    // success response
                    Swal.fire({title: '{{ __("admin.swal.temp.deleted.title") }}', icon: 'success',
                        width: 600,
                        position: 'top-end',
                        showConfirmButton: false,
                        timer: 1000});
                } else {
                    Swal.fire({
                        title: '{{ __("admin.swal.temp.discarded.title") }}',
                        icon: 'success',
                        width: 600,
                        position: 'top-end',
                        showConfirmButton: false,
                        timer: 1000
                    });
                }
            });
        });
        })
    </script>
@endpush
