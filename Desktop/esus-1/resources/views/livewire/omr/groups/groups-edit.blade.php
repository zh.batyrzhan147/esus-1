<div>
    <x-slot name="header"></x-slot>
    <div class="bg-white mx-auto py-6 px-4 sm:px-6 lg:px-8 justify-between items-end flex w-full">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('admin.groups.edit.title') }}
        </h2>
{{--        <div class="flex space-x-1">--}}
{{--            <a href="{{ route('omr.groups.statements',['slug' => $group->id]) }}" class="font-medium text-[#FFFFFF] px-8 py-2.5 rounded bg-[#3B82F6] hover:bg-[#2563EB] flex items-center">--}}
{{--                {{ __('trainer.buttons.table') }}--}}
{{--            </a>--}}
{{--        </div>--}}
    </div>
    <p class="px-4 sm:px-6 lg:px-8 text-sm">{{ __('admin.groups.edit.description') }}</p>



    <div class="py-6 px-4 sm:px-6 lg:px-8 h-full">
        <div class="flex flex-col items-start md:items-center w-full md:w-3/4 mx-auto space-y-4">

            <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-center">
                <div class="w-full md:w-1/4">
                    <label for="affiliate" class="w-full text-left md:text-right block text-sm">{{ __('admin.groups.add.modal.affiliate') }}</label>
                </div>
                <div class="w-full md:w-3/4">
                    <select wire:model="group.affiliate"
                            class="w-full md:w-3/4 px-3 py-1 border rounded focus:border-blue-500 hover:border-blue-500 text-sm outline-none focus:input-shadow">
                        <option value>{{ __('admin.groups.add.modal.chooseaffiliate') }}</option>
                        @foreach(\App\Models\Affiliates::all() ?? []  as $item)
                            <option value="{{ $item->id }}">{{ $item->title }}</option>
                        @endforeach
                    </select>
                    @error('group.affiliate')
                    <p class="text-xs text-red-600">{{ $message }}</p>
                    @enderror
                </div>
            </div>

            <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-center">
                <div class="w-full md:w-1/4">
                    <label for="year" class="w-full text-left md:text-right block text-sm">{{ __('admin.groups.add.modal.year') }}</label>
                </div>
                <div class="w-full md:w-3/4">
                    <select wire:model="year" disabled
                            class="w-full md:w-3/4 px-3 py-1 border rounded focus:border-blue-500 hover:border-blue-500 text-sm outline-none focus:input-shadow">
                        @foreach(\App\Models\Years::orderBy('year','ASC')->get() as $item)
                            <option value="{{ $item->year }}">{{ $item->year }}</option>
                        @endforeach
                    </select>
                    @error('year')
                    <p class="text-xs text-red-600">{{ $message }}</p>
                    @enderror
                </div>
            </div>

            <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-center">
                <div class="w-full md:w-1/4">
                    <label for="course" class="w-full text-left md:text-right block text-sm">{{ __('admin.groups.add.modal.program') }}</label>
                </div>
                <div class="w-full md:w-3/4">
                    <select wire:model="courses" disabled
                            class="w-full md:w-3/4 px-3 py-1 border rounded focus:border-blue-500 hover:border-blue-500 text-sm outline-none focus:input-shadow">
                        <option value>{{ __('admin.groups.add.modal.chooseprogram') }}</option>
                        @foreach($yearItem->courses ?? [] as $courseItem)
                            <option value="{{ $courseItem->id }}">{{ $courseItem->ru_title }}</option>
                        @endforeach
                    </select>
                    @error('courses')
                    <p class="text-xs text-red-600">{{ $message }}</p>
                    @enderror
                </div>
            </div>

            <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-center">
                <div class="w-full md:w-1/4">
                    <label for="language" class="w-full text-left md:text-right block text-sm">{{ __('admin.groups.add.modal.lang') }}</label>
                </div>
                <div class="w-full md:w-3/4">
                    <select wire:model="group.language"
                            class="w-full md:w-3/4 px-3 py-1 border rounded focus:border-blue-500 hover:border-blue-500 text-sm outline-none focus:input-shadow">
                        <option value="">{{ __('admin.groups.add.modal.chooselang') }}</option>
                        @foreach(\App\Models\Languages::all() as $item)
                            <option value="{{ $item->id }}">{{ $item->title }}</option>
                        @endforeach
                    </select>
                    @error('group.language')
                    <p class="text-xs text-red-600">{{ $message }}</p>
                    @enderror
                </div>
            </div>

            <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-center">
                <div class="w-full md:w-1/4">
                    <label for="traineraffiliate"
                           class="w-full text-left md:text-right block text-sm">{{ __('admin.groups.add.modal.affiliate.trainer') }}</label>
                </div>
                <div class="w-full md:w-3/4">
                    <select wire:model="traineraffiliate"
                            class="w-full md:w-3/4 px-3 py-1 border rounded focus:border-blue-500 hover:border-blue-500 text-sm outline-none focus:input-shadow">
                        <option value>{{ __('admin.groups.add.modal.chooseaffiliate') }}</option>
                        @foreach(\App\Models\Affiliates::all() ?? []  as $item)
                            <option value="{{ $item->id }}">{{ $item->title }}</option>
                        @endforeach
                    </select>
                    @error('traineraffiliate')
                    <p class="text-xs text-red-600">{{ $message }}</p>
                    @enderror
                </div>
            </div>

            <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-center">
                <div class="w-full md:w-1/4">
                    <label for="course" class="w-full text-left md:text-right block text-sm">{{ __('admin.groups.add.modal.trainer') }}</label>
                </div>
                <div class="w-full md:w-3/4">
                    <select wire:model="group.trainer"
                            class="w-full md:w-3/4 px-3 py-1 border rounded focus:border-blue-500 hover:border-blue-500 text-sm outline-none focus:input-shadow">
                        <option value>{{ __('admin.groups.add.modal.choosetrainer') }}</option>
                        @foreach($trainers ?? []  as $item)
                            <option value="{{ $item->id }}">{{ $item->fio }}</option>
                        @endforeach
                    </select>
                    @error('group.trainer')
                    <p class="text-xs text-red-600">{{ $message }}</p>
                    @enderror
                </div>
            </div>


            <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-center">
                <div class="w-full md:w-1/4">
                    <label for="slug" class="w-full text-left md:text-right block text-sm">{{ __('admin.groups.add.modal.cifr') }}</label>
                </div>
                <div class="w-full md:w-3/4">
                    <input wire:model="slug" type="text"
                           placeholder="{{ __('admin.groups.add.modal.cifr.placeholder') }}"
                           class="w-full md:w-3/4 px-3 py-1 border rounded focus:border-blue-500 hover:border-blue-500 text-sm outline-none focus:input-shadow"
                    />
                    @error('slug')
                    <p class="text-xs text-red-600">{{ $message }}</p>
                    @enderror
                </div>
            </div>

            <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-center">
                <div class="w-full md:w-1/4">
                    <label for="slug"
                           class="w-full text-left md:text-right block text-sm">{{ __('admin.groups.add.modal.zoomlink') }}</label>
                </div>
                <div class="w-full md:w-3/4">
                    <input wire:model="group.zoom_link" type="text"
                           placeholder="{{ __('admin.groups.add.modal.zoomlink.placeholder') }}"
                           class="w-full md:w-3/4 px-3 py-1 border rounded focus:border-blue-500 hover:border-blue-500 text-sm outline-none focus:input-shadow"
                    />
                    @error('group.zoom_link')
                    <p class="text-xs text-red-600">{{ $message }}</p>
                    @enderror
                </div>
            </div>

            <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-center">
                <div class="w-full md:w-1/4">
                    <label for="slug"
                           class="italic w-full text-left md:text-right block text-sm">{{ __('webinar.info') }}
                    </label>
                </div>
                <div class="w-full md:w-3/4">
                    <p  class="text-sm outline-none focus:input-shadow">{{ __('webinar.len') }}: {{ $webinar_length }} мин.</p>
                    @error('webinar_length')
                    <p class="text- text-red-600">{{ $message }}</p>
                    @enderror

                    <p  class="text-sm outline-none focus:input-shadow"> {{ __('admin.courses.create.form.course.webinar.count') }} {{ $webinar_count }}</p>
                    @error('webinar_count')
                    <p class="text- text-red-600">{{ $message }}</p>
                    @enderror
                </div>
            </div>

            <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-center">
                <div class="w-full md:w-1/4">
                    <label for="offer_agreement" class="w-full text-left md:text-right block text-sm">{{ __('admin.groups.add.modal.dogovor') }}</label>
                </div>
                @if($group->offer_agreement)
                    <a href="{{ $group->getFileUrl($group->offer_agreement) }}" download="{{ __('admin.groups.add.modal.dogovor') }}.{{ pathinfo($group->offer_agreement, PATHINFO_EXTENSION)}}" target="_blank">{{ __('admin.links.download') }}</a>
                @endif
                <div class="w-full md:w-3/4">
                    <input wire:model="offer_agreement" type="file"
                           class="w-full md:w-3/4 px-3 py-1 border rounded focus:border-blue-500 hover:border-blue-500 text-sm outline-none focus:input-shadow"
                    />
                    <svg wire:loading wire:target="offer_agreement" role="status" class="w-8 h-8 mr-2 text-gray-200 animate-spin dark:text-gray-600 fill-blue-600" viewBox="0 0 100 101" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M100 50.5908C100 78.2051 77.6142 100.591 50 100.591C22.3858 100.591 0 78.2051 0 50.5908C0 22.9766 22.3858 0.59082 50 0.59082C77.6142 0.59082 100 22.9766 100 50.5908ZM9.08144 50.5908C9.08144 73.1895 27.4013 91.5094 50 91.5094C72.5987 91.5094 90.9186 73.1895 90.9186 50.5908C90.9186 27.9921 72.5987 9.67226 50 9.67226C27.4013 9.67226 9.08144 27.9921 9.08144 50.5908Z" fill="currentColor"/>
                        <path d="M93.9676 39.0409C96.393 38.4038 97.8624 35.9116 97.0079 33.5539C95.2932 28.8227 92.871 24.3692 89.8167 20.348C85.8452 15.1192 80.8826 10.7238 75.2124 7.41289C69.5422 4.10194 63.2754 1.94025 56.7698 1.05124C51.7666 0.367541 46.6976 0.446843 41.7345 1.27873C39.2613 1.69328 37.813 4.19778 38.4501 6.62326C39.0873 9.04874 41.5694 10.4717 44.0505 10.1071C47.8511 9.54855 51.7191 9.52689 55.5402 10.0491C60.8642 10.7766 65.9928 12.5457 70.6331 15.2552C75.2735 17.9648 79.3347 21.5619 82.5849 25.841C84.9175 28.9121 86.7997 32.2913 88.1811 35.8758C89.083 38.2158 91.5421 39.6781 93.9676 39.0409Z" fill="currentFill"/>
                    </svg>

                    @error('offer_agreement')
                    <p class="text-xs text-red-600">{{ $message }}</p>
                    @enderror
                </div>
            </div>

            <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-center">
                <div class="w-full md:w-1/4">
                    <label for="count_all" class="w-full text-left md:text-right block text-sm">{{ __('admin.groups.add.modal.listeners.count') }}</label>
                </div>
                <div class="w-full md:w-3/4">
                    <input wire:model="group.count_all" type="text"
                           placeholder="{{ __('admin.groups.add.modal.listeners.count.placeholder') }}"
                           class="w-full md:w-3/4 px-3 py-1 border rounded focus:border-blue-500 hover:border-blue-500 text-sm outline-none focus:input-shadow"
                    />
                    @error('group.count_all')
                    <p class="text-xs text-red-600">{{ $message }}</p>
                    @enderror
                </div>
            </div>

            <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-center">
                <div class="w-full md:w-1/4">
                    <label for="start" placeholder="{{ __('admin.groups.add.modal.start.placeholder') }}" class="w-full text-left md:text-right block text-sm">{{ __('admin.groups.add.modal.start') }}</label>
                </div>
                <div class="w-full md:w-3/4">
                    <x-inputs.date id="start" wire:model.lazy="group.start_time" class="w-full md:w-3/4 px-3 py-1 border rounded focus:border-blue-500 hover:border-blue-500 text-sm outline-none focus:input-shadow"/>
                    @error('group.start_time')
                    <p class="text-xs text-red-600">{{ $message }}</p>
                    @enderror
                </div>
            </div>

            <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-center">
                <div class="w-full md:w-1/4">
                    <label for="end" class="w-full text-left md:text-right block text-sm">{{ __('admin.groups.add.modal.end') }}</label>
                </div>
                <div class="w-full md:w-3/4">
                    <x-inputs.date id="end" wire:model.lazy="group.end_time" placeholder="{{ __('admin.groups.add.modal.end.placeholder') }}" class="w-full md:w-3/4 px-3 py-1 border rounded focus:border-blue-500 hover:border-blue-500 text-sm outline-none focus:input-shadow"/>
                    @error('group.end_time')
                    <p class="text-xs text-red-600">{{ $message }}</p>
                    @enderror
                </div>
            </div>

            <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-center">
                <div class="w-full md:w-1/4">
                    <label for="practice_start" placeholder="{{ __('admin.groups.add.modal.practice.start.placeholder') }}"
                           class="w-full text-left md:text-right block text-sm">{{ __('admin.groups.add.modal.practice.start') }}</label>
                </div>
                <div class="w-full md:w-3/4">
                    <x-inputs.date id="practice_start" wire:model.lazy="group.practice_start"
                                   class="w-full md:w-3/4 px-3 py-1 border rounded focus:border-blue-500 hover:border-blue-500 text-sm outline-none focus:input-shadow"/>
                    @error('group.practice_start')
                    <p class="text-xs text-red-600">{{ $message }}</p>
                    @enderror
                </div>
            </div>

            <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-center">
                <div class="w-full md:w-1/4">
                    <label for="practice_end" placeholder="{{ __('admin.groups.add.modal.practice.end.placeholder') }}"
                           class="w-full text-left md:text-right block text-sm">{{ __('admin.groups.add.modal.practice.end') }}</label>
                </div>
                <div class="w-full md:w-3/4">
                    <x-inputs.date id="practice_end" wire:model.lazy="group.practice_end"
                                   class="w-full md:w-3/4 px-3 py-1 border rounded focus:border-blue-500 hover:border-blue-500 text-sm outline-none focus:input-shadow"/>
                    @error('group.practice_end')
                    <p class="text-xs text-red-600">{{ $message }}</p>
                    @enderror
                </div>
            </div>

            <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-center">
                <div class="w-full md:w-1/4">
                    <label for="cert_day" placeholder="{{ __('Дата выдачи сертификата') }}"
                           class="w-full text-left md:text-right block text-sm">{{ __('Дата выдачи сертификата') }}</label>
                </div>
                <div class="w-full md:w-3/4">
                    <x-inputs.date id="cert_day" wire:model.lazy="group.cert_day"
                                   class="w-full md:w-3/4 px-3 py-1 border rounded focus:border-blue-500 hover:border-blue-500 text-sm outline-none focus:input-shadow"/>
                    @error('group.cert_day')
                    <p class="text-xs text-red-600">{{ $message }}</p>
                    @enderror
                </div>
            </div>

            <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-center">
                <div class="w-full md:w-1/4">
                    <label for="change_statements" class="w-full text-left md:text-right block text-sm">{{ __('admin.groups.add.allow_change_statements') }}</label>
                </div>
                <div class="w-full md:w-3/4">
                    <input wire:model.lazy="group.change_statements" type="checkbox" class="form-checkbox">
                </div>
            </div>

            <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-center">
                <div class="w-full md:w-1/4">
                    <label for="status" class="w-full text-left md:text-right block text-sm">{{ __('admin.groups.add.modal.status') }}</label>
                </div>
                <div class="w-full md:w-3/4">
                    <select wire:model="group.status"
                            class="w-full md:w-3/4 px-3 py-1 border rounded focus:border-blue-500 hover:border-blue-500 text-sm outline-none focus:input-shadow">
                        <option value>{{ __('admin.groups.add.modal.choosestatus') }}</option>
                        @foreach(\App\Models\Groups::listGroupStatuses() ?? []  as $key => $item)
                            <option value="{{ $key }}">{{ $item }}</option>
                        @endforeach
                    </select>
                    @error('group.status')
                    <p class="text-xs text-red-600">{{ $message }}</p>
                    @enderror
                </div>
            </div>

            @if ($group->status == \App\Models\Groups::GROUP_STATUS_PLANNED)
                <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-center">
                    <div class="w-full md:w-1/4">
                        <label for=""
                               class="w-full text-left md:text-right block text-sm">{{ __('Пригласительная ссылка:') }}</label>
                    </div>

                    <div class="w-full md:w-3/4">
                        <div class="flex flex-row w-full md:w-3/4 space-x-2">
                            <input type="text"
                                   value="{{ $group->joinLink }}" disabled id="copyLink"
                                   class="w-full px-3 py-1 border rounded focus:border-blue-500 hover:border-blue-500 text-sm outline-none focus:input-shadow"
                            />
                            <button onclick="copyToClipboard()"
                                    class="py-2.5 px-4 rounded bg-green-300 text-base whitespace-nowrap hover:bg-green-500">
                                {{ __('trainer.groups.planned.joinlink.copy') }}
                            </button>
                            <script>
                                function copyToClipboard() {
                                    /* Get the text field */
                                    let copyText = document.getElementById("copyLink");

                                    /* Select the text field */
                                    copyText.select();
                                    copyText.setSelectionRange(0, 99999); /* For mobile devices */

                                    /* Copy the text inside the text field */
                                    navigator.clipboard.writeText(copyText.value);
                                    Swal.fire({
                                        title: '{{ __("admin.swal.grants.title.copied") }}',
                                        icon: 'success',
                                        width: 600,
                                        position: 'top-end',
                                        showConfirmButton: false,
                                        timer: 1000
                                    });

                                }
                            </script>
                        </div>
                    </div>
                </div>
            @endif

            <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-center">
                <div class="w-full md:w-1/4">
                </div>
                <div class="w-full md:w-3/4 space-x-2 flex">
                    <button wire:click="save"
                            class="mb-2 text-sm flex justify-center items-center px-4 py-1 border-0 bg-blue-500 text-white rounded hover:bg-blue-600">
                        {{ __('admin.buttons.save') }}
                    </button>
                    <a href="{{ route('omr.groups.index') }}"
                       class="py-1 px-4 border inline-block m-0 mb-2 text-sm rounded hover:border-black">{{ __('admin.buttons.cancel') }}</a>
                </div>
            </div>
        </div>
    </div>
    <div class="bg-white mx-auto py-2 px-4 sm:px-6 lg:px-8 flex flex-row justify-between">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Список слушателей') }} {{ $group->users()->count() }}/{{$group->count_all}}
        </h2>

        <div class="flex flex-row space-x-2">
            <button wire:click="downloadxlsx"
                    class="mb-2 text-sm flex justify-center items-center px-4 py-1 border-0 bg-blue-500 text-white rounded hover:bg-blue-600">
                {{ __('Скачать XLSX') }}
            </button>
            <button wire:click="downloadcvs"
                    class="mb-2 text-sm flex justify-center items-center px-4 py-1 border-0 bg-blue-500 text-white rounded hover:bg-blue-600">
                {{ __('Скачать CSV для СДО') }}
            </button>
        </div>
    </div>
    <div class="py-6 px-4 sm:px-6 lg:px-8 h-full">
        <div class="py-2 align-middle inline-block min-w-full sm:px-6 lg:px-8">
            <div class="shadow overflow-hidden border-b border-gray-200 sm:rounded">
                <table class="min-w-full divide-y divide-gray-200 w-full">
                    <thead>
                    <tr>
                        <th scope="col"
                            class="px-2 py-3 bg-gray-50 text-left text-sm font-medium text-gray-800">
                            <div class="text-center">{{ __('№') }}</div>
                        </th>
                        <th scope="col"
                            class="px-2 py-3 bg-gray-50 text-left text-sm font-medium text-gray-800">
                            <div class="text-center">{{ __('admin.table.head.iin') }}</div>
                        </th>
                        <th scope="col"
                            class="px-2 py-3 bg-gray-50 text-left text-sm font-medium text-gray-800">
                            <div class="pl-4 border-l border-gray-200">{{ __('admin.table.head.fio') }}</div>
                        </th>
                        <th scope="col"
                            class="px-2 py-3 bg-gray-50 text-left text-sm font-medium text-gray-800">
                            <div class="pl-4">{{ __('admin.table.head.phone') }}</div>
                        </th>
                        <th scope="col"
                            class="px-2 py-3 bg-gray-50 text-left text-sm font-medium text-gray-800">
                            <div class="pl-4">{{ __('listener.profile.account.region') }}</div>
                        </th>
                        <th scope="col"
                            class="px-2 py-3 bg-gray-50 text-left text-sm font-medium text-gray-800">
                            <div class="pl-4">{{ __('listener.table.head.action') }}</div>
                        </th>
                    </tr>
                    </thead>
                    <tbody class="bg-white divide-y divide-gray-200">
                    @foreach($group->users as $item)
                        <tr>
                            <td class="px-6 py-1.5 text-sm text-gray-900">
                                {{ $loop->index+1 }}
                            </td>
                            <td class="px-6 py-1.5 text-sm text-gray-900">
                                {{ $item->iin }}
                            </td>
                            <td class="px-6 py-1.5 whitespace-nowrap text-sm text-gray-900">
                                {{ $item->fio }}
                            </td>
                            <td class="px-6 py-1.5 text-sm text-gray-900">
                                {{ $item->phone }}
                            </td>
                            <td class="px-6 py-1.5 text-sm text-gray-900">
                                @if($item->oblastItem or $item->raionItem or $item->gorodItem)
                                    @if($item->oblastItem)<div class="text-[14px] text-[#18181B] font-light m-0 p-0">Область: {{ $item->oblastItem->ru_title ?? 'Не указано' }}</div>@endif
                                    @if($item->raionItem)<div class="text-[14px] text-[#18181B] font-light m-0 p-0">Район: {{ $item->raionItem->ru_title ?? 'Не указано' }}</div>@endif
                                    @if($item->gorodItem)<div class="text-[14px] text-[#18181B] font-light m-0 p-0">Населенный пункт: {{ $item->gorodItem->ru_title ?? 'Не указано' }}</div>@endif
                                @else
                                    <div class="text-[14px] text-[#18181B] font-light m-0 p-0">Не указано</div>
                                @endif
                            </td>
                            <td class="px-6 py-1.5 text-sm text-gray-900">
                                <button wire:click="$emit('triggerDelete',{{ $item->id }})"
                                        class="text-red-500 hover:text-red-900 bg-none border-0 m-0 cursor-pointer bg-transparent font-normal ">{{ __('admin.links.delete') }}</button>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    @if ($group->status == \App\Models\Groups::GROUP_STATUS_PLANNED)
        <div class="bg-white mx-auto py-2 px-4 sm:px-6 lg:px-8 flex flex-row justify-between">
            <h2 class="font-semibold text-xl text-gray-800 leading-tight">
                {{ __('Список заявок') }}
            </h2>
        </div>
        <div class="py-6 px-4 sm:px-6 lg:px-8 h-full">
            <div class="py-2 align-middle inline-block min-w-full sm:px-6 lg:px-8">
                <div class="shadow overflow-hidden border-b border-gray-200 sm:rounded">
                    <table class="min-w-full divide-y divide-gray-200 w-full">
                        <thead>
                        <tr>
                            <th scope="col"
                                class="px-2 py-3 bg-gray-50 text-left text-sm font-medium text-gray-800">
                                <div class="text-center">{{ __('№') }}</div>
                            </th>
                            <th scope="col"
                                class="px-2 py-3 bg-gray-50 text-left text-sm font-medium text-gray-800">
                                <div class="text-center">{{ __('admin.table.head.iin') }}</div>
                            </th>
                            <th scope="col"
                                class="px-2 py-3 bg-gray-50 text-left text-sm font-medium text-gray-800">
                                <div class="pl-4 border-l border-gray-200">{{ __('admin.table.head.fio') }}</div>
                            </th>
                            <th scope="col"
                                class="px-2 py-3 bg-gray-50 text-left text-sm font-medium text-gray-800">
                                <div class="pl-4">{{ __('admin.table.head.phone') }}</div>
                            </th>
                            <th scope="col"
                                class="px-2 py-3 bg-gray-50 text-left text-sm font-medium text-gray-800">
                                <div class="pl-4">{{ __('listener.profile.account.region') }}</div>
                            </th>
                        </tr>
                        </thead>
                        <tbody class="bg-white divide-y divide-gray-200">
                        @foreach($group->users_sb as $item)
                            <tr>
                                <td class="px-6 py-1.5 text-sm text-gray-900">
                                    {{ $loop->index+1 }}
                                </td>
                                <td class="px-6 py-1.5 text-sm text-gray-900">
                                    {{ $item->iin }}
                                </td>
                                <td class="px-6 py-1.5 whitespace-nowrap text-sm text-gray-900">
                                    {{ $item->fio }}
                                </td>
                                <td class="px-6 py-1.5 text-sm text-gray-900">
                                    {{ $item->phone }}
                                </td>
                                <td class="px-6 py-1.5 text-sm text-gray-900">
                                    @if($item->oblastItem or $item->raionItem or $item->gorodItem)
                                        @if($item->oblastItem)<div class="text-[14px] text-[#18181B] font-light m-0 p-0">Область: {{ $item->oblastItem->ru_title ?? 'Не указано' }}</div>@endif
                                        @if($item->raionItem)<div class="text-[14px] text-[#18181B] font-light m-0 p-0">Район: {{ $item->raionItem->ru_title ?? 'Не указано' }}</div>@endif
                                        @if($item->gorodItem)<div class="text-[14px] text-[#18181B] font-light m-0 p-0">Населенный пункт: {{ $item->gorodItem->ru_title ?? 'Не указано' }}</div>@endif
                                    @else
                                        <div class="text-[14px] text-[#18181B] font-light m-0 p-0">Не указано</div>
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    @endif
</div>


@push('scripts')
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>

    <script type="text/javascript">
        document.addEventListener('DOMContentLoaded', function () {
            @this.on('triggerDelete', itemID => {
                Swal.fire({
                    title: '{{ __("admin.swal.temp.title") }}',
                    text: '{{ __("admin.swal.groups.users.description") }}',
                    icon: "warning",
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#aaa',
                    cancelButtonText: '{{ __("admin.swal.temp.buttons.cancel") }}',
                    confirmButtonText: '{{ __("admin.swal.temp.buttons.approve") }}'
                }).then((result) => {
                    //if user clicks on delete
                    if (result.value) {
                        // calling destroy method to delete
                        @this.call('removeListener', itemID);

                    } else {
                        Swal.fire({
                            title: '{{ __("admin.swal.temp.discarded.title") }}',
                            icon: 'success'
                        });
                    }
                });
            });

            @this.on('successDelete', () => {
                Swal.fire({title: '{{ __("admin.swal.temp.deleted.title") }}', icon: 'success'});
            })
        })
    </script>

@endpush

@push('styles')
{{--    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/flatpickr/dist/flatpickr.min.css">--}}
{{--    <script src="https://cdn.jsdelivr.net/npm/flatpickr"></script>--}}
{{--    <script src="https://cdn.jsdelivr.net/npm/flatpickr/dist/l10n/ru.js"></script>--}}
@endpush
