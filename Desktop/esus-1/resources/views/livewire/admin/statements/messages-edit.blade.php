<div>
    <x-slot name="header">
        <div class="bg-white mx-auto py-6 px-4 sm:px-6 lg:px-8">
            <h2 class="font-semibold text-xl text-gray-800 leading-tight">
                {{ __('admin.groups.edit.title') }}
            </h2>
        </div>
    </x-slot>
    <p class="px-4 sm:px-6 lg:px-8 text-sm">{{ __('admin.groups.edit.description') }}</p>
    <div class="py-6 px-4 sm:px-6 lg:px-8 h-full">
        <div class="flex flex-col items-start md:items-center w-full md:w-3/4 mx-auto space-y-4">
            <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-center">
                <div class="w-full md:w-1/4">
                    <label for="patronymic" class="w-full text-left md:text-right block text-sm">{{ __('admin.users.edit.iin') }}</label>
                </div>
                <div class="w-full md:w-3/4">
                    <span>{{ $message->userItem->iin }}</span>
                </div>
            </div>

            <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-center">
                <div class="w-full md:w-1/4">
                    <label for="patronymic" class="w-full text-left md:text-right block text-sm">{{ __('admin.users.edit.fio') }}</label>
                </div>
                <div class="w-full md:w-3/4">
                    <span>{{ $message->userItem->fio }}</span>
                </div>
            </div>

            <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-center">
                <div class="w-full md:w-1/4">
                    <label for="patronymic" class="w-full text-left md:text-right block text-sm">{{ __('admin.groups.add.modal.cifr') }}</label>
                </div>
                <div class="w-full md:w-3/4">
                    <span>{{ $message->groupItem->slug }}</span>
                </div>
            </div>

            <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-center">
                <div class="w-full md:w-1/4">
                    <label for="patronymic" class="w-full text-left md:text-right block text-sm">{{ __('admin.groups.add.modal.trainer') }}</label>
                </div>
                <div class="w-full md:w-3/4">
                    <span>{{ $message->groupItem->trainerItem->fio }}</span>
                </div>
            </div>

            <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-center">
                <div class="w-full md:w-1/4">
                    <label for="patronymic" class="w-full text-left md:text-right block text-sm">{{ __('admin.statements-messages.available_at.title') }}</label>
                </div>
                <div class="w-full md:w-3/4">
                    <span>{{ date('d.m.Y', strtotime($message->statementItem->available_at)) }}</span>
                </div>
            </div>

            <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-center">
                <div class="w-full md:w-1/4">
                    <label for="patronymic" class="w-full text-left md:text-right block text-sm">{{ __('admin.groups.add.modal.affiliate') }}</label>
                </div>
                <div class="w-full md:w-3/4">
                    <span>{{ $message->affiliateItem->title }}</span>
                </div>
            </div>

            <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-center">
                <div class="w-full md:w-1/4">
                    <label for="patronymic" class="w-full text-left md:text-right block text-sm">{{ __('admin.statements-messages.changedby.title') }}</label>
                </div>
                <div class="w-full md:w-3/4">
                    <span>{{ $message->changedbyItem->fio ?? '' }}</span>
                </div>
            </div>

            <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-center">
                <div class="w-full md:w-1/4">
                    <label for="message.description" class="w-full text-left md:text-right block text-sm">{{ __('admin.statements-messages.description') }}</label>
                </div>
                <div class="w-full md:w-3/4">
                    <textarea wire:model.defer="message.description" type="text"
                           class="w-full md:w-3/4 px-3 py-1 border rounded focus:border-blue-500 hover:border-blue-500 text-sm outline-none focus:input-shadow"
                              rows="3"
                    ></textarea>
                    @error('message.description')
                    <p class="text-xs text-red-600">{{ $message }}</p>
                    @enderror
                </div>
            </div>

            <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-center">
                <div class="w-full md:w-1/4">
                    <label for="file" class="w-full text-left md:text-right block text-sm">{{ __('admin.statements-messages.file') }}:</label>
                </div>
                @if($message->file)
                    <a href="{{ $message->getFileUrl($message->file) }}" download="{{ $message->id }}.{{ pathinfo($message->file, PATHINFO_EXTENSION)}}" target="_blank">{{ __('admin.links.download') }}</a>
                @endif
                <div class="w-full md:w-3/4">
                    <input type="file" wire:model.defer="file" id="file" class="w-full md:w-3/4 px-3 py-1 border rounded focus:border-blue-500 hover:border-blue-500 text-sm outline-none focus:input-shadow"
                           value="{{ old('file', $file) }}" />
                    <svg wire:loading wire:target="file" role="status" class="w-8 h-8 mr-2 text-gray-200 animate-spin dark:text-gray-600 fill-blue-600" viewBox="0 0 100 101" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M100 50.5908C100 78.2051 77.6142 100.591 50 100.591C22.3858 100.591 0 78.2051 0 50.5908C0 22.9766 22.3858 0.59082 50 0.59082C77.6142 0.59082 100 22.9766 100 50.5908ZM9.08144 50.5908C9.08144 73.1895 27.4013 91.5094 50 91.5094C72.5987 91.5094 90.9186 73.1895 90.9186 50.5908C90.9186 27.9921 72.5987 9.67226 50 9.67226C27.4013 9.67226 9.08144 27.9921 9.08144 50.5908Z" fill="currentColor"/>
                        <path d="M93.9676 39.0409C96.393 38.4038 97.8624 35.9116 97.0079 33.5539C95.2932 28.8227 92.871 24.3692 89.8167 20.348C85.8452 15.1192 80.8826 10.7238 75.2124 7.41289C69.5422 4.10194 63.2754 1.94025 56.7698 1.05124C51.7666 0.367541 46.6976 0.446843 41.7345 1.27873C39.2613 1.69328 37.813 4.19778 38.4501 6.62326C39.0873 9.04874 41.5694 10.4717 44.0505 10.1071C47.8511 9.54855 51.7191 9.52689 55.5402 10.0491C60.8642 10.7766 65.9928 12.5457 70.6331 15.2552C75.2735 17.9648 79.3347 21.5619 82.5849 25.841C84.9175 28.9121 86.7997 32.2913 88.1811 35.8758C89.083 38.2158 91.5421 39.6781 93.9676 39.0409Z" fill="currentFill"/>
                    </svg>

                    @error('file')
                    <p class="text-xs text-red-600">{{ $message }}</p>
                    @enderror
                </div>
            </div>

            <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-center">
                <div class="w-full md:w-1/4">
                    <label for="status" class="w-full text-left md:text-right block text-sm">{{ __('admin.statements-messages.status') }}</label>
                </div>
                <div class="w-full md:w-3/4">
                    <select wire:model.defer="message.status"
                            class="w-full md:w-3/4 px-3 py-1 border rounded focus:border-blue-500 hover:border-blue-500 text-sm outline-none focus:input-shadow">
                        @foreach(\App\Models\StatementsMessages::listStatuses() ?? []  as $key => $item)
                            <option value="{{ $key }}">{{ $item }}</option>
                        @endforeach
                    </select>
                    @error('message.status')
                    <p class="text-xs text-red-600">{{ $message }}</p>
                    @enderror
                </div>
            </div>
            <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-center">
                <div class="w-full md:w-1/4">
                </div>
                <div class="w-full md:w-3/4 space-x-2 flex">
                    <button wire:click="save"
                            class="mb-2 text-sm flex justify-center items-center px-4 py-1 border-0 bg-blue-500 text-white rounded hover:bg-blue-600">
                        {{ __('admin.buttons.save') }}
                    </button>
                    <a href="{{ route('admin.statements.index') }}"
                       class="py-1 px-4 border inline-block m-0 mb-2 text-sm rounded hover:border-black">{{ __('admin.buttons.cancel') }}</a>
                </div>
            </div>
        </div>
    </div>

</div>

@push('styles')
{{--    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/flatpickr/dist/flatpickr.min.css">--}}
{{--    <script src="https://cdn.jsdelivr.net/npm/flatpickr"></script>--}}
{{--    <script src="https://cdn.jsdelivr.net/npm/flatpickr/dist/l10n/ru.js"></script>--}}
@endpush
