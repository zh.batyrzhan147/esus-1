<div>
    <x-slot name="header">
        <div class="bg-white mx-auto py-6 px-4 sm:px-6 lg:px-8">
            <h2 class="font-semibold text-xl text-gray-800 leading-tight">
                @if(request()->routeIs('admin.courses.create'))
                    {{ __('admin.courses.create.title') }}
                @else
                    {{ __('admin.courses.edit.title') }}
                @endif
            </h2>
        </div>
    </x-slot>

    <p class="px-4 sm:px-6 lg:px-8 text-sm">
        @if(request()->routeIs('admin.courses.create'))
            {{ __('admin.courses.create.description') }}
        @else
            {{ __('admin.courses.edit.description') }}
        @endif
    </p>

    <div class="py-6 px-4 sm:px-6 lg:px-8 h-full">
        <div class="flex flex-col items-start md:items-center w-full md:w-3/4 mx-auto space-y-4">

            <div class="flex flex-col items-start w-full mx-auto space-y-4">
                <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-center">
                    <div class="w-full md:w-1/4">
                        <label for="kz_title" class="w-full text-left md:text-right block text-sm">{{ __('admin.courses.create.form.kztitle') }}</label>
                    </div>
                    <div class="w-full md:w-3/4">
                        <input wire:model.lazy="kz_title" type="text" name="kz_title" id="kz_title" class="w-full md:w-3/4 px-3 py-1 border rounded focus:border-blue-500 hover:border-blue-500 text-sm outline-none focus:input-shadow"
                               placeholder="{{ __('admin.courses.create.form.kztitle.placeholder') }}" value="{{ old('kz_title', $kz_title) }}" />
                        @error('kz_title')
                        <p class="text-xs text-red-600">{{ $message }}</p>
                        @enderror
                    </div>
                </div>

                <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-center">
                    <div class="w-full md:w-1/4">
                        <label for="ru_title" class="w-full text-left md:text-right block text-sm">{{ __('admin.courses.create.form.rutitle') }}</label>
                    </div>
                    <div class="w-full md:w-3/4">
                        <input wire:model.lazy="ru_title" type="text" name="ru_title" id="ru_title" class="w-full md:w-3/4 px-3 py-1 border rounded focus:border-blue-500 hover:border-blue-500 text-sm outline-none focus:input-shadow"
                               placeholder="{{ __('admin.courses.create.form.rutitle.placeholder') }}" value="{{ old('ru_title', $ru_title) }}" />
                        @error('ru_title')
                        <p class="text-xs text-red-600">{{ $message }}</p>
                        @enderror
                    </div>
                </div>
                <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-center">
                    <div class="w-full md:w-1/4">
                        <label for="course.kz_short_title" class="w-full text-left md:text-right block text-sm">{{ __('Краткое название (Каз):') }}</label>
                    </div>
                    <div class="w-full md:w-3/4">
                        <input wire:model.lazy="course.kz_short_title" type="text" name="course.kz_short_title" id="course.kz_short_title" class="w-full md:w-3/4 px-3 py-1 border rounded focus:border-blue-500 hover:border-blue-500 text-sm outline-none focus:input-shadow"
                               placeholder="{{ __('Введите краткое название на казахском') }}" value="{{ old('kz_short_title', $course->kz_short_title) }}" />
                        @error('kz_short_title')
                        <p class="text-xs text-red-600">{{ $message }}</p>
                        @enderror
                    </div>
                </div>


                <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-center">
                    <div class="w-full md:w-1/4">
                        <label for="course.ru_short_title" class="w-full text-left md:text-right block text-sm">{{ __('Краткое название (Рус):') }}</label>
                    </div>
                    <div class="w-full md:w-3/4">
                        <input wire:model.lazy="course.ru_short_title" type="text" name="course.ru_short_title" id="course.ru_short_title" class="w-full md:w-3/4 px-3 py-1 border rounded focus:border-blue-500 hover:border-blue-500 text-sm outline-none focus:input-shadow"
                               placeholder="{{ __('Введите краткое название на русском') }}" value="{{ old('ru_short_title', $course->ru_short_title) }}" />
                        @error('ru_short_title')
                        <p class="text-xs text-red-600">{{ $message }}</p>
                        @enderror
                    </div>
                </div>

                <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-center">
                    <div class="w-full md:w-1/4">
                        <label for="course.en_short_title" class="w-full text-left md:text-right block text-sm">{{ __('Краткое название (Анг):') }}</label>
                    </div>
                    <div class="w-full md:w-3/4">
                        <input wire:model.lazy="course.en_short_title" type="text" name="course.en_short_title" id="course.en_short_title" class="w-full md:w-3/4 px-3 py-1 border rounded focus:border-blue-500 hover:border-blue-500 text-sm outline-none focus:input-shadow"
                               placeholder="{{ __('Введите краткое название на английском') }}" value="{{ old('en_short_title', $course->en_short_title) }}" />
                        @error('en_short_title')
                        <p class="text-xs text-red-600">{{ $message }}</p>
                        @enderror
                    </div>
                </div>


                <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-center">
                    <div class="w-full md:w-1/4">
                        <label for="slug" class="w-full text-left md:text-right block text-sm">{{ __('admin.courses.create.form.slug') }}</label>
                    </div>
                    <div class="w-full md:w-3/4 ">
                        <div class="w-full md:w-3/4 flex flex-row justify-between">
                            <input wire:model.lazy="slug" type="text" name="slug" id="slug" class="w-full px-3 py-1 border rounded focus:border-blue-500 hover:border-blue-500 text-sm outline-none focus:input-shadow"
                                   placeholder="{{ __('admin.courses.create.form.slug.placeholder') }}" value="{{ old('slug', $slug) }}" />
                            <button type="button" wire:click="generate" class="ml-2 text-sm flex justify-center items-center px-4 py-1 border-0 bg-blue-500 text-white rounded hover:bg-blue-600">{{ __('admin.buttons.generate') }}</button>
                        </div>
                        @error('slug')
                        <p class="text-xs text-red-600">{{ $message }}</p>
                        @enderror
                    </div>
                </div>
            </div>

            <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-center">
                <div class="w-full md:w-1/4">
                    <label for="type" class="w-full text-left md:text-right block text-sm">{{ __('admin.courses.create.form.course.type') }}</label>
                </div>
                <div class="w-full md:w-3/4">
                    <select wire:model.defer="course.type" id="type"
                            class="w-full md:w-3/4 px-3 py-1 border rounded focus:border-blue-500 hover:border-blue-500 text-sm outline-none focus:input-shadow">
                        @foreach(\App\Models\Courses::listTypes() as $key => $type)
                            <option value="{{ $key }}" @if( ($course->type ?? '') == $key) selected @endif>{{ $type }}</option>
                        @endforeach
                    </select>
                    @error('course.type')
                    <p class="text-xs text-red-600">{{ $message }}</p>
                    @enderror
                </div>
            </div>

            <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-center">
                <div class="w-full md:w-1/4">
                    <label for="grading" class="w-full text-left md:text-right block text-sm">{{ __('admin.courses.create.form.course.grading') }}</label>
                </div>
                <div class="w-full md:w-3/4">
                    <select wire:model.defer="course.grading" id="grading"
                            class="w-full md:w-3/4 px-3 py-1 border rounded focus:border-blue-500 hover:border-blue-500 text-sm outline-none focus:input-shadow">
                        @foreach(\App\Models\Courses::listGrading() as $key => $grading)
                            <option value="{{ $key }}" @if( ($course->grading ?? '') == $key) selected @endif>{{ $grading }}</option>
                        @endforeach
                    </select>
                    @error('course.grading')
                    <p class="text-xs text-red-600">{{ $message }}</p>
                    @enderror
                </div>
            </div>

            <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-center">
                <div class="w-full md:w-1/4">
                    <label for="course.en_short_title" class="w-full text-left md:text-right block text-sm">{{ __('Цена программы:') }}</label>
                </div>
                <div class="w-full md:w-3/4">
                    <input wire:model.lazy="price" type="number" name="price" id="price" class="w-full md:w-3/4 px-3 py-1 border rounded focus:border-blue-500 hover:border-blue-500 text-sm outline-none focus:input-shadow"
                           placeholder="{{ __('Введите цену') }}" value="{{ old('price', $course->price) }}" />
                    @error('price')
                    <p class="text-xs text-red-600">{{ $message }}</p>
                    @enderror
                </div>
            </div>

            <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-center">
                <div class="w-full md:w-1/4">
                    <label for="auto_generation" class="w-full text-left md:text-right block text-sm">{{ __('admin.courses.create.form.course.autogeneration') }}</label>
                </div>
                <div class="w-full md:w-3/4">
                    <select wire:model.defer="course.auto_generation" id="auto_generation"
                            class="w-full md:w-3/4 px-3 py-1 border rounded focus:border-blue-500 hover:border-blue-500 text-sm outline-none focus:input-shadow">
                        <option value="1" @if( $course->auto_generation == true) selected @endif>{{ __('admin.courses.create.form.course.autogeneration.true') }}</option>
                        <option value="0" @if( $course->auto_generation == false) selected @endif>{{ __('admin.courses.create.form.course.autogeneration.false') }}</option>
                    </select>
                    @error('course.auto_generation')
                    <p class="text-xs text-red-600">{{ $message }}</p>
                    @enderror
                </div>
            </div>

            <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-center">
                <div class="w-full md:w-1/4">
                    <label for="course_length" class="w-full text-left md:text-right block text-sm">{{ __('admin.courses.create.form.course.length') }}</label>
                </div>
                <div class="w-full md:w-3/4">
                    <input type="text" wire:model.defer="course.course_length" id="course_length" class="w-full md:w-3/4 px-3 py-1 border rounded focus:border-blue-500 hover:border-blue-500 text-sm outline-none focus:input-shadow"
                           placeholder="{{ __('admin.courses.create.form.course.length.placeholder') }}" value="{{ old('course_length', $course->course_length) }}" />
                    @error('course.course_length')
                    <p class="text-xs text-red-600">{{ $message }}</p>
                    @enderror
                </div>
            </div>

            <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-center">
                <div class="w-full md:w-1/4">
                    <label for="practice_length" class="w-full text-left md:text-right block text-sm">{{ __('admin.courses.create.form.course.practice_length') }}</label>
                </div>
                <div class="w-full md:w-3/4">
                    <input type="text" wire:model.defer="course.practice_length" id="practice_length" class="w-full md:w-3/4 px-3 py-1 border rounded focus:border-blue-500 hover:border-blue-500 text-sm outline-none focus:input-shadow"
                           placeholder="{{ __('admin.courses.create.form.course.practice_length.placeholder') }}" value="{{ old('practice_length', $course->practice_length) }}" />
                    @error('course.practice_length')
                    <p class="text-xs text-red-600">{{ $message }}</p>
                    @enderror
                </div>
            </div>

            <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-center">
                <div class="w-full md:w-1/4">
                    <label for="webinar_count" class="w-full text-left md:text-right block text-sm">{{ __('admin.courses.create.form.course.webinar.count') }}</label>
                </div>
                <div class="w-full md:w-3/4">
                    <input type="text" wire:model.defer="course.webinar_count" id="webinar_count" class="w-full md:w-3/4 px-3 py-1 border rounded focus:border-blue-500 hover:border-blue-500 text-sm outline-none focus:input-shadow"
                           placeholder="{{ __('admin.courses.create.form.course.webinar.placeholder') }}" value="{{ old('webinar_count', $course->webinar_count) }}" />
                    @error('course.webinar_count')
                    <p class="text-xs text-red-600">{{ $message }}</p>
                    @enderror
                </div>
            </div>

            <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-center">
                <div class="w-full md:w-1/4">
                    <label for="webinar_length" class="w-full text-left md:text-right block text-sm">{{ __('admin.courses.create.form.course.webinar.length') }}</label>
                </div>
                <div class="w-full md:w-3/4">
                    <input type="text" wire:model.defer="course.webinar_length" id="webinar_length" class="w-full md:w-3/4 px-3 py-1 border rounded focus:border-blue-500 hover:border-blue-500 text-sm outline-none focus:input-shadow"
                           placeholder="{{ __('admin.courses.create.form.course.webinar.length.placeholder') }}" value="{{ old('webinar_length', $course->webinar_length) }}" />
                    @error('course.webinar_length')
                    <p class="text-xs text-red-600">{{ $message }}</p>
                    @enderror
                </div>
            </div>

            <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-center">
                <div class="w-full md:w-1/4">
                    <label for="file_1_listener" class="w-full text-left md:text-right block text-sm">{{ __('admin.courses.create.form.file.kzlistener') }}:</label>
                </div>
                @if($course->file_1_listener)
                    <a href="{{ $course->getFileUrl($course->file_1_listener) }}" download="{{ __('admin.courses.create.form.file.kzlistener') }}.{{ pathinfo($course->file_1_listener, PATHINFO_EXTENSION)}}" target="_blank">{{ __('admin.links.download') }}</a>
                @endif
                <div class="w-full md:w-3/4">
                    <input type="file" wire:model.defer="file_1_listener" id="file_1_listener" class="w-full md:w-3/4 px-3 py-1 border rounded focus:border-blue-500 hover:border-blue-500 text-sm outline-none focus:input-shadow"
                           value="{{ old('file_1_listener', $file_1_listener) }}" />
                    <svg wire:loading wire:target="file_1_listener" role="status" class="w-8 h-8 mr-2 text-gray-200 animate-spin dark:text-gray-600 fill-blue-600" viewBox="0 0 100 101" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M100 50.5908C100 78.2051 77.6142 100.591 50 100.591C22.3858 100.591 0 78.2051 0 50.5908C0 22.9766 22.3858 0.59082 50 0.59082C77.6142 0.59082 100 22.9766 100 50.5908ZM9.08144 50.5908C9.08144 73.1895 27.4013 91.5094 50 91.5094C72.5987 91.5094 90.9186 73.1895 90.9186 50.5908C90.9186 27.9921 72.5987 9.67226 50 9.67226C27.4013 9.67226 9.08144 27.9921 9.08144 50.5908Z" fill="currentColor"/>
                        <path d="M93.9676 39.0409C96.393 38.4038 97.8624 35.9116 97.0079 33.5539C95.2932 28.8227 92.871 24.3692 89.8167 20.348C85.8452 15.1192 80.8826 10.7238 75.2124 7.41289C69.5422 4.10194 63.2754 1.94025 56.7698 1.05124C51.7666 0.367541 46.6976 0.446843 41.7345 1.27873C39.2613 1.69328 37.813 4.19778 38.4501 6.62326C39.0873 9.04874 41.5694 10.4717 44.0505 10.1071C47.8511 9.54855 51.7191 9.52689 55.5402 10.0491C60.8642 10.7766 65.9928 12.5457 70.6331 15.2552C75.2735 17.9648 79.3347 21.5619 82.5849 25.841C84.9175 28.9121 86.7997 32.2913 88.1811 35.8758C89.083 38.2158 91.5421 39.6781 93.9676 39.0409Z" fill="currentFill"/>
                    </svg>
                    @error('file_1_listener')
                    <p class="text-xs text-red-600">{{ $message }}</p>
                    @enderror
                </div>
            </div>

            <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-center">
                <div class="w-full md:w-1/4">
                    <label for="file_2_listener" class="w-full text-left md:text-right block text-sm">{{ __('admin.courses.create.form.file.rulistener') }}:</label>
                </div>
                @if($course->file_2_listener)
                    <a href="{{ $course->getFileUrl($course->file_2_listener) }}" download="{{ __('admin.courses.create.form.file.rulistener') }}.{{ pathinfo($course->file_2_listener, PATHINFO_EXTENSION)}}" target="_blank">{{ __('admin.links.download') }}</a>
                @endif
                <div class="w-full md:w-3/4">
                    <input type="file" wire:model.defer="" id="file_2_listener" class="w-full md:w-3/4 px-3 py-1 border rounded focus:border-blue-500 hover:border-blue-500 text-sm outline-none focus:input-shadow"
                           value="{{ old('file_2_listener', $file_2_listener) }}" />
                    <svg wire:loading wire:target="file_2_listener" role="status" class="w-8 h-8 mr-2 text-gray-200 animate-spin dark:text-gray-600 fill-blue-600" viewBox="0 0 100 101" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M100 50.5908C100 78.2051 77.6142 100.591 50 100.591C22.3858 100.591 0 78.2051 0 50.5908C0 22.9766 22.3858 0.59082 50 0.59082C77.6142 0.59082 100 22.9766 100 50.5908ZM9.08144 50.5908C9.08144 73.1895 27.4013 91.5094 50 91.5094C72.5987 91.5094 90.9186 73.1895 90.9186 50.5908C90.9186 27.9921 72.5987 9.67226 50 9.67226C27.4013 9.67226 9.08144 27.9921 9.08144 50.5908Z" fill="currentColor"/>
                        <path d="M93.9676 39.0409C96.393 38.4038 97.8624 35.9116 97.0079 33.5539C95.2932 28.8227 92.871 24.3692 89.8167 20.348C85.8452 15.1192 80.8826 10.7238 75.2124 7.41289C69.5422 4.10194 63.2754 1.94025 56.7698 1.05124C51.7666 0.367541 46.6976 0.446843 41.7345 1.27873C39.2613 1.69328 37.813 4.19778 38.4501 6.62326C39.0873 9.04874 41.5694 10.4717 44.0505 10.1071C47.8511 9.54855 51.7191 9.52689 55.5402 10.0491C60.8642 10.7766 65.9928 12.5457 70.6331 15.2552C75.2735 17.9648 79.3347 21.5619 82.5849 25.841C84.9175 28.9121 86.7997 32.2913 88.1811 35.8758C89.083 38.2158 91.5421 39.6781 93.9676 39.0409Z" fill="currentFill"/>
                    </svg>
                    @error('file_2_listener')
                    <p class="text-xs text-red-600">{{ $message }}</p>
                    @enderror
                </div>
            </div>

            <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-center">
                <div class="w-full md:w-1/4">
                    <label for="file_1_teacher" class="w-full text-left md:text-right block text-sm">{{ __('admin.courses.create.form.file.kzteacher') }}:</label>
                </div>
                @if($course->file_1_teacher)
                    <a href="{{ $course->getFileUrl($course->file_1_teacher) }}" download="{{ __('admin.courses.create.form.file.kzteacher') }}.{{ pathinfo($course->file_1_teacher, PATHINFO_EXTENSION)}}" target="_blank">{{ __('admin.links.download') }}</a>
                @endif
                <div class="w-full md:w-3/4">
                    <input type="file" wire:model.defer="file_1_teacher" id="file_1_teacher" class="w-full md:w-3/4 px-3 py-1 border rounded focus:border-blue-500 hover:border-blue-500 text-sm outline-none focus:input-shadow"
                           value="{{ old('file_1_teacher', $file_1_teacher) }}" />
                    <svg wire:loading wire:target="file_1_teacher" role="status" class="w-8 h-8 mr-2 text-gray-200 animate-spin dark:text-gray-600 fill-blue-600" viewBox="0 0 100 101" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M100 50.5908C100 78.2051 77.6142 100.591 50 100.591C22.3858 100.591 0 78.2051 0 50.5908C0 22.9766 22.3858 0.59082 50 0.59082C77.6142 0.59082 100 22.9766 100 50.5908ZM9.08144 50.5908C9.08144 73.1895 27.4013 91.5094 50 91.5094C72.5987 91.5094 90.9186 73.1895 90.9186 50.5908C90.9186 27.9921 72.5987 9.67226 50 9.67226C27.4013 9.67226 9.08144 27.9921 9.08144 50.5908Z" fill="currentColor"/>
                        <path d="M93.9676 39.0409C96.393 38.4038 97.8624 35.9116 97.0079 33.5539C95.2932 28.8227 92.871 24.3692 89.8167 20.348C85.8452 15.1192 80.8826 10.7238 75.2124 7.41289C69.5422 4.10194 63.2754 1.94025 56.7698 1.05124C51.7666 0.367541 46.6976 0.446843 41.7345 1.27873C39.2613 1.69328 37.813 4.19778 38.4501 6.62326C39.0873 9.04874 41.5694 10.4717 44.0505 10.1071C47.8511 9.54855 51.7191 9.52689 55.5402 10.0491C60.8642 10.7766 65.9928 12.5457 70.6331 15.2552C75.2735 17.9648 79.3347 21.5619 82.5849 25.841C84.9175 28.9121 86.7997 32.2913 88.1811 35.8758C89.083 38.2158 91.5421 39.6781 93.9676 39.0409Z" fill="currentFill"/>
                    </svg>

                    @error('file_1_teacher')
                    <p class="text-xs text-red-600">{{ $message }}</p>
                    @enderror
                </div>
            </div>

            <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-center">
                <div class="w-full md:w-1/4">
                    <label for="file_2_teacher" class="w-full text-left md:text-right block text-sm">{{ __('admin.courses.create.form.file.ruteacher') }}:</label>
                </div>
                @if($course->file_2_teacher)
                    <a href="{{ $course->getFileUrl($course->file_2_teacher) }}" download="{{ __('admin.courses.create.form.file.ruteacher') }}.{{ pathinfo($course->file_2_teacher, PATHINFO_EXTENSION)}}" target="_blank">{{ __('admin.links.download') }}</a>
                @endif
                <div class="w-full md:w-3/4">
                    <input type="file" wire:model.defer="file_2_teacher" id="file_2_teacher" class="w-full md:w-3/4 px-3 py-1 border rounded focus:border-blue-500 hover:border-blue-500 text-sm outline-none focus:input-shadow"
                           value="{{ old('file_2_teacher', $file_2_teacher) }}" />
                    <svg wire:loading wire:target="file_2_teacher" role="status" class="w-8 h-8 mr-2 text-gray-200 animate-spin dark:text-gray-600 fill-blue-600" viewBox="0 0 100 101" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M100 50.5908C100 78.2051 77.6142 100.591 50 100.591C22.3858 100.591 0 78.2051 0 50.5908C0 22.9766 22.3858 0.59082 50 0.59082C77.6142 0.59082 100 22.9766 100 50.5908ZM9.08144 50.5908C9.08144 73.1895 27.4013 91.5094 50 91.5094C72.5987 91.5094 90.9186 73.1895 90.9186 50.5908C90.9186 27.9921 72.5987 9.67226 50 9.67226C27.4013 9.67226 9.08144 27.9921 9.08144 50.5908Z" fill="currentColor"/>
                        <path d="M93.9676 39.0409C96.393 38.4038 97.8624 35.9116 97.0079 33.5539C95.2932 28.8227 92.871 24.3692 89.8167 20.348C85.8452 15.1192 80.8826 10.7238 75.2124 7.41289C69.5422 4.10194 63.2754 1.94025 56.7698 1.05124C51.7666 0.367541 46.6976 0.446843 41.7345 1.27873C39.2613 1.69328 37.813 4.19778 38.4501 6.62326C39.0873 9.04874 41.5694 10.4717 44.0505 10.1071C47.8511 9.54855 51.7191 9.52689 55.5402 10.0491C60.8642 10.7766 65.9928 12.5457 70.6331 15.2552C75.2735 17.9648 79.3347 21.5619 82.5849 25.841C84.9175 28.9121 86.7997 32.2913 88.1811 35.8758C89.083 38.2158 91.5421 39.6781 93.9676 39.0409Z" fill="currentFill"/>
                    </svg>

                    @error('file_2_teacher')
                    <p class="text-xs text-red-600">{{ $message }}</p>
                    @enderror
                </div>
            </div>


{{--            <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-center">--}}
{{--                <div class="w-full md:w-1/4">--}}
{{--                    <label for="sdo_category" class="w-full text-left md:text-right block text-sm">{{ __('admin.courses.create.form.sdo.category') }}</label>--}}
{{--                </div>--}}
{{--                <div class="w-full md:w-3/4">--}}
{{--                    <input type="text" wire:model.defer="course.sdo_category" id="sdo_category" class="w-full md:w-3/4 px-3 py-1 border rounded focus:border-blue-500 hover:border-blue-500 text-sm outline-none focus:input-shadow"--}}
{{--                          placeholder="{{ __('admin.courses.create.form.sdo.category.placeholder') }}" value="{{ old('sdo_category', $course->sdo_category) }}" />--}}
{{--                    @error('course.sdo_category')--}}
{{--                    <p class="text-xs text-red-600">{{ $message }}</p>--}}
{{--                    @enderror--}}
{{--                </div>--}}
{{--            </div>--}}

            <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-center">
                <div class="w-full md:w-1/4">
                    <label for="offer_agreement" class="w-full text-left md:text-right block text-sm">{{ __('admin.groups.add.modal.dogovor') }}</label>
                </div>
                @if($course->offer_agreement)
                    <a href="{{ $course->getFileUrl($course->offer_agreement) }}" download="{{ __('admin.groups.add.modal.dogovor') }}.{{ pathinfo($course->offer_agreement, PATHINFO_EXTENSION)}}" target="_blank">{{ __('admin.links.download') }}</a>
                @endif
                <div class="w-full md:w-3/4">
                    <input wire:model="offer_agreement" type="file"
                           class="w-full md:w-3/4 px-3 py-1 border rounded focus:border-blue-500 hover:border-blue-500 text-sm outline-none focus:input-shadow"
                    />
                    <svg wire:loading wire:target="offer_agreement" role="status" class="w-8 h-8 mr-2 text-gray-200 animate-spin dark:text-gray-600 fill-blue-600" viewBox="0 0 100 101" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M100 50.5908C100 78.2051 77.6142 100.591 50 100.591C22.3858 100.591 0 78.2051 0 50.5908C0 22.9766 22.3858 0.59082 50 0.59082C77.6142 0.59082 100 22.9766 100 50.5908ZM9.08144 50.5908C9.08144 73.1895 27.4013 91.5094 50 91.5094C72.5987 91.5094 90.9186 73.1895 90.9186 50.5908C90.9186 27.9921 72.5987 9.67226 50 9.67226C27.4013 9.67226 9.08144 27.9921 9.08144 50.5908Z" fill="currentColor"/>
                        <path d="M93.9676 39.0409C96.393 38.4038 97.8624 35.9116 97.0079 33.5539C95.2932 28.8227 92.871 24.3692 89.8167 20.348C85.8452 15.1192 80.8826 10.7238 75.2124 7.41289C69.5422 4.10194 63.2754 1.94025 56.7698 1.05124C51.7666 0.367541 46.6976 0.446843 41.7345 1.27873C39.2613 1.69328 37.813 4.19778 38.4501 6.62326C39.0873 9.04874 41.5694 10.4717 44.0505 10.1071C47.8511 9.54855 51.7191 9.52689 55.5402 10.0491C60.8642 10.7766 65.9928 12.5457 70.6331 15.2552C75.2735 17.9648 79.3347 21.5619 82.5849 25.841C84.9175 28.9121 86.7997 32.2913 88.1811 35.8758C89.083 38.2158 91.5421 39.6781 93.9676 39.0409Z" fill="currentFill"/>
                    </svg>

                    @error('offer_agreement')
                    <p class="text-xs text-red-600">{{ $message }}</p>
                    @enderror
                </div>
            </div>

            <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-center">
                <div class="w-full md:w-1/4">
                    <label for="sdo_link" class="w-full text-left md:text-right block text-sm">{{ __('admin.courses.create.form.sdo.link') }}</label>
                </div>
                <div class="w-full md:w-3/4">
                    <input type="text" wire:model.defer="course.sdo_link" id="sdo_link" class="w-full md:w-3/4 px-3 py-1 border rounded focus:border-blue-500 hover:border-blue-500 text-sm outline-none focus:input-shadow"
                          placeholder="{{ __('admin.courses.create.form.sdo.link.placeholder') }}" value="{{ old('sdo_link', $course->sdo_link) }}" />
                    @error('course.sdo_link')
                    <p class="text-xs text-red-600">{{ $message }}</p>
                    @enderror
                </div>
            </div>

            <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-center">
                <div class="w-full md:w-1/4">
                    <label for="sdo_link_ru" class="w-full text-left md:text-right block text-sm">{{ __('admin.courses.create.form.sdo.link.ru') }}</label>
                </div>
                <div class="w-full md:w-3/4">
                    <input type="text" wire:model.defer="course.sdo_link_ru" id="sdo_link_ru" class="w-full md:w-3/4 px-3 py-1 border rounded focus:border-blue-500 hover:border-blue-500 text-sm outline-none focus:input-shadow"
                           placeholder="{{ __('admin.courses.create.form.sdo.link.placeholder') }}" value="{{ old('sdo_link_ru', $course->sdo_link_ru) }}" />
                    @error('course.sdo_link_ru')
                    <p class="text-xs text-red-600">{{ $message }}</p>
                    @enderror
                </div>
            </div>

            <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-center">
                <div class="w-full md:w-1/4">
                    <label for="sdo_link_en" class="w-full text-left md:text-right block text-sm">{{ __('admin.courses.create.form.sdo.link.en') }}</label>
                </div>
                <div class="w-full md:w-3/4">
                    <input type="text" wire:model.defer="course.sdo_link_en" id="sdo_link_en" class="w-full md:w-3/4 px-3 py-1 border rounded focus:border-blue-500 hover:border-blue-500 text-sm outline-none focus:input-shadow"
                           placeholder="{{ __('admin.courses.create.form.sdo.link.placeholder') }}" value="{{ old('sdo_link_en', $course->sdo_link_en) }}" />
                    @error('course.sdo_link_en')
                    <p class="text-xs text-red-600">{{ $message }}</p>
                    @enderror
                </div>
            </div>

{{--            <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-center">--}}
{{--                <div class="w-full md:w-1/4">--}}
{{--                    <label for="one_drive_link" class="w-full text-left md:text-right block text-sm">{{ __('admin.courses.create.form.onedrive.link') }}</label>--}}
{{--                </div>--}}
{{--                <div class="w-full md:w-3/4">--}}
{{--                    <input type="text" wire:model.defer="course.one_drive_link" id="one_drive_link" class="w-full md:w-3/4 px-3 py-1 border rounded focus:border-blue-500 hover:border-blue-500 text-sm outline-none focus:input-shadow"--}}
{{--                          placeholder="{{ __('admin.courses.create.form.onedrive.link.placeholder') }}" value="{{ old('one_drive_link', $course->one_drive_link) }}" />--}}
{{--                    @error('course.one_drive_link')--}}
{{--                    <p class="text-xs text-red-600">{{ $message }}</p>--}}
{{--                    @enderror--}}
{{--                </div>--}}
{{--            </div>--}}

            <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-center">
                <div class="w-full md:w-1/4">
                </div>
                <div class="w-full md:w-3/4 space-x-2 flex">
                    <button  wire:click="save" class="mb-2 text-sm flex justify-center items-center px-4 py-1 border-0 bg-blue-500 text-white rounded hover:bg-blue-600">@if(request()->routeIs('admin.courses.create')) {{ __('admin.buttons.create') }} @else {{ __('admin.buttons.save') }} @endif</button>
                    <a href="{{ route('admin.courses.index') }}" class="py-1 px-4 border inline-block m-0 mb-2 text-sm rounded hover:border-black">{{ __('admin.buttons.cancel') }}</a>
                </div>
            </div>
        </div>
    </div>
</div>
