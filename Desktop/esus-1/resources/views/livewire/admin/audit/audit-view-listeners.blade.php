<div>
{{--    @dd($audit)--}}
    <div class="bg-white mx-auto py-6 px-4 sm:px-6 lg:px-8">
        <div class="flex justify-between items-center w-full">
            <h2 class="font-semibold text-xl text-gray-800 leading-tight">
                Аудит слушателей за {{ $year }} год
            </h2>
        </div>
    </div>

    <div class="bg-white mx-auto py-6 px-4 sm:px-6 lg:px-8">
        <div class="flex justify-between items-center w-full">
            <p class="font-normal text-xl text-gray-800 leading-tight">
                Ниже описаны группы и программы, в которых не верное колличество слушатей относительно плана.<br>
                В учет взяты только группы которые имеют статус:<br>
                  - Сформировано<br>
                  - Идет обучение<br>
                  - Завершено<br>
                  - На оценивании<br>
                  - Сертификат выдан<br>
                  - В архиве<br>
            </p>
        </div>
    </div>

    <div class="py-6 px-4 sm:px-6 lg:px-8">
        <div class="flex flex-col">
            <div class="-my-2 overflow-x-auto sm:-mx-6 lg:-mx-8">
                <div class="py-2 align-middle inline-block min-w-full sm:px-6 lg:px-8">
                    <div class="shadow overflow-hidden border-b border-gray-200 sm:rounded">
{{--                        @if($audit['grants']['total'])--}}
{{--                            <p class="px-6 py-1.5 bg-gray-100 text-xl text-gray-900 font-bold"><b>Общее количество:</b> План: {{ $audit['grants']['total']['count'] }} | Назначено:{{ $audit['grants']['total']['sum'] }} | Разница:{{ $audit['grants']['total']['count'] - $audit['grants']['total']['sum'] }} </p>--}}
{{--                        @endif--}}
                        @foreach($audit['groups']['items'] ?? [] as $coures)
{{--                            @if($coures[''])--}}
                            <div class="py-4">
                                <p class="px-6 py-1.5 bg-gray-100 text-xl text-gray-900 font-bold">{!! $coures['courses']['title'] ?? '' !!}</p>
{{--                                @if(array_key_exists('total',$coures))--}}
{{--                                <p class="px-6 py-1.5 bg-gray-100 text-xl text-gray-900 font-normal"><b>Общее количество:</b> План: {{ $coures['total']['count'] }} | Назначено:{{ $coures['total']['sum'] }} | Разница:{{ $coures['total']['count'] - $coures['total']['sum'] }} </p>--}}
{{--                                @endif--}}
                                @if (count($coures['items'] ?? []) > 0)
                                <table class="min-w-full divide-y divide-gray-200 w-full">
                                    <thead>
                                    <tr>
                                        <th scope="col"
                                            class="px-2 py-3 bg-gray-50 text-left text-sm font-medium text-gray-800">
                                            <div class="pl-4">{{ __('admin.table.head.affiliatetitle') }}</div>
                                        </th>
                                        <th scope="col"
                                            class="px-2 py-3 bg-gray-50 text-left text-sm font-medium text-gray-800">
                                            <div class="pl-4 border-l border-gray-200">{{ __('admin.table.head.count.listener') }}</div>
                                        </th>
                                        @foreach($languages as $lang)
                                            <th scope="col"
                                                class="px-2 py-3 bg-gray-50 text-left text-sm font-medium text-gray-800">
                                                <div class="pl-4 border-l border-gray-200">{{ __('admin.table.head.count.listener') }} ({{ $lang->title }})
                                                </div>
                                            </th>
                                        @endforeach
                                    </tr>
                                    </thead>
                                    <tbody class="bg-white divide-y divide-gray-200">
                            @foreach($coures['items'] ?? [] as $affiliate)
{{--                                @dd($affiliate)--}}
                                    <tr>
                                        <td class="px-6 py-1.5 whitespace-nowrap text-sm text-gray-900 font-bold">
                                            {{ $affiliate['affiliate']['title'] }}
                                        </td>

                                        <td class="px-6 py-1.5 whitespace-nowrap text-sm text-gray-900">
{{--                                            {{ $affiliate['count_all'] }}--}}
                                        </td>

                                        @foreach($languages as $lang)

                                            <td class="px-6 py-1.5 whitespace-nowrap text-sm text-gray-900">
{{--                                                План: {{ $affiliate['items'][$lang->id]['counts'] ?? 0 }} | Назначено: {{ $affiliate['items'][$lang->id]['groups'] ?? 0 }} | Групп: {{$affiliate['items'][$lang->id]['groupscount'] ?? 0}}--}}

                                                @foreach($affiliate['items'][$lang->id]['groups'] ??  [] as $group)
                                                    Группа:  {{ $group['slug'] }}  | План: {{ $group['planed'] }} | Факт: {{ $group['fact'] }} <br>
                                                @endforeach

                                            </td>
                                        @endforeach
                                    </tr>

                            @endforeach
                                    </tbody>
                                </table>
                                    @endif
                            </div>
                        @endforeach

                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
