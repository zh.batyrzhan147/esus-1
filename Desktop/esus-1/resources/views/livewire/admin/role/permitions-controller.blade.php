<div>
    <x-slot name="header"></x-slot>
    <div class="bg-white mx-auto py-6 px-4 sm:px-6 lg:px-8">
        <div class="w-full flex justify-between items-center">
            <h2 class="font-semibold text-xl text-gray-800 leading-tight">
                {{ __('admin.roles.list.title') }}
            </h2>

            <div class="flex space-x-4">
                @include('admin.common.secondmenu', [
                    'tabname'=>'courses',
                    'popup'=>true,
                ])
                <button wire:click="reloadRoles" class="text-sm flex justify-center items-center space-x-1.5 px-4 py-1 border-0 bg-blue-500 text-white rounded hover:bg-blue-600">
                    <span>{{ __('admin.roles.list.button') }}</span>
                </button>
            </div>
        </div>
    </div>

    <div class="py-6 px-4 sm:px-6 lg:px-8">
        <div class="flex flex-col">
            <div class="-my-2 overflow-x-auto sm:-mx-6 lg:-mx-8">
                <div class="py-2 align-middle inline-block min-w-full sm:px-6 lg:px-8">
                    <div class="shadow overflow-hidden border-b border-gray-200 sm:rounded">
                        <table class="min-w-full divide-y divide-gray-200 w-full">
                            <thead>
                            <tr>
                                <th scope="col"
                                    class="px-2 py-3 bg-gray-50 text-left text-sm font-medium text-gray-800">
                                    <div class="text-center">{{ __('admin.table.head.id') }}</div>
                                </th>

                                <th scope="col"
                                    class="px-2 py-3 bg-gray-50 text-left text-sm font-medium text-gray-800">
                                    <div class="pl-4">{{ __('admin.table.head.role') }}</div>
                                </th>

                                <th scope="col"
                                    class="px-2 py-3 bg-gray-50 text-left text-sm font-medium text-gray-800">
                                    <div class="pl-4">{{ __('admin.table.head.count') }}</div>
                                </th>

                                <th scope="col" width="200"
                                    class="px-2 py-3 bg-gray-50 text-left text-sm font-medium text-gray-800">
                                    <div class="pl-4 border-l border-gray-200">{{ __('admin.table.head.action') }}</div>
                                </th>
                            </tr>
                            </thead>
                            <tbody class="bg-white divide-y divide-gray-200">
                            @foreach ($roles as $item)
                                <tr>
                                    <td class="px-6 py-1.5 whitespace-nowrap text-sm text-gray-900">
                                        {{ $item->id }}
                                    </td>
                                    <td class="px-6 py-1.5 whitespace-nowrap text-sm text-gray-900">
                                        {{ $item->title }}
                                    </td>

                                    <td class="px-6 py-1.5 whitespace-nowrap text-sm text-gray-900">
                                        {{ $item->users()->count() }}
                                    </td>

                                    <td class="px-6 py-1.5 whitespace-nowrap text-sm font-medium space-x-2 text-right">
                                        <a href="{{ route('admin.roles.list',['role'=>$item->id]) }}"
                                                class="text-blue-500 hover:text-blue-900 m-0 font-normal">{{ __('admin.links.show') }}</a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
