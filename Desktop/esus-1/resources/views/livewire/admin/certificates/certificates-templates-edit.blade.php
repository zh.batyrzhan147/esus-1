<div>
    <x-slot name="header"></x-slot>
    <div class="bg-white mx-auto py-6 px-4 sm:px-6 lg:px-8"  wire:ignore >
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            @if(request()->routeIs('admin.certificates-templates.create'))
                {{ __('admin.certificates.create.title') }}
            @else
                {{ __('admin.certificates.edit.title') }}
            @endif
        </h2>
    </div>
    <p class="px-4 sm:px-6 lg:px-8 text-sm" wire:ignore>@if(request()->routeIs('admin.certificates-templates.create')) {{ __('admin.certificates.create.description') }} @else {{ __('admin.certificates.edit.description') }} @endif</p>
    <div class="py-6 px-4 sm:px-6 lg:px-8 h-full">
        <div class="flex flex-col items-start md:items-center w-full md:w-3/4 mx-auto space-y-4">

            <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-center">
                <div class="w-full md:w-1/4">
                    <label for="type" class="w-full text-left md:text-right block text-sm">{{ __('Тип шаблона') }}</label>
                </div>
                <div class="w-full md:w-3/4">
                    <select wire:model="template.type"
                            class="w-full md:w-3/4 px-3 py-1 border rounded focus:border-blue-500 hover:border-blue-500 text-sm outline-none focus:input-shadow">
                        <option value>{{ __('Выберите тип') }}</option>
                        @foreach(\App\Models\CertificatesTemplates::listTypes() as $key => $item)
                            <option value="{{ $key }}">{{ $item }}</option>
                        @endforeach
                    </select>
                    @error('template.type')
                    <p class="text-xs text-red-600">{{ $message }}</p>
                    @enderror
                </div>
            </div>

            <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-center">
                <div class="w-full md:w-1/4">
                    <label for="slug" class="w-full text-left md:text-right block text-sm">{{ __('admin.certificates.edit.form.title') }}</label>
                </div>
                <div class="w-full md:w-3/4">
                    <input wire:model="template.title" type="text"
                         placeholder="{{ __('admin.certificates.edit.form.title.placeholder') }}"  class="w-full md:w-3/4 px-3 py-1 border rounded focus:border-blue-500 hover:border-blue-500 text-sm outline-none focus:input-shadow"
                    />
                    @error('slug')
                    <p class="text-xs text-red-600">{{ $message }}</p>
                    @enderror
                </div>
            </div>

            <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-center">
                <div class="w-full md:w-1/4">
                    <label for="template.active" class="w-full text-left md:text-right block text-sm">{{ __('admin.certificates.edit.form.active') }}</label>
                </div>
                <div class="w-full md:w-3/4">
                    <select wire:model.defer="template.active" id="template.active"
                            class="w-full md:w-3/4 px-3 py-1 border rounded focus:border-blue-500 hover:border-blue-500 text-sm outline-none focus:input-shadow">
                        <option value="1" @if( $template->active == true) selected @endif>{{ __('admin.certificates.edit.form.active.true') }}</option>
                        <option value="0" @if( $template->active == false) selected @endif>{{ __('admin.certificates.edit.form.active.false') }}</option>
                    </select>
                    @error('template.active')
                    <p class="text-xs text-red-600">{{ $message }}</p>
                    @enderror
                </div>
            </div>

            <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-center">
                <div class="w-full md:w-1/4">
                    <label for="year" class="w-full text-left md:text-right block text-sm">{{ __('admin.certificates.edit.form.year') }}</label>
                </div>
                <div class="w-full md:w-3/4">
                    <select wire:model="year"
                            class="w-full md:w-3/4 px-3 py-1 border rounded focus:border-blue-500 hover:border-blue-500 text-sm outline-none focus:input-shadow">
                        <option value>{{ __('admin.certificates.edit.form.chooseyear') }}</option>
                        @foreach(\App\Models\Years::orderBy('year','ASC')->get() as $item)
                            <option value="{{ $item->year }}">{{ $item->year }}</option>
                        @endforeach
                    </select>
                    @error('year')
                    <p class="text-xs text-red-600">{{ $message }}</p>
                    @enderror
                </div>
            </div>

            <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-center">
                <div class="w-full md:w-1/4">
                    <label for="course" class="w-full text-left md:text-right block text-sm">{{ __('admin.certificates.edit.form.program') }}</label>
                </div>
                <div class="w-full md:w-3/4">
                    <select wire:model="course"
                            class="w-full md:w-3/4 px-3 py-1 border rounded focus:border-blue-500 hover:border-blue-500 text-sm outline-none focus:input-shadow">
                        <option value>{{ __('admin.certificates.edit.form.chooseprogram') }}</option>
                        @foreach($yearItem->courses ?? [] as $courseItem)
                            <option value="{{ $courseItem->id }}">{{ $courseItem->ru_title }}</option>
                        @endforeach
                    </select>
                    @error('course')
                    <p class="text-xs text-red-600">{{ $message }}</p>
                    @enderror
                </div>
            </div>

            <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-center">
                <div class="w-full md:w-1/4">
                    <label for="background" class="w-full text-left md:text-right block text-sm">{{ __('admin.certificates.edit.form.bg') }}</label>
                </div>
                <div class="w-full md:w-3/4">
                    @if($template->background)
                        <img src="{{ $template->backgroundUrl }}" height="160" width="90"/>
                    @endif
                    <input wire:model="background" type="file"
                           class="w-full md:w-3/4 px-3 py-1 border rounded focus:border-blue-500 hover:border-blue-500 text-sm outline-none focus:input-shadow"
                    />
                        <svg wire:loading wire:target="background" role="status" class="w-8 h-8 mr-2 text-gray-200 animate-spin dark:text-gray-600 fill-blue-600" viewBox="0 0 100 101" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M100 50.5908C100 78.2051 77.6142 100.591 50 100.591C22.3858 100.591 0 78.2051 0 50.5908C0 22.9766 22.3858 0.59082 50 0.59082C77.6142 0.59082 100 22.9766 100 50.5908ZM9.08144 50.5908C9.08144 73.1895 27.4013 91.5094 50 91.5094C72.5987 91.5094 90.9186 73.1895 90.9186 50.5908C90.9186 27.9921 72.5987 9.67226 50 9.67226C27.4013 9.67226 9.08144 27.9921 9.08144 50.5908Z" fill="currentColor"/>
                            <path d="M93.9676 39.0409C96.393 38.4038 97.8624 35.9116 97.0079 33.5539C95.2932 28.8227 92.871 24.3692 89.8167 20.348C85.8452 15.1192 80.8826 10.7238 75.2124 7.41289C69.5422 4.10194 63.2754 1.94025 56.7698 1.05124C51.7666 0.367541 46.6976 0.446843 41.7345 1.27873C39.2613 1.69328 37.813 4.19778 38.4501 6.62326C39.0873 9.04874 41.5694 10.4717 44.0505 10.1071C47.8511 9.54855 51.7191 9.52689 55.5402 10.0491C60.8642 10.7766 65.9928 12.5457 70.6331 15.2552C75.2735 17.9648 79.3347 21.5619 82.5849 25.841C84.9175 28.9121 86.7997 32.2913 88.1811 35.8758C89.083 38.2158 91.5421 39.6781 93.9676 39.0409Z" fill="currentFill"/>
                        </svg>

                        @error('background')
                    <p class="text-xs text-red-600">{{ $message }}</p>
                    @enderror
                </div>
            </div>

            <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-center">
                <div class="w-full md:w-1/4">
                </div>
                <div class="w-full md:w-3/4 space-x-2 flex">
                    <button wire:click="save"
                            class="mb-2 text-sm flex justify-center items-center px-4 py-1 border-0 bg-blue-500 text-white rounded hover:bg-blue-600">
                        {{ __('admin.buttons.save') }}
                    </button>
                    <a href="{{ route('admin.certificates-templates.index') }}"
                       class="py-1 px-4 border inline-block m-0 mb-2 text-sm rounded hover:border-black">{{ __('admin.buttons.cancel') }}</a>
                </div>
            </div>
        </div>
    </div>
</div>
