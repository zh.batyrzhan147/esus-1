<div class="w-full flex justify-between space-x-4">
    <div class="flex flex-col mb-4 w-full">
        <label class="flex flex-col text-left" for="iinfio">
            <span class="text-gray-700 mb-2">{{ __('Поиск по ID') }}</span>
        </label>
        <input type="text" wire:keydown.enter="applyFilter" wire:model.defer="uuid" id="uuid"
               class="w-full px-3 py-1 border rounded focus:border-blue-500 hover:border-blue-500 text-sm outline-none focus:input-shadow"
               placeholder="{{ __('Введите ID') }}"
        />
    </div>
    <div class="flex flex-col mb-4 w-full">
        <label class="flex flex-col text-left" for="iinfio">
            <span class="text-gray-700 mb-2">{{ __('Поиск по ФИО') }}</span>
        </label>
        <input type="text" wire:keydown.enter="applyFilter" wire:model.defer="fio" id="fio"
               class="w-full px-3 py-1 border rounded focus:border-blue-500 hover:border-blue-500 text-sm outline-none focus:input-shadow"
               placeholder="{{ __('Введите ФИО слушателя') }}"
        />
    </div>
    <div class="flex flex-col mb-4 w-full">
        <label class="flex flex-col text-left" for="iinfio">
            <span class="text-gray-700 mb-2">{{ __('Поиск по шифру группы') }}</span>
        </label>
        <input type="text" wire:keydown.enter="applyFilter" wire:model.defer="group" id="group"
               class="w-full px-3 py-1 border rounded focus:border-blue-500 hover:border-blue-500 text-sm outline-none focus:input-shadow"
               placeholder="{{ __('Введите шифр группы') }}"
        />
    </div>
    <div class="flex space-x-4">
        <div class="flex justify-between items-center mb-4">
            <label class="flex flex-col text-left">
                <span class="text-gray-700 mb-2">{{ __('Тип шаблона') }}</span>
                <select wire:model.defer="type"
                        class="w-48 px-3 py-1 border rounded focus:border-blue-500 hover:border-blue-500 text-sm outline-none focus:input-shadow">
                    <option value>{{ __('Все типы') }}</option>
                    @foreach(\App\Models\CertificatesTemplates::listTypes() as $key => $item)
                        <option value="{{ $key }}">{{ $item }}</option>
                    @endforeach
                </select>
            </label>
        </div>
    </div>
</div>
<div class="flex justify-end">
    <button wire:click="applyFilter"
            class="text-sm flex justify-center items-center space-x-1.5 px-4 py-1 border-0 bg-blue-500 text-white rounded hover:bg-blue-600">
        <span>{{ __('admin.buttons.accept.filters') }}</span>
    </button>
</div>
