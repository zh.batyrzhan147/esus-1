<div>
    <x-slot name="header"></x-slot>
    <div class="bg-white mx-auto py-6 px-4 sm:px-6 lg:px-8">
        <div class="w-full flex justify-between items-center">
            <h2 class="font-semibold text-xl text-gray-800 leading-tight">
                Загрузка Локации
            </h2>

            <div class="flex space-x-4">
                @include('admin.common.secondmenu', [
                    'tabname'=>'courses',
                    'popup'=>true,
                ])
                <button wire:click="add_new"
                        class="text-sm flex justify-center items-center space-x-1.5 px-4 py-1 border-0 bg-blue-500 text-white rounded hover:bg-blue-600">
                    <svg width="14" height="14" viewBox="0 0 14 14" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path
                            d="M7.71838 1.62408H6.78088C6.69755 1.62408 6.65588 1.66575 6.65588 1.74908V6.65533H2C1.91667 6.65533 1.875 6.697 1.875 6.78033V7.71783C1.875 7.80117 1.91667 7.84283 2 7.84283H6.65588V12.7491C6.65588 12.8324 6.69755 12.8741 6.78088 12.8741H7.71838C7.80172 12.8741 7.84338 12.8324 7.84338 12.7491V7.84283H12.5C12.5833 7.84283 12.625 7.80117 12.625 7.71783V6.78033C12.625 6.697 12.5833 6.65533 12.5 6.65533H7.84338V1.74908C7.84338 1.66575 7.80172 1.62408 7.71838 1.62408Z"
                            fill="white"/>
                    </svg>
                    <span>Добавить</span>
                </button>
            </div>
        </div>
    </div>

    <div class="py-6 px-4 sm:px-6 lg:px-8">
        <div class="flex flex-col">
            <div class="-my-2 overflow-x-auto sm:-mx-6 lg:-mx-8">
                <div class="py-2 align-middle inline-block min-w-full sm:px-6 lg:px-8">
                    <div class="shadow overflow-hidden border-b border-gray-200 sm:rounded">
                        <table class="min-w-full divide-y divide-gray-200 w-full">
                            <thead>
                            <tr>
                                <th scope="col"
                                    class="px-2 py-3 bg-gray-50 text-left text-sm font-medium text-gray-800">
                                    <div class="text-center">{{ __('admin.table.head.id') }}</div>
                                </th>
                                <th scope="col"
                                    class="px-2 py-3 bg-gray-50 text-left text-sm font-medium text-gray-800">
                                    <div class="pl-4 border-l border-gray-200">Модель</div>
                                </th>
                                <th scope="col"
                                    class="px-2 py-3 bg-gray-50 text-left text-sm font-medium text-gray-800">
                                    <div class="pl-4 border-l border-gray-200">Статус</div>
                                </th>
                                <th scope="col"
                                    class="px-2 py-3 bg-gray-50 text-left text-sm font-medium text-gray-800">
                                    <div class="pl-4 border-l border-gray-200">Дата</div>
                                </th>
                                <th scope="col" width="200"
                                    class="px-2 py-3 bg-gray-50 text-left text-sm font-medium text-gray-800">
                                    <div class="pl-4 border-l border-gray-200">{{ __('admin.table.head.action') }}</div>
                                </th>
                            </tr>
                            </thead>
                            <tbody class="bg-white divide-y divide-gray-200">
                            @foreach ($imports as $item)
                                <tr>
                                    <td class="px-6 py-1.5 whitespace-nowrap text-sm text-gray-900">
                                        {{ $item->id }}
                                    </td>

                                    <td class="px-6 py-1.5 whitespace-nowrap text-sm text-gray-900">
                                        {{ \App\Models\XlsImports::listModels()[$item->model]  }}
                                    </td>

                                    <td class="px-6 py-1.5 whitespace-nowrap text-sm text-gray-900">
                                        {{ \App\Models\XlsImports::listStatuses()[$item->status] }}
                                    </td>

                                    <td class="px-6 py-1.5 whitespace-nowrap text-sm text-gray-900">
                                        {{ date('d.m.Y H:i', strtotime($item->created_at)) }}
                                    </td>

                                    <td class="px-6 py-1.5 whitespace-nowrap text-sm font-medium space-x-2">
                                        {{--                                        <a href="{{ route('admin.groups.edit', $group->id) }}"--}}
                                        {{--                                           class="text-blue-500 hover:text-blue-900 m-0 font-normal">{{ __('admin.links.edit') }}</a>--}}
                                        {{--                                        <button wire:click="$emit('triggerDelete',{{ $group->id }})"--}}
                                        {{--                                                class="text-red-500 hover:text-red-900 bg-none border-0 m-0 cursor-pointer bg-transparent font-normal ">Удалить</button>--}}
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="mt-4">
                        {{ $imports->appends(request()->query())->links() }}
                    </div>
                </div>
            </div>

        </div>
    </div>
    @if($modal_add_new)
        <x-modal>
            <x-slot name="header">
                Загрузть файл:
                <br><br>
            </x-slot>
            <div class="space-y-2">
                <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-center">
                    <div class="w-full md:w-1/4">
                        <label for="file" class="w-full text-left md:text-right block text-sm">Файл:</label>
                    </div>
                    <div class="w-full md:w-3/4">
                        <input wire:model="file" type="file" id="file"
                               class="w-full md:w-3/4 px-3 py-1 border rounded focus:border-blue-500 hover:border-blue-500 text-sm outline-none focus:input-shadow"/>
                        <svg wire:loading wire:target="file" role="status" class="w-8 h-8 mr-2 text-gray-200 animate-spin dark:text-gray-600 fill-blue-600" viewBox="0 0 100 101" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M100 50.5908C100 78.2051 77.6142 100.591 50 100.591C22.3858 100.591 0 78.2051 0 50.5908C0 22.9766 22.3858 0.59082 50 0.59082C77.6142 0.59082 100 22.9766 100 50.5908ZM9.08144 50.5908C9.08144 73.1895 27.4013 91.5094 50 91.5094C72.5987 91.5094 90.9186 73.1895 90.9186 50.5908C90.9186 27.9921 72.5987 9.67226 50 9.67226C27.4013 9.67226 9.08144 27.9921 9.08144 50.5908Z" fill="currentColor"/>
                            <path d="M93.9676 39.0409C96.393 38.4038 97.8624 35.9116 97.0079 33.5539C95.2932 28.8227 92.871 24.3692 89.8167 20.348C85.8452 15.1192 80.8826 10.7238 75.2124 7.41289C69.5422 4.10194 63.2754 1.94025 56.7698 1.05124C51.7666 0.367541 46.6976 0.446843 41.7345 1.27873C39.2613 1.69328 37.813 4.19778 38.4501 6.62326C39.0873 9.04874 41.5694 10.4717 44.0505 10.1071C47.8511 9.54855 51.7191 9.52689 55.5402 10.0491C60.8642 10.7766 65.9928 12.5457 70.6331 15.2552C75.2735 17.9648 79.3347 21.5619 82.5849 25.841C84.9175 28.9121 86.7997 32.2913 88.1811 35.8758C89.083 38.2158 91.5421 39.6781 93.9676 39.0409Z" fill="currentFill"/>
                        </svg>

                        @error('file')
                        <p class="text-xs text-red-600">{{ $message }}</p>
                        @enderror
                    </div>
                </div>
                <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-center">
                    <div class="w-full md:w-1/4">
                    </div>
                    <div class="w-full md:w-3/4 space-x-2 flex">
                        <button wire:click="add_file"
                                class="mb-2 text-sm flex justify-center items-center px-4 py-1 border-0 bg-blue-500 text-white rounded hover:bg-blue-600">
                            Добавить
                        </button>
                        <button wire:click="$set('modal_add_new',false)"
                                class="py-1 px-4 border inline-block m-0 mb-2 text-sm rounded hover:border-black">
                            Отмена
                        </button>
                    </div>
                </div>
            </div>
        </x-modal>
    @endif
</div>
