<div>
    <x-slot name="header">
        <div class="bg-white mx-auto py-6 px-4 sm:px-6 lg:px-8">
            <h2 class="font-semibold text-xl text-gray-800 leading-tight">
                Генерация расписаний
            </h2>
            <style>

                table {
                    text-align: left;
                    position: relative;
                    border-collapse: collapse;
                }

                th {
                    background: white;
                    position: sticky;
                    top: 0; /* Don't forget this, required for the stickiness */
                    box-shadow: 0 2px 2px -1px rgba(0, 0, 0, 0.4);
                }

            </style>
        </div>
    </x-slot>
    @include('admin.common.secondmenu', [
        'tabname'=>'groups',
        'popup'=>true,
    ])
    <div wire:loading.delay
         class="min-w-screen h-screen animated fadeIn faster fixed left-0 top-0 flex justify-center items-center inset-0 z-50 outline-none focus:outline-none bg-no-repeat bg-center bg-cover">
        <div class="absolute bg-black opacity-80 inset-0 z-0"></div>
        <div class="text-center w-full h-full flex items-center justify-center p-5 relative mx-auto my-auto rounded-xl shadow-lg text-white text-3xl font-bold">Loading...</div>
    </div>
    <div class="flex flex-col items-center py-6 px-4 sm:px-6 lg:px-8">
        <span class="text-3xl font-bold mb-8">Шаг {{ $step }}</span>
        @if($step == 1)
            <div>
                <div class="flex justify-between items-center mb-8">
                    <label class="flex flex-col text-left w-96">
                        <span class="text-gray-700">Выберите год</span>
                        <select wire:model="year"
                                class="w-full px-3 py-1 border rounded focus:border-blue-500 hover:border-blue-500 text-sm outline-none focus:input-shadow">
                            @foreach(\App\Models\Years::orderBy('year','ASC')->get() as $item)
                                <option value="{{ $item->year }}">{{ $item->year }}</option>
                            @endforeach
                        </select>
                    </label>
                </div>
                <div class="flex flex-col w-full">
                    <div class="w-full space-x-2 flex justify-center">
                        <button wire:click="nextStep({{ $step }})"
                                class="mb-2 text-sm flex justify-center items-center px-4 py-1 border-0 bg-blue-500 text-white rounded hover:bg-blue-600">
                            Слудующий шаг ({{ $step + 1 }})
                        </button>
                        <a href="{{ route('admin.schedule.index') }}"
                           class="py-1 px-4 border inline-block m-0 mb-2 text-sm rounded hover:border-black">Отмена</a>
                    </div>
                </div>
            </div>
        @endif

        @if(($step == 2 ) )
            <div>
                <div class="flex flex-col">
                    <table>
                        <thead>
                        <tr>
                            <th scope="col"
                                class="px-2 py-3 bg-gray-50 text-left text-sm font-medium text-gray-800">
                                <div class="pl-4 border-l border-gray-200"></div>
                            </th>

                            @foreach($languages as $lang)
                                <th scope="col"
                                    class="px-2 py-3 bg-gray-50 text-left text-sm font-medium text-gray-800">
                                    <div class="pl-4 border-l border-gray-200">Количество занятых
                                        грантов {{ $lang->title }}</div>
                                </th>

                                <th scope="col"
                                    class="px-2 py-3 bg-gray-50 text-left text-sm font-medium text-gray-800">
                                    <div class="pl-4 border-l border-gray-200">Количество не распределенных
                                        грантов {{ $lang->title }}</div>
                                </th>

                                <th scope="col"
                                    class="px-2 py-3 bg-gray-50 text-left text-sm font-medium text-gray-800">
                                    <div class="pl-4 border-l border-gray-200">Количество групп {{ $lang->title }}</div>
                                </th>

                                <th scope="col"
                                    class="px-2 py-3 bg-gray-50 text-left text-sm font-medium text-gray-800">
                                    <div class="pl-4 border-l border-gray-200">Количество слушателей в
                                        группе {{ $lang->title }}</div>
                                </th>
                            @endforeach

                        </tr>
                        </thead>
                        <tbody class="bg-white divide-y divide-gray-200">
                        @foreach($grantsByCourses as $courses)
                            <tr>
                                <td colspan="100%" class="px-6 py-1.5 text-sm text-gray-900 font-bold py-6 bg-gray-100">
                                    <div class="pl-4">{{ $courses['title'] }}</div>
                                </td>
                            </tr>
                            @foreach($courses['items'] as $affiliate)
                                <tr>
                                    <td class="px-6 py-1.5 text-sm text-gray-900">
                                        <div class="pl-4">{{ $affiliate['title'] }}</div>
                                    </td>
                                    @foreach($languages as $lang)
                                        <td class="px-6 py-1.5 whitespace-nowrap text-sm text-gray-900 ">
                                            <div class="pl-4">{{ $affiliate[$lang->slug]['busyCount'] }}</div>
                                        </td>
                                        <td class="px-6 py-1.5 whitespace-nowrap text-sm text-gray-900 ">
                                            <div class="pl-4">{{ $affiliate[$lang->slug]['count'] }}</div>
                                        </td>
                                        <td class="px-6 py-1.5 whitespace-nowrap text-sm text-gray-900 ">
                                            <div class="pl-4">{{ $affiliate[$lang->slug]['groupsCount'] }}</div>
                                        </td>
                                        <td class="px-6 py-1.5 whitespace-nowrap text-sm text-gray-900 ">
                                            <div class="pl-4">{{ $affiliate[$lang->slug]['countPerGroup'] }}</div>
                                        </td>
                                    @endforeach
                                </tr>
                            @endforeach
                        @endforeach
                        {{--                                    <tr>--}}
                        {{--                                        <td class="px-6 py-1.5 whitespace-nowrap text-sm text-gray-900 font-bold">--}}
                        {{--                                            Общее количество--}}
                        {{--                                        </td>--}}
                        {{--                                    </tr>--}}
                        </tbody>
                    </table>
                </div>
                {{--                @dd($grantsByCourses)--}}
                <div class="flex flex-col">
                    <div class="w-full space-x-2 flex justify-center">
                        <button wire:click="nextStep({{ $step }})"
                                class="mb-2 text-sm flex justify-center items-center px-4 py-1 border-0 bg-blue-500 text-white rounded hover:bg-blue-600">
                            Слудующий шаг ({{ $step + 1 }})
                        </button>
                        <button wire:click="prevStep({{ $step }})"
                                class="py-1 px-4 border inline-block m-0 mb-2 text-sm rounded hover:border-black">
                            Предыдущий шаг ({{ $step - 1 }})
                        </button>
                    </div>
                </div>
            </div>
        @endif

        @if(($step == 3) or ($step == 4))
            <div>
                @if($step == 3)
                    Groups added: {{ $groups->count() }}
                @endif

                @if($step == 4)
                    Groups affected: {{$groupsAffected}}/{{ $groups->count() }}
                @endif

                <div class="flex flex-col">
                    <table>
                        <thead>
                        <tr>
                            <th scope="col"
                                class="px-2 py-3 bg-gray-50 text-left text-sm font-medium text-gray-800">
                                <div class="pl-4 border-l border-gray-200">ФИО</div>
                            </th>

                            <th scope="col"
                                class="px-2 py-3 bg-gray-50 text-left text-sm font-medium text-gray-800">
                                <div class="pl-4 border-l border-gray-200">Текущая нагрузка</div>
                            </th>


                            {{--                            @foreach($languages as $lang)--}}
                            {{--                                <th scope="col"--}}
                            {{--                                    class="px-2 py-3 bg-gray-50 text-left text-sm font-medium text-gray-800">--}}
                            {{--                                    <div class="pl-4 border-l border-gray-200">Количество занятых--}}
                            {{--                                        грантов {{ $lang->title }}</div>--}}
                            {{--                                </th>--}}

                            {{--                                <th scope="col"--}}
                            {{--                                    class="px-2 py-3 bg-gray-50 text-left text-sm font-medium text-gray-800">--}}
                            {{--                                    <div class="pl-4 border-l border-gray-200">Количество не распределенных--}}
                            {{--                                        грантов {{ $lang->title }}</div>--}}
                            {{--                                </th>--}}

                            {{--                                <th scope="col"--}}
                            {{--                                    class="px-2 py-3 bg-gray-50 text-left text-sm font-medium text-gray-800">--}}
                            {{--                                    <div class="pl-4 border-l border-gray-200">Количество групп {{ $lang->title }}</div>--}}
                            {{--                                </th>--}}

                            {{--                                <th scope="col"--}}
                            {{--                                    class="px-2 py-3 bg-gray-50 text-left text-sm font-medium text-gray-800">--}}
                            {{--                                    <div class="pl-4 border-l border-gray-200">Количество слушателей в--}}
                            {{--                                        группе {{ $lang->title }}</div>--}}
                            {{--                                </th>--}}
                            {{--                            @endforeach--}}

                        </tr>
                        </thead>
                        <tbody class="bg-white divide-y divide-gray-200">
                        @foreach($grantsByCourses as $courses)
                            <tr>
                                <td colspan="100%" class="px-6 py-1.5 text-sm text-gray-900 font-bold py-6 bg-gray-200">
                                    <div class="pl-4">{{ $courses['title'] }}</div>
                                </td>
                            </tr>
                            @foreach($courses['items'] as $affiliate)
                                <tr>
                                    <td colspan="100%"
                                        class="px-6 py-1.5 text-sm text-gray-900 font-bold py-6 bg-gray-100">
                                        <div class="pl-4">{{ $affiliate['title'] }}</div>
                                    </td>
                                </tr>
                                @foreach($languages as $lang)
                                    <tr>
                                        <td colspan="100%"
                                            class="px-6 py-1.5 text-sm text-gray-600 py-6 bg-gray-100">
                                            <div class="pl-4">{{ $lang->title }}</div>
                                        </td>
                                    </tr>
                                    @foreach($affiliate[$lang->slug]['tainers'] as $tainers)
                                        <tr>
                                            <td class="px-6 py-1.5 whitespace-nowrap text-sm text-gray-900 ">
                                                <div class="pl-4">{{ $tainers->fio }}</div>
                                            </td>
                                            <td class="px-6 py-1.5 whitespace-nowrap text-sm text-gray-900 ">
                                                <div class="pl-4">{{ $tainers->hours_count }}</div>
                                            </td>
                                        </tr>
                                    @endforeach
                                @endforeach
                            @endforeach
                        @endforeach
                        {{--                                    <tr>--}}
                        {{--                                        <td class="px-6 py-1.5 whitespace-nowrap text-sm text-gray-900 font-bold">--}}
                        {{--                                            Общее количество--}}
                        {{--                                        </td>--}}
                        {{--                                    </tr>--}}
                        </tbody>
                    </table>
                </div>


                <div class="flex flex-col">
                    <div class="w-full space-x-2 flex justify-center">
                        <button wire:click="nextStep({{ $step }})"
                                class="mb-2 text-sm flex justify-center items-center px-4 py-1 border-0 bg-blue-500 text-white rounded hover:bg-blue-600">
                            Слудующий шаг ({{ $step + 1 }})
                        </button>
                        <button wire:click="prevStep({{ $step }})"
                                class="py-1 px-4 border inline-block m-0 mb-2 text-sm rounded hover:border-black">
                            Предыдущий шаг ({{ $step - 1 }})
                        </button>
                    </div>
                </div>
            </div>
        @endif

        @if(($step == 5))
            <div>
                <div class="flex flex-col">
                    <table>
                        <thead>
                        <tr>
                            <th scope="col"
                                class="px-2 py-3 bg-gray-50 text-left text-sm font-medium text-gray-800">
                                <div class="pl-4 border-l border-gray-200"></div>
                            </th>

                            @foreach($languages as $lang)
{{--                                <th scope="col"--}}
{{--                                    class="px-2 py-3 bg-gray-50 text-left text-sm font-medium text-gray-800">--}}
{{--                                    <div class="pl-4 border-l border-gray-200">Количество занятых--}}
{{--                                        грантов {{ $lang->title }}</div>--}}
{{--                                </th>--}}

                                <th scope="col"
                                    class="px-2 py-3 bg-gray-50 text-left text-sm font-medium text-gray-800">
                                    <div class="pl-4 border-l border-gray-200">Количество не распределенных
                                        грантов {{ $lang->title }}</div>
                                </th>

                                <th scope="col"
                                    class="px-2 py-3 bg-gray-50 text-left text-sm font-medium text-gray-800">
                                    <div class="pl-4 border-l border-gray-200">Количество групп {{ $lang->title }}</div>
                                </th>

{{--                                <th scope="col"--}}
{{--                                    class="px-2 py-3 bg-gray-50 text-left text-sm font-medium text-gray-800">--}}
{{--                                    <div class="pl-4 border-l border-gray-200">Количество слушателей в--}}
{{--                                        группе {{ $lang->title }}</div>--}}
{{--                                </th>--}}
                            @endforeach

                        </tr>
                        </thead>
                        <tbody class="bg-white divide-y divide-gray-200">
                        @foreach($grantsByCourses as $courses)
                            <tr>
                                <td colspan="100%" class="px-6 py-1.5 text-sm text-gray-900 font-bold py-6 bg-gray-100">
                                    <div class="pl-4">{{ $courses['title'] }}</div>
                                </td>
                            </tr>
                            @foreach($courses['items'] as $affiliate)
                                <tr>
                                    <td class="px-6 py-1.5 text-sm text-gray-900">
                                        <div class="pl-4">{{ $affiliate['title'] }}</div>
                                    </td>
                                    @foreach($languages as $lang)
{{--                                        <td class="px-6 py-1.5 whitespace-nowrap text-sm text-gray-900 ">--}}
{{--                                            <div class="pl-4">{{ $affiliate[$lang->slug]['busyCount'] ?? 0 }}</div>--}}
{{--                                        </td>--}}
                                        <td class="px-6 py-1.5 whitespace-nowrap text-sm text-gray-900 ">
                                            <div class="pl-4 @if(($affiliate[$lang->slug]['count'] ?? 0) > 0) font-bold @endif">{{ $affiliate[$lang->slug]['count'] ?? 0 }}</div>
                                        </td>
                                        <td class="px-6 py-1.5 whitespace-nowrap text-sm text-gray-900 ">
                                            <div class="pl-4">{{ $affiliate[$lang->slug]['groupsCount'] ?? 0}}</div>
                                        </td>
{{--                                        <td class="px-6 py-1.5 whitespace-nowrap text-sm text-gray-900 ">--}}
{{--                                            <div class="pl-4">{{ $affiliate[$lang->slug]['countPerGroup'] ?? 0}}</div>--}}
{{--                                        </td>--}}
                                    @endforeach
                                </tr>
                            @endforeach
                        @endforeach
                        {{--                                    <tr>--}}
                        {{--                                        <td class="px-6 py-1.5 whitespace-nowrap text-sm text-gray-900 font-bold">--}}
                        {{--                                            Общее количество--}}
                        {{--                                        </td>--}}
                        {{--                                    </tr>--}}
                        </tbody>
                    </table>
                </div>
                {{--                @dd($grantsByCourses)--}}
                <div class="flex flex-col">
                    <div class="w-full space-x-2 flex justify-center">
                        <button wire:click="nextStep({{ $step }})"
                                class="mb-2 text-sm flex justify-center items-center px-4 py-1 border-0 bg-blue-500 text-white rounded hover:bg-blue-600">
                            Завершить!
                        </button>
                    </div>
                </div>
            </div>
        @endif
    </div>

    {{-- Knowing others is intelligence; knowing yourself is true wisdom. --}}
</div>
