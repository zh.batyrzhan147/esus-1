<div>
    <x-slot name="header">
        <div class="bg-white mx-auto py-6 px-4 sm:px-6 lg:px-8">
            <div class="w-full flex justify-between items-center">
                <h2 class="font-semibold text-xl text-gray-800 leading-tight">
                    {{ __('admin.courses.index.title') }}
                </h2>

                @include('admin.common.secondmenu', [
                    'tabname'=>'courses',
                    'popup'=>false,
                    'addlink'=>'oop.courses.create'
                ])
            </div>
        </div>
    </x-slot>

    <div class="flex flex-col mx-auto py-2 px-4 sm:px-6 lg:px-8">
        <div class="w-full flex justify-between space-x-4">
            <div class="flex flex-col mb-4 w-full">
                <label class="flex flex-col text-left">
                    <span class="text-gray-700 mb-2">{{ __('admin.courses.index.filter.title') }}</span>
                </label>
                <input type="text" wire:keydown.enter="applyFilter" wire:model.defer="search"
                       class="w-full px-3 py-1 border rounded focus:border-blue-500 hover:border-blue-500 text-sm outline-none focus:input-shadow"
                       placeholder="{{ __('admin.courses.index.filter.title.placeholder') }}"
                />
            </div>
            <div class="flex space-x-4">
                <div class="flex justify-between items-center mb-4">
                    <label class="flex flex-col text-left">
                        <span class="text-gray-700 mb-2">{{ __('admin.courses.index.filter.type') }}</span>
                        <select wire:model.defer="type"
                                class="w-48 px-3 py-1 border rounded focus:border-blue-500 hover:border-blue-500 text-sm outline-none focus:input-shadow">
                            <option value>{{ __('admin.courses.index.filter.all') }}</option>
                            @foreach(\App\Models\Courses::listTypes() as $key => $item)
                                <option value="{{ $key }}">{{ $item }}</option>
                            @endforeach
                        </select>
                    </label>
                </div>
                <div class="flex justify-between items-center mb-4">
                    <label class="flex flex-col text-left">
                        <span class="text-gray-700 mb-2">{{ __('admin.courses.index.filter.grading') }}</span>
                        <select wire:model.defer="grading"
                                class="w-48 px-3 py-1 border rounded focus:border-blue-500 hover:border-blue-500 text-sm outline-none focus:input-shadow">
                            <option value>{{ __('admin.courses.index.filter.all') }}</option>
                            @foreach(\App\Models\Courses::listGrading() as $key => $item)
                                <option value="{{ $key }}">{{ $item }}</option>
                            @endforeach
                        </select>
                    </label>
                </div>
            </div>
        </div>
        <div class="flex justify-end">
            <button wire:click="applyFilter"
                    class="text-sm flex justify-center items-center space-x-1.5 px-4 py-1 border-0 bg-blue-500 text-white rounded hover:bg-blue-600">
                <span>{{ __('admin.buttons.accept.filters') }}</span>
            </button>
        </div>
    </div>

    <div class="py-6 px-4 sm:px-6 lg:px-8">
        <div class="flex flex-col">
            <div class="-my-2 overflow-x-auto sm:-mx-6 lg:-mx-8">
                <div class="py-2 align-middle inline-block min-w-full sm:px-6 lg:px-8">
                    <div class="shadow overflow-hidden border-b border-gray-200 sm:rounded">
                        <table class="min-w-full divide-y divide-gray-200 w-full">
                            <thead>
                            <tr>
                                <th scope="col" width="50" class="px-3 py-4 bg-gray-50 text-left text-sm font-medium text-gray-800">
                                    <div class="text-center">{{ __('admin.table.head.id') }}</div>
                                </th>
                                <th scope="col"
                                    class="px-2 py-3 bg-gray-50 text-left text-sm font-medium text-gray-800">
                                    <div class="pl-4">{{ __('admin.table.head.kztitle') }}</div>
                                </th>
                                <th scope="col"
                                    class="px-2 py-3 bg-gray-50 text-left text-sm font-medium text-gray-800">
                                    <div class="pl-4 border-l border-gray-200 whitespace-nowrap">{{ __('admin.table.head.program.type') }}</div>
                                </th>
                                <th scope="col"
                                    class="px-2 py-3 bg-gray-50 text-left text-sm font-medium text-gray-800">
                                    <div class="pl-4 border-l border-gray-200">{{ __('admin.table.head.program.grading') }}</div>
                                </th>
                                <th scope="col" width="200"
                                    class="px-2 py-3 bg-gray-50 text-left text-sm font-medium text-gray-800">
                                    <div class="pl-4 border-l border-gray-200">{{ __('admin.table.head.action') }}</div>
                                </th>
                            </tr>
                            </thead>
                            <tbody class="bg-white divide-y divide-gray-200">
                            @if (count($courses) > 0)
                                @foreach ($courses as $course)
                                    <tr>
                                        <td class="px-6 py-1.5 whitespace-nowrap text-sm text-gray-900">
                                            {{ $course->id }}
                                        </td>

                                        <td class="px-6 py-1.5 text-sm text-gray-900">
                                            {!! $course->ru_title !!}
                                        </td>

                                        <td class="px-6 py-1.5 whitespace-nowrap text-sm text-gray-900">
                                            {!! $course->typeString !!}
                                        </td>

                                        <td class="px-6 py-1.5 whitespace-nowrap text-sm text-gray-900">
                                            {!! $course->gradingString !!}
                                        </td>

                                        <td class="px-6 py-1.5 whitespace-nowrap text-sm font-medium space-x-2 text-right">
                                            <a href="{{ route('oop.courses.edit', $course->id) }}"
                                               class="text-blue-500 hover:text-blue-900 m-0 font-normal">{{ __('admin.links.edit') }}</a>
                                            {{--                                            <form class="inline-block"--}}
                                            {{--                                                  action="{{ route('admin.courses.destroy', $course->id) }}"--}}
                                            {{--                                                  method="POST" onsubmit="return confirm('Are you sure?');">--}}
                                            {{--                                                <input type="hidden" name="_method" value="DELETE">--}}
                                            {{--                                                <input type="hidden" name="_token" value="{{ csrf_token() }}">--}}
                                            {{--                                                <input type="submit"--}}
                                            {{--                                                       class="text-red-500 hover:text-red-900 bg-none border-0 m-0 cursor-pointer bg-transparent font-normal border-l border-gray-200 pl-2"--}}
                                            {{--                                                       value="{{ __('admin.links.delete') }}">--}}
                                            {{--                                            </form>--}}
                                        </td>
                                    </tr>
                                @endforeach
                            @else
                                <tr class="h-48 w-full">
                                    <td scope="col" class="h-48 w-full text-center text-xs text-gray-500" colspan="3">
                                        {{ __('admin.table.body.empty') }}
                                    </td>
                                </tr>
                            @endif
                            </tbody>
                        </table>
                    </div>
                    <div class="mt-4">
                        {{ $courses->appends(request()->query())->links() }}
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
