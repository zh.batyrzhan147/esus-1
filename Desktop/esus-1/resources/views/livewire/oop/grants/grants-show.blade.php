<div>
    <x-slot name="header">
        <div class="bg-white mx-auto py-6 px-4 sm:px-6 lg:px-8">
            <h2 class="font-semibold text-xl text-gray-800 leading-tight">
                {{ __('admin.grants.edit.title', ['program' => $grant->coursesItem->ru_title, 'year' => $year]) }}
            </h2>
        </div>
    </x-slot>
    <div class="py-6 px-4 sm:px-6 lg:px-8">
        <div class="flex flex-col space-y-4">
            <div class="flex justify-between items-center">
                <div class="flex flex-row space-x-10">
                    <div class="flex flex-col justify-end">
                        <span class="font-[500] text-[24px]">{{ __('admin.grants.edit.period') }}</span>
                    </div>
                    <div class="flex flex-col">
                        <label class="flex flex-col text-left">
                            <span class="text-gray-700 mb-2">{{ __('admin.grants.edit.start') }}</span>
                            <span>{{ $start }}</span>
                        </label>
                    </div>

                    <div class="flex flex-col">
                        <label class="flex flex-col text-left">
                            <span class="text-gray-700 mb-2">{{ __('admin.grants.edit.end') }}</span>
                            <span>{{ $end }}</span>
                        </label>
                    </div>
                </div>
            </div>
            <div class="w-full h-px bg-gray-200"></div>
            <div class="-my-2 overflow-x-auto sm:-mx-6 lg:-mx-8">
                <div class="py-2 align-middle inline-block min-w-full sm:px-6 lg:px-8">
                    <div class="shadow overflow-hidden border-b border-gray-200 sm:rounded">
                        <table class="min-w-full divide-y divide-gray-200 w-full">
                            <thead>
                            <tr>
                                <th scope="col"
                                    class="px-2 py-3 bg-gray-50 text-left text-sm font-medium text-gray-800">
                                    <div class="pl-4">{{ __('admin.table.head.affiliatetitle') }}</div>
                                </th>
                                <th scope="col"
                                    class="px-2 py-3 bg-gray-50 text-left text-sm font-medium text-gray-800">
                                    <div class="pl-4 border-l border-gray-200">{{ __('admin.table.head.count.listener') }}</div>
                                </th>
                                @foreach(\App\Models\Languages::all() as $lang)
                                    <th scope="col"
                                        class="px-2 py-3 bg-gray-50 text-left text-sm font-medium text-gray-800">
                                        <div class="pl-4 border-l border-gray-200">{{ __('admin.table.head.count.listener') }} ({{ $lang->title }})
                                        </div>
                                    </th>
                                @endforeach
                                <th scope="col" width="200"
                                    class="px-2 py-3 bg-gray-50 text-left text-sm font-medium text-gray-800">
                                    <div class="pl-4 border-l border-gray-200">{{ __('admin.table.head.action') }}</div>
                                </th>
                            </tr>
                            </thead>
                            <tbody class="bg-white divide-y divide-gray-200">
                            <tr wire:key="{{ $grant->id }}" class="bg-gray-100">
                                <td class="px-6 py-1.5 whitespace-nowrap text-xl text-gray-900 font-bold">
                                    {{ __('admin.grants.edit.table.all.count') }}
                                </td>

                                <td class="px-6 py-1.5 whitespace-nowrap text-xl text-gray-900">
                                    {{ $grant->count_all }}
                                </td>
                                @foreach(\App\Models\Languages::all() as $lang)
                                    <td class="px-6 py-1.5 whitespace-nowrap text-xl text-gray-900">
                                        {{ $grant->countsArray[$lang->slug] ?? 0 }}
                                    </td>
                                @endforeach


                                <td class="px-6 py-1.5 whitespace-nowrap text-sm font-medium space-x-2">
                                    {{--                                    <button wire:click="edit({{ $grant->id }})"--}}
                                    {{--                                            class="text-blue-500 hover:text-blue-900 m-0 font-normal">Редактировать--}}
                                    {{--                                    </button>--}}
                                </td>
                            </tr>
                            @foreach($grants as $item)
                                <tr>
                                    <td class="px-6 py-1.5 whitespace-nowrap text-sm text-gray-900 font-bold">
                                        {{ $item->affiliateItem->title }}
                                    </td>

                                    <td class="px-6 py-1.5 whitespace-nowrap text-sm text-gray-900">
                                        {{ $item->count_all }}
                                    </td>

                                    @foreach(\App\Models\Languages::all() as $lang)
                                        <td class="px-6 py-1.5 whitespace-nowrap text-sm text-gray-900">
                                            {{ $item->countsArray[$lang->slug] ?? 0 }}
                                        </td>
                                    @endforeach

                                    <td class="px-6 py-1.5 whitespace-nowrap text-sm font-medium space-x-2">
                                        {{--                                        <button wire:click="edit({{ $item->id }})"--}}
                                        {{--                                                class="text-blue-500 hover:text-blue-900 m-0 font-normal">Редактировать--}}
                                        {{--                                        </button>--}}
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-right">
                            <div class="w-full space-x-2 flex m-2 justify-end">
                                <a href="{{ route('oop.grants.index', ['year'=>$year]) }}"
                                   class="py-1 px-4 border flex m-0 text-sm rounded hover:border-black">{{ __('admin.buttons.back') }}
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>

    {{--    @if($showModalEditgrants)--}}
    {{--        @include('livewire.grants.edit_program_by_affiliate_grants')--}}
    {{--    @endif--}}

{{--    @if($grant->count_all == 0)--}}
{{--        <script>--}}
{{--            document.addEventListener('livewire:load', function () {--}}
{{--                if ({{ $grant->count_all }} === 0) {--}}
{{--                @this.edit({{ $grant->id }});--}}
{{--                }--}}
{{--            })--}}
{{--        </script>--}}
{{--    @endif--}}

</div>
{{--@push('scripts')--}}
{{--    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>--}}

{{--    <script type="text/javascript">--}}
{{--        document.addEventListener('DOMContentLoaded', function () {--}}
{{--        @this.on('saved', () => {--}}
{{--            Swal.fire({--}}
{{--                title: '{{ __("admin.swal.grants.title.success") }}',--}}
{{--                icon: 'success',--}}
{{--                position: 'top-end',--}}
{{--                width: 600,--}}
{{--                showConfirmButton: false,--}}
{{--                timer: 1000--}}
{{--            });--}}
{{--        });--}}
{{--        })--}}
{{--    </script>--}}

{{--@endpush--}}
{{--@push('styles')--}}
{{--    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/flatpickr/dist/flatpickr.min.css">--}}
{{--    <script src="https://cdn.jsdelivr.net/npm/flatpickr"></script>--}}
{{--    <script src="https://cdn.jsdelivr.net/npm/flatpickr/dist/l10n/ru.js"></script>--}}
{{--@endpush--}}
