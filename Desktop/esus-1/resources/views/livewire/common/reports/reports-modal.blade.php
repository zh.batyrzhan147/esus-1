<x-modal>
    <x-slot name="header">
        Создать отчет:
        <br><br>
    </x-slot>
    <div class="space-y-2">
        <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-center">
            <div class="w-full md:w-1/4">
                <label for="type" class="w-full text-left md:text-right block text-sm">{{ __('Тип отчета') }}</label>
            </div>
            <div class="w-full md:w-3/4">
                <select wire:model="reporttype" id="type"
                        class="w-full md:w-3/4 px-3 py-1 border rounded focus:border-blue-500 hover:border-blue-500 text-sm outline-none focus:input-shadow">
                    <option value>Выберите тип отчета</option>
                    @foreach(\App\Models\Reports::listTypes() as $key => $type)
                        <option value="{{ $key }}">{{ $type }}</option>
                    @endforeach
                </select>
            </div>
        </div>

        @if(($reporttype == \App\Models\Reports::REPORT_TYPE_LISTENER_BY_COURSES)
         or ($reporttype == \App\Models\Reports::REPORT_TYPE_PRILOZHENIE_2)
         or ($reporttype == \App\Models\Reports::REPORT_TYPE_PRILOZHENIE_1)
         or ($reporttype == \App\Models\Reports::REPORT_TYPE_FORMA_CPI)
         or ($reporttype == \App\Models\Reports::REPORT_TYPE_COURSES_RESULTS)
        )
            <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-center">
                <div class="w-full md:w-1/4">
                    <label for="starttime" class="w-full text-left md:text-right block text-sm">{{ __('Начиная с:') }}</label>
                </div>
                <div class="w-full md:w-3/4">
                    <x-inputs.date id="starttime" wire:model.defer="starttime" placeholder="{{ __('admin.weekends.modal.date.placeholder') }}" class="w-full md:w-3/4 px-3 py-1 border rounded focus:border-blue-500 hover:border-blue-500 text-sm outline-none focus:input-shadow"/>
                    @error('starttime')
                    <p class="text-xs text-red-600">{{ $message }}</p>
                    @enderror
                </div>
            </div>

            <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-center">
                <div class="w-full md:w-1/4">
                    <label for="endtime" class="w-full text-left md:text-right block text-sm">{{ __('по:') }}</label>
                </div>
                <div class="w-full md:w-3/4">
                    <x-inputs.date id="endtime" wire:model.defer="endtime" placeholder="{{ __('admin.weekends.modal.date.placeholder') }}" class="w-full md:w-3/4 px-3 py-1 border rounded focus:border-blue-500 hover:border-blue-500 text-sm outline-none focus:input-shadow"/>
                    @error('endtime')
                    <p class="text-xs text-red-600">{{ $message }}</p>
                    @enderror
                </div>
            </div>

            <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-center">
                <div class="w-full md:w-1/4">
                    <label for="year"
                           class="w-full text-left md:text-right block text-sm">{{ __('admin.groups.add.modal.year') }}</label>
                </div>
                <div class="w-full md:w-3/4">
                    <select wire:model="year"
                            class="w-full md:w-3/4 px-3 py-1 border rounded focus:border-blue-500 hover:border-blue-500 text-sm outline-none focus:input-shadow">
                        <option value>{{ __('admin.groups.add.modal.chooseyear') }}</option>
                        @foreach(\App\Models\Years::orderBy('year','ASC')->get() as $item)
                            <option value="{{ $item->year }}">{{ $item->year }}</option>
                        @endforeach
                    </select>
                    @error('year')
                    <p class="text-xs text-red-600">{{ $message }}</p>
                    @enderror
                </div>
            </div>

            <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-center">
                <div class="w-full md:w-1/4">
                    <label for="courses"
                           class="w-full text-left md:text-right block text-sm">{{ __('admin.groups.add.modal.program') }}</label>
                </div>
                <div class="w-full md:w-3/4">
                    <select wire:model="courses"
                            class="w-full md:w-3/4 px-3 py-1 border rounded focus:border-blue-500 hover:border-blue-500 text-sm outline-none focus:input-shadow">
                        <option value>{{ __('admin.groups.add.modal.chooseprogram') }}</option>
                        @foreach($yearItem->courses ?? [] as $courseItem)
                            <option value="{{ $courseItem->id }}">{{ $courseItem->ru_title }}</option>
                        @endforeach
                    </select>
                    @error('courses')
                    <p class="text-xs text-red-600">{{ $message }}</p>
                    @enderror
                </div>
            </div>

            <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-center">
                <div class="w-full md:w-1/4">
                    <label for="language"
                           class="w-full text-left md:text-right block text-sm">{{ __('admin.groups.add.modal.lang') }}</label>
                </div>
                <div class="w-full md:w-3/4">
                    <select wire:model="language"
                            class="w-full md:w-3/4 px-3 py-1 border rounded focus:border-blue-500 hover:border-blue-500 text-sm outline-none focus:input-shadow">
                        <option value="">{{ __('admin.groups.add.modal.chooselang') }}</option>
                        @foreach(\App\Models\Languages::all() as $item)
                            <option value="{{ $item->id }}">{{ $item->title }}</option>
                        @endforeach
                    </select>
                    @error('language')
                    <p class="text-xs text-red-600">{{ $message }}</p>
                    @enderror
                </div>
            </div>

        @endif
        @if($reporttype == \App\Models\Reports::REPORT_TYPE_PROGRAM_FILIAL_YEAR)
            <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-center">
                <div class="w-full md:w-1/4">
                    <label for="year"
                           class="w-full text-left md:text-right block text-sm">{{ __('admin.groups.add.modal.year') }}</label>
                </div>
                <div class="w-full md:w-3/4">
                    <select wire:model="year"
                            class="w-full md:w-3/4 px-3 py-1 border rounded focus:border-blue-500 hover:border-blue-500 text-sm outline-none focus:input-shadow">
                        <option value>{{ __('admin.groups.add.modal.chooseyear') }}</option>
                        @foreach(\App\Models\Years::orderBy('year','ASC')->get() as $item)
                            <option value="{{ $item->year }}">{{ $item->year }}</option>
                        @endforeach
                    </select>
                    @error('year')
                    <p class="text-xs text-red-600">{{ $message }}</p>
                    @enderror
                </div>
            </div>
            <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-center">
                <div class="w-full md:w-1/4">
                    <label for="courses"
                           class="w-full text-left md:text-right block text-sm">{{ __('admin.groups.add.modal.program') }}</label>
                </div>
                <div class="w-full md:w-3/4">
                    <select wire:model="courses"
                            class="w-full md:w-3/4 px-3 py-1 border rounded focus:border-blue-500 hover:border-blue-500 text-sm outline-none focus:input-shadow">
                        <option value>{{ __('admin.groups.add.modal.chooseprogram') }}</option>
                        @foreach($yearItem->courses ?? [] as $courseItem)
                            <option value="{{ $courseItem->id }}">{{ $courseItem->ru_title }}</option>
                        @endforeach
                    </select>
                    @error('courses')
                    <p class="text-xs text-red-600">{{ $message }}</p>
                    @enderror
                </div>
            </div>
        @endif

        @if($reporttype == \App\Models\Reports::REPORT_TYPE_TRAINERS_PUBS)
            <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-center">
                <div class="w-full md:w-1/4">
                    <label for="affiliate"
                           class="w-full text-left md:text-right block text-sm">{{ __('admin.groups.add.modal.affiliate') }}</label>
                </div>
                <div class="w-full md:w-3/4">
                    <select wire:model="affiliate"
                            class="w-full md:w-3/4 px-3 py-1 border rounded focus:border-blue-500 hover:border-blue-500 text-sm outline-none focus:input-shadow">
                        <option value>{{ __('admin.groups.add.modal.chooseaffiliate') }}</option>
                        @foreach(\App\Models\Affiliates::orderBy('id','ASC')->get() as $item)
                            <option value="{{ $item->id }}">{{ $item->title }}</option>
                        @endforeach
                    </select>
                    @error('affiliate')
                    <p class="text-xs text-red-600">{{ $message }}</p>
                    @enderror
                </div>
            </div>
            <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-center">
                <div class="w-full md:w-1/4">
                    <label for="language"
                           class="w-full text-left md:text-right block text-sm">{{ __('admin.groups.add.modal.lang') }}</label>
                </div>
                <div class="w-full md:w-3/4">
                    <select wire:model="language"
                            class="w-full md:w-3/4 px-3 py-1 border rounded focus:border-blue-500 hover:border-blue-500 text-sm outline-none focus:input-shadow">
                        <option value="">{{ __('admin.groups.add.modal.chooselang') }}</option>
                        @foreach(\App\Models\Languages::all() as $item)
                            <option value="{{ $item->id }}">{{ $item->title }}</option>
                        @endforeach
                    </select>
                    @error('language')
                    <p class="text-xs text-red-600">{{ $message }}</p>
                    @enderror
                </div>
            </div>

        @endif


        <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-center">
            <div class="w-full md:w-1/4">
            </div>
            <div class="w-full md:w-3/4 space-x-2 flex">
                @if($reporttype)
                    <button wire:click="add_report"
                            class="mb-2 text-sm flex justify-center items-center px-4 py-1 border-0 bg-blue-500 text-white rounded hover:bg-blue-600">
                        Добавить
                    </button>
                @endif
                <button wire:click="$set('modal_add_new',false)"
                        class="py-1 px-4 border inline-block m-0 mb-2 text-sm rounded hover:border-black">
                    Отмена
                </button>
            </div>
        </div>
    </div>
</x-modal>
