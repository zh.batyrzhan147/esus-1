<div>
    <div class="flex w-full border-b-2 border-[#F4F4F5] space-x-2 items-end mb-5">
        <div class="text-5xl font-bold text-black py-2.5 px-4">{{ __('listener.joingroup.title') }}</div>
    </div>
    <div class="flex flex-col mx-auto py-2 px-4 sm:px-6 lg:px-8">
        <div class="w-full md:w-3/4 flex flex-col mx-auto pb-10 lg:pb-0">
            @if($group)
                @if($status)
            <div class="w-full">
                <div class="w-full text-center text-2xl text-black mb-3">{{ __('listener.joingroup.dear', ['name' => Auth::user()->fio]) }}</div>
                <div class="w-full text-justify text-xl text-black mb-6">{!! __('listener.joingroup.text2', ['program' => $group->coursesItem->ru_title]) !!}</div>
            </div>
                    @else
            <div class="w-full">
                <div class="w-full text-center text-2xl text-black mb-3">{{ __('listener.joingroup.dear', ['name' => Auth::user()->fio]) }}</div>
                <div class="w-full text-justify text-xl text-black mb-6">{!! __('listener.joingroup.text', ['trainer' => $group->trainerItem->fio, 'program' => $group->checkLangTitle($group), 'start' => date('d.m.Y',strtotime($group->start_time)), 'end' => date('d.m.Y',strtotime($group->end_time))]) !!}</div>
                <div class="flex w-full justify-between items-end mb-5 space-x-1 px-4">
                    <h2 class="text-2xl font-bold text-center text-black">{{ $group->checkLangTitle($group) }}</h2>
                </div>
                <div class="rounded w-full bg-[#FFFFFF] px-4 py-4 mb-5">
                    <div class="w-full grid gap-x-[20px] gap-y-[40px] grid-cols-2">
                        <div class="w-full">
                            <div class="text-[14px] text-[#3F3F46] font-medium m-0 p-0 mb-1">{{ __('listener.group.group') }}</div>
                            <div class="text-[14px] text-[#000000] font-normal m-0 p-0">{{ $group->slug }}</div>
                        </div>
                        <div class="w-full">
                            <div class="text-[14px] text-[#3F3F46] font-medium m-0 p-0 mb-1">{{ __('listener.group.city') }}</div>
                            <div class="text-[14px] text-[#000000] font-normal m-0 p-0">{{ $group->affiliateItem->title }}</div>
                        </div>
                        <div class="w-full">
                            <div class="text-[14px] text-[#3F3F46] font-medium m-0 p-0 mb-1">{{ __('listener.group.trainer') }}</div>
                            <div class="text-[14px] text-[#000000] font-normal m-0 p-0">{{ $group->trainerItem->fio }}</div>
                        </div>
                        <div class="w-full">
                            <div class="text-[14px] text-[#3F3F46] font-medium m-0 p-0 mb-1">{{ __('listener.group.period') }}</div>
                            <div class="text-[14px] text-[#000000] font-normal m-0 p-0">{{ date('d.m.Y',strtotime($group->start_time)) }} - {{ date('d.m.Y',strtotime($group->end_time)) }}</div>
                        </div>
                        <div class="w-full">
                            <div class="text-[14px] text-[#3F3F46] font-medium m-0 p-0 mb-1">{{ __('listener.group.count.all') }}</div>
                            <div class="text-[14px] text-[#000000] font-normal m-0 p-0">{{ $group->count_all }}</div>
                        </div>
                        <div class="w-full">
                            <div class="text-[14px] text-[#3F3F46] font-medium m-0 p-0 mb-1">{{ __('listener.group.count.busy') }}</div>
                            <div class="text-[14px] text-[#000000] font-normal m-0 p-0">{{ $group->users()->count() }}</div>
                        </div>

                         
                        <div class="w-full">
{{--                            <div class="text-[14px] text-[#3F3F46] font-medium m-0 p-0 mb-1">{{ __('listener.group.offer') }}</div>--}}
{{--                            <div class="text-[14px] text-[#000000] font-normal m-0 p-0">{{ $group->users()->count() }}</div>--}}
                            <div class="flex">
                                <div class="flex items-center h-5">
                                    <input wire:model="agreeoffer" id="helper-checkbox" aria-describedby="helper-checkbox-text" required type="checkbox" value="" class="w-4 h-4 text-blue-600 bg-gray-100 rounded border-gray-300 focus:ring-blue-500 dark:focus:ring-blue-600 dark:ring-offset-gray-800 focus:ring-2 dark:bg-gray-700 dark:border-gray-600">
                                </div>
                                <div class="ml-2 text-sm">
                                    <label for="helper-checkbox" class="font-medium text-gray-900 dark:text-gray-300">{{ __('user.join.offer.agreement.start') }}<a class="text-blue-500 underline hover:text-blue-700" href="{{$group->getFileUrl($group->offer_agreement)}}" download="{{$group->slug}}">{{ __('user.join.offer.agreement.end') }}</a></label>
                                    @error('agreeoffer')
                                    <p class="text-xs text-red-600">{{ $message }}</p>
                                    @enderror
                                </div>

                                
                                       
                            </div>
                        </div>
                        @if($group->coursesItem->price != 0)
                        <div class="w-full">
                            <div class="text-[14px] text-[#3F3F46] font-medium m-0 p-0 mb-1">{{ __('Бағасы:') }}</div>
                            <div class="text-[14px] text-[#000000] font-normal m-0 p-0">{{ $group->coursesItem->price }} {{  $group->coursesItem->currency }}</div> 
    
                        </div>
                        @endif
                    </div>
                </div>
                <div class="w-full flex justify-center items-center mb-8">
                    @if($group->paid)
                        @if($order)
                            <form action="https://kaspi.kz/online" method="post">
                                <input type="hidden" name="TranId" value="{{ $order->id }}"/>
                                <input type="hidden" name="OrderId" value="{{ $order->id }}"/>
                                <input type="hidden" name="Amount" value="{{ $order->total * 100 }}"/>
                                <input type="hidden" name="Service" value="CenPedMas"/>
                                @if($order->isTargetGroups())
                                    <input type="hidden" name="returnUrl" value="{{ URL::route('user.group.view', ['slug' => $order->target->slug]) }}"/>
                                @else
                                    <input type="hidden" name="returnUrl" value="{{ URL::current() }}"/>
                                @endif
                                <input type="hidden" name="Signature" value=""/>
                                <input type="submit" class="mt-5 font-medium text-[#FFFFFF] px-8 py-2.5 rounded bg-green-500 hover:bg-green-700" value="Перейти к оплате"/>
                            </form>
                        @else
                            <button wire:click="makeOrder"
                                    class="whitespace-nowrap text-xl flex justify-center items-center px-10 py-4 border-0 bg-blue-500 text-white rounded hover:bg-blue-600">{{ __('user.join.to.group') }}</button>
                        @endif
                    @else
                        <button wire:click="joinGroup"
                                class="whitespace-nowrap text-xl flex justify-center items-center px-10 py-4 border-0 bg-blue-500 text-white rounded hover:bg-blue-600">{{ __('user.join.to.group') }}</button>
                    @endif
                </div>
            </div>
                @endif
            @else
            <div class="rounded w-full bg-[#FFFFFF] px-4 py-4 mb-5">
                <div class="w-full h-48 flex justify-center items-center">
                    <span>{{ __('listener.joingroup.notfound') }}</span>
                </div>
            </div>
            @endif
        </div>
    </div>
</div>
