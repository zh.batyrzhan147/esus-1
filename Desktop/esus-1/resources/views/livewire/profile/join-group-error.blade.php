<x-auth-layout>
    <div class="flex flex-col w-full p-4 lg:py-20 justify-center items-center">
        <h2 class="text-4xl font-bold mb-6">{{ __('user.welcome') }}</h2>
        <h3 class="text-3xl font-bold mb-6">{{ __('esus.title') }}</h3>
        <div class="text-[17px] text-[#4C4C4C] text-center mb-4">
            <p>{{ __('user.group.join') }}</p>
            <p>{{ __('user.group.complete.register') }}</p>
        </div>

        <div class="w-full lg:w-[460px] mx-auto flex flex-col space-y-8">
            @csrf
            <div class="flex flex-col space-y-4">
                <a href="{{ route('login') }}">
                    <button class="w-full py-6 px-7 border border-[#0077CC] rounded text-[#FBFCFE] text-[18px] bg-gradient-to-b from-[#569DE5] to-[#2C73C5] hover:from-[#2C73C5] hover:to-[#569DE5]">{{ __('user.group.login.accout') }}</button>
                </a>
                <a href="{{ route('register') }}" class="w-full block py-6 px-7 border-2 border-[#000000] rounded text-[#000000] text-[18px] text-center hover:bg-[#000000] hover:text-white">{{ __('user.register') }}</a>
            </div>
        </div>
    </div>
</x-auth-layout>
