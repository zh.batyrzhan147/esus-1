<div class="px-4 py-8 lg:px-14 lg:py-12 bg-white w-full rounded-lg">
    <div class="w-full flex flex-col">
        <div class="w-full flex lg:justify-between items-start mb-7 lg:mb-9">
            <div class="text-3xl lg:text-4xl font-bold">{{ __('listener.profile.hello') }}{{ Auth::user()->name ?? 'пользователь' }}!</div>
            <form action="{{ route('logout') }}" class="hidden lg:flex" method="POST">
                @csrf
                <button type="submit" class="flex px-8 py-4 border-2 rounded-lg border-soft-gray bg-white text-main-black text-base font-normal items-center space-x-2 hover:border-main-black">
                    <svg width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M14.5333 12.8083C14.375 12.8083 14.2167 12.75 14.0917 12.625C13.85 12.3833 13.85 11.9833 14.0917 11.7417L15.7833 10.05L14.0917 8.35834C13.85 8.11668 13.85 7.71668 14.0917 7.47501C14.3333 7.23335 14.7333 7.23335 14.975 7.47501L17.1083 9.60834C17.35 9.85001 17.35 10.25 17.1083 10.4917L14.975 12.625C14.85 12.75 14.6917 12.8083 14.5333 12.8083Z" fill="currentColor"/>
                        <path d="M16.6083 10.675H8.13333C7.79166 10.675 7.50833 10.3917 7.50833 10.05C7.50833 9.70832 7.79166 9.42499 8.13333 9.42499H16.6083C16.95 9.42499 17.2333 9.70832 17.2333 10.05C17.2333 10.3917 16.95 10.675 16.6083 10.675Z" fill="currentColor"/>
                        <path d="M9.8 17.2917C5.50833 17.2917 2.50833 14.2917 2.50833 10C2.50833 5.70834 5.50833 2.70834 9.8 2.70834C10.1417 2.70834 10.425 2.99168 10.425 3.33334C10.425 3.67501 10.1417 3.95834 9.8 3.95834C6.24166 3.95834 3.75833 6.44168 3.75833 10C3.75833 13.5583 6.24166 16.0417 9.8 16.0417C10.1417 16.0417 10.425 16.325 10.425 16.6667C10.425 17.0083 10.1417 17.2917 9.8 17.2917Z" fill="currentColor"/>
                    </svg>
                    <span>
                    {{ __('listener.profile.logout') }}
                    </span>
                </button>
            </form>
        </div>

        <div class="flex lg:hidden mb-7 lg:mb-0">
            <livewire:set-locale/>
        </div>
        <div class="w-full bg-white lg:bg-gray-bg p-0 lg:p-6 rounded-lg mb-8 lg:mb-6">
            <div class="w-full text-lg lg:text-xl font-bold mb-5">{{ __('listener.profile.general') }}</div>
            <div class="w-full grid grid-cols-2 lg:grid-cols-4 gap-4">
                <div class="w-full mb-5 lg:mb-7">
                    <div class="text-base text-soft-gray font-normal mb-1 p-0">{{ __('listener.profile.account.iin') }}</div>
                    <div class="text-base text-main-black font-light m-0 p-0">{{ Auth::user()->iin ?? 'Не указано' }}</div>
                </div>
                <div class="w-full mb-5 lg:mb-7">
                    <div class="text-base text-soft-gray font-normal mb-1 p-0">{{ __('listener.profile.account.email') }}</div>
                    <div class="text-base text-main-black font-light m-0 p-0">{{ Auth::user()->email  ?? 'Не указано' }}</div>
                </div>
                <div class="w-full mb-5 lg:mb-7">
                    <div class="text-base text-soft-gray font-normal mb-1 p-0">{{ __('listener.profile.account.lastname') }}</div>
                    <div class="text-base text-main-black font-normal m-0 p-0">{{ Auth::user()->last_name ?? 'Не указано' }}</div>
                </div>
                <div class="w-full mb-5 lg:mb-7">
                    <div class="text-base text-soft-gray font-normal mb-1 p-0">{{ __('listener.profile.account.lastname.lat') }}</div>
                    <div class="text-base text-main-black font-normal m-0 p-0">{{ Auth::user()->last_name_lat ?? 'Не указано' }}</div>
                </div>
                <div class="w-full mb-5 lg:mb-7">
                    <div class="text-base text-soft-gray font-normal mb-1 p-0">{{ __('listener.profile.account.name') }}</div>
                    <div class="text-base text-main-black font-normal m-0 p-0">{{ Auth::user()->name  ?? 'Не указано' }}</div>
                </div>
                <div class="w-full mb-5 lg:mb-7">
                    <div class="text-base text-soft-gray font-normal mb-1 p-0">{{ __('listener.profile.account.name.lat') }}</div>
                    <div class="text-base text-main-black font-normal m-0 p-0">{{ Auth::user()->name_lat  ?? 'Не указано' }}</div>
                </div>
                <div class="w-full mb-5 lg:mb-7">
                    <div class="text-base text-soft-gray font-normal mb-1 p-0">{{ __('listener.profile.account.patronymic') }}</div>
                    <div class="text-base text-main-black font-normal m-0 p-0">{{ Auth::user()->patronymic ?? 'Не указано' }}</div>
                </div>
                <div class="w-full mb-5 lg:mb-7">
                    <div class="text-base text-soft-gray font-normal mb-1 p-0">{{ __('listener.profile.account.gender') }}</div>
                    <div class="text-base text-main-black font-normal m-0 p-0">{{ Auth::user()->genderItem->title ?? 'Не указано' }}</div>
                </div>
                <div class="w-full mb-5 lg:mb-7">
                    <div class="text-base text-soft-gray font-normal mb-1 p-0">{{ __('listener.profile.account.region') }}</div>
                    @if(Auth::user()->oblastItem or Auth::user()->normalionItem or Auth::user()->gorodItem)
                        @if(Auth::user()->oblastItem)<div class="text-base text-main-black font-light m-0 p-0">Область: {{ Auth::user()->oblastItem->ru_title ?? 'Не указано' }}</div>@endif
                        @if(Auth::user()->raionItem)<div class="text-base text-main-black font-light m-0 p-0">Район: {{ Auth::user()->raionItem->ru_title ?? 'Не указано' }}</div>@endif
                        @if(Auth::user()->gorodItem)<div class="text-base text-main-black font-light m-0 p-0">Населенный пункт: {{ Auth::user()->gorodItem->ru_title ?? 'Не указано' }}</div>@endif
                    @else
                        <div class="text-base text-main-black font-light m-0 p-0">Не указано</div>
                    @endif
                </div>
                <div class="w-full mb-5 lg:mb-7">
                    <div class="text-base text-soft-gray font-normal m-b01p-0">{{ __('listener.profile.account.org') }}</div>
                    <span class="text-base text-main-black font-normalt m-0 p-0">{{ Auth::user()->instituteItem->title ?? 'Не указано' }}</span>
                </div>

                <div class="w-full mb-5 lg:mb-7">
                    <div class="text-base text-soft-gray font-normal mb-1 p-0">{{ __('listener.profile.account.position') }}</div>
                    <div class="text-base text-main-black font-normal m-0 p-0">{{ Auth::user()->jobnameItem->ru_title ?? 'Не указано' }}</div>
                </div>

                <div class="w-full mb-5 lg:mb-7">
                    <div class="text-base text-soft-gray font-normal mb-1 p-0">{{ __('listener.profile.account.nationality') }}</div>
                    <div class="text-base text-main-black font-normal m-0 p-0">{{ Auth::user()->nationalityItem->ru_title ?? 'Не указано' }}</div>
                </div>
                <div class="w-full mb-5 lg:mb-7">
                    <div class="text-base text-soft-gray font-normal mb-1 p-0">{{ __('Категория') }}</div>
                    <div class="text-base text-main-black font-normal m-0 p-0">{{ Auth::user()->academicCategoryItem->ru_title ?? 'Не указано' }}</div>
                </div>
                <div class="w-full mb-5 lg:mb-7">
                    <div class="text-base text-soft-gray font-normal mb-1 p-0">{{ __('Образование') }}</div>
                    <div class="text-base text-main-black font-normal m-0 p-0">{{ Auth::user()->educationItem->ru_title ?? 'Не указано' }}</div>
                </div>
                <div class="w-full mb-5 lg:mb-7">
                    <div class="text-base text-soft-gray font-normal mb-1 p-0">{{ __('Ученая степень') }}</div>
                    <div class="text-base text-main-black font-normal m-0 p-0">{{ Auth::user()->academicDegreeItem->ru_title ?? 'Не указано' }}</div>
                </div>
                <div class="w-full mb-5 lg:mb-7">
                    <div class="text-base text-soft-gray font-normal mb-1 p-0">{{ __('Педагогический стаж') }}</div>
                    <div class="text-base text-main-black font-normal m-0 p-0">{{ Auth::user()->stazh ?? 'Не указано' }}</div>
                </div>
            </div>
            <div class="w-full">
                <a href="{{ route('user.profile.edit') }}" class="flex px-8 py-4 border-2 rounded-lg border-logo-blue bg-white text-main-black text-base font-normal items-center space-x-2 hover:bg-logo-blue hover:text-white w-full justify-center lg:w-min">
                    <svg width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M4.61666 16.2667C4.10832 16.2667 3.63332 16.0917 3.29166 15.7667C2.85832 15.3583 2.64999 14.7417 2.72499 14.075L3.03332 11.375C3.09166 10.8667 3.39999 10.1917 3.75832 9.825L10.6 2.58333C12.3083 0.775001 14.0917 0.725002 15.9 2.43333C17.7083 4.14167 17.7583 5.925 16.05 7.73334L9.20832 14.975C8.85832 15.35 8.20832 15.7 7.69999 15.7833L5.01666 16.2417C4.87499 16.25 4.74999 16.2667 4.61666 16.2667ZM13.275 2.425C12.6333 2.425 12.075 2.825 11.5083 3.425L4.66666 10.675C4.49999 10.85 4.30832 11.2667 4.27499 11.5083L3.96666 14.2083C3.93332 14.4833 3.99999 14.7083 4.14999 14.85C4.29999 14.9917 4.52499 15.0417 4.79999 15L7.48332 14.5417C7.72499 14.5 8.12499 14.2833 8.29166 14.1083L15.1333 6.86667C16.1667 5.76667 16.5417 4.75 15.0333 3.33333C14.3667 2.69167 13.7917 2.425 13.275 2.425Z" fill="currentColor"/>
                        <path d="M14.45 9.125C14.4333 9.125 14.4083 9.125 14.3917 9.125C11.7917 8.86667 9.69999 6.89167 9.29999 4.30833C9.24999 3.96667 9.48332 3.65 9.82499 3.59167C10.1667 3.54167 10.4833 3.775 10.5417 4.11667C10.8583 6.13333 12.4917 7.68333 14.525 7.88333C14.8667 7.91667 15.1167 8.225 15.0833 8.56667C15.0417 8.88333 14.7667 9.125 14.45 9.125Z" fill="currentColor"/>
                        <path d="M17.5 18.9583H2.5C2.15833 18.9583 1.875 18.675 1.875 18.3333C1.875 17.9917 2.15833 17.7083 2.5 17.7083H17.5C17.8417 17.7083 18.125 17.9917 18.125 18.3333C18.125 18.675 17.8417 18.9583 17.5 18.9583Z" fill="currentColor"/>
                    </svg>
                    <span>{{ __('listener.profile.edit') }}</span>
                </a>
            </div>
        </div>
        <div class="w-full bg-white lg:bg-gray-bg p-0 lg:p-6 rounded-lg mb-8 lg:mb-6">
            <div class="w-full text-lg lg:text-xl font-bold mb-5">{{ __('listener.profile.security') }}</div>
            <div class="w-full grid grid-cols-1 lg:grid-cols-4 gap-4">
                <div class="w-full mb-5 lg:mb-7">
                    <div class="text-base text-soft-gray font-normal mb-1 p-0">{{ __('listener.profile.security.password') }}</div>
                    <button wire:click="$toggle('changePassword')" class="flex px-8 py-4 border-2 rounded-lg border-logo-blue bg-white text-main-black text-base font-normal items-center space-x-2 hover:bg-logo-blue hover:text-white w-full justify-center lg:w-min whitespace-nowrap">
                        <svg width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M4.61666 16.2667C4.10832 16.2667 3.63332 16.0917 3.29166 15.7667C2.85832 15.3583 2.64999 14.7417 2.72499 14.075L3.03332 11.375C3.09166 10.8667 3.39999 10.1917 3.75832 9.825L10.6 2.58333C12.3083 0.775001 14.0917 0.725002 15.9 2.43333C17.7083 4.14167 17.7583 5.925 16.05 7.73334L9.20832 14.975C8.85832 15.35 8.20832 15.7 7.69999 15.7833L5.01666 16.2417C4.87499 16.25 4.74999 16.2667 4.61666 16.2667ZM13.275 2.425C12.6333 2.425 12.075 2.825 11.5083 3.425L4.66666 10.675C4.49999 10.85 4.30832 11.2667 4.27499 11.5083L3.96666 14.2083C3.93332 14.4833 3.99999 14.7083 4.14999 14.85C4.29999 14.9917 4.52499 15.0417 4.79999 15L7.48332 14.5417C7.72499 14.5 8.12499 14.2833 8.29166 14.1083L15.1333 6.86667C16.1667 5.76667 16.5417 4.75 15.0333 3.33333C14.3667 2.69167 13.7917 2.425 13.275 2.425Z" fill="currentColor"/>
                            <path d="M14.45 9.125C14.4333 9.125 14.4083 9.125 14.3917 9.125C11.7917 8.86667 9.69999 6.89167 9.29999 4.30833C9.24999 3.96667 9.48332 3.65 9.82499 3.59167C10.1667 3.54167 10.4833 3.775 10.5417 4.11667C10.8583 6.13333 12.4917 7.68333 14.525 7.88333C14.8667 7.91667 15.1167 8.225 15.0833 8.56667C15.0417 8.88333 14.7667 9.125 14.45 9.125Z" fill="currentColor"/>
                            <path d="M17.5 18.9583H2.5C2.15833 18.9583 1.875 18.675 1.875 18.3333C1.875 17.9917 2.15833 17.7083 2.5 17.7083H17.5C17.8417 17.7083 18.125 17.9917 18.125 18.3333C18.125 18.675 17.8417 18.9583 17.5 18.9583Z" fill="currentColor"/>
                        </svg>
                        <span>{{ __('listener.profile.security.password.change') }}</span>
                    </button>
                </div>
                <div class="w-full mb-5 lg:mb-7">
                    <div class="text-base text-soft-gray font-normal mb-1 p-0">{{ __('listener.profile.security.phone') }}</div>
                    <div class="text-base text-main-black font-normal m-0 p-0">{{ Auth::user()->phone ?? 'Не указано' }}</div>
                </div>
            </div>
        </div>
    </div>
    @if($changePassword)
        <div>
            <div x-data="{show: true}" x-show="show" class="min-w-screen h-screen animated fadeIn faster fixed left-0 top-0 flex justify-center items-end lg:items-center inset-0 z-100 outline-none focus:outline-none bg-no-repeat bg-center bg-cover">
                <div class="absolute bg-main-black opacity-50 inset-0 z-0" wire:click="$set('changePassword',false);"></div>
                <div class="w-full lg:w-min py-10 lg:py-16 px-8 lg:px-24 relative mx-auto my-auto mb-0 lg:mb-auto rounded-t-xl lg:rounded-b-xl bg-white m-0" x-on:click.away="show = false">
                    @if($selfpassword == false)
                        <div class="flex flex-col justify-center mb-2">
                            <div class="text-2xl font-bold whitespace-nowrap text-center">{{ __('listener.profile.modal.password.change.title') }}</div>
                            <span class="text-center text-left mt-2">{{ __('listener.profile.security.password.toemail.text') }}</span>
                        </div>
                        <div class="flex flex-row w-full space-x-2">
                        <button wire:click="resetPassword" class="flex px-8 py-4 border-2 rounded-lg border-logo-blue bg-white text-main-black text-base font-normal items-center space-x-2 hover:bg-logo-blue hover:text-white w-full justify-center lg:w-min whitespace-nowrap">
                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="20px" height="20px" viewBox="0 0 20 20" version="1.1">
                                <g id="surface1">
                                    <path style=" stroke:none;fill-rule:nonzero;fill:rgb(0%,0%,0%);fill-opacity:1;" d="M 14.582031 6.667969 L 14.582031 5 L 13.75 5 L 13.75 8.03125 C 13.75 8.425781 13.425781 8.75 13.03125 8.75 L 6.96875 8.75 C 6.574219 8.75 6.25 8.425781 6.25 8.03125 L 6.25 5 L 5.417969 5 L 5.417969 6.667969 L 0 6.667969 L 0 20 L 20 20 L 20 6.667969 Z M 5.417969 7.5 L 5.417969 8.03125 C 5.417969 8.886719 6.113281 9.582031 6.96875 9.582031 L 13.03125 9.582031 C 13.886719 9.582031 14.582031 8.886719 14.582031 8.03125 L 14.582031 7.5 L 18.355469 7.5 L 10 13.707031 L 1.644531 7.5 Z M 0.832031 7.9375 L 6.84375 12.402344 L 0.832031 18.558594 Z M 1.40625 19.167969 L 7.519531 12.902344 L 10 14.746094 L 12.480469 12.902344 L 18.59375 19.167969 Z M 19.167969 18.558594 L 13.15625 12.402344 L 19.167969 7.9375 Z M 19.167969 18.558594 "/>
                                    <path style=" stroke:none;fill-rule:nonzero;fill:rgb(0%,0%,0%);fill-opacity:1;" d="M 12.355469 3.53125 L 10.417969 5.472656 L 10.417969 0 L 9.582031 0 L 9.582031 5.472656 L 7.644531 3.53125 L 7.054688 4.121094 L 10 7.066406 L 12.945312 4.121094 Z M 12.355469 3.53125 "/>
                                </g>
                            </svg>

                            <span>{{ __('listener.profile.security.password.toemail') }}</span>
                        </button>
                        <button wire:click="$set('selfpassword',true);" class="flex px-8 py-4 border-2 rounded-lg border-logo-blue bg-white text-main-black text-base font-normal items-center space-x-2 hover:bg-logo-blue hover:text-white w-full justify-center lg:w-min whitespace-nowrap">
                            <svg width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M4.61666 16.2667C4.10832 16.2667 3.63332 16.0917 3.29166 15.7667C2.85832 15.3583 2.64999 14.7417 2.72499 14.075L3.03332 11.375C3.09166 10.8667 3.39999 10.1917 3.75832 9.825L10.6 2.58333C12.3083 0.775001 14.0917 0.725002 15.9 2.43333C17.7083 4.14167 17.7583 5.925 16.05 7.73334L9.20832 14.975C8.85832 15.35 8.20832 15.7 7.69999 15.7833L5.01666 16.2417C4.87499 16.25 4.74999 16.2667 4.61666 16.2667ZM13.275 2.425C12.6333 2.425 12.075 2.825 11.5083 3.425L4.66666 10.675C4.49999 10.85 4.30832 11.2667 4.27499 11.5083L3.96666 14.2083C3.93332 14.4833 3.99999 14.7083 4.14999 14.85C4.29999 14.9917 4.52499 15.0417 4.79999 15L7.48332 14.5417C7.72499 14.5 8.12499 14.2833 8.29166 14.1083L15.1333 6.86667C16.1667 5.76667 16.5417 4.75 15.0333 3.33333C14.3667 2.69167 13.7917 2.425 13.275 2.425Z" fill="currentColor"/>
                                <path d="M14.45 9.125C14.4333 9.125 14.4083 9.125 14.3917 9.125C11.7917 8.86667 9.69999 6.89167 9.29999 4.30833C9.24999 3.96667 9.48332 3.65 9.82499 3.59167C10.1667 3.54167 10.4833 3.775 10.5417 4.11667C10.8583 6.13333 12.4917 7.68333 14.525 7.88333C14.8667 7.91667 15.1167 8.225 15.0833 8.56667C15.0417 8.88333 14.7667 9.125 14.45 9.125Z" fill="currentColor"/>
                                <path d="M17.5 18.9583H2.5C2.15833 18.9583 1.875 18.675 1.875 18.3333C1.875 17.9917 2.15833 17.7083 2.5 17.7083H17.5C17.8417 17.7083 18.125 17.9917 18.125 18.3333C18.125 18.675 17.8417 18.9583 17.5 18.9583Z" fill="currentColor"/>
                            </svg>
                            <span>{{ __('listener.profile.security.password.self-change') }}</span>
                        </button>
                        </div>
                    @else
                        @livewire('profile.update-password-form')
                    @endif
                </div>
            </div>
        </div>
    @endif
    </div>
</div>


@push('scripts')
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>

    <script type="text/javascript">
        document.addEventListener('DOMContentLoaded', function () {
            @this.on('password_reset',() => {
                Swal.fire({
                    title: '{{ __("Пароль изменен!") }}',
                    text: 'Пароль отправлен на почту',
                    icon: 'success',
                    width: 600,
                    position: 'top-end',
                    showConfirmButton: false,
                    timer: 3000});
            });
        });
        window.onload = function() {
            Livewire.on('saved', () => {
                Swal.fire({
                    title: '{{ __("Пароль изменен!") }}',
                    text: 'Пароль сохранен!',
                    icon: 'success',
                    width: 600,
                    position: 'top-end',
                    showConfirmButton: false,
                    timer: 3000});
            })
        }
    </script>
@endpush
