<div class="px-4 py-8 lg:px-14 lg:py-12 bg-white w-full rounded-lg">
    <div class="w-full flex flex-col">
        <a href="{{ route('user.groups.active') }}" class="w-full flex space-x-1.5 items-center text-main-black text-base hover:text-logo-blue mb-3 lg:mb-5">
            <svg width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path d="M7.97495 15.6833C7.81662 15.6833 7.65828 15.625 7.53328 15.5L2.47495 10.4416C2.23328 10.2 2.23328 9.79998 2.47495 9.55831L7.53328 4.49998C7.77495 4.25831 8.17495 4.25831 8.41662 4.49998C8.65829 4.74164 8.65829 5.14164 8.41662 5.38331L3.79995 9.99998L8.41662 14.6166C8.65829 14.8583 8.65829 15.2583 8.41662 15.5C8.29995 15.625 8.13328 15.6833 7.97495 15.6833Z" fill="currentColor"/>
                <path d="M17.0833 10.625H3.05835C2.71668 10.625 2.43335 10.3417 2.43335 10C2.43335 9.65833 2.71668 9.375 3.05835 9.375H17.0833C17.425 9.375 17.7083 9.65833 17.7083 10C17.7083 10.3417 17.425 10.625 17.0833 10.625Z" fill="currentColor"/>
            </svg>
            <span>{{ __('listener.groups.title') }}</span>
        </a>
        <h2 class="font-bold text-xl lg:text-2xl text-main-black mb-3 lg:mb-5">{{ $group->coursesItem->ru_title }}</h2>
    </div>
    <div class="w-full bg-white lg:bg-gray-bg p-0 lg:p-6 rounded-lg mb-8 lg:mb-6">
        <div class="w-full text-lg lg:text-xl font-bold mb-5">{{ __('listener.profile.general') }}</div>
        <div class="w-full grid grid-cols-1 grid-cols-3">
            <div class="w-full grid grid-cols-2 gap-4 col-span-3 lg:col-span-2">
                <div class="w-full mb-5 lg:mb-7">
                    <div class="text-base text-soft-gray font-normal mb-1 p-0">{{ __('listener.group.group') }}</div>
                    <div class="text-base text-main-black font-light m-0 p-0">{{ $group->slug }}</div>
                </div>
                <div class="w-full mb-5 lg:mb-7">
                    <div class="text-base text-soft-gray font-normal mb-1 p-0">{{ __('listener.group.city') }}</div>
                    <div class="text-base text-main-black font-light m-0 p-0">{{ $group->affiliateItem->title }}</div>
                </div>
                <div class="w-full mb-5 lg:mb-7">
                    <div class="text-base text-soft-gray font-normal mb-1 p-0">{{ __('listener.group.trainer') }}</div>
                    <div class="text-base text-main-black font-light m-0 p-0">{{ $group->trainerItem->fio }}</div>
                </div>
                <div class="w-full mb-5 lg:mb-7">
                    <div class="text-base text-soft-gray font-normal mb-1 p-0">{{ __('listener.group.period') }}</div>
                    <div class="text-base text-main-black font-light m-0 p-0">с {{ date('d.m.Y',strtotime($group->start_time)) }}
                        по {{ date('d.m.Y',strtotime($group->end_time)) }}</div>
                </div>
            </div>

            <div class="w-full col-span-3 mb-4">
                    @if($group->trainerItem->trainer_awards !== null)
                        <div id="accordion-flush-1" data-accordion="collapse" data-active-classes="dark:bg-gray-900 text-gray-900 dark:text-white" data-inactive-classes="text-gray-500 dark:text-gray-400">
                            <h2 id="accordion-flush-heading-1">
                                <button type="button" class="flex items-center justify-between w-full py-5 font-medium text-left text-gray-500 border-b border-gray-200 dark:border-gray-700 dark:text-gray-400" data-accordion-target="#accordion-flush-body-1" aria-expanded="false" aria-controls="accordion-flush-body-1">
                                    <span>{{ __('Награды тренера') }}</span>
                                    <svg data-accordion-icon="" class="w-6 h-6 shrink-0" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" d="M5.293 7.293a1 1 0 011.414 0L10 10.586l3.293-3.293a1 1 0 111.414 1.414l-4 4a1 1 0 01-1.414 0l-4-4a1 1 0 010-1.414z" clip-rule="evenodd"></path></svg>
                                </button>
                            </h2>
                            <div id="accordion-flush-body-1" class="hidden" aria-labelledby="accordion-flush-heading-1">
                                <div class="py-5 font-light border-b border-gray-200 dark:border-gray-700">
                                    {!! $group->trainerItem->trainer_awards !!}
                                </div>
                            </div>
                        </div>
                    @endif
                    @if($group->trainerItem->trainer_theses !== null)
                        <div id="accordion-flush-2" data-accordion="collapse" data-active-classes="dark:bg-gray-900 text-gray-900 dark:text-white" data-inactive-classes="text-gray-500 dark:text-gray-400">
                            <h2 id="accordion-flush-heading-2">
                                <button type="button" class="flex items-center justify-between w-full py-5 font-medium text-left text-gray-500 border-b border-gray-200 dark:border-gray-700 dark:text-gray-400" data-accordion-target="#accordion-flush-body-2" aria-expanded="false" aria-controls="accordion-flush-body-2">
                                    <span>{{ __('Опубликованные статьи тренера') }}</span>
                                    <svg data-accordion-icon="" class="w-6 h-6 shrink-0" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" d="M5.293 7.293a1 1 0 011.414 0L10 10.586l3.293-3.293a1 1 0 111.414 1.414l-4 4a1 1 0 01-1.414 0l-4-4a1 1 0 010-1.414z" clip-rule="evenodd"></path></svg>
                                </button>
                            </h2>
                            <div id="accordion-flush-body-2" class="hidden" aria-labelledby="accordion-flush-heading-2">
                                <div class="py-5 font-light border-b border-gray-200 dark:border-gray-700">
                                    {!! $group->trainerItem->trainer_theses !!}
                                </div>
                            </div>
                        </div>
                    @endif
                    @if($group->trainerItem->trainer_certificates !== null)
                        <div id="accordion-flush-3" data-accordion="collapse" data-active-classes="dark:bg-gray-900 text-gray-900 dark:text-white" data-inactive-classes="text-gray-500 dark:text-gray-400">
                            <h2 id="accordion-flush-heading-3">
                                <button type="button" class="flex items-center justify-between w-full py-5 font-medium text-left text-gray-500 border-b border-gray-200 dark:border-gray-700 dark:text-gray-400" data-accordion-target="#accordion-flush-body-3" aria-expanded="false" aria-controls="accordion-flush-body-3">
                                    <span>{{ __('Пройденные курсы тренера') }}</span>
                                    <svg data-accordion-icon="" class="w-6 h-6 shrink-0" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" d="M5.293 7.293a1 1 0 011.414 0L10 10.586l3.293-3.293a1 1 0 111.414 1.414l-4 4a1 1 0 01-1.414 0l-4-4a1 1 0 010-1.414z" clip-rule="evenodd"></path></svg>
                                </button>
                            </h2>
                            <div id="accordion-flush-body-3" class="hidden" aria-labelledby="accordion-flush-heading-3">
                                <div class="py-5 font-light border-b border-gray-200 dark:border-gray-700">
                                    {!! $group->trainerItem->trainer_certificates !!}
                                </div>
                            </div>
                        </div>
                    @endif

            </div>
            @if($group->offer_agreement)
                <div class="w-full mb-5 lg:mb-7">
                    <a class="w-full text-logo-blue flex items-center space-x-2 hover:text-primary-button" href="{{$group->getFileUrl($group->offer_agreement)}}" target="_blank" download="{{$this->group->slug}}">
                        <svg width="16" height="16" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M7.91997 9.99333C7.7933 9.99333 7.66663 9.94666 7.56663 9.84666L5.85997 8.14C5.66663 7.94666 5.66663 7.62666 5.85997 7.43333C6.0533 7.24 6.3733 7.24 6.56663 7.43333L7.91997 8.78666L9.2733 7.43333C9.46663 7.24 9.78663 7.24 9.97997 7.43333C10.1733 7.62666 10.1733 7.94666 9.97997 8.14L8.2733 9.84666C8.1733 9.94666 8.04663 9.99333 7.91997 9.99333Z" fill="currentColor"/>
                            <path d="M7.92004 9.94675C7.64671 9.94675 7.42004 9.72008 7.42004 9.44675V2.66675C7.42004 2.39341 7.64671 2.16675 7.92004 2.16675C8.19338 2.16675 8.42004 2.39341 8.42004 2.66675V9.44675C8.42004 9.72008 8.19338 9.94675 7.92004 9.94675Z" fill="currentColor"/>
                            <path d="M7.99996 13.9535C4.56663 13.9535 2.16663 11.5535 2.16663 8.12012C2.16663 7.84678 2.39329 7.62012 2.66663 7.62012C2.93996 7.62012 3.16663 7.84678 3.16663 8.12012C3.16663 10.9668 5.15329 12.9535 7.99996 12.9535C10.8466 12.9535 12.8333 10.9668 12.8333 8.12012C12.8333 7.84678 13.06 7.62012 13.3333 7.62012C13.6066 7.62012 13.8333 7.84678 13.8333 8.12012C13.8333 11.5535 11.4333 13.9535 7.99996 13.9535Z" fill="currentColor"/>
                        </svg>
                        <span>Скачать договор оферты</span>
                    </a>
                </div>
            @endif
        </div>
    </div>
    @if ($group->status == \App\Models\Groups::GROUP_STATUS_FINISHED or $group->status == \App\Models\Groups::GROUP_STATUS_FINISHED_TESTING  or $group->status == \App\Models\Groups::GROUP_STATUS_CERTIFICATED  or $group->status == \App\Models\Groups::GROUP_STATUS_ARCHIVE)
        {{--        @push('styles')--}}
        {{--            <link href="https://cdn.jsdelivr.net/npm/daisyui@2.15.0/dist/full.css" rel="stylesheet" type="text/css" />--}}
        {{--        @endpush--}}
        @if(!$feedback)
            <div class="w-full bg-white lg:bg-gray-bg p-0 lg:p-6 rounded-lg mb-8 lg:mb-6" wire:key="feedback">
                <div class="w-full text-lg lg:text-xl font-bold mb-5">Оцените и напишите отзыв для улучшения качества
                    обучения</div>
                <div class="w-full lg:w-3/4 lg:px-4 py-4 flex flex-col bg-white rounded-lg">

                    <div class="flex w-full flex-col justify-center">
                        <div class="flex flex-row w-full justify-between">
                            <div class="flex items-center bg-gray-bg p-2 space-x-1 rounded rating @error('stars') border-red-500 border-2 @enderror">
                                <svg wire:click="$set('stars',1)"
                                     class="w-6 h-6 cursor-pointer @if($stars >= 1) text-primary-button @else text-user-gray @endif"
                                     fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg">
                                    <path
                                        d="M9.049 2.927c.3-.921 1.603-.921 1.902 0l1.07 3.292a1 1 0 00.95.69h3.462c.969 0 1.371 1.24.588 1.81l-2.8 2.034a1 1 0 00-.364 1.118l1.07 3.292c.3.921-.755 1.688-1.54 1.118l-2.8-2.034a1 1 0 00-1.175 0l-2.8 2.034c-.784.57-1.838-.197-1.539-1.118l1.07-3.292a1 1 0 00-.364-1.118L2.98 8.72c-.783-.57-.38-1.81.588-1.81h3.461a1 1 0 00.951-.69l1.07-3.292z"></path>
                                </svg>
                                <svg wire:click="$set('stars',2)"
                                     class="w-6 h-6 cursor-pointer @if($stars >= 2) text-primary-button @else text-user-gray @endif"
                                     fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg">
                                    <path
                                        d="M9.049 2.927c.3-.921 1.603-.921 1.902 0l1.07 3.292a1 1 0 00.95.69h3.462c.969 0 1.371 1.24.588 1.81l-2.8 2.034a1 1 0 00-.364 1.118l1.07 3.292c.3.921-.755 1.688-1.54 1.118l-2.8-2.034a1 1 0 00-1.175 0l-2.8 2.034c-.784.57-1.838-.197-1.539-1.118l1.07-3.292a1 1 0 00-.364-1.118L2.98 8.72c-.783-.57-.38-1.81.588-1.81h3.461a1 1 0 00.951-.69l1.07-3.292z"></path>
                                </svg>
                                <svg wire:click="$set('stars',3)"
                                     class="w-6 h-6 cursor-pointer @if($stars >= 3) text-primary-button @else text-user-gray @endif"
                                     fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg">
                                    <path
                                        d="M9.049 2.927c.3-.921 1.603-.921 1.902 0l1.07 3.292a1 1 0 00.95.69h3.462c.969 0 1.371 1.24.588 1.81l-2.8 2.034a1 1 0 00-.364 1.118l1.07 3.292c.3.921-.755 1.688-1.54 1.118l-2.8-2.034a1 1 0 00-1.175 0l-2.8 2.034c-.784.57-1.838-.197-1.539-1.118l1.07-3.292a1 1 0 00-.364-1.118L2.98 8.72c-.783-.57-.38-1.81.588-1.81h3.461a1 1 0 00.951-.69l1.07-3.292z"></path>
                                </svg>
                                <svg wire:click="$set('stars',4)"
                                     class="w-6 h-6 cursor-pointer @if($stars >= 4) text-primary-button @else text-user-gray @endif"
                                     fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg">
                                    <path
                                        d="M9.049 2.927c.3-.921 1.603-.921 1.902 0l1.07 3.292a1 1 0 00.95.69h3.462c.969 0 1.371 1.24.588 1.81l-2.8 2.034a1 1 0 00-.364 1.118l1.07 3.292c.3.921-.755 1.688-1.54 1.118l-2.8-2.034a1 1 0 00-1.175 0l-2.8 2.034c-.784.57-1.838-.197-1.539-1.118l1.07-3.292a1 1 0 00-.364-1.118L2.98 8.72c-.783-.57-.38-1.81.588-1.81h3.461a1 1 0 00.951-.69l1.07-3.292z"></path>
                                </svg>
                                <svg wire:click="$set('stars',5)"
                                     class="w-6 h-6 cursor-pointer @if($stars >= 5) text-primary-button @else text-user-gray @endif"
                                     fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg">
                                    <path
                                        d="M9.049 2.927c.3-.921 1.603-.921 1.902 0l1.07 3.292a1 1 0 00.95.69h3.462c.969 0 1.371 1.24.588 1.81l-2.8 2.034a1 1 0 00-.364 1.118l1.07 3.292c.3.921-.755 1.688-1.54 1.118l-2.8-2.034a1 1 0 00-1.175 0l-2.8 2.034c-.784.57-1.838-.197-1.539-1.118l1.07-3.292a1 1 0 00-.364-1.118L2.98 8.72c-.783-.57-.38-1.81.588-1.81h3.461a1 1 0 00.951-.69l1.07-3.292z"></path>
                                </svg>
                            </div>
                            <button wire:click="saveFeedback"
                                    class="mb-2 text-sm flex justify-end items-center px-4 py-1 border-0 bg-blue-500 text-white rounded hover:bg-blue-600">
                                {{ __('admin.buttons.submit') }}
                            </button>
                        </div>

                        <div class="rounded w-full mb-5">
                            <label for="message"
                                   class="block mb-2 text-sm font-medium text-gray-900 dark:text-gray-400"></label>
                            <textarea id="message" rows="4" wire:model="feedbacktext"
                                      class="block p-2.5 w-full text-sm text-gray-900 bg-gray-50 rounded-lg border border-gray-300 focus:ring-blue-500 focus:border-blue-500 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                                      placeholder="Ваш отзыв..."></textarea>
                        </div>

                    </div>
                </div>
            </div>
        @else
            <div class="w-full bg-white lg:bg-gray-bg p-0 lg:p-6 rounded-lg mb-8 lg:mb-6" wire:key="feedback-exists">
                <div class="w-full text-lg lg:text-xl font-bold mb-5">Ваш отзыв</div>
                <div class="w-full lg:w-3/4 lg:px-4 py-4 flex flex-col bg-white rounded-lg">
                    <div class="flex w-full flex-col justify-center">
                        <div class="flex flex-row w-full justify-between">
                            <div class="flex bg-gray-bg p-2 space-x-1 rounded items-center">
                                <svg
                                    class="w-6 h-6 @if($stars >= 1) text-primary-button @else text-user-gray @endif"
                                    fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg">
                                    <path
                                        d="M9.049 2.927c.3-.921 1.603-.921 1.902 0l1.07 3.292a1 1 0 00.95.69h3.462c.969 0 1.371 1.24.588 1.81l-2.8 2.034a1 1 0 00-.364 1.118l1.07 3.292c.3.921-.755 1.688-1.54 1.118l-2.8-2.034a1 1 0 00-1.175 0l-2.8 2.034c-.784.57-1.838-.197-1.539-1.118l1.07-3.292a1 1 0 00-.364-1.118L2.98 8.72c-.783-.57-.38-1.81.588-1.81h3.461a1 1 0 00.951-.69l1.07-3.292z"></path>
                                </svg>
                                <svg
                                    class="w-6 h-6 @if($stars >= 2) text-primary-button @else text-user-gray @endif"
                                    fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg">
                                    <path
                                        d="M9.049 2.927c.3-.921 1.603-.921 1.902 0l1.07 3.292a1 1 0 00.95.69h3.462c.969 0 1.371 1.24.588 1.81l-2.8 2.034a1 1 0 00-.364 1.118l1.07 3.292c.3.921-.755 1.688-1.54 1.118l-2.8-2.034a1 1 0 00-1.175 0l-2.8 2.034c-.784.57-1.838-.197-1.539-1.118l1.07-3.292a1 1 0 00-.364-1.118L2.98 8.72c-.783-.57-.38-1.81.588-1.81h3.461a1 1 0 00.951-.69l1.07-3.292z"></path>
                                </svg>
                                <svg
                                    class="w-6 h-6 @if($stars >= 3) text-primary-button @else text-user-gray @endif"
                                    fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg">
                                    <path
                                        d="M9.049 2.927c.3-.921 1.603-.921 1.902 0l1.07 3.292a1 1 0 00.95.69h3.462c.969 0 1.371 1.24.588 1.81l-2.8 2.034a1 1 0 00-.364 1.118l1.07 3.292c.3.921-.755 1.688-1.54 1.118l-2.8-2.034a1 1 0 00-1.175 0l-2.8 2.034c-.784.57-1.838-.197-1.539-1.118l1.07-3.292a1 1 0 00-.364-1.118L2.98 8.72c-.783-.57-.38-1.81.588-1.81h3.461a1 1 0 00.951-.69l1.07-3.292z"></path>
                                </svg>
                                <svg
                                    class="w-6 h-6 @if($stars >= 4) text-primary-button @else text-user-gray @endif"
                                    fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg">
                                    <path
                                        d="M9.049 2.927c.3-.921 1.603-.921 1.902 0l1.07 3.292a1 1 0 00.95.69h3.462c.969 0 1.371 1.24.588 1.81l-2.8 2.034a1 1 0 00-.364 1.118l1.07 3.292c.3.921-.755 1.688-1.54 1.118l-2.8-2.034a1 1 0 00-1.175 0l-2.8 2.034c-.784.57-1.838-.197-1.539-1.118l1.07-3.292a1 1 0 00-.364-1.118L2.98 8.72c-.783-.57-.38-1.81.588-1.81h3.461a1 1 0 00.951-.69l1.07-3.292z"></path>
                                </svg>
                                <svg
                                    class="w-6 h-6 @if($stars >= 5) text-primary-button @else text-user-gray @endif"
                                    fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg">
                                    <path
                                        d="M9.049 2.927c.3-.921 1.603-.921 1.902 0l1.07 3.292a1 1 0 00.95.69h3.462c.969 0 1.371 1.24.588 1.81l-2.8 2.034a1 1 0 00-.364 1.118l1.07 3.292c.3.921-.755 1.688-1.54 1.118l-2.8-2.034a1 1 0 00-1.175 0l-2.8 2.034c-.784.57-1.838-.197-1.539-1.118l1.07-3.292a1 1 0 00-.364-1.118L2.98 8.72c-.783-.57-.38-1.81.588-1.81h3.461a1 1 0 00.951-.69l1.07-3.292z"></path>
                                </svg>
                            </div>
                            <button wire:click="$set('feedback',null)"
                                    class="mb-2 text-sm flex justify-end items-center px-4 py-1 border-0 bg-blue-500 text-white rounded hover:bg-blue-600">
                                {{ __('admin.buttons.edit') }}
                            </button>
                        </div>

                        <div class="rounded w-full">
                            <label for="message"
                                   class="block mb-2 text-sm font-medium text-gray-900 dark:text-gray-400"></label>
                            <textarea id="message" rows="4" wire:model="feedbacktext" disabled
                                      class="block p-2.5 w-full text-sm text-gray-900 bg-gray-50 rounded-lg border border-gray-300 focus:ring-blue-500 focus:border-blue-500 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                                      placeholder="Ваш отзыв..."></textarea>
                        </div>

                    </div>
                </div>
            </div>
        @endif
    @endif

    @if(($group->coursesItem->type == \App\Models\Courses::COURSES_ONLINE or $group->coursesItem->type == \App\Models\Courses::COURSES_MIXED) and ($group->zoom_link))
        <div class="w-full bg-white lg:bg-gray-bg p-0 lg:p-6 rounded-lg mb-8 lg:mb-6">
            <div class="w-full text-lg lg:text-xl font-bold mb-5">Ссылка на видеоконференцию</div>
            <div class="w-full lg:w-3/4 lg:px-4 py-4 flex flex-col bg-white rounded-lg">
                <input disabled class="text-logo-blue text-base font-normal bg-white w-full mb-4" id="zoomlink"
                   value="{{ $group->zoom_link }}"/>
                <button onclick="copyToClipboard()"
                        class="w-full text-logo-blue flex items-center space-x-2 hover:text-primary-button">
                    <svg width="16" height="16" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M7.60004 15.1666H5.06671C2.14004 15.1666 0.833374 13.86 0.833374 10.9333V8.39996C0.833374 5.47329 2.14004 4.16663 5.06671 4.16663H7.06671C7.34004 4.16663 7.56671 4.39329 7.56671 4.66663C7.56671 4.93996 7.34004 5.16663 7.06671 5.16663H5.06671C2.68004 5.16663 1.83337 6.01329 1.83337 8.39996V10.9333C1.83337 13.32 2.68004 14.1666 5.06671 14.1666H7.60004C9.98671 14.1666 10.8334 13.32 10.8334 10.9333V8.93329C10.8334 8.65996 11.06 8.43329 11.3334 8.43329C11.6067 8.43329 11.8334 8.65996 11.8334 8.93329V10.9333C11.8334 13.86 10.5267 15.1666 7.60004 15.1666Z" fill="currentColor"/>
                        <path d="M11.3333 9.43329H9.19998C7.32665 9.43329 6.56665 8.67329 6.56665 6.79995V4.66662C6.56665 4.46662 6.68665 4.27995 6.87332 4.20662C7.05998 4.12662 7.27332 4.17329 7.41998 4.31329L11.6867 8.57995C11.8267 8.71995 11.8733 8.93995 11.7933 9.12662C11.72 9.31329 11.5333 9.43329 11.3333 9.43329ZM7.56665 5.87329V6.79995C7.56665 8.12662 7.87332 8.43329 9.19998 8.43329H10.1266L7.56665 5.87329Z" fill="currentColor"/>
                        <path d="M10.3999 1.83337H7.73328C7.45994 1.83337 7.23328 1.60671 7.23328 1.33337C7.23328 1.06004 7.45994 0.833374 7.73328 0.833374H10.3999C10.6733 0.833374 10.8999 1.06004 10.8999 1.33337C10.8999 1.60671 10.6733 1.83337 10.3999 1.83337Z" fill="currentColor"/>
                        <path d="M4.66663 3.83337C4.39329 3.83337 4.16663 3.60671 4.16663 3.33337C4.16663 1.95337 5.28663 0.833374 6.66663 0.833374H8.41329C8.68663 0.833374 8.91329 1.06004 8.91329 1.33337C8.91329 1.60671 8.68663 1.83337 8.41329 1.83337H6.66663C5.83996 1.83337 5.16663 2.50671 5.16663 3.33337C5.16663 3.60671 4.93996 3.83337 4.66663 3.83337Z" fill="currentColor"/>
                        <path d="M12.7933 11.8334C12.52 11.8334 12.2933 11.6067 12.2933 11.3334C12.2933 11.06 12.52 10.8334 12.7933 10.8334C13.5533 10.8334 14.1667 10.2134 14.1667 9.46004V5.33337C14.1667 5.06004 14.3933 4.83337 14.6667 4.83337C14.94 4.83337 15.1667 5.06004 15.1667 5.33337V9.46004C15.1667 10.7667 14.1 11.8334 12.7933 11.8334Z" fill="currentColor"/>
                        <path d="M14.6666 5.83337H12.6666C10.8933 5.83337 10.1666 5.1067 10.1666 3.33337V1.33337C10.1666 1.13337 10.2866 0.9467 10.4733 0.873366C10.66 0.793366 10.8733 0.840033 11.02 0.980033L15.02 4.98003C15.16 5.12003 15.2066 5.34003 15.1266 5.5267C15.0533 5.71337 14.8666 5.83337 14.6666 5.83337ZM11.1666 2.54003V3.33337C11.1666 4.55337 11.4466 4.83337 12.6666 4.83337H13.46L11.1666 2.54003Z" fill="currentColor"/>
                    </svg>
                    <span>
                    {{ __('trainer.groups.planned.joinlink.copy') }}
                    </span>
                </button>
                <script>
                    function copyToClipboard() {
                        /* Get the text field */
                        let copyText = document.getElementById("zoomlink");

                        /* Select the text field */
                        copyText.select();
                        copyText.setSelectionRange(0, 99999); /* For mobile devices */

                        /* Copy the text inside the text field */
                        navigator.clipboard.writeText(copyText.value);
                        Swal.fire({
                            title: '{{ __("admin.swal.grants.title.copied") }}',
                            icon: 'success',
                            width: 600,
                            position: 'top-end',
                            showConfirmButton: false,
                            timer: 1000
                        });

                    }
                </script>
            </div>
        </div>
    @endif

    @if((!empty($group->coursesItem->sdo_link)) and ($group->languageItem->slug == 'kz') or (!empty($group->coursesItem->sdo_link_ru)) and ($group->languageItem->slug == 'ru') or (!empty($group->coursesItem->sdo_link_en)) and ($group->languageItem->slug == 'en'))
        <div class="w-full bg-white lg:bg-gray-bg p-0 lg:p-6 rounded-lg mb-8 lg:mb-6">
            <div class="w-full text-lg lg:text-xl font-bold mb-5">Ссылка на СДО</div>
            <div class="w-full lg:w-3/4 lg:px-4 py-4 flex flex-col bg-white rounded-lg">
                @if($group->languageItem->slug == 'kz')
                    <input disabled class="text-logo-blue text-base font-normal bg-white w-full mb-4" id="sdolink" value="{{ $group->coursesItem->sdo_link }}"/>
                    <div class="flex-row flex">
                        <a href="{{ $group->coursesItem->sdo_link }}" target="_blank"
                           class="w-full text-logo-blue flex items-center space-x-2 hover:text-primary-button">
                            <svg width="16" height="16" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M7.60004 15.1666H5.06671C2.14004 15.1666 0.833374 13.86 0.833374 10.9333V8.39996C0.833374 5.47329 2.14004 4.16663 5.06671 4.16663H7.06671C7.34004 4.16663 7.56671 4.39329 7.56671 4.66663C7.56671 4.93996 7.34004 5.16663 7.06671 5.16663H5.06671C2.68004 5.16663 1.83337 6.01329 1.83337 8.39996V10.9333C1.83337 13.32 2.68004 14.1666 5.06671 14.1666H7.60004C9.98671 14.1666 10.8334 13.32 10.8334 10.9333V8.93329C10.8334 8.65996 11.06 8.43329 11.3334 8.43329C11.6067 8.43329 11.8334 8.65996 11.8334 8.93329V10.9333C11.8334 13.86 10.5267 15.1666 7.60004 15.1666Z" fill="currentColor"/>
                                <path d="M11.3333 9.43329H9.19998C7.32665 9.43329 6.56665 8.67329 6.56665 6.79995V4.66662C6.56665 4.46662 6.68665 4.27995 6.87332 4.20662C7.05998 4.12662 7.27332 4.17329 7.41998 4.31329L11.6867 8.57995C11.8267 8.71995 11.8733 8.93995 11.7933 9.12662C11.72 9.31329 11.5333 9.43329 11.3333 9.43329ZM7.56665 5.87329V6.79995C7.56665 8.12662 7.87332 8.43329 9.19998 8.43329H10.1266L7.56665 5.87329Z" fill="currentColor"/>
                                <path d="M10.3999 1.83337H7.73328C7.45994 1.83337 7.23328 1.60671 7.23328 1.33337C7.23328 1.06004 7.45994 0.833374 7.73328 0.833374H10.3999C10.6733 0.833374 10.8999 1.06004 10.8999 1.33337C10.8999 1.60671 10.6733 1.83337 10.3999 1.83337Z" fill="currentColor"/>
                                <path d="M4.66663 3.83337C4.39329 3.83337 4.16663 3.60671 4.16663 3.33337C4.16663 1.95337 5.28663 0.833374 6.66663 0.833374H8.41329C8.68663 0.833374 8.91329 1.06004 8.91329 1.33337C8.91329 1.60671 8.68663 1.83337 8.41329 1.83337H6.66663C5.83996 1.83337 5.16663 2.50671 5.16663 3.33337C5.16663 3.60671 4.93996 3.83337 4.66663 3.83337Z" fill="currentColor"/>
                                <path d="M12.7933 11.8334C12.52 11.8334 12.2933 11.6067 12.2933 11.3334C12.2933 11.06 12.52 10.8334 12.7933 10.8334C13.5533 10.8334 14.1667 10.2134 14.1667 9.46004V5.33337C14.1667 5.06004 14.3933 4.83337 14.6667 4.83337C14.94 4.83337 15.1667 5.06004 15.1667 5.33337V9.46004C15.1667 10.7667 14.1 11.8334 12.7933 11.8334Z" fill="currentColor"/>
                                <path d="M14.6666 5.83337H12.6666C10.8933 5.83337 10.1666 5.1067 10.1666 3.33337V1.33337C10.1666 1.13337 10.2866 0.9467 10.4733 0.873366C10.66 0.793366 10.8733 0.840033 11.02 0.980033L15.02 4.98003C15.16 5.12003 15.2066 5.34003 15.1266 5.5267C15.0533 5.71337 14.8666 5.83337 14.6666 5.83337ZM11.1666 2.54003V3.33337C11.1666 4.55337 11.4466 4.83337 12.6666 4.83337H13.46L11.1666 2.54003Z" fill="currentColor"/>
                            </svg>
                            <span>
                            {{ __('trainer.groups.sdo.link.click') }}
                            </span>
                        </a>
                    </div>
                @elseif($group->languageItem->slug == 'ru')
                    <input disabled class="text-logo-blue text-base font-normal bg-white w-full mb-4" id="sdolink"  value="{{ $group->coursesItem->sdo_link_ru }}"/>
                    <div class="flex-row flex">
                        <a href="{{ $group->coursesItem->sdo_link_ru }}" target="_blank"
                           class="w-full text-logo-blue flex items-center space-x-2 hover:text-primary-button">
                            <svg width="16" height="16" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M7.60004 15.1666H5.06671C2.14004 15.1666 0.833374 13.86 0.833374 10.9333V8.39996C0.833374 5.47329 2.14004 4.16663 5.06671 4.16663H7.06671C7.34004 4.16663 7.56671 4.39329 7.56671 4.66663C7.56671 4.93996 7.34004 5.16663 7.06671 5.16663H5.06671C2.68004 5.16663 1.83337 6.01329 1.83337 8.39996V10.9333C1.83337 13.32 2.68004 14.1666 5.06671 14.1666H7.60004C9.98671 14.1666 10.8334 13.32 10.8334 10.9333V8.93329C10.8334 8.65996 11.06 8.43329 11.3334 8.43329C11.6067 8.43329 11.8334 8.65996 11.8334 8.93329V10.9333C11.8334 13.86 10.5267 15.1666 7.60004 15.1666Z" fill="currentColor"/>
                                <path d="M11.3333 9.43329H9.19998C7.32665 9.43329 6.56665 8.67329 6.56665 6.79995V4.66662C6.56665 4.46662 6.68665 4.27995 6.87332 4.20662C7.05998 4.12662 7.27332 4.17329 7.41998 4.31329L11.6867 8.57995C11.8267 8.71995 11.8733 8.93995 11.7933 9.12662C11.72 9.31329 11.5333 9.43329 11.3333 9.43329ZM7.56665 5.87329V6.79995C7.56665 8.12662 7.87332 8.43329 9.19998 8.43329H10.1266L7.56665 5.87329Z" fill="currentColor"/>
                                <path d="M10.3999 1.83337H7.73328C7.45994 1.83337 7.23328 1.60671 7.23328 1.33337C7.23328 1.06004 7.45994 0.833374 7.73328 0.833374H10.3999C10.6733 0.833374 10.8999 1.06004 10.8999 1.33337C10.8999 1.60671 10.6733 1.83337 10.3999 1.83337Z" fill="currentColor"/>
                                <path d="M4.66663 3.83337C4.39329 3.83337 4.16663 3.60671 4.16663 3.33337C4.16663 1.95337 5.28663 0.833374 6.66663 0.833374H8.41329C8.68663 0.833374 8.91329 1.06004 8.91329 1.33337C8.91329 1.60671 8.68663 1.83337 8.41329 1.83337H6.66663C5.83996 1.83337 5.16663 2.50671 5.16663 3.33337C5.16663 3.60671 4.93996 3.83337 4.66663 3.83337Z" fill="currentColor"/>
                                <path d="M12.7933 11.8334C12.52 11.8334 12.2933 11.6067 12.2933 11.3334C12.2933 11.06 12.52 10.8334 12.7933 10.8334C13.5533 10.8334 14.1667 10.2134 14.1667 9.46004V5.33337C14.1667 5.06004 14.3933 4.83337 14.6667 4.83337C14.94 4.83337 15.1667 5.06004 15.1667 5.33337V9.46004C15.1667 10.7667 14.1 11.8334 12.7933 11.8334Z" fill="currentColor"/>
                                <path d="M14.6666 5.83337H12.6666C10.8933 5.83337 10.1666 5.1067 10.1666 3.33337V1.33337C10.1666 1.13337 10.2866 0.9467 10.4733 0.873366C10.66 0.793366 10.8733 0.840033 11.02 0.980033L15.02 4.98003C15.16 5.12003 15.2066 5.34003 15.1266 5.5267C15.0533 5.71337 14.8666 5.83337 14.6666 5.83337ZM11.1666 2.54003V3.33337C11.1666 4.55337 11.4466 4.83337 12.6666 4.83337H13.46L11.1666 2.54003Z" fill="currentColor"/>
                            </svg>
                            <span>
                            {{ __('trainer.groups.sdo.link.click') }}
                            </span>
                        </a>
                    </div>
                @elseif($group->languageItem->slug == 'en')
                    <input disabled class="text-logo-blue text-base font-normal bg-white w-full mb-4" id="sdolink"  value="{{ $group->coursesItem->sdo_link_en }}"/>
                    <div class="flex-row flex">
                        <a href="{{ $group->coursesItem->sdo_link_en }}" target="_blank"
                           class="w-full text-logo-blue flex items-center space-x-2 hover:text-primary-button">
                            <svg width="16" height="16" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M7.60004 15.1666H5.06671C2.14004 15.1666 0.833374 13.86 0.833374 10.9333V8.39996C0.833374 5.47329 2.14004 4.16663 5.06671 4.16663H7.06671C7.34004 4.16663 7.56671 4.39329 7.56671 4.66663C7.56671 4.93996 7.34004 5.16663 7.06671 5.16663H5.06671C2.68004 5.16663 1.83337 6.01329 1.83337 8.39996V10.9333C1.83337 13.32 2.68004 14.1666 5.06671 14.1666H7.60004C9.98671 14.1666 10.8334 13.32 10.8334 10.9333V8.93329C10.8334 8.65996 11.06 8.43329 11.3334 8.43329C11.6067 8.43329 11.8334 8.65996 11.8334 8.93329V10.9333C11.8334 13.86 10.5267 15.1666 7.60004 15.1666Z" fill="currentColor"/>
                                <path d="M11.3333 9.43329H9.19998C7.32665 9.43329 6.56665 8.67329 6.56665 6.79995V4.66662C6.56665 4.46662 6.68665 4.27995 6.87332 4.20662C7.05998 4.12662 7.27332 4.17329 7.41998 4.31329L11.6867 8.57995C11.8267 8.71995 11.8733 8.93995 11.7933 9.12662C11.72 9.31329 11.5333 9.43329 11.3333 9.43329ZM7.56665 5.87329V6.79995C7.56665 8.12662 7.87332 8.43329 9.19998 8.43329H10.1266L7.56665 5.87329Z" fill="currentColor"/>
                                <path d="M10.3999 1.83337H7.73328C7.45994 1.83337 7.23328 1.60671 7.23328 1.33337C7.23328 1.06004 7.45994 0.833374 7.73328 0.833374H10.3999C10.6733 0.833374 10.8999 1.06004 10.8999 1.33337C10.8999 1.60671 10.6733 1.83337 10.3999 1.83337Z" fill="currentColor"/>
                                <path d="M4.66663 3.83337C4.39329 3.83337 4.16663 3.60671 4.16663 3.33337C4.16663 1.95337 5.28663 0.833374 6.66663 0.833374H8.41329C8.68663 0.833374 8.91329 1.06004 8.91329 1.33337C8.91329 1.60671 8.68663 1.83337 8.41329 1.83337H6.66663C5.83996 1.83337 5.16663 2.50671 5.16663 3.33337C5.16663 3.60671 4.93996 3.83337 4.66663 3.83337Z" fill="currentColor"/>
                                <path d="M12.7933 11.8334C12.52 11.8334 12.2933 11.6067 12.2933 11.3334C12.2933 11.06 12.52 10.8334 12.7933 10.8334C13.5533 10.8334 14.1667 10.2134 14.1667 9.46004V5.33337C14.1667 5.06004 14.3933 4.83337 14.6667 4.83337C14.94 4.83337 15.1667 5.06004 15.1667 5.33337V9.46004C15.1667 10.7667 14.1 11.8334 12.7933 11.8334Z" fill="currentColor"/>
                                <path d="M14.6666 5.83337H12.6666C10.8933 5.83337 10.1666 5.1067 10.1666 3.33337V1.33337C10.1666 1.13337 10.2866 0.9467 10.4733 0.873366C10.66 0.793366 10.8733 0.840033 11.02 0.980033L15.02 4.98003C15.16 5.12003 15.2066 5.34003 15.1266 5.5267C15.0533 5.71337 14.8666 5.83337 14.6666 5.83337ZM11.1666 2.54003V3.33337C11.1666 4.55337 11.4466 4.83337 12.6666 4.83337H13.46L11.1666 2.54003Z" fill="currentColor"/>
                            </svg>
                            <span>
                            {{ __('trainer.groups.sdo.link.click') }}
                            </span>
                        </a>
                    </div>
                @endif
            </div>
        </div>
    @endif
    @if($statements->isNotEmpty())
    <div class="w-full bg-white lg:bg-gray-bg p-0 lg:p-6 rounded-lg mb-8 lg:mb-6">
        <div class="w-full text-lg lg:text-xl font-bold mb-5">{{ __('listener.group.attendance') }}</div>
        <div class="relative">
            <div class="flex flex-col lg:table w-full">
                <div class="hidden lg:table-header-group bg-white text-main-black font-normal text-base">
                    <div class="flex flex-col w-full lg:table-row">
                        <div class="w-full flex lg:w-min lg:table-cell text-left py-3 px-4 font-normal"><div class="w-48 ">{{ __('listener.table.head.date') }}</div></div>
                        <div class="w-full flex lg:w-min lg:table-cell text-left py-3 px-4 font-normal"><div>{{ __('listener.table.head.type') }}</div></div>
                        <div class="w-full flex lg:table-cell text-left py-3 px-4 font-normal"><div class="w-40 ">{{ __('listener.table.head.status') }}</div></div>
                        <div class="w-full flex lg:table-cell text-left py-3 px-4 font-normal"><div class="w-40 ">{{ __('listener.table.head.action') }}</div></div>
                    </div>
                </div>
                <div class="flex flex-col w-full lg:table-row-group space-y-2">
                    @foreach($statements as $statement)
                        <div class="flex flex-wrap items-center rounded lg:rounded-none w-full lg:table-row border lg:border-0 lg:border-b border-messages-hover bg-white lg:bg-gray-bg hover:bg-table-hover cursor-pointer" @if($statement->status == \App\Models\Statements::STATEMENTS_MISSING or $statement->status == \App\Models\Statements::STATEMENTS_MISSING_DECLINE ) wire:click="attachdocs({{ $statement->id }})"@endif>
                            <div class="w-full flex lg:w-min lg:table-cell text-lg lg:text-xs text-main-black lg:text-user-gray text-center lg:text-left py-3 px-4"><div class="lg:w-48 flex flex-col font-bold lg:font-normal"><span>{{ date('d.m.Y',strtotime($statement->available_at)) }}</span></div></div>
                            <div class="w-full flex lg:w-min lg:table-cell text-left py-3 px-4"><div class="flex mr-8">{{ __('listener.table.body.type.missing') }}</div></div>
                            @if($statement->status == \App\Models\Statements::STATEMENTS_MISSING)
                                <div class="w-full lg:whitespace-nowrap flex lg:table-cell text-statement-red text-left py-3 px-4">{{ __('listener.table.body.type.add.docs') }}</div>
                                <div class="w-full flex lg:table-cell font-normal text-statement-blue text-left py-3 px-4 hover:text-blue-900">{{ __('listener.links.add') }}</div>
                            @elseif($statement->status == \App\Models\Statements::STATEMENTS_MISSING_SENT)
                                <div class="w-full lg:whitespace-nowrap flex lg:table-cell text-statement-blue text-left py-3 px-4">{{ __('listener.table.body.type.add.sent') }}</div>
                                <div class="hidden lg:table-cell text-left py-3 px-4"></div>
                            @elseif($statement->status == \App\Models\Statements::STATEMENTS_MISSING_DECLINE)
                                <div class="w-full lg:whitespace-nowrap flex lg:table-cell text-statement-red text-left py-3 px-4">{{ __('listener.table.body.type.add.decline') }}</div>
                                <div class="w-full flex lg:table-cell font-normal text-statement-blue text-left py-3 px-4 hover:text-blue-900">{{ __('listener.links.add') }}</div>
                            @elseif($statement->status == \App\Models\Statements::STATEMENTS_SICK)
                                <div class="w-full lg:whitespace-nowrap flex lg:table-cell text-statement-green text-left py-3 px-4">{{ __('listener.table.body.type.valid') }}</div>
                                <div class="hidden lg:table-cell text-left py-3 px-4"></div>
                            @endif
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
    @endif


    @if($group->coursesItem->file_1_listener or $group->coursesItem->file_2_listener)
    <div class="w-full bg-white lg:bg-gray-bg p-0 lg:p-6 rounded-lg mb-8 lg:mb-6">
        <div class="w-full text-lg lg:text-xl font-bold mb-5">{{ __('listener.group.docs') }}</div>
        <div class="w-full lg:w-3/4 lg:px-4 py-4 flex flex-col bg-white rounded-lg">
            @if($group->coursesItem->file_1_listener)
                <div class="w-full flex flex-col mb-4">
                    <div class="text-logo-blue text-base font-normal w-full mb-1">Файл.{{ pathinfo($group->coursesItem->file_1_listener, PATHINFO_EXTENSION) }}</div>
                    <a
                    class="w-full text-logo-blue flex items-center space-x-2 hover:text-primary-button"
                    href="{{ $group->coursesItem->getFileUrl($group->coursesItem->file_1_listener) }}"
                    download="">
                        <svg width="16" height="16" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M7.91997 9.99333C7.7933 9.99333 7.66663 9.94666 7.56663 9.84666L5.85997 8.14C5.66663 7.94666 5.66663 7.62666 5.85997 7.43333C6.0533 7.24 6.3733 7.24 6.56663 7.43333L7.91997 8.78666L9.2733 7.43333C9.46663 7.24 9.78663 7.24 9.97997 7.43333C10.1733 7.62666 10.1733 7.94666 9.97997 8.14L8.2733 9.84666C8.1733 9.94666 8.04663 9.99333 7.91997 9.99333Z" fill="currentColor"/>
                            <path d="M7.92004 9.94675C7.64671 9.94675 7.42004 9.72008 7.42004 9.44675V2.66675C7.42004 2.39341 7.64671 2.16675 7.92004 2.16675C8.19338 2.16675 8.42004 2.39341 8.42004 2.66675V9.44675C8.42004 9.72008 8.19338 9.94675 7.92004 9.94675Z" fill="currentColor"/>
                            <path d="M7.99996 13.9535C4.56663 13.9535 2.16663 11.5535 2.16663 8.12012C2.16663 7.84678 2.39329 7.62012 2.66663 7.62012C2.93996 7.62012 3.16663 7.84678 3.16663 8.12012C3.16663 10.9668 5.15329 12.9535 7.99996 12.9535C10.8466 12.9535 12.8333 10.9668 12.8333 8.12012C12.8333 7.84678 13.06 7.62012 13.3333 7.62012C13.6066 7.62012 13.8333 7.84678 13.8333 8.12012C13.8333 11.5535 11.4333 13.9535 7.99996 13.9535Z" fill="currentColor"/>
                        </svg>
                        <span>Скачать</span>
                    </a>
                </div>
            @endif

            @if($group->coursesItem->file_2_listener)
                <div class="w-full flex flex-col mb-4">
                    <div class="text-logo-blue text-base font-normal w-full mb-1">Файл.{{ pathinfo($group->coursesItem->file_2_listener, PATHINFO_EXTENSION) }}</div>
                    <a
                        class="w-full text-logo-blue flex items-center space-x-2 hover:text-primary-button"
                        href="{{ $group->coursesItem->getFileUrl($group->coursesItem->file_2_listener) }}"
                        download="">
                        <svg width="16" height="16" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M7.91997 9.99333C7.7933 9.99333 7.66663 9.94666 7.56663 9.84666L5.85997 8.14C5.66663 7.94666 5.66663 7.62666 5.85997 7.43333C6.0533 7.24 6.3733 7.24 6.56663 7.43333L7.91997 8.78666L9.2733 7.43333C9.46663 7.24 9.78663 7.24 9.97997 7.43333C10.1733 7.62666 10.1733 7.94666 9.97997 8.14L8.2733 9.84666C8.1733 9.94666 8.04663 9.99333 7.91997 9.99333Z" fill="currentColor"/>
                            <path d="M7.92004 9.94675C7.64671 9.94675 7.42004 9.72008 7.42004 9.44675V2.66675C7.42004 2.39341 7.64671 2.16675 7.92004 2.16675C8.19338 2.16675 8.42004 2.39341 8.42004 2.66675V9.44675C8.42004 9.72008 8.19338 9.94675 7.92004 9.94675Z" fill="currentColor"/>
                            <path d="M7.99996 13.9535C4.56663 13.9535 2.16663 11.5535 2.16663 8.12012C2.16663 7.84678 2.39329 7.62012 2.66663 7.62012C2.93996 7.62012 3.16663 7.84678 3.16663 8.12012C3.16663 10.9668 5.15329 12.9535 7.99996 12.9535C10.8466 12.9535 12.8333 10.9668 12.8333 8.12012C12.8333 7.84678 13.06 7.62012 13.3333 7.62012C13.6066 7.62012 13.8333 7.84678 13.8333 8.12012C13.8333 11.5535 11.4333 13.9535 7.99996 13.9535Z" fill="currentColor"/>
                        </svg>
                        <span>Скачать</span>
                    </a>
                </div>
            @endif
        </div>
    </div>
    @endif

    @if($certificates->count() > 0)
    <div class="w-full bg-white lg:bg-gray-bg p-0 lg:p-6 rounded-lg mb-8 lg:mb-6">
        <div class="w-full text-lg lg:text-xl font-bold mb-5">{{ __('listener.group.certificate') }}</div>
        <div class="relative mb-12 lg:mb-0">
            <div class="flex flex-col lg:table w-full">
                <div class="hidden lg:table-header-group bg-white text-main-black font-normal text-base">
                    <div class="flex flex-col w-full lg:table-row">
                        <div class="w-full flex lg:w-min lg:table-cell text-left py-3 px-4 font-normal"><div>{{ __('Тип') }}</div></div>
                        <div class="w-full flex lg:w-min lg:table-cell text-left py-3 px-4 font-normal"><div>{{ __('listener.table.head.program') }}</div></div>
                        <div class="w-full flex lg:table-cell text-left py-3 px-4 font-normal"><div>{{ __('listener.table.head.for') }}</div></div>
                        <div class="w-full flex lg:w-min lg:table-cell text-left py-3 px-4 font-normal"><div>{{ __('listener.table.head.created') }}</div></div>
                        <div class="w-full flex lg:table-cell text-left py-3 px-4 font-normal"><div class="w-40 ">{{ __('listener.table.head.action') }}</div></div>
                    </div>
                </div>
                <div class="flex flex-col w-full lg:table-row-group space-y-2">
                    @foreach($certificates as $item)
                        <div class="flex flex-wrap items-center rounded lg:rounded-none w-full lg:table-row border lg:border-0 lg:border-b border-messages-hover bg-white lg:bg-gray-bg hover:bg-table-hover cursor-pointer">
                            <div class="w-full flex lg:w-min lg:table-cell text-lg lg:text-xs text-main-black lg:text-user-gray text-center lg:text-left py-3 px-4"><div class="flex flex-col font-bold lg:font-normal">{{ \App\Models\Certificates::listTypes()[$item->type] }}</div></div>
                            <div class="w-full flex lg:w-full lg:table-cell text-left py-3 px-4"><div class="flex">{{ $item->course_title }}</div></div>
                            <div class="w-full flex lg:w-min lg:table-cell text-left py-3 px-4"><div class="flex lg:whitespace-nowrap">{{ $item->fio }}</div></div>
                            <div class="w-full flex lg:w-min lg:table-cell text-lg lg:text-xs text-main-black lg:text-user-gray text-center lg:text-left py-3 px-4"><div class="flex flex-col font-bold lg:font-normal"><span>{{ date('d.m.Y',strtotime($item->created_at)) }}</span></div></div>
                            @if($item->pdf)
                                <div class="w-full lg:whitespace-nowrap flex lg:table-cell text-statement-blue text-left py-3 px-4"><a href="{{ $item->pdf }}" download="{{ $item->group_id }}.pdf" target="_blank">{{ __('listener.links.download') }}</a></div>
                            @else
                                <div class="w-full lg:whitespace-nowrap flex lg:table-cell text-statement-blue text-left py-3 px-4">На обработке</div>
                            @endif
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
    @endif

    @if($modalattach)
        <x-modal>
            <x-slot name="header">
                {{ __('Прикрепить документы') }}
                @if($decline)<p class="text-red-600">Вам отказано, прикрепите новые документы</p>@endif
                <br><br>
            </x-slot>
            <div class="flex flex-col items-start md:items-center w-full md:w-3/4 mx-auto space-y-4">


                <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-center">
                    <div class="w-full md:w-1/4">
                        <label for="patronymic"
                               class="w-full text-left md:text-right block text-sm">{{ __('admin.users.edit.iin') }}</label>
                    </div>
                    <div class="w-full md:w-3/4">
                        <span>{{ auth()->user()->iin }}</span>
                    </div>
                </div>

                <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-center">
                    <div class="w-full md:w-1/4">
                        <label for="patronymic"
                               class="w-full text-left md:text-right block text-sm">{{ __('admin.users.edit.fio') }}</label>
                    </div>
                    <div class="w-full md:w-3/4">
                        <span>{{ auth()->user()->fio }}</span>
                    </div>
                </div>

                <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-center">
                    <div class="w-full md:w-1/4">
                        <label for="patronymic"
                               class="w-full text-left md:text-right block text-sm">{{ __('admin.groups.add.modal.cifr') }}</label>
                    </div>
                    <div class="w-full md:w-3/4">
                        <span>{{ $group->slug }}</span>
                    </div>
                </div>

                <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-center">
                    <div class="w-full md:w-1/4">
                        <label for="patronymic"
                               class="w-full text-left md:text-right block text-sm">{{ __('admin.groups.add.modal.trainer') }}</label>
                    </div>
                    <div class="w-full md:w-3/4">
                        <span>{{ $group->trainerItem->fio }}</span>
                    </div>
                </div>

                <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-center">
                    <div class="w-full md:w-1/4">
                        <label for="patronymic"
                               class="w-full text-left md:text-right block text-sm">{{ __('admin.statements-messages.available_at.title') }}</label>
                    </div>
                    <div class="w-full md:w-3/4">
                        <span>{{ date('d.m.Y', strtotime($modalid->available_at)) }}</span>
                    </div>
                </div>

                <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-center">
                    <div class="w-full md:w-1/4">
                        <label for="patronymic"
                               class="w-full text-left md:text-right block text-sm">{{ __('admin.groups.add.modal.affiliate') }}</label>
                    </div>
                    <div class="w-full md:w-3/4">
                        <span>{{ $group->affiliateItem->title }}</span>
                    </div>
                </div>

                <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-center">
                    <div class="w-full md:w-1/4">
                        <label for="description"
                               class="w-full text-left md:text-right block text-sm">{{ __('admin.statements-messages.description') }}</label>
                    </div>
                    <div class="w-full md:w-3/4">
                    <textarea wire:model.defer="description" type="text"
                              class="w-full md:w-3/4 px-3 py-1 border rounded focus:border-blue-500 hover:border-blue-500 text-sm outline-none focus:input-shadow"
                              rows="3"
                    ></textarea>
                        @error('description')
                        <p class="text-xs text-red-600">{{ $message }}</p>
                        @enderror
                    </div>
                </div>

                <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-center">
                    <div class="w-full md:w-1/4">
                        <label for="file"
                               class="w-full text-left md:text-right block text-sm">{{ __('admin.statements-messages.file') }}
                            :</label>
                    </div>
                    <div class="w-full md:w-3/4">
                        <input type="file" wire:model.defer="file" id="file"
                               class="w-full md:w-3/4 px-3 py-1 border rounded focus:border-blue-500 hover:border-blue-500 text-sm outline-none focus:input-shadow"
                               value="{{ old('file', $file) }}"/>
                        <svg wire:loading wire:target="file" role="status" class="w-8 h-8 mr-2 text-gray-200 animate-spin dark:text-gray-600 fill-blue-600" viewBox="0 0 100 101" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M100 50.5908C100 78.2051 77.6142 100.591 50 100.591C22.3858 100.591 0 78.2051 0 50.5908C0 22.9766 22.3858 0.59082 50 0.59082C77.6142 0.59082 100 22.9766 100 50.5908ZM9.08144 50.5908C9.08144 73.1895 27.4013 91.5094 50 91.5094C72.5987 91.5094 90.9186 73.1895 90.9186 50.5908C90.9186 27.9921 72.5987 9.67226 50 9.67226C27.4013 9.67226 9.08144 27.9921 9.08144 50.5908Z" fill="currentColor"/>
                            <path d="M93.9676 39.0409C96.393 38.4038 97.8624 35.9116 97.0079 33.5539C95.2932 28.8227 92.871 24.3692 89.8167 20.348C85.8452 15.1192 80.8826 10.7238 75.2124 7.41289C69.5422 4.10194 63.2754 1.94025 56.7698 1.05124C51.7666 0.367541 46.6976 0.446843 41.7345 1.27873C39.2613 1.69328 37.813 4.19778 38.4501 6.62326C39.0873 9.04874 41.5694 10.4717 44.0505 10.1071C47.8511 9.54855 51.7191 9.52689 55.5402 10.0491C60.8642 10.7766 65.9928 12.5457 70.6331 15.2552C75.2735 17.9648 79.3347 21.5619 82.5849 25.841C84.9175 28.9121 86.7997 32.2913 88.1811 35.8758C89.083 38.2158 91.5421 39.6781 93.9676 39.0409Z" fill="currentFill"/>
                        </svg>

                        @error('file')
                        <p class="text-xs text-red-600">{{ $message }}</p>
                        @enderror
                    </div>
                </div>


                <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-center">
                    <div class="w-full md:w-1/4">
                    </div>
                    <div class="w-full md:w-3/4 space-x-2 flex">
                        <button type="submit" wire:click="attachfiles({{ $modalid }})"
                                class="mb-2 text-sm flex justify-center items-center px-4 py-1 border-0 bg-blue-500 text-white rounded hover:bg-blue-600">
                            {{ __('admin.buttons.submit') }}
                        </button>
                        <button wire:click="$set('modalattach',false)"
                                class="py-1 px-4 border inline-block m-0 mb-2 text-sm rounded hover:border-black">{{ __('admin.buttons.cancel') }}
                        </button>
                    </div>
                </div>
            </div>
        </x-modal>
    @endif
</div>



@push('scripts')
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>

    <script type="text/javascript">
        document.addEventListener('DOMContentLoaded', function () {
        @this.on('messageExists', () => {
            Swal.fire({
                title: 'Вы уже прикрепляли документы!',
                icon: 'success',
                width: 600,
                position: 'top-end',
                showConfirmButton: false,
                timer: 1000
            });
        });
        })
    </script>
@endpush

