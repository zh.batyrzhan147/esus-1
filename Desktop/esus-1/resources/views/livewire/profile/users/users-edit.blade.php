<div xmlns:wire="http://www.w3.org/1999/xhtml" class="px-4 py-8 lg:px-14 lg:py-12 bg-white w-full rounded-lg">
    <div class="w-full flex flex-col">
        <a href="{{ route('user.profile') }}" class="w-full flex space-x-1.5 items-center text-main-black text-base hover:text-logo-blue mb-3 lg:mb-5">
            <svg width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path d="M7.97495 15.6833C7.81662 15.6833 7.65828 15.625 7.53328 15.5L2.47495 10.4416C2.23328 10.2 2.23328 9.79998 2.47495 9.55831L7.53328 4.49998C7.77495 4.25831 8.17495 4.25831 8.41662 4.49998C8.65829 4.74164 8.65829 5.14164 8.41662 5.38331L3.79995 9.99998L8.41662 14.6166C8.65829 14.8583 8.65829 15.2583 8.41662 15.5C8.29995 15.625 8.13328 15.6833 7.97495 15.6833Z" fill="currentColor"/>
                <path d="M17.0833 10.625H3.05835C2.71668 10.625 2.43335 10.3417 2.43335 10C2.43335 9.65833 2.71668 9.375 3.05835 9.375H17.0833C17.425 9.375 17.7083 9.65833 17.7083 10C17.7083 10.3417 17.425 10.625 17.0833 10.625Z" fill="currentColor"/>
            </svg>
            <span>{{ __('listener.profile.title') }}</span>
        </a>
        <h2 class="font-bold text-xl lg:text-2xl text-main-black mb-3 lg:mb-5">{{ __('admin.users.edit.title') }}</h2>

        <div class="w-full flex flex-col mb-8">
            <div class="w-full mb-3 lg:mb-4 text-main-black text-base font-normal">{{ __('listener.profile.account.personal') }}</div>
            <div class="w-full grid grid-cols-1 lg:grid-cols-2 lg:gap-x-10 gap-y-4 lg:gap-y-6">
                <div>
                    <div class="relative">
                        <input type="text" id="iin" aria-describedby="iin_help" class="block rounded-lg px-4 pb-3 pt-7 w-full text-base text-main-black bg-gray-bg border border-soft-gray appearance-none focus:outline-none focus:ring-0 focus:border-main-black peer cursor-not-allowed" value="{{ $user->iin }}" disabled/>
                        <label for="iin" class="absolute text-sm text-gray-500 peer-focus:text-main-black duration-300 transform -translate-y-4 scale-75 top-6 z-10 origin-0 left-4 peer-placeholder-shown:scale-100 peer-placeholder-shown:translate-y-0 peer-focus:scale-75 peer-focus:-translate-y-4">{{ __('admin.users.edit.iin') }}</label>
                    </div>
                    <p id="iin_help" class="mt-2 pl-2 text-xs text-gray-500">{{ __('listener.profile.account.iin.edit') }}</p>
                </div>
                <div>
                    <div class="relative">
                        <input type="text" id="last_name" aria-describedby="last_name_help" class="block rounded-lg px-4 pb-3 pt-7 w-full text-base text-main-black bg-gray-bg border border-soft-gray appearance-none focus:outline-none focus:ring-0 focus:border-main-black peer"
                               wire:model="user.last_name" placeholder=" "
                        />
                        <label for="last_name" class="absolute text-sm text-gray-500 cursor-text peer-focus:text-main-black duration-300 transform -translate-y-4 scale-75 top-6 z-10 origin-0 left-4 peer-placeholder-shown:scale-100 peer-placeholder-shown:translate-y-0 peer-focus:scale-75 peer-focus:-translate-y-4">{{ __('admin.users.edit.last_name') }}</label>
                    </div>
                    @error('user.last_name')
                    <p id="last_name_help" class="mt-2 pl-2 text-xs text-red-600">{{ $message }}</p>
                    @enderror
                </div>
                <div>
                    <div class="relative">
                        <input type="text" id="name" aria-describedby="name_help" class="block rounded-lg px-4 pb-3 pt-7 w-full text-base text-main-black bg-gray-bg border border-soft-gray appearance-none focus:outline-none focus:ring-0 focus:border-main-black peer"
                               wire:model="user.name" placeholder=" "
                        />
                        <label for="name" class="absolute text-sm text-gray-500 cursor-text peer-focus:text-main-black duration-300 transform -translate-y-4 scale-75 top-6 z-10 origin-0 left-4 peer-placeholder-shown:scale-100 peer-placeholder-shown:translate-y-0 peer-focus:scale-75 peer-focus:-translate-y-4">{{ __('admin.users.edit.name') }}</label>
                    </div>
                    @error('user.name')
                    <p id="name_help" class="mt-2 pl-2 text-xs text-red-600">{{ $message }}</p>
                    @enderror
                </div>
                <div>
                    <div class="relative">
                        <input type="text" id="patronymic" aria-describedby="patronymic_help" class="block rounded-lg px-4 pb-3 pt-7 w-full text-base text-main-black bg-gray-bg border border-soft-gray appearance-none focus:outline-none focus:ring-0 focus:border-main-black peer"
                               wire:model="user.patronymic" placeholder=" "
                        />
                        <label for="patronymic" class="absolute text-sm text-gray-500 cursor-text peer-focus:text-main-black duration-300 transform -translate-y-4 scale-75 top-6 z-10 origin-0 left-4 peer-placeholder-shown:scale-100 peer-placeholder-shown:translate-y-0 peer-focus:scale-75 peer-focus:-translate-y-4">{{ __('admin.users.edit.patronymic') }}</label>
                    </div>
                    @error('user.patronymic')
                    <p id="patronymic_help" class="mt-2 pl-2 text-xs text-red-600">{{ $message }}</p>
                    @enderror
                </div>
                <div>
                    <div class="relative">
                        <input type="text" id="last_name_lat" aria-describedby="last_name_lat_help" class="block rounded-lg px-4 pb-3 pt-7 w-full text-base text-main-black bg-gray-bg border border-soft-gray appearance-none focus:outline-none focus:ring-0 focus:border-main-black peer"
                               wire:model="user.last_name_lat" placeholder=" "
                        />
                        <label for="last_name_lat" class="absolute text-sm text-gray-500 cursor-text peer-focus:text-main-black duration-300 transform -translate-y-4 scale-75 top-6 z-10 origin-0 left-4 peer-placeholder-shown:scale-100 peer-placeholder-shown:translate-y-0 peer-focus:scale-75 peer-focus:-translate-y-4">{{ __('admin.users.edit.last_name.lat') }}</label>
                    </div>
                    @error('user.last_name_lat')
                    <p id="last_name_lat_help" class="mt-2 pl-2 text-xs text-red-600">{{ $message }}</p>
                    @enderror
                </div>
                <div>
                    <div class="relative">
                        <input type="text" id="name_lat" aria-describedby="name_lat_help" class="block rounded-lg px-4 pb-3 pt-7 w-full text-base text-main-black bg-gray-bg border border-soft-gray appearance-none focus:outline-none focus:ring-0 focus:border-main-black peer"
                               wire:model="user.name_lat" placeholder=" "
                        />
                        <label for="name_lat" class="absolute text-sm text-gray-500 cursor-text peer-focus:text-main-black duration-300 transform -translate-y-4 scale-75 top-6 z-10 origin-0 left-4 peer-placeholder-shown:scale-100 peer-placeholder-shown:translate-y-0 peer-focus:scale-75 peer-focus:-translate-y-4">{{ __('admin.users.edit.name.lat') }}</label>
                    </div>
                    @error('user.name_lat')
                    <p id="name_lat_help" class="mt-2 pl-2 text-xs text-red-600">{{ $message }}</p>
                    @enderror
                </div>
                <div>
                    <div class="relative">
                        <input type="email" id="email" aria-describedby="email_help" class="block rounded-lg px-4 pb-3 pt-7 w-full text-base text-main-black bg-gray-bg border border-soft-gray appearance-none focus:outline-none focus:ring-0 focus:border-main-black peer"
                               wire:model="user.email" placeholder=" "
                        />
                        <label for="email" class="absolute text-sm text-gray-500 cursor-text peer-focus:text-main-black duration-300 transform -translate-y-4 scale-75 top-6 z-10 origin-0 left-4 peer-placeholder-shown:scale-100 peer-placeholder-shown:translate-y-0 peer-focus:scale-75 peer-focus:-translate-y-4">{{ __('admin.users.edit.email') }}</label>
                    </div>
                    @error('user.email')
                    <p id="email_help" class="mt-2 pl-2 text-xs text-red-600">{{ $message }}</p>
                    @enderror
                </div>
                <div>
                    <div class="relative">
                        <input type="text" name="phone" id="phone" aria-describedby="phone_help" class="block rounded-lg px-4 pb-3 pt-7 w-full text-base text-main-black bg-gray-bg border border-soft-gray appearance-none focus:outline-none focus:ring-0 focus:border-main-black peer"
                               wire:model="user.phone" placeholder=" "
                        />
                        <label for="phone" class="absolute text-sm text-gray-500 cursor-text peer-focus:text-main-black duration-300 transform -translate-y-4 scale-75 top-6 z-10 origin-0 left-4 peer-placeholder-shown:scale-100 peer-placeholder-shown:translate-y-0 peer-focus:scale-75 peer-focus:-translate-y-4">{{ __('admin.users.edit.phone') }}</label>
                    </div>
                    @error('user.phone')
                    <p id="phone_help" class="mt-2 pl-2 text-xs text-red-600">{{ $message }}</p>
                    @enderror
                </div>
                <div>
                    <div class="relative">
                        <select id="gender" aria-describedby="gender_help" class="block rounded-lg px-4 pb-3 pt-7 w-full text-base text-main-black bg-gray-bg border border-soft-gray appearance-none focus:outline-none focus:ring-0 focus:border-main-black peer"
                               wire:model="user.gender" placeholder=" "
                        >
                            <option value="">{{ __('admin.users.edit.choosesex') }}</option>
                            @foreach(\App\Models\Genders::all() ?? []  as $item)
                                <option value="{{ $item->id }}">{{ $item->title }}</option>
                            @endforeach
                        </select>
                        <label for="gender" class="absolute text-sm text-gray-500 cursor-text peer-focus:text-main-black duration-300 transform -translate-y-4 scale-75 top-6 z-10 origin-0 left-4 peer-placeholder-shown:scale-100 peer-placeholder-shown:translate-y-0 peer-focus:scale-75 peer-focus:-translate-y-4">{{ __('admin.users.edit.sex') }}</label>
                    </div>
                    @error('user.gender')
                    <p id="gender_help" class="mt-2 pl-2 text-xs text-red-600">{{ $message }}</p>
                    @enderror
                </div>
                <div>
                    <div class="relative">
                        <select id="nationality" aria-describedby="nationality_help" class="block rounded-lg px-4 pb-3 pt-7 w-full text-base text-main-black bg-gray-bg border border-soft-gray appearance-none focus:outline-none focus:ring-0 focus:border-main-black peer"
                               wire:model="user.nationality" placeholder=" "
                        >
                            <option value="">{{ __('admin.users.edit.choosenationality') }}</option>
                            @foreach(\App\Models\Nationalities::all() ?? []  as $item)
                                <option value="{{ $item->id }}">{{ $item->ru_title }}</option>
                            @endforeach
                        </select>
                        <label for="nationality" class="absolute text-sm text-gray-500 cursor-text peer-focus:text-main-black duration-300 transform -translate-y-4 scale-75 top-6 z-10 origin-0 left-4 peer-placeholder-shown:scale-100 peer-placeholder-shown:translate-y-0 peer-focus:scale-75 peer-focus:-translate-y-4">{{ __('admin.users.edit.nationality') }}</label>
                    </div>
                    @error('user.nationality')
                    <p id="nationality_help" class="mt-2 pl-2 text-xs text-red-600">{{ $message }}</p>
                    @enderror
                </div>
            </div>
        </div>
        <div class="w-full flex flex-col mb-8">
            <div class="w-full mb-3 lg:mb-4 text-main-black text-base font-normal">{{ __('listener.profile.account.secondary') }}</div>
            <div class="w-full grid grid-cols-1 lg:grid-cols-2 lg:gap-x-10 gap-y-4 lg:gap-y-6">
                <div>
                    <div class="relative">
                        <select id="oblast" aria-describedby="oblast_help" class="block rounded-lg px-4 pb-3 pt-7 w-full text-base text-main-black bg-gray-bg border border-soft-gray appearance-none focus:outline-none focus:ring-0 focus:border-main-black peer"
                               wire:model="oblast" placeholder=" "
                        >
                            <option value="">{{ __('admin.users.edit.chooseobl') }}</option>
                            @foreach(\App\Models\Locations::where('type', \App\Models\Locations::LOCATION_OBLAST)->get() ?? []  as $item)
                                <option value="{{ $item->id }}">{{ $item->ru_title }}</option>
                            @endforeach
                        </select>
                        <label for="oblast" class="absolute text-sm text-gray-500 cursor-text peer-focus:text-main-black duration-300 transform -translate-y-4 scale-75 top-6 z-10 origin-0 left-4 peer-placeholder-shown:scale-100 peer-placeholder-shown:translate-y-0 peer-focus:scale-75 peer-focus:-translate-y-4">{{ __('admin.users.edit.obl') }}</label>
                    </div>
                    @error('oblast')
                    <p id="oblast_help" class="mt-2 pl-2 text-xs text-red-600">{{ $message }}</p>
                    @enderror
                </div>
                @if($oblast)
                    <div>
                        <div class="relative">
                            <select id="raion" aria-describedby="raion_help" class="block rounded-lg px-4 pb-3 pt-7 w-full text-base text-main-black bg-gray-bg border border-soft-gray appearance-none focus:outline-none focus:ring-0 focus:border-main-black peer"
                                   wire:model="raion" placeholder=" "
                            >
                                <option value="">{{ __('admin.users.edit.chooserai') }}</option>
                                @foreach(\App\Models\Locations::where('type', \App\Models\Locations::LOCATION_RAYON)->where('parent',$oblast)->get() ?? []  as $item)
                                    <option value="{{ $item->id }}">{{ $item->ru_title }}</option>
                                @endforeach
                            </select>
                            <label for="raion" class="absolute text-sm text-gray-500 cursor-text peer-focus:text-main-black duration-300 transform -translate-y-4 scale-75 top-6 z-10 origin-0 left-4 peer-placeholder-shown:scale-100 peer-placeholder-shown:translate-y-0 peer-focus:scale-75 peer-focus:-translate-y-4">{{ __('admin.users.edit.rai') }}</label>
                        </div>
                        @error('raion')
                        <p id="raion_help" class="mt-2 pl-2 text-xs text-red-600">{{ $message }}</p>
                        @enderror
                    </div>
                    @if($raion)
{{--                        <div>--}}
{{--                            <div class="relative">--}}
{{--                                <select id="gorod" aria-describedby="gorod_help" class="block rounded-lg px-4 pb-3 pt-7 w-full text-base text-main-black bg-gray-bg border border-soft-gray appearance-none focus:outline-none focus:ring-0 focus:border-main-black peer"--}}
{{--                                       wire:model="gorod" placeholder=" "--}}
{{--                                >--}}
{{--                                    <option value="">{{ __('admin.users.edit.choosecity') }}</option>--}}
{{--                                    @foreach(\App\Models\Locations::where('type', \App\Models\Locations::LOCATION_AUL)->where('parent',$raion)->get() ?? []  as $item)--}}
{{--                                        <option value="{{ $item->id }}">{{ $item->ru_title }}</option>--}}
{{--                                    @endforeach--}}
{{--                                </select>--}}
{{--                                <label for="gorod" class="absolute text-sm text-gray-500 cursor-text peer-focus:text-main-black duration-300 transform -translate-y-4 scale-75 top-6 z-10 origin-0 left-4 peer-placeholder-shown:scale-100 peer-placeholder-shown:translate-y-0 peer-focus:scale-75 peer-focus:-translate-y-4">{{ __('admin.users.edit.city') }}</label>--}}
{{--                            </div>--}}
{{--                            @error('gorod')--}}
{{--                            <p id="gorod_help" class="mt-2 pl-2 text-xs text-red-600">{{ $message }}</p>--}}
{{--                            @enderror--}}
{{--                        </div>--}}
                            <div wire:key="{{$oblast}}.{{$raion}}">
                                <div class="z-100 relative" wire:ignore >
                                    <select wire:model="gorod" aria-describedby="gorod_help" x-ref="gorod" x-data x-init="let gorod = new Choices($refs.gorod,{classNames: {containerInner: 'block rounded-lg px-4 pb-3 pt-7 w-full text-base text-main-black bg-gray-bg border border-soft-gray appearance-none focus:outline-none focus:ring-0 focus:border-main-black peer',}});"
                                            class="block rounded-lg px-4 pb-3 pt-7 w-full text-base text-main-black bg-gray-bg border border-soft-gray appearance-none focus:outline-none focus:ring-0 focus:border-main-black peer">
                                        <option value="">{{ __('admin.users.edit.choosecity') }}</option>
                                        @foreach(\App\Models\Locations::where('type', \App\Models\Locations::LOCATION_AUL)->where('parent',$raion)->get() ?? []  as $item)
                                            <option value="{{ $item->id }}">{{ $item->ru_title }}</option>
                                        @endforeach
                                    </select>
                                    <label for="gorod" class="absolute text-sm text-gray-500 cursor-text peer-focus:text-main-black duration-300 transform -translate-y-4 scale-75 top-6 z-10 origin-0 left-4 peer-placeholder-shown:scale-100 peer-placeholder-shown:translate-y-0 peer-focus:scale-75 peer-focus:-translate-y-4">
                                        {{ __('admin.users.edit.city') }}</label>
                                </div>
                                @error('gorod')
                                <p id="gorod_help" class="mt-2 pl-2 text-xs text-red-600">{{ $message }}</p>
                                @enderror
                            </div>

                        <div wire:key="{{$oblast}}.{{$raion}}.{{$gorod}}">
                            <div class="z-100 relative" wire:ignore >
                                <select wire:model="user.institute" aria-describedby="institute_help" x-ref="institute" x-data x-init="let institute = new Choices($refs.institute,{classNames: {containerInner: 'block rounded-lg px-4 pb-3 pt-7 w-full text-base text-main-black bg-gray-bg border border-soft-gray appearance-none focus:outline-none focus:ring-0 focus:border-main-black peer',}});"
                                        class="block rounded-lg px-4 pb-3 pt-7 w-full text-base text-main-black bg-gray-bg border border-soft-gray appearance-none focus:outline-none focus:ring-0 focus:border-main-black peer">
                                    <option value="" disabled>{{ __('admin.users.edit.choosejob') }}</option>
                                    @foreach(\App\Models\Institutes::where('oblast', $oblast)->where('rayon',$raion)->get() ?? []  as $item)
                                        <option value="{{ $item->id }}">{{ $item->title }}</option>
                                    @endforeach
                                </select>
                                <label for="institute" class="absolute text-sm text-gray-500 cursor-text peer-focus:text-main-black duration-300 transform -translate-y-4 scale-75 top-6 z-10 origin-0 left-4 peer-placeholder-shown:scale-100 peer-placeholder-shown:translate-y-0 peer-focus:scale-75 peer-focus:-translate-y-4">{{ __('admin.users.edit.job') }}</label>
                            </div>
                            @error('user.institute')
                            <p id="institute_help" class="mt-2 pl-2 text-xs text-red-600">{{ $message }}</p>
                            @enderror
                        </div>
                    @endif
                @endif
                <div>
                    <div class="relative">
                        <select id="jobname" aria-describedby="jobname_help" class="block rounded-lg px-4 pb-3 pt-7 w-full text-base text-main-black bg-gray-bg border border-soft-gray appearance-none focus:outline-none focus:ring-0 focus:border-main-black peer"
                               wire:model="user.jobname" placeholder=" "
                        >
                            <option value="">{{ __('admin.users.edit.chooseposition') }}</option>
                            @foreach(\App\Models\JobNames::all() ?? []  as $item)
                                <option value="{{ $item->id }}">{{ $item->ru_title }}</option>
                            @endforeach
                        </select>
                        <label for="jobname" class="absolute text-sm text-gray-500 cursor-text peer-focus:text-main-black duration-300 transform -translate-y-4 scale-75 top-6 z-10 origin-0 left-4 peer-placeholder-shown:scale-100 peer-placeholder-shown:translate-y-0 peer-focus:scale-75 peer-focus:-translate-y-4">{{ __('admin.users.edit.position') }}</label>
                    </div>
                    @error('user.jobname')
                    <p id="jobname_help" class="mt-2 pl-2 text-xs text-red-600">{{ $message }}</p>
                    @enderror
                </div>
                <div>
                    <div class="relative">
                        <select id="academic_category" aria-describedby="academic_category_help" class="block rounded-lg px-4 pb-3 pt-7 w-full text-base text-main-black bg-gray-bg border border-soft-gray appearance-none focus:outline-none focus:ring-0 focus:border-main-black peer"
                               wire:model="user.academic_category" placeholder=" "
                        >
                            <option value="">{{ __('admin.users.edit.choosecategory') }}</option>
                            @foreach(\App\Models\AcademicCategories::all() ?? []  as $item)
                                <option value="{{ $item->id }}">{{ $item->ru_title }}</option>
                            @endforeach
                        </select>
                        <label for="academic_category" class="absolute text-sm text-gray-500 cursor-text peer-focus:text-main-black duration-300 transform -translate-y-4 scale-75 top-6 z-10 origin-0 left-4 peer-placeholder-shown:scale-100 peer-placeholder-shown:translate-y-0 peer-focus:scale-75 peer-focus:-translate-y-4">{{ __('admin.users.edit.category') }}</label>
                    </div>
                    @error('user.academic_category')
                    <p id="academic_category_help" class="mt-2 pl-2 text-xs text-red-600">{{ $message }}</p>
                    @enderror
                </div>
                <div>
                    <div class="relative">
                        <select id="education" aria-describedby="education_help" class="block rounded-lg px-4 pb-3 pt-7 w-full text-base text-main-black bg-gray-bg border border-soft-gray appearance-none focus:outline-none focus:ring-0 focus:border-main-black peer"
                               wire:model="user.education" placeholder=" "
                        >
                            <option value="">{{ __('admin.users.edit.chooselevel') }}</option>
                            @foreach(\App\Models\Educations::all() ?? []  as $item)
                                <option value="{{ $item->id }}">{{ $item->ru_title }}</option>
                            @endforeach
                        </select>
                        <label for="education" class="absolute text-sm text-gray-500 cursor-text peer-focus:text-main-black duration-300 transform -translate-y-4 scale-75 top-6 z-10 origin-0 left-4 peer-placeholder-shown:scale-100 peer-placeholder-shown:translate-y-0 peer-focus:scale-75 peer-focus:-translate-y-4">{{ __('admin.users.edit.level') }}</label>
                    </div>
                    @error('user.education')
                    <p id="education_help" class="mt-2 pl-2 text-xs text-red-600">{{ $message }}</p>
                    @enderror
                </div>
                <div>
                    <div class="relative">
                        <select id="academic_degree" aria-describedby="academic_degree_help" class="block rounded-lg px-4 pb-3 pt-7 w-full text-base text-main-black bg-gray-bg border border-soft-gray appearance-none focus:outline-none focus:ring-0 focus:border-main-black peer"
                               wire:model="user.academic_degree" placeholder=" "
                        >
                            <option value="">{{ __('admin.users.edit.chooseacademic') }}</option>
                            @foreach(\App\Models\AcademicDegrees::all() ?? []  as $item)
                                <option value="{{ $item->id }}">{{ $item->ru_title }}</option>
                            @endforeach
                        </select>
                        <label for="academic_degree" class="absolute text-sm text-gray-500 cursor-text peer-focus:text-main-black duration-300 transform -translate-y-4 scale-75 top-6 z-10 origin-0 left-4 peer-placeholder-shown:scale-100 peer-placeholder-shown:translate-y-0 peer-focus:scale-75 peer-focus:-translate-y-4">{{ __('admin.users.edit.academic') }}</label>
                    </div>
                    @error('user.academic_degree')
                    <p id="academic_degree_help" class="mt-2 pl-2 text-xs text-red-600">{{ $message }}</p>
                    @enderror
                </div>
                <div>
                    <div class="relative">
                        <select id="teach_subject_id" aria-describedby="teach_subject_id_help" class="block rounded-lg px-4 pb-3 pt-7 w-full text-base text-main-black bg-gray-bg border border-soft-gray appearance-none focus:outline-none focus:ring-0 focus:border-main-black peer"
                               wire:model="user.teach_subject_id" placeholder=" "
                        >
                            <option value="">{{ __('admin.users.edit.lesson.placeholder') }}</option>
                            @foreach(\App\Models\TeachSubjects::all() ?? []  as $item)
                                <option value="{{ $item->id }}">{{ $item->ru_title }}</option>
                            @endforeach
                        </select>
                        <label for="teach_subject_id" class="absolute text-sm text-gray-500 cursor-text peer-focus:text-main-black duration-300 transform -translate-y-4 scale-75 top-6 z-10 origin-0 left-4 peer-placeholder-shown:scale-100 peer-placeholder-shown:translate-y-0 peer-focus:scale-75 peer-focus:-translate-y-4">{{ __('admin.users.edit.lesson') }}</label>
                    </div>
                    @error('user.teach_subject_id')
                    <p id="teach_subject_id_help" class="mt-2 pl-2 text-xs text-red-600">{{ $message }}</p>
                    @enderror
                </div>
                <div>
                    <div class="relative">
                    <input type="text" id="stazh" aria-describedby="stazh_help" class="block rounded-lg px-4 pb-3 pt-7 w-full text-base text-main-black bg-gray-bg border border-soft-gray appearance-none focus:outline-none focus:ring-0 focus:border-main-black peer"
                               wire:model="user.stazh" placeholder=" "
                        />
                        <label for="stazh" class="absolute text-sm text-gray-500 cursor-text peer-focus:text-main-black duration-300 transform -translate-y-4 scale-75 top-6 z-10 origin-0 left-4 peer-placeholder-shown:scale-100 peer-placeholder-shown:translate-y-0 peer-focus:scale-75 peer-focus:-translate-y-4">{{ __('admin.users.edit.staj') }}</label>
                    </div>
                    @error('user.stazh')
                    <p id="stazh_help" class="mt-2 pl-2 text-xs text-red-600">{{ $message }}</p>
                    @enderror
                </div>
            </div>
            @can(\App\Models\Permission::PERMISSION_TRAINER)
                <div class="w-full mb-4" >
                    <label for="trainer_awards" class="w-full mb-4">{{ __('Награды') }}</label>
                    <x-inputs.trix id="trainer_awards" :value="$trainer_awards" />
                </div>
                <div class="w-full mb-4" >
                    <label for="trainer_theses" class="w-full mb-4">{{ __('Опубликованные статьи') }}</label>
                    <x-inputs.trix id="trainer_theses" :value="$trainer_theses" />
                </div>
                <div class="w-full mb-4" >
                    <label for="trainer_certificates" class="w-full mb-4">{{ __('Пройденные курсы') }}</label>
                    <x-inputs.trix id="trainer_certificates" :value="$trainer_certificates" />
                </div>
            @endcan
        </div>
        <div class="w-full flex h-px bg-soft-gray mb-8"></div>
        <div class="flex w-full justify-between items-center lg:justify-start space-x-4 mb-8">
            <button wire:click="save"
                    class="w-full lg:w-min py-4 px-10 lg:px-12 border border-primary-button bg-primary-button rounded-lg text-white text-base hover:bg-logo-blue hover:border-logo-blue text-center">
                {{ __('admin.buttons.save') }}
            </button>

            <a href="{{ route('user.profile') }}"
               class="w-full lg:w-min py-4 px-10 lg:px-12 border border-primary-button bg-white rounded-lg text-main-black text-base hover:bg-primary-button hover:border-primary-button hover:text-white text-center">{{ __('admin.buttons.cancel') }}</a>
        </div>
    </div>

    <div class="py-6 px-4 sm:px-6 lg:px-8 h-full">
        <div class="flex flex-col items-start md:items-center w-full md:w-3/4 mx-auto space-y-4">

{{--            @if($user->teach_subject_id == null)--}}
{{--                <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-center">--}}
{{--                    <div class="w-full md:w-1/4">--}}
{{--                        <label for="subject_teach"--}}
{{--                               class="w-full text-left md:text-right block text-sm">{{ __('admin.users.edit.lesson') }}</label>--}}
{{--                    </div>--}}
{{--                    <div class="w-full md:w-3/4">--}}
{{--                        <input wire:model="user.subject_teach" type="text" id="subject_teach"--}}
{{--                               placeholder="{{ __('admin.users.edit.lesson.placeholder') }}"--}}
{{--                               class="w-full md:w-3/4 px-3 py-1 border rounded focus:border-blue-500 hover:border-blue-500 text-sm outline-none focus:input-shadow"/>--}}
{{--                        @error('user.subject_teach')--}}
{{--                        <p class="text-xs text-red-600">{{ $message }}</p>--}}
{{--                        @enderror--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            @endif--}}


{{--            <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-center">--}}
{{--                <div class="w-full md:w-1/4">--}}
{{--                    <label for="iban"--}}
{{--                           class="w-full text-left md:text-right block text-sm">{{ __('admin.users.edit.iban') }}</label>--}}
{{--                </div>--}}
{{--                <div class="w-full md:w-3/4">--}}
{{--                    <input wire:model="user.iban" type="text" id="iban"--}}
{{--                           placeholder="{{ __('admin.users.edit.iban.placeholder') }}"--}}
{{--                           class="w-full md:w-3/4 px-3 py-1 border rounded focus:border-blue-500 hover:border-blue-500 text-sm outline-none focus:input-shadow"/>--}}
{{--                    @error('user.iban')--}}
{{--                    <p class="text-xs text-red-600">{{ $message }}</p>--}}
{{--                    @enderror--}}
{{--                </div>--}}
{{--            </div>--}}

{{--            <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-center">--}}
{{--                <div class="w-full md:w-1/4">--}}
{{--                    <label for="bank_name"--}}
{{--                           class="w-full text-left md:text-right block text-sm">{{ __('admin.users.edit.bank') }}</label>--}}
{{--                </div>--}}
{{--                <div class="w-full md:w-3/4">--}}
{{--                    <input wire:model="user.bank_name" type="text" id="bank_name"--}}
{{--                           placeholder="{{ __('admin.users.edit.bank.placeholder') }}"--}}
{{--                           class="w-full md:w-3/4 px-3 py-1 border rounded focus:border-blue-500 hover:border-blue-500 text-sm outline-none focus:input-shadow"/>--}}
{{--                    @error('user.bank_name')--}}
{{--                    <p class="text-xs text-red-600">{{ $message }}</p>--}}
{{--                    @enderror--}}
{{--                </div>--}}
{{--            </div>--}}


        </div>
    </div>

</div>
@push('styles')
    <link rel="stylesheet" href="https://unpkg.com/trix@2.0.0-alpha.1/dist/trix.css"/>
    <script src="https://unpkg.com/trix@2.0.0-alpha.1/dist/trix.umd.js"></script>

{{--    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/flatpickr/dist/flatpickr.min.css">--}}
{{--    <script src="https://cdn.jsdelivr.net/npm/flatpickr"></script>--}}
{{--    <script src="https://cdn.jsdelivr.net/npm/flatpickr/dist/l10n/ru.js"></script>--}}
    <link href="https://unpkg.com/slim-select@1.27.0/dist/slimselect.min.css" rel="stylesheet"/>
    <link rel="stylesheet"  href="https://cdn.jsdelivr.net/npm/choices.js/public/assets/styles/choices.min.css"/>
    <style>
        html {
            scroll-behavior: smooth;
        }

    </style>
@endpush

@push('scripts')
    <script src="https://cdn.jsdelivr.net/npm/choices.js/public/assets/scripts/choices.min.js"></script>
    <script src="https://unpkg.com/slim-select@1.27.0/dist/slimselect.min.js"></script>
@endpush

@push('phone-mask-scripts')
    <!-- <script src="https://unpkg.com/imask"></script> -->
<script src="https://unpkg.com/imask"></script>
<script src="/js/imask.min.js"></script>
    <script>
        var element = document.getElementById('phone');
        var maskOptions = {
            mask: '+7(700)000-00-00',
            lazy: false
        }
        var mask = new IMask(element, maskOptions);
</script>
@endpush
