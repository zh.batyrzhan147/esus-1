<div>
    <x-slot name="header"></x-slot>
    <div class="bg-white mx-auto py-6 px-4 sm:px-6 lg:px-8 space-y-4">
        <div class="flex w-full justify-between items-center">
            <h2 class="font-semibold text-xl text-gray-800 leading-tight">
                {{ __('admin.users.list.title') }}
            </h2>

            <div class="flex justify-end items-center">
                @include('admin.common.secondmenu', [
                    'popup'=>true,
                ])
                <a href="{{ route('admin.users.create') }}" class="text-sm flex justify-center items-center space-x-1.5 px-4 py-1 border-0 bg-blue-500 text-white rounded hover:bg-blue-600">
                    <svg width="14" height="14" viewBox="0 0 14 14" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M7.71838 1.62408H6.78088C6.69755 1.62408 6.65588 1.66575 6.65588 1.74908V6.65533H2C1.91667 6.65533 1.875 6.697 1.875 6.78033V7.71783C1.875 7.80117 1.91667 7.84283 2 7.84283H6.65588V12.7491C6.65588 12.8324 6.69755 12.8741 6.78088 12.8741H7.71838C7.80172 12.8741 7.84338 12.8324 7.84338 12.7491V7.84283H12.5C12.5833 7.84283 12.625 7.80117 12.625 7.71783V6.78033C12.625 6.697 12.5833 6.65533 12.5 6.65533H7.84338V1.74908C7.84338 1.66575 7.80172 1.62408 7.71838 1.62408Z" fill="white"/>
                    </svg>
                    <span>{{ __('admin.buttons.create') }}</span>
                </a>
            </div>
        </div>
        <div class="w-full flex justify-between items-end">
            <div class="flex flex-col w-full">
                <label class="flex flex-col text-left" for="iinfio">
                    <span class="text-gray-700 mb-2">{{ __('admin.users.list.filter.iin') }}</span>
                </label>
                <input type="text" wire:keydown.enter="applyFilter" wire:model.defer="search" id="iinfio"
                       class="w-3/4 px-3 py-1 border rounded focus:border-blue-500 hover:border-blue-500 text-sm outline-none focus:input-shadow"
                       placeholder="{{ __('admin.users.list.filter.iin.placeholder') }}"
                />
            </div>
            <button wire:click="applyFilter"
                    class="text-sm flex justify-center items-center space-x-1.5 px-4 py-1.5 border-0 bg-blue-500 text-white whitespace-nowrap rounded hover:bg-blue-600">
                <span>{{ __('admin.buttons.accept.filters') }}</span>
            </button>
        </div>
        <div class="flex flex-col">
            <div class="-my-2 overflow-x-auto sm:-mx-6 lg:-mx-8">
                <div class="py-2 align-middle inline-block min-w-full sm:px-6 lg:px-8">
                    <div class="shadow overflow-hidden border-b border-gray-200 sm:rounded">
                        <table class="min-w-full divide-y divide-gray-200 w-full">
                            <thead>
                            <tr>
                                <th scope="col"
                                    class="px-2 py-3 bg-gray-50 text-left text-sm font-medium text-gray-800">
                                    <div class="pl-4">{{ __('admin.table.head.iin') }}</div>
                                </th>

                                <th scope="col"
                                    class="px-2 py-3 bg-gray-50 text-left text-sm font-medium text-gray-800">
                                    <div class="pl-4">{{ __('admin.table.head.fio') }}</div>
                                </th>

                                <th scope="col"
                                    class="px-2 py-3 bg-gray-50 text-left text-sm font-medium text-gray-800">
                                    <div class="pl-4">{{ __('admin.table.head.roles') }}</div>
                                </th>

                                <th scope="col" width="200"
                                    class="px-2 py-3 bg-gray-50 text-left text-sm font-medium text-gray-800">
                                    <div class="pl-4 border-l border-gray-200">{{ __('admin.table.head.action') }}</div>
                                </th>
                            </tr>
                            </thead>
                            <tbody class="bg-white divide-y divide-gray-200">
                            @foreach ($users as $user)
                                <tr>
                                    <td class="px-6 py-1.5 whitespace-nowrap text-sm text-gray-900">
                                        {{ $user->iin }}
                                    </td>

                                    <td class="px-6 py-1.5 whitespace-nowrap text-sm text-gray-900">
                                        <a href="/admin/authin/{{ $user->id }}" class="text-blue-500 hover:text-blue-500 m-0 font-small">Авторизоватся</a> {{ $user->fio }}
                                    </td>
                                    <td class="px-6 py-1.5 text-sm text-gray-900">
                                    @foreach($user->roles as $role)
                                        <span wire:click="$emit('dettachrole','{{ $role->id }}','{{ $user->id }}','{{ $role->title }}','{{ $user->fio }}')" class="{{ $role->badgesColors()[$role->id] }} cursor-pointer	">{{ $role->title }}</span>
                                    @endforeach
                                    </td>

                                    <td class="px-6 py-1.5 whitespace-nowrap text-sm font-medium space-x-2 text-left">
                                        <button id="dropdownDefault{{$user->id}}" data-dropdown-toggle="dropdown{{$user->id}}"
                                                class="text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm px-4 py-2.5 text-center inline-flex items-center dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800"
                                                type="button">Добавить роль
                                            <svg class="w-4 h-4 ml-2" fill="none" stroke="currentColor" viewBox="0 0 24 24"
                                                 xmlns="http://www.w3.org/2000/svg">
                                                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M19 9l-7 7-7-7"></path>
                                            </svg>
                                        </button>
                                        <!-- Dropdown menu -->
                                        <div id="dropdown{{$user->id}}" class="z-10 hidden bg-white divide-y divide-gray-100 rounded shadow w-44 dark:bg-gray-700">
                                            <ul class="py-1 text-sm text-gray-700 dark:text-gray-200" aria-labelledby="dropdownDefault">
                                                @foreach($roles as $item)
                                                    <li>
                                                        <button wire:click="$emit('attachrole','{{ $item->id }}','{{ $user->id }}','{{ $item->title }}','{{ $user->fio }}')" class="block px-4 py-2 hover:bg-gray-100 dark:hover:bg-gray-600 dark:hover:text-white">
                                                            {{ $item->title }}</button>
                                                    </li>
                                                @endforeach
                                            </ul>
                                        </div>

                                        <a href="{{ route('admin.users.edit',['iin' => $user->iin]) }}"
                                           class="text-blue-500 hover:text-blue-900 m-0 font-normal">{{ __('admin.links.edit') }}</a>
                                        <button wire:click="$emit('triggerDelete',{{ $user->id }},'{{$user->iin}}')"
                                                class="text-red-500 hover:text-red-900 bg-none border-0 m-0 cursor-pointer bg-transparent font-normal ">{{ __('admin.links.delete') }}</button>
                                    </td>

                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="mt-4" wire:ignore>
                        {{ $users->appends(request()->query())->links('admin.common.pagination') }}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@push('scripts')
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>

    <script type="text/javascript">
        document.addEventListener('DOMContentLoaded', function () {
        @this.on('triggerSuccessCreate', () => {
            Swal.fire({
                title: '{{ __("admin.swal.grants.title.success2") }}',
                icon: 'success',
                width: 600,
                position: 'top-end',
                showConfirmButton: false,
                timer: 1000
            });
        });

        @this.on('attachrole', (roleID,userID,roleTitle,userFIO) => {
            Swal.fire({
                title: 'Добавить роль ' + roleTitle + '?',
                text: 'Добавить пользователю ' + userFIO + ' роль?',
                icon: "warning",
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#aaa',
                cancelButtonText: '{{ __("admin.swal.temp.buttons.cancel") }}',
                confirmButtonText: '{{ __("admin.swal.temp.buttons.approve") }}'
            }).then((result) => {
                //if user clicks on delete
                if (result.value) {
                    // calling destroy method to delete
                @this.call('attachRoleUser',roleID,userID);
                    // success response
                    Swal.fire({title: '{{ __("admin.swal.temp.deleted.title") }}', icon: 'success',
                        width: 600,
                        position: 'top-end',
                        showConfirmButton: false,
                        timer: 1000});
                } else {
                    Swal.fire({
                        title: '{{ __("admin.swal.temp.discarded.title") }}',
                        icon: 'success',
                        width: 600,
                        position: 'top-end',
                        showConfirmButton: false,
                        timer: 1000
                    });
                }
            });
        });

        @this.on('triggerDelete', (itemID,Iin) => {

            Swal.fire({
                title: 'Вы уверены?',
                text: 'Вы удаляете пользователя с ИИН ' + Iin,
                icon: "warning",
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#aaa',
                cancelButtonText: '{{ __("admin.swal.temp.buttons.cancel") }}',
                confirmButtonText: '{{ __("admin.swal.temp.buttons.approve") }}'
            }).then((result) => {
                //if user clicks on delete
                if (result.value) {
                    // calling destroy method to delete
                @this.call('destroy',itemID);
                    // success response
                    Swal.fire({title: '{{ __("admin.swal.temp.deleted.title") }}', icon: 'success',
                        width: 600,
                        position: 'top-end',
                        showConfirmButton: false,
                        timer: 1000});
                } else {
                    Swal.fire({
                        title: '{{ __("admin.swal.temp.discarded.title") }}',
                        icon: 'success',
                        width: 600,
                        position: 'top-end',
                        showConfirmButton: false,
                        timer: 1000
                    });
                }
            });
        });

        @this.on('dettachrole', (roleID,userID,roleTitle,userFIO) => {
            Swal.fire({
                title: 'Удалить роль ' + roleTitle + '?',
                text: 'Удалить пользователю ' + userFIO + ' роль?',
                icon: "warning",
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#aaa',
                cancelButtonText: '{{ __("admin.swal.temp.buttons.cancel") }}',
                confirmButtonText: '{{ __("admin.swal.temp.buttons.approve") }}'
            }).then((result) => {
                //if user clicks on delete
                if (result.value) {
                    // calling destroy method to delete
                @this.call('dettachRoleUser',roleID,userID);
                    // success response
                    Swal.fire({title: '{{ __("admin.swal.temp.deleted.title") }}', icon: 'success',
                        width: 600,
                        position: 'top-end',
                        showConfirmButton: false,
                        timer: 1000});
                } else {
                    Swal.fire({
                        title: '{{ __("admin.swal.temp.discarded.title") }}',
                        icon: 'success',
                        width: 600,
                        position: 'top-end',
                        showConfirmButton: false,
                        timer: 1000
                    });
                }
            });
        });
        })
    </script>
@endpush
