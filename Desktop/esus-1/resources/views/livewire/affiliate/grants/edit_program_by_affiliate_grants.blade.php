<div>
    <x-modal>
        <x-slot name="header">
            <div class="w-full pb-4">
                <span>{!! __('admin.grants.edit.modal.affiliate.title', ['affiliate' => $editableGrant->coursesItem->ru_title]) !!}</span>
            </div>
        </x-slot>
        <div class="flex flex-col items-start md:items-center w-full md:w-3/4 mx-auto space-y-4">
            <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-center">
                <div class="w-full md:w-1/4">
                    <label for="count_all"
                           class="w-full text-left md:text-right block text-sm">{{ __('admin.grants.edit.modal.course.count') }}</label>
                </div>
                <div class="w-full md:w-3/4" x-data x-init="$refs.editableGrantCountAll.focus()">
                    <span>{{ $editableGrant->count_all }}</span>
                </div>
            </div>
            @foreach(\App\Models\Languages::all() as $lang)
                <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-center">
                    <div class="w-full md:w-1/4">
                        <label for="count['{{ $lang->slug }}']"
                               class="w-full text-left md:text-right block text-sm">{{ __('admin.grants.edit.modal.course.count.lang', ['lang' => $lang->title]) }}</label>
                    </div>
                    <div class="w-full md:w-3/4">
                        <input wire:model.defer="count.{{ $lang->slug }}" type="text" id="count['{{ $lang->slug }}']"
                               x-ref="editableGrantCountAll"
                               placeholder="{{ __('admin.grants.edit.modal.course.count.lang.placeholder', ['lang' => $lang->title]) }}"
                               class="w-full md:w-3/4 px-3 py-1 border rounded focus:border-blue-500 hover:border-blue-500 text-sm outline-none focus:input-shadow"/>
                    </div>
                </div>
            @endforeach
            <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-center">
                <div class="w-full md:w-1/4">
                </div>
                <div class="w-full md:w-3/4 space-x-2 flex">
                    <button type="submit" wire:click="updateGrant"
                            class="mb-2 text-sm flex justify-center items-center px-4 py-1 border-0 bg-blue-500 text-white rounded hover:bg-blue-600">
                        {{ __('admin.buttons.save') }}
                    </button>
                    <button wire:click="$set('showModalEditgrants',false)"
                            class="py-1 px-4 border inline-block m-0 mb-2 text-sm rounded hover:border-black">{{ __('admin.buttons.cancel') }}
                    </button>
                </div>
            </div>
        </div>

    </x-modal>
</div>
