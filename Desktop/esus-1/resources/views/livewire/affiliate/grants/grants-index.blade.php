<div x-data="{ open: false }">
    <x-slot name="header"></x-slot>

    <div class="bg-white mx-auto py-6 px-4 sm:px-6 lg:px-8">
        <div class="flex justify-between items-center">
            <h2 class="font-semibold text-xl text-gray-800 leading-tight">
                {{ __('admin.grants.list.title') }}
            </h2>
            @include('admin.common.secondmenu', [
                'tabname'=>'courses',
                'popup'=>true,
            ])
        </div>
    </div>

    <div class="flex flex-col mx-auto py-2 px-4 sm:px-6 lg:px-8">
        <div class="w-full flex justify-between items-end space-x-4">
            <div class="flex space-x-4">
                <div class="flex justify-between items-center w-96">
                    <label class="flex flex-col text-left">
                        <span class="text-gray-700 mb-2">{{ __('admin.grants.list.filter.chooseyear') }}</span>
                        <select wire:model.defer="year"
                                class="w-48 px-3 py-1 border rounded focus:border-blue-500 hover:border-blue-500 text-sm outline-none focus:input-shadow">
                            @foreach(\App\Models\Years::orderBy('year','ASC')->get() as $yearItem)
                                <option value="{{ $yearItem->year }}">{{ $yearItem->year }}</option>
                            @endforeach
                        </select>
                    </label>
                </div>
            </div>
            <div class="flex justify-end space-x-4 h-7">
{{--                <button class="text-sm flex justify-center items-center space-x-1.5 px-4 py-1 border-0 bg-red-500 text-white rounded hover:bg-red-600">--}}
{{--                    <span>{{ __('admin.buttons.audit') }}</span>--}}
{{--                </button>--}}
                <button wire:click="applyFilter"
                        class="text-sm flex justify-center items-center space-x-1.5 px-4 py-1 border-0 bg-blue-500 text-white rounded hover:bg-blue-600">
                    <span>{{ __('admin.buttons.accept.filters') }}</span>
                </button>
            </div>
        </div>
    </div>

    <div class="py-6 px-4 sm:px-6 lg:px-8">
        <div class="flex flex-col">
            <div class="-my-2 overflow-x-auto sm:-mx-6 lg:-mx-8">
                <div class="py-2 align-middle inline-block min-w-full sm:px-6 lg:px-8">
                    <div class="shadow overflow-hidden border-b border-gray-200 sm:rounded">
                        <table class="min-w-full divide-y divide-gray-200 w-full">
                            <thead>
                            <tr>
                                <th scope="col" class="px-2 py-3 bg-gray-50 text-left text-sm font-medium text-gray-800">
                                    <div class="pl-4">{{ __('admin.table.head.title.program') }}</div>
                                </th>
                                <th scope="col" class="px-2 py-3 bg-gray-50 text-left text-sm font-medium text-gray-800">
                                    <div class="pl-4 border-l border-gray-200 whitespace-nowrap text-right">{{ __('admin.table.head.count.listener') }}</div>
                                </th>
                                @foreach(\App\Models\Languages::all() as $lang)
                                    <th scope="col"
                                        class="px-2 py-3 bg-gray-50 text-left text-sm font-medium text-gray-800">
                                        <div class="pl-4 border-l border-gray-200 text-right">{{ $lang->title }}</div>
                                    </th>
                                @endforeach
                                <th scope="col" width="200" class="px-2 py-3 bg-gray-50 text-left text-sm font-medium text-gray-800">
                                    <div class="pl-4 border-l border-gray-200 text-right">{{ __('admin.table.head.action') }}</div>
                                </th>
                            </tr>
                            </thead>
                            <tbody class="bg-white divide-y divide-gray-200">
                            @foreach ($grants as $grant)
                                <tr>
                                    <td class="px-6 py-1.5 text-sm text-gray-900">
                                        {{ $grant->coursesItem->id }}) {!! $grant->coursesItem->ru_title !!}
                                    </td>

                                    <td class="px-6 py-1.5 whitespace-nowrap text-sm text-gray-900 text-right">
                                        {{ $grant->count_all }}
                                    </td>

                                    @foreach($languages as $lang)
                                        <td class="px-6 py-1.5 whitespace-nowrap text-sm text-gray-900 text-right">
                                            {{ $grant->countsArray[$lang->slug] ?? 0 }}
                                        </td>
                                    @endforeach

                                    <td class="px-6 py-1.5 whitespace-nowrap text-sm font-medium space-x-2 text-right">
                                        <button wire:click="edit({{ $grant->id }})"
                                                class="text-blue-500 hover:text-blue-900 m-0 font-normal">{{ __('admin.links.edit2') }}
                                        </button>
{{--                                        <a href="{{ route('admin.grants.edit', $grant->id) }}" class="text-blue-500 hover:text-blue-900 m-0 font-normal">{{ __('admin.links.edit') }}</a>--}}
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

        </div>
    </div>

    @if($showModalEditgrants)
        @include('livewire.affiliate.grants.edit_program_by_affiliate_grants')
    @endif

</div>

@push('scripts')
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>

    <script type="text/javascript">
        document.addEventListener('DOMContentLoaded', function () {
        @this.on('saved', () => {
            Swal.fire({
                title: '{{ __("admin.swal.grants.title.success") }}',
                icon: 'success',
                position: 'top-end',
                width: 600,
                showConfirmButton: false,
                timer: 1000
            });
        });
        })
    </script>

@endpush
