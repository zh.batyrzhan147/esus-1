<div xmlns:wire="http://www.w3.org/1999/xhtml">
    <x-slot name="header">
        <div class="bg-white mx-auto py-6 px-4 sm:px-6 lg:px-8">
            <h2 class="font-semibold text-xl text-gray-800 leading-tight">
                {{ __('admin.users.edit.title') }}
            </h2>
        </div>
    </x-slot>
    @include('admin.common.secondmenu', [
        'tabname'=>'groups',
        'popup'=>true,
    ])

    <div class="py-6 px-4 sm:px-6 lg:px-8 h-full">
        <div class="flex flex-col items-start md:items-center w-full md:w-3/4 mx-auto space-y-4">
            <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-center">
                <div class="w-full md:w-1/4">
                    <label for="iin" class="w-full text-left md:text-right block text-sm">{{ __('admin.users.edit.iin') }}</label>
                </div>
                <div class="w-full md:w-3/4">
                    <input wire:model="user.iin" type="text" id="iin" placeholder="{{ __('admin.users.edit.iin.placeholder') }}"
                           class="w-full md:w-3/4 px-3 py-1 border rounded focus:border-blue-500 hover:border-blue-500 text-sm outline-none focus:input-shadow"/>
                    @error('user.iin')
                    <p class="text-xs text-red-600">{{ $message }}</p>
                    @enderror
                </div>
            </div>

            <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-center">
                <div class="w-full md:w-1/4">
                    <label for="last_name" class="w-full text-left md:text-right block text-sm">{{ __('admin.users.edit.last_name') }}</label>
                </div>
                <div class="w-full md:w-3/4">
                    <input wire:model="user.last_name" type="text" id="last_name" placeholder="{{ __('admin.users.edit.last_name.placeholder') }}"
                           class="w-full md:w-3/4 px-3 py-1 border rounded focus:border-blue-500 hover:border-blue-500 text-sm outline-none focus:input-shadow"/>
                    @error('user.last_name')
                    <p class="text-xs text-red-600">{{ $message }}</p>
                    @enderror
                </div>
            </div>

            <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-center">
                <div class="w-full md:w-1/4">
                    <label for="last_name_lat" class="w-full text-left md:text-right block text-sm">{{ __('admin.users.edit.last_name.lat') }}</label>
                </div>
                <div class="w-full md:w-3/4">
                    <input wire:model="user.last_name_lat" type="text" id="last_name_lat" placeholder="{{ __('admin.users.edit.last_name.lat.placeholder') }}"
                           class="w-full md:w-3/4 px-3 py-1 border rounded focus:border-blue-500 hover:border-blue-500 text-sm outline-none focus:input-shadow"/>
                    @error('user.last_name_lat')
                    <p class="text-xs text-red-600">{{ $message }}</p>
                    @enderror
                </div>
            </div>

            <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-center">
                <div class="w-full md:w-1/4">
                    <label for="name" class="w-full text-left md:text-right block text-sm">{{ __('admin.users.edit.name') }}</label>
                </div>
                <div class="w-full md:w-3/4">
                    <input wire:model="user.name" type="text" id="name" placeholder="{{ __('admin.users.edit.name.placeholder') }}"
                           class="w-full md:w-3/4 px-3 py-1 border rounded focus:border-blue-500 hover:border-blue-500 text-sm outline-none focus:input-shadow"/>
                    @error('user.name')
                    <p class="text-xs text-red-600">{{ $message }}</p>
                    @enderror
                </div>
            </div>

            <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-center">
                <div class="w-full md:w-1/4">
                    <label for="name_lat" class="w-full text-left md:text-right block text-sm">{{ __('admin.users.edit.name.lat') }}</label>
                </div>
                <div class="w-full md:w-3/4">
                    <input wire:model="user.name_lat" type="text" id="name_lat" placeholder="{{ __('admin.users.edit.name.lat.placeholder') }}"
                           class="w-full md:w-3/4 px-3 py-1 border rounded focus:border-blue-500 hover:border-blue-500 text-sm outline-none focus:input-shadow"/>
                    @error('user.name_lat')
                    <p class="text-xs text-red-600">{{ $message }}</p>
                    @enderror
                </div>
            </div>

            <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-center">
                <div class="w-full md:w-1/4">
                    <label for="patronymic" class="w-full text-left md:text-right block text-sm">{{ __('admin.users.edit.patronymic') }}</label>
                </div>
                <div class="w-full md:w-3/4">
                    <input wire:model="user.patronymic" type="text" id="patronymic" placeholder="{{ __('admin.users.edit.patronymic.placeholder') }}"
                           class="w-full md:w-3/4 px-3 py-1 border rounded focus:border-blue-500 hover:border-blue-500 text-sm outline-none focus:input-shadow"/>
                    @error('user.patronymic')
                    <p class="text-xs text-red-600">{{ $message }}</p>
                    @enderror
                </div>
            </div>

            <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-center">
                <div class="w-full md:w-1/4">
                    <label for="email" class="w-full text-left md:text-right block text-sm">{{ __('admin.users.edit.email') }}</label>
                </div>
                <div class="w-full md:w-3/4">
                    <input wire:model="user.email" type="email" id="email" placeholder="{{ __('admin.users.edit.email.placeholder') }}"
                           class="w-full md:w-3/4 px-3 py-1 border rounded focus:border-blue-500 hover:border-blue-500 text-sm outline-none focus:input-shadow"/>
                    @error('user.email')
                    <p class="text-xs text-red-600">{{ $message }}</p>
                    @enderror
                </div>
            </div>

            <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-center">
                <div class="w-full md:w-1/4">
                    <label for="phone" class="w-full text-left md:text-right block text-sm">{{ __('admin.users.edit.phone') }}</label>
                </div>
                <div class="w-full md:w-3/4">
                    <input wire:model="user.phone" type="text" id="phone" placeholder="{{ __('admin.users.edit.phone.placeholder') }}"
                           class="w-full md:w-3/4 px-3 py-1 border rounded focus:border-blue-500 hover:border-blue-500 text-sm outline-none focus:input-shadow"/>
                    @error('user.phone')
                    <p class="text-xs text-red-600">{{ $message }}</p>
                    @enderror
                </div>
            </div>

            <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-center">
                <div class="w-full md:w-1/4">
                    <label for="gender" class="w-full text-left md:text-right block text-sm">{{ __('admin.users.edit.sex') }}</label>
                </div>
                <div class="w-full md:w-3/4">
                    <select wire:model="user.gender"
                            class="w-full md:w-3/4 px-3 py-1 border rounded focus:border-blue-500 hover:border-blue-500 text-sm outline-none focus:input-shadow">
                        <option value="" >{{ __('admin.users.edit.choosesex') }}</option>
                        @foreach(\App\Models\Genders::all() ?? []  as $item)
                            <option value="{{ $item->id }}">{{ $item->title }}</option>
                        @endforeach
                    </select>
                    @error('user.gender')
                    <p class="text-xs text-red-600">{{ $message }}</p>
                    @enderror
                </div>
            </div>

            <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-center">
                <div class="w-full md:w-1/4">
                    <label for="nationality" class="w-full text-left md:text-right block text-sm">{{ __('admin.users.edit.nationality') }}</label>
                </div>
                <div class="w-full md:w-3/4">
                    <select wire:model="user.nationality"
                            class="w-full md:w-3/4 px-3 py-1 border rounded focus:border-blue-500 hover:border-blue-500 text-sm outline-none focus:input-shadow">
                        <option value="" >{{ __('admin.users.edit.choosenationality') }}</option>
                        @foreach(\App\Models\Nationalities::all() ?? []  as $item)
                            <option value="{{ $item->id }}">{{ $item->ru_title }}</option>
                        @endforeach
                    </select>
                    @error('user.nationality')
                    <p class="text-xs text-red-600">{{ $message }}</p>
                    @enderror
                </div>
            </div>

            <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-center">
                <div class="w-full md:w-1/4">
                    <label for="oblast" class="w-full text-left md:text-right block text-sm">{{ __('admin.users.edit.obl') }}</label>
                </div>
                <div class="w-full md:w-3/4">
                    <select wire:model="oblast"
                            class="w-full md:w-3/4 px-3 py-1 border rounded focus:border-blue-500 hover:border-blue-500 text-sm outline-none focus:input-shadow">
                        <option value="">{{ __('admin.users.edit.chooseobl') }}</option>
                        @foreach(\App\Models\Locations::where('type', \App\Models\Locations::LOCATION_OBLAST)->get() ?? []  as $item)
                            <option value="{{ $item->id }}">{{ $item->ru_title }}</option>
                        @endforeach
                    </select>
                    @error('oblast')
                    <p class="text-xs text-red-600">{{ $message }}</p>
                    @enderror
                </div>
            </div>

            @if($oblast)
                <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-center">
                    <div class="w-full md:w-1/4">
                        <label for="raion" class="w-full text-left md:text-right block text-sm">{{ __('admin.users.edit.rai') }}</label>
                    </div>
                    <div class="w-full md:w-3/4">
                        <select wire:model="raion"
                                class="w-full md:w-3/4 px-3 py-1 border rounded focus:border-blue-500 hover:border-blue-500 text-sm outline-none focus:input-shadow">
                            <option value="">{{ __('admin.users.edit.chooserai') }}</option>
                            @foreach(\App\Models\Locations::where('type', \App\Models\Locations::LOCATION_RAYON)->where('parent',$oblast)->get() ?? []  as $item)
                                <option value="{{ $item->id }}">{{ $item->ru_title }}</option>
                            @endforeach
                        </select>
                        @error('raion')
                        <p class="text-xs text-red-600">{{ $message }}</p>
                        @enderror
                    </div>
                </div>
                @if($raion)
                    <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-center">
                        <div class="w-full md:w-1/4">
                            <label for="gorod" class="w-full text-left md:text-right block text-sm">{{ __('admin.users.edit.city') }}</label>
                        </div>
                        <div class="w-full md:w-3/4">
                            <select wire:model="gorod"
                                    class="w-full md:w-3/4 px-3 py-1 border rounded focus:border-blue-500 hover:border-blue-500 text-sm outline-none focus:input-shadow">
                                <option value="">{{ __('admin.users.edit.choosecity') }}</option>
                                @foreach(\App\Models\Locations::where('type', \App\Models\Locations::LOCATION_AUL)->where('parent',$raion)->get() ?? []  as $item)
                                    <option value="{{ $item->id }}">{{ $item->ru_title }}</option>
                                @endforeach
                            </select>
                            @error('gorod')
                            <p class="text-xs text-red-600">{{ $message }}</p>
                            @enderror
                        </div>
                    </div>
                    <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-center">
                        <div class="w-full md:w-1/4">
                            <label for="institute" class="w-full text-left md:text-right block text-sm">{{ __('admin.users.edit.job') }}</label>
                        </div>
                        <div class="w-full md:w-3/4" {{-- x-data x-init="institute = new SlimSelect({allowDeselect: true, select: $refs.institute, placeholder:'Select institute'});" wire:ignore --}} >
                            <select wire:model="user.institute"  {{--x-on:change="$wire.set('user.institute', institute.selected())" id="institute" x-ref="institute" --}}
                            class="w-full md:w-3/4 px-3 py-1 border rounded focus:border-blue-500 hover:border-blue-500 text-sm outline-none focus:input-shadow">
                                <option value="">{{ __('admin.users.edit.choosejob') }}</option>
                                @foreach(\App\Models\Institutes::where('oblast', $oblast)->where('rayon',$raion)->get() ?? []  as $item)
                                    <option value="{{ $item->id }}">{{ $item->title }}</option>
                                @endforeach
                            </select>
                            @error('user.institute')
                            <p class="text-xs text-red-600">{{ $message }}</p>
                            @enderror
                        </div>
                    </div>
                @endif

            @endif

            <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-center">
                <div class="w-full md:w-1/4">
                    <label for="jobname" class="w-full text-left md:text-right block text-sm">{{ __('admin.users.edit.position') }}</label>
                </div>
                <div class="w-full md:w-3/4">
                    <select wire:model="user.jobname"
                            class="w-full md:w-3/4 px-3 py-1 border rounded focus:border-blue-500 hover:border-blue-500 text-sm outline-none focus:input-shadow">
                        <option value="">{{ __('admin.users.edit.chooseposition') }}</option>
                        @foreach(\App\Models\JobNames::all() ?? []  as $item)
                            <option value="{{ $item->id }}">{{ $item->ru_title }}</option>
                        @endforeach
                    </select>
                    @error('user.jobname')
                    <p class="text-xs text-red-600">{{ $message }}</p>
                    @enderror
                </div>
            </div>
            <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-center">
                <div class="w-full md:w-1/4">
                    <label for="academic_category" class="w-full text-left md:text-right block text-sm">{{ __('admin.users.edit.category') }}</label>
                </div>
                <div class="w-full md:w-3/4">
                    <select wire:model="user.academic_category"
                            class="w-full md:w-3/4 px-3 py-1 border rounded focus:border-blue-500 hover:border-blue-500 text-sm outline-none focus:input-shadow">
                        <option value="">{{ __('admin.users.edit.choosecategory') }}</option>
                        @foreach(\App\Models\AcademicCategories::all() ?? []  as $item)
                            <option value="{{ $item->id }}">{{ $item->ru_title }}</option>
                        @endforeach
                    </select>
                    @error('user.academic_category')
                    <p class="text-xs text-red-600">{{ $message }}</p>
                    @enderror
                </div>
            </div>
            <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-center">
                <div class="w-full md:w-1/4">
                    <label for="education" class="w-full text-left md:text-right block text-sm">{{ __('admin.users.edit.level') }}</label>
                </div>
                <div class="w-full md:w-3/4">
                    <select wire:model="user.education"
                            class="w-full md:w-3/4 px-3 py-1 border rounded focus:border-blue-500 hover:border-blue-500 text-sm outline-none focus:input-shadow">
                        <option value="">{{ __('admin.users.edit.chooselevel') }}</option>
                        @foreach(\App\Models\Educations::all() ?? []  as $item)
                            <option value="{{ $item->id }}">{{ $item->ru_title }}</option>
                        @endforeach
                    </select>
                    @error('user.education')
                    <p class="text-xs text-red-600">{{ $message }}</p>
                    @enderror
                </div>
            </div>
            <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-center">
                <div class="w-full md:w-1/4">
                    <label for="academic_degree" class="w-full text-left md:text-right block text-sm">{{ __('admin.users.edit.academic') }}</label>
                </div>
                <div class="w-full md:w-3/4">
                    <select wire:model="user.academic_degree"
                            class="w-full md:w-3/4 px-3 py-1 border rounded focus:border-blue-500 hover:border-blue-500 text-sm outline-none focus:input-shadow">
                        <option value="">{{ __('admin.users.edit.chooseacademic') }}</option>
                        @foreach(\App\Models\AcademicDegrees::all() ?? []  as $item)
                            <option value="{{ $item->id }}">{{ $item->ru_title }}</option>
                        @endforeach
                    </select>
                    @error('user.academic_degree')
                    <p class="text-xs text-red-600">{{ $message }}</p>
                    @enderror
                </div>
            </div>

            <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-center">
                <div class="w-full md:w-1/4">
                    <label for="subject_teach" class="w-full text-left md:text-right block text-sm">{{ __('admin.users.edit.lesson') }}</label>
                </div>
                <div class="w-full md:w-3/4">
                    <select wire:model="user.teach_subject_id" id="teach_subject_id"
                            class="w-full md:w-3/4 px-3 py-1 border rounded focus:border-blue-500 hover:border-blue-500 text-sm outline-none focus:input-shadow">
                        <option value="">{{ __('admin.users.edit.lesson.placeholder') }}</option>
                        @foreach(\App\Models\TeachSubjects::all() ?? []  as $item)
                            <option value="{{ $item->id }}">{{ $item->ru_title }}</option>
                        @endforeach
                    </select>
                    @error('user.teach_subject_id')
                    <p class="text-xs text-red-600">{{ $message }}</p>
                    @enderror
                </div>
            </div>

            <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-center">
                <div class="w-full md:w-1/4">
                    <label for="stazh" class="w-full text-left md:text-right block text-sm">{{ __('admin.users.edit.staj') }}</label>
                </div>
                <div class="w-full md:w-3/4">
                    <input id="stazh" wire:model.defer="user.stazh" placeholder="{{ __('admin.users.edit.staj.placeholder') }}" class="w-full md:w-3/4 px-3 py-1 border rounded focus:border-blue-500 hover:border-blue-500 text-sm outline-none focus:input-shadow"/>
                    @error('user.stazh')
                    <p class="text-xs text-red-600">{{ $message }}</p>
                    @enderror
                </div>
            </div>

            <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-center">
                <div class="w-full md:w-1/4">
                    <label for="iban" class="w-full text-left md:text-right block text-sm">{{ __('admin.users.edit.iban') }}</label>
                </div>
                <div class="w-full md:w-3/4">
                    <input wire:model="user.iban" type="text" id="iban" placeholder="{{ __('admin.users.edit.iban.placeholder') }}"
                           class="w-full md:w-3/4 px-3 py-1 border rounded focus:border-blue-500 hover:border-blue-500 text-sm outline-none focus:input-shadow"/>
                    @error('user.iban')
                    <p class="text-xs text-red-600">{{ $message }}</p>
                    @enderror
                </div>
            </div>

            <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-center">
                <div class="w-full md:w-1/4">
                    <label for="bank_name" class="w-full text-left md:text-right block text-sm">{{ __('admin.users.edit.bank') }}</label>
                </div>
                <div class="w-full md:w-3/4">
                    <input wire:model="user.bank_name" type="text" id="bank_name" placeholder="{{ __('admin.users.edit.bank.placeholder') }}"
                           class="w-full md:w-3/4 px-3 py-1 border rounded focus:border-blue-500 hover:border-blue-500 text-sm outline-none focus:input-shadow"/>
                    @error('user.bank_name')
                    <p class="text-xs text-red-600">{{ $message }}</p>
                    @enderror
                </div>
            </div>

            {{--            <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-center">--}}
            {{--                <div class="w-full md:w-1/4">--}}
            {{--                    <label for="affiliate" class="w-full text-left md:text-right block text-sm">{{ __('admin.users.edit.affiliate') }}</label>--}}
            {{--                </div>--}}
            {{--                <div class="w-full md:w-3/4">--}}
            {{--                    <select wire:model="user.affiliate" id="affiliate"--}}
            {{--                            class="w-full md:w-3/4 px-3 py-1 border rounded focus:border-blue-500 hover:border-blue-500 text-sm outline-none focus:input-shadow">--}}
            {{--                        <option value="">{{ __('admin.users.edit.affiliate.placeholder') }}</option>--}}
            {{--                        @foreach(\App\Models\Affiliates::all() ?? []  as $item)--}}
            {{--                            <option value="{{ $item->id }}">{{ $item->title }}</option>--}}
            {{--                        @endforeach--}}
            {{--                    </select>--}}
            {{--                    @error('user.affiliate')--}}
            {{--                    <p class="text-xs text-red-600">{{ $message }}</p>--}}
            {{--                    @enderror--}}
            {{--                </div>--}}
            {{--            </div>--}}

            <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-center">
                <div class="w-full md:w-1/4">
                </div>
                <div class="w-full md:w-3/4 space-x-2 flex">
                    <button wire:click="save"
                            class="mb-2 text-sm flex justify-center items-center px-4 py-1 border-0 bg-blue-500 text-white rounded hover:bg-blue-600">
                        {{ __('admin.buttons.save') }}
                    </button>

                    <a href="{{ route('affiliate.groups.index') }}"
                       class="py-1 px-4 border inline-block m-0 mb-2 text-sm rounded hover:border-black">{{ __('admin.buttons.cancel') }}</a>
                </div>
            </div>
        </div>
    </div>

</div>
@push('styles')
    {{--    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/flatpickr/dist/flatpickr.min.css">--}}
    {{--    <script src="https://cdn.jsdelivr.net/npm/flatpickr"></script>--}}
    {{--    <script src="https://cdn.jsdelivr.net/npm/flatpickr/dist/l10n/ru.js"></script>--}}
    <link href="https://unpkg.com/slim-select@1.27.0/dist/slimselect.min.css" rel="stylesheet"/>
    <style>
        html {
            scroll-behavior: smooth;
        }

    </style>
@endpush

@push('scripts')
    <script src="https://unpkg.com/slim-select@1.27.0/dist/slimselect.min.js"></script>
@endpush

