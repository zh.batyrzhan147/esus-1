<div>
    <x-modal>
        <x-slot name="header">
            <div class="flex justify-between">
                <span>{{ __('admin.groups.add.modal.title') }}</span>
                <button wire:click="$set('showAddPopup',false)">
                    <svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="times"
                         class="svg-inline--fa fa-times fa-w-11 w-4 h-4" role="img" xmlns="http://www.w3.org/2000/svg"
                         viewBox="0 0 352 512">
                        <path fill="currentColor"
                              d="M242.72 256l100.07-100.07c12.28-12.28 12.28-32.19 0-44.48l-22.24-22.24c-12.28-12.28-32.19-12.28-44.48 0L176 189.28 75.93 89.21c-12.28-12.28-32.19-12.28-44.48 0L9.21 111.45c-12.28 12.28-12.28 32.19 0 44.48L109.28 256 9.21 356.07c-12.28 12.28-12.28 32.19 0 44.48l22.24 22.24c12.28 12.28 32.2 12.28 44.48 0L176 322.72l100.07 100.07c12.28 12.28 32.2 12.28 44.48 0l22.24-22.24c12.28-12.28 12.28-32.19 0-44.48L242.72 256z"></path>
                    </svg>
                </button>
            </div>
        </x-slot>
        <div class="py-6 px-4 sm:px-6 lg:px-8 h-full">
            <div class="flex flex-col items-start md:items-center w-full md:w-3/4 mx-auto space-y-4">

{{--                <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-center">--}}
{{--                    <div class="w-full md:w-1/4">--}}
{{--                        <label for="affiliate"--}}
{{--                               class="w-full text-left md:text-right block text-sm">{{ __('admin.groups.add.modal.affiliate') }}</label>--}}
{{--                    </div>--}}
{{--                    <div class="w-full md:w-3/4">--}}
{{--                        <select wire:model="affiliate"--}}
{{--                                class="w-full md:w-3/4 px-3 py-1 border rounded focus:border-blue-500 hover:border-blue-500 text-sm outline-none focus:input-shadow">--}}
{{--                            <option value>{{ __('admin.groups.add.modal.chooseaffiliate') }}</option>--}}
{{--                            @foreach(\App\Models\Affiliates::all() ?? []  as $item)--}}
{{--                                <option value="{{ $item->id }}">{{ $item->title }}</option>--}}
{{--                            @endforeach--}}
{{--                        </select>--}}
{{--                        @error('affiliate')--}}
{{--                        <p class="text-xs text-red-600">{{ $message }}</p>--}}
{{--                        @enderror--}}
{{--                    </div>--}}
{{--                </div>--}}

                <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-center">
                    <div class="w-full md:w-1/4">
                        <label for="year"
                               class="w-full text-left md:text-right block text-sm">{{ __('admin.groups.add.modal.year') }}</label>
                    </div>
                    <div class="w-full md:w-3/4">
                        <select wire:model="year"
                                class="w-full md:w-3/4 px-3 py-1 border rounded focus:border-blue-500 hover:border-blue-500 text-sm outline-none focus:input-shadow">
                            <option value>{{ __('admin.groups.add.modal.chooseyear') }}</option>
                            @foreach(\App\Models\Years::orderBy('year','ASC')->get() as $item)
                                <option value="{{ $item->year }}">{{ $item->year }}</option>
                            @endforeach
                        </select>
                        @error('year')
                        <p class="text-xs text-red-600">{{ $message }}</p>
                        @enderror
                    </div>
                </div>

                <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-center">
                    <div class="w-full md:w-1/4">
                        <label for="course"
                               class="w-full text-left md:text-right block text-sm">{{ __('admin.groups.add.modal.program') }}</label>
                    </div>
                    <div class="w-full md:w-3/4">
                        <select wire:model="course"
                                class="w-full md:w-3/4 px-3 py-1 border rounded focus:border-blue-500 hover:border-blue-500 text-sm outline-none focus:input-shadow">
                            <option value>{{ __('admin.groups.add.modal.chooseprogram') }}</option>
                            @foreach($yearItem->courses ?? [] as $courseItem)
                                <option value="{{ $courseItem->id }}">{{ $courseItem->ru_title }}</option>
                            @endforeach
                        </select>
                        @error('course')
                        <p class="text-xs text-red-600">{{ $message }}</p>
                        @enderror
                    </div>
                </div>

                <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-center">
                    <div class="w-full md:w-1/4">
                        <label for="language"
                               class="w-full text-left md:text-right block text-sm">{{ __('admin.groups.add.modal.lang') }}</label>
                    </div>
                    <div class="w-full md:w-3/4">
                        <select wire:model="language"
                                class="w-full md:w-3/4 px-3 py-1 border rounded focus:border-blue-500 hover:border-blue-500 text-sm outline-none focus:input-shadow">
                            <option value="">{{ __('admin.groups.add.modal.chooselang') }}</option>
                            @foreach(\App\Models\Languages::all() as $item)
                                <option value="{{ $item->id }}">{{ $item->title }}</option>
                            @endforeach
                        </select>
                        @error('language')
                        <p class="text-xs text-red-600">{{ $message }}</p>
                        @enderror
                    </div>
                </div>

                <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-center">
                    <div class="w-full md:w-1/4">
                        <label for="traineraffiliate"
                               class="w-full text-left md:text-right block text-sm">{{ __('admin.groups.add.modal.affiliate.trainer') }}</label>
                    </div>
                    <div class="w-full md:w-3/4">
                        <select wire:model="traineraffiliate"
                                class="w-full md:w-3/4 px-3 py-1 border rounded focus:border-blue-500 hover:border-blue-500 text-sm outline-none focus:input-shadow">
                            <option value>{{ __('admin.groups.add.modal.chooseaffiliate') }}</option>
                            @foreach(\App\Models\Affiliates::all() ?? []  as $item)
                                <option value="{{ $item->id }}">{{ $item->title }}</option>
                            @endforeach
                        </select>
                        @error('traineraffiliate')
                        <p class="text-xs text-red-600">{{ $message }}</p>
                        @enderror
                    </div>
                </div>

                <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-center">
                    <div class="w-full md:w-1/4">
                        <label for="course"
                               class="w-full text-left md:text-right block text-sm">{{ __('admin.groups.add.modal.trainer') }}</label>
                    </div>
                    <div class="w-full md:w-3/4">
                        <select wire:model="trainer"
                                class="w-full md:w-3/4 px-3 py-1 border rounded focus:border-blue-500 hover:border-blue-500 text-sm outline-none focus:input-shadow">
                            <option value>{{ __('admin.groups.add.modal.choosetrainer') }}</option>
                            @foreach($trainers ?? []  as $item)
                                <option value="{{ $item->id }}">{{ $item->fio }}</option>
                            @endforeach
                        </select>
                        @error('trainer')
                        <p class="text-xs text-red-600">{{ $message }}</p>
                        @enderror
                    </div>
                </div>


{{--                <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-center">--}}
{{--                    <div class="w-full md:w-1/4">--}}
{{--                        <label for="slug"--}}
{{--                               class="w-full text-left md:text-right block text-sm">{{ __('admin.groups.add.modal.cifr') }}</label>--}}
{{--                    </div>--}}
{{--                    <div class="w-full md:w-3/4">--}}
{{--                        <input wire:model="slug" type="text"--}}
{{--                               placeholder="{{ __('admin.groups.add.modal.cifr.placeholder') }}"--}}
{{--                               class="w-full md:w-3/4 px-3 py-1 border rounded focus:border-blue-500 hover:border-blue-500 text-sm outline-none focus:input-shadow"--}}
{{--                        />--}}
{{--                        @error('slug')--}}
{{--                        <p class="text-xs text-red-600">{{ $message }}</p>--}}
{{--                        @enderror--}}
{{--                    </div>--}}
{{--                </div>--}}

{{--                <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-center">--}}
{{--                    <div class="w-full md:w-1/4">--}}
{{--                        <label for="offer_agreement"--}}
{{--                               class="w-full text-left md:text-right block text-sm">{{ __('admin.groups.add.modal.dogovor') }}</label>--}}
{{--                    </div>--}}
{{--                    <div class="w-full md:w-3/4">--}}
{{--                        <input wire:model="offer_agreement" type="file"--}}
{{--                               class="w-full md:w-3/4 px-3 py-1 border rounded focus:border-blue-500 hover:border-blue-500 text-sm outline-none focus:input-shadow"--}}
{{--                        />--}}
{{--                        @error('offer_agreement')--}}
{{--                        <p class="text-xs text-red-600">{{ $message }}</p>--}}
{{--                        @enderror--}}
{{--                    </div>--}}
{{--                </div>--}}

                <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-center">
                    <div class="w-full md:w-1/4">
                        <label for="count_all"
                               class="w-full text-left md:text-right block text-sm">{{ __('admin.groups.add.modal.listeners.count') }}</label>
                    </div>
                    <div class="w-full md:w-3/4">
                        <input wire:model="count_all" type="text"
                               placeholder="{{ __('admin.groups.add.modal.listeners.count.placeholder') }}"
                               class="w-full md:w-3/4 px-3 py-1 border rounded focus:border-blue-500 hover:border-blue-500 text-sm outline-none focus:input-shadow"
                        />
                        @error('count_all')
                        <p class="text-xs text-red-600">{{ $message }}</p>
                        @enderror
                    </div>
                </div>

                @if($start_time)
                    <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-center" wire:key="{{ $start_time ?? 'item-0' }}">
                        <div class="w-full md:w-1/4">
                            <label for="start"
                                   class="w-full text-left md:text-right block text-sm">{{ __('admin.groups.add.modal.start') }}</label>
                        </div>
                        <div class="w-full md:w-3/4">
                            <x-inputs.date wire:model.lazy="start_time"
                                           placeholder="{{ __('admin.groups.add.modal.start.placeholder') }}"
                                           class="w-full md:w-3/4 px-3 py-1 border rounded focus:border-blue-500 hover:border-blue-500 text-sm outline-none focus:input-shadow"/>
                            @error('start_time')
                            <p class="text-xs text-red-600">{{ $message }}</p>
                            @enderror
                        </div>
                    </div>

                    <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-center" wire:key="{{ $end_time ?? 'item-1' }}">
                        <div class="w-full md:w-1/4">
                            <label for="end"
                                   class="w-full text-left md:text-right block text-sm">{{ __('admin.groups.add.modal.end') }}</label>
                        </div>
                        <div class="w-full md:w-3/4">
                            <x-inputs.date  wire:model.lazy="end_time"
                                           placeholder="{{ __('admin.groups.add.modal.end.placeholder') }}"
                                           class="w-full md:w-3/4 px-3 py-1 border rounded focus:border-blue-500 hover:border-blue-500 text-sm outline-none focus:input-shadow"/>
                            @error('end_time')
                            <p class="text-xs text-red-600">{{ $message }}</p>
                            @enderror
                        </div>
                    </div>
                @else
                    <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-center" wire:key="{{ $start_time ?? 'item-2' }}">
                        <div class="w-full md:w-1/4">
                            <label for="start"
                                   class="w-full text-left md:text-right block text-sm">{{ __('admin.groups.add.modal.start') }}</label>
                        </div>
                        <div class="w-full md:w-3/4">
                            <x-inputs.date wire:model.lazy="start_time"
                                           placeholder="{{ __('admin.groups.add.modal.start.placeholder') }}"
                                           class="w-full md:w-3/4 px-3 py-1 border rounded focus:border-blue-500 hover:border-blue-500 text-sm outline-none focus:input-shadow"/>
                            @error('start_time')
                            <p class="text-xs text-red-600">{{ $message }}</p>
                            @enderror
                        </div>
                    </div>
                @endif
                <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-center" wire:key="{{ 'practice.start' }}">
                    <div class="w-full md:w-1/4">
                        <label for="practice_start" placeholder="{{ __('admin.groups.add.modal.practice.start.placeholder') }}"
                               class="w-full text-left md:text-right block text-sm">{{ __('admin.groups.add.modal.practice.start') }}</label>
                    </div>
                    <div class="w-full md:w-3/4">
                        <x-inputs.date id="practice_start" wire:model.lazy="practice_start"
                                       class="w-full md:w-3/4 px-3 py-1 border rounded focus:border-blue-500 hover:border-blue-500 text-sm outline-none focus:input-shadow"/>
                        @error('practice_start')
                        <p class="text-xs text-red-600">{{ $message }}</p>
                        @enderror
                    </div>
                </div>

                <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-center" wire:key="{{ 'practice.end' }}">
                    <div class="w-full md:w-1/4">
                        <label for="practice_end" placeholder="{{ __('admin.groups.add.modal.practice.end.placeholder') }}"
                               class="w-full text-left md:text-right block text-sm">{{ __('admin.groups.add.modal.practice.end') }}</label>
                    </div>
                    <div class="w-full md:w-3/4">
                        <x-inputs.date id="practice_end" wire:model.lazy="practice_end"
                                       class="w-full md:w-3/4 px-3 py-1 border rounded focus:border-blue-500 hover:border-blue-500 text-sm outline-none focus:input-shadow"/>
                        @error('practice_end')
                        <p class="text-xs text-red-600">{{ $message }}</p>
                        @enderror
                    </div>
                </div>

                <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-center">
                    <div class="w-full md:w-1/4">
                    </div>
                    <div class="w-full md:w-3/4 space-x-2 flex">
                        <button type="submit" wire:click="addGroup"
                                class="mb-2 text-sm flex justify-center items-center px-4 py-1 border-0 bg-blue-500 text-white rounded hover:bg-blue-600">
                            {{ __('admin.buttons.create') }}
                        </button>
                        <button wire:click="$set('showAddPopup',false)"
                                class="py-1 px-4 border inline-block m-0 mb-2 text-sm rounded hover:border-black">{{ __('admin.buttons.cancel') }}
                        </button>
                    </div>
                </div>

            </div>
        </div>

    </x-modal>
</div>
