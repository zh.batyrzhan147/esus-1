<div class="py-4">
    <div class="flex w-full justify-between items-end mb-5 space-x-1 px-4">
        <h2 class="text-2xl font-bold text-black">{{ $group->coursesItem->ru_title ?? $group->coursesItem->kz_title }}</h2>
        <div class="flex space-x-1">

        </div>
    </div>

    <div class="w-full py-2.5 flex flex-col">
        <div class="w-full flex flex-col mb-5">
            <div class="w-full px-4 flex justify-between items-center mb-1">
                <h2 class="w-full text-xl text-[#18181B] mb-4">{{ __('trainer.groups.tables.todate2') }} {{ date('d.m.Y', strtotime($dates[1])) }}-{{ date('d.m.Y', strtotime($dates[7])) }}</h2>
                <div class="flex space-x-1">
                    <div class="relative" x-data="{ open: false }">
                        <button class="flex text-[#A1A1AA] py-[8px] px-[12px] border border-[#A1A1AA] rounded space-x-8 items-center hover:text-[#18181B] hover:border-[#18181B]" @click="open = ! open" @click.away="open = false" @close.stop="open = false">
                            <span class="whitespace-nowrap">{{ date('d.m', strtotime($dates[1])) }}-{{ date('d.m', strtotime($dates[7])) }}</span>
                            <svg width="11" height="8" viewBox="0 0 11 8" fill="none" xmlns="http://www.w3.org/2000/svg" x-bind:class="open ? 'rotate-180' : ''">
                                <path d="M5.91105 6.87762L1.69952 1.07153H1.7877L5.50701 6.19818L5.91172 6.75603L6.31643 6.19818L10.0357 1.07153H10.1226L5.91105 6.87762Z" fill="currentColor" stroke="currentColor"/>
                            </svg>
                        </button>
                        <div class="absolute top-[50px]">
                            <div x-show="open" x-transition:enter="transition ease-out duration-200" x-transition:enter-start="transform opacity-0 scale-95" x-transition:enter-end="transform opacity-100 scale-100" x-transition:leave="transition ease-in duration-75" x-transition:leave-start="transform opacity-100 scale-100" x-transition:leave-end="transform opacity-0 scale-95" class="absolute z-50 w-44 rounded shadow-lg origin-top-right bg-[#FFFFFF]" @click="open = false" style="display: none;">
                                @foreach($weeks as $key => $week)
                                <button wire:click="changeWeek('{{$key}}')" class="block w-full px-4 py-2 leading-5 text-gray-700 @if($key == $dates[1]) bg-gray-100 @endif hover:bg-gray-100 focus:outline-none focus:bg-gray-100 transition duration-150 ease-in-out">{{ $week }}</button>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="px-4 flex w-full justify-between mb-5">
                <div class="flex w-full space-x-1 items-center">
                    <div class="flex w-8 h-8 rounded-full bg-green-600"></div><div>{{ __('trainer.groups.tables.visited') }}</div>
                </div>
                <div class="flex w-full space-x-1 items-center">
                    <div class="flex w-8 h-8 rounded-full bg-blue-600"></div><div>{{ __('trainer.groups.tables.visited.remote') }}</div>
                </div>
                <div class="flex w-full space-x-1 items-center">
                    <div class="flex w-8 h-8 rounded-full bg-red-600"></div><div>{{ __('trainer.groups.tables.notvisited') }}</div>
                </div>
                <div class="flex w-full space-x-1 items-center">
                    <div class="flex w-8 h-8 rounded-full bg-teal-400"></div><div>{{ __('trainer.groups.tables.valid') }}</div>
                </div>
                <div class="flex w-full space-x-1 items-center">
                    <div class="flex w-8 h-8 rounded-full bg-gray-200"></div><div>{{ __('trainer.groups.tables.noinfo') }}</div>
                </div>
            </div>
            <div class="flex flex-col">
                <table class="table-fixed w-full">
                    <thead class="bg-[#F4F4F5] border-b-2 border-[#D4D4D8]">
                    <tr>
                        <th scope="col"
                            class="w-full text-left font-normal text-[15px] text-[#18181B] py-2.5 px-4">
                            ФИО
                        </th>
                        @foreach($dates as $date)
                        <th scope="col" class="w-20 font-normal text-[15px] text-[#18181B] py-2.5 px-4">{{ date('d.m', strtotime($date)) }}</th>
                        @endforeach
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($group->users as $user)
                        <tr class="bg-[#FFFFFF] hover:bg-[#F4F4F5] hover:cursor-pointer border-b-2 border-[#D4D4D8]">
                            <td class="w-full text-left font-light text-[#3A8CF7] py-2.5 px-4">{{ $user->fio }}</td>
                            @foreach($dates as $date)
                                <td @if($group->change_statements) wire:click="$emit('changeStatus',{{ $user->id}},'{{ $date }}')" @endif
                                    class="w-20 text-center font-light text-[#18181B] py-2.5 px-4"><div class="flex w-full justify-center items-center"><div class="flex w-8 h-8 rounded-full  {{ \App\Models\Statements::listColorTypes()[($data[$user->id]['items'][$date] ?? '') ] }}"></div></div></td>
                            @endforeach
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>


@if($group->change_statements)
    @push('scripts')
        <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>

        <script type="text/javascript">
            document.addEventListener('DOMContentLoaded', function () {
                @this.on('changeStatus', (itemID, d) => {
                    Swal.fire({
                        title: 'Выберите статус',
                        input: 'select',
                        inputOptions: {
                            {{ \App\Models\Statements::STATEMENTS_ATTEND }}: "{{ \App\Models\Statements::listTypes()[\App\Models\Statements::STATEMENTS_ATTEND] }}",
                            {{ \App\Models\Statements::STATEMENTS_ONLINE }}: "{{ \App\Models\Statements::listTypes()[\App\Models\Statements::STATEMENTS_ONLINE] }}",
                            {{ \App\Models\Statements::STATEMENTS_MISSING }}: "{{ \App\Models\Statements::listTypes()[\App\Models\Statements::STATEMENTS_MISSING] }}",
                            {{ \App\Models\Statements::STATEMENTS_SICK }}: "{{ \App\Models\Statements::listTypes()[\App\Models\Statements::STATEMENTS_SICK] }}"
                        },
                        inputPlaceholder: 'Выберите статус',
                        showCancelButton: true,

                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#aaa',
                        cancelButtonText: '{{ __("admin.swal.temp.buttons.cancel") }}',
                        confirmButtonText: '{{ __("admin.swal.temp.buttons.save") }}',

                        inputValidator: (value) => {
                            @this.call('changeStatement', itemID, d, value)
                        }
                    })
                });

                @this.on('successChanged', () => {
                    Swal.fire({title: '{{ __("admin.swal.grants.title.success") }}', icon: 'success'});
                })
            })
        </script>

    @endpush
@endif
