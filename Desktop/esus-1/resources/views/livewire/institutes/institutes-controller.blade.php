<div>
    <x-slot name="header">
        <div class="bg-white mx-auto py-6 px-4 sm:px-6 lg:px-8">
            <div class="flex justify-between items-center w-full">
                <h2 class="font-semibold text-xl text-gray-800 leading-tight">
                    {{ __('admin.institutes.list.title') }}
                </h2>

                @include('admin.common.secondmenu', [
                    'tabname'=>'institutes',
                    'popup'=>false,
                    'addlink'=>'admin.institutes.create'
                ])
            </div>
        </div>
    </x-slot>

    <div class="flex flex-col mx-auto py-2 px-4 sm:px-6 lg:px-8">
        <div class="w-full flex justify-between space-x-4">
            <div class="flex flex-col mb-4 w-96">
                <label class="flex flex-col text-left">
                    <span class="text-gray-700 mb-2">{{ __('admin.institutes.list.filter.type') }}</span>
                </label>
                <select  wire:model="type" class="w-full md:w-3/4 px-3 py-1 border rounded focus:border-blue-500 hover:border-blue-500 text-sm outline-none focus:input-shadow">
                    <option value>{{ __('admin.courses.index.filter.all') }}</option>
                    @foreach(\App\Models\Institutes::listTypes() as $key => $list)
                        <option value="{{ $key }}">{{ $list }}</option>
                    @endforeach
                </select>
                @error('cityItem')
                <p class="text-xs text-red-600">{{ $message }}</p>
                @enderror
            </div>
            <div class="flex justify-between items-center mb-4">
                <label class="flex flex-col text-left">
                    <span class="text-gray-700 mb-2">{{ __('Область') }}</span>
                    <select wire:model="obl"
                            class="w-48 px-3 py-1 border rounded focus:border-blue-500 hover:border-blue-500 text-sm outline-none focus:input-shadow">
                        <option value>{{ __('admin.courses.index.filter.all') }}</option>
                        @foreach($oblasti as $item)
                            <option value="{{ $item->id }}">{{ $item->ru_title }}</option>
                        @endforeach
                    </select>
                </label>
            </div>
            <div class="flex justify-between items-center mb-4">
                <label class="flex flex-col text-left">
                    <span class="text-gray-700 mb-2">{{ __('Район') }}</span>
                    <select wire:model="raion"
                            class="w-48 px-3 py-1 border rounded focus:border-blue-500 hover:border-blue-500 text-sm outline-none focus:input-shadow">
                        <option value>{{ __('admin.courses.index.filter.all') }}</option>
                        @foreach($oblItem->childrens ?? [] as $item)
                            <option value="{{ $item->id }}">{{ $item->ru_title }}</option>
                        @endforeach
                    </select>
                </label>
            </div>
            <div class="flex justify-between items-center mb-4">
                <label class="flex flex-col text-left">
                    <span class="text-gray-700 mb-2">{{ __('Населенный пункт') }}</span>
                    <select wire:model="city"
                            class="w-48 px-3 py-1 border rounded focus:border-blue-500 hover:border-blue-500 text-sm outline-none focus:input-shadow">
                        <option value>{{ __('admin.courses.index.filter.all') }}</option>
                        @foreach($raionItem->childrens ?? [] as $item)
                            <option value="{{ $item->id }}">{{ $item->ru_title }}</option>
                        @endforeach
                    </select>
                </label>
            </div>
        </div>



    </div>

    <div class="py-6 px-4 sm:px-6 lg:px-8">
        <div class="flex flex-col">
            <div class="-my-2 overflow-x-auto sm:-mx-6 lg:-mx-8">
                <div class="py-2 align-middle inline-block min-w-full sm:px-6 lg:px-8">
                    <div class="shadow overflow-hidden border-b border-gray-200 sm:rounded">
                        <table class="min-w-full divide-y divide-gray-200 w-full">
                            <thead>
                            <tr>
                                <th scope="col"
                                    class="px-2 py-3 bg-gray-50 text-left text-sm font-medium text-gray-800">
                                    <div class="text-center">{{ __('admin.table.head.id') }}</div>
                                </th>
                                <th scope="col"
                                    class="px-2 py-3 bg-gray-50 text-left text-sm font-medium text-gray-800">
                                    <div class="pl-4">{{ __('admin.table.head.title') }}</div>
                                </th>
                                <th scope="col"
                                    class="px-2 py-3 bg-gray-50 text-left text-sm font-medium text-gray-800">
                                    <div class="pl-4 border-l border-gray-200">{{ __('admin.table.head.location') }}</div>
                                </th>
                                <th scope="col" width="200"
                                    class="px-2 py-3 bg-gray-50 text-left text-sm font-medium text-gray-800">
                                    <div class="pl-4 border-l border-gray-200">{{ __('admin.table.head.action') }}</div>
                                </th>
                            </tr>
                            </thead>
                            <tbody class="bg-white divide-y divide-gray-200">
                            @foreach ($institutes as $institute)
                                <tr>
                                    <td class="px-6 py-1.5 text-sm text-gray-900">
                                        {{ $institute->id }}
                                    </td>
                                    <td class="px-6 py-1.5 text-sm text-gray-900">
                                        {{ $institute->title }}
                                    </td>
                                    <td class="px-6 py-1.5 whitespace-nowrap text-sm text-gray-900">
                                        {{ $institute->Location }}
                                    </td>
                                    <td class="px-6 py-1.5 whitespace-nowrap text-sm font-medium space-x-2">
                                        <a href="{{ route('admin.institutes.edit', $institute->id) }}"
                                           class="text-blue-500 hover:text-blue-900 m-0 font-normal">{{ __('admin.links.edit') }}</a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="mt-4 flex flex-row justify-between">
                        <select wire:model="paginationCount">
                            <option value="10">10</option>
                            <option value="50">50</option>
                            <option value="100">100</option>
                            <option value="200">200</option>
                        </select>
                        <div class="whitespace-nowrap">
                        {{ $institutes->appends(request()->query())->links('') }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
