<div>
    <x-slot name="header">
        <div class="bg-white mx-auto py-6 px-4 sm:px-6 lg:px-8">
            <h2 class="font-semibold text-xl text-gray-800 leading-tight" wire:ignore>
                @if(request()->routeIs('admin.institutes.create'))
                    {{ __('admin.institutes.create.title') }}
                @else
                    {{ __('admin.institutes.edit.title') }}
                @endif
            </h2>
        </div>
    </x-slot>
    <p class="px-4 sm:px-6 lg:px-8 text-sm" wire:ignore>@if(request()->routeIs('admin.institutes.create')) {{ __('admin.institutes.create.description') }} @else {{ __('admin.institutes.edit.description') }} @endif</p>
    <div class="py-6 px-4 sm:px-6 lg:px-8 h-full">
        <div class="flex flex-col items-start md:items-center w-full md:w-3/4 mx-auto space-y-4">


            <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-center">
                <div class="w-full md:w-1/4">
                    <label for="type" class="w-full text-left md:text-right block text-sm">{{ __('admin.institutes.create.form.type') }}</label>
                </div>
                <div class="w-full md:w-3/4">
                    <select wire:model="institute.type" id="type"
                            class="w-full md:w-3/4 px-3 py-1 border rounded focus:border-blue-500 hover:border-blue-500 text-sm outline-none focus:input-shadow">
                        <option>{{ __('admin.institutes.create.form.choosetype') }}</option>
                        @foreach(\App\Models\Institutes::listTypes() as $key=>$item)
                            <option value="{{$key}}">{{ $item }}</option>
                        @endforeach
                    </select>
                    @error('institute.type')
                    <p class="text-xs text-red-600">{{ $message }}</p>
                    @enderror
                </div>
            </div>

            <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-center">
                <div class="w-full md:w-1/4">
                    <label for="title" class="w-full text-left md:text-right block text-sm">{{ __('admin.institutes.create.form.title') }}</label>
                </div>
                <div class="w-full md:w-3/4">
                    <input wire:model="institute.title" type="text" id="title" placeholder="{{ __('admin.institutes.create.form.title.placeholder') }}"
                           class="w-full md:w-3/4 px-3 py-1 border rounded focus:border-blue-500 hover:border-blue-500 text-sm outline-none focus:input-shadow"/>
                    @error('institute.title')
                    <p class="text-xs text-red-600">{{ $message }}</p>
                    @enderror
                </div>
            </div>

            <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-center">
                <div class="w-full md:w-1/4">
                    <label for="kz_title" class="w-full text-left md:text-right block text-sm">{{ __('admin.institutes.create.form.kztitle') }}</label>
                </div>
                <div class="w-full md:w-3/4">
                    <input wire:model="institute.kz_title" type="text" id="kz_title" placeholder="{{ __('admin.institutes.create.form.kztitle.placeholder') }}"
                           class="w-full md:w-3/4 px-3 py-1 border rounded focus:border-blue-500 hover:border-blue-500 text-sm outline-none focus:input-shadow"
                           value="{{ old('title', '') }}"/>
                    @error('institute.kz_title')
                    <p class="text-xs text-red-600">{{ $message }}</p>
                    @enderror
                </div>
            </div>

            <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-center">
                <div class="w-full md:w-1/4">
                    <label for="ru_title" class="w-full text-left md:text-right block text-sm">{{ __('admin.institutes.create.form.rutitle') }}</label>
                </div>
                <div class="w-full md:w-3/4">
                    <input wire:model="institute.ru_title" type="text" id="ru_title" placeholder="{{ __('admin.institutes.create.form.rutitle.placeholder') }}"
                           class="w-full md:w-3/4 px-3 py-1 border rounded focus:border-blue-500 hover:border-blue-500 text-sm outline-none focus:input-shadow"
                           value="{{ old('title', '') }}"/>
                    @error('institute.ru_title')
                    <p class="text-xs text-red-600">{{ $message }}</p>
                    @enderror
                </div>
            </div>

            <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-center">
                <div class="w-full md:w-1/4">
                    <label for="address" class="w-full text-left md:text-right block text-sm">{{ __('admin.institutes.create.form.address') }}</label>
                </div>
                <div class="w-full md:w-3/4">
                    <input wire:model="institute.address" type="text" id="address" placeholder="{{ __('admin.institutes.create.form.address.placeholder') }}"
                           class="w-full md:w-3/4 px-3 py-1 border rounded focus:border-blue-500 hover:border-blue-500 text-sm outline-none focus:input-shadow"
                           value="{{ old('title', '') }}"/>
                    @error('institute.address')
                    <p class="text-xs text-red-600">{{ $message }}</p>
                    @enderror
                </div>
            </div>

            <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-center">
                <div class="w-full md:w-1/4">
                    <label for="oblast" class="w-full text-left md:text-right block text-sm">{{ __('admin.institutes.create.form.obl') }}</label>
                </div>
                <div class="w-full md:w-3/4">
                    <select wire:model="oblastItem" id="oblast"
                            class="w-full md:w-3/4 px-3 py-1 border rounded focus:border-blue-500 hover:border-blue-500 text-sm outline-none focus:input-shadow">
                        <option>{{ __('admin.institutes.create.form.chooseobl') }}</option>
                        @foreach($oblasts as $parent)
                            <option value="{{ $parent->id }}">{{ $parent->kz_title }}</option>
                        @endforeach
                    </select>
                    @error('oblastItem')
                    <p class="text-xs text-red-600">{{ $message }}</p>
                    @enderror
                </div>
            </div>
            @if($oblastItem != null)
                <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-center">
                    <div class="w-full md:w-1/4">
                        <label for="rayon" class="w-full text-left md:text-right block text-sm">{{ __('admin.institutes.create.form.rai') }}</label>
                    </div>
                    <div class="w-full md:w-3/4">
                        @if($oblastValue)
                            <select wire:model="rayonItem" id="rayon"
                                    class="w-full md:w-3/4 px-3 py-1 border rounded focus:border-blue-500 hover:border-blue-500 text-sm outline-none focus:input-shadow">
                                <option>{{ __('admin.institutes.create.form.chooserai') }}</option>

                                @foreach($oblastValue->childrens()->where('type',\App\Models\Locations::LOCATION_RAYON)->get() as $parent)
                                    <option value="{{ $parent->id }}">{{ $parent->kz_title }}</option>
                                @endforeach

                            </select>
                            @error('rayonItem')
                            <p class="text-xs text-red-600">{{ $message }}</p>
                            @enderror
                        @endif
                    </div>
                </div>
                @if($rayonItem)
                    <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-center">
                        <div class="w-full md:w-1/4">
                            <label for="city" class="w-full text-left md:text-right block text-sm">{{ __('admin.institutes.create.form.city') }}</label>
                        </div>
                        <div class="w-full md:w-3/4">
                            <select wire:model="cityItem" id="city"
                                    class="w-full md:w-3/4 px-3 py-1 border rounded focus:border-blue-500 hover:border-blue-500 text-sm outline-none focus:input-shadow">
                                <option>{{ __('admin.institutes.create.form.choosecity') }}</option>
                                @foreach($rayonValue->childrens()->where('type',\App\Models\Locations::LOCATION_AUL)->get() as $parent)
                                    <option value="{{ $parent->id }}">{{ $parent->kz_title }}</option>
                                @endforeach
                            </select>
                            @error('cityItem')
                            <p class="text-xs text-red-600">{{ $message }}</p>
                            @enderror
                        </div>
                    </div>
                @endif
            @endif

            <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-center">
                <div class="w-full md:w-1/4">
                    <label for="type_org" class="w-full text-left md:text-right block text-sm">{{ __('admin.institutes.create.form.type2') }}</label>
                </div>
                <div class="w-full md:w-3/4">
                    <select wire:model="institute.type_org" id="type_org"
                           class="w-full md:w-3/4 px-3 py-1 border rounded focus:border-blue-500 hover:border-blue-500 text-sm outline-none focus:input-shadow">
                        @foreach(\App\Models\Institutes::listMagnetic() as $key => $item)
                            <option value="{{ $key }}">{{ $item }}</option>
                        @endforeach
                    </select>
                    @error('institute.type_org')
                    <p class="text-xs text-red-600">{{ $message }}</p>
                    @enderror
                </div>
            </div>
            @if($institute->type_org == \App\Models\Institutes::INSTITUTE_TYPE_MAGNIT)
                <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-center">
                    <div class="w-full md:w-1/4">
                        <label for="parent_id" class="w-full text-left md:text-right block text-sm">{{ __('') }}</label>
                    </div>
                    <div class="w-full md:w-3/4">
                        <select wire:model="institute.parent_id"
                                class="w-full md:w-3/4 px-3 py-1 border rounded focus:border-blue-500 hover:border-blue-500 text-sm outline-none focus:input-shadow">
                            @foreach(\App\Models\Institutes::where('type_org',\App\Models\Institutes::INSTITUTE_TYPE_VEDUSH)->get() as $item)
                                <option value="{{ $item->id }}">{{ $item->ru_title }}</option>
                            @endforeach
                        </select>
                        @error('institute.parent_id')
                        <p class="text-xs text-red-600">{{ $message }}</p>
                        @enderror
                    </div>
                </div>
            @endif


{{--            <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-center">--}}
{{--                <div class="w-full md:w-1/4">--}}
{{--                    <label for="type_school" class="w-full text-left md:text-right block text-sm">{{ __('admin.institutes.create.form.type3') }}</label>--}}
{{--                </div>--}}
{{--                <div class="w-full md:w-3/4">--}}
{{--                    <input wire:model="institute.type_school" type="text" id="type_school" placeholder="{{ __('admin.institutes.create.form.type3.placeholder') }}"--}}
{{--                           class="w-full md:w-3/4 px-3 py-1 border rounded focus:border-blue-500 hover:border-blue-500 text-sm outline-none focus:input-shadow"--}}
{{--                           value="{{ old('title', '') }}"/>--}}
{{--                    @error('institute.type_school')--}}
{{--                    <p class="text-xs text-red-600">{{ $message }}</p>--}}
{{--                    @enderror--}}
{{--                </div>--}}
{{--            </div>--}}

            <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-center">
                <div class="w-full md:w-1/4">
                    <label for="type" class="w-full text-left md:text-right block text-sm">{{ __('admin.institutes.create.forms_id.type') }}</label>
                </div>
                <div class="w-full md:w-3/4">
                    <select wire:model="institute.forms_id" id="type"
                            class="w-full md:w-3/4 px-3 py-1 border rounded focus:border-blue-500 hover:border-blue-500 text-sm outline-none focus:input-shadow">
                        <option>{{ __('admin.institutes.create.forms_id.choosetype') }}</option>
                        @foreach(\App\Models\InstituteForms::all() as $item)
                            <option value="{{$item->id}}">{{ $item->title }}</option>
                        @endforeach
                    </select>
                    @error('institute.type')
                    <p class="text-xs text-red-600">{{ $message }}</p>
                    @enderror
                </div>
            </div>


            <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-center">
                <div class="w-full md:w-1/4">
                </div>
                <div class="w-full md:w-3/4 space-x-2 flex" wire:ignore>
                    <button wire:click="save"
                            class="mb-2 text-sm flex justify-center items-center px-4 py-1 border-0 bg-blue-500 text-white rounded hover:bg-blue-600">
                        @if(request()->routeIs('admin.institutes.create')) {{ __('admin.buttons.create') }} @else {{ __('admin.buttons.save') }} @endif
                    </button>
                    <a href="{{ route('admin.institutes.index', ['type' => ($institute->type ?? 1)]) }}"
                       class="py-1 px-4 border inline-block m-0 mb-2 text-sm rounded hover:border-black">{{ __('admin.buttons.cancel') }}</a>
                </div>
            </div>
        </div>
    </div>

</div>
