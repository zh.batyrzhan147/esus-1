<div x-data="{ open: false }">
    <x-slot name="header"></x-slot>

    <div class="bg-white mx-auto py-6 px-4 sm:px-6 lg:px-8">
        <div class="flex justify-between items-center">
            <h2 class="font-semibold text-xl text-gray-800 leading-tight">
                {{ __('admin.grants.list.title') }}
            </h2>
            @include('admin.common.secondmenu', [
                'tabname'=>'courses',
                'popup'=>true,
                'addlink'=>'admin.programs.create'
            ])

            <button wire:click="add_courses_modal_open" class="mb-2 text-sm flex justify-center items-center space-x-1.5 px-4 py-1 border-0 bg-blue-500 text-white rounded hover:bg-blue-600">
                <svg width="14" height="14" viewBox="0 0 14 14" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M7.71838 1.62408H6.78088C6.69755 1.62408 6.65588 1.66575 6.65588 1.74908V6.65533H2C1.91667 6.65533 1.875 6.697 1.875 6.78033V7.71783C1.875 7.80117 1.91667 7.84283 2 7.84283H6.65588V12.7491C6.65588 12.8324 6.69755 12.8741 6.78088 12.8741H7.71838C7.80172 12.8741 7.84338 12.8324 7.84338 12.7491V7.84283H12.5C12.5833 7.84283 12.625 7.80117 12.625 7.71783V6.78033C12.625 6.697 12.5833 6.65533 12.5 6.65533H7.84338V1.74908C7.84338 1.66575 7.80172 1.62408 7.71838 1.62408Z" fill="white"/>
                </svg>
                <span>{{ __('admin.buttons.create.program') }}</span>
            </button>
        </div>
    </div>

    <div class="flex flex-col mx-auto py-2 px-4 sm:px-6 lg:px-8">
        <div class="w-full flex justify-between items-end space-x-4">
            <div class="flex space-x-4">
                <div class="flex justify-between items-center w-96">
                    <label class="flex flex-col text-left">
                        <span class="text-gray-700 mb-2">{{ __('admin.grants.list.filter.chooseyear') }}</span>
                        <select wire:model.defer="year"
                                class="w-48 px-3 py-1 border rounded focus:border-blue-500 hover:border-blue-500 text-sm outline-none focus:input-shadow">
                            @foreach(\App\Models\Years::orderBy('year','ASC')->get() as $yearItem)
                                <option value="{{ $yearItem->year }}">{{ $yearItem->year }}</option>
                            @endforeach
                        </select>
                    </label>
                </div>
            </div>
            <div class="flex justify-end space-x-4 h-7">
                <button  wire:click="audit" class="text-sm flex justify-center items-center space-x-1.5 px-4 py-1 border-0 bg-red-500 text-white rounded hover:bg-red-600">
                    <span>{{ __('admin.buttons.audit') }}</span>
                </button>
                <button wire:click="applyFilter"
                        class="text-sm flex justify-center items-center space-x-1.5 px-4 py-1 border-0 bg-blue-500 text-white rounded hover:bg-blue-600">
                    <span>{{ __('admin.buttons.accept.filters') }}</span>
                </button>
            </div>
        </div>
    </div>

    <div class="py-6 px-4 sm:px-6 lg:px-8">
        @if (session()->has('audit'))
            <div class="p-4 mb-4 text-sm text-green-700 bg-green-100 rounded-lg dark:bg-green-200 dark:text-green-800" role="alert">
                <span class="font-medium">{{ session('audit')['text'] ?? ''}}</span>
                <a class="underline text-blue-600 hover:text-blue-800 visited:text-purple-600" href="{{ session('audit')['url'] ?? '#'}}">{{ session('audit')['url'] }}</a>
            </div>
        @endif
        <div class="flex flex-col">
            <div class="-my-2 overflow-x-auto sm:-mx-6 lg:-mx-8">
                <div class="py-2 align-middle inline-block min-w-full sm:px-6 lg:px-8">
                    <div class="shadow overflow-hidden border-b border-gray-200 sm:rounded">
                        <table class="min-w-full divide-y divide-gray-200 w-full">
                            <thead>
                            <tr>
                                <th scope="col" class="px-2 py-3 bg-gray-50 text-left text-sm font-medium text-gray-800">
                                    <div class="pl-4">{{ __('admin.table.head.title.program') }}</div>
                                </th>
                                <th scope="col" class="px-2 py-3 bg-gray-50 text-left text-sm font-medium text-gray-800">
                                    <div class="pl-4 border-l border-gray-200 whitespace-nowrap text-right">{{ __('admin.table.head.count.listener') }}</div>
                                </th>
                                @foreach(\App\Models\Languages::all() as $lang)
                                    <th scope="col"
                                        class="px-2 py-3 bg-gray-50 text-left text-sm font-medium text-gray-800">
                                        <div class="pl-4 border-l border-gray-200 text-right">{{ $lang->title }}</div>
                                    </th>
                                @endforeach
                                <th scope="col" width="200" class="px-2 py-3 bg-gray-50 text-left text-sm font-medium text-gray-800">
                                    <div class="pl-4 border-l border-gray-200 text-right">{{ __('admin.table.head.action') }}</div>
                                </th>
                            </tr>
                            </thead>
                            <tbody class="bg-white divide-y divide-gray-200">
                            <tr class="bg-gray-100">
                                <td class="px-6 py-1.5 whitespace-nowrap text-xl text-gray-900 font-bold">
                                    {{ __('admin.grants.table.all.count') }}
                                </td>

                                <td class="px-6 py-1.5 whitespace-nowrap text-xl text-gray-900 text-right">
                                    {{ $grants_all->count_all ?? 0 }}
                                </td>

                                @foreach(\App\Models\Languages::all() as $lang)
                                    <td class="px-6 py-1.5 whitespace-nowrap text-xl text-gray-900 text-right">
                                        {{ $grants_all->countsArray[$lang->slug] ?? 0 }}
                                    </td>
                                @endforeach

                                <td class="px-6 py-1.5 whitespace-nowrap text-sm font-medium space-x-2 text-right">
                                    <button wire:click="edit_store_grants_all_count"  class="text-blue-500 hover:text-blue-900 m-0 font-normal">{{ __('admin.links.edit2') }}</button>
                                </td>
                            </tr>
                                @foreach ($grants as $grant)
                                    <tr>
                                        <td class="px-6 py-1.5 text-sm text-gray-900">
                                            {{ $grant->coursesItem->id }}) {{ $grant->coursesItem->ru_title }}
                                        </td>

                                        <td class="px-6 py-1.5 whitespace-nowrap text-sm text-gray-900 text-right">
                                            {{ $grant->count_all }}
                                        </td>

                                        @foreach(\App\Models\Languages::all() as $lang)
                                            <td class="px-6 py-1.5 whitespace-nowrap text-sm text-gray-900 text-right">
                                                {{ $grant->countsArray[$lang->slug] ?? 0 }}
                                            </td>
                                        @endforeach

                                        <td class="px-6 py-1.5 whitespace-nowrap text-sm font-medium space-x-2 text-right">
                                            <a href="{{ route('admin.grants.edit', $grant->id) }}" class="text-blue-500 hover:text-blue-900 m-0 font-normal">{{ __('admin.links.edit') }}</a>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

        </div>
    </div>

    @if($showModalEdittotalgrants)
        @include('livewire.grants.edit_total_grants')
    @endif

    @if($showModalAddCourses)
        @include('livewire.grants.courses_add')
    @endif
</div>
