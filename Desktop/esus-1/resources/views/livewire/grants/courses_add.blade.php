<div>
    <x-modal>
        <x-slot name="header">
            <div class="flex justify-between mb-3">
                <span>{{ __('admin.grants.modal.title') }}</span>
                <button wire:click="$set('showModalAddCourses',false)">
                    <svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="times" class="svg-inline--fa fa-times fa-w-11 w-4 h-4" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 352 512"><path fill="currentColor" d="M242.72 256l100.07-100.07c12.28-12.28 12.28-32.19 0-44.48l-22.24-22.24c-12.28-12.28-32.19-12.28-44.48 0L176 189.28 75.93 89.21c-12.28-12.28-32.19-12.28-44.48 0L9.21 111.45c-12.28 12.28-12.28 32.19 0 44.48L109.28 256 9.21 356.07c-12.28 12.28-12.28 32.19 0 44.48l22.24 22.24c12.28 12.28 32.2 12.28 44.48 0L176 322.72l100.07 100.07c12.28 12.28 32.2 12.28 44.48 0l22.24-22.24c12.28-12.28 12.28-32.19 0-44.48L242.72 256z"></path></svg>
                </button>
            </div>
        </x-slot>
        <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-center">
            <div class="w-full md:w-4/4">
                <input
                    placeholder="{{ __('admin.grants.modal.title.placeholder') }}"
                    wire:model.debounce.500ms="query"
                    class="w-full px-3 py-1 border rounded focus:border-blue-500 hover:border-blue-500 text-sm outline-none focus:input-shadow"/>
                <div class="z-10 w-full bg-white rounded-t-none shadow-lg list-group">
                    <div class="py-6 px-4 sm:px-6 lg:px-8">
                        <div class="flex flex-col">
                            <div class="-my-2 overflow-x-auto sm:-mx-6 lg:-mx-8">
                                <table class="min-w-full divide-y divide-gray-200 w-full">
                                    <thead>
                                    <tr>
                                        <th scope="col"
                                            class="px-2 py-3 bg-gray-50 text-left text-sm font-medium text-gray-800">
                                            <div class="text-center">{{ __('admin.table.head.id') }}</div>
                                        </th>
                                        <th scope="col"
                                            class="px-2 py-3 bg-gray-50 text-left text-sm font-medium text-gray-800">
                                            <div class="pl-4">{{ __('admin.table.head.title') }}</div>
                                        </th>
                                        <th scope="col"
                                            class="px-2 py-3 bg-gray-50 text-left text-sm font-medium text-gray-800">
                                            <div class="pl-4">{{ __('admin.table.head.action') }}</div>
                                        </th>
                                    </tr>
                                    </thead>
                                    <tbody class="bg-white divide-y divide-gray-200">
                                    @if(!empty($query))
                                        @if(!empty($courses))
                                            @foreach($courses as $i => $cours)
                                                <tr>
                                                    <td class="px-6 py-1.5 text-sm text-gray-900">
                                                        {!! $cours['id'] !!}
                                                    </td>
                                                    <td class="flex flex-col px-6 py-1.5  text-sm text-gray-900 ">
                                                        <span>{!! $cours['kz_title'] !!}</span>
                                                        <span>{!! $cours['ru_title'] !!}</span>
                                                    </td>
                                                    <td class="px-6 py-1.5 whitespace-nowrap text-sm font-medium space-x-2">
                                                        @if(in_array($cours['id'],$grants->pluck('courses')->toArray()))
                                                            <button wire:click="removeGrants({{ $cours['id'] }})"
                                                                    class="text-red-500 hover:text-red-900 m-0 font-normal">{{ __('admin.links.delete') }}</button>
                                                        @else
                                                            <button wire:click="createGrants({{ $cours['id'] }})"
                                                                    class="text-blue-500 hover:text-blue-900 m-0 font-normal">{{ __('admin.buttons.create') }}</button>
                                                        @endif
                                                    </td>
                                                </tr>
                                            @endforeach
                                        @else
                                            <tr class="h-12 w-full"><td scope="col" class="h-12 w-full text-center text-xs text-gray-500" colspan="3">{{ __('admin.table.body.empty') }}</td></tr>
                                        @endif
                                    @else
                                        <tr class="h-12 w-full"><td scope="col" class="h-12 w-full text-center text-xs text-gray-500" colspan="3">{{ __('admin.table.body.empty') }}</td></tr>
                                    @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </x-modal>
</div>
