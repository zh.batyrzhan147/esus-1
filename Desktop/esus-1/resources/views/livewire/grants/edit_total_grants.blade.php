<div>
    <x-modal>
        <x-slot name="header">
            <div class="flex justify-between mb-3">
            <span>{{ __('admin.grants.modal.count.title') }}</span>
            <button wire:click="$set('showModalEdittotalgrants',false)">
                <svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="times" class="svg-inline--fa fa-times fa-w-11 w-4 h-4" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 352 512"><path fill="currentColor" d="M242.72 256l100.07-100.07c12.28-12.28 12.28-32.19 0-44.48l-22.24-22.24c-12.28-12.28-32.19-12.28-44.48 0L176 189.28 75.93 89.21c-12.28-12.28-32.19-12.28-44.48 0L9.21 111.45c-12.28 12.28-12.28 32.19 0 44.48L109.28 256 9.21 356.07c-12.28 12.28-12.28 32.19 0 44.48l22.24 22.24c12.28 12.28 32.2 12.28 44.48 0L176 322.72l100.07 100.07c12.28 12.28 32.2 12.28 44.48 0l22.24-22.24c12.28-12.28 12.28-32.19 0-44.48L242.72 256z"></path></svg>
            </button>
        </x-slot>
        <div class="flex flex-col items-start md:items-center w-full md:w-3/4 mx-auto space-y-4">
            <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-center">
                <div class="w-full md:w-1/4">
                    <label for="grants_all_count" class="w-full text-left md:text-right block text-sm">{{ __('admin.grants.modal.count.program') }}</label>
                </div>
                <div class="w-full md:w-3/4">
                    <input wire:model="grants_all_count" type="text" id="grants_all_count"
                         placeholder="{{ __('admin.grants.modal.count.program.placeholder') }}"  class="w-full md:w-3/4 px-3 py-1 border rounded focus:border-blue-500 hover:border-blue-500 text-sm outline-none focus:input-shadow"
                    />
                    @error('grants_all_count')
                    <p class="text-xs text-red-600">{{ $message }}</p>
                    @enderror
                </div>
            </div>
            <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-center">
                <div class="w-full md:w-1/4">
                </div>
                <div class="w-full md:w-3/4 space-x-2 flex">
                    <button type="submit" wire:click="store_grants_all_count()"
                            class="mb-2 text-sm flex justify-center items-center px-4 py-1 border-0 bg-blue-500 text-white rounded hover:bg-blue-600">
                        {{ __('admin.buttons.save') }}
                    </button>
                    <button wire:click="$set('showModalEdittotalgrants',false)"
                            class="py-1 px-4 border inline-block m-0 mb-2 text-sm rounded hover:border-black">{{ __('admin.buttons.cancel') }}
                    </button>
                </div>
            </div>
        </div>
    </x-modal>
</div>
