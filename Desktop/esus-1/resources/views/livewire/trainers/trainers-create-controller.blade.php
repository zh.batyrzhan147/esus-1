<div>
    <x-slot name="header">
        <div class="bg-white mx-auto py-6 px-4 sm:px-6 lg:px-8">
            <h2 class="font-semibold text-xl text-gray-800 leading-tight">
                {{ __('admin.trainers.create.title') }}
            </h2>
        </div>
    </x-slot>
    @if($new === true)
    <p class="px-4 sm:px-6 lg:px-8 text-sm text-red-600">{{ __('admin.trainers.create.error.notfound') }}</p>
    @endif
    <div class="py-6 px-4 sm:px-6 lg:px-8 h-full">
        <div class="flex flex-col items-start md:items-center w-full md:w-3/4 mx-auto space-y-4">
            @if($new === null)
                <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-center">
                    <div class="w-full md:w-1/4">
                        <label for="iin-search" class="w-full text-left md:text-right block text-sm">{{ __('admin.trainers.create.iin') }}</label>
                    </div>
                    <div class="w-full md:w-3/4">
                        <input wire:model="iin" type="text" id="iin-search" placeholder="{{ __('admin.trainers.create.iin.placeholder') }}"
                               class="w-full md:w-3/4 px-3 py-1 border rounded focus:border-blue-500 hover:border-blue-500 text-sm outline-none focus:input-shadow"/>
                        @error('iin')
                        <p class="text-xs text-red-600">{{ $message }}</p>
                        @enderror
                    </div>
                </div>
                <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-center">
                    <div class="w-full md:w-1/4">
                    </div>
                    <div class="w-full md:w-3/4 space-x-2 flex">
                        <button wire:click="search"
                                class="mb-2 text-sm flex justify-center items-center px-4 py-1 border-0 bg-blue-500 text-white rounded hover:bg-blue-600">
                            {{ __('admin.buttons.search') }}
                        </button>
                        <a href="{{ route('admin.trainers.index') }}"
                           class="py-1 px-4 border inline-block m-0 mb-2 text-sm rounded hover:border-black">{{ __('admin.buttons.cancel') }}</a>
                    </div>
                </div>
            @endif
            @if($new === true)
                    <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-center">
                        <div class="w-full md:w-1/4">
                            <label for="iin" class="w-full text-left md:text-right block text-sm">{{ __('admin.trainers.create.iin') }}</label>
                        </div>
                        <div class="w-full md:w-3/4">
                            <input wire:model="trainer.iin" type="text" id="iin" placeholder="{{ __('admin.trainers.create.iin.placeholder') }}"
                                   class="w-full md:w-3/4 px-3 py-1 border rounded focus:border-blue-500 hover:border-blue-500 text-sm outline-none focus:input-shadow"/>
                            @error('trainer.iin')
                            <p class="text-xs text-red-600">{{ $message }}</p>
                            @enderror
                        </div>
                    </div>

                    <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-center">
                        <div class="w-full md:w-1/4">
                            <label for="last_name" class="w-full text-left md:text-right block text-sm">{{ __('admin.trainers.create.last_name') }}</label>
                        </div>
                        <div class="w-full md:w-3/4">
                            <input wire:model="trainer.last_name" type="text" id="last_name" placeholder="{{ __('admin.trainers.create.last_name.placeholder') }}"
                                   class="w-full md:w-3/4 px-3 py-1 border rounded focus:border-blue-500 hover:border-blue-500 text-sm outline-none focus:input-shadow"/>
                            @error('trainer.last_name')
                            <p class="text-xs text-red-600">{{ $message }}</p>
                            @enderror
                        </div>
                    </div>

                    <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-center">
                        <div class="w-full md:w-1/4">
                            <label for="name" class="w-full text-left md:text-right block text-sm">{{ __('admin.trainers.create.name') }}</label>
                        </div>
                        <div class="w-full md:w-3/4">
                            <input wire:model="trainer.name" type="text" id="name" placeholder="{{ __('admin.trainers.create.name.placeholder') }}"
                                   class="w-full md:w-3/4 px-3 py-1 border rounded focus:border-blue-500 hover:border-blue-500 text-sm outline-none focus:input-shadow"/>
                            @error('trainer.name')
                            <p class="text-xs text-red-600">{{ $message }}</p>
                            @enderror
                        </div>
                    </div>
                    <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-center">
                        <div class="w-full md:w-1/4">
                            <label for="email" class="w-full text-left md:text-right block text-sm">{{ __('admin.trainers.create.email') }}</label>
                        </div>
                        <div class="w-full md:w-3/4">
                            <input wire:model="trainer.email" type="email" id="email" placeholder="{{ __('admin.trainers.create.email.placeholer') }}"
                                   class="w-full md:w-3/4 px-3 py-1 border rounded focus:border-blue-500 hover:border-blue-500 text-sm outline-none focus:input-shadow"/>
                            @error('trainer.email')
                            <p class="text-xs text-red-600">{{ $message }}</p>
                            @enderror
                        </div>
                    </div>

                    <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-center">
                        <div class="w-full md:w-1/4">
                            <label for="affiliate" class="w-full text-left md:text-right block text-sm">{{ __('admin.trainers.create.affiliate') }}</label>
                        </div>
                        <div class="w-full md:w-3/4">
                            <select wire:model="trainer.affiliate" id="affiliate"
                                    class="w-full md:w-3/4 px-3 py-1 border rounded focus:border-blue-500 hover:border-blue-500 text-sm outline-none focus:input-shadow">
                                <option value="null">{{ __('admin.trainers.create.chooseaffiliate') }}</option>
                                @foreach(\App\Models\Affiliates::all() as $key=>$item)
                                    <option value="{{$item->id}}">{{ $item->title }}</option>
                                @endforeach
                            </select>
                            @error('trainer.affiliate')
                            <p class="text-xs text-red-600">{{ $message }}</p>
                            @enderror
                        </div>
                    </div>

                    <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-center">
                        <div class="w-full md:w-1/4">
                        </div>
                        <div class="w-full md:w-3/4 space-x-2 flex">
                            <button wire:click="save"
                                    class="mb-2 text-sm flex justify-center items-center px-4 py-1 border-0 bg-blue-500 text-white rounded hover:bg-blue-600">
                                {{ __('admin.buttons.save') }}
                            </button>

                            <a href="{{ route('admin.trainers.index') }}"
                               class="py-1 px-4 border inline-block m-0 mb-2 text-sm rounded hover:border-black">{{ __('admin.buttons.cancel') }}</a>
                        </div>
                    </div>
            @endif
        </div>
    </div>
</div>
