<div>
    <div class="flex w-full border-b-2 border-[#F4F4F5] space-x-2 items-end mb-5">
        <div class="text-5xl font-bold text-black py-2.5 px-4">{{ __('trainer.groups.title') }}</div>
        <div class="flex">
            <a href="{{ route('trainer.groups.active') }}" class="text-lg text-[#2563EB] font-medium py-5 mx-5 hover:text-blue-900 @if (request()->routeIs('trainer.groups.active')) border-b-2 border-[#2563EB] @endif">{{ __('trainer.groups.title.active') }}</a>
            <a href="{{ route('trainer.groups.archive') }}" class="text-lg text-[#2563EB] font-medium py-5 mx-5 hover:text-blue-900 @if (request()->routeIs('trainer.groups.archive')) border-b-2 border-[#2563EB] @endif">{{ __('trainer.groups.title.archive') }}</a>
        </div>
    </div>
    <div class="w-full py-2.5 flex flex-col">
        <div class="w-full flex flex-col">
            <h2 class="w-full text-xl text-[#18181B] px-4 mb-4">{{ __('trainer.groups.groups') }}</h2>
            @if($groups->isEmpty())
                <div class="w-full h-28 rounded border border-gray-400 bg-gray-50 flex items-center justify-center my-4">
                    <div class="text-lg text-gray-700">{{ __('trainer.groups.groups.empty') }}</div>
                </div>
            @else
                <div class="flex flex-col">
                    <table class="table-fixed w-full">
                        <thead class="bg-[#F4F4F5] border-b-2 border-[#D4D4D8]">
                        <tr>
                            <th scope="col" class="w-full text-left font-normal text-[15px] text-[#18181B] py-2.5 px-4">{{ __('trainer.table.head.title.program') }}</th>
                            <th scope="col" class="w-48 font-normal text-[15px] text-[#18181B] py-2.5 px-4">{{ __('trainer.table.head.group') }}</th>
                            <th scope="col" class="w-40 font-normal text-[15px] text-[#18181B] py-2.5 px-4">{{ __('trainer.table.head.period') }}</th>
                            <th scope="col" class="w-40 font-normal text-[15px] text-[#18181B] py-2.5 px-4">{{ __('trainer.table.head.status') }}</th>
                            <th scope="col" class="w-40 font-normal text-[15px] text-[#18181B] py-2.5 px-4">{{ __('trainer.table.head.action') }}</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($groups as $group)
                            <tr class="bg-[#FFFFFF] hover:bg-[#F4F4F5] hover:cursor-pointer border-b-2 border-[#D4D4D8]" onclick="window.location='{{ route('trainer.groups.view', ['slug' => $group->slug]) }}'">
                                <td class="w-full text-left font-light text-[15px] text-[#3A8CF7] py-2.5 px-4">{{ $group->coursesItem->ru_title }}</td>
                                <td class="w-48 text-center text-[14px] text-[#18181B] py-2.5 px-4">{{ $group->slug }}</td>
                                <td class="w-40 text-center font-light text-[12px] text-[#939393] py-2.5 px-4"><div class="flex flex-col"><span>с {{ $group->start_time }}</span><span>по {{ $group->end_time }}</span></div></td>
                                <td class="w-40 text-center text-[14px] text-[#18181B] py-2.5 px-4">{{ \App\Models\Groups::listGroupStatuses()[$group->status] }}</td>
                                <td class="w-40 text-center font-light text-[15px] text-[#3A8CF7] py-2.5 px-4">{{ __('trainer.links.login') }}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            @endif
        </div>
    </div>
</div>
