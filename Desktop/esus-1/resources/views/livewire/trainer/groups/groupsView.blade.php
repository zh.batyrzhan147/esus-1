<div class="py-4">
    <div class="flex w-full justify-between items-end mb-5 space-x-1 px-4">
        <h2 class="text-2xl font-bold text-black">{{ $group->coursesItem->ru_title ?? $group->coursesItem->kz_title }}</h2>
        <div class="flex space-x-1">
            @if (auth()->user()->trainer_oauth_group !== $this->getOAuth2Group())
                <button
                    class="font-medium text-[#FFFFFF] px-8 py-2.5 rounded bg-[#3B82F6] hover:bg-[#2563EB] whitespace-nowrap"
                    wire:click="setOAuth2Group"
                >{{ __('Выбрать для СДО') }}</button>
            @else
                <button
                    class="flex font-medium text-[#FFFFFF] px-8 py-2.5 rounded bg-[#71717A] whitespace-nowrap space-x-2" disabled
                >
                    <span>{{ __('Выбрано для СДО') }}</span></button>
            @endif

            @if($group->status == \App\Models\Groups::GROUP_STATUS_PLANNED)
                <button wire:click="$emit('triggerChangeStatus',{{ $group->status }})"
                        class="font-medium text-[#FFFFFF] px-8 py-2.5 rounded bg-[#3B82F6] hover:bg-[#2563EB]">
                    {{ __('trainer.buttons.make') }}
                </button>
            @elseif($group->status == \App\Models\Groups::GROUP_STATUS_FORMED)
                <button
                    @if(strtotime($group->start_time) > strtotime('now'))
                        class="font-medium text-[#FFFFFF] px-8 py-2.5 rounded bg-[#71717A]" disabled
                    @else
                        class="font-medium text-[#FFFFFF] px-8 py-2.5 rounded bg-[#3B82F6] hover:bg-[#2563EB]"   wire:click="$emit('triggerChangeStatus',{{ $group->status }})"
                    @endif
                >{{ __('trainer.buttons.start') }}</button>
            @elseif($group->status == \App\Models\Groups::GROUP_STATUS_INPROGRESS)
                <a href="{{ route('trainer.groups.statements',['slug' => $group->slug]) }}" class="font-medium text-[#FFFFFF] px-8 py-2.5 rounded bg-[#3B82F6] hover:bg-[#2563EB] flex items-center">
                    {{ __('trainer.buttons.table') }}
                </a>
                <button wire:click="$emit('triggerChangeStatus',{{ $group->status }})"
                        class="font-medium text-[#FFFFFF] px-8 py-2.5 rounded bg-red-600 hover:bg-red-700">
                    @if($group->coursesItem->grading) {{ __('trainer.buttons.testing') }} @else {{ __('trainer.buttons.end') }} @endif
                </button>
            @elseif($group->status == \App\Models\Groups::GROUP_STATUS_FINISHED or $group->status == \App\Models\Groups::GROUP_STATUS_FINISHED_TESTING )
                <button wire:click="getAllStatements()" class="font-medium text-[#FFFFFF] px-8 py-2.5 rounded bg-[#3B82F6] hover:bg-[#2563EB]">
                    {{ __('trainer.buttons.table.download') }}
                </button>
                {{--                <button wire:click="$emit('triggerChangeStatus',{{ $group->status }})"--}}
                {{--                        class="font-medium text-[#FFFFFF] px-8 py-2.5 rounded bg-[#3B82F6] hover:bg-[#2563EB]">--}}
                {{--                    Рекомендовать на сертификат--}}
                {{--                </button>--}}
            @elseif($group->status == \App\Models\Groups::GROUP_STATUS_ARCHIVE)
                <button class="font-medium text-[#FFFFFF] px-8 py-2.5 rounded bg-[#71717A]" disabled>{{ __("trainer.buttons.archive") }}</button>
            @endif

        </div>
    </div>

    <div class="rounded w-full bg-[#FFFFFF] px-4 py-4 mb-5">
        <div class="w-full grid gap-[20px] grid-cols-2">
            <div class="w-full">
                <div class="text-[14px] text-[#3F3F46] font-medium m-0 p-0 mb-1">{{ __('trainer.groups.info.group') }}</div>
                <div class="text-[14px] text-[#000000] font-normal m-0 p-0">{{ $group->slug }}</div>
            </div>
            <div class="w-full">
                <div class="text-[14px] text-[#3F3F46] font-medium m-0 p-0 mb-1">{{ __('trainer.groups.info.city') }}</div>
                <div
                    class="text-[14px] text-[#000000] font-normal m-0 p-0 cursor-pointer">{{ $group->affiliateItem->title }}</div>
            </div>
            <div class="w-full">
                <div class="text-[14px] text-[#3F3F46] font-medium m-0 p-0 mb-1">{{ __('trainer.groups.info.status') }}</div>
                <div
                    class="text-[14px] text-[#000000] font-normal m-0 p-0">{{ \App\Models\Groups::listGroupStatuses()[$group->status] }}</div>
            </div>
            <div class="w-full">
                <div class="text-[14px] text-[#3F3F46] font-medium m-0 p-0 mb-1">{{ __('trainer.groups.info.period') }}</div>
                <div
                    class="text-[14px] text-[#000000] font-normal m-0 p-0">{{ date('d.m.Y', strtotime($group->start_time)) }}
                    - {{ date('d.m.Y', strtotime($group->end_time)) }}</div>
            </div>
        </div>
    </div>
    @if((!empty($group->coursesItem->sdo_link)) and ($group->languageItem->slug == 'kz') or (!empty($group->coursesItem->sdo_link_ru)) and ($group->languageItem->slug == 'ru') or (!empty($group->coursesItem->sdo_link_en)) and ($group->languageItem->slug == 'en'))
        <div class="w-full px-4 flex justify-between items-center mb-1">
            <h2 class="w-full text-xl text-[#18181B] mb-4">Ссылка на СДО</h2>
        </div>
        <div class="w-full px-4 flex justify-between items-center mb-1">
            @if($group->languageItem->slug == 'kz')
                <input disabled class="text-[15px] font-[300] bg-white w-full" id="sdolink"  value="{{ $group->coursesItem->sdo_link }}"/>
                <div class="flex-row flex space-x-1">
                    <a href="{{ $group->coursesItem->sdo_link }}" target="_blank"
                       class="py-2.5 px-4 rounded bg-blue-300 text-base whitespace-nowrap hover:bg-blue-500">
                        {{ __('trainer.groups.sdo.link.click') }}
                    </a>
                </div>
            @elseif($group->languageItem->slug == 'ru')
                <input disabled class="text-[15px] font-[300] bg-white w-full" id="sdolink"  value="{{ $group->coursesItem->sdo_link_ru }}"/>
                <div class="flex-row flex space-x-1">
                    <a href="{{ $group->coursesItem->sdo_link_ru }}" target="_blank"
                       class="py-2.5 px-4 rounded bg-blue-300 text-base whitespace-nowrap hover:bg-blue-500">
                        {{ __('trainer.groups.sdo.link.click') }}
                    </a>
                </div>
            @elseif($group->languageItem->slug == 'en')
                <input disabled class="text-[15px] font-[300] bg-white w-full" id="sdolink"  value="{{ $group->coursesItem->sdo_link_en }}"/>
                <div class="flex-row flex space-x-1">
                    <a href="{{ $group->coursesItem->sdo_link_en }}" target="_blank"
                       class="py-2.5 px-4 rounded bg-blue-300 text-base whitespace-nowrap hover:bg-blue-500">
                        {{ __('trainer.groups.sdo.link.click') }}
                    </a>
                </div>
            @endif
        </div>
    @endif
    @if($group->status == \App\Models\Groups::GROUP_STATUS_PLANNED)
        <div class="w-full py-2.5 flex flex-col">
            <div class="w-full flex flex-col mb-5">
                <div class="w-full px-4 flex justify-between items-center mb-1">
                    <h2 class="w-full text-xl text-[#18181B] mb-4">Ссылка на вступление в группу</h2>
                </div>
                <div class="w-full px-4 flex justify-between items-center mb-1">
                    <input disabled class="text-[15px] font-[300] bg-white w-full" id="copyLink"
                           value="{{ $group->joinLink }}"/>
                    <div>
                        <button onclick="copyToClipboard()"
                                class="py-2.5 px-4 rounded bg-green-300 text-base whitespace-nowrap hover:bg-green-500">
                            {{ __('trainer.groups.planned.joinlink.copy') }}
                        </button>
                        <script>
                            function copyToClipboard() {
                                /* Get the text field */
                                let copyText = document.getElementById("copyLink");

                                /* Select the text field */
                                copyText.select();
                                copyText.setSelectionRange(0, 99999); /* For mobile devices */

                                /* Copy the text inside the text field */
                                navigator.clipboard.writeText(copyText.value);
                                Swal.fire({
                                    title: '{{ __("admin.swal.grants.title.copied") }}',
                                    icon: 'success',
                                    width: 600,
                                    position: 'top-end',
                                    showConfirmButton: false,
                                    timer: 1000
                                });

                            }
                        </script>
                    </div>
                </div>
                <div class="w-full px-4 flex justify-between items-center mb-1">
                    <h2 class="w-full text-xl text-[#18181B] mb-4">{{ __('trainer.groups.planned.requests') }}</h2>
                </div>
                @if($group->users_sb->isEmpty())
                    <div
                        class="w-full h-28 rounded border border-gray-400 bg-gray-50 flex items-center justify-center my-4">
                        <div class="text-lg text-gray-700">{{ __('trainer.groups.planned.nolisteners') }}</div>
                    </div>
                @else
                    <div class="flex flex-col">
                        <table class="table-fixed w-full">
                            <thead class="bg-[#F4F4F5] border-b-2 border-[#D4D4D8]">
                            <tr>
                                <th scope="col"
                                    class="w-full text-left font-normal text-[15px] text-[#18181B] py-2.5 w-1/12">
                                    <div class="text-center">{{ __('№') }}</div>
                                </th>
                                <th scope="col"
                                    class="w-full text-left font-normal text-[15px] text-[#18181B] py-2.5 px-4">
                                    {{ __('trainer.table.head.fio') }}
                                </th>
                                <th scope="col" class="w-64 font-normal text-[15px] text-[#18181B] py-2.5 px-4">{{ __('admin.table.head.phone') }}
                                </th>
                                <th scope="col" class="w-64 font-normal text-[15px] text-[#18181B] py-2.5 px-4">{{ __('trainer.table.head.status') }}
                                </th>
                                <th scope="col" class="w-40 font-normal text-[15px] text-[#18181B] py-2.5 px-4">{{ __('trainer.table.head.action') }}
                                </th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($group->users_sb as $user)
                                <tr wire:click="showUserModal({{ $user->id }},0)" class="bg-[#FFFFFF] hover:bg-[#F4F4F5] hover:cursor-pointer border-b-2 border-[#D4D4D8]">
                                    <td class="w-full text-left font-light text-[#3A8CF7] py-2.5 px-4">{{ $loop->index+1 }}</td>
                                    <td class="w-full text-left font-light text-[#3A8CF7] py-2.5 px-4">{{ $user->last_name ?? '' }} {{ $user->name ?? '' }} {{ $user->patronymic ?? '' }}</td>
                                    <td class="w-64 text-left text-[#18181B] py-2.5 px-4 text-center">{{ $user->phone }}</td>
                                    <td class="w-64 text-left text-[#18181B] py-2.5 px-4 text-center">{{ \App\Models\Groups::listUserStatuses()[$user->pivot->status] }}</td>
                                    <td class="w-40 font-light text-[#3A8CF7] py-2.5 px-4 text-center hover:text-blue-900">
                                        {{ __('trainer.links.show') }}
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                @endif
            </div>
        </div>
    @endif
    @if (($group->status == \App\Models\Groups::GROUP_STATUS_PLANNED) or ($group->status == \App\Models\Groups::GROUP_STATUS_FORMED))
        <div class="w-full py-2.5 flex flex-col">
            <div class="w-full flex flex-col mb-5">
                <div class="w-full px-4 flex justify-between items-center mb-1">
                    <h2 class="w-full text-xl text-[#18181B] mb-4">{{ __('trainer.groups.listeners') }} {{ $group->users()->count() }}/{{$group->count_all}}</h2>
                </div>

                @if($group->users->isEmpty())
                    <div
                        class="w-full h-28 rounded border border-gray-400 bg-gray-50 flex items-center justify-center my-4">
                        <div class="text-lg text-gray-700">{{ __('trainer.groups.listeners.nolisteners') }}</div>
                    </div>
                @else
                    <div class="flex flex-col">
                        <table class="table-fixed w-full">
                            <thead class="bg-[#F4F4F5] border-b-2 border-[#D4D4D8]">
                            <tr>
                                <th scope="col"
                                    class="w-full text-left font-normal text-[15px] text-[#18181B] py-2.5 w-1/12">
                                    <div class="text-center">{{ __('№') }}</div>
                                </th>
                                <th scope="col"
                                    class="w-full text-left font-normal text-[15px] text-[#18181B] py-2.5 px-4">
                                    {{ __('trainer.table.head.fio') }}
                                </th>
                                <th scope="col" class="w-64 font-normal text-[15px] text-[#18181B] py-2.5 px-4">{{ __('admin.table.head.phone') }}
                                </th>
                                <th scope="col" class="w-64 font-normal text-[15px] text-[#18181B] py-2.5 px-4">{{ __('trainer.table.head.status') }}
                                </th>
                                <th scope="col" class="w-40 font-normal text-[15px] text-[#18181B] py-2.5 px-4">{{ __('trainer.table.head.action') }}
                                </th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($group->users as $user)
                                <tr wire:click="showUserModal({{ $user->id }},1)" class="bg-[#FFFFFF] hover:bg-[#F4F4F5] hover:cursor-pointer border-b-2 border-[#D4D4D8]">
                                    <td class="w-full text-left font-light text-[#3A8CF7] py-2.5 px-4">{{ $loop->index+1 }}</td>
                                    <td class="w-full text-left font-light text-[#3A8CF7] py-2.5 px-4">{{ $user->last_name ?? '' }} {{ $user->name ?? '' }} {{ $user->patronymic ?? '' }}</td>
                                    <td class="w-64 text-left text-[#18181B] py-2.5 px-4 text-center">{{ $user->phone }}</td>
                                    <td class="w-64 text-left text-[#18181B] py-2.5 px-4 text-center">{{ __('trainer.table.body.status.accepted') }}</td>
                                    <td
                                        class="w-40 font-light text-[#3A8CF7] py-2.5 px-4 text-center hover:text-blue-900">
                                        {{ __('trainer.links.show') }}
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                @endif
            </div>
        </div>
    @endif

    @if ($group->status == \App\Models\Groups::GROUP_STATUS_INPROGRESS)
        <div class="w-full py-2.5 flex flex-col">
            <div class="w-full flex flex-col mb-5">
                @if($group->coursesItem->type == \App\Models\Courses::COURSES_ONLINE or $group->coursesItem->type == \App\Models\Courses::COURSES_MIXED)
                    <div class="w-full px-4 flex justify-between items-center mb-1">
                        <h2 class="w-full text-xl text-[#18181B] mb-4">Ссылка на видеоконференцию</h2>
                    </div>
                    <div class="w-full px-4 flex justify-between items-center mb-1">
                        <input disabled class="text-[15px] font-[300] bg-white w-full" id="zoomlink"
                               value="{{ $group->zoom_link }}"/>
                        <div>
                            <div class="flex-row flex space-x-1">
                                <button wire:click="editZoom"
                                        class="py-2.5 px-4 rounded bg-blue-300 text-base whitespace-nowrap hover:bg-blue-500">
                                    Изменить
                                </button>
                                <button onclick="copyToClipboard()"
                                        class="py-2.5 px-4 rounded bg-green-300 text-base whitespace-nowrap hover:bg-green-500">
                                    {{ __('trainer.groups.planned.joinlink.copy') }}
                                </button>
                            </div>
                            <script>
                                function copyToClipboard() {
                                    /* Get the text field */
                                    let copyText = document.getElementById("zoomlink");

                                    /* Select the text field */
                                    copyText.select();
                                    copyText.setSelectionRange(0, 99999); /* For mobile devices */

                                    /* Copy the text inside the text field */
                                    navigator.clipboard.writeText(copyText.value);
                                    Swal.fire({
                                        title: '{{ __("admin.swal.grants.title.copied") }}',
                                        icon: 'success',
                                        width: 600,
                                        position: 'top-end',
                                        showConfirmButton: false,
                                        timer: 1000
                                    });

                                }
                            </script>

                            @if($zoompopup)
                                <x-modal>
                                    <x-slot name="header">
                                        Изменить ссылку на видеоконференцию
                                        <br><br>
                                    </x-slot>
                                    <div class="flex flex-col items-start md:items-center w-full md:w-3/4 mx-auto space-y-4">
                                        <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-center">
                                            <div class="w-full md:w-1/4">
                                                <label for="zoom_link" class="w-full text-left md:text-right block text-sm">Ссылка: </label>
                                            </div>
                                            <div class="w-full md:w-3/4">
                                                <input wire:model.lazy="zoomLink" type="text" id="zoom_link" placeholder="{{ __('Вставьте ссылку на видеоконференцию') }}"
                                                       class="w-full md:w-3/4 px-3 py-1 border rounded focus:border-blue-500 hover:border-blue-500 text-sm outline-none focus:input-shadow"/>
                                                @error('zoomLink')
                                                <p class="text-xs text-red-600">{{ $message }}</p>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-center">
                                            <div class="w-full md:w-1/4">
                                            </div>
                                            <div class="w-full md:w-3/4 space-x-2 flex">
                                                <button type="submit" wire:click="saveZoomLink"
                                                        class="mb-2 text-sm flex justify-center items-center px-4 py-1 border-0 bg-blue-500 text-white rounded hover:bg-blue-600">
                                                    {{ __('admin.buttons.save') }}
                                                </button>
                                                <button wire:click="removeZoomLink"
                                                        class="mb-2 text-sm flex justify-center items-center px-4 py-1 border-0 bg-red-500 text-white rounded hover:bg-red-600">{{ __('admin.buttons.delete') }}
                                                </button>
                                                <button wire:click="$set('zoompopup',false)"
                                                        class="py-1 px-4 border inline-block m-0 mb-2 text-sm rounded hover:border-black">{{ __('admin.buttons.cancel') }}
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </x-modal>
                            @endif
                        </div>
                    </div>
                @endif
                <div class="w-full px-4 flex justify-between items-center mb-1">
                    <h2 class="w-full text-xl text-[#18181B] mb-4">{{ __('trainer.groups.tables.todate', ['date' => date('d.m.Y')]) }}</h2>
                </div>
                <div class="flex flex-col">
                    <table class="table-fixed w-full">
                        <thead class="bg-[#F4F4F5] border-b-2 border-[#D4D4D8]">
                        <tr>
                            <th scope="col" class="w-full text-left font-normal text-[15px] text-[#18181B] py-2.5 px-4">
                                {{ __('trainer.table.head.fio') }}
                            </th>
                            <th scope="col" class="w-96 font-normal text-[15px] text-[#18181B] py-2.5 px-4 text-center">{{ __('trainer.table.head.action') }}
                            </th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr class="bg-[#FFFFFF] hover:bg-[#F4F4F5] hover:cursor-pointer border-b-2 border-[#D4D4D8]">
                            <td class="w-full text-left font-light text-[#3A8CF7] py-2.5 px-4"></td>
                            <td class="w-96 font-light text-[#3A8CF7] py-2.5 px-4 text-center hover:text-blue-900">
                                <div class="flex space-x-1 justify-center">
                                    <button
                                        wire:click="$emit('triggerChangeAllStatus', {{ \App\Models\Statements::STATEMENTS_ATTEND }})"
                                        class="mb-2 text-sm flex justify-center items-center space-x-1.5 px-4 py-1 border-0 text-white rounded bg-green-500 hover:bg-green-400">
                                        {{ __('trainer.groups.tables.visitedAll') }}
                                    </button>
                                    <button
                                        wire:click="$emit('triggerChangeAllStatus', {{ \App\Models\Statements::STATEMENTS_ONLINE }})"
                                        class="mb-2 text-sm flex justify-center items-center space-x-1.5 px-4 py-1 border-0 text-white rounded bg-blue-500 hover:bg-blue-400">
                                        {{ __('trainer.groups.tables.remoteAll') }}
                                    </button>
                                    <button
                                        wire:click="$emit('triggerChangeAllStatus', {{ \App\Models\Statements::STATEMENTS_MISSING }})"
                                        class="mb-2 text-sm flex justify-center items-center space-x-1.5 px-4 py-1 border-0 text-white rounded bg-red-500 hover:bg-red-400">
                                        {{ __('trainer.groups.tables.notvisitedAll') }}
                                    </button>
                                </div>
                            </td>
                        </tr>
                        @foreach($group->users as $user)
                            <tr class="bg-[#FFFFFF] hover:bg-[#F4F4F5] hover:cursor-pointer border-b-2 border-[#D4D4D8]">
                                <td  wire:click="showUserModal({{ $user->id }},0)" class="w-full text-left font-light text-[#3A8CF7] py-2.5 px-4">{{ $user->last_name ?? '' }} {{ $user->name ?? '' }} {{ $user->patronymic ?? '' }}</td>
                                <td class="w-96 font-light text-[#3A8CF7] py-2.5 px-4 text-center hover:text-blue-900">
                                    <div class="flex space-x-1 justify-center">
                                        <button
                                            wire:click="$emit('setStatementStatus',{{ $group->id }}, {{ $user->id }}, {{ \App\Models\Statements::STATEMENTS_ATTEND }})"
                                            class="mb-2 text-sm flex justify-center items-center space-x-1.5 px-4 py-1 border-0 text-white rounded @if(($statements[$user->id] ?? '') == \App\Models\Statements::STATEMENTS_ATTEND) bg-green-500 @else bg-gray-500 hover:bg-green-500 @endif">
                                            {{ __('trainer.groups.tables.visited') }}
                                        </button>
                                        <button
                                            wire:click="$emit('setStatementStatus',{{ $group->id }}, {{ $user->id }}, {{ \App\Models\Statements::STATEMENTS_ONLINE }})"
                                            class="mb-2 text-sm flex justify-center items-center space-x-1.5 px-4 py-1 border-0 text-white rounded @if(($statements[$user->id] ?? '') == \App\Models\Statements::STATEMENTS_ONLINE) bg-blue-500 @else bg-gray-500 hover:bg-blue-500 @endif">
                                            {{ __('trainer.groups.tables.remote') }}
                                        </button>
                                        <button
                                            @if (($statements[$user->id] ?? '') != \App\Models\Statements::STATEMENTS_SICK)) wire:click="$emit('setStatementStatus',{{ $group->id }}, {{ $user->id }}, {{ \App\Models\Statements::STATEMENTS_MISSING }})" @endif
                                        class="mb-2 text-sm flex justify-center items-center space-x-1.5 px-4 py-1 border-0 text-white rounded @if((($statements[$user->id] ?? '') == \App\Models\Statements::STATEMENTS_MISSING) or (($statements[$user->id] ?? '') == \App\Models\Statements::STATEMENTS_MISSING_SENT) or (($statements[$user->id] ?? '') == \App\Models\Statements::STATEMENTS_MISSING_DECLINE)) bg-red-500 @elseif (($statements[$user->id] ?? '') == \App\Models\Statements::STATEMENTS_SICK)) bg-teal-400 @else  bg-gray-500 hover:bg-red-500 @endif">
                                            {{ __('trainer.groups.tables.notvisited') }}
                                        </button>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    @endif

    @if ($group->status == \App\Models\Groups::GROUP_STATUS_FINISHED or $group->status == \App\Models\Groups::GROUP_STATUS_FINISHED_TESTING)
        <div class="w-full py-2.5 flex flex-col">
            <div class="w-full flex flex-col mb-5">
                <div class="w-full px-4 flex justify-between items-center mb-1">
                    <h2 class="w-full text-xl text-[#18181B] mb-4">{{ __('trainer.groups.list.title') }}</h2>
                </div>
                <div class="flex flex-col">
                    <table class="table-fixed w-full">
                        <thead class="bg-[#F4F4F5] border-b-2 border-[#D4D4D8]">
                        <tr>
                            <th scope="col" class="w-full text-left font-normal text-[15px] text-[#18181B] py-2.5 px-4">
                                {{ __('trainer.table.head.fio') }}
                            </th>
                            <th scope="col" class="w-96 font-normal text-[15px] text-[#18181B] py-2.5 px-4 text-center">{{ __('trainer.table.head.action') }}
                            </th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($group->users as $user)
                            <tr class="bg-[#FFFFFF] hover:bg-[#F4F4F5] hover:cursor-pointer border-b-2 border-[#D4D4D8]">
                                <td class="w-full text-left font-light text-[#3A8CF7] py-2.5 px-4">{{ $user->last_name ?? '' }} {{ $user->name ?? '' }} {{ $user->patronymic ?? '' }}</td>
                                <td class="w-96 font-light text-[#3A8CF7] py-2.5 px-4 text-center hover:text-blue-900">
                                    <div class="flex space-x-1 justify-center">
                                        <label for="default-toggle{{$user->id}}" class="inline-flex relative items-center cursor-pointer" wire:click="recommendToCert({{ $user->id }})">
                                            <input type="checkbox" value="" id="default-toggle{{$user->id}}" class="sr-only peer" @if(($statements[$user->id] ?? '') == \App\Models\Groups::GROUPS_USER_CERT) checked @endif>
                                            <div class="w-11 h-6 bg-gray-200 peer-focus:outline-none peer-focus:ring-0 peer-focus:ring-green-300 dark:peer-focus:ring-green-800 rounded-full peer peer-checked:after:translate-x-full peer-checked:after:border-white after:content-[''] after:absolute after:top-[2px] after:left-[2px] after:bg-white after:border-gray-300 after:border after:rounded-full after:h-5 after:w-5 after:transition-all dark:border-gray-600 peer-checked:bg-green-600"></div>
                                            <span class="ml-3 text-sm font-medium text-gray-900 dark:text-gray-300">@if ($group->coursesItem->grading)
                                                    {{ __('trainer.links.testing') }}
                                                @else
                                                    {{ __('trainer.links.certificete') }}
                                                @endif</span>
                                        </label>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    @endif

    @if ($group->status != \App\Models\Groups::GROUP_STATUS_FINISHED and $group->status != \App\Models\Groups::GROUP_STATUS_FINISHED_TESTING)
        <div class="w-full py-2.5 flex flex-col">
            <div class="w-full flex flex-col mb-5">
                <div class="w-full px-4 flex justify-between items-center mb-1">
                    <h2 class="w-full text-xl text-[#18181B] mb-4">{{ __('trainer.groups.docs') }}</h2>
                </div>
                @if($group->coursesItem->file_1_listener or $group->coursesItem->file_2_listener or $group->coursesItem->file_1_teacher or $group->coursesItem->file_2_teacher)
                    <div class="flex flex-col">
                        <table class="table-fixed w-full">
                            <thead class="bg-[#F4F4F5] border-b-2 border-[#D4D4D8]">
                            <tr>
                                <th scope="col" class="w-full text-left font-normal text-[15px] text-[#18181B] py-2.5 px-4">
                                    {{ __('trainer.table.head.title') }}
                                </th>
                                <th scope="col" class="w-40 font-normal text-[15px] text-[#18181B] py-2.5 px-4">{{ __('trainer.table.head.format') }}</th>
                                <th scope="col" class="w-40 font-normal text-[15px] text-[#18181B] py-2.5 px-4">{{ __('trainer.table.head.action') }}
                                </th>
                            </tr>
                            </thead>
                            <tbody>
                            @if($group->coursesItem->file_1_teacher)
                                <tr class="bg-[#FFFFFF] hover:bg-[#F4F4F5] hover:cursor-pointer border-b-2 border-[#D4D4D8]">
                                    <td class="w-full text-left font-light text-[#3A8CF7] py-2.5 px-4">Файл</td>
                                    <td class="w-40 text-left text-[#18181B] py-2.5 px-4 text-center">{{ pathinfo($group->coursesItem->file_1_teacher, PATHINFO_EXTENSION) }}</td>
                                    <td class="w-40 font-light text-[#3A8CF7] py-2.5 px-4 text-center hover:text-blue-900">
                                        <a href="{{ $group->coursesItem->getFileUrl($group->coursesItem->file_1_teacher) }}" download="{{ __('admin.courses.create.form.file.kzteacher') }}.{{ pathinfo($group->coursesItem->file_1_teacher, PATHINFO_EXTENSION)}}">{{ __('trainer.links.download') }}</a></td>
                                </tr>
                            @endif

                            @if($group->coursesItem->file_2_teacher)
                                <tr class="bg-[#FFFFFF] hover:bg-[#F4F4F5] hover:cursor-pointer border-b-2 border-[#D4D4D8]">
                                    <td class="w-full text-left font-light text-[#3A8CF7] py-2.5 px-4">Файл</td>
                                    <td class="w-40 text-left text-[#18181B] py-2.5 px-4 text-center">{{ pathinfo($group->coursesItem->file_2_teacher, PATHINFO_EXTENSION) }}</td>
                                    <td class="w-40 font-light text-[#3A8CF7] py-2.5 px-4 text-center hover:text-blue-900">
                                        <a  href="{{ $group->coursesItem->getFileUrl($group->coursesItem->file_2_teacher) }}" download="{{ __('admin.courses.create.form.file.ruteacher') }}.{{ pathinfo($group->coursesItem->file_2_teacher, PATHINFO_EXTENSION)}}">{{ __('trainer.links.download') }}</a></td>
                                </tr>
                            @endif
                            </tbody>
                        </table>
                    </div>
                @else
                    <div
                        class="w-full h-28 rounded border border-gray-400 bg-gray-50 flex items-center justify-center my-4">
                        <div class="text-lg text-gray-700">{{ __('trainer.groups.docs.file.notfound') }}</div>
                    </div>
                @endif
            </div>

        </div>
    @endif

    @if($showUserDetails)
        <div>
            <div x-data="{show: true}" x-show="show"
                 class="min-w-screen h-screen animated fadeIn faster fixed left-0 top-0 flex justify-center items-center inset-0 z-50 outline-none focus:outline-none bg-no-repeat bg-center bg-cover">
                <div class="absolute bg-black opacity-80 inset-0 z-0"></div>
                <div class="w-full max-w-[840px] p-5 relative mx-auto my-auto rounded-xl shadow-lg bg-white">
                    <div class="flex justify-between pb-10">
                        <div class="text-[24px] font-bold">{{ __('trainer.groups.modal.title') }}</div>
                        <button wire:click="$set('showUserDetails',false)">
                            <svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="times"
                                 class="svg-inline--fa fa-times fa-w-11 w-4 h-4" role="img"
                                 xmlns="http://www.w3.org/2000/svg" viewBox="0 0 352 512">
                                <path fill="currentColor"
                                      d="M242.72 256l100.07-100.07c12.28-12.28 12.28-32.19 0-44.48l-22.24-22.24c-12.28-12.28-32.19-12.28-44.48 0L176 189.28 75.93 89.21c-12.28-12.28-32.19-12.28-44.48 0L9.21 111.45c-12.28 12.28-12.28 32.19 0 44.48L109.28 256 9.21 356.07c-12.28 12.28-12.28 32.19 0 44.48l22.24 22.24c12.28 12.28 32.2 12.28 44.48 0L176 322.72l100.07 100.07c12.28 12.28 32.2 12.28 44.48 0l22.24-22.24c12.28-12.28 12.28-32.19 0-44.48L242.72 256z"></path>
                            </svg>
                        </button>
                    </div>
                    @if($showmessageform)
                        <div class="py-2 h-full">
                            <div class="rounded w-full bg-[#FFFFFF] px-2">
                                @if($group->status == \App\Models\Groups::GROUP_STATUS_FORMED)
                                    <h2 >Опишите причину исключения:</h2>
                                @else
                                    <h2 >Опишите детали отказа:</h2>
                                @endif
                            </div>
                            <div class="rounded w-full bg-[#FFFFFF]">
                                <textarea wire:model.defer="message" rows="3" class="w-full px-3 py-1 border rounded focus:border-blue-500 hover:border-blue-500 text-sm outline-none focus:input-shadow"></textarea>
                                @error('message')
                                <p class="text-xs text-red-600">{{ $message }}</p>
                                @enderror
                            </div>
                            @if($group->status != \App\Models\Groups::GROUP_STATUS_INPROGRESS)
                                <button class="font-medium text-[#FFFFFF] px-8 py-2.5 rounded bg-red-500 hover:bg-red-600" wire:click="rework({{ $userModal->id }})">{{ __('trainer.buttons.reject') }}</button>
                                <button class="border inline-block px-8 py-2.5 text-sm rounded hover:border-black" wire:click="$set('showmessageform',false)">{{ __('trainer.buttons.cancel') }}</button>
                            @else
                                <button class="font-medium text-[#FFFFFF] px-8 py-2.5 rounded bg-red-500 hover:bg-red-600" wire:click="excludeUser({{ $userModal->id }})">{{ __('trainer.buttons.exclude') }}</button>
                                <button class="border inline-block px-8 py-2.5 text-sm rounded hover:border-black" wire:click="$set('showmessageform',false)">{{ __('trainer.buttons.cancel') }}</button>
                            @endif

                        </div>
                    @endif
                    <div class="py-6 px-4 sm:px-6 lg:px-8 h-full">
                        @if(!$showmessageform)
                            <div class="flex w-full justify-between items-center mb-5 space-x-1 px-4">
                                <h2 class="text-2xl font-bold text-black text-center">{{ $userModal->fio }}</h2>
                                @if ($userStatus != \App\Models\Groups::GROUPS_USER_PAYMENT && ($group->status == \App\Models\Groups::GROUP_STATUS_PLANNED or $group->status == \App\Models\Groups::GROUP_STATUS_FORMED))
                                    <div class="flex space-x-2">
                                        <button class="font-medium text-[#FFFFFF] px-8 py-2.5 rounded bg-red-500 hover:bg-red-600" wire:click="showmessage">{{ __('trainer.buttons.reject') }}</button>
                                        @if (!$typeModal)
                                            <button class="font-medium text-[#FFFFFF] px-8 py-2.5 rounded bg-[#3B82F6] hover:bg-[#2563EB]" wire:click="accept({{ $userModal->id }})">{{ __('trainer.buttons.accept') }}</button>
                                        @endif
                                        @if(in_array($userStatus, [\App\Models\Groups::GROUPS_USER_REWORK, \App\Models\Groups::GROUPS_USER_REQUEST]))
                                            <div class="flex space-x-2">
                                                <button class="font-medium px-8 py-2.5 rounded bg-yellow-100 hover:bg-yellow-100 text-black" wire:click="payment({{ $userModal->id }})">{{ __('trainer.buttons.payment') }}</button>
                                            </div>
                                        @endif
                                    </div>
                                @endif
                                @if($group->status == \App\Models\Groups::GROUP_STATUS_INPROGRESS)
                                    <div class="flex space-x-2">
                                        <button class="font-medium text-[#FFFFFF] px-8 py-2.5 rounded bg-red-500 hover:bg-red-600" wire:click="showmessage">{{ __('trainer.buttons.exclude') }}</button>
                                    </div>
                                @endif
                            </div>
                        @endif
                        @if($userStatus == \App\Models\Groups::GROUPS_USER_PAYMENT)
                            <div class="bg-blue-100 border border-blue-400 text-blue-700 px-4 py-3 rounded relative" role="alert">
                                <strong class="font-bold">Заявка в ожидании оплаты</strong>
                            </div>
                        @endif
                        <div class="rounded w-full bg-[#FFFFFF] px-4 py-4 mb-5">
                            <div class="w-full mb-5">
                                <div class="text-[14px] text-[#3F3F46] font-medium m-0 p-0 mb-1">{{ __('listener.profile.account.iin') }}</div>
                                <div class="text-[14px] text-[#000000] font-normal m-0 p-0">{{ $userModal->iin ?? __('listener.profile.account.noinfo') }}</div>
                            </div>
                            <div class="w-full grid gap-x-[20px] gap-y-5 grid-cols-2">
                                <div class="w-full">
                                    <div class="text-[14px] text-[#3F3F46] font-medium m-0 p-0 mb-1">{{ __('listener.profile.account.lastname') }}</div>
                                    <div class="text-[14px] text-[#000000] font-normal m-0 p-0">{{ $userModal->last_name ?? __('listener.profile.account.noinfo') }}</div>
                                </div>
                                <div class="w-full">
                                    <div class="text-[14px] text-[#3F3F46] font-medium m-0 p-0 mb-1">{{ __('listener.profile.account.region') }}</div>
                                    {{--                                    <div class="text-[14px] text-[#000000] font-normal m-0 p-0">{{ $userModal->oblast ?? __('listener.profile.account.noinfo') }}, {{ $userModal->raion }}, {{ $userModal->gorod }}</div>--}}
                                    @if($userModal->oblastItem or $userModal->raionItem or $userModal->gorodItem)
                                        @if($userModal->oblastItem)<div class="text-[14px] text-[#000000] font-normal m-0 p-0">Область: {{ $userModal->oblastItem->ru_title ?? 'Не указано' }}</div>@endif
                                        @if($userModal->raionItem)<div class="text-[14px] text-[#000000] font-normal m-0 p-0">Район: {{ $userModal->raionItem->ru_title ?? 'Не указано' }}</div>@endif
                                        @if($userModal->gorodItem)<div class="text-[14px] text-[#000000] font-normal m-0 p-0">Населенный пункт: {{ $userModal->gorodItem->ru_title ?? 'Не указано' }}</div>@endif
                                    @else
                                        <div class="text-[14px] text-[#000000] font-normal m-0 p-0">Не указано</div>
                                    @endif
                                </div>
                                <div class="w-full">
                                    <div class="text-[14px] text-[#3F3F46] font-medium m-0 p-0 mb-1">{{ __('listener.profile.account.name') }}</div>
                                    <div class="text-[14px] text-[#000000] font-normal m-0 p-0">{{ $userModal->name ?? __('listener.profile.account.noinfo') }}</div>
                                </div>
                                <div class="w-full">
                                    <div class="text-[14px] text-[#3F3F46] font-medium m-0 p-0 mb-1">{{ __('listener.profile.account.org') }}</div>
                                    <div class="text-[14px] text-[#000000] font-normal m-0 p-0">{{ $userModal->instituteItem->title ?? __('listener.profile.account.noinfo') }}</div>
                                </div>
                                <div class="w-full">
                                    <div class="text-[14px] text-[#3F3F46] font-medium m-0 p-0 mb-1">{{ __('listener.profile.account.patronymic') }}</div>
                                    <div class="text-[14px] text-[#000000] font-normal m-0 p-0">{{ $userModal->patronymic ?? __('listener.profile.account.noinfo') }}</div>
                                </div>
                                <div class="w-full">
                                    <div class="text-[14px] text-[#3F3F46] font-medium m-0 p-0 mb-1">{{ __('listener.profile.account.position') }}</div>
                                    <div class="text-[14px] text-[#000000] font-normal m-0 p-0">{{ $userModal->jobnameItem->title ?? __('listener.profile.account.noinfo') }}</div>
                                </div>
                                <div class="w-full">
                                    <div class="text-[14px] text-[#3F3F46] font-medium m-0 p-0 mb-1">{{ __('listener.profile.account.email') }}</div>
                                    <div class="text-[14px] text-[#000000] font-normal m-0 p-0">{{ $userModal->email ?? __('listener.profile.account.noinfo') }}</div>
                                </div>
                                <div class="w-full">
                                    <div class="text-[14px] text-[#3F3F46] font-medium m-0 p-0 mb-1">{{ __('listener.profile.security.phone') }}</div>
                                    <div class="text-[14px] text-[#000000] font-normal m-0 p-0">{{ $userModal->phone ?? __('listener.profile.account.noinfo') }}</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endif
</div>
@push('scripts')
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>

    <script type="text/javascript">
        document.addEventListener('DOMContentLoaded', function () {
            @this.on('triggerChangeStatus', itemID => {
                Swal.fire({
                    title: '{{ __("admin.swal.temp.title") }}',
                    text: '',
                    icon: "warning",
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#aaa',
                    cancelButtonText: '{{ __("admin.swal.temp.buttons.cancel") }}',
                    confirmButtonText: '{{ __("admin.swal.temp.buttons.approve") }}'
                }).then((result) => {
                    //if user clicks on delete
                    if (result.value) {
                        // calling destroy method to delete
                        @this.call('changeGroupStatus', itemID);
                        // success response
                        Swal.fire({
                            title: '{{ __("admin.swal.grants.title.success2") }}',
                            icon: 'success',
                            width: 600,
                            position: 'top-end',
                            showConfirmButton: false,
                            timer: 1000
                        });
                    } else {
                        Swal.fire({
                            title: '{{ __("admin.swal.temp.discarded.title") }}',
                            icon: 'success',
                            width: 600,
                            position: 'top-end',
                            showConfirmButton: false,
                            timer: 1000
                        });
                    }
                });
            });


            @this.on('showMessage', message => {
                Swal.fire({
                    title: message,
                    icon: 'warning',
                    width: 600,
                    position: 'top-end',
                    showConfirmButton: false,
                    timer: 3000
                });
            });


            @this.on('triggerChangeAllStatus', itemID => {
                Swal.fire({
                    title: '{{ __("admin.swal.temp.title") }}',
                    text: '{{ __("admin.swal.changeAllStatement") }}',
                    icon: "warning",
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#aaa',
                    cancelButtonText: '{{ __("admin.swal.temp.buttons.cancel") }}',
                    confirmButtonText: '{{ __("admin.swal.temp.buttons.approve") }}'
                }).then((result) => {
                    //if user clicks on delete
                    if (result.value) {
                        // calling destroy method to delete
                        @this.call('setStatementAllStatus', itemID);
                        // success response
                        Swal.fire({
                            title: '{{ __("admin.swal.grants.title.success2") }}',
                            icon: 'success',
                            width: 600,
                            position: 'top-end',
                            showConfirmButton: false,
                            timer: 1000
                        });
                    } else {
                        Swal.fire({
                            title: '{{ __("admin.swal.temp.discarded.title") }}',
                            icon: 'success',
                            width: 600,
                            position: 'top-end',
                            showConfirmButton: false,
                            timer: 1000
                        });
                    }
                });
            });

        })
    </script>

@endpush
