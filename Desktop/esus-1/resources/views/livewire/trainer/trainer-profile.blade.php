<div>
    <div class="flex w-full border-b-2 border-[#F4F4F5] space-x-2 items-end mb-5">
        <div class="text-5xl font-bold text-black py-2.5 px-4">{{ __('trainer.groups.title') }}</div>
        <div class="flex">
            <a href="{{ route('trainer.profile') }}" class="text-lg text-[#2563EB] font-medium py-5 mx-5 hover:text-blue-900 @if (request()->routeIs('trainer.profile')) border-b-2 border-[#2563EB] @endif">{{ __('trainer.groups.title.active') }}</a>
            <a href="{{ route('user.groups.archive') }}" class="text-lg text-[#2563EB] font-medium py-5 mx-5 hover:text-blue-900 @if (request()->routeIs('user.groups.archive')) border-b-2 border-[#2563EB] @endif">{{ __('trainer.groups.title.archive') }}</a>
        </div>
    </div>
    <div class="flex flex-col mx-auto py-2 px-4 sm:px-6 lg:px-8">
        <div class="w-full flex flex-col space-y-4">
            <div class="w-full">
                <span class="text-[20px] font-[300]">{{ __('trainer.groups.groups') }}</span>
            </div>
            <div class="w-full flex flex-col">
                <div class="w-full flex justify-end space-x-4">
                    <div class="flex space-x-4">
                        <div class="flex flex-col mb-4 w-full">
                            <label class="flex flex-col text-left">
                                <span class="text-gray-700 mb-2">{{ __('trainer.filter.start') }}</span>
                            </label>
                            <x-inputs.date id="start" wire:model.defer="start_time" />
                            @error('start_time')
                            <p class="text-xs text-red-600">{{ $message }}</p>
                            @enderror
                        </div>
                        <div class="flex flex-col mb-4 w-full">
                            <label class="flex flex-col text-left">
                                <span class="text-gray-700 mb-2">{{ __('trainer.filter.end') }}</span>
                            </label>
                            <x-inputs.date id="end" wire:model.defer="end_time" />
                            @error('end_time')
                            <p class="text-xs text-red-600">{{ $message }}</p>
                            @enderror
                        </div>
                    </div>
                    <div class="flex space-x-4">
                        <div class="flex justify-between items-center mb-4">
                            <label class="flex flex-col text-left">
                                <span class="text-gray-700 mb-2">{{ __('trainer.filter.affiliates') }}</span>
                                <select
                                        class="w-48 px-3 py-1 border rounded focus:border-blue-500 hover:border-blue-500 text-sm outline-none focus:input-shadow">
                                    <option value>{{ __('admin.courses.index.filter.all') }}</option>
{{--                                    @foreach(\App\Models\Affiliates::all() as $item)--}}
{{--                                        <option value="{{ $item->id }}">{{ $item->title }}</option>--}}
{{--                                    @endforeach--}}
                                </select>
                            </label>
                        </div>
                        <div class="flex flex-col mb-4">
                            <label class="flex flex-col text-left">
                                <span class="text-gray-700 mb-2 whitespace-nowrap">{{ __('trainer.filter.langs') }}</span>
                            </label>
                            <div class="flex items-center space-x-2">
                                <select
                                        class="w-48 px-3 py-1 border rounded focus:border-blue-500 hover:border-blue-500 text-sm outline-none focus:input-shadow">
                                    <option value>{{ __('admin.courses.index.filter.all') }}</option>
{{--                                    @foreach(\App\Models\Languages::all() as $item)--}}
{{--                                        <option value="{{ $item->id }}">{{ $item->title }}</option>--}}
{{--                                    @endforeach--}}
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="flex justify-end">
                    <button
                            class="text-sm flex justify-center items-center space-x-1.5 px-4 py-1 border-0 bg-blue-500 text-white rounded hover:bg-blue-600">
                        <span>{{ __('trainer.buttons.accept.filters') }}</span>
                    </button>
                </div>
            </div>
            @if($groups->isEmpty())
                <div class="w-full">
                    <div class="bg-[#F9F9F9] h-40 flex">
                        <span class="m-auto text-[17px] text-[#929292]">{{ __('trainer.groups.groups.empty') }}</span>
                    </div>
                </div>
            @else
                <div class="w-full">
                    <div class="w-full">
                        <table class="w-full text-[15px] text-center">
                            <thead class="bg-[#F9F9F9] border-b-2 ">
                            <tr>
                                <th class="py-4 w-1/3  text-left pl-4">{{ __('trainer.table.head.title.program') }}</th>
                                <th class="py-4 text-left pl-4">{{ __('trainer.table.head.group') }}</th>
                                <th class="py-4 text-left pl-4">{{ __('trainer.table.head.period') }}</th>
                                <th class="py-4 text-left pl-4">{{ __('trainer.table.head.listeners') }}</th>
                                <th class="py-4 ">{{ __('trainer.table.head.action') }}</th>
                            </tr>
                            </thead>
                            <tbody class="">
{{--                            @dd($groups->toArray())--}}
                            @foreach($groups as $group)
                                <tr onclick="window.location = '{{ route('trainer.groups.view', $group->slug) }}'" class="border-b-2 cursor-pointer hover:bg-[#E3E3E3]">
                                    <td class="py-4 w-1/3 text-left pl-4">{{ $group->coursesItem->ru_title }}</td>
                                    <td class="py-4 text-left pl-4">{{ $group->slug }}</td>
                                    <td class="py-4 text-left pl-4">{{ date('d.m.y',strtotime($group->start_time)) }} &mdash; {{ date('d.m.y',strtotime($group->end_time)) }}</td>
                                    <td class="py-4 text-left pl-4">{{ $group->users()->count() }}/{{ $group->count_all }}</td>
                                    <td class="text-[#3A8CF7]"><a href="{{ route('trainer.groups.view', $group->slug) }}">{{ __('trainer.links.login') }}</a></td>
                                </tr>
                            @endforeach
                            </tbody>

                        </table>
                    </div>
                </div>
            @endif
        </div>
        <div class="mb-4"></div>
        <div class="w-full flex flex-col space-y-4">
            <div class="w-full">
                <span class="text-[20px] font-[300]">{{ __('trainer.programs.title') }}</span>
            </div>
            @if(Auth::user()->courses->isEmpty())
                <div class="w-full">
                    <div class="bg-[#F9F9F9] h-40 flex">
                        <span class="m-auto text-[17px] text-[#929292]">{{ __('trainer.programs.title.empty') }}</span>
                    </div>
                </div>
            @else
                <div class="w-full">
                    <div class="w-full">
                        <table class="w-full text-[15px]">
                            <thead class="bg-[#F9F9F9] border-b-2 ">
                            <tr>
                                <th scope="col"
                                    class="px-2 py-3 bg-gray-50 text-left text-sm font-medium text-gray-800">
                                    <div class="pl-4">{{ __('trainer.table.head.title.program') }}</div>
                                </th>
                                <th scope="col"
                                    class="px-2 py-3 bg-gray-50 text-left text-sm font-medium text-gray-800">
                                    <div class="pl-4 border-l border-gray-200 whitespace-nowrap">{{ __('trainer.table.head.title.type') }}</div>
                                </th>
                                <th scope="col"
                                    class="px-2 py-3 bg-gray-50 text-left text-sm font-medium text-gray-800">
                                    <div class="pl-4 border-l border-gray-200">{{ __('trainer.table.head.title.grading') }}</div>
                                </th>
                                <th scope="col"
                                    class="px-2 py-3 bg-gray-50 text-left text-sm font-medium text-gray-800">
                                    <div class="pl-4 border-l border-gray-200">{{ __('admin.table.head.action') }}</div>
                                </th>
                            </tr>
                            </thead>
                            <tbody class="">
                            @foreach(Auth::user()->courses as $course)
                                <tr onclick="window.location = '{{ route('trainer.programs.view', $course->id) }}'" class="cursor-pointer">
                                    <td class="px-6 py-1.5 text-sm text-gray-900">
                                        {!! $course->ru_title !!}
                                    </td>

                                    <td class="px-6 py-1.5 whitespace-nowrap text-sm text-gray-900">
                                        {!! $course->typeString !!}
                                    </td>

                                    <td class="px-6 py-1.5 whitespace-nowrap text-sm text-gray-900">
                                        {!! $course->gradingString !!}
                                    </td>

                                    <td class="px-6 py-1.5 whitespace-nowrap text-sm font-medium space-x-2">
                                        <a href="{{ route('trainer.programs.view', $course->id) }}"
                                           class="text-blue-500 hover:text-blue-900 m-0 font-normal">{{ __('trainer.links.show') }}</a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            @endif
        </div>
    </div>
</div>

@push('styles')
{{--    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/flatpickr/dist/flatpickr.min.css">--}}
{{--    <script src="https://cdn.jsdelivr.net/npm/flatpickr"></script>--}}
{{--    <script src="https://cdn.jsdelivr.net/npm/flatpickr/dist/l10n/ru.js"></script>--}}
@endpush
