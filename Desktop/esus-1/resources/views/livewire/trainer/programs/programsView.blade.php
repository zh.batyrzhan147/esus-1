<div>
    <x-slot name="header"></x-slot>

    <div class="py-6 px-4 sm:px-6 lg:px-8 h-full">
        <div class="flex flex-col items-start md:items-center w-full md:w-3/4 mx-auto space-y-4">
            <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-center">
                <div class="w-full md:w-1/4">
                    <label for="type" class="w-full text-left md:text-right block text-sm">{{ __('trainer.programs.kztitle') }}</label>
                </div>
                <div class="w-full md:w-3/4">
                    <div>{!! $program->kz_title !!}</div>
                </div>
            </div>
            <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-center">
                <div class="w-full md:w-1/4">
                    <label for="type" class="w-full text-left md:text-right block text-sm">{{ __('trainer.programs.rutitle') }}</label>
                </div>
                <div class="w-full md:w-3/4">
                    <div>{!! $program->ru_title !!}</div>
                </div>
            </div>
            <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-center">
                <div class="w-full md:w-1/4">
                    <label for="type" class="w-full text-left md:text-right block text-sm">{{ __('trainer.programs.type') }}</label>
                </div>
                <div class="w-full md:w-3/4">
                    <div>{{ $program->type }}</div>
                </div>
            </div>

            <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-center">
                <div class="w-full md:w-1/4">
                    <label for="type" class="w-full text-left md:text-right block text-sm">{{ __('trainer.programs.grading') }}</label>
                </div>
                <div class="w-full md:w-3/4">
                    <div>{{ $program->grading }}</div>
                </div>
            </div>

            <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-center">
                <div class="w-full md:w-1/4">
                    <label for="course_length" class="w-full text-left md:text-right block text-sm">{{ __('trainer.programs.continues') }}</label>
                </div>
                <div class="w-full md:w-3/4">
                    <div>{{ $program->course_length }}</div>
                </div>
            </div>

            <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-center">
                <div class="w-full md:w-1/4">
                    <label for="webinar_count" class="w-full text-left md:text-right block text-sm">{{ __('trainer.programs.webinar.count') }}</label>
                </div>
                <div class="w-full md:w-3/4">
                    <div>{{ $program->webinar_count }}</div>
                </div>
            </div>

            <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-center">
                <div class="w-full md:w-1/4">
                    <label for="webinar_length" class="w-full text-left md:text-right block text-sm">{{ __('trainer.programs.webinar.count.hours') }}</label>
                </div>
                <div class="w-full md:w-3/4">
                    <div>{{ $program->webinar_length }}</div>
                </div>
            </div>

            <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-center">
                <div class="w-full md:w-1/4">
                    <label for="file_1_listener" class="w-full text-left md:text-right block text-sm">{{ __('trainer.programs.docs.listener.kz') }}</label>
                </div>
                <div class="w-full md:w-3/4">
                    <div>{{ $program->file_1_listener }}</div>
                </div>
            </div>

            <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-center">
                <div class="w-full md:w-1/4">
                    <label for="file_2_listener" class="w-full text-left md:text-right block text-sm">{{ __('trainer.programs.docs.listener.ru') }}</label>
                </div>
                <div class="w-full md:w-3/4">
                    <div>{{ $program->file_2_listener }}</div>
                </div>
            </div>

            <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-center">
                <div class="w-full md:w-1/4">
                    <label for="file_1_teacher" class="w-full text-left md:text-right block text-sm">{{ __('trainer.programs.docs.trainer.kz') }}</label>
                </div>
                <div class="w-full md:w-3/4">
                    <div>{{ $program->file_1_teacher }}</div>
                </div>
            </div>

            <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-center">
                <div class="w-full md:w-1/4">
                    <label for="file_2_teacher" class="w-full text-left md:text-right block text-sm">{{ __('trainer.programs.docs.trainer.ru') }}</label>
                </div>
                <div class="w-full md:w-3/4">
                    <div>{{ $program->file_2_teacher }}</div>
                </div>
            </div>


            <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-center">
                <div class="w-full md:w-1/4">
                    <label for="sdo_category" class="w-full text-left md:text-right block text-sm">{{ __('trainer.programs.sdo.category') }}</label>
                </div>
                <div class="w-full md:w-3/4">
                    <div>{{ $program->sdo_category }}</div>
                </div>
            </div>

            <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-center">
                <div class="w-full md:w-1/4">
                    <label for="sdo_link" class="w-full text-left md:text-right block text-sm">{{ __('trainer.programs.sdo.link') }}</label>
                </div>
                <div class="w-full md:w-3/4">
                    <div>{{ $program->sdo_link }}</div>
                </div>
            </div>

            <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-center">
                <div class="w-full md:w-1/4">
                    <label for="one_drive_link" class="w-full text-left md:text-right block text-sm">{{ __('trainer.programs.onedrive.link') }}</label>
                </div>
                <div class="w-full md:w-3/4">
                    <div>{{ $program->one_drive_link }}</div>
                </div>
            </div>

        </div>
    </div>
    <div class="flex flex-col mx-auto py-2 px-4 sm:px-6 lg:px-8">
        <div class="w-full flex flex-col space-y-4">
            <div class="w-full">
                <span class="text-[20px] font-[300]">{{ __('trainer.programs.groups.title', ['program' => $program->ru_title ?? $program->kz_title ?? '']) }}</span>
            </div>
            @if(Auth::user()->groups->isEmpty())
                <div class="w-full">
                    <div class="bg-[#F9F9F9] h-40 flex">
                        <span class="m-auto text-[17px] text-[#929292]">{{ __('trainer.groups.groups.empty') }}</span>
                    </div>
                </div>
            @else
                <div class="w-full">
                    <div class="w-full">
                        <table class="w-full text-[15px] text-center">
                            <thead class="bg-[#F9F9F9] border-b-2 ">
                            <tr>
                                <th class="py-4 w-1/3  text-left pl-4">{{ __('trainer.table.head.title.program') }}</th>
                                <th class="py-4 text-left pl-4">{{ __('trainer.table.head.group') }}</th>
                                <th class="py-4 text-left pl-4">{{ __('trainer.table.head.period') }}</th>
                                <th class="py-4 text-left pl-4">{{ __('trainer.table.head.listeners') }}</th>
                                <th class="py-4 ">{{ __('trainer.table.head.action') }}</th>
                            </tr>
                            </thead>
                            <tbody class="">
                            @foreach(Auth::user()->groups as $group)
                                @if($group->courses === $program->id)
                                <tr onclick="window.location = '{{ route('trainer.groups.view', $group->slug) }}'" class="border-b-2 cursor-pointer hover:bg-[#E3E3E3]">
                                    <td class="py-4 w-1/3 text-left pl-4">{{ $group->coursesItem->ru_title }}</td>
                                    <td class="py-4 text-left pl-4">{{ $group->slug }}</td>
                                    <td class="py-4 text-left pl-4">{{ date('d.m.y',strtotime($group->start_time)) }} &mdash; {{ date('d.m.y',strtotime($group->end_time)) }}</td>
                                    <td class="py-4 text-left pl-4">{{ $group->users()->count() }}/{{ $group->count_all }}</td>
                                    <td class="text-[#3A8CF7]"><a href="{{ route('trainer.groups.view', $group->slug) }}">{{ __('trainer.links.login') }}</a></td>
                                </tr>
                                @endif
                            @endforeach
                            </tbody>

                        </table>
                    </div>
                </div>
            @endif
        </div>
    </div>
</div>
