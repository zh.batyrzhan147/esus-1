<div>
    <div class="bg-white mx-auto py-6 px-4 sm:px-6 lg:px-8">
        <div class="flex w-full justify-between items-center">
            <div class="flex items-center">
                <h2 class="font-semibold text-xl text-gray-800 leading-tight">
                    {{ __('admin.groups.certificate.title') }}: {{ $group->slug }}
                </h2>
            </div>

            <div class="flex justify-end items-center">
                @if($group->status == \App\Models\Groups::GROUP_STATUS_FINISHED)
                    <button wire:click="$emit('triggerChangeStatus',{{ $group->status }})"
                            class="mb-2 text-sm flex justify-center items-center space-x-1.5 px-4 py-1 border-0 bg-blue-500 text-white rounded hover:bg-blue-600">
                        <span>{{ __('admin.groups.certificate.button') }}</span>
                    </button>
                @endif
                @if($group->status == \App\Models\Groups::GROUP_STATUS_CERTIFICATED)
                    <span>{{ __('admin.groups.certificate.button.done') }}</span>
                @endif
            </div>

        </div>
    </div>
    <div class="py-6 px-4 sm:px-6 lg:px-8">
        <div class="flex flex-col">
            @if($this->group->coursesItem->certificateTemplate()->where('type', \App\Models\CertificatesTemplates::CERTIFICATES_TEMPLATES_TYPE_CERTIFICATE)->first())
                <div class="-my-2 overflow-x-auto sm:-mx-6 lg:-mx-8">

                    <div class="flex w-full justify-between items-center px-4 sm:px-6 lg:px-8">
                        <div class="flex items-center">
                            <h2 class="font-semibold text-xl text-gray-800 leading-tight">
                                {{ __('admin.groups.certificate.title2') }}
                            </h2>
                        </div>
                        @if(count($selected)>0)
                            <div class="flex items-center">
                                <button wire:click="issueSelected"
                                        class="mb-2 text-sm flex justify-center items-center space-x-1.5 px-4 py-1 border-0 bg-blue-500 text-white rounded hover:bg-blue-600">
                                    Выдать выбранным
                                </button>
                            </div>
                        @endif
                    </div>
                    <div class="py-2 align-middle inline-block min-w-full sm:px-6 lg:px-8">
                        <div class="shadow overflow-hidden border-b border-gray-200 sm:rounded">
                            <table class="min-w-full divide-y divide-gray-200 w-full">
                                <thead>
                                <tr>
                                    <th scope="col"
                                        class="px-2 py-3 bg-gray-50 text-left text-sm font-medium text-gray-800">
                                        <div class="pl-4">{{ __('admin.table.head.id') }}</div>
                                    </th>
                                    <th scope="col"
                                        class="px-2 py-3 bg-gray-50 text-left text-sm font-medium text-gray-800">
                                        <div class="pl-4 border-l border-gray-200">{{ __('admin.table.head.fio') }}</div>
                                    </th>
                                    <th scope="col"
                                        class="px-2 py-3 bg-gray-50 text-left text-sm font-medium text-gray-800">
                                        <div class="pl-4">{{ __('admin.table.head.group') }}</div>
                                    </th>
                                    <th scope="col"
                                        class="px-2 py-3 bg-gray-50 text-left text-sm font-medium text-gray-800">
                                        <div class="pl-4">{{ __('admin.table.head.recommended') }}</div>
                                    </th>

                                @if($group->status != \App\Models\Groups::GROUP_STATUS_CERTIFICATED)
                                        <th scope="col"
                                            class="px-2 py-3 w-[20px] bg-gray-50 text-center text-sm font-medium text-gray-800">
                                            <input type="checkbox" wire:model="selectAll"/>
                                        </th>
                                    @endif
{{--                                    <th scope="col" width="200"--}}
{{--                                        class="px-2 py-3 bg-gray-50 text-left text-sm font-medium text-gray-800">--}}
{{--                                        <div class="pl-4 border-l border-gray-200">{{ __('admin.table.head.action') }}</div>--}}
{{--                                    </th>--}}
                                </tr>
                                </thead>
                                <tbody class="bg-white divide-y divide-gray-200">
                                @foreach($group->users as $item)
                                    @if(!in_array($item->id,$certs->pluck('user')->toArray()))
                                    <tr>
                                        <td class="px-6 py-1.5 text-sm text-gray-900">
                                            {{ $item->id }}
                                        </td>
                                        <td class="px-6 py-1.5 whitespace-nowrap text-sm text-gray-900">
                                            {{ $item->fio }}
                                        </td>
                                        <td class="px-6 py-1.5 text-sm text-gray-900">
                                            {{ $group->slug }}
                                        </td>
                                        <td class="px-6 py-1.5 text-sm text-gray-900">
                                            <label for="default-toggle{{$item->id}}" class="inline-flex relative items-center cursor-pointer">
                                                <input type="checkbox" value="" disabled id="default-toggle{{$item->id}}" class="sr-only peer" @if(in_array($item->id,$recommended)) checked @endif>
                                                <div class="w-11 h-6 bg-gray-200 peer-focus:outline-none peer-focus:ring-0 peer-focus:ring-green-300 dark:peer-focus:ring-green-800 rounded-full peer peer-checked:after:translate-x-full peer-checked:after:border-white after:content-[''] after:absolute after:top-[2px] after:left-[2px] after:bg-white after:border-gray-300 after:border after:rounded-full after:h-5 after:w-5 after:transition-all dark:border-gray-600 peer-checked:bg-green-600"></div>
                                                <span class="ml-3 text-sm font-medium text-gray-900 dark:text-gray-300">@if ($group->coursesItem->grading)
                                                        {{ __('admin.table.tested') }}
                                                    @else
                                                        {{ __('admin.table.certificeted') }}
                                                    @endif</span>
                                            </label>
                                        </td>
                                        @if($group->status != \App\Models\Groups::GROUP_STATUS_CERTIFICATED)
                                            <td class="px-6 py-1.5 text-sm text-gray-900 w-[20px]">
                                                <input type="checkbox" value="{{$item->id}}"
                                                       wire:model="selected" />
                                            </td>
                                        @endif
{{--                                        <td class="px-6 py-1.5 whitespace-nowrap text-sm font-medium space-x-2">--}}
{{--                                            @if($group->status != \App\Models\Groups::GROUP_STATUS_CERTIFICATED)--}}
{{--                                                <button wire:click="issue({{$item->id}})"--}}
{{--                                                        class="text-blue-500 hover:text-blue-900 m-0 font-normal">{{ __('admin.links.give') }}--}}
{{--                                                </button>--}}
{{--                                            @endif--}}
{{--                                        </td>--}}
                                    </tr>
                                    @endif
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>


                    <div class="flex w-full justify-between items-center px-4 sm:px-6 lg:px-8 mt-8">
                        <div class="flex items-center">
                            <h2 class="font-semibold text-xl text-gray-800 leading-tight">
                                {{ __('admin.groups.certificate.title3') }}
                            </h2>
                        </div>
                        <div class="flex items-center space-x-2">
                            <button wire:click="$emit('removeAll')"
                                    class="mb-2 text-sm flex justify-center items-center space-x-1.5 px-4 py-1 border-0 bg-red-600 text-white rounded hover:bg-red-800">
                                Удалить все
                            </button>
                            <button wire:click="downloadArchive"
                                    class="mb-2 text-sm flex justify-center items-center space-x-1.5 px-4 py-1 border-0 bg-blue-500 text-white rounded hover:bg-blue-600">
                                Скачать архив
                            </button>
                        </div>
                    </div>

                    <div class="py-2 align-middle inline-block min-w-full sm:px-6 lg:px-8">
                        <div class="shadow overflow-hidden border-b border-gray-200 sm:rounded">
                            <table class="min-w-full divide-y divide-gray-200 w-full">
                                <thead>
                                <tr>
                                    <th scope="col"
                                        class="px-2 py-3 bg-gray-50 text-left text-sm font-medium text-gray-800">
                                        <div class="text-center">{{ __('admin.table.head.id') }}</div>
                                    </th>
                                    <th scope="col"
                                        class="px-2 py-3 bg-gray-50 text-left text-sm font-medium text-gray-800">
                                        <div class="pl-4 border-l border-gray-200">{{ __('admin.table.head.fio') }}</div>
                                    </th>
                                    <th scope="col"
                                        class="px-2 py-3 bg-gray-50 text-left text-sm font-medium text-gray-800">
                                        <div class="pl-4">{{ __('admin.table.head.group') }}</div>
                                    </th>
                                    <th scope="col" width="200"
                                        class="px-2 py-3 bg-gray-50 text-left text-sm font-medium text-gray-800">
                                        <div class="pl-4 border-l border-gray-200">{{ __('admin.table.head.action') }}</div>
                                    </th>
                                </tr>
                                </thead>
                                <tbody class="bg-white divide-y divide-gray-200">
                                @forelse($certs as $item)
                                    <tr>
                                        <td class="px-6 py-1.5 text-sm text-gray-900">
                                            {{ $item->id }}
                                        </td>
                                        <td class="px-6 py-1.5 whitespace-nowrap text-sm text-gray-900">
                                            {{ $item->fio }}
                                        </td>
                                        <td class="px-6 py-1.5 text-sm text-gray-900">
                                            {{ $item->group_title }}
                                        </td>
                                        <td class="px-6 py-1.5 whitespace-nowrap text-sm font-medium space-x-2">
                                            @if($item->pdf)
                                                <a href="{{ $item->pdf }}" download="" target="_blank">{{ __('admin.links.download') }}</a>
                                                @if($group->status != \App\Models\Groups::GROUP_STATUS_CERTIFICATED)
                                                <button wire:click="$emit('triggerDelete',{{ $item->id }})"
                                                        class="text-red-500 hover:text-red-900 bg-none border-0 m-0 cursor-pointer bg-transparent font-normal ">{{ __('admin.links.delete') }}</button>
                                                @endif
                                            @else
                                                <span>На обработке</span>
                                            @endif
                                        </td>
                                    </tr>
                                @empty
                                    <tr class="h-48 w-full"><td scope="col" class="h-48 w-full text-center text-xs text-gray-500" colspan="6">{{ __('admin.table.body.empty') }}</td></tr>
                                @endforelse
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            @else
                <div class="flex flex-col space-y-3">
                <span class="text-2xl text-red-600">{{ __('admin.groups.certificate.error.text') }} </span>
                <b>{{ $group->coursesItem->ru_title }}</b>
                <a href="{{ route('ooup.certificates-templates.index') }}" class="text-blue-600 text-xl hover:underline">{{ __('admin.groups.certificate.link.create') }}</a>
                </div>
            @endif
        </div>
    </div>
</div>

@push('scripts')
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    <script type="text/javascript">
        document.addEventListener('DOMContentLoaded', function () {
        @this.on('triggerChangeStatus', itemID => {
            Swal.fire({
                title: '{{ __("admin.swal.temp.title") }}',
                text: '',
                icon: "warning",
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#aaa',
                cancelButtonText: '{{ __("admin.swal.temp.buttons.cancel") }}',
                confirmButtonText: '{{ __("admin.swal.temp.buttons.approve") }}'
            }).then((result) => {
                //if user clicks on delete
                if (result.value) {
                    // calling destroy method to delete
                @this.call('changeGroupStatus', itemID);
                    // success response
                    Swal.fire({
                        title: 'Удачно!', icon: 'success',
                        width: 600,
                        position: 'top-end',
                        showConfirmButton: false,
                        timer: 1000
                    });
                } else {
                    Swal.fire({
                        title: '{{ __("admin.swal.temp.discarded.title") }}',
                        icon: 'success',
                        width: 600,
                        position: 'top-end',
                        showConfirmButton: false,
                        timer: 1000
                    });
                }
            });
        });
        @this.on('removeAll',() => {
                Swal.fire({
                    title: 'Удалить все сертификаты?',
                    text: '',
                    icon: "warning",
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#aaa',
                    confirmButtonText: 'Удалить!',
                    cancelButtonText: 'Отменить'
                }).then((result) => {
                    //if user clicks on delete
                    if (result.value) {
                        Swal.fire({
                            title: 'Удалить все сертификаты?',
                            text: 'Удаление сертификатов может привезти к не обратимым последствиям!',
                            icon: "error",
                            showCancelButton: true,
                            confirmButtonColor: '#3085d6',
                            cancelButtonColor: '#aaa',
                            confirmButtonText: 'Я уверен, удалить!',
                            cancelButtonText: 'Отменить'
                        }).then((result) => {
                            //if user clicks on delete
                            if (result.value) {
                                // calling destroy method to delete
                                @this.call('destroyAllCertificates');
                            } else {
                                Swal.fire({
                                    title: '{{ __("admin.swal.temp.discarded.title") }}',
                                    icon: 'success',
                                    width: 600,
                                    position: 'top-end',
                                    showConfirmButton: false,
                                    timer: 1000
                                });
                            }
                        });
                    } else {
                        Swal.fire({
                            title: '{{ __("admin.swal.temp.discarded.title") }}',
                            icon: 'success',
                            width: 600,
                            position: 'top-end',
                            showConfirmButton: false,
                            timer: 1000
                        });
                    }
                });
        });
        @this.on('triggerDelete', itemID => {
            console.log(itemID);

            Swal.fire({
                title: '{{ __("admin.swal.temp.title") }}',
                text: 'Год обучения будет удален! Это может привести к необратимым последствиям!',
                icon: "warning",
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#aaa',
                confirmButtonText: 'Я уверен, удалить!'
            }).then((result) => {
                //if user clicks on delete
                if (result.value) {
                    // calling destroy method to delete
                @this.call('destroy',itemID);
                    // success response
                } else {
                    Swal.fire({
                        title: '{{ __("admin.swal.temp.discarded.title") }}',
                        icon: 'success',
                        width: 600,
                        position: 'top-end',
                        showConfirmButton: false,
                        timer: 1000
                    });
                }
            });
        });
        @this.on('SuccessDeleted',() => {
                Swal.fire({title: '{{ __("admin.swal.temp.deleted.title") }}', icon: 'success',
                    width: 600,
                    position: 'top-end',
                    showConfirmButton: false,
                    timer: 1000});
            })
        });
    </script>

@endpush
