<div>
    <x-slot name="header">
        <div class="bg-white mx-auto py-6 px-4 sm:px-6 lg:px-8">
            <h2 class="font-semibold text-xl text-gray-800 leading-tight">
                    {{ __('admin.courses.show.title') }}
            </h2>
        </div>
    </x-slot>

    <div class="py-6 px-4 sm:px-6 lg:px-8 h-full">
        <div class="flex flex-col items-start md:items-center w-full md:w-3/4 mx-auto space-y-4">
            <div class="flex flex-col items-start w-full mx-auto space-y-4">
                <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-center">
                    <div class="w-full md:w-1/4">
                        <label for="kz_title" class="w-full text-left md:text-right block text-sm">{{ __('admin.courses.create.form.kztitle') }}</label>
                    </div>
                    <div class="w-full md:w-3/4">
                        <span>{{ $course->kz_title }}</span>
                        {{--                        <input wire:model.lazy="kz_title" type="text" name="kz_title" id="kz_title" class="w-full md:w-3/4 px-3 py-1 border rounded focus:border-blue-500 hover:border-blue-500 text-sm outline-none focus:input-shadow"--}}
                        {{--                               value="{{ old('kz_title', $kz_title) }}" />--}}
                        {{--                        @error('kz_title')--}}
                        {{--                        <p class="text-xs text-red-600">{{ $message }}</p>--}}
                        {{--                        @enderror--}}
                    </div>
                </div>

                <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-center">
                    <div class="w-full md:w-1/4">
                        <label for="ru_title" class="w-full text-left md:text-right block text-sm">{{ __('admin.courses.create.form.rutitle') }}</label>
                    </div>
                    <div class="w-full md:w-3/4">
                        <span>{{ $course->ru_title }}</span>
                        {{--                        <input wire:model.lazy="ru_title" type="text" name="ru_title" id="ru_title" class="w-full md:w-3/4 px-3 py-1 border rounded focus:border-blue-500 hover:border-blue-500 text-sm outline-none focus:input-shadow"--}}
                        {{--                               value="{{ old('ru_title', $ru_title) }}" />--}}
                        {{--                        @error('ru_title')--}}
                        {{--                        <p class="text-xs text-red-600">{{ $message }}</p>--}}
                        {{--                        @enderror--}}
                    </div>
                </div>

                <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-center">
                    <div class="w-full md:w-1/4">
                        <label for="kz_short_title" class="w-full text-left md:text-right block text-sm">{{ __('Краткое название (Каз):') }}</label>
                    </div>
                    <div class="w-full md:w-3/4">
                        <span>{{ $course->kz_short_title }}</span>
                    </div>
                </div>


                <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-center">
                    <div class="w-full md:w-1/4">
                        <label for="ru_short_title" class="w-full text-left md:text-right block text-sm">{{ __('Краткое название (Рус):') }}</label>
                    </div>
                    <div class="w-full md:w-3/4">
                        <span>{{ $course->ru_short_title }}</span>
                    </div>
                </div>

                <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-center">
                    <div class="w-full md:w-1/4">
                        <label for="en_short_title" class="w-full text-left md:text-right block text-sm">{{ __('Краткое название (Анг):') }}</label>
                    </div>
                    <div class="w-full md:w-3/4">
                        <span>{{ $course->ru_short_title }}</span>
                    </div>
                </div>

                <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-center">
                    <div class="w-full md:w-1/4">
                        <label for="slug" class="w-full text-left md:text-right block text-sm">{{ __('admin.courses.create.form.slug') }}</label>
                    </div>
                    <div class="w-full md:w-3/4 ">
                        <span>{{ $course->slug }}</span>
                        {{--                        <div class="w-full md:w-3/4 flex flex-row">--}}
                        {{--                            <input wire:model.lazy="slug" type="text" name="slug" id="slug" class="w-full md:w-3/4 px-3 py-1 border rounded focus:border-blue-500 hover:border-blue-500 text-sm outline-none focus:input-shadow"--}}
                        {{--                                   value="{{ old('slug', $slug) }}" />--}}
                        {{--                            <button type="button" wire:click="generate" class="ml-2 text-sm flex justify-center items-center px-4 py-1 border-0 bg-blue-500 text-white rounded hover:bg-blue-600">Генерировать</button>--}}
                        {{--                        </div>--}}
                        {{--                        @error('slug')--}}
                        {{--                        <p class="text-xs text-red-600">{{ $message }}</p>--}}
                        {{--                        @enderror--}}
                    </div>
                </div>
            </div>

            <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-center">
                <div class="w-full md:w-1/4">
                    <label for="type" class="w-full text-left md:text-right block text-sm">{{ __('admin.courses.create.form.course.type') }}</label>
                </div>
                <div class="w-full md:w-3/4">
                    <span>{{ \App\Models\Courses::listTypes()[$course->type] }}</span>
                    {{--                    <select wire:model.defer="course.type" id="type"--}}
                    {{--                            class="w-full md:w-3/4 px-3 py-1 border rounded focus:border-blue-500 hover:border-blue-500 text-sm outline-none focus:input-shadow">--}}
                    {{--                        @foreach(\App\Models\Courses::listTypes() as $key => $type)--}}
                    {{--                            <option value="{{ $key }}" @if( ($course->type ?? '') == $key) selected @endif>{{ $type }}</option>--}}
                    {{--                        @endforeach--}}
                    {{--                    </select>--}}
                    {{--                    @error('course.type')--}}
                    {{--                    <p class="text-xs text-red-600">{{ $message }}</p>--}}
                    {{--                    @enderror--}}
                </div>
            </div>

            <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-center">
                <div class="w-full md:w-1/4">
                    <label for="grading" class="w-full text-left md:text-right block text-sm">{{ __('admin.courses.create.form.course.grading') }}</label>
                </div>
                <div class="w-full md:w-3/4">
                    <span>{{ \App\Models\Courses::listGrading()[$course->grading] }}</span>
                    {{--                    <select wire:model.defer="course.grading" id="grading"--}}
                    {{--                            class="w-full md:w-3/4 px-3 py-1 border rounded focus:border-blue-500 hover:border-blue-500 text-sm outline-none focus:input-shadow">--}}
                    {{--                        @foreach(\App\Models\Courses::listGrading() as $key => $grading)--}}
                    {{--                            <option value="{{ $key }}" @if( ($course->grading ?? '') == $key) selected @endif>{{ $grading }}</option>--}}
                    {{--                        @endforeach--}}
                    {{--                    </select>--}}
                    {{--                    @error('course.grading')--}}
                    {{--                    <p class="text-xs text-red-600">{{ $message }}</p>--}}
                    {{--                    @enderror--}}
                </div>
            </div>

            <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-center">
                <div class="w-full md:w-1/4">
                    <label for="auto_generation" class="w-full text-left md:text-right block text-sm">{{ __('admin.courses.create.form.course.autogeneration') }}</label>
                </div>
                <div class="w-full md:w-3/4">
                    <span>@if( $course->auto_generation == true) Разрешено @else Запрещено  @endif</span>
                    {{--                    <select wire:model.defer="course.auto_generation" id="auto_generation"--}}
                    {{--                            class="w-full md:w-3/4 px-3 py-1 border rounded focus:border-blue-500 hover:border-blue-500 text-sm outline-none focus:input-shadow">--}}
                    {{--                        <option value="1" @if( $course->auto_generation == true) selected @endif>Разрешено</option>--}}
                    {{--                        <option value="0" @if( $course->auto_generation == false) selected @endif>Запрещено</option>--}}
                    {{--                    </select>--}}
                    {{--                    @error('course.auto_generation')--}}
                    {{--                    <p class="text-xs text-red-600">{{ $message }}</p>--}}
                    {{--                    @enderror--}}
                </div>
            </div>

            <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-center">
                <div class="w-full md:w-1/4">
                    <label for="course_length" class="w-full text-left md:text-right block text-sm">{{ __('admin.courses.create.form.course.length') }}</label>
                </div>
                <div class="w-full md:w-3/4">
                    <span>{{ $course->course_length }}</span>
                    {{--                    <input type="text" wire:model.defer="course.course_length" id="course_length" class="w-full md:w-3/4 px-3 py-1 border rounded focus:border-blue-500 hover:border-blue-500 text-sm outline-none focus:input-shadow"--}}
                    {{--                           value="{{ old('course_length', $course->course_length) }}" />--}}
                    {{--                    @error('course.course_length')--}}
                    {{--                    <p class="text-xs text-red-600">{{ $message }}</p>--}}
                    {{--                    @enderror--}}
                </div>
            </div>
            <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-center">
                <div class="w-full md:w-1/4">
                    <label for="course_length" class="w-full text-left md:text-right block text-sm">{{ __('admin.courses.create.form.course.length') }}</label>
                </div>
                <div class="w-full md:w-3/4">
                    <span>{{ $course->practice_length }}</span>
                    {{--                    <input type="text" wire:model.defer="course.course_length" id="course_length" class="w-full md:w-3/4 px-3 py-1 border rounded focus:border-blue-500 hover:border-blue-500 text-sm outline-none focus:input-shadow"--}}
                    {{--                           value="{{ old('course_length', $course->course_length) }}" />--}}
                    {{--                    @error('course.course_length')--}}
                    {{--                    <p class="text-xs text-red-600">{{ $message }}</p>--}}
                    {{--                    @enderror--}}
                </div>
            </div>


            <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-center">
                <div class="w-full md:w-1/4">
                    <label for="webinar_count" class="w-full text-left md:text-right block text-sm">{{ __('admin.courses.create.form.course.webinar.count') }}</label>
                </div>
                <div class="w-full md:w-3/4">
                    <span>{{ $course->webinar_count }}</span>
                    {{--                    <input type="text" wire:model.defer="course.webinar_count" id="webinar_count" class="w-full md:w-3/4 px-3 py-1 border rounded focus:border-blue-500 hover:border-blue-500 text-sm outline-none focus:input-shadow"--}}
                    {{--                           value="{{ old('webinar_count', $course->webinar_count) }}" />--}}
                    {{--                    @error('course.webinar_count')--}}
                    {{--                    <p class="text-xs text-red-600">{{ $message }}</p>--}}
                    {{--                    @enderror--}}
                </div>
            </div>

            <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-center">
                <div class="w-full md:w-1/4">
                    <label for="webinar_length" class="w-full text-left md:text-right block text-sm">{{ __('admin.courses.create.form.course.webinar.length') }}</label>
                </div>
                <div class="w-full md:w-3/4">
                    <span>{{ $course->webinar_length }}</span>
                    {{--                    <input type="text" wire:model.defer="course.webinar_length" id="webinar_length" class="w-full md:w-3/4 px-3 py-1 border rounded focus:border-blue-500 hover:border-blue-500 text-sm outline-none focus:input-shadow"--}}
                    {{--                           value="{{ old('webinar_length', $course->webinar_length) }}" />--}}
                    {{--                    @error('course.webinar_length')--}}
                    {{--                    <p class="text-xs text-red-600">{{ $message }}</p>--}}
                    {{--                    @enderror--}}
                </div>
            </div>

            <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-center">
                <div class="w-full md:w-1/4">
                    <label for="file_1_listener" class="w-full text-left md:text-right block text-sm">{{ __('admin.courses.create.form.file.kzlistener') }}:</label>
                </div>
                @if($course->file_1_listener)
                    <a href="{{ $course->getFileUrl($course->file_1_listener) }}" download="{{ __('admin.courses.create.form.file.kzlistener') }}.{{ pathinfo($course->file_1_listener, PATHINFO_EXTENSION)}}" target="_blank">{{ __('admin.links.download') }}</a>
                @endif
                <div class="w-full md:w-3/4">
                    {{--                    <input type="file" wire:model.defer="file_1_listener" id="file_1_listener" class="w-full md:w-3/4 px-3 py-1 border rounded focus:border-blue-500 hover:border-blue-500 text-sm outline-none focus:input-shadow"--}}
                    {{--                           value="{{ old('file_1_listener', $course->file_1_listener) }}" />--}}
                    {{--                    @error('course.file_1_listener')--}}
                    {{--                    <p class="text-xs text-red-600">{{ $message }}</p>--}}
                    {{--                    @enderror--}}
                </div>
            </div>

            <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-center">
                <div class="w-full md:w-1/4">
                    <label for="file_2_listener" class="w-full text-left md:text-right block text-sm">{{ __('admin.courses.create.form.file.rulistener') }}:</label>
                </div>
                @if($course->file_2_listener)
                    <a href="{{ $course->getFileUrl($course->file_2_listener) }}" download="{{ __('admin.courses.create.form.file.rulistener') }}.{{ pathinfo($course->file_2_listener, PATHINFO_EXTENSION)}}" target="_blank">{{ __('admin.links.download') }}</a>
                @endif
                <div class="w-full md:w-3/4">
                    {{--                    <input type="file" wire:model.defer="file_2_listener" id="file_2_listener" class="w-full md:w-3/4 px-3 py-1 border rounded focus:border-blue-500 hover:border-blue-500 text-sm outline-none focus:input-shadow"--}}
                    {{--                           value="{{ old('file_2_listener', $course->file_2_listener) }}" />--}}
                    {{--                    @error('course.file_2_listener')--}}
                    {{--                    <p class="text-xs text-red-600">{{ $message }}</p>--}}
                    {{--                    @enderror--}}
                </div>
            </div>

            <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-center">
                <div class="w-full md:w-1/4">
                    <label for="file_1_teacher" class="w-full text-left md:text-right block text-sm">{{ __('admin.courses.create.form.file.kzteacher') }}:</label>
                </div>
                @if($course->file_1_teacher)
                    <a href="{{ $course->getFileUrl($course->file_1_teacher) }}" download="{{ __('admin.courses.create.form.file.kzteacher') }}.{{ pathinfo($course->file_1_teacher, PATHINFO_EXTENSION)}}" target="_blank">{{ __('admin.links.download') }}</a>
                @endif
                <div class="w-full md:w-3/4">
                    {{--                    <input type="file" wire:model.defer="file_1_teacher" id="file_1_teacher" class="w-full md:w-3/4 px-3 py-1 border rounded focus:border-blue-500 hover:border-blue-500 text-sm outline-none focus:input-shadow"--}}
                    {{--                           value="{{ old('file_1_teacher', $course->file_1_teacher) }}" />--}}
                    {{--                    @error('course.file_1_teacher')--}}
                    {{--                    <p class="text-xs text-red-600">{{ $message }}</p>--}}
                    {{--                    @enderror--}}
                </div>
            </div>

            <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-center">
                <div class="w-full md:w-1/4">
                    <label for="file_2_teacher" class="w-full text-left md:text-right block text-sm">{{ __('admin.courses.create.form.file.ruteacher') }}:</label>
                </div>
                @if($course->file_2_teacher)
                    <a href="{{ $course->getFileUrl($course->file_2_teacher) }}" download="{{ __('admin.courses.create.form.file.ruteacher') }}.{{ pathinfo($course->file_2_teacher, PATHINFO_EXTENSION)}}" target="_blank">{{ __('admin.links.download') }}</a>
                @endif
                <div class="w-full md:w-3/4">
                    {{--                    <input type="file" wire:model.defer="file_2_teacher" id="file_2_teacher" class="w-full md:w-3/4 px-3 py-1 border rounded focus:border-blue-500 hover:border-blue-500 text-sm outline-none focus:input-shadow"--}}
                    {{--                           value="{{ old('file_2_teacher', $course->file_2_teacher) }}" />--}}
                    {{--                    @error('course.file_2_teacher')--}}
                    {{--                    <p class="text-xs text-red-600">{{ $message }}</p>--}}
                    {{--                    @enderror--}}
                </div>
            </div>


            <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-center">
                <div class="w-full md:w-1/4">
                    <label for="sdo_category" class="w-full text-left md:text-right block text-sm">{{ __('admin.courses.create.form.sdo.category') }}</label>
                </div>
                <div class="w-full md:w-3/4">
                    <span>{{ $course->sdo_category }}</span>
                    {{--                    <input type="text" wire:model.defer="course.sdo_category" id="sdo_category" class="w-full md:w-3/4 px-3 py-1 border rounded focus:border-blue-500 hover:border-blue-500 text-sm outline-none focus:input-shadow"--}}
                    {{--                           value="{{ old('sdo_category', $course->sdo_category) }}" />--}}
                    {{--                    @error('course.sdo_category')--}}
                    {{--                    <p class="text-xs text-red-600">{{ $message }}</p>--}}
                    {{--                    @enderror--}}
                </div>
            </div>

            <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-center">
                <div class="w-full md:w-1/4">
                    <label for="offer_agreement" class="w-full text-left md:text-right block text-sm">{{ __('admin.groups.add.modal.dogovor') }}</label>
                </div>
                @if($course->offer_agreement)
                    <a href="{{ $course->getFileUrl($course->offer_agreement) }}" download="{{ __('admin.groups.add.modal.dogovor') }}.{{ pathinfo($course->offer_agreement, PATHINFO_EXTENSION)}}" target="_blank">{{ __('admin.links.download') }}</a>
                @endif
            </div>

            <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-center">
                <div class="w-full md:w-1/4">
                    <label for="sdo_link" class="w-full text-left md:text-right block text-sm">{{ __('admin.courses.create.form.sdo.link') }}</label>
                </div>
                <div class="w-full md:w-3/4">
                    <span>{{ $course->one_drive_link }}</span>
                    {{--                    <input type="text" wire:model.defer="course.sdo_link" id="sdo_link" class="w-full md:w-3/4 px-3 py-1 border rounded focus:border-blue-500 hover:border-blue-500 text-sm outline-none focus:input-shadow"--}}
                    {{--                           placeholder="{{ __('admin.courses.create.form.sdo.link.placeholder') }}" value="{{ old('sdo_link', $course->sdo_link) }}" />--}}
                    {{--                    @error('course.sdo_link')--}}
                    {{--                    <p class="text-xs text-red-600">{{ $message }}</p>--}}
                    {{--                    @enderror--}}
                </div>
            </div>

            <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-center">
                <div class="w-full md:w-1/4">
                    <label for="one_drive_link" class="w-full text-left md:text-right block text-sm">{{ __('admin.courses.create.form.onedrive.link') }}</label>
                </div>
                <div class="w-full md:w-3/4">
                    <span>{{ $course->one_drive_link }}</span>
                    {{--                    <input type="text" wire:model.defer="course.one_drive_link" id="one_drive_link" class="w-full md:w-3/4 px-3 py-1 border rounded focus:border-blue-500 hover:border-blue-500 text-sm outline-none focus:input-shadow"--}}
                    {{--                           placeholder="{{ __('admin.courses.create.form.onedrive.link.placeholder') }}" value="{{ old('one_drive_link', $course->one_drive_link) }}" />--}}
                    {{--                    @error('course.one_drive_link')--}}
                    {{--                    <p class="text-xs text-red-600">{{ $message }}</p>--}}
                    {{--                    @enderror--}}
                </div>
            </div>

            <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-center">
                <div class="w-full md:w-1/4">
                </div>
                <div class="w-full md:w-3/4 space-x-2 flex">
                    {{--                    <button  wire:click="save" class="mb-2 text-sm flex justify-center items-center px-4 py-1 border-0 bg-blue-500 text-white rounded hover:bg-blue-600">@if(request()->routeIs('admin.courses.create')) {{ __('admin.buttons.create') }} @else {{ __('admin.buttons.save') }} @endif</button>--}}
                    <a href="{{ route('omr.courses.index') }}" class="py-1 px-4 border inline-block m-0 mb-2 text-sm rounded hover:border-black">{{ __('admin.buttons.cancel') }}</a>
                </div>
            </div>
        </div>
    </div>
</div>
