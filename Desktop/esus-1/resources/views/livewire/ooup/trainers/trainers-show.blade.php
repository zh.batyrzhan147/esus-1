<div xmlns:wire="http://www.w3.org/1999/xhtml">
    <x-slot name="header">
        <div class="bg-white mx-auto py-6 px-4 sm:px-6 lg:px-8">
            <h2 class="font-semibold text-xl text-gray-800 leading-tight">
                {{ __('admin.trainers.edit.title') }}
            </h2>
        </div>
    </x-slot>
    @include('admin.common.secondmenu', [
        'tabname'=>'groups',
        'popup'=>true,
    ])
    @if (session()->has('user_for_trainer_found'))
        <p class="px-4 sm:px-6 lg:px-8 text-sm"> {!! session()->get('user_for_trainer_found') !!}</p>
    @endif



    <div class="py-6 px-4 sm:px-6 lg:px-8 h-full">
        <div class="flex flex-col items-start md:items-center w-full md:w-3/4 mx-auto space-y-4">
            <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-center">
                <div class="w-full md:w-1/4">
                    <label for="iin" class="w-full text-left md:text-right block text-sm">{{ __('admin.trainers.create.iin') }}</label>
                </div>
                <div class="w-full md:w-3/4">
                    <span>{{ $trainer->iin }}</span>
                </div>
            </div>

            <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-center">
                <div class="w-full md:w-1/4">
                    <label for="last_name" class="w-full text-left md:text-right block text-sm">{{ __('admin.trainers.create.last_name') }}</label>
                </div>
                <div class="w-full md:w-3/4">
                    <span>{{ $trainer->last_name }}</span>
                </div>
            </div>
            <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-center">
                <div class="w-full md:w-1/4">
                    <label for="last_name" class="w-full text-left md:text-right block text-sm">{{ __('admin.trainers.edit.last_name.lat') }}</label>
                </div>
                <div class="w-full md:w-3/4">
                    <span>{{ $trainer->last_name_lat }}</span>
                </div>
            </div>


            <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-center">
                <div class="w-full md:w-1/4">
                    <label for="name" class="w-full text-left md:text-right block text-sm">{{ __('admin.trainers.create.name') }}</label>
                </div>
                <div class="w-full md:w-3/4">
                    <span>{{ $trainer->name }}</span>
                </div>
            </div>

            <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-center">
                <div class="w-full md:w-1/4">
                    <label for="name" class="w-full text-left md:text-right block text-sm">{{ __('admin.trainers.edit.name.lat') }}</label>
                </div>
                <div class="w-full md:w-3/4">
                    <span>{{ $trainer->name_lat }}</span>
                </div>
            </div>

            <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-center">
                <div class="w-full md:w-1/4">
                    <label for="patronymic" class="w-full text-left md:text-right block text-sm">{{ __('admin.trainers.create.patronymic') }}</label>
                </div>
                <div class="w-full md:w-3/4">
                    <span>{{ $trainer->patronymic }}</span>
                </div>
            </div>

            <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-center">
                <div class="w-full md:w-1/4">
                    <label for="email" class="w-full text-left md:text-right block text-sm">{{ __('admin.trainers.create.email') }}</label>
                </div>
                <div class="w-full md:w-3/4">
                    <span>{{ $trainer->email }}</span>
                </div>
            </div>

            <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-center">
                <div class="w-full md:w-1/4">
                    <label for="phone" class="w-full text-left md:text-right block text-sm">{{ __('admin.trainers.create.phone') }}</label>
                </div>
                <div class="w-full md:w-3/4">
                    <span>{{ $trainer->phone }}</span>
                </div>
            </div>

            <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-center">
                <div class="w-full md:w-1/4">
                    <label for="gender" class="w-full text-left md:text-right block text-sm">{{ __('admin.trainers.create.sex') }}</label>
                </div>
                <div class="w-full md:w-3/4">
                    <span>{{ $trainer->genderItem->title ?? '' }}</span>
                </div>
            </div>

            <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-center">
                <div class="w-full md:w-1/4">
                    <label for="nationality" class="w-full text-left md:text-right block text-sm">{{ __('admin.trainers.create.nationality') }}</label>
                </div>
                <div class="w-full md:w-3/4">
                    <span>{{ $trainer->nationalityItem->ru_title ?? ''  }}</span>
                    {{--                    <select wire:model="trainer.nationality"--}}
                    {{--                            class="w-full md:w-3/4 px-3 py-1 border rounded focus:border-blue-500 hover:border-blue-500 text-sm outline-none focus:input-shadow">--}}
                    {{--                        <option value="" >Выберите национальность</option>--}}
                    {{--                        @foreach(\App\Models\Nationalities::all() ?? []  as $item)--}}
                    {{--                            <option value="{{ $item->id }}">{{ $item->ru_title }}</option>--}}
                    {{--                        @endforeach--}}
                    {{--                    </select>--}}
                    {{--                    @error('trainer.nationality')--}}
                    {{--                    <p class="text-xs text-red-600">{{ $message }}</p>--}}
                    {{--                    @enderror--}}
                </div>
            </div>

            <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-center">
                <div class="w-full md:w-1/4">
                    <label for="affiliate" class="w-full text-left md:text-right block text-sm">{{ __('admin.trainers.create.affiliate') }}</label>
                </div>
                <div class="w-full md:w-3/4">
                    <span>{{ $trainer->affiliateItem->title ?? ''  }}</span>
                    {{--                    <select wire:model="trainer.affiliate" id="affiliate"--}}
                    {{--                            class="w-full md:w-3/4 px-3 py-1 border rounded focus:border-blue-500 hover:border-blue-500 text-sm outline-none focus:input-shadow">--}}
                    {{--                        <option value="null">Выберите филиал</option>--}}
                    {{--                        @foreach(\App\Models\Affiliates::all() as $key=>$item)--}}
                    {{--                            <option value="{{$item->id}}">{{ $item->title }}</option>--}}
                    {{--                        @endforeach--}}
                    {{--                    </select>--}}
                    {{--                    @error('trainer.affiliate')--}}
                    {{--                    <p class="text-xs text-red-600">{{ $message }}</p>--}}
                    {{--                    @enderror--}}
                </div>
            </div>


            <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-center">
                <div class="w-full md:w-1/4">
                    <label for="languages" class="w-full text-left md:text-right block text-sm">{{ __('admin.trainers.create.lang') }}</label>
                </div>
                <div class="w-full md:w-3/4 space-x-1">
                    @foreach($trainer->languages as $lang)
                        <div class="flex justify-between">
                            - {{ $lang->title ?? ''  }}
                        </div>
                    @endforeach
                    {{--                    @foreach(\App\Models\Languages::all()->pluck('title', 'id')->toArray() as $id => $lang)--}}
                    {{--                        @if(in_array($id, $trainer->lang))--}}
                    {{--                            <span>{{ $lang }}</span>--}}
                    {{--                        @endif--}}
                    {{--                    @endforeach--}}
                </div>
            </div>
            <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-center">
                <div class="w-full md:w-1/4">
                    <label for="auto_generation" class="w-full text-left md:text-right block text-sm">{{ __('admin.trainers.create.generate') }}</label>
                </div>
                <div class="w-full md:w-3/4">
                    <span>@if( $trainer->auto_generation == true) Разрешено @else Запрещено  @endif</span>
                    {{--                    <select wire:model.defer="trainer.auto_generation" name="auto_generation" id="auto_generation"--}}
                    {{--                            class="w-full md:w-3/4 px-3 py-1 border rounded focus:border-blue-500 hover:border-blue-500 text-sm outline-none focus:input-shadow">--}}
                    {{--                        <option value="1" @if( $trainer->auto_generation == true) selected @endif>Разрешено</option>--}}
                    {{--                        <option value="0" @if( $trainer->auto_generation == false) selected @endif>Запрещено</option>--}}
                    {{--                    </select>--}}
                    {{--                    @error('trainer.auto_generation')--}}
                    {{--                    <p class="text-xs text-red-600">{{ $message }}</p>--}}
                    {{--                    @enderror--}}
                </div>
            </div>

            <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-center">
                <div class="w-full md:w-1/4">
                    <label for="oblast" class="w-full text-left md:text-right block text-sm">{{ __('admin.trainers.create.obl') }}</label>
                </div>
                <div class="w-full md:w-3/4">
                    <span> {{ $trainer->oblastItem->ru_title ?? '' }}</span>
                    {{--                    <select wire:model="oblast"--}}
                    {{--                            class="w-full md:w-3/4 px-3 py-1 border rounded focus:border-blue-500 hover:border-blue-500 text-sm outline-none focus:input-shadow">--}}
                    {{--                        <option value="">Выберите область</option>--}}
                    {{--                        @foreach(\App\Models\Locations::where('type', \App\Models\Locations::LOCATION_OBLAST)->get() ?? []  as $item)--}}
                    {{--                            <option value="{{ $item->id }}">{{ $item->ru_title }}</option>--}}
                    {{--                        @endforeach--}}
                    {{--                    </select>--}}
                    {{--                    @error('oblast')--}}
                    {{--                    <p class="text-xs text-red-600">{{ $message }}</p>--}}
                    {{--                    @enderror--}}
                </div>
            </div>

            {{--            @if($oblast)--}}
            <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-center">
                <div class="w-full md:w-1/4">
                    <label for="raion" class="w-full text-left md:text-right block text-sm">{{ __('admin.trainers.create.rai') }}</label>
                </div>
                <div class="w-full md:w-3/4">
                    <span> {{ $trainer->raionItem->ru_title ?? '' }}</span>
                    {{--                        <select wire:model="raion"--}}
                    {{--                                class="w-full md:w-3/4 px-3 py-1 border rounded focus:border-blue-500 hover:border-blue-500 text-sm outline-none focus:input-shadow">--}}
                    {{--                            <option value="">Выберите район</option>--}}
                    {{--                            @foreach(\App\Models\Locations::where('type', \App\Models\Locations::LOCATION_RAYON)->where('parent',$oblast)->get() ?? []  as $item)--}}
                    {{--                                <option value="{{ $item->id }}">{{ $item->ru_title }}</option>--}}
                    {{--                            @endforeach--}}
                    {{--                        </select>--}}
                    {{--                        @error('raion')--}}
                    {{--                        <p class="text-xs text-red-600">{{ $message }}</p>--}}
                    {{--                        @enderror--}}
                </div>
            </div>
            {{--                @if($raion)--}}
            <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-center">
                <div class="w-full md:w-1/4">
                    <label for="gorod" class="w-full text-left md:text-right block text-sm">{{ __('admin.trainers.create.city') }}</label>
                </div>
                <div class="w-full md:w-3/4">
                    <span> {{ $trainer->gorodItem->ru_title ?? '' }}</span>
                    {{--                            <select wire:model="gorod"--}}
                    {{--                                    class="w-full md:w-3/4 px-3 py-1 border rounded focus:border-blue-500 hover:border-blue-500 text-sm outline-none focus:input-shadow">--}}
                    {{--                                <option value="">Выберите населенный пункт</option>--}}
                    {{--                                @foreach(\App\Models\Locations::where('type', \App\Models\Locations::LOCATION_AUL)->where('parent',$raion)->get() ?? []  as $item)--}}
                    {{--                                    <option value="{{ $item->id }}">{{ $item->ru_title }}</option>--}}
                    {{--                                @endforeach--}}
                    {{--                            </select>--}}
                    {{--                            @error('gorod')--}}
                    {{--                            <p class="text-xs text-red-600">{{ $message }}</p>--}}
                    {{--                            @enderror--}}
                </div>
            </div>
            <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-center">
                <div class="w-full md:w-1/4">
                    <label for="institute" class="w-full text-left md:text-right block text-sm">{{ __('admin.trainers.create.job') }}</label>
                </div>
                <div
                    class="w-full md:w-3/4" {{-- x-data x-init="institute = new SlimSelect({allowDeselect: true, select: $refs.institute, placeholder:'Select institute'});" wire:ignore --}} >
                    <span>{{ $trainer->instituteItem->ru_title ?? ''  }}</span>
                    {{--                            <select wire:model="trainer.institute"--}}
                    {{--                                    x-on:change="$wire.set('user.institute', institute.selected())" id="institute" x-ref="institute" --}}
                    {{--                                    class="w-full md:w-3/4 px-3 py-1 border rounded focus:border-blue-500 hover:border-blue-500 text-sm outline-none focus:input-shadow">--}}
                    {{--                                <option value="">Выберите место работы</option>--}}
                    {{--                                @foreach(\App\Models\Institutes::where('oblast', $oblast)->where('rayon',$raion)->get() ?? []  as $item)--}}
                    {{--                                    <option value="{{ $item->id }}">{{ $item->ru_title }}</option>--}}
                    {{--                                @endforeach--}}
                    {{--                            </select>--}}
                    {{--                            @error('trainer.institute')--}}
                    {{--                            <p class="text-xs text-red-600">{{ $message }}</p>--}}
                    {{--                            @enderror--}}
                </div>
            </div>
            {{--                @endif--}}
            {{--            @endif--}}
            <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-start">
                <div class="w-full md:w-1/4">
                    <label class="w-full text-left md:text-right block text-sm">{{ __('admin.trainers.create.program') }}</label>
                </div>
                <div class="flex flex-col w-full md:w-3/4">
                    {{--                    <button wire:click="add_courses_modal_open"--}}
                    {{--                            class="mb-2 text-sm flex w-48 justify-center items-center space-x-1.5 px-4 py-1 border-0 bg-blue-500 text-white rounded hover:bg-blue-600">--}}
                    {{--                        Добавить курсы--}}
                    {{--                    </button>--}}


                    <div class=" flex flex-col w-full md:w-3/4">
                        @foreach($trainer->courses as $item)
                            <div class="flex justify-between">
                                - {{ $item->ru_title ?? ''  }}
                                {{--                                <button wire:click="removeCourse({{ $item->id }})"--}}
                                {{--                                        class="text-red-500 hover:text-red-900 m-0 font-normal">x--}}
                                {{--                                </button>--}}
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>

            <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-center">
                <div class="w-full md:w-1/4">
                    <label for="academic_category" class="w-full text-left md:text-right block text-sm">{{ __('admin.trainers.create.category') }}</label>
                </div>
                <div class="w-full md:w-3/4">
                    <span>{{ $trainer->academicCategoryItem->ru_title ?? '' }}</span>
                    {{--                    <select wire:model="trainer.academic_category"--}}
                    {{--                            class="w-full md:w-3/4 px-3 py-1 border rounded focus:border-blue-500 hover:border-blue-500 text-sm outline-none focus:input-shadow">--}}
                    {{--                        <option value="">Выберите категорию</option>--}}
                    {{--                        @foreach(\App\Models\AcademicCategories::all() ?? []  as $item)--}}
                    {{--                            <option value="{{ $item->id }}">{{ $item->ru_title }}</option>--}}
                    {{--                        @endforeach--}}
                    {{--                    </select>--}}
                    {{--                    @error('trainer.academic_category')--}}
                    {{--                    <p class="text-xs text-red-600">{{ $message }}</p>--}}
                    {{--                    @enderror--}}
                </div>
            </div>
            <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-center">
                <div class="w-full md:w-1/4">
                    <label for="education" class="w-full text-left md:text-right block text-sm">{{ __('admin.trainers.create.level') }}</label>
                </div>
                <div class="w-full md:w-3/4">
                    <span>{{ $trainer->educationItem->ru_title ?? ''  }}</span>
                    {{--                    <select wire:model="trainer.education"--}}
                    {{--                            class="w-full md:w-3/4 px-3 py-1 border rounded focus:border-blue-500 hover:border-blue-500 text-sm outline-none focus:input-shadow">--}}
                    {{--                        <option value="">Выберите уровень образования</option>--}}
                    {{--                        @foreach(\App\Models\Educations::all() ?? []  as $item)--}}
                    {{--                            <option value="{{ $item->id }}">{{ $item->ru_title }}</option>--}}
                    {{--                        @endforeach--}}
                    {{--                    </select>--}}
                    {{--                    @error('trainer.education')--}}
                    {{--                    <p class="text-xs text-red-600">{{ $message }}</p>--}}
                    {{--                    @enderror--}}
                </div>
            </div>
            <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-center">
                <div class="w-full md:w-1/4">
                    <label for="academic_degree" class="w-full text-left md:text-right block text-sm">{{ __('admin.trainers.create.academic') }}</label>
                </div>
                <div class="w-full md:w-3/4">
                    <span>{{ $trainer->academicDegreeItem->ru_title ?? '' }}</span>
                    {{--                    <select wire:model="trainer.academic_degree"--}}
                    {{--                            class="w-full md:w-3/4 px-3 py-1 border rounded focus:border-blue-500 hover:border-blue-500 text-sm outline-none focus:input-shadow">--}}
                    {{--                        <option value="">Выберите ученную степень</option>--}}
                    {{--                        @foreach(\App\Models\AcademicDegrees::all() ?? []  as $item)--}}
                    {{--                            <option value="{{ $item->id }}">{{ $item->ru_title }}</option>--}}
                    {{--                        @endforeach--}}
                    {{--                    </select>--}}
                    {{--                    @error('trainer.academic_degree')--}}
                    {{--                    <p class="text-xs text-red-600">{{ $message }}</p>--}}
                    {{--                    @enderror--}}
                </div>
            </div>

            <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-center">
                <div class="w-full md:w-1/4">
                    <label for="subject_teach" class="w-full text-left md:text-right block text-sm">{{ __('admin.trainers.create.lesson') }}</label>
                </div>
                <div class="w-full md:w-3/4">
                    <span>{{ $trainer->teachSubjectItem->ru_title ?? 'Не выбрано' }}</span>
                    {{--                    <input wire:model="trainer.subject_teach" type="text" id="subject_teach"--}}
                    {{--                           class="w-full md:w-3/4 px-3 py-1 border rounded focus:border-blue-500 hover:border-blue-500 text-sm outline-none focus:input-shadow"/>--}}
                    {{--                    @error('trainer.subject_teach')--}}
                    {{--                    <p class="text-xs text-red-600">{{ $message }}</p>--}}
                    {{--                    @enderror--}}
                </div>
            </div>

            <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-center">
                <div class="w-full md:w-1/4">
                    <label for="stazh" class="w-full text-left md:text-right block text-sm">{{ __('admin.trainers.create.staj') }}</label>
                </div>
                <div class="w-full md:w-3/4">
                    <span>{{ $trainer->stazh }}</span>
                    {{--                    <input id="stazh" wire:model.defer="trainer.stazh"--}}
                    {{--                                   placeholder="Укажите педагогический стаж работы"--}}
                    {{--                                   class="w-full md:w-3/4 px-3 py-1 border rounded focus:border-blue-500 hover:border-blue-500 text-sm outline-none focus:input-shadow"/>--}}
                    {{--                    @error('trainer.stazh')--}}
                    {{--                    <p class="text-xs text-red-600">{{ $message }}</p>--}}
                    {{--                    @enderror--}}
                </div>
            </div>

            <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-center">
                <div class="w-full md:w-1/4">
                    <label for="iban" class="w-full text-left md:text-right block text-sm">{{ __('admin.trainers.create.iban') }}</label>
                </div>
                <div class="w-full md:w-3/4">
                    <span>{{ $trainer->iban }}</span>
                    {{--                    <input wire:model="trainer.iban" type="text" id="iban"--}}
                    {{--                           class="w-full md:w-3/4 px-3 py-1 border rounded focus:border-blue-500 hover:border-blue-500 text-sm outline-none focus:input-shadow"/>--}}
                    {{--                    @error('trainer.iban')--}}
                    {{--                    <p class="text-xs text-red-600">{{ $message }}</p>--}}
                    {{--                    @enderror--}}
                </div>
            </div>

            <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-center">
                <div class="w-full md:w-1/4">
                    <label for="bank_name" class="w-full text-left md:text-right block text-sm">{{ __('admin.trainers.create.bank') }}</label>
                </div>
                <div class="w-full md:w-3/4">
                    <span>{{ $trainer->bank_name }}</span>
                </div>
            </div>

            <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-center">
                <div class="w-full md:w-1/4">
                    <label for="attracted" class="w-full text-left md:text-right block text-sm">{{ __('admin.trainers.create.attracted') }}</label>
                </div>
                <div class="w-full md:w-3/4">
                    <span>{{ $trainer->attractedcatItem->ru_title }}</span>
                </div>
            </div>

            <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-center">
                <div class="w-full md:w-1/4">
                </div>
                <div class="w-full md:w-3/4 space-x-2 flex">
                    <a href="{{ route('ooup.trainers.index') }}"
                       class="py-1 px-4 border inline-block m-0 mb-2 text-sm rounded hover:border-black">{{ __('admin.buttons.cancel') }}</a>
                </div>
            </div>
        </div>
    </div>
</div>

