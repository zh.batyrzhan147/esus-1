<div>
    <x-slot name="header">
        <div class="bg-white mx-auto py-6 px-4 sm:px-6 lg:px-8">
            <div class="flex justify-between items-center w-full">
                <h2 class="font-semibold text-xl text-gray-800 leading-tight">
                    {{ __('Аудит') }}
                </h2>

                @include('admin.common.secondmenu', [
                    'tabname'=>'courses',
                    'popup'=>true,
                    'addlink'=>''
                ])
            </div>
        </div>
    </x-slot>

    <div class="bg-white mx-auto py-6 px-4 sm:px-6 lg:px-8">
        <div class="w-full flex justify-between items-center">
            <h2 class="font-semibold text-xl text-gray-800 leading-tight">
                {{ __('admin.reports.list.title') }}
            </h2>
            <button wire:click="add_new"
                    class="text-sm flex justify-center items-center space-x-1.5 px-4 py-1 border-0 bg-blue-500 text-white rounded hover:bg-blue-600">
                <svg width="14" height="14" viewBox="0 0 14 14" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path
                        d="M7.71838 1.62408H6.78088C6.69755 1.62408 6.65588 1.66575 6.65588 1.74908V6.65533H2C1.91667 6.65533 1.875 6.697 1.875 6.78033V7.71783C1.875 7.80117 1.91667 7.84283 2 7.84283H6.65588V12.7491C6.65588 12.8324 6.69755 12.8741 6.78088 12.8741H7.71838C7.80172 12.8741 7.84338 12.8324 7.84338 12.7491V7.84283H12.5C12.5833 7.84283 12.625 7.80117 12.625 7.71783V6.78033C12.625 6.697 12.5833 6.65533 12.5 6.65533H7.84338V1.74908C7.84338 1.66575 7.80172 1.62408 7.71838 1.62408Z"
                        fill="white"/>
                </svg>
                <span>Добавить</span>
            </button>
        </div>
    </div>
    <div class="py-6 px-4 sm:px-6 lg:px-8">
        <div class="flex flex-col">
            <div class="-my-2 overflow-x-auto sm:-mx-6 lg:-mx-8">
                <div class="py-2 align-middle inline-block min-w-full sm:px-6 lg:px-8">
                    <div class="shadow overflow-hidden border-b border-gray-200 sm:rounded">
                        <table class="min-w-full divide-y divide-gray-200 w-full">
                            <thead>
                            <tr>
                                <th scope="col"
                                    class="px-2 py-3 bg-gray-50 text-left text-sm font-medium text-gray-800">
                                    <div class="text-center">{{ __('admin.table.head.id') }}</div>
                                </th>
                                <th scope="col"
                                    class="px-2 py-3 bg-gray-50 text-left text-sm font-medium text-gray-800">
                                    <div class="pl-4">{{ __('Время запроса') }}</div>
                                </th>
                                <th scope="col"
                                    class="px-2 py-3 bg-gray-50 text-left text-sm font-medium text-gray-800">
                                    <div class="pl-4">{{ __('Год обучения') }}</div>
                                </th>
                                <th scope="col"
                                    class="px-2 py-3 bg-gray-50 text-left text-sm font-medium text-gray-800">
                                    <div class="pl-4">{{ __('Тип аудита') }}</div>
                                </th>
                                <th scope="col"
                                    class="px-2 py-3 bg-gray-50 text-left text-sm font-medium text-gray-800">
                                    <div class="pl-4">{{ __('Пользователь') }}</div>
                                </th>
                                <th scope="col"
                                    class="px-2 py-3 bg-gray-50 text-left text-sm font-medium text-gray-800">
                                    <div class="pl-4 border-l border-gray-200">{{ __('admin.table.head.fio') }}</div>
                                </th>
                                <th scope="col" width="200"
                                    class="px-2 py-3 bg-gray-50 text-left text-sm font-medium text-gray-800">
                                    <div class="pl-4 border-l border-gray-200">{{ __('admin.table.head.action') }}</div>
                                </th>
                            </tr>
                            </thead>
                            <tbody class="bg-white divide-y divide-gray-200">
                            @foreach ($audits as $item)
                                <tr>
                                    <td class="px-6 py-1.5 text-sm text-gray-900">
                                        {{ $item->id }}
                                    </td>
                                    <td class="px-6 py-1.5 text-sm text-gray-900">
                                        {{ $item->created_at }}
                                    </td>
                                    <td class="px-6 py-1.5 whitespace-nowrap text-sm text-gray-900">
                                        {{ $item->year }}
                                    </td>
                                    <td class="px-6 py-1.5 whitespace-nowrap text-sm text-gray-900">
                                    {{ \App\Models\AuditReports::listTypes()[$item->type] }}
                                    </td>
                                    <td class="px-6 py-1.5 text-sm text-gray-900">
                                        {{ $item->userItem->fio }}
                                    </td>
                                    <td class="px-6 py-1.5 whitespace-nowrap text-sm text-gray-900">
                                        {{ \App\Models\AuditReports::listStatuses()[$item->status] }}
                                    </td>
                                    <td class="px-6 py-1.5 whitespace-nowrap text-sm font-medium space-x-2">
                                        <a href="{{ route('ooup.audit.view', $item->id) }}"
                                           class="text-blue-500 hover:text-blue-900 m-0 font-normal">{{ __('admin.links.show') }}</a>
                                        @if($item->status != \App\Models\AuditReports::AUDIT_REPORT_STATUS_NEW)
                                        <button wire:click="$emit('triggerDelete',{{ $item->id }})"
                                                class="text-red-500 hover:text-red-900 bg-none border-0 m-0 cursor-pointer bg-transparent font-normal ">{{ __('admin.links.delete') }}</button>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="mt-4" wire:ignore>
                        {{ $audits->appends(request()->query())->links('admin.common.pagination') }}
                    </div>
                </div>
            </div>
        </div>
    </div>
    @if($modal_add_new)
        <x-modal>
            <x-slot name="header">
                Создать отчет:
                <br><br>
            </x-slot>
            <div class="space-y-2">
                <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-center">
                    <div class="w-full md:w-1/4">
                        <label for="type" class="w-full text-left md:text-right block text-sm">{{ __('Тип аудита') }}</label>
                    </div>
                    <div class="w-full md:w-3/4">
                        <select wire:model="reporttype" id="type"
                                class="w-full md:w-3/4 px-3 py-1 border rounded focus:border-blue-500 hover:border-blue-500 text-sm outline-none focus:input-shadow">
                            <option value>Выберите тип аудита</option>
                            @foreach(\App\Models\AuditReports::listTypes() as $key => $type)
                                <option value="{{ $key }}">{{ $type }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>

                @if($reporttype != null)
                    <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-center">
                        <div class="w-full md:w-1/4">
                            <label for="year"
                                   class="w-full text-left md:text-right block text-sm">{{ __('admin.groups.add.modal.year') }}</label>
                        </div>
                        <div class="w-full md:w-3/4">
                            <select wire:model="year"
                                    class="w-full md:w-3/4 px-3 py-1 border rounded focus:border-blue-500 hover:border-blue-500 text-sm outline-none focus:input-shadow">
                                <option value>{{ __('admin.groups.add.modal.chooseyear') }}</option>
                                @foreach(\App\Models\Years::orderBy('year','ASC')->get() as $item)
                                    <option value="{{ $item->year }}">{{ $item->year }}</option>
                                @endforeach
                            </select>
                            @error('year')
                            <p class="text-xs text-red-600">{{ $message }}</p>
                            @enderror
                        </div>
                    </div>
                @endif

                <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-center">
                    <div class="w-full md:w-1/4">
                    </div>
                    <div class="w-full md:w-3/4 space-x-2 flex">
                        @if($reporttype != null)
                            <button wire:click="add_report"
                                    class="mb-2 text-sm flex justify-center items-center px-4 py-1 border-0 bg-blue-500 text-white rounded hover:bg-blue-600">
                                Добавить
                            </button>
                        @endif
                        <button wire:click="$set('modal_add_new',false)"
                                class="py-1 px-4 border inline-block m-0 mb-2 text-sm rounded hover:border-black">
                            Отмена
                        </button>
                    </div>
                </div>
            </div>
        </x-modal>
    @endif
</div>


@push('scripts')
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>

    <script type="text/javascript">
        document.addEventListener('DOMContentLoaded', function () {
        @this.on('triggerSuccessCreate', () => {
            Swal.fire({
                title: '{{ __("admin.swal.lang.title.success") }}',
                icon: 'success',
                width: 600,
                position: 'top-end',
                showConfirmButton: false,
                timer: 1000
            });
        });

        @this.on('triggerDelete', itemID => {
            console.log(itemID);

            Swal.fire({
                title: '{{ __("admin.swal.temp.title") }}',
                text: '{{ __("") }}',
                icon: "warning",
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#aaa',
                cancelButtonText: '{{ __("admin.swal.temp.buttons.cancel") }}',
                confirmButtonText: '{{ __("admin.swal.temp.buttons.approve") }}'
            }).then((result) => {
                //if user clicks on delete
                if (result.value) {
                    // calling destroy method to delete
                @this.call('destroy',itemID);
                    // success response
                    Swal.fire({title: '{{ __("admin.swal.temp.deleted.title") }}', icon: 'success',
                        width: 600,
                        position: 'top-end',
                        showConfirmButton: false,
                        timer: 1000});
                } else {
                    Swal.fire({
                        title: '{{ __("admin.swal.temp.discarded.title") }}',
                        icon: 'success',
                        width: 600,
                        position: 'top-end',
                        showConfirmButton: false,
                        timer: 1000
                    });
                }
            });
        });
        })
    </script>
@endpush
