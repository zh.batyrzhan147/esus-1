<div>
    <x-slot name="header">
        <div class="bg-white mx-auto py-6 px-4 sm:px-6 lg:px-8">
            <div class="flex justify-between items-center w-full">
                <h2 class="font-semibold text-xl text-gray-800 leading-tight">
                    {{ __('admin.certificates.index.title') }}
                </h2>

                @include('admin.common.secondmenu', [
                    'tabname'=>'courses',
                    'popup'=>true,
                    'addlink'=>''
                ])
            </div>
        </div>
    </x-slot>
    <div class="flex flex-col mx-auto py-2 px-4 sm:px-6 lg:px-8">
        @include('livewire.admin.certificates.partial.certificates-list-filters')
    </div>

    <div class="py-6 px-4 sm:px-6 lg:px-8">
        <div class="flex flex-col">
            <div class="-my-2 overflow-x-auto sm:-mx-6 lg:-mx-8">
                <div class="py-2 align-middle inline-block min-w-full sm:px-6 lg:px-8">
                    <div class="shadow overflow-hidden border-b border-gray-200 sm:rounded">
                        <table class="min-w-full divide-y divide-gray-200 w-full">
                            <thead>
                            <tr>
                                <th scope="col"
                                    class="px-2 py-3 bg-gray-50 text-left text-sm font-medium text-gray-800">
                                    <div class="text-center">{{ __('admin.table.head.id') }}</div>
                                </th>
                                <th scope="col"
                                    class="px-2 py-3 bg-gray-50 text-left text-sm font-medium text-gray-800">
                                    <div class="pl-4">{{ __('Тип сертификата') }}</div>
                                </th>
                                <th scope="col"
                                    class="px-2 py-3 bg-gray-50 text-left text-sm font-medium text-gray-800">
                                    <div class="pl-4">{{ __('admin.table.head.group') }}</div>
                                </th>
                                <th scope="col"
                                    class="px-2 py-3 bg-gray-50 text-left text-sm font-medium text-gray-800">
                                    <div class="pl-4 border-l border-gray-200">{{ __('admin.table.head.fio') }}</div>
                                </th>
                                <th scope="col" width="200"
                                    class="px-2 py-3 bg-gray-50 text-left text-sm font-medium text-gray-800">
                                    <div class="pl-4 border-l border-gray-200">{{ __('admin.table.head.action') }}</div>
                                </th>
                            </tr>
                            </thead>
                            <tbody class="bg-white divide-y divide-gray-200">
                            @foreach ($certificates as $item)
                                <tr>
                                    <td class="px-6 py-1.5 text-sm text-gray-900">
                                        {{ $item->uuid }}
                                    </td>
                                    <td class="px-6 py-1.5 text-sm text-gray-900">
                                        {{ \App\Models\Certificates::listTypes()[$item->type] }}
                                    </td>
                                    <td class="px-6 py-1.5 text-sm text-gray-900">
                                        {{ $item->group_title }}
                                    </td>
                                    <td class="px-6 py-1.5 whitespace-nowrap text-sm text-gray-900">
                                        {{ $item->fio }}
                                    </td>
                                    <td class="px-6 py-1.5 whitespace-nowrap text-sm font-medium space-x-2">
                                        <a href="{{ route('ooup.certificates.show', $item->id) }}"
                                           class="text-blue-500 hover:text-blue-900 m-0 font-normal">{{ __('admin.links.show') }}</a>
                                        <button wire:click="$emit('triggerDelete',{{ $item->id }})"
                                                class="text-red-500 hover:text-red-900 bg-none border-0 m-0 cursor-pointer bg-transparent font-normal ">{{ __('admin.links.delete') }}</button>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="mt-4">
                        {{ $certificates->appends(request()->query())->links('admin.common.pagination') }}
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>

@push('scripts')
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>

    <script type="text/javascript">
        document.addEventListener('DOMContentLoaded', function () {
        @this.on('triggerDelete', itemID => {
            console.log(itemID);

            Swal.fire({
                title: '{{ __("admin.swal.temp.title") }}',
                text: '{{ __("") }}',
                icon: "warning",
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#aaa',
                cancelButtonText: '{{ __("admin.swal.temp.buttons.cancel") }}',
                confirmButtonText: '{{ __("admin.swal.temp.buttons.approve") }}'
            }).then((result) => {
                //if user clicks on delete
                if (result.value) {
                    // calling destroy method to delete
                @this.call('destroy',itemID);
                    // success response
                    Swal.fire({title: '{{ __("admin.swal.temp.deleted.title") }}', icon: 'success',
                        width: 600,
                        position: 'top-end',
                        showConfirmButton: false,
                        timer: 1000});
                } else {
                    Swal.fire({
                        title: '{{ __("admin.swal.temp.discarded.title") }}',
                        icon: 'success',
                        width: 600,
                        position: 'top-end',
                        showConfirmButton: false,
                        timer: 1000
                    });
                }
            });
        });
        })
    </script>
@endpush
