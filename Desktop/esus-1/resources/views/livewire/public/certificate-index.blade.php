<div>
    <div class="rounded w-full bg-[#FFFFFF] px-4 mb-5">
        <div class="flex flex-1 flex-col w-full">
            <div class="flex flex-1 w-full bg-white overflow-y-auto paragraph px-4">
                <div class="w-full flex flex-col">
                    <div class="w-full flex text-xl text-black font-medium">&nbsp;</div>
                    <div class="flex w-full border-b-2 border-[#F4F4F5] space-x-2 items-end mb-5">
                        <div class="text-5xl font-bold text-black p-2.5">{{ __('certificate.verify.title') }}</div>
                    </div>
                    <div class="w-[896px] p-2.5 flex flex-col mx-auto">
                        <div class="text-center text-xl text-gray-900 mb-5">
                            {{ __('certificate.verify.description') }}
                        </div>
                        <div class="flex flex-col md:flex-row md:space-x-4 space-y-2 md:space-y-0 justify-center mb-3">
                            <div>
                                <input wire:model="uuid" placeholder="{{ __('certificate.verify.input.placeholder') }}" class="text-xl w-full px-3 py-1 border rounded focus:border-blue-500 placeholder:text-sm hover:border-blue-500 text-sm outline-none focus:input-shadow"/>
                            </div>
                            <button wire:click="verifyuuid" class="mb-2 whitespace-nowrap text-sm flex justify-center items-center space-x-1.5 px-4 py-1 border-0 bg-blue-500 text-white rounded hover:bg-blue-600">Проверить сертификат</button>
                        </div>
                        <div class="text-sm flex justify-center text-red-600">
                        @error('uuid')
                        {{ $message }}
                        @enderror
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


</div>
