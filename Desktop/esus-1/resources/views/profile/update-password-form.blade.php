<div>
    <div class="w-full lg:min-w-xs">
        <form wire:submit.prevent="updatePassword" class="w-full">
            <div class="flex flex-col justify-center space-y-6">
                <div class="w-full">
                    <input id="current_password" type="password" wire:model.defer="state.current_password" autocomplete="current-password" placeholder="{{ __('listener.profile.modal.password.change.password') }}"
                           class="w-full p-4 text-base text-main-black border border-soft-gray bg-gray-bg rounded-lg focus:border-logo-blue hover:border-main-black focus:hover:border-logo-blue outline-0 focus:outline-0 placeholder:text-gray-400"/>
                    <x-jet-input-error for="current_password" class="mt-2" />
                </div>

                <div class="w-full">
                    <input id="password" type="password" wire:model.defer="state.password" autocomplete="new-password" placeholder="{{ __('listener.profile.modal.password.change.newpassword') }}"
                           class="w-full p-4 text-base text-main-black border border-soft-gray bg-gray-bg rounded-lg focus:border-logo-blue hover:border-main-black focus:hover:border-logo-blue outline-0 focus:outline-0 placeholder:text-gray-400"/>
                    <x-jet-input-error for="password" class="mt-2" />
                </div>

                <div class="w-full">
                    <input id="password_confirmation" type="password" wire:model.defer="state.password_confirmation" autocomplete="new-password" placeholder="{{ __('listener.profile.modal.password.change.newpassword2') }}"
                           class="w-full p-4 text-base text-main-black border border-soft-gray bg-gray-bg rounded-lg focus:border-logo-blue hover:border-main-black focus:hover:border-logo-blue outline-0 focus:outline-0 placeholder:text-gray-400"/>
                    <x-jet-input-error for="password_confirmation" class="mt-2" />
                </div>

                <x-jet-action-message class="mr-3" on="saved">
                    {{ __('listener.profile.modal.password.change.success') }}
                </x-jet-action-message>
                <button type="submit"
                        class="w-full py-4 px-8 border border-primary-button bg-primary-button rounded-lg text-white text-base hover:bg-logo-blue hover:border-logo-blue mx-auto">{{ __('listener.buttons.change') }}</button>
            </div>
        </form>
    </div>
</div>
