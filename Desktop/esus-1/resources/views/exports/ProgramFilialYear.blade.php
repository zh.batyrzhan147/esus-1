<style>

</style>
<table style="height:auto; width:100%; margin:0px auto;" border=1>
    <tbody>
        <tr>
           <td colspan="{{ 3 + $years->count() }}"  style="table-layout: auto;border: 1px solid;overflow:auto; max-height:100px;">
                {{ $course->ru_title }}
           </td>
        </tr>
        <tr>
            <td style="font-weight: bold">
                #
            </td>
            <td style="font-weight: bold">
                Место проведения курсов
            </td>
            @foreach($years as $year)
                <td style="font-weight: bold">
                    {{ $year->year }}
                </td>
            @endforeach
            <td style="font-weight: bold">
                Всего
            </td>
        </tr>
        @foreach($affiliates as $affiliate)
            @php
                $count = 0;
            @endphp
            <tr>
                <td>
                    {{ $loop->index + 1 }}
                </td>
                <td>
                    {{ $table[$affiliate->id]['title'] }}
                </td>
                @foreach($years as $year)
                    @php
                        $count = $count + $table[$affiliate->id][$year->id]['count'];
                    @endphp
                    <td>
                        {{ $table[$affiliate->id][$year->id]['count'] }}
                    </td>
                @endforeach
                <td style="font-weight:bold">
                    {{ $table['total'][0][$affiliate->id] }}
                </td>
            </tr>
        @endforeach
            <tr>
               <td colspan="2">
                    Итого:
                </td>

                @foreach($years as $year)
                    <td style="font-weight:bold">
                        {{ $table['total'][$year->id][0] }}
                    </td>
                @endforeach
                <td style="font-weight:bold">
                    {{ $table['total'][0][0] }}
                </td>
            </tr>
    </tbody>
</table>
