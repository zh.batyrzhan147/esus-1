<table>
    <tbody>
        <tr>
            <td colspan="10" style="text-align: right;">
                Қосымша 2
            </td>
        </tr>
    </tbody>
</table>
@if(count($table) > 0)
{{--    @foreach($table as $group)--}}
        <table class="table-fixed w-full">
            <thead class="bg-[#F4F4F5] border-b-2 border-[#D4D4D8]">
            <tr>
                <th style="border: 2px solid black;" scope="col"
                    class="w-full text-left font-normal text-[15px] text-[#18181B] py-2.5 px-4">
                    №
                </th>
                <th style="border: 2px solid black;" scope="col"
                    class="w-full text-left font-normal text-[15px] text-[#18181B] py-2.5 px-4">
                    Аты-жөні
                </th>
                <th style="border: 2px solid black;" scope="col"
                    class="w-full text-left font-normal text-[15px] text-[#18181B] py-2.5 px-4">
                    ЖСН
                </th>
                <th style="border: 2px solid black;" scope="col"
                    class="w-full text-left font-normal text-[15px] text-[#18181B] py-2.5 px-4">
                    Бағдарлама
                </th>
                <th style="border: 2px solid black;" scope="col"
                    class="w-full text-left font-normal text-[15px] text-[#18181B] py-2.5 px-4">
                    Топ №
                </th>
                <th style="border: 2px solid black;" scope="col"
                    class="w-full text-left font-normal text-[15px] text-[#18181B] py-2.5 px-4">
                    ЦПМ/Филиал ЦПМ
                </th>
                <th style="border: 2px solid black;" scope="col"
                    class="w-full text-left font-normal text-[15px] text-[#18181B] py-2.5 px-4">
                    Оқу периоды
                </th>
                <th style="border: 2px solid black;" scope="col"
                    class="w-full text-left font-normal text-[15px] text-[#18181B] py-2.5 px-4">
                    Оқу тілі
                </th>
                <th style="border: 2px solid black;" scope="col"
                    class="w-full text-left font-normal text-[15px] text-[#18181B] py-2.5 px-4">
                    Тренердің аты-жөні
                </th>
                <th style="border: 2px solid black;" scope="col"
                    class="w-full text-left font-normal text-[15px] text-[#18181B] py-2.5 px-4">
                    Бағаланған күн
                </th>
            </tr>
            </thead>
            <tbody>
            @foreach($table['users'] as $user)
                <tr class="bg-[#FFFFFF] hover:bg-[#F4F4F5] hover:cursor-pointer border-b-2 border-[#D4D4D8]">
                    <td style="border: 2px solid black;" > {{ $user['number'] }}</td>
                    <td style="border: 2px solid black;" > {{ $user['fio'] }}</td>
                    <td style="border: 2px solid black;" > {{ $user['iin'] }}</td>
                    <td style="border: 2px solid black;" > {{ $user['courseskz'] }}</td>
                    <td style="border: 2px solid black;" > {{ $user['slug'] }}</td>
                    <td style="border: 2px solid black;" > {{ $user['affiliate'] }}</td>
                    <td style="border: 2px solid black;" > {{ $user['period'] }}</td>
                    <td style="border: 2px solid black;" > {{ $user['languagekz'] }}</td>
                    <td style="border: 2px solid black;" > {{ $user['trainer'] }}</td>
                    <td style="border: 2px solid black;" > {{ $user['lastdate'] }}</td>
                </tr>
            @endforeach
                <tr class="bg-[#FFFFFF] hover:bg-[#F4F4F5] hover:cursor-pointer border-b-2 border-[#D4D4D8]">
                    <td style="border: 2px solid black;" colspan="2"><b>Барлығы:</b></td>
                    <td style="border: 2px solid black;text-align: left;" colspan="8" ><b>{{ count($table['users']) }}</b></td>
                </tr>
            </tbody>
        </table>
{{--    @endforeach--}}
@else
    <table class="table-fixed w-full">
        <tbody>
        <tr>
            <td>Белгіленген критериймен топ табылмады!</td>
        </tr>
        </tbody>
    </table>
@endif
