<table style="height:auto; width:100%; margin:0px auto;" border=1>
    <tbody >
        <tr>
            <td>#</td>
            <td>Шифр группы</td>
            <td>ФИО тренера</td>
            <td>Филлиал</td>
            <td>Название курса</td>
            <td>Язык обучения</td>
            <td>Год обучения</td>
            <td>Сдали</td>
            <td>Не сдали</td>
            <td>Всего слушателей</td>
            <td>Процент сдавших(%)</td>
        </tr>
        @foreach($table as $item)
            <tr>
                <td>{{ $loop->index + 1 }}</td>
                <td>{{ $item['slug'] }}</td>
                <td>{{ $item['trainer'] }}</td>
                <td>{{ $item['affiliate'] }}</td>
                <td>{{ $item['course'] }}</td>
                <td>{{ $item['language'] }}</td>
                <td>{{ $item['year'] }}</td>
                <td>{{ $item['passed'] }}</td>
                <td>{{ $item['not_passed'] }}</td>
                <td>{{ $item['total'] }}</td>
                <td>{{ $item['percentage'] }}</td>
            </tr>
        @endforeach
    </tbody>
</table>
