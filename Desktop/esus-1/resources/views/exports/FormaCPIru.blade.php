<table>
    <tbody>
        <tr>
            <td colspan="10" style="text-align: right;">
                Приложение 2
            </td>
        </tr>
    </tbody>
</table>
@if(count($table) > 0)
{{--    @foreach($table as $group)--}}
        <table class="table-fixed w-full">
            <thead class="bg-[#F4F4F5] border-b-2 border-[#D4D4D8]">
            <tr>
                <th style="border: 2px solid black;" scope="col"
                    class="w-full text-left font-normal text-[15px] text-[#18181B] py-2.5 px-4">
                    №
                </th>
                <th style="border: 2px solid black;" scope="col"
                    class="w-full text-left font-normal text-[15px] text-[#18181B] py-2.5 px-4">
                    Ф.И.О.
                </th>
                <th style="border: 2px solid black;" scope="col"
                    class="w-full text-left font-normal text-[15px] text-[#18181B] py-2.5 px-4">
                    ИИН
                </th>
                <th style="border: 2px solid black;" scope="col"
                    class="w-full text-left font-normal text-[15px] text-[#18181B] py-2.5 px-4">
                    Программа
                </th>
                <th style="border: 2px solid black;" scope="col"
                    class="w-full text-left font-normal text-[15px] text-[#18181B] py-2.5 px-4">
                    № группы
                </th>
                <th style="border: 2px solid black;" scope="col"
                    class="w-full text-left font-normal text-[15px] text-[#18181B] py-2.5 px-4">
                    ЦПМ/Филиал ЦПМ
                </th>
                <th style="border: 2px solid black;" scope="col"
                    class="w-full text-left font-normal text-[15px] text-[#18181B] py-2.5 px-4">
                    Период обучения
                </th>
                <th style="border: 2px solid black;" scope="col"
                    class="w-full text-left font-normal text-[15px] text-[#18181B] py-2.5 px-4">
                    Язык обучения
                </th>
                <th style="border: 2px solid black;" scope="col"
                    class="w-full text-left font-normal text-[15px] text-[#18181B] py-2.5 px-4">
                    Ф.И.О. тренера
                </th>
                <th style="border: 2px solid black;" scope="col"
                    class="w-full text-left font-normal text-[15px] text-[#18181B] py-2.5 px-4">
                    Дата оценивания
                </th>
            </tr>
            </thead>
            <tbody>
            @foreach($table['users'] as $user)
                <tr class="bg-[#FFFFFF] hover:bg-[#F4F4F5] hover:cursor-pointer border-b-2 border-[#D4D4D8]">
                    <td style="border: 2px solid black;" > {{ $user['number'] }}</td>
                    <td style="border: 2px solid black;" > {{ $user['fio'] }}</td>
                    <td style="border: 2px solid black;" > {{ $user['iin'] }}</td>
                    <td style="border: 2px solid black;" > {{ $user['courses'] }}</td>
                    <td style="border: 2px solid black;" > {{ $user['slug'] }}</td>
                    <td style="border: 2px solid black;" > {{ $user['affiliate'] }}</td>
                    <td style="border: 2px solid black;" > {{ $user['period'] }}</td>
                    <td style="border: 2px solid black;" > {{ $user['language'] }}</td>
                    <td style="border: 2px solid black;" > {{ $user['trainer'] }}</td>
                    <td style="border: 2px solid black;" > {{ $user['lastdate'] }}</td>
                </tr>
            @endforeach
                <tr class="bg-[#FFFFFF] hover:bg-[#F4F4F5] hover:cursor-pointer border-b-2 border-[#D4D4D8]">
                    <td style="border: 2px solid black;" colspan="2"><b>Всего:</b></td>
                    <td style="border: 2px solid black;text-align: left;" colspan="8" ><b>{{ count($table['users']) }}</b></td>
                </tr>
            </tbody>
        </table>
{{--    @endforeach--}}
@else
    <table class="table-fixed w-full">
        <tbody>
        <tr>
            <td>Группы по этим критериям не найдены</td>
        </tr>
        </tbody>
    </table>
@endif
