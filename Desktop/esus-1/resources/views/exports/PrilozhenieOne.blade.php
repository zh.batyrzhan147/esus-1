<link rel="stylesheet" href="{{ mix('css/app.css') }}">
<table>
    <tbody>
    <tr>
        <td colspan="11" style="text-align:right;">
            Приложение 1
        </td>
    </tr>
    <tr>
        <td colspan="11" style="text-align:right;">
            к приказу директора частного учреждения
        </td>
    </tr>
    <tr>
        <td colspan="11" style="text-align:right;">
            «Центр педагогического мастерства»
        </td>
    </tr>
    <tr>
        <td colspan="11" style="text-align:right;">
            от « ______» _______________2022 г.
        </td>
    </tr>
    <tr>
        <td colspan="11" style="text-align:right;">
            №______________
        </td>
    </tr>
    </tbody>
</table>

<table>
    <tbody>
    <tr>
        <td colspan="11" rowspan="3">
            <p>График проведения и список тренеров курсов по образовательным программам повышения квалификации
                педагогических кадров в рамках обновления содержания среднего образования, разработанным частным
                учреждением «Центр педагогического мастерства» (далее -Учреждение), в рамках оказания платных
                образовательных услуг </p>
        </td>
    </tr>
    </tbody>
</table>
<br>

@if(count($table) > 0)
    <table class="table-fixed w-full border-2 border-[#000000]">
        <tbody>

        <tr>
            <td rowspan="2" style="border: 2px solid black;text-align:center;">№</td>
            <td rowspan="2" style="border: 2px solid black;text-align:center;">Программы</td>
            <td rowspan="2" style="border: 2px solid black;text-align:center;">Период обучения</td>
            <td rowspan="2" style="border: 2px solid black;text-align:center;">Шифр группы</td>
            <td rowspan="2" style="border: 2px solid black;text-align:center;">Количество слушателей</td>
            <td rowspan="2" style="border: 2px solid black;text-align:center;">Язык обучения</td>
            <td colspan="4" style="border: 2px solid black;text-align:center;" rowspan="1">Тренер</td>
            <td colspan="1" style="border: 2px solid black;text-align:center;" rowspan="1">Учебная нагрузка</td>

        </tr>
        <tr>
            <td  style="border: 2px solid black;text-align:center;">ФИО</td>
            <td  style="border: 2px solid black;text-align:center;">Место работы (полностью)</td>
            <td  style="border: 2px solid black;text-align:center;">Должность</td>
            <td  style="border: 2px solid black;text-align:center;">категория по найму</td>
            <td  style="border: 2px solid black;text-align:center;">ак.часов</td>
        </tr>
        </tbody>
    </table>
    @foreach($table['affiliates'] ?? [] as $affiliate)
        <table class="table-fixed w-full">
            <tbody style="border: 2px solid black;">
            <tr>
                <td colspan="11" style="border: 2px solid black;text-align:center;">
                    <p>{{ $table['title'][$affiliate] ?? '' }}</p>
                </td>
            </tr>
            @php
                $sum1 = 0;
                $sum2 = 0;
            @endphp
            @foreach($table[$affiliate] as $group)
                <tr class="bg-[#FFFFFF] hover:bg-[#F4F4F5] hover:cursor-pointer border-b-2 border-[#D4D4D8]">
                    <td style="border: 2px solid black;"
                        class="w-full text-left font-light text-[#3A8CF7] py-2.5 px-4"> {{ $loop->index + 1 }} </td>
                    <td style="border: 2px solid black;"
                        class="w-full text-left font-light text-[#3A8CF7] py-2.5 px-4"> {{ $group['courses'] }} </td>
                    <td style="border: 2px solid black;"
                        class="w-full text-left font-light text-[#3A8CF7] py-2.5 px-4"> {{ $group['period'] }} </td>
                    <td style="border: 2px solid black;"
                        class="w-full text-left font-light text-[#3A8CF7] py-2.5 px-4"> {{ $group['slug'] }} </td>
                    <td style="border: 2px solid black;"
                        class="w-full text-left font-light text-[#3A8CF7] py-2.5 px-4"> {{ $group['count'] }} </td>
                    <td style="border: 2px solid black;"
                        class="w-full text-left font-light text-[#3A8CF7] py-2.5 px-4"> {{ $group['language'] }} </td>
                    <td style="border: 2px solid black;"
                        class="w-full text-left font-light text-[#3A8CF7] py-2.5 px-4"> {{ $group['trainer_fio'] }} </td>
                    <td style="border: 2px solid black;"
                        class="w-full text-left font-light text-[#3A8CF7] py-2.5 px-4"> {{ $group['trainer_work'] }} </td>
                    <td style="border: 2px solid black;"
                        class="w-full text-left font-light text-[#3A8CF7] py-2.5 px-4"> {{ $group['trainer_jobname'] }} </td>
                    <td style="border: 2px solid black;"
                        class="w-full text-left font-light text-[#3A8CF7] py-2.5 px-4"> {{ $group['trainer_attractedcat'] }} </td>
                    <td style="border: 2px solid black;"
                        class="w-full text-left font-light text-[#3A8CF7] py-2.5 px-4"> {{ $group['course_hours'] }} </td>
                </tr>
                @php
                    $sum1 = $sum1 + intval($group['count']);
                    $sum2 = $sum2 + intval($group['course_hours']);
                @endphp
            @endforeach
            <tr class="bg-[#000000] hover:bg-[#F4F4F5] hover:cursor-pointer border-b-2 border-[#D4D4D8]">
                <td style="border: 2px solid black;"></td>
                <td style="border: 2px solid black;" colspan="3"><b>Всего:</b></td>
                <td style="border: 2px solid black;text-align: right;">@if(intval($sum1) == 0) 0 @else {{ $sum1 }} @endif</td>
                <td style="border: 2px solid black;" colspan="5"></td>
                <td style="border: 2px solid black;text-align: right;" >{{ intval($sum2) }}</td>
            </tr>
            </tbody>
        </table>
    @endforeach
@else
    <table class="table-fixed w-full">
        <tbody>
        <tr>
            <td>Группы по этим критериям не найдены</td>
        </tr>
        </tbody>
    </table>
@endif
