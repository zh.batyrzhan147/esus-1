@if(count($table) > 0)
    @foreach($table as $group)
        <table class="table-fixed w-full border-2 border-[#000000]">
            <tbody>
            <tr>
                <td colspan="7" style="text-align:center;">{!!  $group['courseskz']  !!}</td>
            </tr>
            <tr>
                <td colspan="7" style="text-align:center;">{{ $group['period'] }}</td>
            </tr>
            <tr>
                <td colspan="7" style="text-align:center;">{{ $group['format'] }}</td>
            </tr>
            <tr>
                <td colspan="7" style="text-align:center;">{{ $group['affiliate'] }}</td>
            </tr>
            </tbody>
        </table>
        <table class="table-fixed w-full">
            <thead class="bg-[#F4F4F5] border-b-2 border-[#D4D4D8]">
            <tr>
                <th style="border: 2px solid black;" scope="col"
                    class="w-full text-left font-normal text-[15px] text-[#18181B] py-2.5 px-4">
                    №
                </th>
                <th style="border: 2px solid black;" scope="col"
                    class="w-full text-left font-normal text-[15px] text-[#18181B] py-2.5 px-4">
                    Аты-жөні
                </th>
                <th style="border: 2px solid black;" scope="col"
                    class="w-full text-left font-normal text-[15px] text-[#18181B] py-2.5 px-4">
                    Жұмыс орны
                </th>
                <th style="border: 2px solid black;" scope="col"
                    class="w-full text-left font-normal text-[15px] text-[#18181B] py-2.5 px-4">
                    Лауазымы
                </th>
                <th style="border: 2px solid black;" scope="col"
                    class="w-full text-left font-normal text-[15px] text-[#18181B] py-2.5 px-4">
                    Оқу тілі
                </th>
                <th style="border: 2px solid black;" scope="col"
                    class="w-full text-left font-normal text-[15px] text-[#18181B] py-2.5 px-4">
                    Тренер
                </th>
                <th style="border: 2px solid black;" scope="col"
                    class="w-full text-left font-normal text-[15px] text-[#18181B] py-2.5 px-4">
                    Топ
                </th>
            </tr>
            </thead>
            <tbody style="border: 2px solid black;">
            @foreach($group['users'] as $user)
                <tr class="bg-[#FFFFFF] hover:bg-[#F4F4F5] hover:cursor-pointer border-b-2 border-[#D4D4D8]">
                    <td style="border: 2px solid black;" class="w-full text-left font-light text-[#3A8CF7] py-2.5 px-4">{{ $user['number'] }}</td>
                    <td style="border: 2px solid black;" class="w-full text-left font-light text-[#3A8CF7] py-2.5 px-4">{{ $user['fio'] }}</td>
                    <td style="border: 2px solid black;" class="w-full text-left font-light text-[#3A8CF7] py-2.5 px-4">{{ $user['working'] }}</td>
                    <td style="border: 2px solid black;" class="w-full text-left font-light text-[#3A8CF7] py-2.5 px-4">{{ $user['jobnamekz'] }}</td>
                    <td style="border: 2px solid black;" class="w-full text-left font-light text-[#3A8CF7] py-2.5 px-4">{{ $user['languagekz'] }}</td>
                    <td style="border: 2px solid black;" class="w-full text-left font-light text-[#3A8CF7] py-2.5 px-4">{{ $user['trainer'] }}</td>
                    <td style="border: 2px solid black;" class="w-full text-left font-light text-[#3A8CF7] py-2.5 px-4">{{ $user['group'] }}</td>
                </tr>
            @endforeach
                <tr class="bg-[#FFFFFF] hover:bg-[#F4F4F5] hover:cursor-pointer border-b-2 border-[#D4D4D8]">
                    <td style="border: 2px solid black;" colspan="2"><b>Барлығы:</b></td>
                    <td style="border: 2px solid black;text-align: left;" colspan="5" ><b>{{ count($group['users']) }}</b></td>
                </tr>
            </tbody>
        </table>
    @endforeach
@else
    <table class="table-fixed w-full">
        <tbody>
        <tr>
            <td>Ондай топ табылмады</td>
        </tr>
        </tbody>
    </table>
@endif
