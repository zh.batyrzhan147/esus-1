<table>
    <tbody>
        <tr>
            <td colspan="7" rowspan="5" style="text-align:right;">
                <p>Приложение</p>
                <p>к приказу директора частного учреждения</p>
                <p>«Центр педагогического мастерства»</p>
                <p>от « ______» _______________2022 г.</p>
                <p>№______________</p>
            </td>
        </tr>
    </tbody>
</table>
<br>
<br>
<br>
<br>
<table>
    <tbody>
    <tr>
        <td colspan="7" rowspan="3">
            <p>Список слушателей, завершивших курсы по образовательным программам повышения квалификации педагогов по развитию предметных компетенций </p>
            <p>учителей «Урок в школе: фокусы и стратегии улучшений»,  разработанным частным учреждением «Центр педагогического мастерства» (далее – Учреждение)</p>
        </td>
    </tr>
    </tbody>
</table>
<br>

@if(count($table) > 0)
    @foreach($table as $group)
        <table class="table-fixed w-full border-2 border-[#000000]">
            <tbody>
            <tr>
                <td colspan="7" style="text-align:center;">{!!  $group['courses']  !!}</td>
            </tr>
            <tr>
                <td colspan="7" style="text-align:center;">{{ $group['period'] }}</td>
            </tr>
            <tr>
                <td colspan="7" style="text-align:center;">{{ $group['format'] }}</td>
            </tr>
            <tr>
                <td colspan="7" style="text-align:center;">{{ $group['affiliate'] }}</td>
            </tr>
            </tbody>
        </table>
        <table class="table-fixed w-full">
            <thead class="bg-[#F4F4F5] border-b-2 border-[#D4D4D8]">
            <tr>
                <th style="border: 2px solid black;" scope="col"
                    class="w-full text-left font-normal text-[15px] text-[#18181B] py-2.5 px-4">
                    №
                </th>
                <th style="border: 2px solid black;" scope="col"
                    class="w-full text-left font-normal text-[15px] text-[#18181B] py-2.5 px-4">
                    Ф.И.О.
                </th>
                <th style="border: 2px solid black;" scope="col"
                    class="w-full text-left font-normal text-[15px] text-[#18181B] py-2.5 px-4">
                    Место работы
                </th>
                <th style="border: 2px solid black;" scope="col"
                    class="w-full text-left font-normal text-[15px] text-[#18181B] py-2.5 px-4">
                    Должность
                </th>
                <th style="border: 2px solid black;" scope="col"
                    class="w-full text-left font-normal text-[15px] text-[#18181B] py-2.5 px-4">
                    Язык обучения
                </th>
                <th style="border: 2px solid black;" scope="col"
                    class="w-full text-left font-normal text-[15px] text-[#18181B] py-2.5 px-4">
                    Тренер
                </th>
                <th style="border: 2px solid black;" scope="col"
                    class="w-full text-left font-normal text-[15px] text-[#18181B] py-2.5 px-4">
                    Группа
                </th>
            </tr>
            </thead>
            <tbody style="border: 2px solid black;">
            @foreach($group['users'] as $user)
                <tr class="bg-[#FFFFFF] hover:bg-[#F4F4F5] hover:cursor-pointer border-b-2 border-[#D4D4D8]">
                    <td style="border: 2px solid black;" class="w-full text-left font-light text-[#3A8CF7] py-2.5 px-4">{{ $user['number'] }}</td>
                    <td style="border: 2px solid black;" class="w-full text-left font-light text-[#3A8CF7] py-2.5 px-4">{{ $user['fio'] }}</td>
                    <td style="border: 2px solid black;" class="w-full text-left font-light text-[#3A8CF7] py-2.5 px-4">{{ $user['working'] }}</td>
                    <td style="border: 2px solid black;" class="w-full text-left font-light text-[#3A8CF7] py-2.5 px-4">{{ $user['jobname'] }}</td>
                    <td style="border: 2px solid black;" class="w-full text-left font-light text-[#3A8CF7] py-2.5 px-4">{{ $user['language'] }}</td>
                    <td style="border: 2px solid black;" class="w-full text-left font-light text-[#3A8CF7] py-2.5 px-4">{{ $user['trainer'] }}</td>
                    <td style="border: 2px solid black;" class="w-full text-left font-light text-[#3A8CF7] py-2.5 px-4">{{ $user['group'] }}</td>
                </tr>
            @endforeach
                <tr class="bg-[#FFFFFF] hover:bg-[#F4F4F5] hover:cursor-pointer border-b-2 border-[#D4D4D8]">
                    <td style="border: 2px solid black;" colspan="2"><b>Всего:</b></td>
                    <td style="border: 2px solid black;text-align: left;" colspan="5" ><b>{{ count($group['users']) }}</b></td>
                </tr>
            </tbody>
        </table>
    @endforeach
@else
    <table class="table-fixed w-full">
        <tbody>
        <tr>
            <td>Группы по этим критериям не найдены</td>
        </tr>
        </tbody>
    </table>
@endif
