@if(count($table) > 0)
    @foreach($table as $group)
        <table class="table-fixed w-full">
            <tbody>
            <tr>
                <td>{{ $group['courses'] }}</td>
            </tr>
            <tr>
                <td>{{ $group['period'] }}</td>
            </tr>
            <tr>
                <td>{{ $group['format'] }}</td>
            </tr>
            <tr>
                <td>{{ $group['affiliate'] }}</td>
            </tr>
            </tbody>
        </table>
        <table class="table-fixed w-full">
            <thead class="bg-[#F4F4F5] border-b-2 border-[#D4D4D8]">
            <tr>
                <th scope="col"
                    class="w-full text-left font-normal text-[15px] text-[#18181B] py-2.5 px-4">
                    №
                </th>
                <th scope="col"
                    class="w-full text-left font-normal text-[15px] text-[#18181B] py-2.5 px-4">
                    Ф.И.О.
                </th>
                <th scope="col"
                    class="w-full text-left font-normal text-[15px] text-[#18181B] py-2.5 px-4">
                    Место работы
                </th>
                <th scope="col"
                    class="w-full text-left font-normal text-[15px] text-[#18181B] py-2.5 px-4">
                    Должность
                </th>
                <th scope="col"
                    class="w-full text-left font-normal text-[15px] text-[#18181B] py-2.5 px-4">
                    Язык обучения
                </th>
                <th scope="col"
                    class="w-full text-left font-normal text-[15px] text-[#18181B] py-2.5 px-4">
                    Тренер
                </th>
                <th scope="col"
                    class="w-full text-left font-normal text-[15px] text-[#18181B] py-2.5 px-4">
                    Группа
                </th>
            </tr>
            </thead>
            <tbody>
            @foreach($group['users'] as $user)
                <tr class="bg-[#FFFFFF] hover:bg-[#F4F4F5] hover:cursor-pointer border-b-2 border-[#D4D4D8]">
                    <td class="w-full text-left font-light text-[#3A8CF7] py-2.5 px-4">{{ $user['number'] }}</td>
                    <td class="w-full text-left font-light text-[#3A8CF7] py-2.5 px-4">{{ $user['fio'] }}</td>
                    <td class="w-full text-left font-light text-[#3A8CF7] py-2.5 px-4">{{ $user['working'] }}</td>
                    <td class="w-full text-left font-light text-[#3A8CF7] py-2.5 px-4">{{ $user['jobname'] }}</td>
                    <td class="w-full text-left font-light text-[#3A8CF7] py-2.5 px-4">{{ $user['language'] }}</td>
                    <td class="w-full text-left font-light text-[#3A8CF7] py-2.5 px-4">{{ $user['trainer'] }}</td>
                    <td class="w-full text-left font-light text-[#3A8CF7] py-2.5 px-4">{{ $user['group'] }}</td>
                </tr>
            @endforeach
            </tbody>
        </table>
    @endforeach
@else
    <table class="table-fixed w-full">
        <tbody>
        <tr>
            <td>Группы по этим критериям не найдены</td>
        </tr>
        </tbody>
    </table>
@endif
