<table style="border: #000000; border-style: solid; border-width: 2px;">
    <tbody>
        <tr>
            <td rowspan="2"><b>#</b></td>
            <td rowspan="2"></td>
            <td rowspan="2"><b>ФИО</b></td>
            <td rowspan="1" colspan="2"><b>Общая информация</b></td>
            <td rowspan="2"><b>Награды</b></td>
            <td rowspan="1" colspan="2"><b>издательские продукты (количество)*</b></td>
        </tr>
        <tr>
            <td><b>Должность</b></td>
            <td><b>Предмет</b></td>
            <td><b>Опубликованные статьи</b></td>
            <td><b>Пройденные курсы</b></td>
        </tr>
        @php
            $total = 0;
        @endphp
        @foreach($affiliates as $affiliate)
            @if(array_key_exists($affiliate->id, $table))
                @php
                    $i = 0;
                @endphp
                <tr>
                    <td colspan="8"><b>{{ $affiliate->title }}</b></td>
                </tr>
                @foreach($table[$affiliate->id] as $trainer)
                    @php
                        $i++;
                        $total++;
                    @endphp
                    <tr>
                        <td>{{ $total }}</td>
                        <td>{{ $i }}</td>
                        <td>{{ $trainer['fio'] }}</td>
                        <td>{{ $trainer['jobnameItem'] }}</td>
                        <td>{{ $trainer['teach_subject_id'] }}</td>
                        <td>{!!  $trainer['trainer_awards'] !!}</td>
                        <td>{!! $trainer['trainer_theses'] !!}</td>
                        <td>{!! $trainer['trainer_certificates'] !!}</td>
                    </tr>
                @endforeach
            @endif
        @endforeach
        <tr>
            <td colspan="1"></td>
            <td colspan="2"><b>Всего:</b></td>
            <td colspan="1"><b>{{ $total }}</b></td>
        </tr>
    </tbody>
</table>
