<table class="table-fixed w-full">
    <thead class="bg-[#F4F4F5] border-b-2 border-[#D4D4D8]">
    <tr>
        <th scope="col"
            class="w-full text-left font-normal text-[15px] text-[#18181B] py-2.5 px-4">
            ФИО
        </th>
        @foreach($dates as $date)
            <th scope="col" class="w-20 font-normal text-[15px] text-[#18181B] py-2.5 px-4">{{ date('d.m.Y', strtotime($date)) }}</th>
        @endforeach
    </tr>
    </thead>
    <tbody>
    @foreach($group->users as $user)
        <tr class="bg-[#FFFFFF] hover:bg-[#F4F4F5] hover:cursor-pointer border-b-2 border-[#D4D4D8]">
            <td class="w-full text-left font-light text-[#3A8CF7] py-2.5 px-4">{{ $user->fio }}</td>
            @foreach($dates as $date)
                <td class="w-20 text-center font-light text-[#18181B] py-2.5 px-4"><div class="flex w-full justify-center items-center"><div class="flex w-8 h-8 rounded-full">{{ \App\Models\Statements::listShortTypes()[($data[$user->id]['items'][$date] ?? '') ] }}</div></div></td>
            @endforeach
        </tr>
    @endforeach
    </tbody>
</table>
