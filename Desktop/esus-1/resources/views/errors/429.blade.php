@extends('errors::illustrated-layout')

@section('title', __('Слишком много запросов'))
@section('code', '429')
@section('message', __('Слишком много запросов'))
