@extends('errors::illustrated-layout')

@section('title', __('Ошибка сервера'))
@section('code', '500')
@section('message', __('Ошибка сервера'))
