@extends('errors::illustrated-layout')

@section('title', __('Срок действия авторизации устарел'))
@section('code', '419')
@section('message', __('Срок действия авторизации устарел'))
