@extends('errors::illustrated-layout')

@section('title', __('Неавторизованный'))
@section('code', '401')
@section('message', __('Неавторизованный'))
