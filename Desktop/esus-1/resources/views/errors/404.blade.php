@extends('errors::illustrated-layout')

@section('title', __('Не найдено'))
@section('code', '404')
@section('message', __('Не найдено'))
