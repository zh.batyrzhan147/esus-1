<x-auth-layout>
    <div class="flex flex-col w-full p-4 lg:py-20 justify-center items-center">
        @if (session('status'))
            <div class="mb-10">
                <svg width="82" height="82" viewBox="0 0 82 82" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path fill-rule="evenodd" clip-rule="evenodd" d="M61.3368 30.4876L38.8405 50.7038L26.645 39.4711L29.3549 36.5289L38.8737 45.2962L58.6632 27.5125L61.3368 30.4876Z" fill="#79C346"/>
                    <path fill-rule="evenodd" clip-rule="evenodd" d="M41 78C61.4345 78 78 61.4345 78 41C78 20.5655 61.4345 4 41 4C20.5655 4 4 20.5655 4 41C4 61.4345 20.5655 78 41 78ZM41 82C63.6437 82 82 63.6437 82 41C82 18.3563 63.6437 0 41 0C18.3563 0 0 18.3563 0 41C0 63.6437 18.3563 82 41 82Z" fill="#79C346"/>
                </svg>
            </div>
            <h2 class="text-4xl font-bold mb-6">{{ __('admin.auth.forgot.reset') }}</h2>
            <div class="text-[18px] font-medium mb-6 text-center">
                {{ session('status') }}
            </div>
            <a href="{{ route('login') }}" class="py-4 px-7 border border-[#0077CC] rounded text-[#FBFCFE] text-[18px] bg-gradient-to-b from-[#569DE5] to-[#2C73C5] hover:from-[#2C73C5] hover:to-[#569DE5]">{{ __('admin.auth.forgot.continue') }}</a>

        @else
            <h2 class="text-4xl font-bold mb-6">{{ __('admin.auth.forgot.question') }}</h2>
            <div class="text-[17px] text-[#4C4C4C] text-center mb-4">
                <p>{{ __('admin.auth.forgot.description') }}</p>
                <p>{{ __('admin.auth.forgot.have.acc') }} <a href="{{ route('login') }}" class="text-[#3A8CF7] hover:text-[#2563EB]">{{ __('admin.auth.forgot.then.login') }}</a></p>
            </div>

            <form class="w-full lg:w-[460px] mx-auto flex flex-col space-y-8" method="POST" action="{{ route('password.email') }}">
                @csrf
                <div class="flex flex-col space-y-2">
                    <input id="iin" type="text" name="iin" :value="old('iin')" required autofocus placeholder="{{ __('admin.auth.forgot.form.iin.placeholder') }}" class="w-full py-6 px-7 text-[17px] text-[#4C4C4C] border-2 border-[#D6D6D6] focus:border-[#3487F4] rounded hover:border-[#4C4C4C] focus:hover:border-[#3487F4] outline-0 focus:outline-0"/>
                    <x-jet-validation-errors class="font-light text-[#B91C1C] mb-4 text-center" />
                </div>
                <button type="submit" class="w-full py-6 px-7 border border-[#0077CC] rounded text-[#FBFCFE] text-[18px] bg-gradient-to-b from-[#569DE5] to-[#2C73C5] hover:from-[#2C73C5] hover:to-[#569DE5]">{{ __('admin.auth.forgot.form.submit') }}</button>
            </form>
        @endif
    </div>
</x-auth-layout>
