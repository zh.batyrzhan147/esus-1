<x-auth-layout>
    
    @include('header-guest')

    <div class="flex flex-col w-full p-4 lg:py-10 justify-center items-center">
        <h2 class="text-4xl font-bold mb-12">{{ __('admin.auth.register.title') }}</h2>
        <div class="h-px w-full bg-[#EAEAEA] mb-9"></div>

        <x-jet-validation-errors class="font-light text-[#B91C1C] mb-4 text-center" />

        <form class="w-full lg:w-[460px] mx-auto flex flex-col space-y-8" method="POST" action="{{ route('register') }}">
            @csrf
            <div class="text-[17px] text-center">
                <p>{{ __('admin.auth.register.description') }}</p>
                <p>{{ __('admin.auth.register.have.acc') }} <a href="{{ route('login') }}" class="text-[#3A8CF7] hover:text-[#2563EB]">{{ __('admin.auth.register.then.login') }}</a></p>
            </div>
            <div class="flex flex-col space-y-3">
                <input id="iin" type="text" name="iin" :value="old('iin')" required autofocus autocomplete="iin" placeholder="{{ __('admin.auth.register.form.iin') }}" class="w-full py-6 px-7 text-[17px] text-[#4C4C4C] border-2 border-[#D6D6D6] rounded focus:border-[#3487F4] hover:border-[#4C4C4C] focus:hover:border-[#3487F4] outline-0 focus:outline-0" />
                <input id="last_name" type="text" name="last_name" :value="old('last_name')" required autofocus autocomplete="last_name" placeholder="{{ __('admin.auth.register.form.lastname') }}" class="w-full py-6 px-7 text-[17px] text-[#4C4C4C] border-2 border-[#D6D6D6] rounded focus:border-[#3487F4] hover:border-[#4C4C4C] focus:hover:border-[#3487F4] outline-0 focus:outline-0" />
                <input id="name" type="text" name="name" :value="old('name')" required autofocus autocomplete="name" placeholder="{{ __('admin.auth.register.form.firstname') }}" class="w-full py-6 px-7 text-[17px] text-[#4C4C4C] border-2 border-[#D6D6D6] rounded focus:border-[#3487F4] hover:border-[#4C4C4C] focus:hover:border-[#3487F4] outline-0 focus:outline-0" />
                <input type="text" name="phone" id="phone" :value="old('phone')"  autofocus autocomplete="phone" placeholder="{{ __('admin.auth.register.form.phone') }}" class="w-full py-6 px-7 text-[17px] text-[#4C4C4C] border-2 border-[#D6D6D6] rounded focus:border-[#3487F4] hover:border-[#4C4C4C] focus:hover:border-[#3487F4] outline-0 focus:outline-0" />
            </div>
            <div class="h-px w-full bg-[#EAEAEA]"></div>
            <div class="flex flex-col space-y-3">
                <input id="email" type="email" name="email" :value="old('email')" required placeholder="{{ __('admin.auth.register.form.email') }}" class="w-full py-6 px-7 text-[17px] text-[#4C4C4C] border-2 border-[#D6D6D6] rounded focus:border-[#3487F4] hover:border-[#4C4C4C] focus:hover:border-[#3487F4] outline-0 focus:outline-0" />
                {{-- <input id="password" type="password" name="password" required autocomplete="new-password" placeholder="{{ __('admin.auth.register.form.password') }}" class="w-full py-6 px-7 text-[17px] text-[#4C4C4C] border-2 border-[#D6D6D6] rounded focus:border-[#3487F4] hover:border-[#4C4C4C] focus:hover:border-[#3487F4] outline-0 focus:outline-0"/>--}}
                {{-- <input id="password_confirmation" type="password" name="password_confirmation" required autocomplete="new-password" placeholder="{{ __('admin.auth.register.form.password2') }} " class="w-full py-6 px-7 text-[17px] text-[#4C4C4C] border-2 border-[#D6D6D6] rounded focus:border-[#3487F4] hover:border-[#4C4C4C] focus:hover:border-[#3487F4] outline-0 focus:outline-0"/>--}}
            </div>
            <div class="h-px w-full bg-[#EAEAEA]"></div>
            <div class="text-[14px] text-[#929292] text-center">
                <p>{{ __('admin.auth.register.form.agree') }}</p>
            </div>
            <button type="submit" class="w-full py-6 px-7 border border-[#0077CC] rounded text-[#FBFCFE] text-[18px] bg-gradient-to-b from-[#569DE5] to-[#2C73C5] hover:from-[#2C73C5] hover:to-[#569DE5]">{{ __('admin.auth.register.form.submit') }}</button>
        </form>
    </div>

    @livewireScripts

    @push('phone-mask-scripts')
    <script src="/js/imask.min.js"></script>
    <script>
        var element = document.getElementById('phone');
        var maskOptions = {
            mask: '+7(700)000-00-00',
            lazy: false
        }

        var element2 = document.getElementById('iin');
        var maskOptions2 = {
            mask: '000 000 000 000',
            lazy: false
        }

        var mask = new IMask(element, maskOptions);
        var mask2 = new IMask(element2, maskOptions2);
    </script>
    @endpush
</x-auth-layout>