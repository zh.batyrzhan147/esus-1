<x-auth-layout>

    @include('header-guest')

    <div class="flex flex-col w-full p-4 lg:py-20 justify-center items-center">
        <h2 class="text-4xl font-bold mb-6">{{ __('admin.auth.login.title') }}</h2>

        <x-jet-validation-errors class="font-light text-[#B91C1C] mb-4 text-center" />

        @if (session('status'))
            <div class="font-light text-[#B91C1C] mb-4 text-center">
                {{ session('status') }}
            </div>
        @endif

        <!-- @if(session()->has('registered')) -->
         
        <!-- @endif -->

        <div class="flex flex-col justify-center mb-2">
                <div class="text-2xl text-red-600 text-center font-bold">Электронды поштаңызды тексеріңіз, құпиясөз поштаңызға жолданды!</div>
            </div>

        <form class="w-full lg:w-[460px] mx-auto flex flex-col space-y-4" method="POST" action="{{ route('login') }}">

       

            @csrf
            <div class=" w-full mx-auto flex flex-col space-y-8">
                <input type="text" name="iin" id="iin" placeholder="{{ __('admin.auth.login.form.iin') }}" class="w-full py-6 px-7 text-[17px] text-[#4C4C4C] border-2 border-[#D6D6D6] rounded focus:border-[#3487F4] hover:border-[#4C4C4C] focus:hover:border-[#3487F4] outline-0 focus:outline-0" required autofocus/>
                <input type="password" name="password" id="password" placeholder="{{ __('admin.auth.login.form.password') }}" class="w-full py-6 px-7 text-[17px] text-[#4C4C4C] border-2 border-[#D6D6D6] rounded focus:border-[#3487F4] hover:border-[#4C4C4C] focus:hover:border-[#3487F4] outline-0 focus:outline-0" required autocomplete="current-password"/>
                <button type="submit" class="w-full py-6 px-7 border border-[#0077CC] rounded text-[#FBFCFE] text-[18px] bg-gradient-to-b from-[#569DE5] to-[#2C73C5] hover:from-[#2C73C5] hover:to-[#569DE5]">{{ __('admin.auth.login.form.submit') }}</button>
            </div>
            <div class="flex flex-col space-y-4">
                <a href="{{ route('register') }}" class="w-full block py-6 px-7 border-2 border-[#000000] rounded text-[#000000] text-[18px] text-center hover:bg-[#000000] hover:text-white">{{ __('admin.auth.login.form.register') }}</a>
                <a href="{{ route('password.request') }}" class="text-[17px] text-[#3A8CF7] hover:text-[#2563EB]">{{ __('admin.auth.login.form.forgot') }}</a>
            </div>
        </form>
    </div>
   
    @livewireScripts

</x-auth-layout>
