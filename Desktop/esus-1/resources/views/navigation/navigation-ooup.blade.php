<nav x-data="{ open: false }" class="bg-gray-100 border-b border-gray-100" wire:ignore>
    <!-- Primary Navigation Menu -->
    <div class="bg-white mx-auto px-4 sm:px-6 lg:px-8">
        <div class="flex justify-between h-16">
            <div class="flex top-menu">
                <!-- Logo -->
                <div class="flex-shrink-0 flex items-center mr-6">
                    <a href="{{ route('dashboard') }}">
                        <div class="flex h-9 w-auto items-center space-x-3">
                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Layer_1" x="0px" y="0px" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve" width="32" height="32">
                                <g>
                                    <rect x="451" y="200.688" style="fill:#FDBF00;" width="30" height="142.207"/>
                                    <path style="fill:#FDBF00;" d="M466,454.105l-47-48.252l47-96.504l46,96.504L466,454.105z"/>
                                    <polygon style="fill:#0095FF;" points="512,192.895 256,327.895 0,192.895 256,57.895  "/>
                                    <polygon style="fill:#006EFF;" points="512,192.895 256,327.895 256,57.895  "/>
                                    <path style="fill:#4BB9EC;" d="M382.599,207.594C343.001,188.094,299.5,178.195,256,178.195s-87.001,9.899-126.599,29.399   l-8.401,4.2v146.1h270v-146.1L382.599,207.594z"/>
                                    <path style="fill:#0095FF;" d="M391,211.795v146.1H256v-179.7c43.5,0,87.001,9.899,126.599,29.399L391,211.795z"/>
                                </g>
                            </svg>
                            <span class="font-bold">
                                ESUS
                            </span>
                        </div>
                    </a>
                </div>

                                <div class="group hidden sm:-my-px sm:ml-4 sm:flex relative">
                                    <button class="outline-none focus:outline-none px-3 py-1 flex items-center min-w-32 @if(request()->routeIs('admin.years.*') || request()->routeIs('admin.languages.*')|| request()->routeIs('admin.courses.*')|| request()->routeIs('admin.grants.*')) border-b-2 border-[#026dff] @endif">
                                        <span class="flex-1">Учебный процесс</span>
                                    </button>
                                    <div class="absolute bottom-0 left-0 z-50">
                                        <ul class="bg-white border rounded-sm transform scale-0 group-hover:scale-100 absolute top-0 transition duration-150 ease-in-out origin-top min-w-32">
                                            <li class="px-3 py-1 hover:bg-gray-100 whitespace-nowrap">
                                                <a href="{{ route('ooup.years.index') }}">Учебные годы</a>
                                            </li>
                                            <li class="px-3 py-1 hover:bg-gray-100 whitespace-nowrap">
                                                <a href="{{ route('ooup.weekends.index') }}">Выходные и рабочие дни</a>
                                            </li>
{{--                                            <li class="px-3 py-1 hover:bg-gray-100 whitespace-nowrap">--}}
{{--                                                <a href="{{ route('ooup.languages.index') }}">Язык обучения</a>--}}
{{--                                            </li>--}}
                                            <li class="px-3 py-1 hover:bg-gray-100 whitespace-nowrap">
                                                <a href="{{ route('ooup.courses.index') }}">Программы</a>
                                            </li>
                                            <li class="px-3 py-1 hover:bg-gray-100 whitespace-nowrap">
                                                <a href="{{ route('ooup.grants.index') }}">Курсы</a>
                                            </li>
                                            <li class="px-3 py-1 hover:bg-gray-100 whitespace-nowrap">
                                                <a href="{{ route('ooup.certificates-templates.index') }}">Шаблоны сертификатов</a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>

                                <div class="group hidden sm:-my-px sm:ml-4 sm:flex relative">
                                    <button class="outline-none focus:outline-none px-3 py-1 flex items-center min-w-32  @if(request()->routeIs('admin.trainers.*') || request()->routeIs('admin.groups.*')|| request()->routeIs('admin.schedule.*')) border-b-2 border-[#026dff] @endif">
                                        <span class="flex-1">Группы</span>
                                    </button>
                                    <div class="absolute bottom-0 left-0 z-50">
                                        <ul class="bg-white border rounded-sm transform scale-0 group-hover:scale-100 absolute top-0 transition duration-150 ease-in-out origin-top min-w-32">
                                            <li class="px-3 py-1 hover:bg-gray-100 whitespace-nowrap">
                                                <a href="{{ route('ooup.trainers.index') }}">Тренеры</a>
                                            </li>
                                            <li class="px-3 py-1 hover:bg-gray-100 whitespace-nowrap">
                                                <a href="{{ route('ooup.groups.index') }}">Группы</a>
                                            </li>
                                            <li class="px-3 py-1 hover:bg-gray-100 whitespace-nowrap">
                                                <a href="{{ route('ooup.certificates.index') }}">Сертификаты</a>
                                            </li>
                                            <li class="px-3 py-1 hover:bg-gray-100 whitespace-nowrap">
                                                <a href="{{ route('ooup.schedule.index') }}">Расписание</a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>

                <div class="group hidden sm:-my-px sm:ml-4 sm:flex relative">
                    <button class="outline-none focus:outline-none px-3 py-1 flex items-center min-w-32  @if(request()->routeIs('admin.trainers.*') || request()->routeIs('admin.groups.*')|| request()->routeIs('admin.schedule.*')) border-b-2 border-[#026dff] @endif">
                        <span class="flex-1">Отчеты</span>
                    </button>
                    <div class="absolute bottom-0 left-0 z-50">
                        <ul class="bg-white border rounded-sm transform scale-0 group-hover:scale-100 absolute top-0 transition duration-150 ease-in-out origin-top min-w-32">
                            <li class="px-3 py-1 hover:bg-gray-100 whitespace-nowrap">
                                <a href="{{ route('ooup.reports.index') }}">Отчеты</a>
                            </li>
                        </ul>
                    </div>
                </div>

                <div class="group hidden sm:-my-px sm:ml-4 sm:flex relative">
                    <button class="outline-none focus:outline-none px-3 py-1 flex items-center min-w-32  @if(request()->routeIs('admin.trainers.*') || request()->routeIs('admin.groups.*')|| request()->routeIs('admin.schedule.*')) border-b-2 border-[#026dff] @endif">
                        <span class="flex-1">Аудит</span>
                    </button>
                    <div class="absolute bottom-0 left-0 z-50">
                        <ul class="bg-white border rounded-sm transform scale-0 group-hover:scale-100 absolute top-0 transition duration-150 ease-in-out origin-top min-w-32">
                            <li class="px-3 py-1 hover:bg-gray-100 whitespace-nowrap">
                                <a href="{{ route('ooup.audit.index') }}">Аудит</a>
                            </li>
                        </ul>
                    </div>
                </div>



                {{--                <div class="group hidden sm:-my-px sm:ml-4 sm:flex relative">--}}
                {{--                    <button class="outline-none focus:outline-none px-3 py-1 flex items-center min-w-32 @if(request()->routeIs('admin.jobnames.*') || request()->routeIs('admin.educations.*')|| request()->routeIs('admin.academicdegrees.*')|| request()->routeIs('admin.academiccategories.*')|| request()->routeIs('admin.nationalities.*')) border-b-2 border-[#026dff] @endif">--}}
                {{--                        <span class="flex-1">Информация сотрудников</span>--}}
                {{--                    </button>--}}
                {{--                    <div class="absolute bottom-0 left-0 z-50">--}}
                {{--                        <ul class="bg-white border rounded-sm transform scale-0 group-hover:scale-100 absolute top-0 transition duration-150 ease-in-out origin-top min-w-32">--}}
                {{--                            <li class="px-3 py-1 hover:bg-gray-100 whitespace-nowrap">--}}
                {{--                                <a href="{{ route('admin.jobnames.index') }}">Должность</a>--}}
                {{--                            </li>--}}
                {{--                            <li class="px-3 py-1 hover:bg-gray-100 whitespace-nowrap">--}}
                {{--                                <a href="{{ route('admin.educations.index') }}">Образование</a>--}}
                {{--                            </li>--}}
                {{--                            <li class="px-3 py-1 hover:bg-gray-100 whitespace-nowrap">--}}
                {{--                                <a href="{{ route('admin.academicdegrees.index') }}">Ученая степень</a>--}}
                {{--                            </li>--}}
                {{--                            <li class="px-3 py-1 hover:bg-gray-100 whitespace-nowrap">--}}
                {{--                                <a href="{{ route('admin.academiccategories.index') }}">Категория</a>--}}
                {{--                            </li>--}}
                {{--                            <li class="px-3 py-1 hover:bg-gray-100 whitespace-nowrap">--}}
                {{--                                <a href="{{ route('admin.nationalities.index') }}">Национальность</a>--}}
                {{--                            </li>--}}
                {{--                            <li class="px-3 py-1 hover:bg-gray-100 whitespace-nowrap">--}}
                {{--                                <a href="{{ route('admin.genders.index') }}">Пол</a>--}}
                {{--                            </li>--}}
                {{--                        </ul>--}}
                {{--                    </div>--}}
                {{--                </div>--}}

                {{--                <div class="group hidden sm:-my-px sm:ml-4 sm:flex relative">--}}
                {{--                    <button class="outline-none focus:outline-none px-3 py-1 flex items-center min-w-32 @if(request()->routeIs('admin.affiliates.*') || request()->routeIs('admin.locations.*')) border-b-2 border-[#026dff] @endif">--}}
                {{--                        <span class="flex-1">Локаций</span>--}}
                {{--                    </button>--}}
                {{--                    <div class="absolute bottom-0 left-0 z-50">--}}
                {{--                        <ul class="bg-white border rounded-sm transform scale-0 group-hover:scale-100 absolute top-0 transition duration-150 ease-in-out origin-top min-w-32">--}}
                {{--                            <li class="px-3 py-1 hover:bg-gray-100 whitespace-nowrap">--}}
                {{--                                <a href="{{ route('admin.affiliates.index') }}">Филиал</a>--}}
                {{--                            </li>--}}
                {{--                            <li class="px-3 py-1 hover:bg-gray-100 whitespace-nowrap">--}}
                {{--                                <a href="{{ route('admin.locations.oblast.index') }}">Область</a>--}}
                {{--                            </li>--}}
                {{--                            <li class="px-3 py-1 hover:bg-gray-100 whitespace-nowrap">--}}
                {{--                                <a href="{{ route('admin.locations.rayon.index') }}">Район</a>--}}
                {{--                            </li>--}}
                {{--                            <li class="px-3 py-1 hover:bg-gray-100 whitespace-nowrap">--}}
                {{--                                <a href="{{ route('admin.locations.aul.index') }}">Населенный пункт</a>--}}
                {{--                            </li>--}}
                {{--                        </ul>--}}
                {{--                    </div>--}}
                {{--                </div>--}}

                {{--                <div class="group hidden sm:-my-px sm:ml-4 sm:flex relative">--}}
                {{--                    <div class="outline-none focus:outline-none px-3 py-1 flex items-center min-w-32 whitespace-nowrap  @if(request()->routeIs('admin.institutes.*')) border-b-2 border-[#026dff] @endif">--}}
                {{--                        <a href="{{ route('admin.institutes.index') }}">Учебные заведения</a>--}}
                {{--                    </div>--}}
                {{--                </div>--}}

                {{--                <div class="group hidden sm:-my-px sm:ml-4 sm:flex relative">--}}
                {{--                    <button class="outline-none focus:outline-none px-3 py-1 flex items-center min-w-32 whitespace-nowrap @if(request()->routeIs('admin.users.*') || request()->routeIs('admin.roles.*')) border-b-2 border-[#026dff] @endif">--}}
                {{--                        <span class="flex-1">Пользователи</span>--}}
                {{--                    </button>--}}
                {{--                    <div class="absolute bottom-0 left-0 z-50">--}}
                {{--                        <ul class="bg-white border rounded-sm transform scale-0 group-hover:scale-100 absolute top-0 transition duration-150 ease-in-out origin-top min-w-32">--}}
                {{--                            <li class="px-3 py-1 hover:bg-gray-100 whitespace-nowrap">--}}
                {{--                                <a href="{{ route('admin.users.index') }}">Пользователи</a>--}}
                {{--                            </li>--}}
                {{--                            <li class="px-3 py-1 hover:bg-gray-100 whitespace-nowrap">--}}
                {{--                                <a href="{{ route('admin.roles.index') }}">Роли пользователей</a>--}}
                {{--                            </li>--}}
                {{--                        </ul>--}}
                {{--                    </div>--}}
                {{--                </div>--}}

                <style>
                    /* since nested groupes are not supported we have to use
                    regular css for the nested dropdowns
                    */
                    .top-menu li>ul                 { transform: translatex(100%) scale(0) }
                    .top-menu li a                  { display: inline-block; width: 100%; padding-right: 50px; }
                    .top-menu li:hover>ul           { transform: translatex(101%) scale(1) }
                    .top-menu li > button svg       { transform: rotate(-90deg) }
                    .top-menu li:hover > button svg { transform: rotate(-270deg) }
                    /* Below styles fake what can be achieved with the tailwind config
                    you need to add the group-hover variant to scale and define your custom
                    min width style.
                    See https://codesandbox.io/s/tailwindcss-multilevel-dropdown-y91j7?file=/index.html
                    for implementation with config file
                    */
                    .top-menu .group:hover .group-hover\:scale-100 { transform: scale(1); }
                    .top-menu .group:hover .group-hover\:-rotate-180 { transform: rotate(180deg) }
                    .top-menu .scale-0 { transform: scale(0) }
                    .top-menu .min-w-32 { min-width: 8rem }
                </style>

                <!-- Navigation Links -->
                {{--                <div class="hidden space-x-8 sm:-my-px sm:ml-10 sm:flex">--}}
                {{--                    <x-jet-nav-link href="{{ route('dashboard') }}" :active="request()->routeIs('dashboard')">--}}
                {{--                        {{ __('Dashboard') }}--}}
                {{--                    </x-jet-nav-link>--}}
                {{--                </div>--}}
                {{--                @can(\App\Models\Permission::PERMISSION_SUPERADMIN)--}}
                {{--                    <div class="hidden space-x-8 sm:-my-px sm:ml-10 sm:flex">--}}
                {{--                        <x-jet-nav-link href="{{ route('admin.courses.index') }}" :active="request()->routeIs('admin.*')">--}}
                {{--                            Администрирование--}}
                {{--                        </x-jet-nav-link>--}}
                {{--                    </div>--}}
                {{--                @endcan--}}
                {{--                @can(\App\Models\Permission::PERMISSION_SUPERADMIN)--}}
                {{--                    <div class="hidden space-x-8 sm:-my-px sm:ml-10 sm:flex">--}}
                {{--                        <x-jet-nav-link href="{{ route('admin.users.index') }}" :active="request()->routeIs('admin.users.*')">--}}
                {{--                            Users--}}
                {{--                        </x-jet-nav-link>--}}
                {{--                    </div>--}}
                {{--                @endcan--}}
            </div>

            <!-- Settings Dropdown -->
            <div class="hidden sm:flex sm:items-center sm:ml-6">
                <div class="hidden sm:flex items-center cursor-pointer px-3">
                    <livewire:set-locale/>
                </div>
                <x-jet-dropdown align="right" width="48">
                    <x-slot name="trigger">
                        @if (Laravel\Jetstream\Jetstream::managesProfilePhotos())
                            <button class="flex text-sm border-2 border-transparent rounded-full focus:outline-none focus:border-gray-300 transition duration-150 ease-in-out">
                                <img class="h-8 w-8 rounded-full object-cover" src="{{ Auth::user()->profile_photo_url }}" alt="{{ Auth::user()->name }}" />
                            </button>
                        @else
                            <button class="flex items-center text-sm font-medium text-gray-500 hover:text-gray-700 hover:border-gray-300 focus:outline-none focus:text-gray-700 focus:border-gray-300 transition duration-150 ease-in-out">
                                <div>{{ Auth::user()->name }}</div>

                                <div class="ml-1">
                                    <svg class="fill-current h-4 w-4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20">
                                        <path fill-rule="evenodd" d="M5.293 7.293a1 1 0 011.414 0L10 10.586l3.293-3.293a1 1 0 111.414 1.414l-4 4a1 1 0 01-1.414 0l-4-4a1 1 0 010-1.414z" clip-rule="evenodd" />
                                    </svg>
                                </div>
                            </button>
                        @endif
                    </x-slot>

                    <x-slot name="content">
                        <!-- Account Management -->
                        <div class="block px-4 py-2 text-xs text-gray-400">
                            {{ __('Управление аккаунтом') }}
                        </div>

                        <x-jet-dropdown-link href="{{ route('user.profile') }}">
                            {{ __('Профиль') }}
                        </x-jet-dropdown-link>

                        @if (Laravel\Jetstream\Jetstream::hasApiFeatures())
                            <x-jet-dropdown-link href="{{ route('api-tokens.index') }}">
                                {{ __('API Tokens') }}
                            </x-jet-dropdown-link>
                        @endif

                        <div class="border-t border-gray-100"></div>

                        <!-- Team Management -->
                        @if (Laravel\Jetstream\Jetstream::hasTeamFeatures())
                            <div class="block px-4 py-2 text-xs text-gray-400">
                                {{ __('Manage Team') }}
                            </div>

                            <!-- Team Settings -->
                            <x-jet-dropdown-link href="{{ route('teams.show', Auth::user()->currentTeam->id) }}">
                                {{ __('Team Settings') }}
                            </x-jet-dropdown-link>

                            @can('create', Laravel\Jetstream\Jetstream::newTeamModel())
                                <x-jet-dropdown-link href="{{ route('teams.create') }}">
                                    {{ __('Create New Team') }}
                                </x-jet-dropdown-link>
                            @endcan

                            <div class="border-t border-gray-100"></div>

                            <!-- Team Switcher -->
                            <div class="block px-4 py-2 text-xs text-gray-400">
                                {{ __('Switch Teams') }}
                            </div>

                            @foreach (Auth::user()->allTeams() as $team)
                                <x-jet-switchable-team :team="$team" />
                            @endforeach

                            <div class="border-t border-gray-100"></div>
                    @endif

                    <!-- Authentication -->
                        <form method="POST" action="{{ route('logout') }}">
                            @csrf

                            <x-jet-dropdown-link href="{{ route('logout') }}"
                                                 onclick="event.preventDefault();
                                                            this.closest('form').submit();">
                                {{ __('Выйти') }}
                            </x-jet-dropdown-link>
                        </form>
                    </x-slot>
                </x-jet-dropdown>
            </div>

            <!-- Hamburger -->
            <div class="-mr-2 flex items-center sm:hidden">
                <div class="flex items-center cursor-pointer mr-2">
                    <livewire:set-locale/>
                </div>
                <button @click="open = ! open" class="inline-flex items-center justify-center p-2 rounded-md text-gray-400 hover:text-gray-500 hover:bg-gray-100 focus:outline-none focus:bg-gray-100 focus:text-gray-500 transition duration-150 ease-in-out">
                    <svg class="h-6 w-6" stroke="currentColor" fill="none" viewBox="0 0 24 24">
                        <path :class="{'hidden': open, 'inline-flex': ! open }" class="inline-flex" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M4 6h16M4 12h16M4 18h16" />
                        <path :class="{'hidden': ! open, 'inline-flex': open }" class="hidden" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M6 18L18 6M6 6l12 12" />
                    </svg>
                </button>
            </div>
        </div>
    </div>

    <!-- Responsive Navigation Menu -->
    <div :class="{'block': open, 'hidden': ! open}" class="hidden sm:hidden">
    {{--        <div class="pt-2 pb-3 space-y-1">--}}
    {{--            <x-jet-responsive-nav-link href="{{ route('dashboard') }}" :active="request()->routeIs('dashboard')">--}}
    {{--                {{ __('Dashboard') }}--}}
    {{--            </x-jet-responsive-nav-link>--}}
    {{--        </div>--}}

    <!-- Responsive Settings Options -->
        <div class="pt-4 pb-1 border-t border-gray-200">
            <div class="flex items-center px-4">
                <div class="flex-shrink-0">
                    <img class="h-10 w-10 rounded-full" src="{{ Auth::user()->profile_photo_url }}" alt="{{ Auth::user()->name }}" />
                </div>

                <div class="ml-3">
                    <div class="font-medium text-base text-gray-800">{{ Auth::user()->name }}</div>
                    <div class="font-medium text-sm text-gray-500">{{ Auth::user()->email }}</div>
                </div>
            </div>

            <div class="mt-3 space-y-1">
                <!-- Account Management -->
                <x-jet-responsive-nav-link href="{{ route('profile.show') }}" :active="request()->routeIs('profile.show')">
                    {{ __('Профиль') }}
                </x-jet-responsive-nav-link>

                @if (Laravel\Jetstream\Jetstream::hasApiFeatures())
                    <x-jet-responsive-nav-link href="{{ route('api-tokens.index') }}" :active="request()->routeIs('api-tokens.index')">
                        {{ __('API Tokens') }}
                    </x-jet-responsive-nav-link>
                @endif

            <!-- Authentication -->
                <form method="POST" action="{{ route('logout') }}">
                    @csrf

                    <x-jet-responsive-nav-link href="{{ route('logout') }}"
                                               onclick="event.preventDefault();
                                                this.closest('form').submit();">
                        {{ __('Выйти') }}
                    </x-jet-responsive-nav-link>
                </form>

                <!-- Team Management -->
                @if (Laravel\Jetstream\Jetstream::hasTeamFeatures())
                    <div class="border-t border-gray-200"></div>

                    <div class="block px-4 py-2 text-xs text-gray-400">
                        {{ __('Manage Team') }}
                    </div>

                    <!-- Team Settings -->
                    <x-jet-responsive-nav-link href="{{ route('teams.show', Auth::user()->currentTeam->id) }}" :active="request()->routeIs('teams.show')">
                        {{ __('Team Settings') }}
                    </x-jet-responsive-nav-link>

                    <x-jet-responsive-nav-link href="{{ route('teams.create') }}" :active="request()->routeIs('teams.create')">
                        {{ __('Create New Team') }}
                    </x-jet-responsive-nav-link>

                    <div class="border-t border-gray-200"></div>

                    <!-- Team Switcher -->
                    <div class="block px-4 py-2 text-xs text-gray-400">
                        {{ __('Switch Teams') }}
                    </div>

                    @foreach (Auth::user()->allTeams() as $team)
                        <x-jet-switchable-team :team="$team" component="jet-responsive-nav-link" />
                    @endforeach
                @endif
            </div>
        </div>
    </div>
</nav>
