@if ($paginator->hasPages())
    <nav role="navigation" aria-label="{{ __('Pagination Navigation') }}" class="flex items-center justify-between text-xs">
        <div class="flex justify-between flex-1 sm:hidden">
            @if ($paginator->onFirstPage())
                <span class="relative inline-flex items-center px-2.5 py-1 text-xs font-medium text-gray-500 bg-white border border-gray-300 cursor-default leading-5 rounded-l-md">
                    {!! __('< Назад') !!}
                </span>
            @else
                <a href="{{ $paginator->previousPageUrl() }}" class="relative inline-flex items-center px-2.5 py-1 text-xs font-medium text-gray-700 bg-white border border-gray-300 leading-5 rounded-l-md hover:text-gray-500 focus:outline-none focus:border-blue-300 active:bg-gray-100 active:text-gray-700 transition ease-in-out duration-150">
                    {!! __('< Назад') !!}
                </a>
            @endif

            @if ($paginator->hasMorePages())
                <a href="{{ $paginator->nextPageUrl() }}" class="relative inline-flex items-center px-2.5 py-1 ml-3 text-xs font-medium text-gray-700 bg-white border border-gray-300 leading-5 rounded-r-md hover:text-gray-500 focus:outline-none focus:border-blue-300 active:bg-gray-100 active:text-gray-700 transition ease-in-out duration-150">
                    {!! __('Вперед >') !!}
                </a>
            @else
                <span class="relative inline-flex items-center px-2.5 py-1 ml-3 text-xs font-medium text-gray-500 bg-white border border-gray-300 cursor-default leading-5 rounded-r-md">
                    {!! __('Вперед >') !!}
                </span>
            @endif
        </div>

        <div class="hidden sm:flex-1 sm:flex sm:items-center sm:justify-end sm:space-x-4">
            <div>
                <p class="text-xs text-gray-700 leading-5">
                    <span class="font-medium">{{ $paginator->firstItem() }}</span>
                    {!! __('-') !!}
                    <span class="font-medium">{{ $paginator->lastItem() }}</span>
                    {!! __('/') !!}
                    <span class="font-medium">{{ $paginator->total() }}</span>
                </p>
            </div>

            <div>
                <span class="relative z-0 inline-flex shadow-sm rounded-md">
                    {{-- Previous Page Link --}}
                    @if ($paginator->onFirstPage())
                        <span aria-disabled="true" aria-label="{{ __('pagination.previous') }}">
                            <span class="relative inline-flex items-center px-2 py-2.5 text-xs font-medium text-gray-500 bg-white border border-gray-300 cursor-default rounded-l-md leading-5" aria-hidden="true">
                                <!-- <svg class="w-5 h-5" fill="currentColor" viewBox="0 0 20 20">
                                    <path fill-rule="evenodd" d="M12.707 5.293a1 1 0 010 1.414L9.414 10l3.293 3.293a1 1 0 01-1.414 1.414l-4-4a1 1 0 010-1.414l4-4a1 1 0 011.414 0z" clip-rule="evenodd" />
                                </svg> -->
                                <svg class="w-3 h-2" viewBox="0 0 7 12" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                    <path fill-rule="evenodd" d="M6.83938 2.06646V1.03119C6.83938 0.941456 6.73625 0.891902 6.66661 0.946813L0.629107 5.66244C0.57781 5.70233 0.536302 5.75341 0.50775 5.81179C0.479198 5.87016 0.464355 5.93428 0.464355 5.99927C0.464355 6.06425 0.479198 6.12838 0.50775 6.18675C0.536302 6.24512 0.57781 6.29621 0.629107 6.3361L6.66661 11.0517C6.73759 11.1066 6.83938 11.0571 6.83938 10.9673V9.93208C6.83938 9.86645 6.80857 9.80351 6.75768 9.76333L1.93625 5.99994L6.75768 2.23521C6.80857 2.19503 6.83938 2.13208 6.83938 2.06646Z" clip-rule="evenodd" />
                                </svg>
                            </span>
                        </span>
                    @else
                        <a href="{{ $paginator->previousPageUrl() }}" rel="prev" class="relative inline-flex items-center px-2 py-1 text-xs font-medium text-gray-500 bg-white border border-gray-300 rounded-l-md leading-5 hover:text-blue-400 focus:z-10 focus:outline-none focus:border-blue-300 active:bg-gray-100 active:text-gray-500 transition ease-in-out duration-150" aria-label="{{ __('pagination.previous') }}">
                            <!-- <svg class="w-5 h-5" fill="currentColor" viewBox="0 0 20 20">
                                <path fill-rule="evenodd" d="M12.707 5.293a1 1 0 010 1.414L9.414 10l3.293 3.293a1 1 0 01-1.414 1.414l-4-4a1 1 0 010-1.414l4-4a1 1 0 011.414 0z" clip-rule="evenodd" />
                            </svg> -->
                            <svg class="w-3 h-2" viewBox="0 0 7 12" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                <path fill-rule="evenodd" d="M6.83938 2.06646V1.03119C6.83938 0.941456 6.73625 0.891902 6.66661 0.946813L0.629107 5.66244C0.57781 5.70233 0.536302 5.75341 0.50775 5.81179C0.479198 5.87016 0.464355 5.93428 0.464355 5.99927C0.464355 6.06425 0.479198 6.12838 0.50775 6.18675C0.536302 6.24512 0.57781 6.29621 0.629107 6.3361L6.66661 11.0517C6.73759 11.1066 6.83938 11.0571 6.83938 10.9673V9.93208C6.83938 9.86645 6.80857 9.80351 6.75768 9.76333L1.93625 5.99994L6.75768 2.23521C6.80857 2.19503 6.83938 2.13208 6.83938 2.06646Z" clip-rule="evenodd" />
                            </svg>
                        </a>
                    @endif

                    {{-- Pagination Elements --}}
                    @foreach ($elements as $element)
                        {{-- "Three Dots" Separator --}}
                        @if (is_string($element))
                            <span aria-disabled="true">
                                <span class="relative inline-flex items-center px-2.5 py-1 -ml-px text-xs font-medium text-gray-700 bg-white border border-gray-300 cursor-default leading-5">{{ $element }}</span>
                            </span>
                        @endif

                        {{-- Array Of Links --}}
                        @if (is_array($element))
                            @foreach ($element as $page => $url)
                                @if ($page == $paginator->currentPage())
                                    <span aria-current="page">
                                        <span class="relative inline-flex items-center px-2.5 py-1 -ml-px text-xs font-medium text-gray-500 bg-white border border-gray-300 cursor-default leading-5">{{ $page }}</span>
                                    </span>
                                @else
                                    <a href="{{ $url }}" class="relative inline-flex items-center px-2.5 py-1 -ml-px text-xs font-medium text-gray-700 bg-white border border-gray-300 leading-5 hover:text-blue-500 hover:font-bold focus:z-10 focus:outline-none focus:border-blue-300 active:bg-gray-100 active:text-gray-700 transition ease-in-out duration-150" aria-label="{{ __('Go to page :page', ['page' => $page]) }}">
                                        {{ $page }}
                                    </a>
                                @endif
                            @endforeach
                        @endif
                    @endforeach

                    {{-- Next Page Link --}}
                    @if ($paginator->hasMorePages())
                        <a href="{{ $paginator->nextPageUrl() }}" rel="next" class="relative inline-flex items-center px-2 py-1 -ml-px text-xs font-medium text-gray-500 bg-white border border-gray-300 rounded-r-md leading-5 hover:text-blue-400 focus:z-10 focus:outline-none focus:border-blue-300 active:bg-gray-100 active:text-gray-500 transition ease-in-out duration-150" aria-label="{{ __('pagination.next') }}">
                            <!-- <svg class="w-5 h-5" fill="currentColor" viewBox="0 0 20 20">
                                <path fill-rule="evenodd" d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z" clip-rule="evenodd" />
                            </svg> -->
                            <svg class="w-3 h-2" viewBox="0 0 7 12" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                <path fill-rule="evenodd" d="M6.39777 5.66306L0.360269 0.947433C0.344493 0.935013 0.325533 0.927294 0.305568 0.925163C0.285603 0.923033 0.265441 0.926576 0.247399 0.935388C0.229357 0.944199 0.214166 0.95792 0.203571 0.974976C0.192976 0.992031 0.187406 1.01173 0.187501 1.03181V2.06708C0.187501 2.1327 0.218305 2.19565 0.269198 2.23583L5.09063 6.00056L0.269198 9.76529C0.216966 9.80547 0.187501 9.86842 0.187501 9.93404V10.9693C0.187501 11.059 0.290626 11.1086 0.360269 11.0537L6.39777 6.33806C6.44908 6.29803 6.4906 6.24683 6.51915 6.18835C6.5477 6.12986 6.56254 6.06564 6.56254 6.00056C6.56254 5.93548 6.5477 5.87125 6.51915 5.81277C6.4906 5.75429 6.44908 5.70309 6.39777 5.66306Z" clip-rule="evenodd" />
                            </svg>
                        </a>
                    @else
                        <span aria-disabled="true" aria-label="{{ __('pagination.next') }}">
                            <span class="relative inline-flex items-center px-2 py-2.5 -ml-px text-xs font-medium text-gray-500 bg-white border border-gray-300 cursor-default rounded-r-md leading-5" aria-hidden="true">
                                <!-- <svg class="w-5 h-5" fill="currentColor" viewBox="0 0 20 20">
                                    <path fill-rule="evenodd" d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z" clip-rule="evenodd" />
                                </svg> -->
                                <svg class="w-3 h-2" viewBox="0 0 7 12" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                    <path fill-rule="evenodd" d="M6.39777 5.66306L0.360269 0.947433C0.344493 0.935013 0.325533 0.927294 0.305568 0.925163C0.285603 0.923033 0.265441 0.926576 0.247399 0.935388C0.229357 0.944199 0.214166 0.95792 0.203571 0.974976C0.192976 0.992031 0.187406 1.01173 0.187501 1.03181V2.06708C0.187501 2.1327 0.218305 2.19565 0.269198 2.23583L5.09063 6.00056L0.269198 9.76529C0.216966 9.80547 0.187501 9.86842 0.187501 9.93404V10.9693C0.187501 11.059 0.290626 11.1086 0.360269 11.0537L6.39777 6.33806C6.44908 6.29803 6.4906 6.24683 6.51915 6.18835C6.5477 6.12986 6.56254 6.06564 6.56254 6.00056C6.56254 5.93548 6.5477 5.87125 6.51915 5.81277C6.4906 5.75429 6.44908 5.70309 6.39777 5.66306Z" clip-rule="evenodd" />
                                </svg>
                            </span>
                        </span>
                    @endif
                </span>
            </div>
        </div>
    </nav>
@endif
