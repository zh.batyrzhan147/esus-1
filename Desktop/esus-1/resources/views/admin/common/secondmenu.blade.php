<div class="flex">
    @if(!$popup)
    <a href="{{ route($addlink) }}" class="mb-2 text-sm flex justify-center items-center space-x-1.5 px-4 py-1 border-0 bg-blue-500 text-white rounded hover:bg-blue-600">
        <svg width="14" height="14" viewBox="0 0 14 14" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M7.71838 1.62408H6.78088C6.69755 1.62408 6.65588 1.66575 6.65588 1.74908V6.65533H2C1.91667 6.65533 1.875 6.697 1.875 6.78033V7.71783C1.875 7.80117 1.91667 7.84283 2 7.84283H6.65588V12.7491C6.65588 12.8324 6.69755 12.8741 6.78088 12.8741H7.71838C7.80172 12.8741 7.84338 12.8324 7.84338 12.7491V7.84283H12.5C12.5833 7.84283 12.625 7.80117 12.625 7.71783V6.78033C12.625 6.697 12.5833 6.65533 12.5 6.65533H7.84338V1.74908C7.84338 1.66575 7.80172 1.62408 7.71838 1.62408Z" fill="white"/>
        </svg>
        <span>{{ __('admin.buttons.create') }}</span>
    </a>
    @endif
</div>
