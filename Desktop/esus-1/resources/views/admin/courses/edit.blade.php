<x-admin-layout>
    <x-slot name="header">
        <div class="bg-white mx-auto py-6 px-4 sm:px-6 lg:px-8">
            <h2 class="font-semibold text-xl text-gray-800 leading-tight">
                Редактировать Образование
            </h2>
        </div>
    </x-slot>

    <p class="px-4 sm:px-6 lg:px-8 text-sm">Редактируйте нужные поля и сохраните данные</p>
    <div class="py-6 px-4 sm:px-6 lg:px-8 h-full">
        <form method="post" action="{{ route('admin.courses.update', $course->id) }}" class="flex flex-col items-start md:items-center w-full md:w-3/4 mx-auto space-y-4">
            @csrf
            @method('PUT')
            <livewire:admin.slug-generator  :course="$course"/>

            <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-center">
                <div class="w-full md:w-1/4">
                    <label for="type" class="w-full text-left md:text-right block text-sm">Тип курса:</label>
                </div>
                <div class="w-full md:w-3/4">
                    <select name="type" id="type"
                            class="w-full md:w-3/4 px-3 py-1 border rounded focus:border-blue-500 hover:border-blue-500 text-sm outline-none focus:input-shadow">
                        @foreach(\App\Models\Courses::listTypes() as $key => $type)
                            <option value="{{ $key }}" @if( ($course->type ?? '') == $key) selected @endif>{{ $type }}</option>
                        @endforeach
                    </select>
                    @error('type')
                    <p class="text-xs text-red-600">{{ $message }}</p>
                    @enderror
                </div>
            </div>

            <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-center">
                <div class="w-full md:w-1/4">
                    <label for="grading" class="w-full text-left md:text-right block text-sm">Оценивание:</label>
                </div>
                <div class="w-full md:w-3/4">
                    <select name="grading" id="grading"
                            class="w-full md:w-3/4 px-3 py-1 border rounded focus:border-blue-500 hover:border-blue-500 text-sm outline-none focus:input-shadow">
                        @foreach(\App\Models\Courses::listGrading() as $key => $grading)
                            <option value="{{ $key }}" @if( ($course->grading ?? '') == $key) selected @endif>{{ $grading }}</option>
                        @endforeach
                    </select>
                    @error('grading')
                    <p class="text-xs text-red-600">{{ $message }}</p>
                    @enderror
                </div>
            </div>

            <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-center">
                <div class="w-full md:w-1/4">
                    <label for="auto_generation" class="w-full text-left md:text-right block text-sm">Автогенерация расписания:</label>
                </div>
                <div class="w-full md:w-3/4">
                    <select name="auto_generation" id="auto_generation"
                            class="w-full md:w-3/4 px-3 py-1 border rounded focus:border-blue-500 hover:border-blue-500 text-sm outline-none focus:input-shadow">
                        <option value="1" @if( $course->auto_generation == true) selected @endif>Разрешено</option>
                        <option value="0" @if( $course->auto_generation == false) selected @endif>Запрещено</option>
                    </select>
                    @error('auto_generation')
                    <p class="text-xs text-red-600">{{ $message }}</p>
                    @enderror
                </div>
            </div>

            <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-center">
                <div class="w-full md:w-1/4">
                    <label for="course_length" class="w-full text-left md:text-right block text-sm">Продолжительность курса:</label>
                </div>
                <div class="w-full md:w-3/4">
                    <input type="text" name="course_length" id="course_length" class="w-full md:w-3/4 px-3 py-1 border rounded focus:border-blue-500 hover:border-blue-500 text-sm outline-none focus:input-shadow"
                           value="{{ old('course_length', $course->course_length) }}" />
                    @error('course_length')
                    <p class="text-xs text-red-600">{{ $message }}</p>
                    @enderror
                </div>
            </div>

            <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-center">
                <div class="w-full md:w-1/4">
                    <label for="webinar_count" class="w-full text-left md:text-right block text-sm">Колличество вебинаров:</label>
                </div>
                <div class="w-full md:w-3/4">
                    <input type="text" name="webinar_count" id="webinar_count" class="w-full md:w-3/4 px-3 py-1 border rounded focus:border-blue-500 hover:border-blue-500 text-sm outline-none focus:input-shadow"
                           value="{{ old('webinar_count', $course->webinar_count) }}" />
                    @error('webinar_count')
                    <p class="text-xs text-red-600">{{ $message }}</p>
                    @enderror
                </div>
            </div>

            <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-center">
                <div class="w-full md:w-1/4">
                    <label for="webinar_length" class="w-full text-left md:text-right block text-sm">Продолжительность вебинара в часах:</label>
                </div>
                <div class="w-full md:w-3/4">
                    <input type="text" name="webinar_length" id="webinar_length" class="w-full md:w-3/4 px-3 py-1 border rounded focus:border-blue-500 hover:border-blue-500 text-sm outline-none focus:input-shadow"
                           value="{{ old('webinar_length', $course->webinar_length) }}" />
                    @error('webinar_length')
                    <p class="text-xs text-red-600">{{ $message }}</p>
                    @enderror
                </div>
            </div>

            <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-center">
                <div class="w-full md:w-1/4">
                    <label for="file_1_listener" class="w-full text-left md:text-right block text-sm">Файл слушателя (Каз):</label>
                </div>
                <div class="w-full md:w-3/4">
                    <input type="text" name="file_1_listener" id="file_1_listener" class="w-full md:w-3/4 px-3 py-1 border rounded focus:border-blue-500 hover:border-blue-500 text-sm outline-none focus:input-shadow"
                           value="{{ old('file_1_listener', $course->file_1_listener) }}" />
                    @error('file_1_listener')
                    <p class="text-xs text-red-600">{{ $message }}</p>
                    @enderror
                </div>
            </div>

            <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-center">
                <div class="w-full md:w-1/4">
                    <label for="file_2_listener" class="w-full text-left md:text-right block text-sm">Файл слушателя (рус):</label>
                </div>
                <div class="w-full md:w-3/4">
                    <input type="text" name="file_2_listener" id="file_2_listener" class="w-full md:w-3/4 px-3 py-1 border rounded focus:border-blue-500 hover:border-blue-500 text-sm outline-none focus:input-shadow"
                           value="{{ old('file_2_listener', $course->file_2_listener) }}" />
                    @error('file_2_listener')
                    <p class="text-xs text-red-600">{{ $message }}</p>
                    @enderror
                </div>
            </div>

            <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-center">
                <div class="w-full md:w-1/4">
                    <label for="file_1_teacher" class="w-full text-left md:text-right block text-sm">Файл преподавателя (каз):</label>
                </div>
                <div class="w-full md:w-3/4">
                    <input type="text" name="file_1_teacher" id="file_1_teacher" class="w-full md:w-3/4 px-3 py-1 border rounded focus:border-blue-500 hover:border-blue-500 text-sm outline-none focus:input-shadow"
                           value="{{ old('file_1_teacher', $course->file_1_teacher) }}" />
                    @error('file_1_teacher')
                    <p class="text-xs text-red-600">{{ $message }}</p>
                    @enderror
                </div>
            </div>

            <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-center">
                <div class="w-full md:w-1/4">
                    <label for="file_2_teacher" class="w-full text-left md:text-right block text-sm">Файл преподавателя (рус):</label>
                </div>
                <div class="w-full md:w-3/4">
                    <input type="text" name="file_2_teacher" id="file_2_teacher" class="w-full md:w-3/4 px-3 py-1 border rounded focus:border-blue-500 hover:border-blue-500 text-sm outline-none focus:input-shadow"
                           value="{{ old('file_2_teacher', $course->file_2_teacher) }}" />
                    @error('file_2_teacher')
                    <p class="text-xs text-red-600">{{ $message }}</p>
                    @enderror
                </div>
            </div>


            <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-center">
                <div class="w-full md:w-1/4">
                    <label for="sdo_category" class="w-full text-left md:text-right block text-sm">Категория в СДО:</label>
                </div>
                <div class="w-full md:w-3/4">
                    <input type="text" name="sdo_category" id="sdo_category" class="w-full md:w-3/4 px-3 py-1 border rounded focus:border-blue-500 hover:border-blue-500 text-sm outline-none focus:input-shadow"
                           value="{{ old('sdo_category', $course->sdo_category) }}" />
                    @error('sdo_category')
                    <p class="text-xs text-red-600">{{ $message }}</p>
                    @enderror
                </div>
            </div>

            <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-center">
                <div class="w-full md:w-1/4">
                    <label for="sdo_link" class="w-full text-left md:text-right block text-sm">Ссылка в СДО:</label>
                </div>
                <div class="w-full md:w-3/4">
                    <input type="text" name="sdo_link" id="sdo_link" class="w-full md:w-3/4 px-3 py-1 border rounded focus:border-blue-500 hover:border-blue-500 text-sm outline-none focus:input-shadow"
                           value="{{ old('sdo_link', $course->sdo_link) }}" />
                    @error('sdo_link')
                    <p class="text-xs text-red-600">{{ $message }}</p>
                    @enderror
                </div>
            </div>

            <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-center">
                <div class="w-full md:w-1/4">
                    <label for="one_drive_link" class="w-full text-left md:text-right block text-sm">OneDrive ссылка:</label>
                </div>
                <div class="w-full md:w-3/4">
                    <input type="text" name="one_drive_link" id="one_drive_link" class="w-full md:w-3/4 px-3 py-1 border rounded focus:border-blue-500 hover:border-blue-500 text-sm outline-none focus:input-shadow"
                           value="{{ old('one_drive_link', $course->one_drive_link) }}" />
                    @error('one_drive_link')
                    <p class="text-xs text-red-600">{{ $message }}</p>
                    @enderror
                </div>
            </div>





            <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-center">
                <div class="w-full md:w-1/4">
                </div>
                <div class="w-full md:w-3/4 space-x-2 flex">
                    <button class="mb-2 text-sm flex justify-center items-center px-4 py-1 border-0 bg-blue-500 text-white rounded hover:bg-blue-600">Сохранить</button>
                    <a href="{{ route('admin.courses.index') }}" class="py-1 px-4 border inline-block m-0 mb-2 text-sm rounded hover:border-black">Отмена</a>
                </div>
            </div>
        </form>
    </div>
</x-admin-layout>
