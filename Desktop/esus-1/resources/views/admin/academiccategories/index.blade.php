<x-admin-layout>
    <x-slot name="header">
        <div class="bg-white mx-auto py-6 px-4 sm:px-6 lg:px-8">
            <div class="flex w-full justify-between">
                <h2 class="font-semibold text-xl text-gray-800 leading-tight">
                    {{ __('admin.academiccategories.index.title') }}
                </h2>
                @include('admin.common.secondmenu', [
                    'tabname'=>'employees',
                    'popup'=>false,
                    'addlink'=>'admin.academiccategories.create'
                ])
            </div>
        </div>
    </x-slot>

    <div class="py-6 px-4 sm:px-6 lg:px-8">
        <div class="flex flex-col">
            <div class="-my-2 overflow-x-auto sm:-mx-6 lg:-mx-8">
                <div class="py-2 align-middle inline-block min-w-full sm:px-6 lg:px-8">
                    <div class="shadow overflow-hidden border-b border-gray-200 sm:rounded">
                        <table class="min-w-full divide-y divide-gray-200 w-full">
                            <thead>
                            <tr>
                                <th scope="col" width="50" class="px-3 py-4 bg-gray-50 text-left text-sm font-medium text-gray-800">
                                    <div class="text-center">{{ __('admin.table.head.id') }}</div>
                                </th>
                                <th scope="col" class="px-2 py-3 bg-gray-50 text-left text-sm font-medium text-gray-800">
                                    <div class="pl-4 border-l border-gray-200">{{ __('admin.table.head.rutitle') }}</div>
                                </th>
                                <th scope="col" class="px-2 py-3 bg-gray-50 text-left text-sm font-medium text-gray-800">
                                    <div class="pl-4 border-l border-gray-200">{{ __('admin.table.head.kztitle') }}</div>
                                </th>
                                <th scope="col" width="200" class="px-2 py-3 bg-gray-50 text-left text-sm font-medium text-gray-800">
                                    <div class="pl-4 border-l border-gray-200">{{ __('admin.table.head.action') }}</div>
                                </th>
                            </tr>
                            </thead>
                            <tbody class="bg-white divide-y divide-gray-200">
                                @if (count($academicCategories) > 0)
                            @foreach ($academicCategories as $academicCategory)
                                <tr>
                                    <td class="px-6 py-1.5 whitespace-nowrap text-sm text-gray-900">
                                        {{ $academicCategory->id }}
                                    </td>

                                    <td class="px-6 py-1.5 whitespace-nowrap text-sm text-gray-900">
                                        {{ $academicCategory->ru_title }}
                                    </td>

                                    <td class="px-6 py-1.5 whitespace-nowrap text-sm text-gray-900">
                                        {{ $academicCategory->kz_title }}
                                    </td>

                                    <td class="px-6 py-1.5 whitespace-nowrap text-sm font-medium space-x-2 text-right">
                                        <a href="{{ route('admin.academiccategories.edit', $academicCategory->id) }}" class="text-blue-500 hover:text-blue-900 m-0 font-normal">{{ __('admin.links.edit') }}</a>
                                        <form class="inline-block" action="{{ route('admin.academiccategories.destroy', $academicCategory->id) }}" method="POST" onsubmit="return confirm('Are you sure?');">
                                            <input type="hidden" name="_method" value="DELETE">
                                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                            <input type="submit" class="text-red-500 hover:text-red-900 bg-none border-0 m-0 cursor-pointer bg-transparent font-normal border-l border-gray-200 pl-2" value="{{ __('admin.links.delete') }}">
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                                @else
                                <tr class="h-48 w-full"><td scope="col" class="h-48 w-full text-center text-xs text-gray-500" colspan="3">{{ __('admin.table.body.empty') }}</td></tr>
                                @endif
                            </tbody>
                        </table>
                    </div>
                    <div class="mt-4">
                        {{ $academicCategories->links('admin.common.pagination') }}
                    </div>
                </div>
            </div>
        </div>
    </div>
</x-admin-layout>
