<x-admin-layout>
    <x-slot name="header">
        <div class="bg-white mx-auto py-6 px-4 sm:px-6 lg:px-8">
            <h2 class="font-semibold text-xl text-gray-800 leading-tight">
                {{ __('admin.affiliates.create.title') }}
            </h2>
        </div>
    </x-slot>

    <p class="px-4 sm:px-6 lg:px-8 text-sm">{{ __('admin.affiliates.create.description') }}</p>
    <div class="py-6 px-4 sm:px-6 lg:px-8 h-full">
        <form method="post" action="{{ route('admin.affiliates.store') }}" class="flex flex-col items-start md:items-center w-full md:w-3/4 mx-auto space-y-4">
            @csrf
            <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-center">
                <div class="w-full md:w-1/4">
                    <label for="title" class="w-full text-left md:text-right block text-sm">{{ __('admin.affiliates.create.form.title') }}</label>
                </div>
                <div class="w-full md:w-3/4">
                    <input type="text" name="title" id="title" class="w-full md:w-3/4 px-3 py-1 border rounded focus:border-blue-500 hover:border-blue-500 text-sm outline-none focus:input-shadow" placeholder="{{ __('admin.affiliates.create.form.title.placeholder') }}" value="{{ old('title', '') }}" />
                    @error('title')
                        <p class="text-xs text-red-600">{{ $message }}</p>
                    @enderror
                </div>
            </div>

            <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-center">
                <div class="w-full md:w-1/4">
                    <label for="director" class="w-full text-left md:text-right block text-sm">{{ __('admin.affiliates.create.form.dir') }}</label>
                </div>
                <div class="w-full md:w-3/4">
                    <input type="text" name="director" id="director" class="w-full md:w-3/4 px-3 py-1 border rounded focus:border-blue-500 hover:border-blue-500 text-sm outline-none focus:input-shadow" placeholder="{{ __('admin.affiliates.create.form.dir.placeholder') }}" value="{{ old('director', '') }}" />
                    @error('director')
                        <p class="text-xs text-red-600">{{ $message }}</p>
                    @enderror
                </div>
            </div>

            <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-center">
                <div class="w-full md:w-1/4">
                    <label for="address" class="w-full text-left md:text-right block text-sm">{{ __('admin.affiliates.create.form.address') }}</label>
                </div>
                <div class="w-full md:w-3/4">
                    <input type="text" name="address" id="address" class="w-full md:w-3/4 px-3 py-1 border rounded focus:border-blue-500 hover:border-blue-500 text-sm outline-none focus:input-shadow" placeholder="{{ __('admin.affiliates.create.form.address.placeholder') }}" value="{{ old('address', '') }}" />
                    @error('address')
                        <p class="text-xs text-red-600">{{ $message }}</p>
                    @enderror
                </div>
            </div>

            <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-center">
                <div class="w-full md:w-1/4">
                    <label for="phone" class="w-full text-left md:text-right block text-sm">{{ __('admin.affiliates.create.form.phone') }}</label>
                </div>
                <div class="w-full md:w-3/4">
                    <input type="text" name="phone" id="phone" class="w-full md:w-3/4 px-3 py-1 border rounded focus:border-blue-500 hover:border-blue-500 text-sm outline-none focus:input-shadow" placeholder="{{ __('admin.affiliates.create.form.phone.placeholder') }}" value="{{ old('phone', '') }}" />
                    @error('phone')
                        <p class="text-xs text-red-600">{{ $message }}</p>
                    @enderror
                </div>
            </div>

            <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-center">
                <div class="w-full md:w-1/4">
                    <label for="email" class="w-full text-left md:text-right block text-sm">{{ __('admin.affiliates.create.form.email') }}</label>
                </div>
                <div class="w-full md:w-3/4">
                    <input type="text" name="email" id="email" class="w-full md:w-3/4 px-3 py-1 border rounded focus:border-blue-500 hover:border-blue-500 text-sm outline-none focus:input-shadow" placeholder="{{ __('admin.affiliates.create.form.email.placeholder') }}" value="{{ old('email', '') }}" />
                    @error('email')
                        <p class="text-xs text-red-600">{{ $message }}</p>
                    @enderror
                </div>
            </div>

            <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-center">
                <div class="w-full md:w-1/4">
                </div>
                <div class="w-full md:w-3/4 space-x-2 flex">
                    <button class="mb-2 text-sm flex justify-center items-center px-4 py-1 border-0 bg-blue-500 text-white rounded hover:bg-blue-600">{{ __('admin.buttons.create') }}</button>
                    <a href="{{ route('admin.affiliates.index') }}" class="py-1 px-4 border inline-block m-0 mb-2 text-sm rounded hover:border-black">{{ __('admin.buttons.cancel') }}</a>
                </div>
            </div>
        </form>
    </div>
</x-admin-layout>
