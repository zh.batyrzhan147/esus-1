<link href="https://unpkg.com/slim-select@1.27.0/dist/slimselect.min.css" rel="stylesheet"/>
<script src="https://unpkg.com/slim-select@1.27.0/dist/slimselect.min.js"></script>

<x-admin-layout>
    <x-slot name="header">
        <div class="bg-white mx-auto py-6 px-4 sm:px-6 lg:px-8">
            <h2 class="font-semibold text-xl text-gray-800 leading-tight">
                Создать Филиал
            </h2>
        </div>
    </x-slot>

    <p class="px-4 sm:px-6 lg:px-8 text-sm">Заполните все поля для создания программы</p>
    <div class="py-6 px-4 sm:px-6 lg:px-8 h-full">
        <form method="post" action="{{ route('admin.programs.update', $program->id) }}" class="flex flex-col items-start md:items-center w-full md:w-3/4 mx-auto space-y-4">
            @csrf
            @method('PUT')
            <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-center">
                <div class="w-full md:w-1/4">
                    <label for="title" class="w-full text-left md:text-right block text-sm">Название:</label>
                </div>
                <div class="w-full md:w-3/4">
                    <input type="text" name="title" id="title" class="w-full md:w-3/4 px-3 py-1 border rounded focus:border-blue-500 hover:border-blue-500 text-sm outline-none focus:input-shadow"  value="{{ old('title', $program->title) }}" />
                    @error('title')
                    <p class="text-xs text-red-600">{{ $message }}</p>
                    @enderror
                </div>
            </div>

            <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-center">
                <div class="w-full md:w-1/4">
                    <label for="slug" class="w-full text-left md:text-right block text-sm">ЧПУ:</label>
                </div>
                <div class="w-full md:w-3/4">
                    <input type="text" name="slug" id="slug" class="w-full md:w-3/4 px-3 py-1 border rounded focus:border-blue-500 hover:border-blue-500 text-sm outline-none focus:input-shadow"  value="{{ old('slug', $program->slug) }}" />
                    @error('slug')
                    <p class="text-xs text-red-600">{{ $message }}</p>
                    @enderror
                </div>
            </div>

            <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-center">
                <div class="w-full md:w-1/4">
                    <label for="start" class="w-full text-left md:text-right block text-sm">Начало:</label>
                </div>
                <div class="w-full md:w-3/4">
                    <input type="text" name="start" id="start" class="w-full md:w-3/4 px-3 py-1 border rounded focus:border-blue-500 hover:border-blue-500 text-sm outline-none focus:input-shadow"  value="{{ old('start', $program->start) }}" />
                    @error('start')
                    <p class="text-xs text-red-600">{{ $message }}</p>
                    @enderror
                </div>
            </div>

            <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-center">
                <div class="w-full md:w-1/4">
                    <label for="end" class="w-full text-left md:text-right block text-sm">Конец:</label>
                </div>
                <div class="w-full md:w-3/4">
                    <input type="text" name="end" id="end" class="w-full md:w-3/4 px-3 py-1 border rounded focus:border-blue-500 hover:border-blue-500 text-sm outline-none focus:input-shadow"  value="{{ old('end', $program->end) }}" />
                    @error('end')
                    <p class="text-xs text-red-600">{{ $message }}</p>
                    @enderror
                </div>
            </div>

            <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-center">
                <div class="w-full md:w-1/4">
                    <label for="year" class="w-full text-left md:text-right block text-sm">Учебный год:</label>
                </div>
                <div class="w-full md:w-3/4">
                    <input type="text" name="year" id="year" class="w-full md:w-3/4 px-3 py-1 border rounded focus:border-blue-500 hover:border-blue-500 text-sm outline-none focus:input-shadow"  value="{{ old('year', $program->year) }}" />
                    @error('year')
                    <p class="text-xs text-red-600">{{ $message }}</p>
                    @enderror
                </div>
            </div>


            <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-center">
                <div class="w-full md:w-1/4">
                    <label for="courses" class="w-full text-left md:text-right block text-sm">Курсы:</label>
                </div>
                <div class="w-full md:w-3/4">
                    <select x-data x-init="courses = new SlimSelect({select: $refs.courses, placeholder:'Select Courses'});"
                            name="courses[]" id="courses" x-ref="courses"
                            class="w-full md:w-3/4 px-3 py-1 border rounded focus:border-blue-500 hover:border-blue-500 text-sm outline-none focus:input-shadow" multiple="multiple">
                        @foreach($courses as $id => $course)
                            <option value="{{ $id }}"{{ in_array($id, old('course', $program->courses->pluck('id')->toArray())) ? ' selected' : '' }}>{{ $course }}</option>
                        @endforeach
                    </select>
                    @error('courses[]')
                    <p class="text-xs text-red-600">{{ $message }}</p>
                    @enderror
                </div>
            </div>


            <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-center">
                <div class="w-full md:w-1/4">
                </div>
                <div class="w-full md:w-3/4 space-x-2 flex">
                    <button class="mb-2 text-sm flex justify-center items-center px-4 py-1 border-0 bg-blue-500 text-white rounded hover:bg-blue-600">Сохранить</button>
                    <a href="{{ route('admin.programs.index') }}" class="py-1 px-4 border inline-block m-0 mb-2 text-sm rounded hover:border-black">Отмена</a>
                </div>
            </div>
        </form>
    </div>
</x-admin-layout>


