<x-admin-layout>
    <x-slot name="header">
        <div class="bg-white mx-auto py-6 px-4 sm:px-6 lg:px-8">
            <h2 class="font-semibold text-xl text-gray-800 leading-tight">
                Метаданные
            </h2>
        </div>
    </x-slot>

    @include('admin.common.secondmenu', [
        'tabname'=>'courses',
        'popup'=>false,
        'addlink'=>'admin.programs.create'
    ])

    <div class="py-6 px-4 sm:px-6 lg:px-8">
        <div class="flex flex-col">
            <div class="-my-2 overflow-x-auto sm:-mx-6 lg:-mx-8">
                <div class="py-2 align-middle inline-block min-w-full sm:px-6 lg:px-8">
                    <div class="shadow overflow-hidden border-b border-gray-200 sm:rounded">
                        <table class="min-w-full divide-y divide-gray-200 w-full">
                            <thead>
                            <tr>
                                <th scope="col" width="50" class="px-3 py-4 bg-gray-50 text-left text-sm font-medium text-gray-800">
                                    <div class="text-center">{{ __('admin.table.head.id') }}</div>
                                </th>
                                <th scope="col"
                                    class="px-2 py-3 bg-gray-50 text-left text-sm font-medium text-gray-800">
                                    <div class="pl-4">Название</div>
                                </th>
                                <th scope="col"
                                    class="px-2 py-3 bg-gray-50 text-left text-sm font-medium text-gray-800">
                                    <div class="pl-4 border-l border-gray-200">ЧПУ</div>
                                </th>
                                <th scope="col"
                                    class="px-2 py-3 bg-gray-50 text-left text-sm font-medium text-gray-800">
                                    <div class="pl-4 border-l border-gray-200">Начало</div>
                                </th>
                                <th scope="col"
                                    class="px-2 py-3 bg-gray-50 text-left text-sm font-medium text-gray-800">
                                    <div class="pl-4 border-l border-gray-200">Конец</div>
                                </th>
                                <th scope="col"
                                    class="px-2 py-3 bg-gray-50 text-left text-sm font-medium text-gray-800">
                                    <div class="pl-4 border-l border-gray-200">Учебный год</div>
                                </th>
                                <th scope="col" width="200"
                                    class="px-2 py-3 bg-gray-50 text-left text-sm font-medium text-gray-800">
                                    <div class="pl-4 border-l border-gray-200">{{ __('admin.table.head.action') }}</div>
                                </th>
                            </tr>
                            </thead>
                            <tbody class="bg-white divide-y divide-gray-200">
                            @if (count($programs) > 0)
                                @foreach ($programs as $program)
                                    <tr>
                                        <td class="px-6 py-1.5 whitespace-nowrap text-sm text-gray-900">
                                        {{ $program->id }}
                                        </td>

                                        <td class="px-6 py-1.5 whitespace-nowrap text-sm text-gray-900">
                                            {{ $program->title }}
                                        </td>

                                        <td class="px-6 py-1.5 whitespace-nowrap text-sm text-gray-900">
                                            {{ $program->slug }}
                                        </td>

                                        <td class="px-6 py-1.5 whitespace-nowrap text-sm text-gray-900">
                                            {{ $program->start }}
                                        </td>

                                        <td class="px-6 py-1.5 whitespace-nowrap text-sm text-gray-900">
                                            {{ $program->end }}
                                        </td>

                                        <td class="px-6 py-1.5 whitespace-nowrap text-sm text-gray-900">
                                            {{ $program->year }}
                                        </td>

                                        <td class="px-6 py-1.5 whitespace-nowrap text-sm font-medium space-x-2">
                                        <!-- <a href="{{ route('admin.programs.show', $program->id) }}" class="text-blue-600 hover:text-blue-900 mb-2 mr-2">View</a> -->
                                            <a href="{{ route('admin.programs.edit', $program->id) }}"
                                               class="text-blue-500 hover:text-blue-900 m-0 font-normal">{{ __('admin.links.edit') }}</a>
                                            <form class="inline-block"
                                                  action="{{ route('admin.programs.destroy', $program->id) }}"
                                                  method="POST" onsubmit="return confirm('Are you sure?');">
                                                <input type="hidden" name="_method" value="DELETE">
                                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                <input type="submit"
                                                       class="text-red-500 hover:text-red-900 bg-none border-0 m-0 cursor-pointer bg-transparent font-normal border-l border-gray-200 pl-2"
                                                       value="Удалить">
                                            </form>
                                        </td>
                                    </tr>
                                @endforeach
                            @else
                                <tr class="h-48 w-full">
                                    <td scope="col" class="h-48 w-full text-center text-xs text-gray-500" colspan="6">
                                        {{ __('admin.table.body.empty') }}
                                    </td>
                                </tr>
                            @endif
                            </tbody>
                        </table>
                    </div>
                    <div class="mt-4">
                        {{ $programs->links('admin.common.pagination') }}
                    </div>
                </div>
            </div>

        </div>
    </div>
</x-admin-layout>
