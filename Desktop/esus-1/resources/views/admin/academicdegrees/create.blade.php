<x-admin-layout>
    <x-slot name="header">
        <div class="bg-white mx-auto py-6 px-4 sm:px-6 lg:px-8">
            <h2 class="font-semibold text-xl text-gray-800 leading-tight">
                {{ __('admin.academicdegrees.create.title') }}
            </h2>
        </div>
    </x-slot>

    <p class="px-4 sm:px-6 lg:px-8 text-sm">{{ __('admin.academicdegrees.create.description') }}</p>
    <div class="py-6 px-4 sm:px-6 lg:px-8 h-full">
        <form method="post" action="{{ route('admin.academicdegrees.store') }}" class="flex flex-col items-start md:items-center w-full md:w-3/4 mx-auto space-y-4">
            @csrf
            <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-center">
                <div class="w-full md:w-1/4">
                    <label for="kz_title" class="w-full text-left md:text-right block text-sm">{{ __('admin.academicdegrees.create.form.kztitle') }}</label>
                </div>
                <div class="w-full md:w-3/4">
                    <input type="text" name="kz_title" id="kz_title" class="w-full md:w-3/4 px-3 py-1 border rounded focus:border-blue-500 hover:border-blue-500 text-sm outline-none focus:input-shadow" placeholder="{{ __('admin.academicdegrees.create.form.kztitle.placeholder') }}" value="{{ old('kz_title', '') }}" />
                    @error('kz_title')
                        <p class="text-xs text-red-600">{{ $message }}</p>
                    @enderror
                </div>
            </div>

            <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-center">
                <div class="w-full md:w-1/4">
                    <label for="ru_title" class="w-full text-left md:text-right block text-sm">{{ __('admin.academicdegrees.create.form.rutitle') }}</label>
                </div>
                <div class="w-full md:w-3/4">
                    <input type="text" name="ru_title" id="ru_title" class="w-full md:w-3/4 px-3 py-1 border rounded focus:border-blue-500 hover:border-blue-500 text-sm outline-none focus:input-shadow" placeholder="{{ __('admin.academicdegrees.create.form.rutitle.placeholder') }}" value="{{ old('ru_title', '') }}" />
                    @error('ru_title')
                        <p class="text-xs text-red-600">{{ $message }}</p>
                    @enderror
                </div>
            </div>

            <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-center">
                <div class="w-full md:w-1/4">
                </div>
                <div class="w-full md:w-3/4 space-x-2 flex">
                    <button class="mb-2 text-sm flex justify-center items-center px-4 py-1 border-0 bg-blue-500 text-white rounded hover:bg-blue-600">{{ __('admin.buttons.create') }}</button>
                    <a href="{{ route('admin.academicdegrees.index') }}" class="py-1 px-4 border inline-block m-0 mb-2 text-sm rounded hover:border-black">{{ __('admin.buttons.cancel') }}</a>
                </div>
            </div>
        </form>
    </div>
</x-admin-layout>
