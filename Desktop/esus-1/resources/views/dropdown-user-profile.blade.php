<div class="relative">
    <button
        class="flex text-main-black text-base font-normal space-x-2.5 items-center"
        @click="open = ! open" @click.away="open = false" @close.stop="open = false">
        <span class="flex">{{ Auth::user()->name ?? '' }}</span>
        <div class="flex space-x-1 items-center">
            <div class="w-10 h-10 rounded-full border-2 border-user-border bg-user-bg text-logo-blue text-base flex items-center justify-center">
                {{Str::limit((Auth::user()->name ?? ''), 1, $end='')}}{{Str::limit((Auth::user()->last_name ?? ''), 1, $end='')}}
            </div>
            <svg width="12" height="12" viewBox="0 0 12 12" fill="none" xmlns="http://www.w3.org/2000/svg" x-bind:class="open ? 'rotate-180' : ''">
                <path d="M5.99977 8.4C5.64977 8.4 5.29977 8.265 5.03477 8L1.77477 4.74C1.62977 4.595 1.62977 4.355 1.77477 4.21C1.91977 4.065 2.15977 4.065 2.30477 4.21L5.56477 7.47C5.80477 7.71 6.19477 7.71 6.43477 7.47L9.69477 4.21C9.83977 4.065 10.0798 4.065 10.2248 4.21C10.3698 4.355 10.3698 4.595 10.2248 4.74L6.96477 8C6.69977 8.265 6.34977 8.4 5.99977 8.4Z" fill="currentColor"/>
            </svg>
        </div>
    </button>
    <div class="absolute top-12 -left-10">
        <div x-show="open" x-transition:enter="transition ease-out duration-200"
             x-transition:enter-start="transform opacity-0 scale-95"
             x-transition:enter-end="transform opacity-100 scale-100"
             x-transition:leave="transition ease-in duration-75"
             x-transition:leave-start="transform opacity-100 scale-100"
             x-transition:leave-end="transform opacity-0 scale-95"
             class="absolute z-50 w-48 rounded shadow-user origin-top-right bg-white py-3 px-4" @click="open = false"
             style="display: none;">
            <div class="w-full flex flex-col space-y-3">
                @can(\App\Models\Permission::PERMISSION_SUPERADMIN)
                    <a href="{{ route('admin.homepage') }}" class="flex text-sm {{ request()->routeIs('admin.*') ? 'text-main-black': 'text-user-gray'}} hover:text-main-black">
                        Панель Администратора
                    </a>
                @endcan

                @can(\App\Models\Permission::PERMISSION_OMR)
                    <a href="{{ route('omr.index') }}" class="flex text-sm {{ request()->routeIs('admin.*') ? 'text-main-black' : 'text-user-gray'}} hover:text-main-black">
                        Панель ОМР
                    </a>
                @endcan
                @can(\App\Models\Permission::PERMISSION_OOP)
                    <a href="{{ route('oop.index') }}" class="flex text-sm {{ request()->routeIs('admin.*') ? 'text-main-black' : 'text-user-gray'}} hover:text-main-black">
                        Панель ООП
                    </a>
                @endcan
                @can(\App\Models\Permission::PERMISSION_OOUP)
                    <a href="{{ route('ooup.index') }}" class="flex text-sm {{ request()->routeIs('admin.*') ? 'text-main-black' : 'text-user-gray'}} hover:text-main-black">
                        Панель ООУП
                    </a>
                @endcan
                @can(\App\Models\Permission::PERMISSION_AFFILIATE)
    {{--                    <a href="{{ route('') }}" class="flex py-2.5 px-4 text-[#18181B] justify-center items-center rounded @if (request()->routeIs('affiliate.*')) bg-[#F4F4F5] @endif">Филлиал</a>--}}
                        <a href="{{ route('affiliate.homepage') }}" class="flex text-sm {{ request()->routeIs('affiliate.*') ? 'text-main-black' : 'text-user-gray'}} hover:text-main-black">
                            Панель Филлиала
                        </a>
                @endcan
                @can(\App\Models\Permission::PERMISSION_ACCOUNTANT)
                        {{--                    <a href="{{ route('') }}" class="flex py-2.5 px-4 text-[#18181B] justify-center items-center rounded @if (request()->routeIs('affiliate.*')) bg-[#F4F4F5] @endif">Филлиал</a>--}}
                        <a href="{{ route('accountant.index') }}" class="flex text-sm {{ request()->routeIs('accountant.*') ? 'text-main-black' : 'text-user-gray'}} hover:text-main-black">
                            Панель Бухгалтера
                        </a>
                @endcan

                <div class="w-full h-px bg-gray-bg"></div>

                <a href="{{ route('profile.show') }}" class="flex space-x-1.5 items-center text-sm {{ request()->routeIs('user.profile') ? 'text-main-black' : 'text-user-gray'}}  hover:text-main-black">
                    <svg width="16" height="16" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M8.10646 7.74666C8.08646 7.74666 8.07313 7.74666 8.05313 7.74666C8.01979 7.74 7.97313 7.74 7.93313 7.74666C5.99979 7.68666 4.53979 6.16666 4.53979 4.29333C4.53979 2.38666 6.09313 0.833328 7.99979 0.833328C9.90646 0.833328 11.4598 2.38666 11.4598 4.29333C11.4531 6.16666 9.98646 7.68666 8.12646 7.74666C8.11979 7.74666 8.11313 7.74666 8.10646 7.74666ZM7.99979 1.83333C6.64646 1.83333 5.5398 2.94 5.5398 4.29333C5.5398 5.62666 6.57979 6.7 7.90646 6.74666C7.93979 6.74 8.03313 6.74 8.11979 6.74666C9.42646 6.68666 10.4531 5.61333 10.4598 4.29333C10.4598 2.94 9.35313 1.83333 7.99979 1.83333Z" fill="currentColor"/>
                        <path d="M8.11307 15.0333C6.80641 15.0333 5.49307 14.7 4.49974 14.0333C3.57307 13.42 3.06641 12.58 3.06641 11.6667C3.06641 10.7533 3.57307 9.90667 4.49974 9.28667C6.49974 7.96001 9.73974 7.96001 11.7264 9.28667C12.6464 9.90001 13.1597 10.74 13.1597 11.6533C13.1597 12.5667 12.6531 13.4133 11.7264 14.0333C10.7264 14.7 9.41974 15.0333 8.11307 15.0333ZM5.05307 10.1267C4.41307 10.5533 4.06641 11.1 4.06641 11.6733C4.06641 12.24 4.41974 12.7867 5.05307 13.2067C6.71307 14.32 9.51307 14.32 11.1731 13.2067C11.8131 12.78 12.1597 12.2333 12.1597 11.66C12.1597 11.0933 11.8064 10.5467 11.1731 10.1267C9.51307 9.02 6.71307 9.02 5.05307 10.1267Z" fill="currentColor"/>
                    </svg>
                    <span>
                    {{ __('Профиль') }}
                    </span>
                </a>
                <!-- Authentication -->
                <form method="POST" action="{{ route('logout') }}">
                    @csrf

                    <button type="submit"
                       class="flex space-x-1.5 items-center text-sm text-user-gray hover:text-main-black">
                        <svg width="16" height="16" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M11.6266 10.2467C11.5 10.2467 11.3733 10.2 11.2733 10.1C11.08 9.90667 11.08 9.58667 11.2733 9.39334L12.6266 8.04001L11.2733 6.68667C11.08 6.49334 11.08 6.17334 11.2733 5.98001C11.4666 5.78667 11.7866 5.78667 11.98 5.98001L13.6866 7.68667C13.88 7.88001 13.88 8.20001 13.6866 8.39334L11.98 10.1C11.88 10.2 11.7533 10.2467 11.6266 10.2467Z" fill="currentColor"/>
                            <path d="M13.2867 8.53999H6.50671C6.23338 8.53999 6.00671 8.31333 6.00671 8.03999C6.00671 7.76666 6.23338 7.53999 6.50671 7.53999H13.2867C13.56 7.53999 13.7867 7.76666 13.7867 8.03999C13.7867 8.31333 13.56 8.53999 13.2867 8.53999Z" fill="currentColor"/>
                            <path d="M7.84005 13.8333C4.40671 13.8333 2.00671 11.4333 2.00671 8.00001C2.00671 4.56667 4.40671 2.16667 7.84005 2.16667C8.11338 2.16667 8.34005 2.39334 8.34005 2.66667C8.34005 2.94001 8.11338 3.16667 7.84005 3.16667C4.99338 3.16667 3.00671 5.15334 3.00671 8.00001C3.00671 10.8467 4.99338 12.8333 7.84005 12.8333C8.11338 12.8333 8.34005 13.06 8.34005 13.3333C8.34005 13.6067 8.11338 13.8333 7.84005 13.8333Z" fill="currentColor"/>
                        </svg>
                        <span>
                        {{ __('Выйти') }}
                        </span>
                    </button>
                </form>
            </div>
        </div>
    </div>
</div>
