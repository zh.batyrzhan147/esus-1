<header class="flex w-full bg-white fixed lg:relative bottom-0 z-99" x-data="{ open: false }">
    <div class="hidden lg:flex max-w-screen-xl w-full mx-auto py-3 px-2 justify-between items-center">
        <div>
            <a href="{{ route('dashboard') }}" class="flex text-2xl font-bold justify-center items-center space-x-2">
                <svg width="32" height="32" viewBox="0 0 32 32" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path fill-rule="evenodd" clip-rule="evenodd" d="M0 16C4.84192e-07 18.5263 2.03301 20.5593 6.09902 24.6253L7.37469 25.901C11.4407 29.967 13.4737 32 16 32C18.5263 32 20.5593 29.967 24.6253 25.901L25.901 24.6253C29.967 20.5593 32 18.5263 32 16C32 13.4737 29.967 11.4407 25.901 7.37468L24.6253 6.09901C20.5593 2.033 18.5263 0 16 0C13.4737 1.1079e-06 11.4407 2.03301 7.37468 6.09902L6.09901 7.37469C2.033 11.4407 -4.25901e-07 13.4737 0 16ZM8.74933 27.2713C11.5932 30.0868 13.3664 31.645 15.2976 31.946L20.2129 26.844L15.9291 25.5371L14.84 21.1806L8.74933 27.2713Z" fill="url(#paint0_linear_259_399)"/>
                    <defs>
                        <linearGradient id="paint0_linear_259_399" x1="10.6654" y1="-18.5891" x2="46.8224" y2="9.9351" gradientUnits="userSpaceOnUse">
                            <stop stop-color="#3B82F6"/>
                            <stop offset="1" stop-color="#1B55B5"/>
                        </linearGradient>
                    </defs>
                </svg>
                <span class="text-logo-blue">ESUS</span>
            </a>
        </div>
        <nav class="flex space-x-8">
            <a href="{{ route('user.profile') }}" class="flex {{ request()->routeIs('user.profile') ? 'text-main-black' : 'text-soft-gray' }} font-semibold hover:text-main-black">Обзор</a>
            <a href="{{ route('messages.list') }}" class="relative flex {{ request()->routeIs('messages.list') ? 'text-main-black' : 'text-soft-gray' }} font-semibold hover:text-main-black">
                <span>Сообщения</span>
                @if (auth()->user()->unreadNotifications->first())
                <span class="absolute top-0 -right-2 inline-block w-2 h-2 transform translate-x-1/2 -translate-y-1/2 bg-red-600 rounded-full"></span>
                @endif
            </a>
            <a href="{{ route('user.groups.active') }}" class="flex {{ (request()->routeIs('user.groups.*') or request()->routeIs('user.group.view')) ? 'text-main-black' : 'text-soft-gray' }} font-semibold hover:text-main-black">Мои курсы</a>
            <a href="{{ route('user.orders.list') }}" class="flex {{ (request()->routeIs('user.orders.*') or request()->routeIs('user.orders.list')) ? 'text-main-black' : 'text-soft-gray' }} font-semibold hover:text-main-black">Мои заказы</a>
            @can(\App\Models\Permission::PERMISSION_TRAINER)
            <a href="{{ route('trainer.groups.active') }}" class="flex {{ request()->routeIs('trainer.*') ? 'text-main-black' : 'text-soft-gray' }} font-semibold hover:text-main-black">Тренер</a>
            @endcan

            <div class="hidden lg:flex cursor-pointer">
                <livewire:set-locale/>
            </div>
        </nav>
        @include('dropdown-user-profile')
    </div>
    <div class="grid @cannot(\App\Models\Permission::PERMISSION_TRAINER) grid-cols-3 @endcannot @can(\App\Models\Permission::PERMISSION_TRAINER) grid-cols-4 @endcan lg:hidden w-full">
        <a href="{{ route('messages.list') }}" class="flex flex-col {{ request()->routeIs('messages.list') ? 'text-main-black' : 'text-soft-gray' }} font-normal text-xs hover:text-main-black py-3 px-1 justify-center items-center space-y-1.5">
            @if(request()->routeIs('messages.list'))
                <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M19.5 8C20.8807 8 22 6.88071 22 5.5C22 4.11929 20.8807 3 19.5 3C18.1193 3 17 4.11929 17 5.5C17 6.88071 18.1193 8 19.5 8Z" fill="currentColor"/>
                    <path d="M19.5 8C20.8807 8 22 6.88071 22 5.5C22 4.11929 20.8807 3 19.5 3C18.1193 3 17 4.11929 17 5.5C17 6.88071 18.1193 8 19.5 8Z" fill="currentColor"/>
                    <path d="M20.72 9.31C20.02 9.53 19.25 9.57 18.45 9.37C17.11 9.02 16.02 7.95 15.65 6.61C15.47 5.96 15.46 5.32 15.57 4.74C15.7 4.1 15.25 3.5 14.61 3.5H7C4 3.5 2 5 2 8.5V15.5C2 19 4 20.5 7 20.5H17C20 20.5 22 19 22 15.5V10.26C22 9.6 21.36 9.1 20.72 9.31ZM15.52 11.15L14.34 12.09C13.68 12.62 12.84 12.88 12 12.88C11.16 12.88 10.31 12.62 9.66 12.09L6.53 9.59C6.21 9.33 6.16 8.85 6.41 8.53C6.67 8.21 7.14 8.15 7.46 8.41L10.59 10.91C11.35 11.52 12.64 11.52 13.4 10.91L14.58 9.97C14.9 9.71 15.38 9.76 15.63 10.09C15.89 10.41 15.84 10.89 15.52 11.15Z" fill="currentColor"/>
                </svg>
            @else
            <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path d="M17 21.25H7C3.35 21.25 1.25 19.15 1.25 15.5V8.5C1.25 4.85 3.35 2.75 7 2.75H14C14.41 2.75 14.75 3.09 14.75 3.5C14.75 3.91 14.41 4.25 14 4.25H7C4.14 4.25 2.75 5.64 2.75 8.5V15.5C2.75 18.36 4.14 19.75 7 19.75H17C19.86 19.75 21.25 18.36 21.25 15.5V10.5C21.25 10.09 21.59 9.75 22 9.75C22.41 9.75 22.75 10.09 22.75 10.5V15.5C22.75 19.15 20.65 21.25 17 21.25Z" fill="currentColor"/>
                <path d="M11.9998 12.87C11.1598 12.87 10.3098 12.61 9.65978 12.08L6.52978 9.58002C6.20978 9.32002 6.14978 8.85002 6.40978 8.53002C6.66978 8.21002 7.13977 8.15003 7.45977 8.41003L10.5898 10.91C11.3498 11.52 12.6398 11.52 13.3998 10.91L14.5798 9.97002C14.8998 9.71002 15.3798 9.76002 15.6298 10.09C15.8898 10.41 15.8398 10.89 15.5098 11.14L14.3298 12.08C13.6898 12.61 12.8398 12.87 11.9998 12.87Z" fill="currentColor"/>
                <path d="M19.5 8.75C17.71 8.75 16.25 7.29 16.25 5.5C16.25 3.71 17.71 2.25 19.5 2.25C21.29 2.25 22.75 3.71 22.75 5.5C22.75 7.29 21.29 8.75 19.5 8.75ZM19.5 3.75C18.54 3.75 17.75 4.54 17.75 5.5C17.75 6.46 18.54 7.25 19.5 7.25C20.46 7.25 21.25 6.46 21.25 5.5C21.25 4.54 20.46 3.75 19.5 3.75Z" fill="currentColor"/>
            </svg>
            @endif
            <span>Сообщения</span>
        </a>
        <a href="{{ route('user.groups.active') }}" class="flex flex-col {{ (request()->routeIs('user.groups.*') or request()->routeIs('user.group.view')) ? 'text-main-black' : 'text-soft-gray' }} font-normal text-xs hover:text-main-black py-3 px-1 justify-center items-center space-y-1.5">
            @if(request()->routeIs('user.groups.*') or request()->routeIs('user.group.view'))
                <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M19.11 4.96C15.2 1.05 8.85 1.05 4.94 4.96C0.959996 8.94 1.03 15.43 5.14 19.33C8.94 22.92 15.1 22.92 18.9 19.33C23.02 15.43 23.09 8.94 19.11 4.96ZM16.38 16.65C15.18 17.79 13.6 18.36 12.02 18.36C10.44 18.36 8.86 17.79 7.66 16.65C7.36 16.36 7.35 15.89 7.63 15.59C7.92 15.29 8.39 15.28 8.69 15.56C10.52 17.29 13.51 17.3 15.35 15.56C15.65 15.28 16.13 15.29 16.41 15.59C16.7 15.89 16.68 16.36 16.38 16.65Z" fill="currentColor"/>
                </svg>
            @else
            <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path d="M12.0001 22.72C9.33006 22.72 6.65005 21.76 4.62005 19.83C2.49005 17.81 1.29006 15.09 1.25006 12.17C1.21006 9.23997 2.33005 6.48997 4.40005 4.41997C8.59005 0.229966 15.4101 0.229966 19.6001 4.41997C21.6701 6.48997 22.7901 9.23997 22.7501 12.17C22.7101 15.1 21.5101 17.82 19.3801 19.83C17.3501 21.76 14.6701 22.72 12.0001 22.72ZM12.0001 2.78001C9.63006 2.78001 7.26005 3.67996 5.46005 5.47996C3.68005 7.25996 2.72006 9.63001 2.75006 12.15C2.78006 14.67 3.81005 17.01 5.65005 18.74C9.15005 22.06 14.8501 22.05 18.3501 18.74C20.1801 17.01 21.2101 14.66 21.2501 12.15C21.2901 9.64001 20.3201 7.25996 18.5401 5.47996C16.7401 3.67996 14.3701 2.78001 12.0001 2.78001Z" fill="currentColor"/>
                <path d="M11.9998 18.32C10.4198 18.32 8.84975 17.75 7.64975 16.62C7.34975 16.33 7.33975 15.86 7.61975 15.56C7.90975 15.26 8.37975 15.25 8.67975 15.53C10.5097 17.26 13.4898 17.26 15.3198 15.53C15.6198 15.25 16.0998 15.26 16.3798 15.56C16.6598 15.86 16.6498 16.34 16.3498 16.62C15.1498 17.75 13.5798 18.32 11.9998 18.32Z" fill="currentColor"/>
            </svg>
            @endif
            <span>Мои курсы</span>
        </a>
        @can(\App\Models\Permission::PERMISSION_TRAINER)
        <a href="{{ route('trainer.groups.active') }}" class="flex flex-col {{ request()->routeIs('trainer.*') ? 'text-main-black' : 'text-soft-gray' }} font-normal text-xs hover:text-main-black py-3 px-1 justify-center items-center space-y-1.5">
            @if(request()->routeIs('trainer.*'))
                <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M19.12 9.11999C18.73 9.11999 18.42 9.42999 18.42 9.81999V11.4C18.42 14.94 15.54 17.82 12 17.82C8.45999 17.82 5.57999 14.94 5.57999 11.4V9.80999C5.57999 9.41999 5.26999 9.10999 4.87999 9.10999C4.48999 9.10999 4.17999 9.41999 4.17999 9.80999V11.39C4.17999 15.46 7.30999 18.81 11.3 19.17V21.3C11.3 21.69 11.61 22 12 22C12.39 22 12.7 21.69 12.7 21.3V19.17C16.68 18.82 19.82 15.46 19.82 11.39V9.80999C19.81 9.42999 19.5 9.11999 19.12 9.11999Z" fill="currentColor"/>
                    <path d="M12 2C9.55999 2 7.57999 3.98 7.57999 6.42V11.54C7.57999 13.98 9.55999 15.96 12 15.96C14.44 15.96 16.42 13.98 16.42 11.54V6.42C16.42 3.98 14.44 2 12 2ZM13.31 8.95C13.24 9.21 13.01 9.38 12.75 9.38C12.7 9.38 12.65 9.37 12.6 9.36C12.21 9.25 11.8 9.25 11.41 9.36C11.09 9.45 10.78 9.26 10.7 8.95C10.61 8.64 10.8 8.32 11.11 8.24C11.7 8.08 12.32 8.08 12.91 8.24C13.21 8.32 13.39 8.64 13.31 8.95ZM13.84 7.01C13.75 7.25 13.53 7.39 13.29 7.39C13.22 7.39 13.16 7.38 13.09 7.36C12.39 7.1 11.61 7.1 10.91 7.36C10.61 7.47 10.27 7.31 10.16 7.01C10.05 6.71 10.21 6.37 10.51 6.27C11.47 5.92 12.53 5.92 13.49 6.27C13.79 6.38 13.95 6.71 13.84 7.01Z" fill="currentColor"/>
                </svg>
            @else
                <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M12 16.25C9.38 16.25 7.25 14.12 7.25 11.5V6C7.25 3.38 9.38 1.25 12 1.25C14.62 1.25 16.75 3.38 16.75 6V11.5C16.75 14.12 14.62 16.25 12 16.25ZM12 2.75C10.21 2.75 8.75 4.21 8.75 6V11.5C8.75 13.29 10.21 14.75 12 14.75C13.79 14.75 15.25 13.29 15.25 11.5V6C15.25 4.21 13.79 2.75 12 2.75Z" fill="currentColor"/>
                    <path d="M12 19.75C7.37 19.75 3.6 15.98 3.6 11.35V9.65002C3.6 9.24002 3.94 8.90002 4.35 8.90002C4.76 8.90002 5.1 9.24002 5.1 9.65002V11.35C5.1 15.15 8.2 18.25 12 18.25C15.8 18.25 18.9 15.15 18.9 11.35V9.65002C18.9 9.24002 19.24 8.90002 19.65 8.90002C20.06 8.90002 20.4 9.24002 20.4 9.65002V11.35C20.4 15.98 16.63 19.75 12 19.75Z" fill="currentColor"/>
                    <path d="M13.39 7.18001C13.31 7.18001 13.22 7.17001 13.13 7.14001C12.4 6.87001 11.6 6.87001 10.87 7.14001C10.48 7.28001 10.05 7.08001 9.91 6.69001C9.77 6.30001 9.97 5.87001 10.36 5.73001C11.42 5.35001 12.59 5.35001 13.65 5.73001C14.04 5.87001 14.24 6.30001 14.1 6.69001C13.98 6.99001 13.69 7.18001 13.39 7.18001Z" fill="currentColor"/>
                    <path d="M12.8 9.30001C12.73 9.30001 12.67 9.29001 12.6 9.27001C12.2 9.16001 11.79 9.16001 11.39 9.27001C10.99 9.38001 10.58 9.14001 10.47 8.74001C10.36 8.35001 10.6 7.94001 11 7.83001C11.65 7.65001 12.35 7.65001 13 7.83001C13.4 7.94001 13.64 8.35001 13.53 8.75001C13.44 9.08001 13.13 9.30001 12.8 9.30001Z" fill="currentColor"/>
                    <path d="M12 22.75C11.59 22.75 11.25 22.41 11.25 22V19C11.25 18.59 11.59 18.25 12 18.25C12.41 18.25 12.75 18.59 12.75 19V22C12.75 22.41 12.41 22.75 12 22.75Z" fill="currentColor"/>
                </svg>
            @endif
            <span>Тренер</span>
        </a>
        @endcan
        <a href="{{ route('profile.show') }}" class="flex flex-col {{ request()->routeIs('user.profile') ? 'text-main-black' : 'text-soft-gray' }} font-normal text-xs hover:text-main-black py-3 px-1 justify-center items-center space-y-1.5">
            @if(request()->routeIs('user.profile'))
            <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path d="M12 2C9.38 2 7.25 4.13 7.25 6.75C7.25 9.32 9.26 11.4 11.88 11.49C11.96 11.48 12.04 11.48 12.1 11.49C12.12 11.49 12.13 11.49 12.15 11.49C12.16 11.49 12.16 11.49 12.17 11.49C14.73 11.4 16.74 9.32 16.75 6.75C16.75 4.13 14.62 2 12 2Z" fill="currentColor"/>
                <path d="M17.08 14.15C14.29 12.29 9.74001 12.29 6.93001 14.15C5.66001 15 4.96001 16.15 4.96001 17.38C4.96001 18.61 5.66001 19.75 6.92001 20.59C8.32001 21.53 10.16 22 12 22C13.84 22 15.68 21.53 17.08 20.59C18.34 19.74 19.04 18.6 19.04 17.36C19.03 16.13 18.34 14.99 17.08 14.15Z" fill="currentColor"/>
            </svg>
            @else
            <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path d="M12.1596 11.62C12.1296 11.62 12.1096 11.62 12.0796 11.62C12.0296 11.61 11.9596 11.61 11.8996 11.62C8.9996 11.53 6.8096 9.25 6.8096 6.44C6.8096 3.58 9.1396 1.25 11.9996 1.25C14.8596 1.25 17.1896 3.58 17.1896 6.44C17.1796 9.25 14.9796 11.53 12.1896 11.62C12.1796 11.62 12.1696 11.62 12.1596 11.62ZM11.9996 2.75C9.9696 2.75 8.3096 4.41 8.3096 6.44C8.3096 8.44 9.8696 10.05 11.8596 10.12C11.9096 10.11 12.0496 10.11 12.1796 10.12C14.1396 10.03 15.6796 8.42 15.6896 6.44C15.6896 4.41 14.0296 2.75 11.9996 2.75Z" fill="currentColor"/>
                <path d="M12.1696 22.55C10.2096 22.55 8.23961 22.05 6.74961 21.05C5.35961 20.13 4.59961 18.87 4.59961 17.5C4.59961 16.13 5.35961 14.86 6.74961 13.93C9.74961 11.94 14.6096 11.94 17.5896 13.93C18.9696 14.85 19.7396 16.11 19.7396 17.48C19.7396 18.85 18.9796 20.12 17.5896 21.05C16.0896 22.05 14.1296 22.55 12.1696 22.55ZM7.57961 15.19C6.61961 15.83 6.09961 16.65 6.09961 17.51C6.09961 18.36 6.62961 19.18 7.57961 19.81C10.0696 21.48 14.2696 21.48 16.7596 19.81C17.7196 19.17 18.2396 18.35 18.2396 17.49C18.2396 16.64 17.7096 15.82 16.7596 15.19C14.2696 13.53 10.0696 13.53 7.57961 15.19Z" fill="currentColor"/>
            </svg>
            @endif
            <span>Профиль</span>
        </a>
    </div>
</header>
