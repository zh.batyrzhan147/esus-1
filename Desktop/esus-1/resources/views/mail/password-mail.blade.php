{{--@php--}}
{{--    $new = false;--}}
{{--    $iin = '11111';--}}
{{--    $password = 'asdasdasd-asdasdsad';--}}
{{--@endphp--}}
<!DOCTYPE html>
<html>
<head>
    <title>Электронная система учета слушателей</title>
</head>
<body>
    <div marginheight="0" marginwidth="0" style="width:100%!important;margin:0;padding:0">
        <center>
            <table border="0" cellpadding="8" cellspacing="0" style="background-color:#ffffff;background:#ffffff;width:100%!important;margin:0;padding:0">
                <tbody>
                    <tr>
                        <td valign="top">
                            <table align="center" border="0" cellpadding="0" cellspacing="0" style="line-height:25px">
                                <tbody>
                                    <tr><td colspan="3" height="36"></td></tr>
                                    <tr>
                                        <td width="36"></td>
                                        <td>
                                        <td align="left" style="font-size:11pt;color:#444444;font-family:proxima_nova,'Open Sans','Lucida Grande','Segoe UI',Arial,Verdana,'Lucida Sans Unicode',Tahoma,'Sans Serif';border-collapse:collapse" valign="top" width="540">
                                            <table border="0" cellpadding="0" cellspacing="0" style="width:100%;font-family:-apple-system,BlinkMacSystemFont,Helvetica Neue,Helvetica,Segoe UI,Arial,sans-serif,Apple Color Emoji,Segoe UI Emoji,Segoe UI Symbol">
                                                <tbody>
                                                    <tr>
                                                        <td align="center">
                                                            <table border="0" cellpadding="0" cellspacing="0" style="width:547px;min-width:547px">
                                                                <tbody>
                                                                    <tr>
                                                                        <td>
                                                                            <svg width="104" height="32" viewBox="0 0 104 32" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                                                <path d="M41.517 25V7.54545H53.2784V10.5881H45.2074V14.7472H52.6733V17.7898H45.2074V21.9574H53.3125V25H41.517ZM65.6811 12.5653C65.6129 11.8778 65.3203 11.3437 64.8033 10.9631C64.2862 10.5824 63.5845 10.392 62.6982 10.392C62.0959 10.392 61.5874 10.4773 61.1726 10.6477C60.7578 10.8125 60.4396 11.0426 60.218 11.3381C60.0021 11.6335 59.8942 11.9688 59.8942 12.3438C59.8828 12.6562 59.9482 12.929 60.0902 13.1619C60.2379 13.3949 60.4396 13.5966 60.6953 13.767C60.951 13.9318 61.2464 14.0767 61.5817 14.2017C61.9169 14.321 62.2749 14.4233 62.6555 14.5085L64.2237 14.8835C64.9851 15.054 65.6839 15.2812 66.3203 15.5653C66.9567 15.8494 67.5078 16.1989 67.9737 16.6136C68.4396 17.0284 68.8004 17.517 69.0561 18.0795C69.3175 18.642 69.451 19.2869 69.4567 20.0142C69.451 21.0824 69.1783 22.0085 68.6385 22.7926C68.1044 23.571 67.3317 24.1761 66.3203 24.608C65.3146 25.0341 64.1016 25.2472 62.6811 25.2472C61.272 25.2472 60.0447 25.0312 58.9993 24.5994C57.9595 24.1676 57.147 23.5284 56.5618 22.6818C55.9822 21.8295 55.6783 20.7756 55.6499 19.5199H59.2209C59.2607 20.1051 59.4283 20.5937 59.7237 20.9858C60.0249 21.3722 60.4254 21.6648 60.9254 21.8636C61.4311 22.0568 62.0021 22.1534 62.6385 22.1534C63.2635 22.1534 63.8061 22.0625 64.2663 21.8807C64.7322 21.6989 65.093 21.446 65.3487 21.1222C65.6044 20.7983 65.7322 20.4261 65.7322 20.0057C65.7322 19.6136 65.6158 19.2841 65.3828 19.017C65.1555 18.75 64.8203 18.5227 64.3771 18.3352C63.9396 18.1477 63.4027 17.9773 62.7663 17.8239L60.8658 17.3466C59.3942 16.9886 58.2322 16.429 57.38 15.6676C56.5277 14.9062 56.1044 13.8807 56.1101 12.5909C56.1044 11.5341 56.3857 10.6108 56.9538 9.82102C57.5277 9.03125 58.3146 8.41477 59.3146 7.97159C60.3146 7.52841 61.451 7.30682 62.7237 7.30682C64.0192 7.30682 65.1499 7.52841 66.1158 7.97159C67.0874 8.41477 67.843 9.03125 68.3828 9.82102C68.9226 10.6108 69.201 11.5256 69.218 12.5653H65.6811ZM82.7202 7.54545H86.4105V18.8807C86.4105 20.1534 86.1065 21.267 85.4986 22.2216C84.8963 23.1761 84.0526 23.9205 82.9673 24.4545C81.8821 24.983 80.6179 25.2472 79.1747 25.2472C77.7259 25.2472 76.4588 24.983 75.3736 24.4545C74.2884 23.9205 73.4446 23.1761 72.8423 22.2216C72.2401 21.267 71.9389 20.1534 71.9389 18.8807V7.54545H75.6293V18.5653C75.6293 19.2301 75.7741 19.821 76.0639 20.3381C76.3594 20.8551 76.7741 21.2614 77.3082 21.5568C77.8423 21.8523 78.4645 22 79.1747 22C79.8906 22 80.5128 21.8523 81.0412 21.5568C81.5753 21.2614 81.9872 20.8551 82.277 20.3381C82.5724 19.821 82.7202 19.2301 82.7202 18.5653V7.54545ZM98.9155 12.5653C98.8473 11.8778 98.5547 11.3437 98.0376 10.9631C97.5206 10.5824 96.8189 10.392 95.9325 10.392C95.3303 10.392 94.8217 10.4773 94.407 10.6477C93.9922 10.8125 93.674 11.0426 93.4524 11.3381C93.2365 11.6335 93.1286 11.9688 93.1286 12.3438C93.1172 12.6562 93.1825 12.929 93.3246 13.1619C93.4723 13.3949 93.674 13.5966 93.9297 13.767C94.1854 13.9318 94.4808 14.0767 94.8161 14.2017C95.1513 14.321 95.5092 14.4233 95.8899 14.5085L97.4581 14.8835C98.2195 15.054 98.9183 15.2812 99.5547 15.5653C100.191 15.8494 100.742 16.1989 101.208 16.6136C101.674 17.0284 102.035 17.517 102.29 18.0795C102.552 18.642 102.685 19.2869 102.691 20.0142C102.685 21.0824 102.413 22.0085 101.873 22.7926C101.339 23.571 100.566 24.1761 99.5547 24.608C98.549 25.0341 97.3359 25.2472 95.9155 25.2472C94.5064 25.2472 93.2791 25.0312 92.2337 24.5994C91.1939 24.1676 90.3814 23.5284 89.7962 22.6818C89.2166 21.8295 88.9126 20.7756 88.8842 19.5199H92.4553C92.495 20.1051 92.6626 20.5937 92.9581 20.9858C93.2592 21.3722 93.6598 21.6648 94.1598 21.8636C94.6655 22.0568 95.2365 22.1534 95.8729 22.1534C96.4979 22.1534 97.0405 22.0625 97.5007 21.8807C97.9666 21.6989 98.3274 21.446 98.5831 21.1222C98.8388 20.7983 98.9666 20.4261 98.9666 20.0057C98.9666 19.6136 98.8501 19.2841 98.6172 19.017C98.3899 18.75 98.0547 18.5227 97.6115 18.3352C97.174 18.1477 96.6371 17.9773 96.0007 17.8239L94.1001 17.3466C92.6286 16.9886 91.4666 16.429 90.6143 15.6676C89.7621 14.9062 89.3388 13.8807 89.3445 12.5909C89.3388 11.5341 89.62 10.6108 90.1882 9.82102C90.7621 9.03125 91.549 8.41477 92.549 7.97159C93.549 7.52841 94.6854 7.30682 95.9581 7.30682C97.2536 7.30682 98.3842 7.52841 99.3501 7.97159C100.322 8.41477 101.077 9.03125 101.617 9.82102C102.157 10.6108 102.435 11.5256 102.452 12.5653H98.9155Z" fill="#1B55B5"/>
                                                                                <path fill-rule="evenodd" clip-rule="evenodd" d="M0 16C4.84192e-07 18.5263 2.03301 20.5593 6.09902 24.6253L7.37469 25.901C11.4407 29.967 13.4737 32 16 32C18.5263 32 20.5593 29.967 24.6253 25.901L25.901 24.6253C29.967 20.5593 32 18.5263 32 16C32 13.4737 29.967 11.4407 25.901 7.37468L24.6253 6.09901C20.5593 2.033 18.5263 0 16 0C13.4737 1.1079e-06 11.4407 2.03301 7.37468 6.09902L6.09901 7.37469C2.033 11.4407 -4.25901e-07 13.4737 0 16ZM8.74933 27.2713C11.5932 30.0868 13.3664 31.645 15.2976 31.946L20.2129 26.844L15.9291 25.5371L14.84 21.1806L8.74933 27.2713Z" fill="url(#paint0_linear_31_4529)"/>
                                                                                <defs>
                                                                                    <linearGradient id="paint0_linear_31_4529" x1="10.6654" y1="-18.5891" x2="46.8224" y2="9.9351" gradientUnits="userSpaceOnUse">
                                                                                        <stop stop-color="#3B82F6"/>
                                                                                        <stop offset="1" stop-color="#1B55B5"/>
                                                                                    </linearGradient>
                                                                                </defs>
                                                                            </svg>
                                                                        </td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="center">
                                                            <table border="0" cellpadding="0" cellspacing="0" style="width:547px;min-width:547px;padding-top:16px">
                                                                <tbody>
                                                                    <tr>
                                                                        <td style="font-size:10.5pt;color:#000000">
                                                                            @if($new)
                                                                                <span style="font-size:36px;line-height:32px"><b>Поздравляем</b> с успешной регистрацией в <b>Электронной системе учета слушателей</b>!</span><br>
                                                                            @else
                                                                                <span style="font-size:36px;line-height:32px">Вы запросили <b>сброс пароля</b>!</span>
                                                                            @endif
                                                                        </td>
                                                                    </tr>
                                                                <tr><td style="padding-top:4px"><span style="font-size:15px;color:#666666">Для доступа в системе используйте следующие данные:</span></td></tr>
                                                                </tbody>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="left">
                                                            <table border="0" cellpadding="0" cellspacing="0" style="width:500px;min-width:500px">
                                                                <tbody>
                                                                    <tr>
                                                                        <td align="center">
                                                                            <table border="0" cellpadding="0" cellspacing="0" style="width:500px;min-width:500px">
                                                                                <tbody>
                                                                                    <tr><td height="32" style="height:32px;min-height:32px"></td></tr>
                                                                                    <tr>
                                                                                        <td align="left" style="vertical-align:middle;max-width:500px;white-space:nowrap;overflow:hidden">
                                                                                            <div style="text-align:left;text-decoration:none;line-height:24px;font-size:14pt;color:#000000" target="_blank">ИИН: <span style="font-size:18pt;font-weight:bold;">{{ $iin }}</span></div>
                                                                                            <div style="text-align:left;text-decoration:none;line-height:24px;font-size:14pt;color:#000000" target="_blank">Пароль: <span style="font-size:18pt;font-weight:bold;">{{ $password }}</span></div>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr><td height="32" style="height:32px;min-height:32px"></td></tr>
                                                                                </tbody>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr><td align="center"><table border="0" cellpadding="0" cellspacing="0" style="width:500px;min-width:500px"><tbody><tr><td align="left" style="padding-top:16px;border-bottom:1px solid #dddfe1"><div style="width:0px;height:0px"></div></td></tr></tbody></table></td></tr>
                                                    <tr><td colspan="3" height="36"></td></tr>
                                                    <tr>
                                                        <td align="center" style="font-size:0px;padding:15px 0px 20px 0px;word-break:break-word">
                                                            <table cellpadding="0" cellspacing="0" role="presentation" style="border-collapse:separate;width:auto;line-height:100%">
                                                                <tbody>
                                                                <tr>
                                                                    <td align="center" bgcolor="#1B55B5" role="presentation" style="border:0px solid #000;border-radius:5px;background:#1B55B5" valign="middle">
                                                                        <a href="https://esus.kz/login" style="display:inline-block;background:#1B55B5;color:#ffffff;font-family:Helvetica,sans-serif;font-size:16px;font-weight:normal;line-height:100%;Margin:0;text-decoration:none;text-transform:none;padding:11px 32px 11px 32px;border-radius:5px" target="_blank" data-saferedirecturl="https://www.google.com/url?q=https://esus.kz/login">
                                                                            <div>
                                                                                <span style="font-size:16px;font-family:helvetica,arial,sans-serif">Войти в систему</span>
                                                                            </div>
                                                                        </a>
                                                                    </td>
                                                                </tr>
                                                                </tbody>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </td>
                                        <td width="36"></td>
                                    </tr>
                                    <tr><td colspan="3" height="36"></td></tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                </tbody>
            </table>
        </center>
    </div>
</body>
</html>
