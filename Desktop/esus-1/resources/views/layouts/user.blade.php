<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>{{ config('app.name', 'ESUS') }}</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css2?family=Inter:wght@300;400;500;600;700&display=swap" rel="stylesheet">

        <!-- Styles -->
        <link rel="stylesheet" href="{{ mix('css/app.css') }}">
        @stack('styles')
        @livewireStyles

        <!-- Scripts -->
        <script src="{{ mix('js/app.js') }}" defer></script>
    </head>
    <body class="font-inter bg-gray-bg">
        @include('navigation-user')
        <!-- Page Content -->
        <main class="flex flex-col max-w-screen-xl w-full mx-auto lg:my-8">
            {{ $slot }}
        </main>
        @include('footer')
        @livewireScripts
        @stack('modals')
        @stack('scripts')
        @stack('phone-mask-scripts')
    </body>
</html>
