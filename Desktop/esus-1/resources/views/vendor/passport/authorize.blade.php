<x-auth-layout>
    <div class="flex flex-col w-full p-4 lg:py-20 justify-center items-center">
{{--        <h2 class="text-4xl font-bold mb-6">{{ __('Запрос авторизации') }}</h2>--}}
{{--        <h2 class="text-xl font-normal mb-6"><strong>{{ $client->name ?? 'name' }}</strong> запрашивает разрешение на доступ к вашей учетной записи.</h2>--}}
        <h2 class="text-xl font-normal mb-6">Нажмите кнопку <strong>продолжить</strong> чтобы посмотреть материал курса.</h2>
{{--        @if (count($scopes) > 0)--}}
{{--            <div class="flex flex-col justify-center mb-2">--}}
{{--                @foreach ($scopes as $scope)--}}
{{--                <div class="text-2xl text-center">{{ $scope->description }}</div>--}}
{{--                @endforeach--}}
{{--            </div>--}}
{{--        @endif--}}

        <div class="w-full lg:w-[460px] mx-auto flex flex-col space-y-4">
            <div class="flex flex-col space-y-4">
                <form method="post" action="{{ route('passport.authorizations.approve') }}">
                    @csrf

                    <input type="hidden" name="state" value="{{ $request->state }}">
                    <input type="hidden" name="client_id" value="{{ $client->id }}">
                    <input type="hidden" name="auth_token" value="{{ $authToken }}">
                    <button type="submit" class="w-full py-6 px-7 border border-[#0077CC] rounded text-[#FBFCFE] text-[18px] bg-gradient-to-b from-[#569DE5] to-[#2C73C5] hover:from-[#2C73C5] hover:to-[#569DE5]">Продолжить</button>
                </form>
{{--                <form method="post" action="{{ route('passport.authorizations.deny') }}">--}}
{{--                    @csrf--}}
{{--                    @method('DELETE')--}}

{{--                    <input type="hidden" name="state" value="{{ $request->state }}">--}}
{{--                    <input type="hidden" name="client_id" value="{{ $client->id }}">--}}
{{--                    <input type="hidden" name="auth_token" value="{{ $authToken }}">--}}
{{--                    <button class="w-full block py-6 px-7 border-2 border-[#000000] rounded text-[#000000] text-[18px] text-center hover:bg-[#000000] hover:text-white">Отмена</button>--}}
{{--                </form>--}}
            </div>
        </div>
    </div>

    @livewireScripts

</x-auth-layout>
