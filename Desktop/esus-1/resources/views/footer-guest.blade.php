<footer class="hidden lg:flex w-full bg-white relative z-40">
    <div class="flex max-w-screen-xl w-full mx-auto py-8 px-2 justify-between items-start">
        <div class="w-full flex flex-col space-y-4 justify-start">
            <div class="flex text-2xl font-bold justify-start items-center space-x-2">
                <svg width="32" height="32" viewBox="0 0 32 32" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path fill-rule="evenodd" clip-rule="evenodd" d="M0 16C4.84192e-07 18.5263 2.03301 20.5593 6.09902 24.6253L7.37469 25.901C11.4407 29.967 13.4737 32 16 32C18.5263 32 20.5593 29.967 24.6253 25.901L25.901 24.6253C29.967 20.5593 32 18.5263 32 16C32 13.4737 29.967 11.4407 25.901 7.37468L24.6253 6.09901C20.5593 2.033 18.5263 0 16 0C13.4737 1.1079e-06 11.4407 2.03301 7.37468 6.09902L6.09901 7.37469C2.033 11.4407 -4.25901e-07 13.4737 0 16ZM8.74933 27.2713C11.5932 30.0868 13.3664 31.645 15.2976 31.946L20.2129 26.844L15.9291 25.5371L14.84 21.1806L8.74933 27.2713Z" fill="url(#paint0_linear_259_399)"/>
                    <defs>
                        <linearGradient id="paint0_linear_259_399" x1="10.6654" y1="-18.5891" x2="46.8224" y2="9.9351" gradientUnits="userSpaceOnUse">
                            <stop stop-color="#3B82F6"/>
                            <stop offset="1" stop-color="#1B55B5"/>
                        </linearGradient>
                    </defs>
                </svg>
                <span class="text-logo-blue">ESUS</span>
            </div>
            <div class="text-base font-normal text-main-black w-52">{{ __('footer.description') }}</div>
        </div>
        <div class="flex w-full flex-col items-end">
            <nav class="flex space-x-8">
                <a href="{{ route('user.profile') }}" class="flex text-soft-gray font-semibold hover:text-main-black space-x-1.5 items-center">
                    <svg width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M10.1331 9.68329C10.1081 9.68329 10.0915 9.68329 10.0665 9.68329C10.0248 9.67496 9.96647 9.67496 9.91647 9.68329C7.4998 9.60829 5.6748 7.70829 5.6748 5.36663C5.6748 2.98329 7.61647 1.04163 9.9998 1.04163C12.3831 1.04163 14.3248 2.98329 14.3248 5.36663C14.3165 7.70829 12.4831 9.60829 10.1581 9.68329C10.1498 9.68329 10.1415 9.68329 10.1331 9.68329ZM9.9998 2.29163C8.30814 2.29163 6.92481 3.67496 6.92481 5.36663C6.92481 7.03329 8.2248 8.37496 9.88314 8.43329C9.9248 8.42496 10.0415 8.42496 10.1498 8.43329C11.7831 8.35829 13.0665 7.01663 13.0748 5.36663C13.0748 3.67496 11.6915 2.29163 9.9998 2.29163Z" fill="currentColor"/>
                        <path d="M10.1413 18.7917C8.50801 18.7917 6.86634 18.375 5.62467 17.5417C4.46634 16.775 3.83301 15.725 3.83301 14.5834C3.83301 13.4417 4.46634 12.3834 5.62467 11.6084C8.12467 9.95004 12.1747 9.95004 14.658 11.6084C15.808 12.375 16.4497 13.425 16.4497 14.5667C16.4497 15.7084 15.8163 16.7667 14.658 17.5417C13.408 18.375 11.7747 18.7917 10.1413 18.7917ZM6.31634 12.6584C5.51634 13.1917 5.08301 13.875 5.08301 14.5917C5.08301 15.3 5.52467 15.9834 6.31634 16.5084C8.39134 17.9 11.8913 17.9 13.9663 16.5084C14.7663 15.975 15.1997 15.2917 15.1997 14.575C15.1997 13.8667 14.758 13.1834 13.9663 12.6584C11.8913 11.275 8.39134 11.275 6.31634 12.6584Z" fill="currentColor"/>
                    </svg>
                    <span>{{ __('index.login.footer') }}</span>
                </a>

            </nav>
            <div class="text-sm font-normal text-soft-gray mt-10">{{ date('Y') }}. {{ __('footer.copyright') }}</div>
        </div>
    </div>
</footer>
