<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', App\Http\Livewire\Index::class)->name('homepage');
Route::get('/verify-certificate/{uuid}', App\Http\Livewire\Public\CertificateVerification::class)->name('verify-certificate');
Route::get('/verify-certificate', App\Http\Livewire\Public\CertificateIndex::class)->name('verify-certificate-index');
Route::post('forgot-password', [\App\Actions\Fortify\ResetPassword::class,'store'])->name('password.email');
Route::middleware(['middleware' => 'auth'])->get('/dashboard', function () {
//    return view('dashboard');
    return redirect('/profile');
})->name('dashboard');
Route::get('/need-complete-registration',function () {
    return view('errors.complete-registration');
})->name('errors.complete-registration');
Route::get('join-group/{uuid}', \App\Http\Livewire\Profile\JoinGroup::class)->middleware('user.complete.fill')->name('join-group');

Route::group(['middleware' => 'auth'], function () {
    Route::get('profile',\App\Http\Livewire\Profile\UserController::class)->name('user.profile');
    Route::get('profile/edit',\App\Http\Livewire\Profile\Users\UsersEdit::class)->name('user.profile.edit');
    Route::get('profile/orders/{id?}',\App\Http\Livewire\Profile\Orders::class)->name('user.orders.list')->where('id', '[0-9]+');
    Route::get('user/profile', function () {return redirect('profile');})->name('profile.show');
});

Route::group(['middleware' => ['auth','user.complete.fill']], function () {
    Route::get('messages',\App\Http\Livewire\Profile\MessagesList::class)->name('messages.list');
    Route::get('groups/active',\App\Http\Livewire\Profile\Groups\Active::class)->name('user.groups.active');
    Route::get('groups/archive',\App\Http\Livewire\Profile\Groups\Archive::class)->name('user.groups.archive');
    Route::get('group/{slug}',\App\Http\Livewire\Profile\GroupView::class)->name('user.group.view');
});

Route::prefix('trainer')->name('trainer.')->group(function () {
    Route::group(['middleware' => 'auth'], function () {
        Route::get('/', \App\Http\Livewire\Trainer\TrainerProfile::class)->name('profile');
        Route::get('/groups/active', \App\Http\Livewire\Trainer\Groups\Active::class)->name('groups.active');
        Route::get('/groups/archive', \App\Http\Livewire\Trainer\Groups\Archive::class)->name('groups.archive');
        Route::get('/group/{slug}', \App\Http\Livewire\Trainer\Groups\GroupsView::class)->name('groups.view');
        Route::get('/group/{slug}/statements', \App\Http\Livewire\Trainer\Groups\StatementsView::class)->name('groups.statements');
        Route::get('/programs/{id}', \App\Http\Livewire\Trainer\Programs\ProgramsView::class)->name('programs.view');
    });
});

//Route::prefix('affiliate')->name('affiliate.')->group(function () {
//    Route::group(['middleware' => 'auth'], function () {
//        Route::get('/', function () { return redirect('/affiliate/messages');})->name('index');
//        Route::get('/trainers', \App\Http\Livewire\Affiliate\Trainers\TrainersList::class)->name('trainers.index');
//        Route::get('/groups', \App\Http\Livewire\Affiliate\Groups\GroupsList::class)->name('groups.index');
//        Route::get('/schedule', \App\Http\Livewire\Affiliate\Schedule\ScheduleList::class)->name('schedule.index');
//        Route::get('/messages', \App\Http\Livewire\Affiliate\Messages\MessagesList::class)->name('messages.index');
//        Route::get('/grants', \App\Http\Livewire\Affiliate\Grants\GrantsList::class)->name('grants.index');
//    });
//});

Route::prefix('oop')->name('oop.')->group(function () {
    Route::group(['middleware' => 'auth'], function () {
        Route::get('/', function () { return redirect('/oop/courses');})->name('index');
        Route::get('/courses', \App\Http\Livewire\Oop\Courses\CoursesIndex::class)->name('courses.index');
        Route::get('/courses/create', \App\Http\Livewire\Oop\Courses\CoursesEdit::class)->name('courses.create');
        Route::get('/courses/{id}/edit', \App\Http\Livewire\Oop\Courses\CoursesEdit::class)->name('courses.edit');
        Route::get('/grants', \App\Http\Livewire\Oop\Grants\GrantsIndex::class)->name('grants.index');
        Route::get('/grants/{id}/show', \App\Http\Livewire\Oop\Grants\GrantsShow::class)->name('grants.show');
        Route::get('/trainers', \App\Http\Livewire\Oop\Trainers\TrainersIndex::class)->name('trainers.index');
        Route::get('/trainers/{id}/show', \App\Http\Livewire\Oop\Trainers\TrainersShow::class)->name('trainers.show');
    });
});

Route::prefix('omr')->name('omr.')->group(function () {
    Route::group(['middleware' => 'auth'], function () {
        Route::get('/', function () { return redirect('/omr/trainers');})->name('index');
        Route::get('/trainers', \App\Http\Livewire\Omr\Trainers\TrainersIndex::class)->name('trainers.index');
        Route::get('/trainers/create', \App\Http\Livewire\Omr\Trainers\TrainersEdit::class)->name('trainers.create');
        Route::get('/trainers/{id}/edit', \App\Http\Livewire\Omr\Trainers\TrainersEdit::class)->name('trainers.edit');
        Route::get('/grants', \App\Http\Livewire\Omr\Grants\GrantsIndex::class)->name('grants.index');
        Route::get('/grants/{id}/show', \App\Http\Livewire\Omr\Grants\GrantsShow::class)->name('grants.show');
        Route::get('/courses', \App\Http\Livewire\Omr\Courses\CoursesIndex::class)->name('courses.index');
        Route::get('/courses/{id}/show', \App\Http\Livewire\Omr\Courses\CoursesShow::class)->name('courses.show');
        Route::get('/schedule', \App\Http\Livewire\Omr\Schedule\ScheduleIndex::class)->name('schedule.index');
        Route::get('/groups', \App\Http\Livewire\Omr\Groups\GroupsIndex::class)->name('groups.index');
        Route::get('/groups/create', \App\Http\Livewire\Omr\Groups\GroupsEdit::class)->name('groups.create');
        Route::get('/groups/{id}/edit', \App\Http\Livewire\Omr\Groups\GroupsEdit::class)->name('groups.edit');

    });
});

Route::prefix('ooup')->name('ooup.')->group(function () {
    Route::group(['middleware' => 'auth'], function () {
        Route::get('/', function () { return redirect('/ooup/groups');})->name('index');
        Route::get('/years', \App\Http\Livewire\Ooup\Years\YearsIndex::class)->name('years.index');
        Route::get('/courses', \App\Http\Livewire\Ooup\Courses\CoursesIndex::class)->name('courses.index');
        Route::get('/courses/{id}/show', \App\Http\Livewire\Ooup\Courses\CoursesShow::class)->name('courses.show');
        Route::get('/grants', \App\Http\Livewire\Ooup\Grants\GrantsIndex::class)->name('grants.index');
        Route::get('/grants/{grant}', \App\Http\Livewire\Ooup\Grants\GrantsEdit::class)->name('grants.edit');
        Route::get('/trainers', \App\Http\Livewire\Ooup\Trainers\TrainersIndex::class)->name('trainers.index');
        Route::get('/trainers/{id}/show', \App\Http\Livewire\Ooup\Trainers\TrainersShow::class)->name('trainers.show');
        Route::get('/groups', \App\Http\Livewire\Ooup\Groups\GroupsIndex::class)->name('groups.index');
        Route::get('/groups/create', \App\Http\Livewire\Ooup\Groups\GroupsEdit::class)->name('groups.create');
        Route::get('/groups/{id}/edit', \App\Http\Livewire\Ooup\Groups\GroupsEdit::class)->name('groups.edit');
        Route::get('/groups/{slug}/statements', \App\Http\Livewire\Ooup\Groups\StatementsView::class)->name('groups.statements');
        Route::get('groups/{id}/certificates', \App\Http\Livewire\Ooup\Groups\GroupsCertificates::class)->name('groups.certificates');
        Route::get('/weekends', App\Http\Livewire\Ooup\Weekends\WeekendsIndex::class)->name('weekends.index');
        Route::get('/schedule', \App\Http\Livewire\Ooup\Schedule\ScheduleIndex::class)->name('schedule.index');
        Route::get('certificates', \App\Http\Livewire\Ooup\Certificates\CertificatesList::class)->name('certificates.index');
        Route::get('certificates/{id}/show', \App\Http\Livewire\Ooup\Certificates\CertificatesEdit::class)->name('certificates.show');
        Route::get('cert-templates', \App\Http\Livewire\Ooup\Certificates\CertificatesTemplatesList::class)->name('certificates-templates.index');
        Route::get('cert-templates/create', \App\Http\Livewire\Ooup\Certificates\CertificatesTemplatesEdit::class)->name('certificates-templates.create');
        Route::get('cert-templates/{id}/edit', \App\Http\Livewire\Ooup\Certificates\CertificatesTemplatesEdit::class)->name('certificates-templates.edit');
        Route::get('audits', \App\Http\Livewire\Ooup\Audit\AuditIndex::class)->name('audit.index');
        Route::get('audits/{id}', \App\Http\Livewire\Ooup\Audit\AuditView::class)->name('audit.view');
        Route::get('reports', \App\Http\Livewire\Ooup\Reports\ListenersByCourses::class)->name('reports.index');
    });
});

// TODO: Дать имя для `admin` роута и поменять везде
Route::prefix('admin')->name('admin.')->group(function () {
// Route::prefix('admin')->name('')->group(function () {
    Route::group(['middleware' => 'auth'], function () {
        Route::get('/', function () { return redirect('/admin/schedule');})->name('homepage');
        Route::get('audits', \App\Http\Livewire\Admin\Audit\AuditIndex::class)->name('audit.index');
        Route::get('audits/{id}', \App\Http\Livewire\Admin\Audit\AuditView::class)->name('audit.view');
        Route::get('statements', \App\Http\Livewire\Admin\Statements\MessagesIndex::class)->name('statements.index');
        Route::get('statements/{id}', \App\Http\Livewire\Admin\Statements\MessagesEdit::class)->name('statements.edit');
        Route::get('certificates', \App\Http\Livewire\Admin\Certificates\CertificatesList::class)->name('certificates.index');
        Route::get('certificates/{id}', \App\Http\Livewire\Admin\Certificates\CertificatesEdit::class)->name('certificates.edit');
        Route::get('cert-templates', \App\Http\Livewire\Admin\Certificates\CertificatesTemplatesList::class)->name('certificates-templates.index');
        Route::get('cert-templates/create', \App\Http\Livewire\Admin\Certificates\CertificatesTemplatesEdit::class)->name('certificates-templates.create');
        Route::get('cert-templates/{id}/edit', \App\Http\Livewire\Admin\Certificates\CertificatesTemplatesEdit::class)->name('certificates-templates.edit');
        Route::get('schedule', \App\Http\Livewire\Admin\ScheduleController::class)->name('schedule.index');
        Route::get('schedule/generate', \App\Http\Livewire\Admin\ScheduleGenerateController::class)->name('schedule.generate');
        Route::get('languages', \App\Http\Livewire\Admin\LanguagesController::class)->name('languages.index');
        Route::get('years', \App\Http\Livewire\Admin\YearsController::class)->name('years.index');
        Route::get('attractedcats', \App\Http\Livewire\Admin\Attracted\AttractedIndex::class)->name('attracted.index');
        Route::get('trainers/create', \App\Http\Livewire\Admin\TrainersCreateController::class)->name('trainers.create');
        Route::get('trainers/{id}/edit', \App\Http\Livewire\Admin\TrainersEditController::class)->name('trainers.edit');
        Route::get('trainers', \App\Http\Livewire\Admin\TrainersController::class)->name('trainers.index');
        Route::get('authin/{id}', \App\Http\Livewire\Admin\AuthinController::class)->name('authin.index');
        Route::get('groups/create', \App\Http\Livewire\Admin\GroupsEditController::class)->name('groups.create');
        Route::get('groups/{id}/edit', \App\Http\Livewire\Admin\GroupsEditController::class)->name('groups.edit');
        Route::get('groups/{id}/user-edit/{user}', \App\Http\Livewire\Admin\Groups\GroupsEditUserController::class)->name('groups.user-edit');
        Route::get('groups/{id}/certificates', \App\Http\Livewire\Admin\Groups\GroupsCertificates::class)->name('groups.certificates');
        Route::get('groups', \App\Http\Livewire\Admin\GroupsController::class)->name('groups.index');
        Route::get('institutes/create', \App\Http\Livewire\Admin\InstitutesCreateController::class)->name('institutes.create');
        Route::get('institutes/{id}/edit', \App\Http\Livewire\Admin\InstitutesCreateController::class)->name('institutes.edit');
        Route::get('institutes', \App\Http\Livewire\Admin\InstitutesController::class)->name('institutes.index');
        Route::get('teach-subjects', \App\Http\Livewire\Admin\TeachSubject\TeachSubjectList::class)->name('teach-subjects.index');
        Route::get('grants', \App\Http\Livewire\Admin\GrantsController::class)->name('grants.index');
        Route::get('grants/{grant}', \App\Http\Livewire\Admin\GrantEditController::class)->name('grants.edit');
//        Route::resource('programs', \App\Http\Controllers\Admin\ProgramsController::class);
        Route::get('courses', \App\Http\Livewire\Admin\CoursesController::class)->name('courses.index');
        Route::get('courses/create', \App\Http\Livewire\Admin\Courses\CoursesEdit::class)->name('courses.create');
        Route::get('courses/{id}/edit', \App\Http\Livewire\Admin\Courses\CoursesEdit::class)->name('courses.edit');
        Route::resource('courses', \App\Http\Controllers\Admin\CoursesController::class)->except(['index','create','edit']);
        Route::resource('academiccategories', \App\Http\Controllers\Admin\AcademicCategoryController::class);
        Route::resource('academicdegrees', \App\Http\Controllers\Admin\AcademicDegreesController::class);
        Route::resource('educations', \App\Http\Controllers\Admin\EducationsController::class);
        Route::resource('jobnames', \App\Http\Controllers\Admin\JobNamesController::class);
        Route::resource('nationalities', \App\Http\Controllers\Admin\NationalitiesController::class);
        Route::resource('genders', \App\Http\Controllers\Admin\GendersController::class);
        Route::resource('affiliates', \App\Http\Controllers\Admin\AffiliatesController::class);
        Route::get('locations/oblast', [\App\Http\Controllers\Admin\LocationsController::class,'index_oblast'])->name('locations.oblast.index');
        Route::get('locations/rayon', [\App\Http\Controllers\Admin\LocationsController::class,'index_rayon'])->name('locations.rayon.index');
//        Route::get('locations/city', [\App\Http\Controllers\Admin\LocationsController::class,'index_city'])->name('locations.city.index');
        Route::get('locations/city', [\App\Http\Controllers\Admin\LocationsController::class,'index_aul'])->name('locations.aul.index');
        Route::resource('locations', \App\Http\Controllers\Admin\LocationsController::class);
        Route::get('weekends', \App\Http\Livewire\Admin\WeekendsController::class)->name('weekends.index');
        Route::get('users', \App\Http\Livewire\Admin\UsersController::class)->name('users.index');
        Route::get('roles', \App\Http\Livewire\Admin\Role\PermitionsController::class)->name('roles.index');
        Route::get('roles/{role}', \App\Http\Livewire\Admin\Role\UsersList::class)->name('roles.list');
        Route::get('users/create', \App\Http\Livewire\Admin\UsersEditController::class)->name('users.create');
        Route::get('users/{iin}', \App\Http\Livewire\Admin\UsersEditController::class)->name('users.edit');
        Route::get('imports',\App\Http\Livewire\Admin\Imports\LocationsImport::class)->name('imports.locations');
        Route::get('reports', \App\Http\Livewire\Admin\Reports\ListenersByCourses::class)->name('reports.index');
    });
});

Route::prefix('affiliate')->name('affiliate.')->group(function () {
// Route::prefix('admin')->name('')->group(function () {
    Route::group(['middleware' => 'auth'], function () {
        Route::get('/', function () { return redirect('/affiliate/schedule');})->name('homepage');
        Route::get('statements', \App\Http\Livewire\Affiliate\Statements\MessagesIndex::class)->name('statements.index');
        Route::get('statements/{id}', \App\Http\Livewire\Affiliate\Statements\MessagesEdit::class)->name('statements.edit');
//        Route::get('certificates', \App\Http\Livewire\Admin\Certificates\CertificatesList::class)->name('certificates.index');
//        Route::get('certificates/{id}/edit', \App\Http\Livewire\Admin\Certificates\CertificatesEdit::class)->name('certificates.edit');
//        Route::get('cert-templates', \App\Http\Livewire\Admin\Certificates\CertificatesTemplatesList::class)->name('certificates-templates.index');
//        Route::get('cert-templates/create', \App\Http\Livewire\Admin\Certificates\CertificatesTemplatesEdit::class)->name('certificates-templates.create');
//        Route::get('cert-templates/{id}/edit', \App\Http\Livewire\Admin\Certificates\CertificatesTemplatesEdit::class)->name('certificates-templates.edit');
        Route::get('schedule', \App\Http\Livewire\Affiliate\Schedule\ScheduleIndex::class)->name('schedule.index');
//        Route::get('schedule/generate', \App\Http\Livewire\Admin\ScheduleGenerateController::class)->name('schedule.generate');
//        Route::get('languages', \App\Http\Livewire\Admin\LanguagesController::class)->name('languages.index');
//        Route::get('years', \App\Http\Livewire\Admin\YearsController::class)->name('years.index');
        Route::get('trainers/create', \App\Http\Livewire\Affiliate\Trainer\TrainerCreate::class)->name('trainers.create');
        Route::get('trainers/{id}/edit', \App\Http\Livewire\Affiliate\Trainer\TrainerEdit::class)->name('trainers.edit');
        Route::get('trainers', \App\Http\Livewire\Affiliate\Trainer\TrainerIndex::class)->name('trainers.index');
        Route::get('groups/create', \App\Http\Livewire\Affiliate\Group\GroupEdit::class)->name('groups.create');
        Route::get('groups/{id}/edit', \App\Http\Livewire\Affiliate\Group\GroupEdit::class)->name('groups.edit');
        Route::get('groups/{id}/user-edit/{user}', \App\Http\Livewire\Affiliate\Group\GroupsEditUserController::class)->name('groups.user-edit');
        Route::get('/groups/{slug}/statements', \App\Http\Livewire\Affiliate\Group\StatementsView::class)->name('groups.statements');
//        Route::get('groups/{id}/certificates', \App\Http\Livewire\Admin\Groups\GroupsCertificates::class)->name('groups.certificates');
        Route::get('groups', \App\Http\Livewire\Affiliate\Group\GroupIndex::class)->name('groups.index');
//        Route::get('institutes/create', \App\Http\Livewire\Admin\InstitutesCreateController::class)->name('institutes.create');
//        Route::get('institutes/{id}/edit', \App\Http\Livewire\Admin\InstitutesCreateController::class)->name('institutes.edit');
//        Route::get('institutes', \App\Http\Livewire\Admin\InstitutesController::class)->name('institutes.index');
        Route::get('grants', \App\Http\Livewire\Affiliate\Grants\GrantsIndex::class)->name('grants.index');
//        Route::get('grants/{grant}', \App\Http\Livewire\Admin\GrantEditController::class)->name('grants.edit');
//        Route::resource('programs', \App\Http\Controllers\Admin\ProgramsController::class);
        Route::get('courses', \App\Http\Livewire\Affiliate\Courses\CoursesIndex::class)->name('courses.index');
//        Route::get('courses/create', \App\Http\Livewire\Admin\Courses\CoursesEdit::class)->name('courses.create');
        Route::get('courses/{id}/show', \App\Http\Livewire\Affiliate\Courses\CoursesShow::class)->name('courses.show');
//        Route::resource('courses', \App\Http\Controllers\Admin\CoursesController::class)->except(['index','create','edit']);
//        Route::resource('academiccategories', \App\Http\Controllers\Admin\AcademicCategoryController::class);
//        Route::resource('academicdegrees', \App\Http\Controllers\Admin\AcademicDegreesController::class);
//        Route::resource('educations', \App\Http\Controllers\Admin\EducationsController::class);
//        Route::resource('jobnames', \App\Http\Controllers\Admin\JobNamesController::class);
//        Route::resource('nationalities', \App\Http\Controllers\Admin\NationalitiesController::class);
//        Route::resource('genders', \App\Http\Controllers\Admin\GendersController::class);
//        Route::resource('affiliates', \App\Http\Controllers\Admin\AffiliatesController::class);
//        Route::get('locations/oblast', [\App\Http\Controllers\Admin\LocationsController::class,'index_oblast'])->name('locations.oblast.index');
//        Route::get('locations/rayon', [\App\Http\Controllers\Admin\LocationsController::class,'index_rayon'])->name('locations.rayon.index');
//        Route::get('locations/city', [\App\Http\Controllers\Admin\LocationsController::class,'index_city'])->name('locations.city.index');
//        Route::get('locations/city', [\App\Http\Controllers\Admin\LocationsController::class,'index_aul'])->name('locations.aul.index');
//        Route::resource('locations', \App\Http\Controllers\Admin\LocationsController::class);
//        Route::get('weekends', \App\Http\Livewire\Admin\WeekendsController::class)->name('weekends.index');
//        Route::get('users', \App\Http\Livewire\Admin\UsersController::class)->name('users.index');
//        Route::get('roles', \App\Http\Livewire\Admin\Role\PermitionsController::class)->name('roles.index');
//        Route::get('roles/{role}', \App\Http\Livewire\Admin\Role\UsersList::class)->name('roles.list');
//        Route::get('users/{iin}', \App\Http\Livewire\Admin\UsersEditController::class)->name('users.edit');
//        Route::get('imports',\App\Http\Livewire\Admin\Imports\LocationsImport::class)->name('imports.locations');
        Route::get('reports', \App\Http\Livewire\Affiliate\Reports\ListenersByCourses::class)->name('reports.index');
    });
});

Route::prefix('accountant')->name('accountant.')->group(function () {
    Route::group(['middleware' => 'auth'], function () {
        Route::get('/', function () { return redirect('/accountant/groups');})->name('index');
//        Route::get('/years', \App\Http\Livewire\Accountant\Years\YearsIndex::class)->name('years.index');
        Route::get('/courses', \App\Http\Livewire\Accountant\Courses\CoursesIndex::class)->name('courses.index');
        Route::get('/courses/{id}/show', \App\Http\Livewire\Accountant\Courses\CoursesShow::class)->name('courses.show');
        Route::get('/grants', \App\Http\Livewire\Accountant\Grants\GrantsIndex::class)->name('grants.index');
        Route::get('/grants/{id}/show', \App\Http\Livewire\Accountant\Grants\GrantsShow::class)->name('grants.show');
        Route::get('/trainers', \App\Http\Livewire\Accountant\Trainers\TrainersIndex::class)->name('trainers.index');
        Route::get('/trainers/{id}/show', \App\Http\Livewire\Accountant\Trainers\TrainersShow::class)->name('trainers.show');
        Route::get('/groups', \App\Http\Livewire\Accountant\Groups\GroupsIndex::class)->name('groups.index');
//        Route::get('/groups/create', \App\Http\Livewire\Accountant\Groups\GroupsEdit::class)->name('groups.create');
        Route::get('/groups/{id}/edit', \App\Http\Livewire\Accountant\Groups\GroupsEdit::class)->name('groups.edit');
        Route::get('groups/{id}/certificates', \App\Http\Livewire\Accountant\Groups\GroupsCertificates::class)->name('groups.certificates');
//        Route::get('/weekends', App\Http\Livewire\Accountant\Weekends\WeekendsIndex::class)->name('weekends.index');
        Route::get('/schedule', \App\Http\Livewire\Accountant\Schedule\ScheduleIndex::class)->name('schedule.index');
        Route::get('certificates', \App\Http\Livewire\Accountant\Certificates\CertificatesList::class)->name('certificates.index');
        Route::get('certificates/{id}/show', \App\Http\Livewire\Accountant\Certificates\CertificatesEdit::class)->name('certificates.show');
//        Route::get('cert-templates', \App\Http\Livewire\Accountant\Certificates\CertificatesTemplatesList::class)->name('certificates-templates.index');
//        Route::get('cert-templates/create', \App\Http\Livewire\Accountant\Certificates\CertificatesTemplatesEdit::class)->name('certificates-templates.create');
//        Route::get('cert-templates/{id}/edit', \App\Http\Livewire\Accountant\Certificates\CertificatesTemplatesEdit::class)->name('certificates-templates.edit');
        Route::get('audits', \App\Http\Livewire\Accountant\Audit\AuditIndex::class)->name('audit.index');
        Route::get('audits/{id}', \App\Http\Livewire\Accountant\Audit\AuditView::class)->name('audit.view');
        Route::get('reports', \App\Http\Livewire\Accountant\Reports\ListenersByCourses::class)->name('reports.index');
        Route::get('/payments', \App\Http\Livewire\Accountant\Payments\PaymentsIndex::class)->name('payments.index');
    });

});
