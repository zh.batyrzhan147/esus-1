<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Additional App Settings
    |--------------------------------------------------------------------------
    |
    | This option controls the process after user registration, 
    | Launches into the system users who do not have all their portfolios filled out.
    | Supported: "true" -  skip_full_fields: yes (activate)
    |            "false" - skip_full_fields: no  (deactivate)
    |
    */
    'user_skip_full_fields' => false,
];
