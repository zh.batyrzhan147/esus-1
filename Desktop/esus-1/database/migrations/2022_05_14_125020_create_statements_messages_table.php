<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStatementsMessagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('statements_messages', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user')->references('id')->on('users');
            $table->foreignId('group')->references('id')->on('groups');
            $table->foreignId('affiliate')->references('id')->on('affiliates');
            $table->foreignId('statement')->references('id')->on('statements');
            $table->text('description')->nullable();
            $table->string('file')->nullable();
            $table->integer('status')->default(0);
            $table->foreignId('changedby')->nullable()->references('id')->on('users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('statements_messages');
    }
}
