<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddAutogenerationFiledsToCourses extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('courses', function (Blueprint $table) {
            $table->boolean('auto_generation')->default(false);
        });

        Schema::table('users', function (Blueprint $table) {
            $table->boolean('auto_generation')->default(false);
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('courses', function (Blueprint $table) {
            $table->dropColumn('auto_generation');
        });

        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('auto_generation');
        });
    }
}
