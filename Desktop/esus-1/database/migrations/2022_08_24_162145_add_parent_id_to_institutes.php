<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddParentIdToInstitutes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('institute_forms', function (Blueprint $table) {
            $table->id();
            $table->string('title');
            $table->timestamps();
        });

        Schema::table('institutes', function (Blueprint $table) {
            $table->foreignId('forms_id')->nullable()->references('id')->on('institute_forms');
            $table->foreignId('parent_id')->nullable()->references('id')->on('institutes');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('institutes', function (Blueprint $table) {
            $table->dropForeign('institutes_forms_id_foreign');
            $table->dropColumn('forms_id');
            $table->dropForeign('institutes_parent_id_foreign');
            $table->dropColumn('parent_id');
        });

        Schema::dropIfExists('institute_forms');
    }
}
