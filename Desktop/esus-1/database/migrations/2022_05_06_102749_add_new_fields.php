<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddNewFields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->boolean('attracted')->default(false);
            $table->integer('gorono')->nullable();
        });

        Schema::table('courses', function (Blueprint $table) {
            $table->string('offer_agreement')->nullable();
        });

        Schema::table('groups', function (Blueprint $table) {
            $table->string('zoom_link')->nullable();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('attracted');
            $table->dropColumn('gorono');
        });

        Schema::table('courses', function (Blueprint $table) {
            $table->dropColumn('offer_agreement');
        });
        Schema::table('groups', function (Blueprint $table) {
            $table->dropColumn('zoom_link');
        });
    }
}
