<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->id();
            $table->string('code')->nullable();
            $table->float('total')->default(0);
            $table->boolean('payed')->default(false);
            $table->boolean('canceled')->default(false);
            $table->string('currency')->default('KZT');
            $table->text('notes')->nullable();
            $table->text('comment')->nullable();
            $table->json('properties')->nullable();
            $table->text('reason_canceled')->nullable();
            $table->dateTime('date_payed')->nullable();
            $table->dateTime('date_canceled')->nullable();
            $table->foreignId('user_id')->references('id')->on('users')->cascadeOnDelete();
            $table->morphs('target');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
