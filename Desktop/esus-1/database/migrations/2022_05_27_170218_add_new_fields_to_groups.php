<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddNewFieldsToGroups extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('groups', function (Blueprint $table) {
            $table->date('practice_start')->nullable();
            $table->date('practice_end')->nullable();
        });

        Schema::table('courses', function (Blueprint $table) {
            $table->integer('practice_length')->default(0);
        });

        Schema::create('attracted_cats', function (Blueprint $table) {
            $table->id();
            $table->string('kz_title')->nullable();
            $table->string('ru_title')->nullable();
            $table->timestamps();
        });

        Schema::table('users', function (Blueprint $table) {
            $table->foreignId('attractedcat')->nullable()->references('id')->on('attracted_cats');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropForeign('users_attractedcat_foreign');
            $table->dropColumn('attractedcat');
        });
        Schema::dropIfExists('attracted_cats');

        Schema::table('groups', function (Blueprint $table) {
            $table->dropColumn('practice_start');
            $table->dropColumn('practice_end');
        });

        Schema::table('courses', function (Blueprint $table) {
            $table->dropColumn('practice_length');
        });



    }
}
