<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCoursesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('years', function (Blueprint $table) {
            $table->id();
            $table->integer('year')->unique();
            $table->timestamps();
        });

        Schema::create('courses', function (Blueprint $table) {
            $table->id();
            $table->text('kz_title')->nullable();
            $table->text('ru_title')->nullable();
            $table->string('slug')->unique();
            $table->integer('type')->nullable();
            $table->boolean('grading')->default(false);
            $table->integer('course_length')->default(0);
            $table->foreignId('developer')->nullable()->references('id')->on('users');
            $table->string('file_1_listener')->nullable();
            $table->string('file_2_listener')->nullable();
            $table->string('file_1_teacher')->nullable();
            $table->string('file_2_teacher')->nullable();
            $table->integer('webinar_count')->nullable();
            $table->integer('webinar_length')->nullable();
            //sdo
            $table->string('sdo_category')->nullable();
            $table->string('sdo_link')->nullable();
            $table->string('one_drive_link')->nullable();
            $table->timestamps();
        });

        Schema::create('courses_years', function (Blueprint $table) {
            $table->foreignId('course_id')->references('id')->on('courses')->cascadeOnDelete();
            $table->foreignId('year_id')->references('id')->on('years')->cascadeOnDelete();
        });

        Schema::create('courses_users', function (Blueprint $table) {
            $table->foreignId('course_id')->references('id')->on('courses')->cascadeOnDelete();
            $table->foreignId('user_id')->references('id')->on('users')->cascadeOnDelete();
        });

        Schema::create('job_names_users', function (Blueprint $table) {
            $table->foreignId('course_id')->references('id')->on('courses')->cascadeOnDelete();
            $table->foreignId('job_name_id')->references('id')->on('job_names')->cascadeOnDelete();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('courses_years');
        Schema::dropIfExists('job_names_users');
        Schema::dropIfExists('courses_users');
        Schema::dropIfExists('courses');
        Schema::dropIfExists('years');
    }
}
