<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInstitutesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('institutes_types', function (Blueprint $table) {
            $table->id();
            $table->string('title');
            $table->timestamps();
        });

        Schema::create('institutes', function (Blueprint $table) {
            $table->id();
            $table->string('title')->nullable();
            $table->text('kz_title')->nullable();
            $table->text('ru_title')->nullable();
            $table->foreignId('type')->nullable()->references('id')->on('institutes_types');
            $table->foreignId('city')->nullable()->references('id')->on('locations');
            $table->foreignId('rayon')->nullable()->references('id')->on('locations');
            $table->foreignId('oblast')->nullable()->references('id')->on('locations');
            $table->string('address')->nullable();
            $table->string('type_org')->nullable();
            $table->string('type_school')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('institutes');
    }
}
