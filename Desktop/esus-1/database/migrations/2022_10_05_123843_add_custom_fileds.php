<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddCustomFileds extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->text('trainer_awards')->nullable();
            $table->text('trainer_theses')->nullable();
            $table->text('trainer_certificates')->nullable();
            $table->string('trainer_oauth_group')->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
           $table->dropColumn('trainer_awards');
           $table->dropColumn('trainer_theses');
           $table->dropColumn('trainer_certificates');
           $table->dropColumn('trainer_oauth_group');
        });
    }
}
