<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCertificatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('certificates', function (Blueprint $table) {
            $table->id();
            $table->string('pdf')->nullable();
            $table->string('fio');
            $table->string('group_title');
            $table->integer('group_id');
            $table->string('course_title');
            $table->integer('course_id');
            $table->integer('year');
            $table->string('description')->nullable();
            $table->string('url')->nullable();
            $table->integer('views_count')->default(0);
            $table->foreignId('user')->nullable()->references('id')->on('users');
            $table->foreignId('template')->nullable()->references('id')->on('certificates_templates');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('certificates');
    }
}
