<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFieldsToReports extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::table('reports', function (Blueprint $table) {
            $table->foreignId('year')->nullable()->references('id')->on('years');
            $table->foreignId('course')->nullable()->references('id')->on('courses');
            $table->foreignId('language')->nullable()->references('id')->on('languages');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('reports', function (Blueprint $table) {
            $table->dropForeign('reports_year_foreign');
            $table->dropColumn('year');
            $table->dropForeign('reports_course_foreign');
            $table->dropColumn('course');
            $table->dropForeign('reports_language_foreign');
            $table->dropColumn('language');

        });
    }
}
