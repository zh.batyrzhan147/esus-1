<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateExcludedUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('excluded_users', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user')->nullable()->references('id')->on('users');
            $table->foreignId('group')->nullable()->references('id')->on('groups');
            $table->text('message')->nullable();
            $table->foreignId('excluded_by')->nullable()->references('id')->on('users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('excluded_users');
    }
}
