<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAuditReportsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('audit_reports', function (Blueprint $table) {
            $table->id();
            $table->string('file')->nullable();
            $table->foreignId('user')->references('id')->on('users');
            $table->foreignId('year')->references('id')->on('years');
            $table->jsonb('data')->nullable();
            $table->integer('status')->default(0);
            $table->integer('type');
            $table->foreignId('affiliate')->nullable()->references('id')->on('affiliates');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('audit_reports');
    }
}
