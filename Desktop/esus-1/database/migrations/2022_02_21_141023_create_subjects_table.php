<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSubjectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('subjects', function (Blueprint $table) {
            $table->id();
            $table->string('title');
            $table->foreignId('fromUser')->references('id')->on('users');
            $table->foreignId('toUser')->nullable()->references('id')->on('users');
            $table->foreignId('toAffiliate')->nullable()->references('id')->on('affiliates');
            $table->foreignId('editedBy')->nullable()->references('id')->on('users');
            $table->string('files')->nullable();
            $table->text('request')->nullable();
            $table->foreignId('statement')->references('id')->on('statements');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('subjects');
    }
}
