<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMetaInformations extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('locations', function (Blueprint $table) {
//            $table->integer('id')->autoIncrement()->primary();
            $table->id();
//            $table->integer('parent')->unsigned()->nullable();
            $table->foreignId('parent')->nullable()->references('id')->on('locations');
            $table->string('ru_title');
            $table->string('kz_title');
            $table->string('type');
            $table->timestamps();
        });

        Schema::create('job_names', function (Blueprint $table) {
            $table->id();
            $table->string('ru_title');
            $table->string('kz_title');
            $table->timestamps();
        });

        Schema::create('educations', function (Blueprint $table) {
            $table->id();
            $table->string('ru_title');
            $table->string('kz_title');
            $table->timestamps();
        });

        Schema::create('academic_degrees', function (Blueprint $table) {
            $table->id();
            $table->string('ru_title');
            $table->string('kz_title');
            $table->timestamps();
        });

        Schema::create('academic_categories', function (Blueprint $table) {
            $table->id();
            $table->string('ru_title');
            $table->string('kz_title');
            $table->timestamps();
        });

        Schema::create('nationalities', function (Blueprint $table) {
            $table->id();
            $table->string('ru_title');
            $table->string('kz_title');
            $table->timestamps();
        });

        Schema::create('languages', function (Blueprint $table) {
            $table->id();
            $table->string('slug')->unique();
            $table->string('title');
            $table->timestamps();
        });

        Schema::create('genders', function (Blueprint $table) {
            $table->id();
            $table->string('slug')->unique();
            $table->string('title');
            $table->timestamps();
        });

        Schema::create('affiliates', function (Blueprint $table) {
            $table->id();
            $table->string('title');
            $table->string('director')->nullable();
            $table->string('address')->nullable();
            $table->string('phone')->nullable();
            $table->string('email')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('locations');
        Schema::dropIfExists('job_names');
        Schema::dropIfExists('educations');
        Schema::dropIfExists('academic_degrees');
        Schema::dropIfExists('academic_categories');
        Schema::dropIfExists('nationalities');
        Schema::dropIfExists('languages');
        Schema::dropIfExists('genders');
        Schema::dropIfExists('affiliates');
    }
}
