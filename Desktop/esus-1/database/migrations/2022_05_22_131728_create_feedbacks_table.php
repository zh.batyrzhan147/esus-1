<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFeedbacksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('feedbacks', function (Blueprint $table) {
            $table->id();
            $table->integer('stars')->default(5);
            $table->text('description')->nullable();
            $table->foreignId('year')->nullable()->references('id')->on('years');
            $table->foreignId('trainer')->nullable()->references('id')->on('users');
            $table->foreignId('listener')->nullable()->references('id')->on('users');
            $table->foreignId('course')->nullable()->references('id')->on('courses');
            $table->foreignId('group')->nullable()->references('id')->on('groups');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('feedbacks');
    }
}
