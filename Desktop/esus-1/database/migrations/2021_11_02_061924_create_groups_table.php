<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGroupsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('groups', function (Blueprint $table) {
            $table->id();

            $table->string('slug')->unique();
            $table->foreignId('language')->nullable()->references('id')->on('languages');
            $table->foreignId('affiliate')->nullable()->references('id')->on('affiliates');
            $table->foreignId('year')->nullable()->references('id')->on('years');
            $table->string('offer_agreement')->nullable();
            $table->integer('count_all')->default(0);
            $table->foreignId('courses')->nullable()->references('id')->on('courses');
            $table->foreignId('trainer')->nullable()->references('id')->on('users');
            $table->date('start_time')->nullable();
            $table->date('end_time')->nullable();
            $table->integer('type')->nullable();
            $table->boolean('active')->default(true);
            $table->boolean('staged')->default(false);
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('groups_users', function (Blueprint $table) {
            $table->foreignId('group_id')->references('id')->on('groups')->cascadeOnDelete();
            $table->foreignId('user_id')->references('id')->on('users')->cascadeOnDelete();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('groups_users');
        Schema::dropIfExists('groups');
    }
}
