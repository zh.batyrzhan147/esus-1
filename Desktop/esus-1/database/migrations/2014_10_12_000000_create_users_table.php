<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->uuid('iin')->unique();
            $table->string('name');
            $table->string('last_name');
            $table->string('patronymic')->nullable();
            $table->string('name_lat')->nullable();
            $table->string('last_name_lat')->nullable();

            $table->foreignId('gender')->nullable()->references('id')->on('genders');
            $table->foreignId('nationality')->nullable()->references('id')->on('nationalities');
            $table->string('phone')->nullable();
            $table->string('email')->unique();

            $table->foreignId('oblast')->nullable()->references('id')->on('locations');
            $table->foreignId('raion')->nullable()->references('id')->on('locations');
            $table->foreignId('gorod')->nullable()->references('id')->on('locations');

            $table->foreignId('institute')->nullable()->references('id')->on('institutes');

            $table->foreignId('jobname')->nullable()->references('id')->on('job_names');
            $table->foreignId('academic_category')->nullable()->references('id')->on('academic_categories');
            $table->foreignId('education')->nullable()->references('id')->on('educations');
            $table->foreignId('academic_degree')->nullable()->references('id')->on('academic_degrees');
            $table->string('subject_teach')->nullable();
            $table->string('length_work')->nullable();
            //bookkeeping
            $table->string('iban')->nullable();
            $table->string('bank_name')->nullable();

            //trainer
            $table->foreignId('affiliate')->nullable()->references('id')->on('affiliates');
            $table->string('trainer_type')->nullable();
            $table->string('trainer_cv')->nullable();
            $table->integer('hours_count')->default(0);

            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->rememberToken();
            $table->foreignId('current_team_id')->nullable()->nullable();
            $table->text('profile_photo_path')->nullable();
            $table->timestamps();
        });

        Schema::create('users_languages', function (Blueprint $table) {
            $table->foreignId('language_id')->references('id')->on('languages')->cascadeOnDelete();
            $table->foreignId('user_id')->references('id')->on('users')->cascadeOnDelete();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users_languages');
        Schema::dropIfExists('users');
    }
}
