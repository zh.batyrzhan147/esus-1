<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTeachSubjectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('teach_subjects', function (Blueprint $table) {
            $table->id();
            $table->string('kz_title')->nullable();
            $table->string('ru_title')->nullable();
            $table->timestamps();
        });

        Schema::table('users',function (Blueprint $table) {
            $table->foreignId('teach_subject_id')->nullable()->references('id')->on('teach_subjects');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users',function (Blueprint $table) {
            $table->dropForeign('users_teach_subject_id_foreign');
            $table->dropColumn('teach_subject_id');
        });
        Schema::dropIfExists('teach_subjects');
    }
}
