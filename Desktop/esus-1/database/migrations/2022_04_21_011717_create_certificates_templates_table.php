<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCertificatesTemplatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('certificates_templates', function (Blueprint $table) {
            $table->id();
            $table->boolean('active')->default(true);
            $table->string('title');
            $table->string('background');
            $table->foreignId('course')->references('id')->on('courses');
            $table->foreignId('year')->nullable()->references('id')->on('years');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('certificates_templates');
    }
}
