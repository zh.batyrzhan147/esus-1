<?php

namespace Database\Seeders;

use App\Models\CourseDates;
use App\Models\Groups;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        if (\App::environment() === 'production') {
            $this->call([
                PermissionsTableSeeder::class,
                RolesTableSeeder::class,
                PermissionRoleTableSeeder::class,
                UsersTableSeeder::class,
                RoleUserTableSeeder::class,
            ]);
        } else {
            $this->call([
                MetaInformationsSeeder::class,
//            InstitutesTypesSeeder::class,
                (\App::environment() === 'production') ? InstitutesSeeder::class : NullSeeder::class,

                (\App::environment() === 'local') ? CoursesSeeder::class : NullSeeder::class,
                (\App::environment() === 'local') ? GrantsSeeder::class : NullSeeder::class,

                PermissionsTableSeeder::class,
                RolesTableSeeder::class,
                PermissionRoleTableSeeder::class,
                UsersTableSeeder::class,
                RoleUserTableSeeder::class,
//            GroupsSeeder::class,
                CourseDatesSeeder::class,
            ]);
        }
    }
}
