<?php

namespace Database\Seeders;

use App\Models\AcademicCategories;
use App\Models\AcademicDegrees;
use App\Models\Affiliates;
use App\Models\Educations;
use App\Models\Genders;
use App\Models\JobNames;
use App\Models\Languages;
use App\Models\Locations;
use App\Models\Nationalities;
use App\Models\Years;
use Illuminate\Database\Seeder;

class MetaInformationsSeeder extends Seeder
{
    public function run()
    {
        $faker = \Faker\Factory::create();

        Years::insert([
            'id' => 2022,
            'year' => 2022
        ]);

        AcademicCategories::insert([
            [
                'id' => 1,
                'ru_title' => 'Высшая',
                'kz_title' => 'Жоғары',
            ],[
                'id' => 2,
                'ru_title' => 'Вторая',
                'kz_title' => 'Екінші',
            ],[
                'id' => 3,
                'ru_title' => 'Первая',
                'kz_title' => 'Бірінші',
            ],[
                'id' => 4,
                'ru_title' => '-= Без категории =-',
                'kz_title' => '-= Без категории =-',
            ],[
                'id' => 5,
                'ru_title' => '(НИШ) Эксперт',
                'kz_title' => '(НИШ) Эксперт',
            ],[
                'id' => 6,
                'ru_title' => '(НИШ) Модератор',
                'kz_title' => '(НИШ) Модератор',
            ],[
                'id' => 7,
                'ru_title' => '(НИШ) Учитель',
                'kz_title' => '(НИШ) Учитель',
            ],[
                'id' => 8,
                'ru_title' => '(НИШ) Учитель-стажер',
                'kz_title' => '(НИШ) Учитель-стажер',
            ],

        ]);

//        AcademicDegrees::insert([
//            [
//                'id' => 1,
//                'ru_title' => 'Ученная степень 1',
//                'kz_title' => 'Ученная степень 1',
//            ],[
//                'id' => 2,
//                'ru_title' => 'Ученная степень 2',
//                'kz_title' => 'Ученная степень 2',
//            ],[
//                'id' => 3,
//                'ru_title' => 'Ученная степень 3',
//                'kz_title' => 'Ученная степень 3',
//            ],[
//                'id' => 4,
//                'ru_title' => 'Ученная степень 4',
//                'kz_title' => 'Ученная степень 4',
//            ],
//        ]);

        Affiliates::insert([
            [
                'title' => 'ЦА'
            ],[
                'title' => 'Карагандинский филиал'
            ],[
                'title' => 'Алматинский филиал'
            ],[
                'title' => 'Атырауский филиал'
            ]

        ]);

        Educations::insert([
            [
                'id' => 1,
                'ru_title' => 'Среднее',
                'kz_title' => 'Орташа',
            ],[
                'id' => 2,
                'ru_title' => 'Высшее',
                'kz_title' => 'Жоғары',
            ],[
                'id' => 3,
                'ru_title' => 'Магистр',
                'kz_title' => 'Магистр',
            ],[
                'id' => 4,
                'ru_title' => 'Доктор наук',
                'kz_title' => 'Доктор наук',
            ],

        ]);

//        JobNames::insert([
//            [
//                'id' => 1,
//                'ru_title' => 'Должность 1',
//                'kz_title' => 'Должность 1',
//            ],[
//                'id' => 2,
//                'ru_title' => 'Должность 2',
//                'kz_title' => 'Должность 2',
//            ],[
//                'id' => 3,
//                'ru_title' => 'Должность 3',
//                'kz_title' => 'Должность 3',
//            ],[
//                'id' => 4,
//                'ru_title' => 'Должность 4',
//                'kz_title' => 'Должность 4',
//            ]
//        ]);

        Languages::insert([
            [
                'id' => 1,
                'slug' => 'kz',
                'title' => 'Қазақша'
            ],[
                'id' => 2,
                'slug' => 'ru',
                'title' => 'Русский'
            ]
        ]);

//        Nationalities::insert([
//            [
//                'id' => 1,
//                'ru_title' => 'Национальность 1',
//                'kz_title' => 'Национальность 1',
//            ],[
//                'id' => 2,
//                'ru_title' => 'Национальность 2',
//                'kz_title' => 'Национальность 2',
//            ],[
//                'id' => 3,
//                'ru_title' => 'Национальность 3',
//                'kz_title' => 'Национальность 3',
//            ],[
//                'id' => 4,
//                'ru_title' => 'Национальность 4',
//                'kz_title' => 'Национальность 4',
//            ]
//        ]);

        Genders::insert([
            [
                'id' => 1,
                'slug' => 'man',
                'title' => 'Мужчина'
            ],[
                'id' => 2,
                'slug' => 'woman',
                'title' => 'Женщина'
            ]
        ]);

//        Locations::insert([
//            [
//                'id' => 1,
//                'type' => Locations::LOCATION_OBLAST,
//                'ru_title' => 'Нур-султан',
//                'kz_title' => 'Нур-султан'
//            ],[
//                'id' => 2,
//                'type' => Locations::LOCATION_OBLAST,
//                'ru_title' => 'Алматы',
//                'kz_title' => 'Алматы'
//            ],[
//                'id' => 3,
//                'type' => Locations::LOCATION_OBLAST,
//                'ru_title' => 'Шымкент',
//                'kz_title' => 'Шымкент'
//            ],[
//                'id' => 4,
//                'type' => Locations::LOCATION_OBLAST,
//                'ru_title' => 'Алматинская область',
//                'kz_title' => 'Алматинская область'
//            ],[
//                'id' => 5,
//                'type' => Locations::LOCATION_OBLAST,
//                'ru_title' => 'Акмолинская область',
//                'kz_title' => 'Акмолинская область'
//            ],[
//                'id' => 6,
//                'type' => Locations::LOCATION_OBLAST,
//                'ru_title' => 'Атырауская область',
//                'kz_title' => 'Атырауская область'
//            ],[
//                'id' => 7,
//                'type' => Locations::LOCATION_OBLAST,
//                'ru_title' => 'Актюбинская область',
//                'kz_title' => 'Актюбинская область'
//            ],[
//                'id' => 8,
//                'type' => Locations::LOCATION_OBLAST,
//                'ru_title' => 'Восточно-Казахстанская область',
//                'kz_title' => 'Восточно-Казахстанская область'
//            ],[
//                'id' => 9,
//                'type' => Locations::LOCATION_OBLAST,
//                'ru_title' => 'Жамбылская область',
//                'kz_title' => 'Жамбылская область'
//            ],[
//                'id' => 10,
//                'type' => Locations::LOCATION_OBLAST,
//                'ru_title' => 'Западно-Казахстанская область',
//                'kz_title' => 'Западно-Казахстанская область'
//            ],[
//                'id' => 11,
//                'type' => Locations::LOCATION_OBLAST,
//                'ru_title' => 'Карагандинская область',
//                'kz_title' => 'Карагандинская область'
//            ],[
//                'id' => 12,
//                'type' => Locations::LOCATION_OBLAST,
//                'ru_title' => 'Костанайская область',
//                'kz_title' => 'Костанайская область'
//            ],[
//                'id' => 13,
//                'type' => Locations::LOCATION_OBLAST,
//                'ru_title' => 'Кызылординская область',
//                'kz_title' => 'Кызылординская область'
//            ],[
//                'id' => 14,
//                'type' => Locations::LOCATION_OBLAST,
//                'ru_title' => 'Мангистауская область',
//                'kz_title' => 'Мангистауская область'
//            ],[
//                'id' => 15,
//                'type' => Locations::LOCATION_OBLAST,
//                'ru_title' => 'Павлодарская область',
//                'kz_title' => 'Павлодарская область'
//            ],[
//                'id' => 16,
//                'type' => Locations::LOCATION_OBLAST,
//                'ru_title' => 'Северо-Казахстанская область',
//                'kz_title' => 'Северо-Казахстанская область'
//            ],[
//                'id' => 17,
//                'type' => Locations::LOCATION_OBLAST,
//                'ru_title' => 'Туркестанская область',
//                'kz_title' => 'Туркестанская область'
//            ]]);
//        Locations::insert([
//            [
//                'type' => Locations::LOCATION_RAYON,
//                'parent' => 4,
//                'ru_title' => 'Каскелен',
//                'kz_title' => 'Каскелен'
//            ],[
//                'type' => Locations::LOCATION_RAYON,
//                'parent' => 4,
//                'ru_title' => 'Талгар',
//                'kz_title' => 'Талгар'
//            ],[
//                'type' => Locations::LOCATION_RAYON,
//                'parent' => 4,
//                'ru_title' => 'Капшагай',
//                'kz_title' => 'Капшагай'
//            ],[
//                'type' => Locations::LOCATION_RAYON,
//                'parent' => 4,
//                'ru_title' => 'Жаркент',
//                'kz_title' => 'Жаркент'
//            ],[
//                'type' => Locations::LOCATION_RAYON,
//                'parent' => 4,
//                'ru_title' => 'Текели',
//                'kz_title' => 'Текели'
//            ],[
//                'type' => Locations::LOCATION_RAYON,
//                'parent' => 4,
//                'ru_title' => 'Есик',
//                'kz_title' => 'Есик'
//            ],[
//                'type' => Locations::LOCATION_RAYON,
//                'parent' => 4,
//                'ru_title' => 'Уштобе',
//                'kz_title' => 'Уштобе'
//            ],[
//                'type' => Locations::LOCATION_RAYON,
//                'parent' => 4,
//                'ru_title' => 'Ушарал',
//                'kz_title' => 'Ушарал'
//            ],[
//                'type' => Locations::LOCATION_RAYON,
//                'parent' => 4,
//                'ru_title' => 'Сарканд',
//                'kz_title' => 'Сарканд'
//            ],[
//                'type' => Locations::LOCATION_RAYON,
//                'parent' => 5,
//                'ru_title' => 'Степногорск',
//                'kz_title' => 'Степногорск'
//            ],[
//                'type' => Locations::LOCATION_RAYON,
//                'parent' => 5,
//                'ru_title' => 'Щучинск',
//                'kz_title' => 'Щучинск'
//            ],[
//                'type' => Locations::LOCATION_RAYON,
//                'parent' => 5,
//                'ru_title' => 'Атбасар',
//                'kz_title' => 'Атбасар'
//            ],[
//                'type' => Locations::LOCATION_RAYON,
//                'parent' => 5,
//                'ru_title' => 'Макинск',
//                'kz_title' => 'Макинск'
//            ],[
//                'type' => Locations::LOCATION_RAYON,
//                'parent' => 5,
//                'ru_title' => 'Акколь',
//                'kz_title' => 'Акколь'
//            ],[
//                'type' => Locations::LOCATION_RAYON,
//                'parent' => 5,
//                'ru_title' => 'Есиль',
//                'kz_title' => 'Есиль'
//            ],[
//                'type' => Locations::LOCATION_RAYON,
//                'parent' => 5,
//                'ru_title' => 'Ерейментау',
//                'kz_title' => 'Ерейментау'
//            ],[
//                'type' => Locations::LOCATION_RAYON,
//                'parent' => 5,
//                'ru_title' => 'Державинск',
//                'kz_title' => 'Державинск'
//            ],[
//                'type' => Locations::LOCATION_RAYON,
//                'parent' => 5,
//                'ru_title' => 'Степняк',
//                'kz_title' => 'Степняк'
//            ],[
//                'type' => Locations::LOCATION_RAYON,
//                'parent' => 5,
//                'ru_title' => 'Косшы',
//                'kz_title' => 'Косшы'
//            ],[
//                'type' => Locations::LOCATION_RAYON,
//                'parent' => 6,
//                'ru_title' => 'Кульсары',
//                'kz_title' => 'Кульсары'
//            ],[
//                'type' => Locations::LOCATION_RAYON,
//                'parent' => 7,
//                'ru_title' => 'Кандыагаш',
//                'kz_title' => 'Кандыагаш'
//            ],[
//                'type' => Locations::LOCATION_RAYON,
//                'parent' => 7,
//                'ru_title' => 'Шалкар',
//                'kz_title' => 'Шалкар'
//            ],[
//                'type' => Locations::LOCATION_RAYON,
//                'parent' => 7,
//                'ru_title' => 'Хромтау',
//                'kz_title' => 'Хромтау'
//            ],[
//                'type' => Locations::LOCATION_RAYON,
//                'parent' => 7,
//                'ru_title' => 'Алга',
//                'kz_title' => 'Алга'
//            ],[
//                'type' => Locations::LOCATION_RAYON,
//                'parent' => 7,
//                'ru_title' => 'Эмба',
//                'kz_title' => 'Эмба'
//            ],[
//                'type' => Locations::LOCATION_RAYON,
//                'parent' => 7,
//                'ru_title' => 'Темир',
//                'kz_title' => 'Темир'
//            ],[
//                'type' => Locations::LOCATION_RAYON,
//                'parent' => 7,
//                'ru_title' => 'Жем',
//                'kz_title' => 'Жем'
//            ],[
//                'type' => Locations::LOCATION_RAYON,
//                'parent' => 8,
//                'ru_title' => 'Риддер',
//                'kz_title' => 'Риддер'
//            ],[
//                'type' => Locations::LOCATION_RAYON,
//                'parent' => 8,
//                'ru_title' => 'Аягоз',
//                'kz_title' => 'Аягоз'
//            ],[
//                'type' => Locations::LOCATION_RAYON,
//                'parent' => 8,
//                'ru_title' => 'Алтай',
//                'kz_title' => 'Алтай'
//            ],[
//                'type' => Locations::LOCATION_RAYON,
//                'parent' => 8,
//                'ru_title' => 'Шемонаиха',
//                'kz_title' => 'Шемонаиха'
//            ],[
//                'type' => Locations::LOCATION_RAYON,
//                'parent' => 8,
//                'ru_title' => 'Зайсан',
//                'kz_title' => 'Зайсан'
//            ],[
//                'type' => Locations::LOCATION_RAYON,
//                'parent' => 8,
//                'ru_title' => 'Курчатов',
//                'kz_title' => 'Курчатов'
//            ],[
//                'type' => Locations::LOCATION_RAYON,
//                'parent' => 8,
//                'ru_title' => 'Серебрянск',
//                'kz_title' => 'Серебрянск'
//            ],[
//                'type' => Locations::LOCATION_RAYON,
//                'parent' => 8,
//                'ru_title' => 'Чарск',
//                'kz_title' => 'Чарск'
//            ],[
//                'type' => Locations::LOCATION_RAYON,
//                'parent' => 9,
//                'ru_title' => 'Шу',
//                'kz_title' => 'Шу'
//            ],[
//                'type' => Locations::LOCATION_RAYON,
//                'parent' => 9,
//                'ru_title' => 'Каратау',
//                'kz_title' => 'Каратау'
//            ],[
//                'type' => Locations::LOCATION_RAYON,
//                'parent' => 9,
//                'ru_title' => 'Жанатас',
//                'kz_title' => 'Жанатас'
//            ],[
//                'type' => Locations::LOCATION_RAYON,
//                'parent' => 10,
//                'ru_title' => 'Аксай',
//                'kz_title' => 'Аксай'
//            ],[
//                'type' => Locations::LOCATION_RAYON,
//                'parent' => 11,
//                'ru_title' => 'Жезказган',
//                'kz_title' => 'Жезказган'
//            ],[
//                'type' => Locations::LOCATION_RAYON,
//                'parent' => 11,
//                'ru_title' => 'Балхаш',
//                'kz_title' => 'Балхаш'
//            ],[
//                'type' => Locations::LOCATION_RAYON,
//                'parent' => 11,
//                'ru_title' => 'Сатпаев',
//                'kz_title' => 'Сатпаев'
//            ],[
//                'type' => Locations::LOCATION_RAYON,
//                'parent' => 11,
//                'ru_title' => 'Сарань',
//                'kz_title' => 'Сарань'
//            ],[
//                'type' => Locations::LOCATION_RAYON,
//                'parent' => 11,
//                'ru_title' => 'Шахтинск',
//                'kz_title' => 'Шахтинск'
//            ],[
//                'type' => Locations::LOCATION_RAYON,
//                'parent' => 11,
//                'ru_title' => 'Абай',
//                'kz_title' => 'Абай'
//            ],[
//                'type' => Locations::LOCATION_RAYON,
//                'parent' => 11,
//                'ru_title' => 'Приозёрск',
//                'kz_title' => 'Приозёрск'
//            ],[
//                'type' => Locations::LOCATION_RAYON,
//                'parent' => 11,
//                'ru_title' => 'Каражал',
//                'kz_title' => 'Каражал'
//            ],[
//                'type' => Locations::LOCATION_RAYON,
//                'parent' => 11,
//                'ru_title' => 'Каркаралинск',
//                'kz_title' => 'Каркаралинск'
//            ],[
//                'type' => Locations::LOCATION_RAYON,
//                'parent' => 12,
//                'ru_title' => 'Лисаковск',
//                'kz_title' => 'Лисаковск'
//            ],[
//                'type' => Locations::LOCATION_RAYON,
//                'parent' => 12,
//                'ru_title' => 'Житикара',
//                'kz_title' => 'Житикара'
//            ],[
//                'type' => Locations::LOCATION_RAYON,
//                'parent' => 12,
//                'ru_title' => 'Аркалык',
//                'kz_title' => 'Аркалык'
//            ],[
//                'type' => Locations::LOCATION_RAYON,
//                'parent' => 12,
//                'ru_title' => 'Тобыл',
//                'kz_title' => 'Тобыл'
//            ],[
//                'type' => Locations::LOCATION_RAYON,
//                'parent' => 13,
//                'ru_title' => 'Байконур',
//                'kz_title' => 'Байконур'
//            ],[
//                'type' => Locations::LOCATION_RAYON,
//                'parent' => 13,
//                'ru_title' => 'Аральск',
//                'kz_title' => 'Аральск'
//            ],[
//                'type' => Locations::LOCATION_RAYON,
//                'parent' => 13,
//                'ru_title' => 'Казалинск',
//                'kz_title' => 'Казалинск'
//            ],[
//                'type' => Locations::LOCATION_RAYON,
//                'parent' => 14,
//                'ru_title' => 'Жанаозен',
//                'kz_title' => 'Жанаозен'
//            ],[
//                'type' => Locations::LOCATION_RAYON,
//                'parent' => 14,
//                'ru_title' => 'Форт-Шевченко',
//                'kz_title' => 'Форт-Шевченко'
//            ],[
//                'type' => Locations::LOCATION_RAYON,
//                'parent' => 15,
//                'ru_title' => 'Аксу',
//                'kz_title' => 'Аксу'
//            ],[
//                'type' => Locations::LOCATION_RAYON,
//                'parent' => 16,
//                'ru_title' => 'Тайынша',
//                'kz_title' => 'Тайынша'
//            ],[
//                'type' => Locations::LOCATION_RAYON,
//                'parent' => 16,
//                'ru_title' => 'Булаево',
//                'kz_title' => 'Булаево'
//            ],[
//                'type' => Locations::LOCATION_RAYON,
//                'parent' => 16,
//                'ru_title' => 'Сергеевка',
//                'kz_title' => 'Сергеевка'
//            ],[
//                'type' => Locations::LOCATION_RAYON,
//                'parent' => 16,
//                'ru_title' => 'Мамлютка',
//                'kz_title' => 'Мамлютка'
//            ],[
//                'type' => Locations::LOCATION_RAYON,
//                'parent' => 17,
//                'ru_title' => 'Кентау',
//                'kz_title' => 'Кентау'
//            ],[
//                'type' => Locations::LOCATION_RAYON,
//                'parent' => 17,
//                'ru_title' => 'Арыс',
//                'kz_title' => 'Арыс'
//            ],[
//                'type' => Locations::LOCATION_RAYON,
//                'parent' => 17,
//                'ru_title' => 'Сарыагаш',
//                'kz_title' => 'Сарыагаш'
//            ],[
//                'type' => Locations::LOCATION_RAYON,
//                'parent' => 17,
//                'ru_title' => 'Шардара',
//                'kz_title' => 'Шардара'
//            ],[
//                'type' => Locations::LOCATION_RAYON,
//                'parent' => 17,
//                'ru_title' => 'Жетысай',
//                'kz_title' => 'Жетысай'
//            ],[
//                'type' => Locations::LOCATION_RAYON,
//                'parent' => 17,
//                'ru_title' => 'Ленгер',
//                'kz_title' => 'Ленгер'
//            ],
//
//        ]);
//        User::insert($users);
    }
}
