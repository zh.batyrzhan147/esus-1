<?php

namespace Database\Seeders;

use App\Models\Role;
use App\Models\User;
use Illuminate\Database\Seeder;

class RoleUserTableSeeder extends Seeder
{
    public function run()
    {
        User::findOrFail(1)->roles()->sync(Role::ROLE_SUPERADMIN);
//        User::findOrFail(2)->roles()->sync(Role::ROLE_USER);
    }
}
