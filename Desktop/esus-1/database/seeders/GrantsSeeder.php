<?php

namespace Database\Seeders;

use App\Models\Affiliates;
use App\Models\Courses;
use App\Models\Grants;
use App\Models\Languages;
use App\Models\Programs;
use App\Models\Years;
use Illuminate\Database\Seeder;

class GrantsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = \Faker\Factory::create();
        $affiliates = Affiliates::all();
        $courses = Courses::all();


        Years::all()->each(function ($years) use ($courses) {
            $years->courses()->attach(
                $courses->random(rand(10, 20))->pluck('id')->toArray()
            );
        });

        $programs = Years::where('id',2022)->first()->courses;

        foreach ($affiliates as $affiliate) {
            foreach ($programs as $program) {
                $all = $faker->numberBetween(100, 400);
                $kz = $all - $faker->numberBetween(100, ceil($all*0.8));
                $ru = $all - $kz;
                $lang = [
                    'kz' => $kz,
                    'ru' => $ru
                ];
                Grants::insert([
                    'year' => 2022,
                    'count_all' => $all,
                    'counts' => json_encode($lang),
                    'affiliate' => $affiliate->id,
                    'courses' => $program->id,
                ]);
            }
        }
        $all = Grants::all()
            ->sum('count_all');
        $counts = [];
        foreach (Languages::all() as $lang) {
            $counts[$lang->slug] = array_sum(Grants::all()->pluck('countsArray')->pluck($lang->slug)->toArray());
        }
//        $this->grants_all->counts = json_encode($counts)
//
//        $kz = Grants::all()
//            ->sum('count_kz');
//        $ru = $all - $kz;
        Grants::insert([
            'year' => 2022,
            'count_all' => $all,
            'counts' => json_encode($counts),
        ]);
//
        foreach ($affiliates as $affiliate) {
            $all = Grants::all()
                ->where('affiliate', $affiliate->id)
                ->sum('count_all');

            $counts = [];
            foreach (Languages::all() as $lang) {
                $counts[$lang->slug] = array_sum(Grants::all()->where('affiliate', $affiliate->id)->pluck('countsArray')->pluck($lang->slug)->toArray());
            }
//            $kz = Grants::all()
//                ->where('affiliate', $affiliate->id)
//                ->sum('count_kz');
//            $ru = $all - $kz;
            Grants::insert([
                'year' => 2022,
                'count_all' => $all,
                'counts' => json_encode($counts),
                'affiliate' => $affiliate->id,
            ]);
        }
//
        foreach ($programs as $program) {
            $all = Grants::all()
                ->where('courses', $program->id)
                ->sum('count_all');

            $counts = [];
            foreach (Languages::all() as $lang) {
                $counts[$lang->slug] = array_sum(Grants::all()->where('courses', $program->id)->pluck('countsArray')->pluck($lang->slug)->toArray());
            }
//            $kz = Grants::all()
//                ->where('program', $program->id)
//                ->sum('count_kz');
//            $ru = $all - $kz;
            Grants::insert([
                'year' => 2022,
                'count_all' => $all,
                'counts' => json_encode($counts),
                'courses' => $program->id,
            ]);
        }

    }
}
