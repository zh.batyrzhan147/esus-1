<?php

namespace Database\Seeders;

use App\Models\Affiliates;
use App\Models\Languages;
use App\Models\Role;
use App\Models\User;
use App\Models\Years;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class UsersTableSeeder extends Seeder
{
    public function run()
    {
        $faker = \Faker\Factory::create();

        $users = [
            [
                'id' => 1,
                'name' => 'Admin',
                'last_name' => 'Admin',
                'email' => 'admin@admin.com',
                'password' => bcrypt('password'),
                'iin' => 'admin',
                'remember_token' => null,
                'email_verified_at' => date('Y-m-d h:i:s',strtotime('now')),
            ],
        ];

        User::insert($users);
        if (\App::environment() === 'local') {

            $courses = Years::where('id', 2022)->first()->courses;

            $i = 2;
            $affilates = Affiliates::all();
            foreach ($courses as $course) {
                foreach ($affilates as $aff) {
                    $user = User::insert([[
                        'id' => $i,
                        'name' => $faker->name,
                        'last_name' => $faker->lastName,
                        'email' => $faker->unique()->safeEmail,
                        'password' => bcrypt('password'),
                        'iin' => $faker->numerify('############'),
                        'remember_token' => Str::random(10),
                        'affiliate' => $aff->id,
                    ], [
                        'id' => $i + 1,
                        'name' => $faker->name,
                        'last_name' => $faker->lastName,
                        'email' => $faker->unique()->safeEmail,
                        'password' => bcrypt('password'),
                        'iin' => $faker->numerify('############'),
                        'remember_token' => Str::random(10),
                        'affiliate' => $aff->id,
                    ], [
                        'id' => $i + 2,
                        'name' => $faker->name,
                        'last_name' => $faker->lastName,
                        'email' => $faker->unique()->safeEmail,
                        'password' => bcrypt('password'),
                        'iin' => $faker->numerify('############'),
                        'remember_token' => Str::random(10),
                        'affiliate' => $aff->id,
                    ], [
                        'id' => $i + 3,
                        'name' => $faker->name,
                        'last_name' => $faker->lastName,
                        'email' => $faker->unique()->safeEmail,
                        'password' => bcrypt('password'),
                        'iin' => $faker->numerify('############'),
                        'remember_token' => Str::random(10),
                        'affiliate' => $aff->id,
                    ]]);
                    if ($user) {
                        $course->trainers()->attach([$i]);
                        $course->trainers()->attach([($i + 1)]);
                        $course->trainers()->attach([$i + 2]);
                        $course->trainers()->attach([$i + 3]);
                        $i = $i + 4;
                    }
                }
            }

            User::all()->each(function ($users) {
                $users->roles()->attach(
                    [Role::ROLE_USER, Role::ROLE_TRAINER]
                );
                $users->languages()->attach(
                    [($users->id % 2) + 1]
                );
            });
        }


    }
}
