<?php

namespace Database\Seeders;

use App\Models\Permission;
use Illuminate\Database\Seeder;

class PermissionsTableSeeder extends Seeder
{
    public function run()
    {
//        $permissions = [
//            [
//                'id'    => 1,
//                'title' => \App\Models\Permission::PERMISSION_SUPERADMIN,
//            ],
//            [
//                'id'    => 2,
//                'title' => \App\Models\Permission::PERMISSION_SUPERADMIN,
//            ],
//        ];

//        Permission::insert($permissions);
        foreach (Permission::listPermissions() as $key => $item)
        {
            Permission::insert([
                'id'    => $key,
                'title' => $item,
            ]);
        }
    }
}
