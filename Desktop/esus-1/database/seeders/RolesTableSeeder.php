<?php

namespace Database\Seeders;

use App\Models\Role;
use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{
    public function run()
    {

        foreach (Role::listRoles() as $key => $role)
        {
            Role::insert([
                'id'    => $key,
                'title' => $role,
            ]);
        }

    }
}
