<?php

namespace Database\Seeders;

use App\Models\Courses;
use App\Models\Programs;
use Illuminate\Database\Seeder;

class ProgramsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Programs::factory(6)->create();

        $courses = Courses::all();

        Programs::all()->each(function ($programs) use ($courses) {
            $programs->courses()->attach(
                $courses->random(rand(1, 3))->pluck('id')->toArray()
            );
        });

    }
}
