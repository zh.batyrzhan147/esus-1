<?php

namespace Database\Seeders;

use App\Models\CourseDates;
use App\Models\Years;
use Illuminate\Database\Seeder;

class CourseDatesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = \Faker\Factory::create();

        $courses = Years::where('id', 2022)->first()->courses;
        foreach ($courses as $course) {
            $date = date('Y-m-d',$faker->dateTimeBetween('01.01.2022','31.09.2022','Asia/Almaty')->getTimestamp());
            $length = ceil($course->course_length * 12 /40)*7;
            CourseDates::insert([
                'start' => $date,
                'end' => date('Y-m-d',strtotime($date.' +'.$length.' days')),
                'courses' => $course->id,
                'year' => 2022,
            ]);
        }
    }
}
