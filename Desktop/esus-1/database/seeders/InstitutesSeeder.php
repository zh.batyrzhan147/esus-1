<?php

namespace Database\Seeders;

use App\Models\Institutes;
use App\Models\InstitutesTypes;
use App\Models\Locations;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Storage;

class InstitutesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

//        Institutes::factory(10)->create();
        $storagePath  = Storage::disk('local')->getDriver()->getAdapter()->getPathPrefix();

        $locations = \JsonMachine\JsonMachine::fromFile($storagePath.'/locations.json');

        foreach ($locations as $location) {
            $obl = Locations::where('ru_title',$location['Область'])->where('type',Locations::LOCATION_OBLAST)->first();
            if (!$obl) {
                $obl = new Locations([
                    'type' => Locations::LOCATION_OBLAST,
                    'ru_title' => $location['Область'],
                    'kz_title' => $location['Область'],
                ]);
                $obl->save();
            }

            $rayon = Locations::where('ru_title',$location['Район'])->where('type',Locations::LOCATION_RAYON)->first();
            if (!$rayon) {
                $rayon = new Locations([
                    'type' => Locations::LOCATION_RAYON,
                    'parent' => $obl->id,
                    'ru_title' => $location['Район'],
                    'kz_title' => $location['Район'],
                ]);
                $rayon->save();
            }

            $aul = Locations::where('ru_title',$location['Населенный пункт'])->where('type',Locations::LOCATION_AUL)->first();
            if (!$aul) {
                $aul = new Locations([
                    'type' => Locations::LOCATION_AUL,
                    'parent' => $rayon->id,
                    'ru_title' => $location['Населенный пункт'],
                    'kz_title' => $location['Населенный пункт'],
                ]);
                $aul->save();
            }

            $insttype = InstitutesTypes::where('title',$location['Тип организации образования'])->first();
            if (!$insttype) {
                $insttype = new InstitutesTypes([
                    'title' => $location['Тип организации образования'],
                ]);
                $insttype->save();
            }

            $insitute = Institutes::find($location['ID']);
            if (!$insitute) {

                Institutes::insert([
                    'id' => $location['ID'],
                    'title' => $location['Наименование организации'],
                    'kz_title' => $location['Полное наименование на казахском'],
                    'ru_title' => $location['Полное наименование на русском'],
                    'type'  => $insttype->id,
                    'city' => $aul->id,
                    'rayon' => $rayon->id,
                    'oblast' => $obl->id,
                    'address' => '',
                    'type_org' => $location['Форма собственности'],
                    'type_school' => '',
                ]);
            }
        }

    }
}
