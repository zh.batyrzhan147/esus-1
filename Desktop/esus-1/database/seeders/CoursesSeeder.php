<?php

namespace Database\Seeders;

use App\Models\Courses;
use App\Models\Years;
use Cocur\Slugify\Slugify;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Storage;

class CoursesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Years::updateOrCreate([
            'id' => 2021,
            'year' => 2021
        ]);
        Years::updateOrCreate([
            'id' => 2022,
            'year' => 2022
        ]);

//        \App\Models\Courses::factory(10)->create();
        $storagePath  = Storage::disk('local')->getDriver()->getAdapter()->getPathPrefix();

        $courses = \JsonMachine\JsonMachine::fromFile($storagePath.'/courses.json');

        foreach ($courses as $cours) {
//            dd($cours);
//            die();\
            $faker = \Faker\Factory::create();
            $slugify = new Slugify();

            Courses::insert([
                'kz_title'          => $cours['p_name_kz'],
                'ru_title'         => $cours['p_name'],
                'slug'              => substr($slugify->slugify($cours['p_name']),1,255),
                'type'              => $faker->randomElement([Courses::COURSES_ONLINE]),
                'grading'           => $faker->randomElement([Courses::COURSES_GRADING_YES]),
                'course_length'     => $faker->randomElement([4,5,6,7,8])*8,
            ]);
        }


    }
}
