<?php

namespace Database\Seeders;

use App\Models\Permission;
use App\Models\Role;
use Illuminate\Database\Seeder;

class PermissionRoleTableSeeder extends Seeder
{
    public function run()
    {
        $superadmin_permissions = Permission::all();
        Role::findOrFail(Role::ROLE_SUPERADMIN)->permissions()->sync($superadmin_permissions->pluck('id'));

        Role::findOrFail(Role::ROLE_ADMIN)->permissions()->sync(
            Permission::PERMISSION_ADMIN
        );

        Role::findOrFail(Role::ROLE_TRAINER)->permissions()->sync(
            Permission::PERMISSION_TRAINER
        );

        Role::findOrFail(Role::ROLE_OOP)->permissions()->sync(
            Permission::PERMISSION_OOP
        );

        Role::findOrFail(Role::ROLE_OMR)->permissions()->sync(
            Permission::PERMISSION_OMR
        );

        Role::findOrFail(Role::ROLE_OOUP)->permissions()->sync(
            Permission::PERMISSION_OOUP
        );

        Role::findOrFail(Role::ROLE_AFFILIATE)->permissions()->sync(
            Permission::PERMISSION_AFFILIATE
        );

        Role::findOrFail(Role::ROLE_ACCOUNTANT)->permissions()->sync(
            Permission::PERMISSION_ACCOUNTANT
        );

//        Role::findOrFail(Role::ROLE_USER)->permissions()->sync(
//            Permission::PERMISSION_USER
//        );

//        $admin_permissions = Permission::all();
//        Role::findOrFail(1)->permissions()->sync($admin_permissions->pluck('id'));
//        $user_permissions = $admin_permissions->filter(function ($permission) {
//            return substr($permission->title, 0, 5) != 'user_';
//        });
//        Role::findOrFail(2)->permissions()->sync($user_permissions);
    }
}
