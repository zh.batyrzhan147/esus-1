<?php

namespace Database\Factories;

use App\Models\Courses;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class CoursesFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Courses::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $title = $this->faker->jobTitle;
        return [
            'kz_title'          => $title,
            'ru_title'         => $title,
            'slug'              => $this->faker->slug(10),
            'type'              => $this->faker->randomElement(['online','offline','mixed']),
            'grading'           => $this->faker->boolean,
            'course_length'     => $this->faker->randomNumber(2),
//                'developer'         => '',
            'file_1_listener'   => $this->faker->imageUrl,
            'file_2_listener'   => $this->faker->imageUrl,
            'file_1_teacher'    => $this->faker->imageUrl,
            'file_2_teacher'    => $this->faker->imageUrl,
            'webinar_count'     => $this->faker->randomDigitNot(0),
            'webinar_length'    => $this->faker->randomNumber(2),
            'sdo_category'      => 'SDO_category 1',
            'sdo_link'          => 'SDO_LINK',
            'one_drive_link'    => 'ONE_DRIVE_LINK',
        ];
    }
}
