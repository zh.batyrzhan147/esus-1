<?php

namespace Database\Factories;

use App\Models\Institutes;
use App\Models\Locations;
use Illuminate\Database\Eloquent\Factories\Factory;

class InstitutesFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Institutes::class;


    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $title = $this->faker->jobTitle;
        $cityId = $this->faker->randomElement(Locations::where('type',Locations::LOCATION_RAYON)->get()->pluck('id')->toArray());
        $city = Locations::find($cityId);
        return [
            'title' => $title,
            'kz_title' => $title,
            'ru_title' => $title,
            'type'  => $this->faker->randomElement(array_keys(Institutes::listTypes())),
            'city' => $city->id,
            'rayon' => null,
            'oblast' => $city->parentItem,
            'address' => $this->faker->address,
            'type_org' => '',
            'type_school' => '',
        ];
    }
}
