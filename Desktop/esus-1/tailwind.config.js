const defaultTheme = require('tailwindcss/defaultTheme');

module.exports = {
    content: [
        './vendor/laravel/jetstream/**/*.blade.php',
        './storage/framework/views/*.php',
        './resources/views/**/*.blade.php',

    ],

    theme: {
        extend: {
            colors: {
                'gray-bg': '#F8F8F8',
                'messages-hover': '#EDEDED',
                'table-hover': '#E3EEFF',
                'main-black': '#1C1C1C',
                'statement-green': '#63D650',
                'statement-red': '#FF5A43',
                'statement-blue': '#3B82F6',
                'statement-black': '#1C1C1C',
                'logo-blue': '#1B55B5',
                'user-border': '#E1E1E1',
                'user-bg': '#E2ECFF',
                'user-gray': '#646464',
                'soft-gray': '#B5B5B5',
                'primary-button': '#3B82F6',
            },
            fontFamily: {
                sans: ['Roboto', ...defaultTheme.fontFamily.sans],
                'inter': ['Inter', 'sans-serif'],
            },
            boxShadow: {
                'user': '0px 8px 24px rgba(17, 34, 95, 0.12)',
            },
            zIndex: {
                '99': '99',
                '100': '100',
            },
            minWidth: {
                'xs': '320px',
                'sm': '600px',
            },
            transformOrigin: {
                '0': '0',
            }
        },
    },

    variants: {
        opacity: ['responsive', 'hover', 'focus', 'disabled'],
    },

    plugins: [
        require('flowbite/plugin')
    ],
};
