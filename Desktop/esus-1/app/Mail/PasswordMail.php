<?php

namespace App\Mail;

use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class PasswordMail extends Mailable
{
    use Queueable, SerializesModels;

    private $iin;
    private $password;
    private $new;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($iin, $password = null, $new = false)
    {
        $this->iin = $iin;
        $this->password = $password;
        $this->new = $new;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $subject = 'Новый пароль';
        if ($this->new) $subject = 'Успешная регистрация';
        return $this->view('mail.password-mail',[
            'iin' => $this->iin,
            'password' => $this->password,
            'new' => $this->new,
        ])->subject($subject);
    }
}
