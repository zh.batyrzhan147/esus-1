<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Admin\Controller;
use App\Models\Permission;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;

class UserController extends Controller
{
    public function get(Request $request)
    {
        $user =  $request->user();
        if (!Gate::denies(Permission::PERMISSION_TRAINER)) {
            return [
                'iin' => $user->iin,
                'email' => $user->email,
                'username' => $user->email,
                'last_name' => $user->last_name,
                'name' => $user->name,
                'middlename' => $user->patronymic,
                'phone' => $user->phone,
                'city' => $user->oblastItem->kz_title,
                'slug' => $user->trainer_oauth_group,
                'group' => $user->trainer_oauth_group ? substr($user->trainer_oauth_group, 5) : $user->trainer_oauth_group,
//                'trainer' => $user->trainer_oauth_group,
            ];

        } else {
            return [
                'iin' => $user->iin,
                'email' => $user->email,
                'username' => $user->email,
                'last_name' => $user->last_name,
                'name' => $user->name,
                'middlename' => $user->patronymic,
                'phone' => $user->phone,
                'city' => $user->oblastItem->kz_title,
                'slug' => $user->last_group_slug,
                'group' => $user->last_group_slug ? substr($user->last_group_slug, strpos($user->last_group_slug, "-") + 1) : $user->last_group_slug,
//                'trainer' => $user->trainer_oauth_group,
            ];
        }
    }
}
