<?php

namespace App\Http\Controllers\Api;

use Carbon\Carbon;
use App\Models\Orders;
use App\Events\Order\Payed;
use Illuminate\Http\Request;
use App\Http\Controllers\Admin\Controller;

class KaspiController extends Controller
{
    /**
     * @var string
     */
    const PROVIDER_IP = '194.187.247.';

    /**
     * @var int
     */
    const RESULT_SUCCESS = 0;

    /**
     * @var int
     */
    const RESULT_ERROR_NOT_FOUND = 1;

    /**
     * @var int
     */
    const RESULT_ERROR_CANCELED = 2;

    /**
     * @var int
     */
    const RESULT_ERROR_PAYED = 3;

    /**
     * @var int
     */
    const RESULT_ERROR_PROCESS = 4;

    /**
     * @var int
     */
    const RESULT_ERROR_OTHER = 5;

    /**
     * @param Request $request
     * @return array
     */
    public function get(Request $request)
    {
        $response = [
            'txn_id' => $request->get('txn_id'),
            'prv_txn' => null,
            'sum' => (float)$request->get('sum'),
            'result' => static::RESULT_SUCCESS,
            'comment' => null
        ];

        if (empty($response['txn_id'])) {
            $response['result'] = static::RESULT_ERROR_NOT_FOUND;

            return response()->xml($response);
        }

        /** @var Orders $order */
        $order = Orders::query()->whereExternalId($response['txn_id'])->first();

        if (empty($order)) {
            $order = Orders::find((int)$request->get('account'));
        }

        if (is_null($order)) {
            $response['result'] = static::RESULT_ERROR_NOT_FOUND;
        } elseif ($order->isCanceled()) {
            $response['result'] = static::RESULT_ERROR_CANCELED;
        } else {
            $response['prv_txn'] = $order->id;

            if ($request->get('command') === 'pay') {
                if ($response['sum'] !== $order->total) {
                    $response['result'] = static::RESULT_ERROR_OTHER;
                    $response['comment'] = 'Error sum';
                }
            }

            if ($order->isPayed()) {
                $response['result'] = static::RESULT_ERROR_PAYED;
            }
        }

        if ($response['result'] === static::RESULT_SUCCESS) {
            switch ($request->get('command')) {
                case 'pay':
                    $order->payed = true;
                    $order->date_payed = Carbon::parse($request->get('txn_date'));
                    $order->external_id = $response['txn_id'];

                    try {
                        $order->save();

                        event(new Payed($order));

                        $response['comment'] = 'Success';
                    } catch (\Exception $error) {
                        $response['result'] = static::RESULT_ERROR_OTHER;
                        $response['comment'] = $error->getMessage();
                    }

                    break;
                case 'check':
                    $user = $order->user;
                    $name = null;

                    if ($order->isTargetGroups()) {
                        $name = $order->target->coursesItem->ru_title;
                    }

                    $response['sum'] = (float)$order->total;
                    $response['fields'] = [
                        'field1' => [
                            '@attributes' => ['name' => 'Ф.И.О.'],
                            '@value' => implode(' ', array_filter([$user->name, $user->last_name]))
                        ],
                        'field2' => [
                            '@attributes' => ['name' => 'Наименование'],
                            '@value' => $name
                        ]
                    ];

                    break;
            }
        }

        return response()->xml($response);
    }
}
