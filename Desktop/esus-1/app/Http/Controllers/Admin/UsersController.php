<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\StoreUserRequest;
use App\Http\Requests\UpdateUserRequest;
use App\Models\Role;
use App\Models\User;
use Illuminate\Support\Facades\Gate;
use Symfony\Component\HttpFoundation\Response;
use function abort_if;
use function redirect;
use function view;

class UsersController extends Controller
{
    public function index()
    {
        abort_if(Gate::denies(\App\Models\Permission::PERMISSION_SUPERADMIN), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $users = User::with('roles')->get();

        return view('users.index', compact('users'))->layout('layouts.admin');
    }

    public function create()
    {
        abort_if(Gate::denies(\App\Models\Permission::PERMISSION_SUPERADMIN), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $roles = Role::pluck('title', 'id');

        return view('users.create', compact('roles'))->layout('layouts.admin');
    }

    public function store(StoreUserRequest $request)
    {
        $user = User::create($request->validated());
        $user->roles()->sync($request->input('roles', []));

        return redirect()->route('admin.users.index');
    }

    public function show(User $user)
    {
        abort_if(Gate::denies(\App\Models\Permission::PERMISSION_SUPERADMIN), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('users.show', compact('user'))->layout('layouts.admin');
    }

    public function edit(User $user)
    {
        abort_if(Gate::denies(\App\Models\Permission::PERMISSION_SUPERADMIN), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $roles = Role::pluck('title', 'id');

        $user->load('roles');

        return view('users.edit', compact('user', 'roles'))->layout('layouts.admin');
    }

    public function update(UpdateUserRequest $request, User $user)
    {
        $user->update($request->validated());
        $user->roles()->sync($request->input('roles', []));

        return redirect()->route('admin.users.index');
    }

    public function destroy(User $user)
    {
        abort_if(Gate::denies(\App\Models\Permission::PERMISSION_SUPERADMIN), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $user->delete();

        return redirect()->route('admin.users.index');
    }
}
