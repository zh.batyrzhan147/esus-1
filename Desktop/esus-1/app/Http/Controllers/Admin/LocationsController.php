<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\UpdateLocationsRequest;
use App\Models\Locations;
use Illuminate\Support\Facades\Gate;
use Symfony\Component\HttpFoundation\Response;
use function abort_if;
use function redirect;
use function view;

class LocationsController extends Controller
{

    private function getRedirectRoute($type) {
        if ($type == Locations::LOCATION_RAYON){
            $url = 'admin.locations.rayon.index';
        } elseif ($type == Locations::LOCATION_OBLAST){
            $url = 'admin.locations.oblast.index';
        } else {
            $url = 'admin.locations.aul.index';
        }

        return $url;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index_aul()
    {
        abort_if(Gate::denies(\App\Models\Permission::PERMISSION_SUPERADMIN), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $locations = Locations::where('type',Locations::LOCATION_AUL)->paginate(20);

        return view('admin.locations.aul.index', compact('locations'))->layout('layouts.admin');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index_city()
    {
        abort_if(Gate::denies(\App\Models\Permission::PERMISSION_SUPERADMIN), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $locations = Locations::where('type',Locations::LOCATION_GOROD)->paginate(20);

        return view('admin.locations.city.index', compact('locations'))->layout('layouts.admin');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index_oblast()
    {
        abort_if(Gate::denies(\App\Models\Permission::PERMISSION_SUPERADMIN), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $locations = Locations::where('type',Locations::LOCATION_OBLAST)->paginate(20);

        return view('admin.locations.oblast.index', compact('locations'))->layout('layouts.admin');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index_rayon()
    {
        abort_if(Gate::denies(\App\Models\Permission::PERMISSION_SUPERADMIN), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $locations = Locations::where('type',Locations::LOCATION_RAYON)->paginate(20);

        return view('admin.locations.rayon.index', compact('locations'))->layout('layouts.admin');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
//        abort_if(Gate::denies(\App\Models\Permission::PERMISSION_SUPERADMIN), Response::HTTP_FORBIDDEN, '403 Forbidden');
//
//        $locations = Locations::where('type',Locations::LOCATION_AUL)->paginate(20);

//        return view('admin.locations.aul.index', compact('locations'));
        return redirect()->route('admin.locations.oblast.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        abort_if(Gate::denies(\App\Models\Permission::PERMISSION_SUPERADMIN), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.locations.create')->layout('layouts.admin');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UpdateLocationsRequest $request)
    {
        Locations::create($request->validated());


        return redirect()->route($this->getRedirectRoute($request->validated()['type']));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Locations  $locations
     * @return \Illuminate\Http\Response
     */
    public function show(Locations $locations)
    {
        abort_if(Gate::denies(\App\Models\Permission::PERMISSION_SUPERADMIN), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.locations.show', compact('locations'))->layout('layouts.admin');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Locations $location
     * @return \Illuminate\Http\Response
     */
    public function edit(Locations $location)
    {
        abort_if(Gate::denies(\App\Models\Permission::PERMISSION_SUPERADMIN), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.locations.edit', compact('location'))->layout('layouts.admin');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateLocationsRequest $request
     * @param Locations $location
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateLocationsRequest $request, Locations $location)
    {

        $location->update($request->validated());

        return redirect()->route($this->getRedirectRoute($request->validated()['type']));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Locations $location
     * @return \Illuminate\Http\Response
     */
    public function destroy(Locations $location)
    {
        abort_if(Gate::denies(\App\Models\Permission::PERMISSION_SUPERADMIN), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $location->delete();

        return redirect()->route('admin.locations.oblast.index');
    }
}
