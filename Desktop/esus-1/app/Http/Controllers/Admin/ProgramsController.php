<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\UpdateProgramsRequest;
use App\Models\Courses;
use App\Models\Programs;
use Illuminate\Support\Facades\Gate;
use Symfony\Component\HttpFoundation\Response;
use function abort_if;
use function redirect;
use function view;

class ProgramsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        abort_if(Gate::denies(\App\Models\Permission::PERMISSION_SUPERADMIN), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $programs = Programs::paginate(20);

        return view('admin.programs.index', compact('programs'))->layout('layouts.admin');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        abort_if(Gate::denies(\App\Models\Permission::PERMISSION_SUPERADMIN), Response::HTTP_FORBIDDEN, '403 Forbidden');
        $courses = Courses::pluck('kz_title', 'id');

        return view('admin.programs.create', compact('courses'))->layout('layouts.admin');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param UpdateProgramsRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(UpdateProgramsRequest $request)
    {
        $program = Programs::create($request->validated());

        $program->courses()->sync($request->input('courses', []));

        return redirect()->route('admin.programs.index');
    }

    /**
     * Display the specified resource.
     *
     * @param Programs $program
     * @return \Illuminate\Http\Response
     */
    public function show(Programs $program)
    {
        abort_if(Gate::denies(\App\Models\Permission::PERMISSION_SUPERADMIN), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.programs.show', compact('program'))->layout('layouts.admin');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Programs $program
     * @return \Illuminate\Http\Response
     */
    public function edit(Programs $program)
    {
        abort_if(Gate::denies(\App\Models\Permission::PERMISSION_SUPERADMIN), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $courses = Courses::pluck('kz_title', 'id');

        $program->load('courses');

        return view('admin.programs.edit', compact('program', 'courses'))->layout('layouts.admin');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateProgramsRequest $request
     * @param Programs $program
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateProgramsRequest $request, Programs $program)
    {
        $program->update($request->validated());
        $program->courses()->sync($request->input('courses', []));

        return redirect()->route('admin.programs.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Programs $program
     * @return \Illuminate\Http\Response
     */
    public function destroy(Programs $program)
    {
        abort_if(Gate::denies(\App\Models\Permission::PERMISSION_SUPERADMIN), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $program->delete();

        return redirect()->route('admin.programs.index');
    }
}
