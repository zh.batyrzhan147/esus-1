<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\UpdateAcademicDegreeRequest;
use App\Models\AcademicDegrees;
use Illuminate\Support\Facades\Gate;
use Symfony\Component\HttpFoundation\Response;
use function abort_if;
use function redirect;
use function view;

class AcademicDegreesController extends Controller
{
    public function index()
    {
        abort_if(Gate::denies(\App\Models\Permission::PERMISSION_SUPERADMIN), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $academicDegrees = AcademicDegrees::paginate(20);

        return view('admin.academicdegrees.index', compact('academicDegrees'))->layout('layouts.admin');
    }

    public function create()
    {
        abort_if(Gate::denies(\App\Models\Permission::PERMISSION_SUPERADMIN), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.academicdegrees.create')->layout('layouts.admin');
    }

    public function store(UpdateAcademicDegreeRequest $request)
    {
        AcademicDegrees::create($request->validated());

        return redirect()->route('admin.academicdegrees.index');
    }

    public function show(AcademicDegrees $academicdegree)
    {
        abort_if(Gate::denies(\App\Models\Permission::PERMISSION_SUPERADMIN), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.academicdegrees.show', compact('academicdegree'))->layout('layouts.admin');
    }

    public function edit(AcademicDegrees $academicdegree)
    {
        abort_if(Gate::denies(\App\Models\Permission::PERMISSION_SUPERADMIN), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.academicdegrees.edit', compact('academicdegree'))->layout('layouts.admin');
    }

    public function update(UpdateAcademicDegreeRequest $request, AcademicDegrees $academicdegree)
    {
        $academicdegree->update($request->validated());

        return redirect()->route('admin.academicdegrees.index');
    }

    public function destroy(AcademicDegrees $academicdegree)
    {
        abort_if(Gate::denies(\App\Models\Permission::PERMISSION_SUPERADMIN), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $academicdegree->delete();

        return redirect()->route('admin.academicdegrees.index');
    }
}
