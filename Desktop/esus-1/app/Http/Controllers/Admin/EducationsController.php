<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\UpdateEducationRequest;
use App\Models\Educations;
use Illuminate\Support\Facades\Gate;
use Symfony\Component\HttpFoundation\Response;
use function abort_if;
use function redirect;
use function view;

class EducationsController extends Controller
{
    public function index()
    {
        abort_if(Gate::denies(\App\Models\Permission::PERMISSION_SUPERADMIN), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $educations = Educations::paginate(20);

        return view('admin.educations.index', compact('educations'))->layout('layouts.admin');
    }

    public function create()
    {
        abort_if(Gate::denies(\App\Models\Permission::PERMISSION_SUPERADMIN), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.educations.create')->layout('layouts.admin');
    }

    public function store(UpdateEducationRequest $request)
    {
        Educations::create($request->validated());

        return redirect()->route('admin.educations.index');
    }

    public function show(educations $education)
    {
        abort_if(Gate::denies(\App\Models\Permission::PERMISSION_SUPERADMIN), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.educations.show', compact('education'))->layout('layouts.admin');
    }

    public function edit(educations $education)
    {
        abort_if(Gate::denies(\App\Models\Permission::PERMISSION_SUPERADMIN), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.educations.edit', compact('education'))->layout('layouts.admin');
    }

    public function update(UpdateEducationRequest $request, educations $education)
    {
        $education->update($request->validated());

        return redirect()->route('admin.educations.index');
    }

    public function destroy(educations $education)
    {
        abort_if(Gate::denies(\App\Models\Permission::PERMISSION_SUPERADMIN), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $education->delete();

        return redirect()->route('admin.educations.index');
    }
}
