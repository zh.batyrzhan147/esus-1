<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\UpdateAffiliateRequest;
use App\Models\Affiliates;
use Illuminate\Support\Facades\Gate;
use Symfony\Component\HttpFoundation\Response;
use function abort_if;
use function redirect;
use function view;

class AffiliatesController extends Controller
{
    public function index()
    {
        abort_if(Gate::denies(\App\Models\Permission::PERMISSION_SUPERADMIN), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $affiliates = Affiliates::paginate(20);

        return view('admin.affiliates.index', compact('affiliates'))->layout('layouts.admin');
    }

    public function create()
    {
        abort_if(Gate::denies(\App\Models\Permission::PERMISSION_SUPERADMIN), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.affiliates.create')->layout('layouts.admin');
    }

    public function store(UpdateAffiliateRequest $request)
    {
        Affiliates::create($request->validated());

        return redirect()->route('admin.affiliates.index');
    }

    public function show(affiliates $affiliate)
    {
        abort_if(Gate::denies(\App\Models\Permission::PERMISSION_SUPERADMIN), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.affiliates.show', compact('affiliate'))->layout('layouts.admin');
    }

    public function edit(affiliates $affiliate)
    {
        abort_if(Gate::denies(\App\Models\Permission::PERMISSION_SUPERADMIN), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.affiliates.edit', compact('affiliate'))->layout('layouts.admin');
    }

    public function update(UpdateAffiliateRequest $request, affiliates $affiliate)
    {
        $affiliate->update($request->validated());

        return redirect()->route('admin.affiliates.index');
    }

    public function destroy(affiliates $affiliate)
    {
        abort_if(Gate::denies(\App\Models\Permission::PERMISSION_SUPERADMIN), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $affiliate->delete();

        return redirect()->route('admin.affiliates.index');
    }
}
