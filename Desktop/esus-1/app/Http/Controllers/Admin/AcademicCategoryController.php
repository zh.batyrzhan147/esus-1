<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\UpdateAcademicCategoryRequest;
use App\Models\AcademicCategories;
use Illuminate\Support\Facades\Gate;
use Symfony\Component\HttpFoundation\Response;
use function abort_if;
use function redirect;
use function view;

class AcademicCategoryController extends Controller
{
    public function index()
    {
        abort_if(Gate::denies(\App\Models\Permission::PERMISSION_SUPERADMIN), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $academicCategories = AcademicCategories::paginate(20);

        return view('admin.academiccategories.index', compact('academicCategories'))->layout('layouts.admin');
    }

    public function create()
    {
        abort_if(Gate::denies(\App\Models\Permission::PERMISSION_SUPERADMIN), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.academiccategories.create')->layout('layouts.admin');
    }

    public function store(UpdateAcademicCategoryRequest $request)
    {
        AcademicCategories::create($request->validated());

        return redirect()->route('admin.academiccategories.index');
    }

    public function show(AcademicCategories $academiccategory)
    {
        abort_if(Gate::denies(\App\Models\Permission::PERMISSION_SUPERADMIN), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.academiccategories.show', compact('academiccategory'))->layout('layouts.admin');
    }

    public function edit(AcademicCategories $academiccategory)
    {
        abort_if(Gate::denies(\App\Models\Permission::PERMISSION_SUPERADMIN), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.academiccategories.edit', compact('academiccategory'))->layout('layouts.admin');
    }

    public function update(UpdateAcademicCategoryRequest $request, AcademicCategories $academiccategory)
    {
        $academiccategory->update($request->validated());

        return redirect()->route('admin.academiccategories.index');
    }

    public function destroy(AcademicCategories $academiccategory)
    {
        abort_if(Gate::denies(\App\Models\Permission::PERMISSION_SUPERADMIN), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $academiccategory->delete();

        return redirect()->route('admin.academiccategories.index');
    }
}
