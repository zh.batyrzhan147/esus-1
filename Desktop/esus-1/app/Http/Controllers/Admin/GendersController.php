<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\StoreGendersRequest;
use App\Http\Requests\UpdateGendersRequest;
use App\Models\Genders;
use Illuminate\Support\Facades\Gate;
use Symfony\Component\HttpFoundation\Response;
use function abort_if;
use function redirect;
use function view;

class GendersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        abort_if(Gate::denies(\App\Models\Permission::PERMISSION_SUPERADMIN), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $genders = Genders::paginate(20);

        return view('admin.genders.index', compact('genders'))->layout('layouts.admin');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        abort_if(Gate::denies(\App\Models\Permission::PERMISSION_SUPERADMIN), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.genders.create')->layout('layouts.admin');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreGendersRequest  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(StoreGendersRequest $request)
    {
        Genders::create($request->validated());

        return redirect()->route('admin.genders.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Genders  $genders
     * @return \Illuminate\Http\Response
     */
    public function show(Genders $genders)
    {
        abort_if(Gate::denies(\App\Models\Permission::PERMISSION_SUPERADMIN), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.genders.show', compact('genders'))->layout('layouts.admin');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Genders  $gender
     * @return \Illuminate\Http\Response
     */
    public function edit(Genders $gender)
    {
        abort_if(Gate::denies(\App\Models\Permission::PERMISSION_SUPERADMIN), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.genders.edit', compact('gender'))->layout('layouts.admin');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateGendersRequest  $request
     * @param  \App\Models\Genders  $gender
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(UpdateGendersRequest $request, Genders $gender)
    {
        $gender->update($request->validated());

        return redirect()->route('admin.genders.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Genders  $gender
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(Genders $gender)
    {
        abort_if(Gate::denies(\App\Models\Permission::PERMISSION_SUPERADMIN), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $gender->delete();

        return redirect()->route('admin.genders.index');
    }
}
