<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\UpdateJobNameRequest;
use App\Models\JobNames;
use Illuminate\Support\Facades\Gate;
use Symfony\Component\HttpFoundation\Response;
use function abort_if;
use function redirect;
use function view;

class JobNamesController extends Controller
{
    public function index()
    {
        abort_if(Gate::denies(\App\Models\Permission::PERMISSION_SUPERADMIN), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $jobNames = JobNames::paginate(20);

        return view('admin.jobnames.index', compact('jobNames'))->layout('layouts.admin');
    }

    public function create()
    {
        abort_if(Gate::denies(\App\Models\Permission::PERMISSION_SUPERADMIN), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.jobnames.create')->layout('layouts.admin');
    }

    public function store(UpdateJobNameRequest $request)
    {
        JobNames::create($request->validated());

        return redirect()->route('admin.jobnames.index');
    }

    public function show(jobNames $jobname)
    {
        abort_if(Gate::denies(\App\Models\Permission::PERMISSION_SUPERADMIN), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.jobnames.show', compact('jobname'))->layout('layouts.admin');
    }

    public function edit(jobNames $jobname)
    {
        abort_if(Gate::denies(\App\Models\Permission::PERMISSION_SUPERADMIN), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.jobnames.edit', compact('jobname'))->layout('layouts.admin');
    }

    public function update(UpdateJobNameRequest $request, jobNames $jobname)
    {
        $jobname->update($request->validated());

        return redirect()->route('admin.jobnames.index');
    }

    public function destroy(jobNames $jobname)
    {
        abort_if(Gate::denies(\App\Models\Permission::PERMISSION_SUPERADMIN), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $jobname->delete();

        return redirect()->route('admin.jobnames.index');
    }
}
