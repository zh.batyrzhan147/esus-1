<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\NewCoursesRequest;
use App\Http\Requests\UpdateCoursesRequest;
use App\Models\Courses;
use Illuminate\Support\Facades\Gate;
use Symfony\Component\HttpFoundation\Response;
use function abort_if;
use function redirect;
use function view;

class CoursesController extends Controller
{

    //Moved to Livewire
//    /**
//     * Display a listing of the resource.
//     *
//     * @return \Illuminate\Http\Response
//     */
//    public function index()
//    {
//        abort_if(Gate::denies(\App\Models\Permission::PERMISSION_SUPERADMIN), Response::HTTP_FORBIDDEN, '403 Forbidden');
//
//        $courses = Courses::paginate(20);
//
//        return view('admin.courses.index', compact('courses'));
//    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        abort_if(Gate::denies(\App\Models\Permission::PERMISSION_SUPERADMIN), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.courses.create')->layout('layouts.admin');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param NewCoursesRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(NewCoursesRequest $request)
    {
        Courses::create($request->validated());

        return redirect()->route('admin.courses.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Courses  $course
     * @return \Illuminate\Http\Response
     */
    public function show(Courses $course)
    {
        abort_if(Gate::denies(\App\Models\Permission::PERMISSION_SUPERADMIN), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.courses.show', compact('course'))->layout('layouts.admin');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Courses  $course
     * @return \Illuminate\Http\Response
     */
    public function edit(Courses $course)
    {
        abort_if(Gate::denies(\App\Models\Permission::PERMISSION_SUPERADMIN), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.courses.edit', compact('course'))->layout('layouts.admin');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateCoursesRequest $request
     * @param \App\Models\Courses $course
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateCoursesRequest $request, Courses $course)
    {
        $course->update($request->validated());

        return redirect()->route('admin.courses.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Courses  $course
     * @return \Illuminate\Http\Response
     */
    public function destroy(Courses $course)
    {
        abort_if(Gate::denies(\App\Models\Permission::PERMISSION_SUPERADMIN), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $course->delete();

        return redirect()->route('admin.courses.index');
    }
}
