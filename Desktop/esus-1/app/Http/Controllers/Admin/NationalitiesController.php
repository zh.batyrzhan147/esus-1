<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\UpdateNationalityRequest;
use App\Models\Nationalities;
use Illuminate\Support\Facades\Gate;
use Symfony\Component\HttpFoundation\Response;
use function abort_if;
use function redirect;
use function view;

class NationalitiesController extends Controller
{
    public function index()
    {
        abort_if(Gate::denies(\App\Models\Permission::PERMISSION_SUPERADMIN), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $nationalities = Nationalities::paginate(20);

        return view('admin.nationalities.index', compact('nationalities'))->layout('layouts.admin');
    }

    public function create()
    {
        abort_if(Gate::denies(\App\Models\Permission::PERMISSION_SUPERADMIN), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.nationalities.create')->layout('layouts.admin');
    }

    public function store(UpdateNationalityRequest $request)
    {
        Nationalities::create($request->validated());

        return redirect()->route('admin.nationalities.index');
    }

    public function show(nationalities $nationality)
    {
        abort_if(Gate::denies(\App\Models\Permission::PERMISSION_SUPERADMIN), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.nationalities.show', compact('nationality'))->layout('layouts.admin');
    }

    public function edit(nationalities $nationality)
    {
        abort_if(Gate::denies(\App\Models\Permission::PERMISSION_SUPERADMIN), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.nationalities.edit', compact('nationality'))->layout('layouts.admin');
    }

    public function update(UpdateNationalityRequest $request, nationalities $nationality)
    {
        $nationality->update($request->validated());

        return redirect()->route('admin.nationalities.index');
    }

    public function destroy(nationalities $nationality)
    {
        abort_if(Gate::denies(\App\Models\Permission::PERMISSION_SUPERADMIN), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $nationality->delete();

        return redirect()->route('admin.nationalities.index');
    }
}
