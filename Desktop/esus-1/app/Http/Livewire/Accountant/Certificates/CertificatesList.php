<?php

namespace App\Http\Livewire\Accountant\Certificates;

use App\Models\Certificates;
use Illuminate\Support\Facades\Gate;
use Livewire\Component;
use Livewire\WithPagination;
use Symfony\Component\HttpFoundation\Response;

class CertificatesList extends Component
{
    use WithPagination;
    public $ipp = 20;

    public function mount()
    {
        abort_if(Gate::denies(\App\Models\Permission::PERMISSION_OOUP), Response::HTTP_FORBIDDEN, '403 Ошибка доступа ');
    }
    public function render()
    {
        $item = Certificates::query();
        $item = $this->applySearch($item);
        return view('livewire.accountant.certificates.certificates-list',[
            'certificates' => $item->paginate($this->ipp),
        ])->layout('layouts.accountant');
    }

    private function applySearch(\Illuminate\Database\Eloquent\Builder $item)
    {
        return $item;
    }
}
