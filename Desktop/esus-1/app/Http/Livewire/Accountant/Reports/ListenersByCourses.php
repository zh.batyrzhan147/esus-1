<?php

namespace App\Http\Livewire\Accountant\Reports;

use App\Models\Reports;
use App\Models\Years;
use Illuminate\Support\Facades\Gate;
use Livewire\Component;
use Symfony\Component\HttpFoundation\Response;

class ListenersByCourses extends Component
{

    public $ipp = 20;
    public $modal_add_new = false;
    public $reporttype;
    public $year;
    public $courses;
    public $yearItem;
    public $language;
    public $starttime;
    public $endtime;

    public function mount()
    {
        abort_if(Gate::denies(\App\Models\Permission::PERMISSION_OOUP), Response::HTTP_FORBIDDEN, '403 Ошибка доступа ');
    }

    public function render()
    {
        $reports = Reports::query();
        $reports = $this->applyFilter($reports);

        return view('livewire.accountant.reports.listeners-by-courses',[
            'reports' => $reports->paginate($this->ipp)
        ])->layout('layouts.accountant');
    }

    public function applyFilter($item)
    {
        $item = $item->with('courseItem','languageItem');
        $item = $item->orderBy('created_at', 'DESC');
        return $item;
    }

    public function add_new()
    {
        $this->modal_add_new = true;
    }

    public function updatedYear($value)
    {
        $this->yearItem = Years::where('id', $value)->first();
        $this->courses = null;
    }


    public function add_report()
    {
        if ($this->reporttype == Reports::REPORT_TYPE_LISTENER_BY_COURSES) {
            $report = new Reports();
            $report->user = auth()->user()->id;
            $report->type = $this->reporttype;
            $report->starttime = $this->starttime;
            $report->endtime = $this->endtime;
            $report->year  = $this->year;
            $report->course = $this->courses;
            $report->language = $this->language;
            $report->save();
        }
        $this->modal_add_new = false;
    }


}
