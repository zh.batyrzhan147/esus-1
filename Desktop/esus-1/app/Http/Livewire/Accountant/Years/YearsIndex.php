<?php

namespace App\Http\Livewire\Accountant\Years;

use App\Models\Years;
use Illuminate\Support\Facades\Gate;
use Livewire\Component;
use Livewire\WithPagination;
use Symfony\Component\HttpFoundation\Response;

class YearsIndex extends Component
{

    use WithPagination;
    public $modal_add_new_year = false;
    public Years $newyear;
    use \App\Traits\ValidateMessages;
    protected $rules = [
        'newyear.year' => 'required|unique:years,year|integer',
    ];

    public function mount()
    {
        abort_if(Gate::denies(\App\Models\Permission::PERMISSION_OOUP), Response::HTTP_FORBIDDEN, '403 Ошибка доступа ');
    }
    public function add_new_year()
    {
        $this->newyear = new Years();
        $this->modal_add_new_year = true;
    }

    public function store_new_year()
    {
        $this->validate();
        $this->newyear->id = $this->newyear->year;
        $this->newyear->save();
        $this->modal_add_new_year = false;
        $this->emitUp('triggerSuccessCreate');

    }

    public function destroy($id)
    {
        Years::find($id)->delete();
    }
    public function render()
    {
        return view('livewire.accountant.years.years-index', [
            'years' => Years::orderBy('year', 'ASC')->paginate(),
        ])->layout('layouts.accountant');
    }
}
