<?php

namespace App\Http\Livewire\Accountant\Groups;

use App\Jobs\GenerateCertificate;
use App\Models\Certificates;
use App\Models\Groups;
use App\Models\User;
use Hashids\Hashids;
use Livewire\Component;
use Ramsey\Uuid\Uuid;

class GroupsCertificates extends Component
{
    public $group;

    public function mount($id = null)
    {
        $this->group = Groups::where('id', $id)->first();
        if (!$this->group) abort(404);
    }

    public function issue($id)
    {
        if ($this->group->status == Groups::GROUP_STATUS_FINISHED or $this->group->status == Groups::GROUP_STATUS_FINISHED_TESTING) {
            $cert = Certificates::where('user', $id)->where('group_id',$this->group->id)->first();
            if (!$cert) {
                $user = User::where('id', $id)->first();
                $uuid4 = Uuid::uuid4();
                $hashids = new Hashids($uuid4->toString());
                $uuid = $hashids->encode(1,2,3,4).date('y');


                $c = new Certificates([
                        'fio' => $user->fio,
                        'group_title' => $this->group->slug,
                        'group_id' => $this->group->id,
                        'course_title' => $this->group->coursesItem->kz_title,
                        'course_id' => $this->group->coursesItem->id,
                        'year' => $this->group->year,
                        'description' => '',
                        'user' => $user->id,
                        'template' => $this->group->coursesItem->certificateTemplate->id,
                        'uuid' => $uuid,
                    ]
                );
                $c->save();
            }
        }
    }
    public function changeGroupStatus($status)
    {
        if ($status == Groups::GROUP_STATUS_FINISHED or $status == Groups::GROUP_STATUS_FINISHED_TESTING) {
            $this->group->status = Groups::GROUP_STATUS_CERTIFICATED;
            $this->group->save();
        }
    }

    public function destroy($id)
    {
        Certificates::find($id)->delete();
    }

    public function render()
    {
        $certs = Certificates::where('group_id', $this->group->id)->get();
        return view('livewire.accountant.groups.groups-certificates', [
            'certs' => $certs,
        ])->layout('layouts.accountant');
    }
}
