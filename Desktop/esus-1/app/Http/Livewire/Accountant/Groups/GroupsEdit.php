<?php

namespace App\Http\Livewire\Accountant\Groups;

use App\Exports\StatementsExport;
use App\Models\Courses;
use App\Models\Groups;
use App\Models\Years;
use Illuminate\Support\Facades\Gate;
use Livewire\Component;
use Livewire\WithFileUploads;
use Maatwebsite\Excel\Facades\Excel;
use Ramsey\Uuid\Uuid;
use Symfony\Component\HttpFoundation\Response;
use function abort_if;
use function view;

class GroupsEdit extends Component
{
    use WithFileUploads;
    public $group;
    public $year;
    public $yearItem;
    public $trainers = [];
    public $courses;
    public $offer_agreement;
    public $slug;

    use \App\Traits\ValidateMessages;
    public function rules()
    {
        return [
            'slug' => 'required|string|unique:groups,slug,' . $this->group->id,
            'group.language' => 'required|integer',
            'group.affiliate' => 'integer|nullable',
            'year' => 'required|integer',
            'offer_agreement' => 'nullable|file|max:1000000',
            'courses' => 'required|integer',
            'group.trainer' => 'required|integer',
            'group.status' => 'required|integer',
            'group.count_all' => 'required|integer',
            'group.start_time' => 'required|date',
            'group.end_time' => 'required|date'
        ];
    }

//        protected $rules = ;

    public function mount($id = null)
    {
        abort_if(Gate::denies(\App\Models\Permission::PERMISSION_OOUP), Response::HTTP_FORBIDDEN, '403 Ошибка доступа ');

        if ($id) {
            $this->group = Groups::find($id);
            $this->slug = $this->group->slug;
            $this->year = $this->group->year;
            $this->group->slug = $this->slug;
            if ($this->year) $this->yearItem = Years::where('id', $this->year)->first();
            if ($this->year) $this->courses = $this->group->courses;
            if ($this->courses) $this->trainers = Courses::where('id', $this->courses)->first()->trainers;
        } else {
            abort(404);
//            $this->group = new Groups();
        }
    }

    public function updatedYear($value)
    {
        $this->yearItem = Years::where('id', $value)->first();
        $this->courses = null;
        $this->group->trainer = null;
    }


    public function updated()
    {
        if ($this->courses) {
            $cours = Courses::where('id', $this->courses)->first();
            $lang = $this->group->language;
            if ($cours->type == Courses::COURSES_ONLINE) {
                $this->trainers = $cours->trainers()->whereHas('languages', function ($q) use ($lang) {
                    $q->where('id', $lang);
                })->get();
            } else {
                $this->trainers = $cours->trainers()->where('affiliate', $this->group->affiliate)->whereHas('languages', function ($q) use ($lang) {
                    $q->where('id', $lang);
                })->get();
            }
        }
    }

    public function getAllStatements()
    {
        return Excel::download(new StatementsExport($this->group), $this->group->slug.'.xlsx');
    }


//    public function save()
//    {
//        $this->validate();
//        $this->group->slug = $this->slug;
//        $this->group->year = $this->year;
//        $this->group->courses = $this->courses;
//        $this->group->offer_agreement = ($this->offer_agreement) ? $this->offer_agreement->store('/' . date('Y') . '/' . date('m') . '/' . date('d'), 'offer_agreements') : null;
//        $this->group->save();
//        $this->redirectRoute('accountant.groups.index',['year' => $this->group->year, 'course' => $this->group->courses]);
//    }

    public function render()
    {
        return view('livewire.accountant.groups.groups-edit')->layout('layouts.accountant');
    }
}
