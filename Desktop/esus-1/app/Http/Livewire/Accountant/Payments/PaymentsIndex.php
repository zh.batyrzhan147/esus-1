<?php

namespace App\Http\Livewire\Accountant\Payments;


use App\Http\Livewire\Weekend;
use App\Models\Courses;
use App\Models\Groups;
use App\Models\Orders;

use App\Models\Years;
use DateTime;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Gate;
use Livewire\Component;
use Livewire\WithFileUploads;
use Livewire\WithPagination;
use Maatwebsite\Excel\Facades\Excel;
use Ramsey\Uuid\Uuid;
use Symfony\Component\HttpFoundation\Response;
use function abort_if;
use function view;


class PaymentsIndex extends Component
{

    use WithFileUploads;
    use WithPagination;

    public $paginationCount = 20;
    public $showAddPopup = false;

    public $year;
    public $yearItem;
    public $course;

    public $customers = [];
    public $language;
    public $affiliate;
    public $search;
    public $courseItem;
    protected $queryString;

    public $traineraffiliate;

    public $start_time;
    public $end_time;



    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

        $this->course = null;
        $this->queryString = [
            'start_time' => ['except' => date('Y-m-01')],
            'end_time' => ['except' => date('Y-m-t')],
            'search',
            'year',
            //  'status',
            'course',
            'affiliate',
        ];
    }


    use \App\Traits\ValidateMessages;

    protected $rules = [
        'start_time' => 'nullable|date',
        'end_time' => 'nullable|date',
        // 'paid' => 'nullable|boolean',
        'year' => 'required|integer',
        'course' => 'required|integer',
        'affiliate' => 'required|integer',
        'trainer' => 'required|integer',
        'language' => 'required|integer',
        // 'count_all' => 'required|integer',
        // 'start_time' => 'required|date',
        // 'end_time' => 'required|date',
        // 'practice_start' => 'nullable|date',
        // 'practice_end' => 'nullable|date',
        'traineraffiliate' => 'required|integer',
        //        'status' => 'nullable|integer'
    ];

    public function mount()
    {
        abort_if(Gate::denies(\App\Models\Permission::PERMISSION_SUPERADMIN), Response::HTTP_FORBIDDEN, '403 Ошибка доступа ');
        if (!$this->start_time) $this->start_time = date('Y-m-01');
        if (!$this->end_time) $this->end_time = date('Y-m-t');

        if ($this->year) $this->yearItem = Years::where('year', $this->year)->first();
    }

    public function updatedYear()
    {
        $this->course = null;
    }


    public function render()
    {
        $orders = Orders::query();

        $orders = $this->applySearch($orders);

        return view('livewire.accountant.payments.payments-index', [
            // 'orders' => $orders->paginate($this->paginationCount),
            'orders' => $orders->paginate($this->paginationCount),
        ])->layout('layouts.accountant');
    }

    public function getTable()
    {
        $table = [];
        $orders = Orders::query();
        $orders = $this->applySearch($orders)->get();
        $i = 0;
        foreach ($orders as $order) {
            if ($order->isPayed()) {

                $i = $order->id;
                $table['orders'][$i]['pay_id'] = $order->id;
                $table['orders'][$i]['fio'] =  $order->getUserWithId($order->user_id);
                $table['orders'][$i]['oblast'] =  $order->getOblastWithId($order->user_id);
                $table['orders'][$i]['filial'] =  $order->getAffilateWithId($order->target_id);
                $table['orders'][$i]['course'] =  $order->getCourseNameWithTargetId($order->target_id);
                $table['orders'][$i]['trainer'] =  $order->getTrainerWithId($order->target_id);
                $table['orders'][$i]['slug'] =  $order->getSlugWithId($order->target_id);
                $table['orders'][$i]['date_payed'] =  date('d.m.Y  H:i:s', strtotime($order->date_payed)) ?? "Пусто";
                $table['orders'][$i]['total'] =   $order->total . " " . $order->currency;
            }
        }

        return $table;
    }



    public function updated()
    {
        if ($this->year) $this->yearItem = Years::where('year', $this->year)->first();

        // if ($this->course) {
        //     $this->courseItem = Courses::where('id', $this->course)->first();
        //     $lang = $this->language;
        //     if ($this->traineraffiliate) {
        //         $this->customers = $this->courseItem->customers()->where('affiliate', $this->traineraffiliate)->whereHas('language', function ($q) use ($lang) {
        //             $q->where('id', $lang);
        //         })->get();
        //     }
        // }
    }

    public function downloadxlsx()
    {
        return (new \App\Exports\PayDatasToXLS(self::getTable()))->download('1.xlsx', \Maatwebsite\Excel\Excel::XLSX);
    }


    public function applyFilter()
    {
        $this->resetPage();
    }

    private function applySearch(\Illuminate\Database\Eloquent\Builder $item)
    {
        $item = $item->where('payed', 1);

        if ($this->start_time !== null || $this->end_time !== null) {
            $item = $item->where(function ($q) {
                $q->where('date_payed', '>=', $this->start_time);
            });
        }

        if ($this->end_time !== null) {
            $item = $item->Where(function ($q) {
                $q->where('date_payed', '<=', $this->end_time);
            });
        }

        if ($this->search) {
            $item = $item->where('id', 'LIKE', '%' . $this->search . '%');
        }

        if ($this->affiliate) {
            $item = $item->whereIn('target_id', function ($query) {
                $query->select('id')
                    ->from('groups')
                    ->whereRaw('affiliate = ?', [$this->affiliate]);
            });
        }

        if ($this->course) {
            $item = $item->whereIn('target_id', function ($query) {
                $query->select('id')->from('groups')->whereRaw('courses = ?', [$this->course]);
            });
        }

        return $item;
    }


    protected function updatedStartTime($value)
    {
        if (!$value) {
            $this->start_time = date('Y-m-01');
        }
        if ($this->end_time) {
            if ($this->start_time > $this->end_time) {
                $this->end_time = $this->start_time;
                //                $this->emit('$refresh');
            }
        }
    }

    protected function updatedEndTime($value)
    {
        if (!$value) {
            $this->end_time = date('Y-m-t');
        }

        if ($this->start_time) {
            if ($this->start_time > $this->end_time) {
                $this->start_time = $this->end_time;
                //                $this->emit('$refresh');
            }
        }
    }
}

