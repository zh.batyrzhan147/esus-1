<?php

namespace App\Http\Livewire\Accountant\Trainers;

use App\Models\User;
use Illuminate\Support\Facades\Gate;
use Livewire\Component;
use Symfony\Component\HttpFoundation\Response;

class TrainersShow extends Component
{
    public $trainer;

    public function mount($id = null)
    {
        abort_if(Gate::denies(\App\Models\Permission::PERMISSION_OOUP), Response::HTTP_FORBIDDEN, '403 Ошибка доступа ');
        if ($id) {
            $this->trainer = User::find($id);
        } else {
            abort(404);
        }
    }

    public function render()
    {
        return view('livewire.accountant.trainers.trainers-show')->layout('layouts.accountant');
    }
}
