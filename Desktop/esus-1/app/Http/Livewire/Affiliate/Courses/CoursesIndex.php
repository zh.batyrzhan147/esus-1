<?php

namespace App\Http\Livewire\Affiliate\Courses;

use App\Models\Courses;
use Illuminate\Support\Facades\Gate;
use Livewire\Component;
use Livewire\WithPagination;
use Symfony\Component\HttpFoundation\Response;

class CoursesIndex extends Component
{

    use WithPagination;

    public $search;
    public $type;
    public $grading;

    private $courses;


    private $rules = [
        'search' => 'nullable|string',
        'type' => 'nullable|integer',
        'grading' => 'nullable|integer'
    ];
    private $affiliate;


    public function mount()
    {
        abort_if(Gate::denies(\App\Models\Permission::PERMISSION_AFFILIATE), Response::HTTP_FORBIDDEN, '403 Ошибка доступа ');
        $this->affiliate = auth()->user()->affiliate;
        if(!$this->affiliate) abort(403);
        $this->applyFilter();
    }

    public function applyFilter()
    {
        $this->courses = Courses::query();
        $this->courses = $this->applySearch($this->courses)->paginate();
    }


    public function render()
    {
//
        return view('livewire.affiliate.courses.courses-index',[
            'courses' => $this->courses,
        ])->layout('layouts.affiliate');
    }

    private function applySearch(\Illuminate\Database\Eloquent\Builder $item)
    {
        if ($this->search) {
            $search = '%' . $this->search . '%';
            $item = $item->where(function ($query) use ($search) {
                $query->where('kz_title', 'LIKE', $search)
                    ->orWhere('ru_title', 'LIKE', $search)
                ;
            });
        }

        if ($this->type !== null) {
            $item = $item->where('type', $this->type);
        }

        if ($this->grading !== null)
        {
            $item = $item->where('grading', $this->grading);
        }

        return $item;
    }
}
