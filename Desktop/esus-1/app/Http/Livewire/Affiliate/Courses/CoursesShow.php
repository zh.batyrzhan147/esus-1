<?php

namespace App\Http\Livewire\Affiliate\Courses;

use App\Models\Courses;
use Illuminate\Support\Facades\Gate;
use Livewire\Component;
use Symfony\Component\HttpFoundation\Response;

class CoursesShow extends Component
{

    public $course;

    public function mount($id = null)
    {
        abort_if(Gate::denies(\App\Models\Permission::PERMISSION_AFFILIATE), Response::HTTP_FORBIDDEN, '403 Ошибка доступа ');
        $this->affiliate = auth()->user()->affiliate;
        if(!$this->affiliate) abort(403);
        if ($id) {
            $this->course = Courses::find($id);
        } else {
            abort(404);
        }
    }

    public function render()
    {
        return view('livewire.affiliate.courses.courses-show')->layout('layouts.affiliate');
    }
}
