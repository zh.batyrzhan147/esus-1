<?php

namespace App\Http\Livewire\Affiliate\Reports;

use App\Models\Reports;
use App\Models\Years;
use Illuminate\Support\Facades\Gate;
use Livewire\Component;
use Symfony\Component\HttpFoundation\Response;

class ListenersByCourses extends Component
{

    public $ipp = 20;
    public $modal_add_new = false;
    public $reporttype;
    public $year;
    public $courses;
    public $yearItem;
    public $language;
    public $starttime;
    public $endtime;
    public $affiliate;

    public function mount()
    {
        abort_if(Gate::denies(\App\Models\Permission::PERMISSION_AFFILIATE), Response::HTTP_FORBIDDEN, '403 Ошибка доступа ');
        $this->affiliate = auth()->user()->affiliate;
        if(!$this->affiliate) abort(403);
    }

    public function render()
    {
        $reports = Reports::query();
        $reports = $this->applyFilter($reports);

        return view('livewire.affiliate.reports.listeners-by-courses',[
            'reports' => $reports->paginate($this->ipp)
        ])->layout('layouts.affiliate');
    }

    public function applyFilter($item)
    {
        $item = $item->where('affiliate', $this->affiliate);
        $item = $item->with('courseItem','languageItem');
        $item = $item->orderBy('created_at', 'DESC');
        return $item;
    }

    public function add_new()
    {
        $this->modal_add_new = true;
    }

    public function updatedYear($value)
    {
        $this->yearItem = Years::where('id', $value)->first();
        $this->courses = null;
    }

    public function recreate($id)
    {
        $report = Reports::where('id', $id)->first();
        $report->status = Reports::REPORT_STATUS_NEW;
        $report->file = null;
        $report->save();
    }


    public function add_report()
    {
//        if ($this->reporttype == Reports::REPORT_TYPE_LISTENER_BY_COURSES) {
            $report = new Reports();
            $report->affiliate = $this->affiliate;
            $report->user = auth()->user()->id;
            $report->type = $this->reporttype;
            $report->starttime = $this->starttime;
            $report->endtime = $this->endtime;
            $report->year  = $this->year;
            $report->course = $this->courses;
            $report->language = $this->language;
            $report->save();
//        }
        $this->modal_add_new = false;
    }


}
