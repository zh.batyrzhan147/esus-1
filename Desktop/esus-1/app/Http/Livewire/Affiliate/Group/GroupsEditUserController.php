<?php

namespace App\Http\Livewire\Affiliate\Group;

use App\Models\Courses;
use App\Models\Groups;
use App\Models\User;
use App\Models\Years;
use Illuminate\Support\Facades\Gate;
use Livewire\Component;
use Symfony\Component\HttpFoundation\Response;

class GroupsEditUserController extends Component
{

    public $user;
    public $oblast;
    public $raion;
    public $gorod;
    public $group;
    public $affiliate;

    use \App\Traits\ValidateMessages;
    public $rules = [
        'user.iin' => 'digits:12|required',
        'user.name' => 'string|required',
        'user.last_name' => 'string|required',
        'user.name_lat' => 'string|nullable',
        'user.last_name_lat' => 'string|nullable',
        'user.patronymic' => 'string|nullable',
        'user.phone' => 'string|nullable',
        'user.email' => 'email|required',
        'user.affiliate' => 'nullable',
        'user.auto_generation' => 'required|boolean',
        'user.gender' => 'required|integer',
        'user.institute' => 'integer|nullable',
        'user.nationality' => 'nullable|integer',
        'user.academic_category' => 'nullable|integer',
        'user.jobname' => 'nullable|integer',
        'oblast' => 'required|integer',
        'raion' => 'required|integer',
        'gorod' => 'nullable|integer',
        'user.education' => 'nullable|integer',
        'user.academic_degree' => 'nullable|integer',
//        'user.subject_teach' => 'string|nullable',
        'user.stazh' => 'required|integer',
        'user.iban' => 'string|nullable',
        'user.bank_name' => 'string|nullable',
        'user.teach_subject_id' => 'nullable|integer'
    ];

    public function updatedOblast()
    {
        $this->raion = null;
        $this->gorod = null;
    }

    public function save()
    {
        $this->validate();
        if ($this->user->affiliate == '') $this->user->affiliate = null;
        $this->user->oblast = $this->oblast;
        $this->user->raion = $this->raion;
        $this->user->gorod = $this->gorod;

        $this->user->save();

        return redirect()->route('affiliate.groups.index');

    }
    public function mount($id = null, $user = null)
    {
        abort_if(Gate::denies(\App\Models\Permission::PERMISSION_AFFILIATE), Response::HTTP_FORBIDDEN, '403 Ошибка доступа ');

        $this->affiliate = auth()->user()->affiliate;
        if(!$this->affiliate) abort(403);

        if ($id) {
            $this->group = Groups::find($id);
            if ($this->group->affiliate != $this->affiliate) abort(403);
        } else {
            abort(404);
        }

        if(!in_array($user,$this->group->users->pluck('id')->toArray())) abort(404);

        if ($user) {
            $this->user = User::where('id', $user)->first();
            if (!$this->user) {
                abort(404);
            }

            $this->oblast = $this->user->oblast;
            $this->raion = $this->user->raion;
            $this->gorod = $this->user->gorod;

        } else {
            abort(404);
        }

    }
    public function render()
    {
        return view('livewire.affiliate.group.group-users-edit-controller')->layout('layouts.affiliate');;
    }
}
