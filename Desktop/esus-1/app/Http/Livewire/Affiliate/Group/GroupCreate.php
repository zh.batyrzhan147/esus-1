<?php

namespace App\Http\Livewire\Affiliate\Group;

use Livewire\Component;

class GroupCreate extends Component
{
    public function render()
    {
        return view('livewire.affiliate.group.group-create');
    }
}
