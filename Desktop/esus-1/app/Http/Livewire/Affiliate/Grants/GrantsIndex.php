<?php

namespace App\Http\Livewire\Affiliate\Grants;

use App\Models\Courses;
use App\Models\Grants;
use App\Models\Languages;
use Illuminate\Support\Facades\Gate;
use Livewire\Component;
use Symfony\Component\HttpFoundation\Response;

class GrantsIndex extends Component
{

    public $year;
    public $programs;
    private $grants;
    public $programsid;

    protected $queryString;
    public $editableGrant;
    public $count;
    public $showModalEditgrants = false;

    public function __construct($id = null)
    {
        parent::__construct($id);
        $this->year = date('Y');
        $this->queryString = [
            'year' => ['except' => date('Y')],
        ];
    }

    use \App\Traits\ValidateMessages;
    protected $rules = [
        'year'  => 'integer',
        'count' => 'nullable|array',
        'editableGrant' => 'nullable'
    ];

    function mount()
    {
        abort_if(Gate::denies(\App\Models\Permission::PERMISSION_AFFILIATE), Response::HTTP_FORBIDDEN, '403 Ошибка доступа ');
        $this->affiliate = auth()->user()->affiliate;

        if (!$this->affiliate) abort(403);

    }

    public function updatedYear()
    {
    }

    public function render()
    {
        $grants = Grants::whereNotNull('courses')->where('affiliate',auth()->user()->affiliate)->where('year', $this->year)->with('coursesItem')->get();
        return view('livewire.affiliate.grants.grants-index',[
            'languages' => \App\Models\Languages::all(),
            'grants' => $grants,
        ])->layout('layouts.affiliate');
    }

    public function applyFilter()
    {
        $this->emit('$refresh');
    }

    public function edit(Grants $grant)
    {
        $this->resetFields();
        $this->editableGrant = $grant;
        $this->count = $this->editableGrant->countsArray;
        $this->showModalEditgrants = true;

    }

    public function resetFields()
    {
        $this->editableGrant = null;
    }

    public function updateGrant()
    {
        $this->validate();
        $this->editableGrant->counts = json_encode($this->count);
        $this->editableGrant->save();
        $this->showModalEditgrants = false;
//        $this->resetFields();
        $this->emit('saved');
    }

}
