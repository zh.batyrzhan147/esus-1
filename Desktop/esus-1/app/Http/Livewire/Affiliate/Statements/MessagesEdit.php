<?php

namespace App\Http\Livewire\Affiliate\Statements;

use App\Models\Statements;
use App\Models\StatementsMessages;
use App\Traits\ValidateMessages;
use Illuminate\Support\Facades\Gate;
use Livewire\Component;
use Livewire\WithFileUploads;
use Symfony\Component\HttpFoundation\Response;

class MessagesEdit extends Component
{

    use WithFileUploads;
    public $message;
    public $file;
    use ValidateMessages;
    private $affiliate;

    protected $rules = [
        'message.description' => 'nullable|string',
        'message.status' => 'required|integer',
        'file' => 'nullable|file|max:1000000',
    ];
    public function mount($id = null)
    {
        abort_if(Gate::denies(\App\Models\Permission::PERMISSION_AFFILIATE), Response::HTTP_FORBIDDEN, '403 Ошибка доступа ');
        if (!$id) abort(404);
        $this->message = StatementsMessages::where('id', $id)->first();
        if (!$this->message) abort(404);
        $this->affiliate = auth()->user()->affiliate;
        if (!$this->affiliate) abort(403);
        if ($this->message->groupItem->affiliate != $this->affiliate) abort(403);
    }

    public function save()
    {
        $this->validate();
        $this->message->changedby = auth()->user()->id;
        $this->message->save();
        $this->file && $this->message->update([
            'file' => $this->file->store('/messages/'.$this->message->id,'public'),
        ]);

        if ($this->message->status == StatementsMessages::STATEMENTSMESSAGE_STATUS_ACCEPT) {
            $this->message->statementItem->status = Statements::STATEMENTS_SICK;
            $this->message->statementItem->save();
        }

        if ($this->message->status == StatementsMessages::STATEMENTSMESSAGE_STATUS_DECLINE) {
            $this->message->statementItem->status = Statements::STATEMENTS_MISSING_DECLINE;
            $this->message->statementItem->save();
        }
        $this->redirectRoute('affiliate.statements.index');
    }

    public function render()
    {
        return view('livewire.affiliate.statements.messages-edit')->layout('layouts.affiliate');
    }
}
