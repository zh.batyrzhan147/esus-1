<?php

namespace App\Http\Livewire\Affiliate\Statements;

use App\Models\StatementsMessages;
use Illuminate\Support\Facades\Gate;
use Livewire\Component;
use Livewire\WithPagination;
use Symfony\Component\HttpFoundation\Response;

class MessagesIndex extends Component
{
    use WithPagination;
    private $statements;
    public $ipp = 20;
    public $search;
    private $affiliate;

    function mount()
    {
        abort_if(Gate::denies(\App\Models\Permission::PERMISSION_AFFILIATE), Response::HTTP_FORBIDDEN, '403 Ошибка доступа ');

        $this->affiliate = auth()->user()->affiliate;

        if (!$this->affiliate) abort(403);
    }

    protected  $rules = [
        'search' => 'string'
    ];

    public function applyFilter()
    {
        $this->resetPage();
    }

    public function render()
    {
        $this->statements = StatementsMessages::query();
        $this->statements = $this->applySearch($this->statements);

        return view('livewire.affiliate.statements.messages-index', [
            'statements' => $this->statements->paginate($this->ipp),
        ])->layout('layouts.affiliate');
    }

    private function applySearch(\Illuminate\Database\Eloquent\Builder $item)
    {
        if ($this->search) {
            $search = $this->search;
//            $item = $item->where('')
            $item = $item->whereHas('userItem', function ($q) use ($search) {
                $q->where('iin', 'LIKE', '%'.$search.'%');
            });
        }

        $item = $item->where('affiliate', auth()->user()->affiliate);

//        $item = $item->with('userItem', 'groupItem', 'affiliateItem', 'statementItem');
        return $item;
    }

}
