<?php

namespace App\Http\Livewire\Affiliate\Trainer;

use App\Models\Role;
use App\Models\User;
use Illuminate\Support\Facades\Gate;
use Livewire\Component;
use Symfony\Component\HttpFoundation\Response;

class TrainerCreate extends Component
{

    public $iin;
    public $trainer;
    public $new = null;

    use \App\Traits\ValidateMessages;
    protected $rules = [
        'iin' => 'required|digits:12',
        'trainer.iin' => 'digits:12|required|unique:users,iin',
        'trainer.name' => 'string|required',
        'trainer.last_name' => 'string|required',
        'trainer.email' => 'email|required|unique:users,email',
//        'trainer.affiliate' => 'nullable',
    ];

    public function mount()
    {
        abort_if(Gate::denies(\App\Models\Permission::PERMISSION_AFFILIATE), Response::HTTP_FORBIDDEN, '403 Ошибка доступа ');
        $this->affiliate = auth()->user()->affiliate;
        if(!$this->affiliate) abort(403);

    }

    public function search()
    {
        $this->validate([
            'iin' => 'required|digits:12'
        ]);
        $trainer = User::where('iin',$this->iin)->where('iin', '!=', 'admin')->first();
        if ($trainer) {
            if($trainer->roles->contains(Role::ROLE_TRAINER))
            {
                session()->flash('trainer_is_busy', 'trainer_is_busy');
            } else {
                return redirect()->route('affiliate.trainers.edit', ['id' => $trainer->id])->with('user_for_trainer_found', 'Пользователь найден, нажмите сохранить для добавления.');
            }
//            $this->trainer = $trainer;
//            $this->new = false;
        } else {
            $this->trainer = new User();
            $this->trainer->iin = $this->iin;
            $this->new = true;
        }
    }

    public function save()
    {
        $this->validate();
        $this->trainer->affiliate = auth()->user()->affiliate;
        $this->trainer->password = bcrypt(random_bytes(10));
        $this->trainer->save();
        $this->trainer->roles()->syncWithoutDetaching(Role::ROLE_TRAINER);
        $this->redirectRoute('affiliate.trainers.edit',['id' => $this->trainer->id]);
    }
    public function render()
    {
        return view('livewire.affiliate.trainer.trainer-create')->layout('layouts.affiliate');
    }
//
//    public function render()
//    {
//        return view('');
//    }
}
