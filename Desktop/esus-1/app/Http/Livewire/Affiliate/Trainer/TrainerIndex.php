<?php

namespace App\Http\Livewire\Affiliate\Trainer;

use App\Models\Role;
use App\Models\User;
use Illuminate\Support\Facades\Gate;
use Livewire\Component;
use Livewire\WithPagination;
use Symfony\Component\HttpFoundation\Response;

class TrainerIndex extends Component
{

    use WithPagination;
    public $search;
    private $affiliate;
    public $language;
    public $ipp = 20;
    private $trainers;
    protected $queryString = [
        'search' => ['except' => ''],
//        'affiliate' => ['except' => ''],
        'language' => ['except' => ''],
    ];

    use \App\Traits\ValidateMessages;
    private $rules = [
        'search' => 'nullable|string',
//        'affiliate' => 'nullable|integer',
        'language' => 'nullable|integer'
    ];


    public function mount()
    {
        abort_if(Gate::denies(\App\Models\Permission::PERMISSION_AFFILIATE), Response::HTTP_FORBIDDEN, '403 Ошибка доступа ');
        $this->affiliate = auth()->user()->affiliate;
        if(!$this->affiliate) abort(403);

    }

    public function applyFilter()
    {
        $this->resetPage();
    }

    public function render()
    {
        $this->trainers = User::query();
        $this->trainers = $this->applySearch($this->trainers);

        return view('livewire.affiliate.trainer.trainer-index', [
            'trainers' => $this->trainers->paginate($this->ipp),
        ])->layout('layouts.affiliate');

//        return view('');
    }

    private function applySearch(\Illuminate\Database\Eloquent\Builder $item)
    {
        $item = $item->whereHas('roles', function ($q) {
            $q->where('id', Role::ROLE_TRAINER);
        });
        if ($this->search) {
            $search = $this->search;
            $item = $item->where(function ($query) use ($search) {
                $searchItems = explode(" ", preg_replace('/\s+/', ' ', trim($search)));
                foreach ($searchItems as $text) {
                    $query->orWhere('iin', 'LIKE', '%' . $text . '%')
                        ->orWhere('name', 'LIKE', '%' . $text . '%')
                        ->orWhere('last_name', 'LIKE', '%' . $text . '%')
                        ->orWhere('patronymic', 'LIKE', '%' . $text . '%');
                }

            });
        }
        $item = $item->where('affiliate', auth()->user()->affiliate);

        if ($this->language) {
            $lang = $this->language;
            $item = $item->whereHas('languages',function ($q) use ($lang) {$q->where('id',$lang);});
        }
        return $item;
    }

}
