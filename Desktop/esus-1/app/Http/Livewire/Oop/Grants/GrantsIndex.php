<?php

namespace App\Http\Livewire\Oop\Grants;

use App\Models\Courses;
use App\Models\Grants;
use App\Models\Languages;
use Illuminate\Support\Facades\Gate;
use Livewire\Component;
use Symfony\Component\HttpFoundation\Response;

class GrantsIndex extends Component
{

    public $year;
    public $programs;
    public $grants;
    public $programsid;
    public $grants_all;
    public $total_all;
    public $total_ru;
    public $total_kz;
    public $grants_all_count;

    //Search elements
    public $coursesselected;
    public $query;
    public $courses;
    public $highlightIndex;

    //modal
    public $showModalEdittotalgrants = false;
    public $showModalAddCourses = false;


    protected $queryString;

    public function __construct($id = null)
    {
        parent::__construct($id);
        $this->year = date('Y');
        $this->queryString = [
            'year' => ['except' => date('Y')],
        ];
    }

    public function reset2()
    {
        $this->query = '';
        $this->courses = [];
    }

    public function updatedShowModalAddCourses() {
        $this->reset2();
    }
    public function updatedQuery()
    {
        $search = '%'.$this->query.'%';
        $this->courses = Courses::select('id','kz_title','ru_title')
            ->where(function ($query) use ($search) {
                $query->where('kz_title', 'LIKE', $search)
                    ->orWhere('ru_title', 'LIKE', $search)
                ;
            })
            ->get()
            ->toArray();
    }
//
//    public function selectContact()
//    {
//        $cours = $this->courses[$this->highlightIndex] ?? null;
//        if ($cours) {
//            dd($cours);
//            $this->coursesselected[] = $cours;
////            $this->redirect(route('show-contact', $contact['id']));
//        }
//    }
    use \App\Traits\ValidateMessages;
    protected $rules = [
        'year'  => 'integer',
        'grants_all_count' => 'required|numeric',
        'query' => ''
    ];

    function recount()
    {
        if ($this->grants_all) {
            $counts = [];
            foreach (Languages::all() as $lang) {
                $counts[$lang->slug] = array_sum($this->grants->pluck('countsArray')->pluck($lang->slug)->toArray());
            }
            $this->grants_all->counts = json_encode($counts);
            $this->grants_all->save();
        }
    }
    public function removeGrants($id)
    {
        $cours = Courses::find($id);
        $cours->years()->detach($this->year);
        $grant = Grants::firstOrNew([
            'year' => $this->year,
            'affiliate' => null,
            'courses' => $cours->id,
        ]);
        $grant->delete();
    }
    public function createGrants($id)
    {
        $cours = Courses::find($id);
        $cours->years()->syncWithoutDetaching($this->year);
        $grant = Grants::firstOrNew([
            'year' => $this->year,
            'affiliate' => null,
            'courses' => $cours->id,
        ]);
        $grant->save();

//        $grant = Grants::where('courses',$cours->id)->where('affiliate', null)->where('year', $this->year)->first();
//        if (!$grant) {
//            ;
//        }
//        $this->grants_all = Grants::where('program', null)->where('affiliate', null)->where('year', $this->year)->first();
//        $this->grants_all_count = $this->grants_all->count_all ?? 0;
//
//        $this->grants = Grants::whereNotNull('program')->whereNull('affiliate')->where('year', $this->year)->get();
//
//        $this->programs = Programs::where('year', $this->year)->get();
//        foreach ($this->programs as $program) {
//            if (!Grants::where(['year' => $this->year,'affiliate' => null,'program' => $program->id])->first()) {
//                Grants::insert([
//                    'year' => $this->year,
//                    'affiliate' => null,
//                    'program' => $program->id,
//                ]);
//            }
//        }
    }
    function mount()
    {
        abort_if(Gate::denies(\App\Models\Permission::PERMISSION_OOP), Response::HTTP_FORBIDDEN, '403 Ошибка доступа ');
    }

    public function updatedYear()
    {
//        $this->createGrants();
//        $this->recount();
    }

    public function store_grants_all_count()
    {
        $this->validate();
        if (!$this->grants_all) {
            $this->grants_all = Grants::create([
                'year' => $this->year,
                'count_all' => $this->grants_all_count
            ]);
        } else {
            $this->grants_all->update([
                'year' => $this->year,
                'count_all' => $this->grants_all_count
            ]);
        }
        $this->recount();
        $this->showModalEdittotalgrants = false;
    }

    public function render()
    {
        $this->grants_all = Grants::where('courses', null)->where('affiliate', null)->where('year', $this->year)->first();
        $this->grants = Grants::whereNotNull('courses')->whereNull('affiliate')->where('year', $this->year)->get();
        $this->recount();
//        return view('livewire.grants.grants-controller')->layout('layouts.admin');
        return view('livewire.oop.grants.grants-index')->layout('layouts.oop');
    }

    public function audit()
    {

    }

    public function applyFilter()
    {
        $this->emit('$refresh');
    }
    public function edit_store_grants_all_count()
    {
        $this->grants_all_count = $this->grants_all->count_all ?? 0;
        $this->showModalEdittotalgrants = true;
    }

    public function add_courses_modal_open()
    {
        $this->showModalAddCourses = true;
    }

}
