<?php


namespace App\Http\Livewire;


use App\Models\Weekends;

class Weekend
{
    public static function isWeekend($date)
    {
        $item = Weekends::where('day', date('Y-m-d', strtotime($date)))->first();
        if ($item) {
            if ($item->type == Weekends::WEEKENDS_HOLIDAY) return true;
            if ($item->type == Weekends::WEEKENDS_WORKING) return false;
        }

        $weekDay = date('w', strtotime($date));
        $result = ($weekDay == 0 || $weekDay == 6);
//        if (!$result) {
//
//            if (strtotime($date) < strtotime('now')) {
//                $result = true;
//            }
//        }
        return $result;

    }
}
