<?php

namespace App\Http\Livewire\Ooup\Schedule;

use App\Http\Livewire\Weekend;
use App\Models\Groups;
use Illuminate\Support\Facades\Gate;
use Livewire\Component;
use Symfony\Component\HttpFoundation\Response;

class ScheduleIndex extends Component
{

    public $rowData;

    public $start_time;
    public $end_time;
    public $trainer;
    public $showby = 'auto';
    public $affiliate;
    public $header;
    public $result;
    public $groupstatus;

    public $showEditableModal;
    public $modalGroup;

    protected $queryString;

    use \App\Traits\ValidateMessages;
    protected $rules = [
        'start_time' => 'nullable|date',
        'end_time' => 'nullable|date',
        'trainer' => 'nullable|integer',
        'affiliate' => 'nullable|integer',
    ];

    public function __construct()
    {
        parent::__construct();

        $this->queryString = [
            'start_time' => ['except' => date('Y-m-01')],
            'end_time' => ['except' => date('Y-m-t')],
            'trainer',
            'affiliate',
            'showby' => ['except' => 'auto']
        ];
    }

    public function showModal($id)
    {
        $this->modalGroup = Groups::where('id', $id)->first();
        $this->showEditableModal = true;
    }

    protected function updatedStartTime($value)
    {
        if (!$value) {
            $this->start_time = date('Y-m-01');
        }
        if ($this->end_time) {
            if ($this->start_time > $this->end_time) {
                $this->end_time = $this->start_time;
//                $this->emit('$refresh');
            }
        }
    }

    protected function updatedEndTime($value)
    {
        if (!$value) {
            $this->end_time = date('Y-m-t');
        }

        if ($this->start_time) {
            if ($this->start_time > $this->end_time) {
                $this->start_time = $this->end_time;
//                $this->emit('$refresh');
            }
        }
    }


    public function mount()
    {
        abort_if(Gate::denies(\App\Models\Permission::PERMISSION_OOUP), Response::HTTP_FORBIDDEN, '403 Ошибка доступа ');

        if (!$this->start_time) $this->start_time = date('Y-m-01');
        if (!$this->end_time) $this->end_time = date('Y-m-t');
        $data = [];
        $this->gantt();
    }

    public function gantt()
    {
        if ($this->showby == 'byweek') {
            $byDay = false;
        } elseif ($this->showby == 'byday') {
            $byDay = true;
        } else {
            $byDay = (floor((strtotime($this->end_time) - strtotime($this->start_time)) / (60 * 60 * 24)) > 60) ? false : true;
        }
        $this->header = [];
        $start = $this->start_time;
        $i = 1;
        while ($start <= $this->end_time) {
            if ($byDay) {
                if (!Weekend::isWeekend($start)) {
                    $this->header[$i]['day'] = $start;
                    $this->header[$i]['title'] = date('d.m', strtotime($start));
                    $i++;
                }
                $start = date('Y-m-d', strtotime($start . '+1 day'));
            } else {
                $w = date('W', strtotime($start));
                $this->header[$i]['day'] = [];
                while ($w == date('W', strtotime($start))) {
                    if (!Weekend::isWeekend($start)) {
                        $this->header[$i]['day'][] = $start;
                    }
                    $start = date('Y-m-d', strtotime($start . '+1 day'));
                }
//                $this->>header[$i]['day'] = $start;
                $this->header[$i]['title'] = date('d.m', strtotime(reset($this->header[$i]['day']) ?? '')) . '-' . date('d.m', strtotime(end($this->header[$i]['day']) ?? ''));
                $i++;
            }
        }
        $groups = Groups::query();
        $groups = $this->applySearch($groups)->get();

        $this->result = [];
        foreach ($groups as $group) {
            $start = 1;
            $end = count($this->header) + 1;
            $this->result[$group->trainer]['trainer'] = $group->trainerItem->fio;
            $this->result[$group->trainer]['hours_count'] = $group->trainerItem->hours_count;
            foreach ($this->header as $key => $item) {
                if ($byDay) {
                    if ($item['day'] == $group->start_time) {
                        $start = $key;
                    }
                    if ($item['day'] == $group->end_time) {
                        $end = $key + 1;
                    }
                } else {
                    if (in_array($group->start_time, $item['day'])) {
                        $start = $key;
                    }
                    if (in_array($group->end_time, $item['day'])) {
                        $end = $key + 1;
                    }
                }
            }

            $this->result[$group->trainer]['groups'][$group->id]['time'] = $start . '/' . $end;
            $this->result[$group->trainer]['groups'][$group->id]['slug'] = $group->slug;
            $this->result[$group->trainer]['groups'][$group->id]['start'] = ($group->start_time < $this->start_time);
            $this->result[$group->trainer]['groups'][$group->id]['end'] = ($group->end_time > $this->end_time);
            $this->result[$group->trainer]['groups'][$group->id]['color'] = $this->getColor($group->courses);
        }
    }

    function getColor($num)
    {
        $hash = md5('esus' . $num); // modify 'color' to get a different palette
        $r = hexdec(substr($hash, 0, 2));
        $g = hexdec(substr($hash, 2, 2));
        $b = hexdec(substr($hash, 4, 2));
        $luma = (($r * 299) + ($g * 587) + ($b * 114)) / 1000;
        return array(
            hexdec(substr($hash, 0, 2)), // r
            hexdec(substr($hash, 2, 2)), // g
            hexdec(substr($hash, 4, 2)),
            $luma); //b
    }

    public function applyFilter()
    {
        $this->gantt();
    }


    public function render()
    {
        return view('livewire.ooup.schedule.schedule-index', [
//            'groups' => $groups,
            'header' => $this->header,
            'results' => $this->result,
        ])->layout('layouts.ooup');
    }

    private function applySearch(\Illuminate\Database\Eloquent\Builder $item)
    {
        if ($this->groupstatus) {
            $item = $item->where('status',$this->groupstatus);
        }
//        $item = ($this->type) ? $item->where('type', $this->type) : $item;
        if ($this->start_time !== null) {
            $item = $item->where(function ($q) {
                $q->where('start_time', '>=', $this->start_time)
                    ->orWhere('end_time', '>=', $this->start_time);
            });
        }

        if ($this->end_time !== null) {
            $item = $item->Where(function ($q) {
                $q->where('start_time', '<=', $this->end_time)
                    ->orWhere('end_time', '<=', $this->end_time);
            });
        }

        if ($this->affiliate) {
            $item = $item->where('affiliate', $this->affiliate);
        }

        $item = $item->orderBy('trainer', 'ASC')->orderBy('start_time', 'ASC');
        return $item;
    }
}
