<?php

namespace App\Http\Livewire\Ooup\Grants;

use App\Models\Affiliates;
use App\Models\CourseDates;
use App\Models\Grants;
use App\Models\Languages;
use Illuminate\Support\Facades\Gate;
use Livewire\Component;
use Symfony\Component\HttpFoundation\Response;

class GrantsEdit extends Component
{


    public $year;
    public $affilates;
    public $grant;
    public $grants;
    public $showModalEditgrants = false;
    public $editableGrant = null;
    public $count;
    public $start;
    public $end;
    use \App\Traits\ValidateMessages;
    protected $rules = [
        'editableGrant.count_all' => 'integer|required',
        'grant.count_all' => 'integer|required',
        'year' => 'integer',
        'count' => 'nullable|array',
        'start' => 'date|nullable',
        'end' => 'date|nullable',
    ];

    function mount(Grants $grant)
    {
        abort_if(Gate::denies(\App\Models\Permission::PERMISSION_OOUP), Response::HTTP_FORBIDDEN, '403 Ошибка доступа ');
        $this->grant = $grant;
        $this->year = $this->grant->year;
        $this->affilates = Affiliates::all();
        foreach ($this->affilates as $affilate) {
//            if (!Grants::where(['year' => $this->year, 'affiliate' => $affilate->id, 'courses' => $this->grant->coursesItem->id])->first()) {
//                Grants::insert([
//                    'year' => $this->year,
//                    'affiliate' => $affilate->id,
//                    'courses' => $this->grant->coursesItem->id,
//                ]);
//            }
            $grant = Grants::firstOrNew([
                'year' => $this->year,
                'affiliate' => $affilate->id,
                'courses' => $this->grant->coursesItem->id,
            ]);
            $grant->save();

        }

        $courseDate = CourseDates::firstOrCreate(
            [
                'courses' => $this->grant->coursesItem->id,
                'year' => $this->year,
            ]
        );

        if ($courseDate) {
            $this->start = $courseDate->start;
            $this->end = $courseDate->end;
        }
    }

    public function change()
    {

    }

    protected function recount()
    {
        $this->grants = Grants::whereNotNull('affiliate')->where('courses', $this->grant->coursesItem->id)->where('year', $this->year)->get();
        $counts = [];
        foreach (Languages::all() as $lang) {
            $counts[$lang->slug] = array_sum($this->grants->pluck('countsArray')->pluck($lang->slug)->toArray());
        }
        $this->grant->counts = json_encode($counts);
        $this->grant->save();
    }

    public function save()
    {
        CourseDates::where(
            [
                'courses' => $this->grant->coursesItem->id,
                'year' => $this->year,
            ]
        )->update(
            [
                'start' => $this->start,
                'end' => $this->end,
            ]
        );
        $this->emit('saved');
    }
//    public function updatedStart($value)
//    {
////        $this->validate();
//
//
//    }

//    public function updatedEnd($value)
//    {
////        $this->validate();
//
//
//    }

    public function edit(Grants $grant)
    {
        $this->resetFields();
        $this->editableGrant = $grant;
//        $this->rangekz = (int)($this->editableGrant->count_all/2);
        $this->count = $this->editableGrant->countsArray;
        $this->showModalEditgrants = true;

    }

    public function update()
    {
        $this->validate();
        $this->editableGrant->counts = json_encode($this->count);
        $this->editableGrant->save();

        $this->showModalEditgrants = false;
        $this->resetFields();
        $this->emit('saved');
    }

    public function updateAll()
    {
        $this->validate();

        $this->grant->save();

        $this->showModalEditgrants = false;

        $this->resetFields();
        $this->emit('saved');
    }

    public function resetFields()
    {
        $this->editableGrant = null;
//        $this->count = null;
    }

    public function render()
    {
//        $this->grant = Grants::find($this->grant->id);
//        $this->grants = Grants::whereNotNull('affiliate')->where('courses', $this->grant->coursesItem->id)->where('year', $this->year)->get();
        $this->recount();
        return view('livewire.ooup.grants.grants-edit')->layout('layouts.ooup');
//        return view('livewire.grants.grant-edit-controller')->layout('layouts.admin');
    }
}
