<?php

namespace App\Http\Livewire\Ooup\Certificates;

use App\Models\Certificates;
use App\Traits\ValidateMessages;
use Illuminate\Support\Facades\Gate;
use Livewire\Component;
use Livewire\WithPagination;
use Symfony\Component\HttpFoundation\Response;

class CertificatesList extends Component
{
    use WithPagination;
    public $ipp = 20;
    public $type;
    public $group;
    public $uuid;
    public $fio;

    use ValidateMessages;
    protected $queryString = [
        'type' => ['except' => ''],
        'group' => ['except' => ''],
        'fio' => ['except' => ''],
        'uuid' => ['except' => ''],
    ];

    public function rules()
    {
        return [
            'type' => 'nullable|integer',
            'group' => 'nullable|string',
            'fio' => 'nullable|string',
            'uuid' => 'nullable|string',
        ];
    }
    public function applyFilter()
    {
        $this->resetPage();
    }


    public function mount()
    {
        abort_if(Gate::denies(\App\Models\Permission::PERMISSION_OOUP), Response::HTTP_FORBIDDEN, '403 Ошибка доступа ');
    }
    public function render()
    {
        $item = Certificates::query();
        $item = $this->applySearch($item);
        return view('livewire.ooup.certificates.certificates-list',[
            'certificates' => $item->paginate($this->ipp),
        ])->layout('layouts.ooup');
    }

    public function destroy($id)
    {
        Certificates::find($id)->delete();
    }

    private function applySearch(\Illuminate\Database\Eloquent\Builder $item)
    {
        if ($this->type) $item = $item->where('type', $this->type);
        if ($this->group) $item = $item->where('group_title','LIKE', '%' . $this->group . '%');
        if ($this->fio) $item = $item->where('fio','LIKE', '%' . $this->fio . '%');
        if ($this->uuid) $item = $item->where('uuid','LIKE', '%' . $this->uuid . '%');
        return $item;
    }
}
