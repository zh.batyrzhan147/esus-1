<?php

namespace App\Http\Livewire\Ooup\Certificates;

use App\Models\Certificates;
use Illuminate\Support\Facades\Gate;
use Livewire\Component;
use Symfony\Component\HttpFoundation\Response;

class CertificatesEdit extends Component
{
    public $certificate;
    public function mount($id = null)
    {
        abort_if(Gate::denies(\App\Models\Permission::PERMISSION_OOUP), Response::HTTP_FORBIDDEN, '403 Ошибка доступа ');
        $this->certificate = Certificates::where('id',$id)->first();
        if (!$this->certificate) abort(404);

    }

    public function render()
    {
        return view('livewire.ooup.certificates.certificates-edit')->layout('layouts.ooup');
    }
}
