<?php

namespace App\Http\Livewire\Ooup\Certificates;

use App\Models\CertificatesTemplates;
use App\Models\Years;
use Illuminate\Support\Facades\Gate;
use Livewire\Component;
use Livewire\WithFileUploads;
use Symfony\Component\HttpFoundation\Response;

class CertificatesTemplatesEdit extends Component
{
    use WithFileUploads;
    public $template;
    public $background;
    public $year;
    public $yearItem;
    public $course;
    use \App\Traits\ValidateMessages;
    public function rules()
    {
        if ($this->template->id) {
            return [
                'template.active' => 'required|boolean',
                'template.title' => 'required|string',
                'year' => 'required|integer',
                'background' => 'nullable|file',
                'course' => 'required|integer',
                'template.type' => 'required|integer',
            ];
        } else {
            return [
                'template.active' => 'required|boolean',
                'template.title' => 'required|string',
                'year' => 'required|integer',
                'background' => 'required|file',
                'course' => 'required|integer',
                'template.type' => 'required|integer',
            ];
        }
    }



    public function mount($id = null)
    {
        abort_if(Gate::denies(\App\Models\Permission::PERMISSION_OOUP), Response::HTTP_FORBIDDEN, '403 Ошибка доступа ');
        if ($id) {
            $this->template = CertificatesTemplates::where('id', $id)->first();
            if (!$this->template) abort(404);
            $this->year = $this->template->year;
            if ($this->year)  $this->yearItem = Years::where('id', $this->year)->first();
            if ($this->year) $this->course = $this->template->course;
        } else {
            $this->template = new CertificatesTemplates();
        }
    }

    public function updatedYear($value)
    {
        $this->yearItem = Years::where('id', $value)->first();
        $this->courses = null;
    }

    public function save()
    {
        $this->validate();
        $this->template->year = $this->year;
        $this->template->course = $this->course;
        if ($this->background) $this->template->background = ($this->background) ? $this->background->store('/' . date('Y') . '/' . date('m') . '/' . date('d'), 'certificate-templates') : null;
        $this->template->save();
        $this->redirectRoute('ooup.certificates-templates.index');
    }



    public function render()
    {
        return view('livewire.ooup.certificates.certificates-templates-edit')->layout('layouts.ooup');
    }
}
