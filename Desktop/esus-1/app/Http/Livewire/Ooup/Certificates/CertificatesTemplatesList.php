<?php

namespace App\Http\Livewire\Ooup\Certificates;

use App\Models\Certificates;
use App\Models\CertificatesTemplates;
use App\Traits\ValidateMessages;
use Barryvdh\DomPDF\Facade\Pdf;
use Barryvdh\Snappy\Facades\SnappyPdf;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Storage;
use Livewire\Component;
use Livewire\WithPagination;
use Ramsey\Uuid\Uuid;
use Symfony\Component\HttpFoundation\Response;

class CertificatesTemplatesList extends Component
{
    use WithPagination;
    public $ipp = 20;
    public $type;

    use ValidateMessages;

    protected $queryString = [
        'type' => [
            'except' => ''
        ]
    ];
    public function rules()
    {
        return [
            'type' => 'nullable|integer',
        ];
    }
    public function applyFilter()
    {
        $this->resetPage();
    }

    public function mount()
    {
        abort_if(Gate::denies(\App\Models\Permission::PERMISSION_OOUP), Response::HTTP_FORBIDDEN, '403 Ошибка доступа ');
    }

    public function runtest($id = null)
    {
        if(!$id) return;
        $template = CertificatesTemplates::where('id',$id)->first();
        if ($template->type == CertificatesTemplates::CERTIFICATES_TEMPLATES_TYPE_CERTIFICATE) {
            $data = [
                'title' => 'Қожамжаров Әлқожағұл Ұлықбекұлыныңұлы',
                'date' => date('m/d/Y'),
                'background' => $template->backgroundUrl,
                'url' => 'url',
                'uuid' => 'ASDAsdsA' . date('y')
            ];
            $file = SnappyPdf::loadView('certificates.certificates', $data, [], 'UTF-8')->setPaper('a4', 'landscape');
            $path = storage_path('app/public') . date('/Y/m/d', strtotime('now'));
            Storage::makeDirectory('public' . date('/Y/m/d', strtotime('now')));
            $uuid4 = Uuid::uuid4();
            $file_name = $uuid4 . '.' . 'pdf';
            $file->save($path . '/' . $file_name);
            return response()->download($path . '/' . $file_name);
        } elseif ($template->type == CertificatesTemplates::CERTIFICATES_TEMPLATES_TYPE_REFERENCE) {
            $data = [
                'title' => 'Қожамжаров Әлқожағұл Ұлықбекұлыныңұлы',
                'date' => date('d.m.Y') .'-'.date('d.m.Y'),
                'background' => $template->backgroundUrl,
                'url' => 'url',
                'uuid' => 'ASDAsdsA' . date('y')
            ];
            $file = SnappyPdf::loadView('certificates.information', $data, [], 'UTF-8')->setPaper('a4', 'landscape');
            $path = storage_path('app/public') . date('/Y/m/d', strtotime('now'));
            Storage::makeDirectory('public' . date('/Y/m/d', strtotime('now')));
            $uuid4 = Uuid::uuid4();
            $file_name = $uuid4 . '.' . 'pdf';
            $file->save($path . '/' . $file_name);
            return response()->download($path . '/' . $file_name);
        }
    }

    public function render()
    {
        $item = CertificatesTemplates::query();
        $item = $this->applySearch($item);
        return view('livewire.ooup.certificates.certificates-templates-list',[
            'templates' => $item->paginate($this->ipp),
        ])->layout('layouts.ooup');
    }

    public function destroy($id)
    {
        CertificatesTemplates::find($id)->delete();
    }

    private function applySearch(\Illuminate\Database\Eloquent\Builder $item)
    {
        $item = $item->orderBy('id', 'DESC');
        if ($this->type) $item = $item->where('type', $this->type);
        return $item;
    }

}
