<?php

namespace App\Http\Livewire\Ooup\Courses;

use App\Models\Courses;
use Illuminate\Support\Facades\Gate;
use Livewire\Component;
use Symfony\Component\HttpFoundation\Response;

class CoursesShow extends Component
{
    public $course;

    public function mount($id = null)
    {
        abort_if(Gate::denies(\App\Models\Permission::PERMISSION_OOUP), Response::HTTP_FORBIDDEN, '403 Ошибка доступа ');
        if ($id) {
            $this->course = Courses::find($id);
        } else {
            abort(404);
        }
    }

    public function render()
    {
        return view('livewire.ooup.courses.courses-show')->layout('layouts.ooup');
    }
}
