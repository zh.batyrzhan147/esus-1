<?php

namespace App\Http\Livewire\Ooup\Courses;

use App\Models\Courses;
use Illuminate\Support\Facades\Gate;
use Livewire\Component;
use Livewire\WithPagination;
use Symfony\Component\HttpFoundation\Response;

class CoursesIndex extends Component
{
    use WithPagination;

    public $search;
    public $type = '';
    public $grading = '';

    private $courses;

    protected $queryString = [
        'search' => ['except' => ''],
        'grading' => ['except' => ''],
        'type' => ['except' => ''],

    ];
    use \App\Traits\ValidateMessages;
    private $rules = [
        'search' => 'nullable|string',
        'type' => 'nullable|integer',
        'grading' => 'nullable|integer'
    ];


    public function mount()
    {
        abort_if(Gate::denies(\App\Models\Permission::PERMISSION_OOUP), Response::HTTP_FORBIDDEN, '403 Ошибка доступа ');
//        $this->applyFilter();
    }

    public function applyFilter()
    {
        $this->resetPage();
    }


    public function render()
    {
        $this->courses = Courses::query();
        $this->courses = $this->applySearch($this->courses)->paginate();
        return view('livewire.ooup.courses.courses-index',[
            'courses' => $this->courses,
        ])->layout('layouts.ooup');
    }

    private function applySearch(\Illuminate\Database\Eloquent\Builder $item)
    {
        if ($this->search) {
            $search = '%' . $this->search . '%';
            $item = $item->where(function ($query) use ($search) {
                $query->where('kz_title', 'LIKE', $search)
                    ->orWhere('ru_title', 'LIKE', $search)
                ;
            });
        }

        if ($this->type != '') {
            $item = $item->where('type', $this->type);
        }

        if ($this->grading != '')
        {
            $item = $item->where('grading', $this->grading);
        }

        return $item;
    }
}
