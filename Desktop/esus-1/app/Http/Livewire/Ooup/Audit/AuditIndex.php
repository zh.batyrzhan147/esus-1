<?php

namespace App\Http\Livewire\Ooup\Audit;

use App\Models\AuditReports;
use App\Models\Years;
use App\Traits\ValidateMessages;
use Illuminate\Support\Facades\Gate;
use Livewire\Component;
use Symfony\Component\HttpFoundation\Response;

class AuditIndex extends Component
{
    public $ipp = 20;
    public $modal_add_new = false;
    public $reporttype;
    public $year;

    public function mount()
    {
        abort_if(Gate::denies(\App\Models\Permission::PERMISSION_OOUP), Response::HTTP_FORBIDDEN, '403 Ошибка доступа ');
    }

    public function render()
    {
        $audits = AuditReports::query();
        $audits = $this->applyFilter($audits);
        return view('livewire.ooup.audit.audit-index',[
            'audits' => $audits->paginate($this->ipp),
        ])->layout('layouts.ooup');
    }

    private function applyFilter(\Illuminate\Database\Eloquent\Builder $item)
    {
        $item = $item->orderBy('created_at','DESC');
        $item = $item->with('userItem');
        return $item;
    }

    public function destroy($id)
    {
        AuditReports::find($id)->delete();
    }



    public function add_new()
    {
        $this->modal_add_new = true;
    }
    use ValidateMessages;
    public function add_report()
    {
        $this->validate([
            'reporttype' => 'required|integer',
            'year' => 'required|integer',
        ]);
        $auditReport = new AuditReports();
        $auditReport->year = $this->year;
        $auditReport->type = $this->reporttype;
        $auditReport->user = auth()->user()->id;
        $auditReport->save();
        $this->modal_add_new = false;
    }
}
