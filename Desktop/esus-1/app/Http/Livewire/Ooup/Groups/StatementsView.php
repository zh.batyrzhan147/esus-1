<?php

namespace App\Http\Livewire\Ooup\Groups;

use App\Exports\StatementsExport;
use App\Http\Livewire\Weekend;
use App\Models\Groups;
use App\Models\Statements;
use DateTime;
use Livewire\Component;

class StatementsView extends Component
{

    public $slug;
    public $group;

    public $dates = [];
    public $firstDate;
    public $data = [];
    public $weeks = [];

    public function changeWeek($date)
    {
        $this->firstDate = $date;
    }

    public function changeStatement($userId, $date, $status)
    {
        Statements::updateOrCreate(
            [
                'group' => $this->group->id,
                'user' => $userId,
                'available_at' => date('Y-m-d', strtotime($date)),
            ],
            ['status' => $status]
        );
    }

    public function mount($slug = null)
    {
        if ($slug) {
            $this->slug = $slug;
        } else {
            abort(404);
        }
        $this->group = Groups::where('id', $this->slug)->first();
        if (!$this->group) abort(404);
        $this->firstDate = $this->group->start_time;
        $f = new DateTime($this->firstDate);
        if($f->format('w') !== '1')
            $f->modify('previous monday');
        while (strtotime($f->format('Y-m-d')) <= strtotime($this->group->end_time)) {
            $this->weeks[$f->format('Y-m-d')] = $f->format('d.m').'-'.$f->modify('next sunday')->format('d.m');
            $f->modify('next monday');
        }
    }

    public function render()
    {

        $firstDay = new DateTime($this->firstDate);
//        $firstDay = new DateTime('now');
        if($firstDay->format('w') !== '1')
            $firstDay->modify('previous monday');

        $currentweekday = $firstDay->format("w");
        $this->dates = [];
        if ($currentweekday == 0) {
            $currentweekday = 7;
        }
        for ($i = 1; $i< 8; $i++) {
            if ($currentweekday == $i) {
                $this->dates[$i] = $firstDay->format("Y-m-d");
            } else {
                if ($i < $currentweekday) {
                    $this->dates[$i] = date("Y-m-d",strtotime('-'. intval($currentweekday - $i).' day '.$firstDay->format('Y-m-d')));
                } else {
                    $this->dates[$i] = date("Y-m-d",strtotime('+'. intval($i - $currentweekday).' day '.$firstDay->format('Y-m-d')));
                }
            }
        }
        $this->data = [];
        $statements = Statements::where('group',$this->group->id)->whereIn('available_at',$this->dates)->with('userItem')->get();
        foreach ($statements as $statement)
        {
            $this->data[$statement->user]['user'] = $statement->userItem->fio;
            $this->data[$statement->user]['items'][$statement->available_at] = $statement->status;
        }
//        $users = $this->group->users;
//        $usersArray = [];
//        foreach ($this->group->users as $key => $user) {
//            $usersArray[$key]['user'] = $user->toArray();
//            $usersArray[$key]['stat'] = Statements::where('group',$this->group->id)->where('user',$user->id)->whereIn('available_at',$this->dates)->get()->pluck('status','available_at')->toArray();
//        }
//        dd($users->toArray());
//
//        dd($usersArray);

//        dd($firstDay);
//        while (Weekend::isWeekend($firstDay->format('Y-m-d'))) {
//            $firstDay->modify('+1 day');
//        }


        return view('livewire.ooup.groups.statements')->layout('layouts.ooup');
    }
}
