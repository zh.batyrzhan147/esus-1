<?php

namespace App\Http\Livewire\Ooup\Groups;

use App\Jobs\GenerateCertificate;
use App\Models\Certificates;
use App\Models\Groups;
use App\Models\User;
use Hashids\Hashids;
use Livewire\Component;
use Ramsey\Uuid\Uuid;

class GroupsCertificates extends Component
{
    public $group;


    public $selectAll = false;
    public $selected = [];
    public array $recommended;

    public function resetSelected()
    {
        $this->reset('selected');
    }

    public function updatedSelected()
    {
        $this->selectAll = false;
    }

    public function updatedSelectAll()
    {
        if ($this->selectAll) {
            $this->selected = $this->group->users->pluck('id')->toArray();
        } else {
            $this->selected = [];
        }
    }

    public function issueSelected()
    {
        foreach ($this->selected?? [] as $item) {
            $this->issue($item);
        }
        $this->selectAll = false;
        $this->resetSelected();
    }


    public function mount($id = null)
    {
        $this->group = Groups::where('id', $id)->first();
        $this->recommended = $this->group->users_sb->pluck('id')->toArray();
        if (!$this->group) abort(404);
    }

    public function issue($id)
    {
        if ($this->group->status == Groups::GROUP_STATUS_FINISHED or $this->group->status == Groups::GROUP_STATUS_FINISHED_TESTING) {
            $cert = Certificates::where('user', $id)->where('group_id',$this->group->id)->where('type',Certificates::CERTIFICATES_TYPE_CERTIFICATE)->first();
            if (!$cert) {
                $user = User::where('id', $id)->first();
                $uuid4 = Uuid::uuid4();
                $hashids = new Hashids($uuid4->toString());
                $uuid = $hashids->encode(1,2,3,4).date('y');

                if ($this->group->coursesItem->certificateTemplate()->where('type',\App\Models\CertificatesTemplates::CERTIFICATES_TEMPLATES_TYPE_CERTIFICATE)->first()) {
                    $c = new Certificates([
                            'fio' => $user->fio,
                            'group_title' => $this->group->slug,
                            'group_id' => $this->group->id,
                            'course_title' => $this->group->coursesItem->kz_title,
                            'course_id' => $this->group->coursesItem->id,
                            'year' => $this->group->year,
                            'description' => '',
                            'user' => $user->id,
                            'template' => $this->group->coursesItem->certificateTemplate()->where('type', \App\Models\CertificatesTemplates::CERTIFICATES_TEMPLATES_TYPE_CERTIFICATE)->first()->id,
                            'uuid' => $uuid,
                            'type' => Certificates::CERTIFICATES_TYPE_CERTIFICATE,
                            'cert_day' => $this->group->cert_day ?? null,
                        ]
                    );
                    $c->save();
                }
            }
        }
    }
    public function changeGroupStatus($status)
    {
        if ($status == Groups::GROUP_STATUS_FINISHED or $status == Groups::GROUP_STATUS_FINISHED_TESTING) {
            $this->group->status = Groups::GROUP_STATUS_CERTIFICATED;
            $this->group->save();
        }
    }

    public function destroy($id)
    {
        Certificates::find($id)->delete();
        $this->emit('SuccessDeleted');
    }

    public function destroyAllCertificates()
    {
        Certificates::where('group_id',$this->group->id)->delete();
        $this->emit('SuccessDeleted');
    }
    public function downloadArchive()
    {
        $public_dir=public_path();
        $zip_file = $this->group->slug.'.zip'; // Name of our archive to download

        $zip = new \ZipArchive();
        if ($zip->open($public_dir . '/storage/'.date('Y/m/d').'/' . $zip_file, \ZipArchive::CREATE| \ZipArchive::OVERWRITE) === TRUE) {
            $certs = Certificates::where('group_id', $this->group->id)->where('type',Certificates::CERTIFICATES_TYPE_CERTIFICATE)->get();
            foreach ($certs as $cert) {
                $zip->addFile($public_dir.(str_replace(url('/'),'',$cert->pdf)), $cert->fio.'.pdf');
            }
            $zip->close();
        }
        $headers = array(
            'Content-Type' => 'application/octet-stream',
        );
        $filetopath=$public_dir.'/storage/'.date('Y/m/d').'/' . $zip_file;

        if(file_exists($filetopath)){
            return response()->download($filetopath,$zip_file,$headers);
        }
    }

    public function render()
    {
        $certs = Certificates::where('group_id', $this->group->id)->where('type',Certificates::CERTIFICATES_TYPE_CERTIFICATE)->orderby('fio','ASC')->get();
        return view('livewire.ooup.groups.groups-certificates', [
            'certs' => $certs,
        ])->layout('layouts.ooup');
    }
}
