<?php

namespace App\Http\Livewire\Omr\Groups;

use App\Models\Courses;
use App\Models\Groups;
use App\Models\Years;
use Illuminate\Support\Facades\Gate;
use Livewire\Component;
use Livewire\WithFileUploads;
use Ramsey\Uuid\Uuid;
use Symfony\Component\HttpFoundation\Response;
use function abort_if;
use function view;

class GroupsEdit extends Component
{
    use WithFileUploads;
    public $group;
    public $year;
    public $yearItem;
    public $trainers = [];
    public $courses;
    public $offer_agreement;
    public $slug;
    public $traineraffiliate;
    public $webinar_length;
    public $webinar_count;

    use \App\Traits\ValidateMessages;
    public function rules()
    {
        return [
            'slug' => 'required|string|unique:groups,slug,' . $this->group->id,
            'group.language' => 'required|integer',
            'group.affiliate' => 'integer|nullable',
            'year' => 'required|integer',
            'offer_agreement' => 'nullable|file|max:1000000',
            'courses' => 'required|integer',
            'group.trainer' => 'required|integer',
            'group.status' => 'required|integer',
            'group.count_all' => 'required|integer',
            'group.start_time' => 'required|date',
            'group.end_time' => 'required|date',
            'group.practice_start' => 'nullable|date',
            'group.practice_end' => 'nullable|date',
            'traineraffiliate' => 'required|integer',
            'group.zoom_link' => 'nullable|string',
            'group.cert_day' => 'nullable|date',
            'group.change_statements' => 'nullable|boolean',
            'webinar_length' => 'nullable|integer',
            'webinar_count' => 'nullable|integer',
        ];
    }

//        protected $rules = ;

    public function mount($id = null)
    {
        abort_if(Gate::denies(\App\Models\Permission::PERMISSION_OMR), Response::HTTP_FORBIDDEN, '403 Ошибка доступа ');

        if ($id) {
            $this->group = Groups::find($id);
            if ($this->group->trainer) {
                $this->traineraffiliate = $this->group->trainerItem->affiliate;
            }
            $this->slug = $this->group->slug;
            $this->year = $this->group->year;
            $this->group->slug = $this->slug;
            if ($this->year) $this->yearItem = Years::where('id', $this->year)->first();
            if ($this->year) $this->courses = $this->group->courses;
            if ($this->courses) {
                $cours = Courses::where('id', $this->courses)->first();
                $lang = $this->group->language;
                if ($this->traineraffiliate) {
                    $this->trainers = $cours->trainers()->where('affiliate', $this->traineraffiliate)->whereHas('languages', function ($q) use ($lang) {
                        $q->where('id', $lang);
                    })->get();
                }
                $this->webinar_length = Courses::where('id', $this->courses)->first()->webinar_length ?? 0;
                $this->webinar_count = Courses::where('id', $this->courses)->first()->webinar_count ?? 0;
            }
        } else {
            $this->group = new Groups();
        }
    }

    public function updatedYear($value)
    {
        $this->yearItem = Years::where('id', $value)->first();
        $this->courses = null;
        $this->group->trainer = null;
    }


    public function updated()
    {
        if ($this->courses) {
            $cours = Courses::where('id', $this->courses)->first();
            $lang = $this->group->language;
            if ($this->traineraffiliate) {
                $this->trainers = $cours->trainers()->where('affiliate', $this->traineraffiliate)->whereHas('languages', function ($q) use ($lang) {
                    $q->where('id', $lang);
                })->get();
            }
        }
    }

    public function save()
    {
        $this->validate();
        $this->group->slug = $this->slug;
        $this->group->year = $this->year;
        $this->group->courses = $this->courses;
        $this->group->offer_agreement = ($this->offer_agreement) ? $this->offer_agreement->store('/' . date('Y') . '/' . date('m') . '/' . date('d'), 'public') : null;
        $this->group->save();
        $this->redirectRoute('omr.groups.index',['year' => $this->group->year, 'course' => $this->group->courses]);
    }

    public function downloadcvs()
    {
        return (new \App\Exports\GroupUsersToSDO($this->group))->download($this->group->slug.'.csv', \Maatwebsite\Excel\Excel::CSV, [
            'Content-Type' => 'text/csv',
        ]);
    }

    public function downloadxlsx()
    {
        return (new \App\Exports\GroupUsersToXLS($this->group))->download($this->group->slug.'.xlsx', \Maatwebsite\Excel\Excel::XLSX);
    }

    public function removeListener($id)
    {
        $this->group->users()->detach($id);
        $this->group->refresh();
        $this->emit('successDelete');
    }


    public function render()
    {
        return view('livewire.omr.groups.groups-edit')->layout('layouts.omr');
    }
}
