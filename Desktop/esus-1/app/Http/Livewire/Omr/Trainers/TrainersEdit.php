<?php

namespace App\Http\Livewire\Omr\Trainers;

use App\Models\Courses;
use App\Models\Role;
use App\Models\User;
use Illuminate\Support\Facades\Gate;
use Livewire\Component;
use Symfony\Component\HttpFoundation\Response;

class TrainersEdit extends Component
{
    public $trainer;
    public $courses;
    public $showModalAddCourses = false;
    public $query;
    public $languages = [];
    public $oblast;
    public $raion;
    public $gorod;
    public $stazh;
    public $new = false;

    public $trainer_awards;
    public $trainer_theses;
    public $trainer_certificates;


    public function updatedOblast()
    {
        $this->raion = null;
        $this->gorod = null;
    }
    use \App\Traits\ValidateMessages;
    protected $rules = [
        'trainer.iin' => 'digits:12|required',
        'trainer.name' => 'string|required',
        'trainer.last_name' => 'string|required',
        'trainer.patronymic' => 'string|nullable',
        'trainer.phone' => 'string|nullable',
        'trainer.email' => 'email|required',
        'trainer.affiliate' => 'nullable',
        'query' => '',
        'languages' => 'array|nullable',
        'trainer.auto_generation' => 'required|boolean',

        'trainer.name_lat' => 'string|nullable',
        'trainer.last_name_lat' => 'string|nullable',
        'trainer.gender' => 'required|integer',
        'trainer.institute' => 'integer|nullable',
        'trainer.nationality' => 'nullable|integer',
        'trainer.academic_category' => 'nullable|integer',
        'trainer.jobname' => 'nullable|integer',
        'oblast' => 'required|integer',
        'raion' => 'required|integer',
        'gorod' => 'nullable|integer',
        'trainer.education' => 'nullable|integer',
        'trainer.academic_degree' => 'nullable|integer',
//        'trainer.subject_teach' => 'string|nullable',
        'trainer.teach_subject_id' => 'nullable|integer',
        'trainer.stazh' => 'required|integer',
        'trainer.iban' => 'string|nullable',
        'trainer.bank_name' => 'string|nullable',
        'trainer.attractedcat' => 'nullable|integer',
        'trainer_awards' => 'nullable',
        'trainer_theses' => 'nullable',
        'trainer_certificates' => 'nullable',

    ];
    public function mount($id = null)
    {
        abort_if(Gate::denies(\App\Models\Permission::PERMISSION_OMR), Response::HTTP_FORBIDDEN, '403 Ошибка доступа ');

        if ($id) {
            $this->trainer = User::find($id);
            $this->languages = $this->trainer->languages->pluck('id')->toArray();
        } else {
            $this->trainer = new User();
            $this->new = true;
        }
        $this->oblast = $this->trainer->oblast;
        $this->raion = $this->trainer->raion;
        $this->gorod = $this->trainer->gorod;
        $this->stazh = $this->trainer->stazh;
        $this->trainer_awards = $this->trainer->trainer_awards ?? null;
        $this->trainer_theses = $this->trainer->trainer_theses ?? null;
        $this->trainer_certificates = $this->trainer->trainer_certificates ?? null;


    }

    public function reset2()
    {
        $this->query = '';
        $this->courses = [];
    }

    public function updatedShowModalAddCourses() {
        $this->reset2();
    }
    public function updatedQuery()
    {
        $search = '%'.$this->query.'%';
        $this->courses = Courses::select('id','kz_title','ru_title')
            ->where(function ($query) use ($search) {
                $query->where('kz_title', 'LIKE', $search)
                    ->orWhere('ru_title', 'LIKE', $search)
                ;
            })
            ->get()
            ->toArray();
    }
    public function add_courses_modal_open()
    {
        $this->showModalAddCourses = true;
    }

    public function addCourse($id)
    {
        $this->trainer->courses()->syncWithoutDetaching($id);
    }

    public function removeCourse($id)
    {
        $this->trainer->courses()->detach($id);
    }

    public function search()
    {
        $this->validate([
            'trainer.iin' => 'digits:12|required'
        ]);
        $item = User::where('iin',$this->trainer->iin)->first();
        if ($item)
        {
            return redirect()->route('omr.trainers.edit',['id' => $item->id])->with('user_for_trainer_found', 'Пользователь найден, нажмите сохранить для добавления. ');;
            $this->new = false;
        } else {
            session()->flash('user_not_found', 'user_not_found');
        }
    }

    public function save()
    {
        $this->validate();
        if ($this->trainer->affiliate == 'null') {
            $this->trainer->affiliate = null;
        }
        $this->trainer->languages()->sync($this->languages);

        $this->trainer->oblast = $this->oblast;
        $this->trainer->raion = $this->raion;
        $this->trainer->gorod = $this->gorod;
        $this->trainer->trainer_awards = $this->trainer_awards;
        $this->trainer->trainer_theses = $this->trainer_theses;
        $this->trainer->trainer_certificates = $this->trainer_certificates;

        $this->trainer->save();
        $this->trainer->roles()->syncWithoutDetaching(Role::ROLE_TRAINER);

        return redirect()->route('omr.trainers.index');
    }

    public function delete()
    {
        $this->trainer->roles()->detach(Role::ROLE_TRAINER);
        return redirect()->route('omr.trainers.index');
    }

    public function render()
    {
        return view('livewire.omr.trainers.trainers-edit')->layout('layouts.omr');
    }
}
