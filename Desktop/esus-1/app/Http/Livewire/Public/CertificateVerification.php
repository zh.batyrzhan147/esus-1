<?php

namespace App\Http\Livewire\Public;

use App\Models\Certificates;
use Livewire\Component;

class CertificateVerification extends Component
{
    public $certificate;

    public function mount($uuid)
    {
        $this->certificate = Certificates::where('uuid',$uuid)->first();
    }

    public function render()
    {
        return view('livewire.public.certificate-verification')->layout('layouts.guest');
    }
}
