<?php

namespace App\Http\Livewire\Public;

use App\Traits\ValidateMessages;
use Livewire\Component;

class CertificateIndex extends Component
{
    public $uuid;
    use ValidateMessages;
    protected $rules = [
        'uuid' => 'required|string'
    ];
    public function verifyuuid()
    {
        $this->validate();
        return $this->redirectRoute('verify-certificate',['uuid' => $this->uuid]);
    }

    public function render()
    {
        return view('livewire.public.certificate-index')->layout('layouts.guest');
    }
}
