<?php

namespace App\Http\Livewire\Profile;

use Illuminate\Support\Facades\Auth;
use Livewire\Component;
use Livewire\WithPagination;
use App\Models\Orders;

class MessagesList extends Component
{
//    public $messages;
//    public $unreadmessages;
    public $message = null;
    public $showmodal = false;
    use WithPagination;

    public function mount()
    {
    }

    public function readNotification($id = null)
    {
        if ($id) {
            $this->message = auth()->user()->notifications()->where('id', $id)->first();
            auth()->user()
                ->unreadNotifications
                ->when($id, function ($query) use ($id) {
                    return $query->where('id', $id);
                })
                ->markAsRead();
            $this->showmodal = true;
        }
    }

    public function closeModal()
    {
        $this->message = null;
        $this->showmodal = false;
    }


    public function markasread($id)
    {


//        $this->emit('$refresh');
//
//        dd($id);
    }

    public function render()
    {
        $unreadmessages = auth()->user()->unreadNotifications()->paginate(10, ['*'], 'unreadPage');
        $messages = auth()->user()->readNotifications()->paginate(10, ['*'], 'messagesPage');

        return view('livewire.profile.messages-list', [
            'messages' => $messages,
            'unreadmessages' => $unreadmessages,
        ])->layout('layouts.user');;
    }
}
