<?php

namespace App\Http\Livewire\Profile;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;
use Livewire\Component;
use Symfony\Component\HttpFoundation\Response;

class UserController extends Component
{
    protected $listeners = ['saved' => 'passwordSaved'];
    public $edituser = false;

    public $iin;
    public $email;
    public $last_name;
    public $gorod;
    public $name;
    public $institute;
    public $patronymic;
    public $jobname;
    public $gender;
    public $nationality;
    public $selfpassword = false;


    public function updatedChangePassword()
    {
        $this->selfpassword = false;
    }
    public function passwordSaved()
    {
        $this->changePassword = false;
    }

    public bool $changePassword = false;

    public function edit_profile()
    {
        $this->edituser = true;
    }

    public function save_profile()
    {
        $this->edituser = false;
    }
    public function mount()
    {
        $this->iin = Auth::user()->iin;
        $this->email = Auth::user()->email;
        $this->last_name = Auth::user()->last_name;
        $this->gorod = Auth::user()->gorod->id ?? null;
        $this->name = Auth::user()->name;
        $this->institute = Auth::user()->instituteItem->id ?? null;
        $this->patronymic = Auth::user()->patronymic;
        $this->jobname = Auth::user()->jobnameItem->id ?? null;
        $this->gender = Auth::user()->genderItem->id ?? null;
        $this->nationality = Auth::user()->nationalityItem->id ?? null;
    }

    public function resetPassword()
    {
        Auth::user()->resetPassword();
        $this->changePassword = false;
        $this->emit('password_reset');
    }
    public function render(Request $request)
    {
        if ($enter_url = $request->session()->get('enter_url')){
            redirect($enter_url);
        }
        return view('livewire.profile.user-profile')->layout('layouts.user');
    }
}
