<?php

namespace App\Http\Livewire\Profile;

use App\Models\Groups;
use App\Models\Orders;
use App\Traits\ValidateMessages;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Notification;
use Livewire\Component;

class JoinGroup extends Component
{
    public $order;
    public $group;
    public $status;
    public $unreg = false;
    public $agreeoffer = false;
    use ValidateMessages;
    public function mount($uuid = null)
    {
        if (!Auth::user()) {
            $this->unreg = true;
        }
        if ($uuid) {
            $this->group = Groups::where('hash', $uuid)->where('status',Groups::GROUP_STATUS_PLANNED)->first();
            if (!$this->group) {
                abort(404);
            }
//            if ($this->group->trainer === (Auth::user()->id ?? '')) {
//                abort(403);
//            }
        } else {
            abort(404);
        }

        if (!$this->unreg) {
            $this->order = $this->getOrder();
        }
    }


    public function checkUser($id)
    {
        $aproved_ids = $this->group->users()->pluck('id')->toArray();
        $existing_ids = $this->group->users_sb()->pluck('id')->toArray();
        $allids = array_unique(array_merge($aproved_ids,$existing_ids), SORT_REGULAR);
        if (in_array($id,$allids)) {
            return true;
        } else {
            return false;
        }
    }
    public function attachUniqueUsers($id)
    {
        if (!$this->checkUser($id))
        {
            $this->group->users_sb()->attach($id,['status' => Groups::GROUPS_USER_REQUEST]);
        }
//        @dd($ids->diff($allids));
//        $existing_ids = $this->group->users()->whereIn('user_id', $ids)->pluck('user_id');
//        $this->posts()->attach($ids->diff($existing_ids));
    }


    public function joinGroup()
    {
        $this->validate([
            'agreeoffer' => 'required|boolean'
        ]);
        if ($this->agreeoffer == false) {
            $this->agreeoffer = null;
            $this->validate([
                'agreeoffer' => 'required|boolean'
            ]);
        }
        $this->status = $this->attachUniqueUsers(Auth::user()->id);

//        $this->group->users_sb()->attach(Auth::user()->id,['status' => Groups::GROUPS_USER_STATUS1]);
    }


    public function makeOrder()
    {
        if ($this->agreeoffer) {
            $this->order = $this->getOrder(true);
        } else {
            $this->agreeoffer = null;
            $this->validate([
                'agreeoffer' => 'required|boolean'
            ]);
        }
    }

    public function render(Request $request)
    {
        $request->session()->put('enter_url', $_SERVER['REQUEST_URI']);

        if (Auth::check()) {
            $request->session()->pull('enter_url');
        }

        if ($this->unreg) return view('livewire.profile.join-group-error')->layout('layouts.guest');
        $this->status = $this->checkUser(Auth::user()->id);
        return view('livewire.profile.join-group')->layout('layouts.user');
    }

    /**
     * @param bool $new
     * @return Orders|null
     */
    protected function getOrder(bool $new = false): ?Orders
    {
        $user = Auth::user();
        $order = Orders::whereUserId($user->id)->wherePayed(false)->whereCanceled(false)->whereHasMorph('target', Groups::class, function ($query) {
            $query->whereId($this->group->id);
        })->first();

        if ($new && empty($order)) {
            $order = new Orders();
            $order->total = $this->group->coursesItem->price;
            $order->user_id = $user->id;
            $order->target()->associate($this->group);
            $order->save();
        }

        return $order;
    }
}
