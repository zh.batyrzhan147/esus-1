<?php

namespace App\Http\Livewire\Profile;

use App\Models\Certificates;
use App\Models\Feedbacks;
use App\Models\Groups;
use App\Models\Statements;
use App\Models\StatementsMessages;
use App\Traits\ValidateMessages;
use Illuminate\Support\Facades\Auth;
use Livewire\Component;
use Livewire\WithFileUploads;

class GroupView extends Component
{
    use WithFileUploads;
    public $group;
    public $statements;
    public $modalattach = false;
    public $modalid;
    public $file;
    public $description;
    public $message;
    public $decline = false;
    public $certificates;
    public $feedback;
    public $stars;
    public $feedbacktext;

    use ValidateMessages;
    protected $rules = [
        'description' => 'required|string',
        'file' => 'nullable|file|max:1000000',
        'feedback' => 'nullable'
    ];
    public function mount($slug = null)
    {
        if ($slug) {
            $this->group = Groups::where('slug', $slug)->first();
            if ($this->group) {
                if (!in_array(Auth::user()->id, $this->group->users->pluck('id')->toArray())) {
                    abort(403);
                }
            } else {
                abort(404);
            }

            $this->statements = Statements::where('group', $this->group->id)->where('user', Auth::user()->id)
                ->whereIn('status', [Statements::STATEMENTS_MISSING,Statements::STATEMENTS_MISSING_SENT,Statements::STATEMENTS_MISSING_DECLINE, Statements::STATEMENTS_SICK])
                ->get();
            $this->certificates = Certificates::where('group_id',$this->group->id)->where('user',auth()->user()->id)->get();

            if ($this->group->status == \App\Models\Groups::GROUP_STATUS_FINISHED or $this->group->status == \App\Models\Groups::GROUP_STATUS_FINISHED_TESTING  or $this->group->status == \App\Models\Groups::GROUP_STATUS_CERTIFICATED  or $this->group->status == \App\Models\Groups::GROUP_STATUS_ARCHIVE)
            {
                $this->feedback = Feedbacks::where('listener', auth()->user()->id)->where('group',$this->group->id)->first();
                if ($this->feedback) {
                    $this->stars  = $this->feedback->stars;
                    $this->feedbacktext = $this->feedback->description;
                }
            }
        }
    }

    public function attachdocs($id = null)
    {
        $tmp = StatementsMessages::where('statement', $id)
//            ->where('status',StatementsMessages::STATEMENTSMESSAGE_STATUS_NEW)
            ->first();
        if (!$tmp) {
            $this->decline = false;
            $this->modalid = Statements::where('id', $id)->first();
            if ($this->modalid) $this->modalattach = true;
        } else {
            if ($tmp->status == StatementsMessages::STATEMENTSMESSAGE_STATUS_NEW)  $this->emit('messageExists');
            if ($tmp->status == StatementsMessages::STATEMENTSMESSAGE_STATUS_UPDATED)  $this->emit('messageExists');
            if ($tmp->status == StatementsMessages::STATEMENTSMESSAGE_STATUS_DECLINE)
            {
                $this->modalid = Statements::where('id', $id)->first();
                $this->decline = true;
                if ($this->modalid) $this->modalattach = true;
            }
        }
    }

    public function attachfiles($id = null)
    {
        $this->validate();
        $statement = Statements::where('id', $id)->first();
        if ($statement) {
            $tmp = StatementsMessages::where('statement', $statement->id)->first();
            if (!$tmp) $tmp = new StatementsMessages();
            $tmp->user = auth()->user()->id;
            $tmp->group = $this->group->id;
            $tmp->affiliate = $this->group->affiliate;
            $tmp->statement = $statement->id;
            $tmp->description = $this->description;
            $tmp->status = ($this->decline) ? StatementsMessages::STATEMENTSMESSAGE_STATUS_UPDATED : StatementsMessages::STATEMENTSMESSAGE_STATUS_NEW;
            $tmp->save();
            $this->file && $tmp->update([
                'file' => $this->file->store('/messages/'.$tmp->id,'public'),
            ]);
            $statement->status = Statements::STATEMENTS_MISSING_SENT;
            $statement->save();
        }
        $this->modalattach = false;
        $this->modalid = null;
    }

    public function render()
    {
        return view('livewire.profile.group-view')->layout('layouts.user');
    }

    public function saveFeedback()
    {
        $this->validate([
            'stars' => 'required|integer',
            'feedbacktext' => 'nullable|string'
        ]);

        Feedbacks::updateOrCreate(
            [
                'year' => $this->group->year,
                'trainer' => $this->group->trainer,
                'listener' => auth()->user()->id,
                'course' => $this->group->courses,
                'group' => $this->group->id,
            ],
            [
                'stars' => $this->stars,
                'description' => $this->feedbacktext,
            ]
        );
        return redirect(request()->header('Referer'));
    }
}
