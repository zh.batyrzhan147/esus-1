<?php

namespace App\Http\Livewire\Profile\Users;

use App\Models\User;
use Illuminate\Support\Facades\Gate;
use Livewire\Component;
use Symfony\Component\HttpFoundation\Response;

class UsersEdit extends Component
{
    public $user;
    public $oblast;
    public $raion;
    public $gorod;
    public $trainer_awards;
    public $trainer_theses;
    public $trainer_certificates;

    use \App\Traits\ValidateMessages;
    public $rules = [
//        'user.iin' => 'digits:12|required',
        'user.name' => 'required|string',
        'user.last_name' => 'required|string',
        'user.name_lat' => 'required|regex:/^[a-zA-Z]+$/u|string',
        'user.last_name_lat' => 'required|regex:/^[a-zA-Z]+$/u|string',
        'user.patronymic' => 'required|string',
        'user.phone' => 'required|string',
        'user.email' => 'required|email',
//        'user.auto_generation' => 'required|boolean',
        'user.gender' => 'required|integer',
        'user.institute' => 'required|integer',
        'user.nationality' => 'required|integer',
        'user.academic_category' => 'required|integer',
        'user.jobname' => 'required|integer',
        'oblast' => 'required|integer',
        'raion' => 'required|integer',
        'gorod' => 'required|integer',
        'user.education' => 'required|integer',
        'user.academic_degree' => 'required|integer',
        'user.teach_subject_id' => 'nullable|integer',
//        'user.subject_teach' => 'required|string',
        'user.stazh' => 'required|integer',

//        'user.iban' => 'string|nullable',
//        'user.bank_name' => 'string|nullable',

        'trainer_awards' => 'nullable',
        'trainer_theses' => 'nullable',
        'trainer_certificates' => 'nullable',
    ];

    public function updatedUser($value)
    {
        $this->validate([
            'user.name_lat' => 'required|regex:/^[a-zA-Z]+$/u|string',
            'user.last_name_lat' => 'required|regex:/^[a-zA-Z]+$/u|string',
        ]);
    }

    public function updatedOblast()
    {
        $this->raion = null;
        $this->gorod = null;
    }

    public function updatedGorod($value)
    {
        if (is_array($value)) $this->gorod = reset($value);
    }

    public function save()
    {

        if (is_array($this->user->institute))  { $q = $this->user->institute; $this->user->institute = reset($q);}
        $this->validate();
        if ($this->user->affiliate == '') $this->user->affiliate = null;
        $this->user->oblast = $this->oblast;
        $this->user->raion = $this->raion;
        $this->user->gorod = $this->gorod;
        $this->user->trainer_awards = $this->trainer_awards;
        $this->user->trainer_theses = $this->trainer_theses;
        $this->user->trainer_certificates = $this->trainer_certificates;

        $this->user->save();

        return redirect()->route('user.profile');

    }
    public function mount($iin = null)
    {
        $this->user = auth()->user();
        $this->oblast = $this->user->oblast ?? null;
        $this->raion = $this->user->raion ?? null;
        $this->gorod = $this->user->gorod ?? null;
        $this->trainer_awards = $this->user->trainer_awards ?? null;
        $this->trainer_theses = $this->user->trainer_theses ?? null;
        $this->trainer_certificates = $this->user->trainer_certificates ?? null;
    }

    public function render()
    {
        return view('livewire.profile.users.users-edit')->layout('layouts.user');
    }
}
