<?php

namespace App\Http\Livewire\Profile\Groups;

use App\Models\Groups;
use Illuminate\Support\Facades\Auth;
use Livewire\Component;

class Active extends Component
{
    public function render()
    {
        return view('livewire.profile.groups.active',[
            'groups' => Auth::user()
                ->listenerGroups()
                ->whereIn('status',[\App\Models\Groups::GROUP_STATUS_PLANNED,\App\Models\Groups::GROUP_STATUS_FORMED,\App\Models\Groups::GROUP_STATUS_INPROGRESS])
                ->with('trainerItem','coursesItem')
                ->get(),
        ])->layout('layouts.user');
    }
}
