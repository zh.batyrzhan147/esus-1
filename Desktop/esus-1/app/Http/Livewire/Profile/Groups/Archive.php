<?php

namespace App\Http\Livewire\Profile\Groups;

use App\Models\Groups;
use Illuminate\Support\Facades\Auth;
use Livewire\Component;

class Archive extends Component
{
    public function render()
    {
        return view('livewire.profile.groups.archive',[
            'groups' => Auth::user()
                ->listenerGroups()
                ->whereIn('status',[\App\Models\Groups::GROUP_STATUS_FINISHED,\App\Models\Groups::GROUP_STATUS_CERTIFICATED,\App\Models\Groups::GROUP_STATUS_FINISHED_TESTING,\App\Models\Groups::GROUP_STATUS_ARCHIVE])
                ->with('trainerItem','coursesItem')
                ->get(),
        ])->layout('layouts.user');
    }
}
