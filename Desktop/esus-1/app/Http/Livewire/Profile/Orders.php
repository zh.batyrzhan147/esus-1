<?php

namespace App\Http\Livewire\Profile;

use Livewire\Component;
use Livewire\WithPagination;

class Orders extends Component
{
    use WithPagination;

    /**
     * @var Orders
     */
    public $item = null;

    /**
     * @var bool
     */
    public $show = false;

    /**
     * @param int $id
     */
    public function mount(int $id = null)
    {
        if ($id) {
            $this->show($id);
        }
    }

    /**
     * @param int $id
     * @return void
     */
    public function show(int $id)
    {
        $this->item = auth()->user()->orders()->findOrFail($id);
        $this->show = true;
    }

    /**
     * @return void
     */
    public function close()
    {
        $this->show = false;
    }

    /**
     * @return mixed
     */
    public function render()
    {
        return view('livewire.profile.orders', ['list' => auth()->user()->orders()->get()])->layout('layouts.user');
    }
}
