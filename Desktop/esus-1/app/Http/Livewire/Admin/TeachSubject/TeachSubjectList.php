<?php

namespace App\Http\Livewire\Admin\TeachSubject;

use App\Models\TeachSubjects;
use App\Traits\ValidateMessages;
use Illuminate\Support\Facades\Gate;
use Livewire\Component;
use Symfony\Component\HttpFoundation\Response;

class TeachSubjectList extends Component
{
    public $ipp = 20;
    public $modal = false;
    public ?TeachSubjects $teachSubject;

    function mount()
    {
        abort_if(Gate::denies(\App\Models\Permission::PERMISSION_SUPERADMIN), Response::HTTP_FORBIDDEN, '403 Ошибка доступа ');
    }

    use ValidateMessages;
    protected  $rules = [
        'teachSubject.kz_title' => 'required|string',
        'teachSubject.ru_title' => 'required|string',
    ];

    public function applyFilter()
    {
        $this->resetPage();
    }


    public function render()
    {
        $items = TeachSubjects::query();
        $items = $this->applySearch($items);
        return view('livewire.admin.teach-subject.teach-subject-list',[
            'items' => $items->paginate($this->ipp),
        ])->layout('layouts.admin');
    }

    private function applySearch(\Illuminate\Database\Eloquent\Builder $item)
    {
        return $item;
    }

    public function add_new()
    {
        $this->teachSubject = new TeachSubjects();
        $this->modal = true;
    }

    public function edit($id)
    {
        $this->teachSubject = TeachSubjects::find($id);
        $this->modal = true;

    }

    public function save()
    {
        $this->validate();
        $this->teachSubject->save();
        $this->modal = false;
        $this->teachSubject = null;
    }

    public function destroy($id)
    {
        TeachSubjects::find($id)->delete();
        $this->emit('deleteSuccess');
    }
}
