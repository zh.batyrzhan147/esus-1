<?php

namespace App\Http\Livewire\Admin\Attracted;

use App\Models\AttractedCats;
use App\Traits\ValidateMessages;
use Illuminate\Support\Facades\Gate;
use Livewire\Component;
use Livewire\WithPagination;
use Symfony\Component\HttpFoundation\Response;

class AttractedIndex extends Component
{

    use WithPagination;
    private $items;
    public $ipp = 20;
    public $search;
    public $affiliate;
    public $newitem;
    public  $modal_add_new = false;

    function mount()
    {
        abort_if(Gate::denies(\App\Models\Permission::PERMISSION_SUPERADMIN), Response::HTTP_FORBIDDEN, '403 Ошибка доступа ');
    }
    use ValidateMessages;
    protected  $rules = [
        'newitem.ru_title' => 'required|string',
        'newitem.kz_title' => 'required|string',
    ];

    public function applyFilter()
    {
        $this->resetPage();
    }

    public function render()
    {
        $this->items = AttractedCats::query();
        $this->items = $this->applySearch($this->items);

        return view('livewire.admin.attracted.attracted-index', [
            'items' => $this->items->paginate($this->ipp),
        ])->layout('layouts.admin');
    }

    private function applySearch(\Illuminate\Database\Eloquent\Builder $item)
    {
        $item = $item->orderBy('id', 'DESC');
        return $item;
    }

    public function add_new_item()
    {
        $this->newitem = new AttractedCats();
        $this->modal_add_new = true;
    }

    public function edit($id)
    {
        $this->newitem = AttractedCats::where('id',$id)->first();
        $this->modal_add_new = true;
    }

    public function save()
    {
        $this->validate();
        $this->newitem->save();
        $this->newitem = null;
        $this->modal_add_new = false;
    }

    public function destroy($id)
    {
        AttractedCats::find($id)->delete();
    }
}
