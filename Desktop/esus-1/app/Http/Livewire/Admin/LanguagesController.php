<?php

namespace App\Http\Livewire\Admin;

use App\Models\Languages;
use Illuminate\Support\Facades\Gate;
use Livewire\Component;
use Livewire\WithPagination;
use Symfony\Component\HttpFoundation\Response;
use function abort_if;
use function view;

class LanguagesController extends Component
{
    use WithPagination;
    public $modal_add_new_lang = false;
    public Languages $newlang;
    use \App\Traits\ValidateMessages;
    protected $rules = [
        'newlang.slug' => 'required|unique:languages,slug|string',
        'newlang.title' => 'required|string',
    ];

    public function mount()
    {
        abort_if(Gate::denies(\App\Models\Permission::PERMISSION_SUPERADMIN), Response::HTTP_FORBIDDEN, '403 Ошибка доступа ');
    }
    public function add_new_lang()
    {
        $this->newlang = new Languages();
        $this->modal_add_new_lang = true;
    }

    public function store_new_lang()
    {
        $this->validate();
        $this->newlang->save();
        $this->modal_add_new_lang = false;
        $this->emitUp('triggerSuccessCreate');
    }

    public function destroy($id)
    {
        Languages::find($id)->delete();
        return $this->redirectRoute('admin.languages.index');
    }


    public function render()
    {
        return view('livewire.languages.languages-controller', [
            'languages' => Languages::orderBy('id', 'ASC')->paginate(),
        ])->layout('layouts.admin');
    }
}
