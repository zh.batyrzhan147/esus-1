<?php

namespace App\Http\Livewire\Admin;

use App\Models\Courses;
use Illuminate\Support\Facades\Gate;
use Livewire\Component;
use Livewire\WithPagination;
use Symfony\Component\HttpFoundation\Response;
use function abort_if;
use function view;

class CoursesController extends Component
{
    use WithPagination;

    public $search;
    public $type;
    public $grading;

    private $courses;

    use \App\Traits\ValidateMessages;
    private $rules = [
        'search' => 'nullable|string',
        'type' => 'nullable|integer',
        'grading' => 'nullable|integer'
    ];


    public function mount()
    {
        abort_if(Gate::denies(\App\Models\Permission::PERMISSION_SUPERADMIN), Response::HTTP_FORBIDDEN, '403 Ошибка доступа ');
        $this->applyFilter();
    }

    public function applyFilter()
    {
        $this->courses = Courses::query();
        $this->courses = $this->applySearch($this->courses)->paginate();
    }


    public function render()
    {
        return view('admin.courses.index',[
            'courses' => $this->courses,
        ])->layout('layouts.admin');
    }

    private function applySearch(\Illuminate\Database\Eloquent\Builder $item)
    {
        if ($this->search) {
            $search = '%' . $this->search . '%';
            $item = $item->where(function ($query) use ($search) {
                $query->where('kz_title', 'LIKE', $search)
                    ->orWhere('ru_title', 'LIKE', $search)
                ;
            });
        }

        if ($this->type !== null) {
            $item = $item->where('type', $this->type);
        }

        if ($this->grading !== null)
        {
            $item = $item->where('grading', $this->grading);
        }

        return $item;
    }
}
