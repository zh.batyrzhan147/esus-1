<?php

namespace App\Http\Livewire\Admin;

use Cocur\Slugify\Slugify;
use Livewire\Component;
use function old;

class SlugGenerator extends Component
{
    public $kz_title;
    public $ru_title;
    public $slug;
    public $cid = '';
    public $rules = [
    ];

    use \App\Traits\ValidateMessages;
    public function rules()
    {
        return [
            'kz_title' => 'required|string',
            'ru_title' => 'required|string',
            'slug' => 'required|string|unique:groups,slug'.$this->cid,
        ];
    }


    public function mount($course = null)
    {
        if ($course) {
            $this->cid = ','.$course->id;
            $this->kz_title = (old('kz_title'))? old('kz_title'):$course->kz_title;
            $this->ru_title = (old('ru_title'))? old('ru_title'):$course->ru_title;
            $this->slug = (old('slug'))? old('slug'):$course->slug;
        } else {
            $this->kz_title = old('kz_title');
            $this->ru_title = old('ru_title');
            $this->slug = old('slug');
            $this->cid = '';
        }
    }

    public function generate()
    {
        $slugify = new Slugify();
        if ($this->kz_title) {
            $this->slug = substr($slugify->slugify($this->kz_title), 0, 255);
        } elseif ($this->ru_title) {
            $this->slug = substr($slugify->slugify($this->ru_title), 0, 255);
        }
    }

    public function render()
    {
        return <<<'blade'
        <div class="flex flex-col items-start w-full mx-auto space-y-4">
            <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-center">
                <div class="w-full md:w-1/4">
                    <label for="kz_title" class="w-full text-left md:text-right block text-sm">Название на казахском:</label>
                </div>
                <div class="w-full md:w-3/4">
                    <input wire:model="kz_title" type="text" name="kz_title" id="kz_title" class="w-full md:w-3/4 px-3 py-1 border rounded focus:border-blue-500 hover:border-blue-500 text-sm outline-none focus:input-shadow"
                           value="{{ old('kz_title', $kz_title) }}" />
                    @error('kz_title')
                    <p class="text-xs text-red-600">{{ $message }}</p>
                    @enderror
                </div>
            </div>

            <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-center">
                <div class="w-full md:w-1/4">
                    <label for="ru_title" class="w-full text-left md:text-right block text-sm">Название на русском:</label>
                </div>
                <div class="w-full md:w-3/4">
                    <input wire:model="ru_title" type="text" name="ru_title" id="ru_title" class="w-full md:w-3/4 px-3 py-1 border rounded focus:border-blue-500 hover:border-blue-500 text-sm outline-none focus:input-shadow"
                           value="{{ old('ru_title', $ru_title) }}" />
                    @error('ru_title')
                    <p class="text-xs text-red-600">{{ $message }}</p>
                    @enderror
                </div>
            </div>

            <div class="flex flex-col md:flex-row w-full md:space-x-4 md:items-center">
                <div class="w-full md:w-1/4">
                    <label for="slug" class="w-full text-left md:text-right block text-sm">ЧПУ:</label>
                </div>
                <div class="w-full md:w-3/4 ">
                    <div class="w-full md:w-3/4 flex flex-row">
                        <input wire:model="slug" type="text" name="slug" id="slug" class="w-full md:w-3/4 px-3 py-1 border rounded focus:border-blue-500 hover:border-blue-500 text-sm outline-none focus:input-shadow"
                               value="{{ old('slug', $slug) }}" />
                        <button type="button" wire:click="generate" class="ml-2 text-sm flex justify-center items-center px-4 py-1 border-0 bg-blue-500 text-white rounded hover:bg-blue-600">Генерировать</button>
                    </div>
                    @error('slug')
                    <p class="text-xs text-red-600">{{ $message }}</p>
                    @enderror
                </div>
            </div>
        </div>
        blade;
    }
}
