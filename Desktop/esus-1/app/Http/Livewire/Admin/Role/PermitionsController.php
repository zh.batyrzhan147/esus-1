<?php

namespace App\Http\Livewire\Admin\Role;

use App\Models\Permission;
use App\Models\Role;
use Illuminate\Support\Facades\Gate;
use Livewire\Component;
use Symfony\Component\HttpFoundation\Response;

class PermitionsController extends Component
{
    public mixed $roles;

    public function mount()
    {
        abort_if(Gate::denies(\App\Models\Permission::PERMISSION_SUPERADMIN), Response::HTTP_FORBIDDEN, '403 Ошибка доступа ');
        $this->roles = Role::all();
    }

    public function reloadRoles()
    {
        abort_if(Gate::denies(\App\Models\Permission::PERMISSION_SUPERADMIN), Response::HTTP_FORBIDDEN, '403 Ошибка доступа ');
        foreach (Role::listRoles() as $key => $role) {
            Role::insertOrIgnore([
                'id'    => $key,
                'title' => $role,
            ]);
        }

        foreach (Permission::listPermissions() as $key => $item)
        {
            Permission::insertOrIgnore([
                'id'    => $key,
                'title' => $item,
            ]);
        }

        $superadmin_permissions = Permission::all();
        Role::findOrFail(Role::ROLE_SUPERADMIN)->permissions()->sync($superadmin_permissions->pluck('id'));

        Role::findOrFail(Role::ROLE_ADMIN)->permissions()->sync(
            Permission::PERMISSION_ADMIN
        );

        Role::findOrFail(Role::ROLE_TRAINER)->permissions()->sync(
            Permission::PERMISSION_TRAINER
        );

//        Role::findOrFail(Role::ROLE_USER)->permissions()->sync(
//            Permission::PERMISSION_USER
//        );

        Role::findOrFail(Role::ROLE_AFFILIATE)->permissions()->sync(
            Permission::PERMISSION_AFFILIATE
        );

        Role::findOrFail(Role::ROLE_OOP)->permissions()->sync(
            Permission::PERMISSION_OOP
        );

        Role::findOrFail(Role::ROLE_OMR)->permissions()->sync(
            Permission::PERMISSION_OMR
        );

        Role::findOrFail(Role::ROLE_OOUP)->permissions()->sync(
            Permission::PERMISSION_OOUP
        );

        Role::findOrFail(Role::ROLE_ACCOUNTANT)->permissions()->sync(
            Permission::PERMISSION_ACCOUNTANT
        );

        $this->roles = Role::all();
    }
    public function render()
    {
        return view('livewire.admin.role.permitions-controller')->layout('layouts.admin');
    }
}
