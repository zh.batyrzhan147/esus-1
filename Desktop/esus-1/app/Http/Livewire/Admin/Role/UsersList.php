<?php

namespace App\Http\Livewire\Admin\Role;

use App\Models\Role;
use App\Models\User;
use Livewire\Component;
use Livewire\WithPagination;

class UsersList extends Component
{
    use WithPagination;

    public $search;
    public $affiliate;
    public $language;
    public $role;
    protected $users;
    public $modal_add_new = false;
    public $iin;
    public $new;

    use \App\Traits\ValidateMessages;
    protected $rules = [
        'new' => 'nullable|array',
        'iin' => 'digits:12',
    ];

    public function mount(Role $role)
    {
        $this->role = $role;
    }

    public function render()
    {
        $this->users = User::query();
        $this->users = $this->applySearch($this->users);

        return view('livewire.admin.role.users-list', [
            'users' => $this->users->paginate(20),
        ])->layout('layouts.admin');
    }

    public function search()
    {
        $this->new = User::where('iin', $this->iin)
//            ->where('iin', '!=', 'admin')
            ->first();
        if (!$this->new) {
            $this->new = false;
        }
    }

    public function add_new()
    {
        $this->iin = null;
        $this->new = null;
        $this->modal_add_new = true;
    }

    public function add_user()
    {
        $this->new->roles()->attach($this->role->id);
        $this->iin = null;
        $this->new = null;
        $this->modal_add_new = false;
    }

    public function applyFilter()
    {
        $this->resetPage();
    }

    public function destroy($id)
    {
        if (!(($this->role->id == ROLE::ROLE_SUPERADMIN) and ($id == 1)))
            User::find($id)->roles()->detach($this->role->id);
    }

    private function applySearch(\Illuminate\Database\Eloquent\Builder $item)
    {
        $item = $item->whereHas('roles', function ($q) {
            $q->where('id', $this->role->id);
        });
        if ($this->search) {
            $search = $this->search;
            $item = $item->where(function ($query) use ($search) {
                $searchItems = explode(" ", preg_replace('/\s+/', ' ', trim($search)));
                foreach ($searchItems as $text) {
                    $query->orWhere('iin', 'LIKE', '%' . $text . '%')
                        ->orWhere('name', 'LIKE', '%' . $text . '%')
                        ->orWhere('last_name', 'LIKE', '%' . $text . '%')
                        ->orWhere('patronymic', 'LIKE', '%' . $text . '%');
                }

            });
        }
        if ($this->affiliate) {
            $item = $item->where('affiliate', $this->affiliate);
        }

        if ($this->language) {
            $lang = $this->language;
            $item = $item->whereHas('languages', function ($q) use ($lang) {
                $q->where('id', $lang);
            });
        }
        return $item;
    }
}
