<?php

namespace App\Http\Livewire\Admin;

use Livewire\Component;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AuthinController extends Component
{
    public function render(Request $request)
    {
        Auth::loginUsingId($request->id);
        redirect('/profile');
        return view('livewire.profile.user-profile')->layout('layouts.user');
    }
}
