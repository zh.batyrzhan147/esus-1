<?php

namespace App\Http\Livewire\Admin;

use App\Http\Livewire\Weekend;
use App\Models\Courses;
use App\Models\Groups;
use App\Models\Years;
use DateTime;
use Illuminate\Support\Facades\Gate;
use Livewire\Component;
use Livewire\WithFileUploads;
use Livewire\WithPagination;
use Ramsey\Uuid\Uuid;
use Symfony\Component\HttpFoundation\Response;
use function abort_if;
use function view;


class GroupsController extends Component
{
    use WithFileUploads;
    use WithPagination;

    public $year;
    public $paid = null;
    public $course;
    public $trainer;
    public $language;
    public $yearItem;
    public $affiliate;
    public $showAddPopup = false;
    public $trainers = [];
    public $notUsedGrants = false;
    public $slug;
    public $offer_agreement;
    public $paginationCount = 10;
    protected $queryString;
    public $count_all;
    public $start_time;
    public $end_time;

    public $status;
    public $search;
    public $courseItem;
    public $traineraffiliate;
    public $practice_start;
    public $practice_end;

    public function  queryString() {
        return [
            'paginationCount' => [ 'except' => 10]
              ];
      }

    public function __construct()
    {
        parent::__construct();
//        $this->year = date('Y');
        $this->course = null;
        $this->queryString = [
            'search',
            'year',
            'status',
            'course',
            'affiliate',
        ];
    }

    use \App\Traits\ValidateMessages;

    protected $rules = [
        'paid' => 'nullable|boolean',
        'year' => 'required|integer',
        'course' => 'required|integer',
        'affiliate' => 'required|integer',
        'trainer' => 'required|integer',
        'language' => 'required|integer',
        'count_all' => 'required|integer',
        'start_time' => 'required|date',
        'end_time' => 'required|date',
        'practice_start' => 'nullable|date',
        'practice_end' => 'nullable|date',
        'traineraffiliate' => 'required|integer',

//        'status' => 'nullable|integer'
    ];

    public function mount()
    {
        abort_if(Gate::denies(\App\Models\Permission::PERMISSION_SUPERADMIN), Response::HTTP_FORBIDDEN, '403 Ошибка доступа ');
        if ($this->year) $this->yearItem = Years::where('year', $this->year)->first();
    }

    public function updatedYear()
    {
        $this->course = null;
    }

//    public function updatedCourse()
//    {
//        $this->trainer = null;
//        if ($this->course) $this->trainers = Courses::where('id',$this->course)->first()->trainers;
//    }

//    public function updatedLanguage()
//    {
//        $lang = $this->language;
//        if ($this->course) $this->trainers = Courses::where('id',$this->course)->first()->trainers()->whereHas('languages',function ($q) use ($lang) {$q->where('id',$lang);})->get();
//    }

    public function add_new_group()
    {
        $this->showAddPopup = true;
    }

    public function updated()
    {
        if ($this->year) $this->yearItem = Years::where('year', $this->year)->first();
        if ($this->course) {
            $this->courseItem = Courses::where('id', $this->course)->first();
            $lang = $this->language;
            if ($this->traineraffiliate) {
                $this->trainers = $this->courseItem->trainers()->where('affiliate', $this->traineraffiliate)->whereHas('languages', function ($q) use ($lang) {
                    $q->where('id', $lang);
                })->get();
            }

        }
    }

    public function updatedStartTime()
    {
        if ($this->courseItem && $this->yearItem) {
            $days = intval(ceil(($this->courseItem->course_length + $this->courseItem->practice_length) / 8));
            $firstDay = new DateTime($this->start_time);
            while (Weekend::isWeekend($firstDay->format('Y-m-d'))) {
                $firstDay->modify('+1 day');
            }

            $lastday = new DateTime($firstDay->format('Y-m-d'));
            $i = 1;
            while ($i < $days) {
                $lastday->modify('+1 day');
                if (!Weekend::isWeekend($lastday->format('Y-m-d'))) {
                    $i++;
                }
            }
            $this->start_time = $firstDay->format('Y-m-d');
            $this->end_time = $lastday->format('Y-m-d');
        }
    }
//    public function dehydrate()
//    {
//        if ($this->year && $this->course && $this->affiliate && $this->language) {
//            $grants = Grants::where('courses', $this->course)->where('affiliate', $this->affiliate)->where('year', $this->year)->first();
//            $groups = Groups::where('courses', $this->course)->where('affiliate', $this->affiliate)->where('year', $this->year)->where('language',$this->language)->get()->pluck('count_all')->toArray();
//            $used = array_sum($groups);
//            dd($used);
//        }
//    }
    public function addGroup()
    {

        $this->validate();
        $group = new Groups([
            'slug' => Uuid::uuid4(),
            'language' => $this->language,
            'affiliate' => $this->affiliate,
            'year' => $this->year,
            'offer_agreement' => $this->courseItem->offer_agreement ?? null,
            'courses' => $this->course,
            'trainer' => $this->trainer,
            'count_all' => $this->count_all,
            'start_time' => $this->start_time,
            'end_time' => $this->end_time,
            'status' => Groups::GROUP_STATUS_PLANNED,
            'practice_start' => $this->practice_start,
            'practice_end' => $this->practice_end,
            'hash' => Uuid::uuid4(),
        ]);
        $group->save();
        $group->slug = (($group->affiliate < 10) ? '0' . $group->affiliate : $group->affiliate) . '-' . (($group->courses < 10) ? '0' . $group->courses : $group->courses) . '-' . (($group->id < 10) ? '0' . $group->id : $group->id) . '-' . date('y', strtotime('01.01.' . $group->year));
        $group->save();
        $this->search = $group->slug;
        $this->showAddPopup = false;
    }

    public function applyFilter()
    {
        $this->resetPage();
    }

    public function render()
    {
        $groups = Groups::query();
        $groups = $this->applySearch($groups);


        return view('livewire.groups.groups-controller', [
            'groups' => $groups->paginate($this->paginationCount),
        ])->layout('layouts.admin');
    }

    public function destroy($id)
    {
        Groups::find($id)->delete();
    }

    private function applySearch(\Illuminate\Database\Eloquent\Builder $item)
    {
        if ($this->affiliate) {
            $item = $item->where('affiliate', $this->affiliate);
        }
        if ($this->search) {
            $item = $item->where('slug', 'LIKE', '%' . $this->search . '%');
        }
        if ($this->status) {
            $item = $item->where('status', $this->status);
        }
        if (in_array($this->paid, ['true', 'false'])) {
            $item = $item->where('paid', $this->paid === 'true');
        }
        $item = $item->where('active', true)->where('staged', false);
        $item = ($this->year) ? $item->where('year', $this->year) : $item;
        $item = ($this->course) ? $item->where('courses', $this->course) : $item;

        return $item;
    }

    public function downloadcvs(Groups $group)
    {
        return (new \App\Exports\GroupUsersToSDO($group))->download($group->slug.'.csv', \Maatwebsite\Excel\Excel::CSV, [
            'Content-Type' => 'text/csv',
        ]);
    }

}
