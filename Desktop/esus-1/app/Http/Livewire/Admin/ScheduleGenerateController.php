<?php

namespace App\Http\Livewire\Admin;

use App\Http\Livewire\Weekend;
use App\Models\Affiliates;
use App\Models\Courses;
use App\Models\Grants;
use App\Models\Groups;
use App\Models\Languages;
use App\Models\Role;
use App\Models\User;
use App\Models\Years;
use DateTime;
use Illuminate\Support\Facades\Gate;
use Livewire\Component;
use Symfony\Component\HttpFoundation\Response;
use function abort_if;
use function view;

class ScheduleGenerateController extends Component
{
    public $step = 1;
    public $year;
    public $grants;
    public $groups;
    public $grantsByCourses = [];
    public $languages;
    public $affiliates;
    public $courses = [];
//    public $trainers;
    public $groupsAffected;

    public function __construct($id = null)
    {
        parent::__construct($id);
        set_time_limit(0);
        $this->year = 2022;
    }

    public function mount()
    {
        abort_if(Gate::denies(\App\Models\Permission::PERMISSION_SUPERADMIN), Response::HTTP_FORBIDDEN, '403 Ошибка доступа ');
    }

    use \App\Traits\ValidateMessages;
    protected $rules =
        [
            'year' => 'integer'
        ];

    public function nextStep($step)
    {
        $this->step = $step + 1;
        $this->updatedStep($this->step);
    }

    public function prevStep($step)
    {
        $this->step = $step - 1;
        $this->updatedStep($this->step);
    }

    private function action1()
    {
        $trainers = User::whereHas('roles', function ($q) {
            $q->where('id', Role::ROLE_TRAINER);
        })->get();
        foreach ( $trainers as $trainer)
        {
            $this->recountHours($trainer);
        }
        $this->grants = Grants::whereNotNull('courses')->whereNotNull('affiliate')->orderBy('affiliate', 'ASC')->orderBy('courses', 'ASC')->where('year', $this->year)->get();

        $this->groups = Groups::where('year', $this->year)->get();

//        User::where('hours_count', '>', 0)->update(['hours_count' => 0]);

        $this->languages = Languages::all();

        $this->courses = Years::where('id', $this->year)->first()->courses;
        $this->affiliates = Affiliates::all();
        foreach ($this->grants as $grant) {
            $this->grantsByCourses[$grant->courses]['items'][$grant->affiliate]['title'] = $grant->affiliateItem->title;
            $this->grantsByCourses[$grant->courses]['items'][$grant->affiliate]['id'] = $grant->affiliateItem->id;
            $this->grantsByCourses[$grant->courses]['title'] = $grant->coursesItem->kz_title;
            $this->grantsByCourses[$grant->courses]['id'] = $grant->coursesItem->id;
            $this->grantsByCourses[$grant->courses]['type'] = $grant->coursesItem->type;
            foreach ($this->languages as $lang) {


                if (array_key_exists($lang->slug, $grant->countsArray ?? []))  {
                    $grantsCount = intval($grant->countsArray[$lang->slug]);
                } else {
                    $grantsCount = 0;
                }

                $groupsCount = 0;
                $busyCount = 0;
                if (is_int($grantsCount)) {
                    $groups = Groups::where('courses', $grant->coursesItem->id)
                        ->where('staged', false)
                        ->where('affiliate', $grant->affiliateItem->id)
                        ->where('language', $lang->id);

                    $busyCount = $groups->sum('count_all');
//                    if ($busyCount > 0) {
//                        foreach ($groups->get() as $group) {
//                            $user = $group->trainerItem;
//                            $user->hours_count = $user->hours_count + $group->count_all;
//                            $user->save();
//                        }
//                    }
                    $grantsCount = $grantsCount - $busyCount;
                    $groupsCount = (($grantsCount % 25) > 15) ? ceil($grantsCount / 25) : floor($grantsCount / 25);

                    if (intval($groupsCount) == 0) {
                        if (intval($grantsCount) !== 0) {
                            $groupsCount = 1;
                        }
                    } elseif (ceil($grantsCount / $groupsCount) > 30) {
                        $groupsCount++;
                    }
                }

                $this->grantsByCourses[$grant->courses]['items'][$grant->affiliate][$lang->slug]['groupsCount'] = $groupsCount;
                $this->grantsByCourses[$grant->courses]['items'][$grant->affiliate][$lang->slug]['busyCount'] = $busyCount;
                $this->grantsByCourses[$grant->courses]['items'][$grant->affiliate][$lang->slug]['count'] = $grantsCount;
                $this->grantsByCourses[$grant->courses]['items'][$grant->affiliate][$lang->slug]['countPerGroup'] = ($groupsCount == 0) ? 0 : floor($grantsCount / $groupsCount);
            }
        }
    }

    private function action2()
    {
        $faker = \Faker\Factory::create();
        Groups::where('staged', true)->delete();
        foreach ($this->grantsByCourses as $courses) {
            foreach ($courses['items'] as $affiliate) {
                foreach ($this->languages as $lang) {
                    $mod = ($affiliate[$lang->slug]['countPerGroup'] == 0) ? 0 : $affiliate[$lang->slug]['count'] % $affiliate[$lang->slug]['countPerGroup'];
                    for ($i = 1; $i <= $affiliate[$lang->slug]['groupsCount']; $i++) {
                        $q = 0;
                        if ($mod > 0) {
                            $q = 1;
                            $mod--;
                        }
                        $cpg = $affiliate[$lang->slug]['countPerGroup'] + $q;

                        $group = new Groups([
                            'slug' => $faker->sha256(),
                            'language' => $lang->id,
                            'affiliate' => $affiliate['id'],
                            'year' => $this->year,
                            'count_all' => $cpg,
                            'courses' => $courses['id'],
                            'trainer' => null,
                            'start_time' => null,
                            'end_time' => null,
                            'type' => $courses['type'],
                            'active' => false,
                            'staged' => true,
                            'status' => Groups::GROUP_STATUS_PLANNED,
                        ]);
                        $group->save();
                    }
                }
            }
        }

        $this->groups = Groups::where('staged', true)->get();
    }

    private function action3()
    {
//        $users = User::query();

        foreach ($this->grantsByCourses as $courses) {
//            if (($courses['type'] == Courses::COURSES_OFFLINE) or ($courses['type'] == Courses::COURSES_MIXED)) {
            foreach ($courses['items'] as $affiliate) {
                foreach ($this->languages as $lang) {
                    $langId = $lang->id;
                    $courseId = $courses['id'];
                    $affiliateId = $affiliate['id'];
//                    $groups = Groups::where('courses', $courseId)
//                        ->where('affiliate', $affiliateId)
//                        ->where('language', $langId)->where('staged', true)->get();
                    $this->grantsByCourses[$courseId]['items'][$affiliateId][$lang->slug]['tainers'] =
                        User::where('affiliate', $affiliateId)
                            ->whereHas('roles', function ($q) {
                                $q->where('id', Role::ROLE_TRAINER);
                            })
                            ->whereHas('languages', function ($q) use ($langId) {
                                $q->where('id', $langId);
                            })
                            ->whereHas('courses', function ($q) use ($courseId) {
                                $q->where('id', $courseId);
                            })
                            ->orderBy('hours_count', 'ASC')
                            ->get();

                }
            }
        }

//        dd($this->grantsByCourses);
    }

    private function action4()
    {
        $this->groupsAffected = 0;
        foreach ($this->grantsByCourses as $courses) {
            if (($courses['type'] == Courses::COURSES_OFFLINE) or ($courses['type'] == Courses::COURSES_MIXED)) {
                foreach ($courses['items'] as $affiliate) {
                    foreach ($this->languages as $lang) {
                        $langId = $lang->id;
                        $courseId = $courses['id'];
                        $affiliateId = $affiliate['id'];
                        $groups = Groups::where('courses', $courseId)
                            ->where('affiliate', $affiliateId)
                            ->where('language', $langId)->where('staged', true)->get();
                        foreach ($groups as $group) {
                            $trainer = User::whereHas('roles', function ($q) {
                                $q->where('id', Role::ROLE_TRAINER);
                            })
                                ->whereHas('languages', function ($q) use ($langId) {
                                    $q->where('id', $langId);
                                })
                                ->whereHas('courses', function ($q) use ($courseId) {
                                    $q->where('id', $courseId);
                                })
                                ->where('affiliate', $affiliateId)
                                ->where('hours_count', '<', 1960)
                                ->orderBy('hours_count', 'ASC')
                                ->first();
                            if ($trainer) {
                                $this->groupsAffected++;
                                $group->trainer = $trainer->id;
                                $trainer->hours_count = $trainer->hours_count + $group->coursesItem->course_length;
                                $trainer->save();
                                $group->save();
                            }
                        }
                    }
                }
            }
        }

        foreach ($this->grantsByCourses as $courses) {
            if ($courses['type'] == Courses::COURSES_ONLINE) {
                foreach ($courses['items'] as $affiliate) {
                    foreach ($this->languages as $lang) {
                        $langId = $lang->id;
                        $courseId = $courses['id'];
                        $affiliateId = $affiliate['id'];
                        $groups = Groups::where('courses', $courseId)
                            ->where('affiliate', $affiliateId)
                            ->where('language', $langId)->where('staged', true)->get();
                        foreach ($groups as $group) {
                            $trainer = User::whereHas('roles', function ($q) {
                                $q->where('id', Role::ROLE_TRAINER);
                            })
                                ->whereHas('languages', function ($q) use ($langId) {
                                    $q->where('id', $langId);
                                })
                                ->whereHas('courses', function ($q) use ($courseId) {
                                    $q->where('id', $courseId);
                                })
                                ->where('hours_count', '<=', (800 - $group->coursesItem->course_length))
                                ->orderBy('hours_count', 'ASC')
                                ->first();
                            if ($trainer) {
                                $this->groupsAffected++;
                                $group->trainer = $trainer->id;
                                $trainer->hours_count = $trainer->hours_count + $group->coursesItem->course_length;
                                $trainer->save();
                                $group->save();
                            }
                        }
                    }
                }
            }
        }
    }



    private function action5()
    {
        $trainers = User::where('hours_count','>',0)->orderBy('hours_count','DESC')->get();
//        $this->courses = Years::where('id', $this->year)->first()->courses()->get();
//        foreach ($this->courses as $course) {
//            $trainers = $course->groups->pluck('trainerItem')->toArray();
//            dd($trainers);
//        }
        foreach ($trainers as $trainer) {
//            $groups = $trainer->groups()->orderBy('start_time','ASC')->get()->toArray();
            $groups = $trainer->groups()->where('staged', true)->get();
            foreach ($groups as $group) {
                $success = true;
                $lastgroup = Groups::where('trainer',$trainer->id)
                                    ->orderBy('start_time','DESC')
                                    ->first();
                $start = $group->coursesItem->courseDates()->where('year',$this->year)->first()->start;
                $end = $group->coursesItem->courseDates()->where('year',$this->year)->first()->end;
                $firstDay = ($lastgroup->end_time) ? date('Y-m-d',strtotime($lastgroup->end_time.' +1 day')):$start;
                $days = ceil(($this->courseItem->course_length + $this->courseItem->practice_length)/8);
                $firstDay = new DateTime($firstDay);
                $firstDay->modify('next monday');
                while (Weekend::isWeekend($firstDay->format('Y-m-d'))) {
                    $firstDay->modify('+1 day');
                }

                if (strtotime($firstDay->format('Y-m-d')) > strtotime($end)) {
                    $success = false;
                }
                $lastday = $firstDay->format('Y-m-d');
//                $lastday = $firstDay;
                $i = 1;
                while($i < $days) {
                    $lastday = date('Y-m-d',strtotime($lastday.' +1 day'));
//                    $lastday->modify('+1 day');
                    if (!Weekend::isWeekend($lastday)) {
                        $i++;
                    }
                }
                if (strtotime($lastday) > strtotime($end)) {
                    $success = false;
                }
                if ($success) {
//                dd($days, $firstDay,$lastday);
                    $group->start_time = $firstDay;
                    $group->end_time = $lastday;
                    $group->active = true;
                    $group->staged = false;
                    $group->hash = $group->slug;
                    $group->slug = (($group->affiliate < 10) ? '0' . $group->affiliate : $group->affiliate) . '-' . (($group->courses < 10) ? '0' . $group->courses : $group->courses) . '-' . (($group->id < 10) ? '0' . $group->id : $group->id) . '-' . date('y', strtotime('01.01.' . $this->year));
                    $group->save();
                } else {
                    $group->delete();
                }

            }

            $this->recountHours($trainer);
        }

    }

    public function finish()
    {
        $this->redirectRoute('admin.schedule.index',[
            'start_time' => $this->year.'-01-01',
            'end_time' => $this->year.'-12-31',
        ]);
    }

    public function recountHours($trainer)
    {
        $hours = 0;
        foreach ($trainer->groups as $group) {
            $hours = $hours + $group->coursesItem->course_length;
        }
        $trainer->hours_count = $hours;
        $trainer->save();
    }

    public function action6()
    {
        $this->grantsByCourses = [];
        $this->grants = Grants::whereNotNull('courses')->whereNotNull('affiliate')->orderBy('affiliate', 'ASC')->orderBy('courses', 'ASC')->where('year', $this->year)->get();

        $this->groups = Groups::where('year', $this->year)->get();

        $this->languages = Languages::all();

        $this->courses = Years::where('id', $this->year)->first()->courses;
        $this->affiliates = Affiliates::all();
        foreach ($this->grants as $grant) {

            foreach ($this->languages as $lang) {


                if (array_key_exists($lang->slug, $grant->countsArray ?? []))  {
                    $grantsCount = intval($grant->countsArray[$lang->slug]);
                } else {
                    $grantsCount = 0;
                }

                $groupsCount = 0;
                $busyCount = 0;
                if (is_int($grantsCount)) {
                    $groups = Groups::where('courses', $grant->coursesItem->id)
                        ->where('staged', false)
                        ->where('affiliate', $grant->affiliateItem->id)
                        ->where('language', $lang->id);

                    $busyCount = $groups->sum('count_all');

                    $grantsCount = $grantsCount - $busyCount;
                    $groupsCount = (($grantsCount % 25) > 15) ? ceil($grantsCount / 25) : floor($grantsCount / 25);

                    if (intval($groupsCount) == 0) {
                        if (intval($grantsCount) !== 0) {
                            $groupsCount = 1;
                        }
                    } elseif (ceil($grantsCount / $groupsCount) > 30) {
                        $groupsCount++;
                    }
                }


                if ($grantsCount > 0) {
                    $this->grantsByCourses[$grant->courses]['items'][$grant->affiliate]['title'] = $grant->affiliateItem->title;
                    $this->grantsByCourses[$grant->courses]['items'][$grant->affiliate]['id'] = $grant->affiliateItem->id;
                    $this->grantsByCourses[$grant->courses]['title'] = $grant->coursesItem->kz_title;
                    $this->grantsByCourses[$grant->courses]['id'] = $grant->coursesItem->id;
                    $this->grantsByCourses[$grant->courses]['type'] = $grant->coursesItem->type;

                    $this->grantsByCourses[$grant->courses]['items'][$grant->affiliate][$lang->slug]['groupsCount'] = $groupsCount;
                    $this->grantsByCourses[$grant->courses]['items'][$grant->affiliate][$lang->slug]['busyCount'] = $busyCount;
                    $this->grantsByCourses[$grant->courses]['items'][$grant->affiliate][$lang->slug]['count'] = $grantsCount;
                    $this->grantsByCourses[$grant->courses]['items'][$grant->affiliate][$lang->slug]['countPerGroup'] = ($groupsCount == 0) ? 0 : floor($grantsCount / $groupsCount);
                }
            }
        }
    }

    public function updatedStep($value)
    {
        if ($value == 2) {
            $this->action1();
        } elseif ($value == 3) {
            $this->action2();
            $this->action3();
        } elseif ($value == 4) {
            $this->action4();
            $this->action3();
        } elseif ($value == 5) {
            $this->action5();
            $this->action6();
        } elseif ($value == 6) {
            $this->finish();
        }
    }

    public function render()
    {
        return view('livewire.schedule.schedule-generate-controller')->layout('layouts.admin');
    }
}
