<?php

namespace App\Http\Livewire\Admin;

use App\Models\Institutes;
use App\Models\Locations;
use Illuminate\Support\Facades\Gate;
use Livewire\Component;
use Symfony\Component\HttpFoundation\Response;
use function abort_if;
use function redirect;
use function view;

class InstitutesCreateController extends Component
{
    public $institute;
    public $oblasts;
    public $rayon;
    public $city;
    public $oblastItem;
    public $rayonItem;
    public $cityItem;
    public $oblastValue;
    public $rayonValue;
    public $cityValue;

    use \App\Traits\ValidateMessages;
    protected $rules = [
        'institute.title' => 'string|required',
        'institute.kz_title' => 'string|required',
        'institute.ru_title' => 'string|required',
        'institute.type' => 'integer|required',
        'cityItem' => 'integer|required',
        'rayonItem' => 'integer|required',
        'oblastItem' => 'required|integer',
        'institute.address' => 'string|nullable',
        'institute.type_org' => 'string|nullable',
        'institute.type_school' => 'string|nullable',
        'institute.parent_id' => 'nullable|integer',
        'institute.forms_id' => 'nullable|integer',
    ];


    public function updatedOblastItem($value)
    {
        if ($value) {
            $this->oblastValue = Locations::find($value);
        } else {

            $this->oblastValue = null;
            $this->rayonValue = null;
            $this->cityValue = null;
            $this->oblastItem = null;
            $this->rayonItem = null;
            $this->cityItem = null;
        }
    }


    public function updatedRayonItem($value)
    {

        if ($value) {
            $this->rayonValue = Locations::find($value);
        } else {
            $this->rayonItem = null;
            $this->rayonValue = null;
            $this->cityValue = null;
            $this->cityItem = null;
        }
    }

    public function updatedCityItem($value)
    {

        if ($value) {
            $this->cityValue = Locations::find($value);
        } else {
            $this->cityValue = null;
            $this->cityItem = null;
        }
    }


    public function mount($id = null)
    {
        abort_if(Gate::denies(\App\Models\Permission::PERMISSION_SUPERADMIN), Response::HTTP_FORBIDDEN, '403 Ошибка доступа ');
        if ($id) {
            $this->institute = Institutes::find($id);
            $this->oblastValue = $this->institute->oblastItem;
            $this->rayonValue = $this->institute->rayonItem;
            $this->cityValue = $this->institute->cityItem;
            $this->oblastItem = $this->institute->oblast;
            $this->rayonItem = $this->institute->rayon;
            $this->cityItem = $this->institute->city;
        } else {
            $this->institute = new Institutes();
        }
    }

    public function save()
    {
        $this->validate();
        $this->institute->oblast = $this->oblastItem;
        $this->institute->rayon = $this->rayonItem;
        $this->institute->city = $this->cityItem;

        $this->institute->save();
        return redirect()->route('admin.institutes.index',['type' => $this->institute->type]);
    }

    public function render()
    {
        $this->oblasts = Locations::where('type',Locations::LOCATION_OBLAST)->get();
        return view('livewire.institutes.institutes-create-controller')->layout('layouts.admin');
    }
}
