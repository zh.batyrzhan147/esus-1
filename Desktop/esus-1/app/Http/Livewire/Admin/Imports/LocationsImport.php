<?php

namespace App\Http\Livewire\Admin\Imports;

use App\Jobs\XlsImportJob;
use App\Models\Institutes;
use App\Models\XlsImports;
use Illuminate\Support\Facades\Gate;
use Livewire\Component;
use Livewire\WithFileUploads;
use Livewire\WithPagination;
use Symfony\Component\HttpFoundation\Response;

class LocationsImport extends Component
{
    use WithFileUploads;
    use WithPagination;

    public $ipp = 20;
    public $file;
    public $modal_add_new = false;

    use \App\Traits\ValidateMessages;
    public function rules()
    {
        return [
            'file' => 'required|file|max:1000000',
        ];
    }

    public function mount()
    {
        abort_if(Gate::denies(\App\Models\Permission::PERMISSION_SUPERADMIN), Response::HTTP_FORBIDDEN, '403 Ошибка доступа ');
    }

    public function render()
    {
        $imports = XlsImports::query();
        $imports = $this->applySearch($imports);

        return view('livewire.admin.imports.locations-import', [
            'imports' => $imports->paginate($this->ipp)
        ])->layout('layouts.admin');
    }

    private function applySearch(\Illuminate\Database\Eloquent\Builder $item)
    {
        $item = $item->where('model', XlsImports::IMPORTS_MODEL_LOCATION);

        return $item;
    }


    public function add_new()
    {
        $this->modal_add_new = true;
    }

    public function add_file()
    {
        $this->validate();
        $import = new XlsImports();
        $import->model = XlsImports::IMPORTS_MODEL_LOCATION;
        $import->file = $this->file->store('/' . date('Y') . '/' . date('m') . '/' . date('d'), 'public');
        $import->status = XlsImports::IMPORTS_STATUS_CREATED;
        $import->save();

        $this->modal_add_new = false;
    }


}
