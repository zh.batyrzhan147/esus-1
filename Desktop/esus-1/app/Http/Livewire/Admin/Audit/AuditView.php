<?php

namespace App\Http\Livewire\Admin\Audit;

use App\Models\AuditReports;
use App\Models\Languages;
use Illuminate\Support\Facades\Gate;
use Livewire\Component;
use Symfony\Component\HttpFoundation\Response;

class AuditView extends Component
{
    private $audit;
    private $languages;
    public function mount($id)
    {
        abort_if(Gate::denies(\App\Models\Permission::PERMISSION_SUPERADMIN), Response::HTTP_FORBIDDEN, '403 Ошибка доступа ');

        $this->audit = AuditReports::where('id', $id)->first();
        $this->languages = Languages::all();
    }
    public function render()
    {
        if ($this->audit->status == AuditReports::AUDIT_REPORT_STATUS_FINISHED) {
            if ($this->audit->type == AuditReports::AUDIT_REPORT_TYPE_COURSES) {
                return view('livewire.admin.audit.audit-view-courses', [
                    'audit' => json_decode($this->audit->data, true),
                    'year' => $this->audit->year,
                    'languages' => $this->languages,
                ])->layout('layouts.admin');
            } elseif($this->audit->type == AuditReports::AUDIT_REPORT_TYPE_GROUPS) {
                return view('livewire.admin.audit.audit-view-groups', [
                    'audit' => json_decode($this->audit->data, true),
                    'year' => $this->audit->year,
                    'languages' => $this->languages,
                ])->layout('layouts.admin');
            } elseif($this->audit->type == AuditReports::AUDIT_REPORT_TYPE_LISTENERS) {
                return view('livewire.admin.audit.audit-view-listeners', [
                    'audit' => json_decode($this->audit->data, true),
                    'year' => $this->audit->year,
                    'languages' => $this->languages,
                ])->layout('layouts.admin');
            }
        } else {
            return view('livewire.admin.audit.audit-not-ready', [
                'type' => $this->audit->type,
                'year' => $this->audit->year,
            ])->layout('layouts.admin');
        }
    }
}
