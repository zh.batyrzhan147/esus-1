<?php

namespace App\Http\Livewire\Admin\Courses;

use App\Models\Courses;
use App\Rules\mod8;
use Cocur\Slugify\Slugify;
use Livewire\Component;
use Livewire\WithFileUploads;

class CoursesEdit extends Component
{
    use WithFileUploads;

    public $price;
    public $course;
    public $kz_title;
    public $ru_title;
    public $slug;
    public $cid = '';
    public $rules = [
    ];

    public $file_1_listener;
    public function updatedFile1Listener()
    {
        $this->validate();
    }
    public $file_2_listener;
    public $file_1_teacher;
    public $file_2_teacher;
    public $offer_agreement;

    use \App\Traits\ValidateMessages;
    public function rules()
    {
        return [
            'price' => 'nullable|numeric',
            'kz_title' => 'required|string',
            'ru_title' => 'required|string',
            'slug' => 'required|string|unique:courses,slug'.$this->cid,
            'course.type' => 'integer',
            'course.grading' => '',
            'course.auto_generation' => ['required', 'boolean'],
            'course.course_length' => [
                'required','integer', new mod8()
            ],
            'course.practice_length' => [
                'required','integer', 
            ],
            'course.webinar_count' => ['nullable','integer'],
            'course.webinar_length' => '',
            'file_1_listener' => 'nullable|file|max:1000000',
            'file_2_listener' => 'nullable|file|max:1000000',
            'file_1_teacher' => 'nullable|file|max:1000000',
            'file_2_teacher' => 'nullable|file|max:1000000',
            'offer_agreement' => 'nullable|file|max:1000000',
//            'course.sdo_category' => '',
            'course.sdo_link' => 'nullable|string',
            'course.sdo_link_ru' => 'nullable|string',
            'course.sdo_link_en' => 'nullable|string',
            'course.kz_short_title' => 'nullable|string',
            'course.ru_short_title' => 'nullable|string',
            'course.en_short_title' => 'nullable|string',
//            'course.one_drive_link' => '',
        ];
    }


    public function mount($id = null)
    {
        if ($id) {
            $this->course = Courses::where('id', $id)->first();

            if ($this->course) {
                $this->cid = ','.$this->course->id;
                $this->kz_title = (old('kz_title'))? old('kz_title'):$this->course->kz_title;
                $this->ru_title = (old('ru_title'))? old('ru_title'):$this->course->ru_title;
                $this->slug = (old('slug'))? old('slug'):$this->course->slug;
                $this->price = old('price') ?? $this->course->price;
            } else {
                $this->kz_title = old('kz_title');
                $this->ru_title = old('ru_title');
                $this->slug = old('slug');
                $this->cid = '';
            }

        } else {
            $this->course = new Courses();
        }
    }

    public function generate()
    {
        $slugify = new Slugify();
        if ($this->kz_title) {
            $this->slug = substr($slugify->slugify($this->kz_title), 0, 255);
        } elseif ($this->ru_title) {
            $this->slug = substr($slugify->slugify($this->ru_title), 0, 255);
        }
    }

    public function save()
    {
        $this->validate();
        $this->course->kz_title = $this->kz_title;
        $this->course->ru_title = $this->ru_title;
        $this->course->slug = $this->slug;
        $this->course->price = $this->price;
        $this->course->save();

        $this->file_1_listener && $this->course->update([
            'file_1_listener' => $this->file_1_listener->store('/courses/'.$this->course->id,'public'),
        ]);
        $this->file_2_listener && $this->course->update([
            'file_2_listener' => $this->file_2_listener->store('/courses/'.$this->course->id,'public'),
        ]);
        $this->file_1_teacher && $this->course->update([
            'file_1_teacher' => $this->file_1_teacher->store('/courses/'.$this->course->id,'public'),
        ]);
        $this->file_2_teacher && $this->course->update([
            'file_2_teacher' => $this->file_2_teacher->store('/courses/'.$this->course->id,'public'),
        ]);
        $this->offer_agreement && $this->course->update([
            'offer_agreement' => $this->offer_agreement->store('/courses/'.$this->course->id,'public'),
        ]);

        $this->redirectRoute('admin.courses.index');
    }

    public function render()
    {
        return view('livewire.admin.courses.courses-edit')->layout('layouts.admin');
    }
}
