<?php

namespace App\Http\Livewire\Admin\Statements;

use App\Models\StatementsMessages;
use Illuminate\Support\Facades\Gate;
use Livewire\Component;
use Livewire\WithPagination;
use Symfony\Component\HttpFoundation\Response;

class MessagesIndex extends Component
{
    use WithPagination;
    private $statements;
    public $ipp = 20;
    public $search;
    public $affiliate;

    function mount()
    {
        abort_if(Gate::denies(\App\Models\Permission::PERMISSION_SUPERADMIN), Response::HTTP_FORBIDDEN, '403 Ошибка доступа ');
    }

    protected  $rules = [
        'search' => 'string'
    ];

    public function applyFilter()
    {
        $this->resetPage();
    }

    public function render()
    {
        $this->statements = StatementsMessages::query();
        $this->statements = $this->applySearch($this->statements);

        return view('livewire.admin.statements.messages-index', [
            'statements' => $this->statements->paginate($this->ipp),
        ])->layout('layouts.admin');
    }

    private function applySearch(\Illuminate\Database\Eloquent\Builder $item)
    {
        if ($this->search) {
            $search = $this->search;
//            $item = $item->where('')
            $item = $item->whereHas('userItem', function ($q) use ($search) {
                $q->where('iin', 'LIKE', '%'.$search.'%');
            });
        }

        if ($this->affiliate) {
            $item = $item->where('affiliate', $this->affiliate);
        }
//        $item = $item->with('userItem', 'groupItem', 'affiliateItem', 'statementItem');
        return $item;
    }
}
