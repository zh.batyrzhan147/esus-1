<?php

namespace App\Http\Livewire\Admin;

use App\Models\Weekends;
use Illuminate\Support\Facades\Gate;
use Livewire\Component;
use Livewire\WithPagination;
use Symfony\Component\HttpFoundation\Response;
use function abort_if;
use function view;

class WeekendsController extends Component
{
    use WithPagination;

    public $modal_add_new = false;
    public Weekends $newday;

    use \App\Traits\ValidateMessages;
    protected $rules = [
        'newday.day' => 'required|unique:weekends,day|date',
        'newday.type' => 'required|integer',
    ];

    public function mount()
    {
        abort_if(Gate::denies(\App\Models\Permission::PERMISSION_SUPERADMIN), Response::HTTP_FORBIDDEN, '403 Ошибка доступа ');
    }

    public function add_new()
    {
        $this->newday = new Weekends();
        $this->modal_add_new = true;
    }

    public function store_new()
    {
        $this->validate();
        $this->newday->save();
        $this->modal_add_new = false;
        $this->emitUp('triggerSuccessCreate');
    }

    public function destroy($id)
    {
        Weekends::find($id)->delete();
    }

    public function render()
    {
        $weekends = Weekends::orderBy('day', 'DESC')->paginate();
        return view('livewire.weekends.weekendsList', [
            'weekends' => $weekends,
        ])->layout('layouts.admin');
    }
}
