<?php

namespace App\Http\Livewire\Admin;

use App\Models\Locations;
use Illuminate\Support\Facades\Gate;
use Livewire\Component;
use Symfony\Component\HttpFoundation\Response;
use function abort_if;
use function view;

class LocationsSelectComponent extends Component
{
    public $location;
    public $type;
    public $parent1;
    public $parent2;
    public $parentList = [];
    public $parentList2 = [];
    public $showparentList = false;
    public $showparentListobl = false;
    public $showparentList2 = false;

    public function mount() {
        abort_if(Gate::denies(\App\Models\Permission::PERMISSION_SUPERADMIN), Response::HTTP_FORBIDDEN, '403 Ошибка доступа ');

        if ($this->location) {
            $this->type = $this->location->type;

            if ($this->location->parent) {
                $this->parent1 = Locations::find($this->location->parent);
            }
            if (($this->type == Locations::LOCATION_AUL) and ($this->parent1)) {
                $this->showparentList2 = true;
                $this->showparentListobl = true;
                $this->parent2 = Locations::find($this->parent1->parent);
                $this->parentList = Locations::where('type', Locations::LOCATION_OBLAST)->get();
                $this->parentList2 = Locations::where('type', Locations::LOCATION_RAYON)->get();
            } elseif (($this->type == Locations::LOCATION_RAYON)) {
                $this->parentList = Locations::where('type', Locations::LOCATION_OBLAST)->get();
                $this->showparentList = true;
            }
        }
    }
    public function changeobl($value) {
        $this->parentList2 = Locations::where('parent', $value)->get();
//        @dd($this->parentList2);
        if (count($this->parentList2)>0) {
            $this->showparentList2 = true;
        } else {
            $this->showparentList2 = false;
        }
    }
    public function change($value) {

        if ($value == Locations::LOCATION_AUL) {
//            $this->parent2 = Locations::find($this->parent1->parent);
            $this->parentList = Locations::where('type', Locations::LOCATION_OBLAST)->get();
            $this->showparentListobl = true;
            $this->showparentList = false;
            $this->showparentList2 = false;
//            $this->parentList2 = Locations::where('type', Locations::LOCATION_RAYON)->get();
        } elseif (($value == Locations::LOCATION_RAYON)) {
            $this->parentList2 = [];
            $this->parentList = Locations::where('type', Locations::LOCATION_OBLAST)->get();
            $this->showparentListobl = false;
            $this->showparentList = true;
            $this->showparentList2 = false;
        } else {
            $this->parentList2 = [];
            $this->parentList = [];
            $this->showparentListobl = false;
            $this->showparentList = false;
            $this->showparentList2 = false;
        }

//        if ($value == Locations::LOCATION_AUL) {
//            $this->parentList = Locations::where('type', Locations::LOCATION_OBLAST)->get();
//            $this->parentList2 = Locations::where('type', Locations::LOCATION_RAYON)->get();
////            @dd($this->parentList2);
//            $this->showParentList2 = true;
//        } elseif ( $value == Locations::LOCATION_GOROD) {
//            $this->parentList = Locations::where('type', Locations::LOCATION_OBLAST)->get();
//            $this->showParentList2 = false;
//        } elseif ( $value == Locations::LOCATION_RAYON) {
//            $this->parentList = Locations::where('type', Locations::LOCATION_OBLAST)->get();
//            $this->showParentList2 = false;
//        } else {
//            $this->parentList = [];
//            $this->showParentList2 = false;
//        }
    }
    public function render()
    {
        return view('livewire.locations-select-component')->layout('layouts.admin');
    }
}
