<?php

namespace App\Http\Livewire\Admin;

use App\Models\Role;
use App\Models\User;
use Illuminate\Support\Facades\Gate;
use Livewire\Component;
use Livewire\WithPagination;
use Symfony\Component\HttpFoundation\Response;
use function abort_if;
use function view;

class UsersController extends Component
{
    use WithPagination;
    private $users;
    public $search;
    public $roles;
    protected $trackComponentPath;
    protected $queryString = [
        'search' => ['except' => ''],
    ];

    use \App\Traits\ValidateMessages;
    protected $rules = [
        'search' => 'nullable|string',
    ];

    public function mount()
    {
        abort_if(Gate::denies(\App\Models\Permission::PERMISSION_SUPERADMIN), Response::HTTP_FORBIDDEN, '403 Ошибка доступа ');
        $this->roles = Role::all();
    }

    public function applyFilter()
    {
        $this->resetPage();

    }

    public function attachRoleUser($rolesID = null, $userID = null)
    {
        if ((!$rolesID) or (!$userID)) return;

        $user = User::where('id',$userID)->first();
        if (!$user) return;
        $user->roles()->detach($rolesID);
        $user->roles()->attach($rolesID);
    }


    public function dettachRoleUser($rolesID = null, $userID = null)
    {
        if ((!$rolesID) or (!$userID)) return;

        $user = User::where('id',$userID)->first();
        if (!$user) return;
        $user->roles()->detach($rolesID);
    }



    private function applySearch(\Illuminate\Database\Eloquent\Builder $item): \Illuminate\Database\Eloquent\Builder
    {
        $item = $item->where('iin', '!=', 'admin');
        if ($this->search) {
            $search = $this->search;
            $item = $item->where(function ($query) use ($search) {
                $searchItems = explode(" ", preg_replace('/\s+/', ' ', trim($search)));
                foreach ($searchItems as $text) {
                    $query->orWhere('iin', 'LIKE', '%' . $text . '%')
                        ->orWhere('name', 'LIKE', '%' . $text . '%')
                        ->orWhere('last_name', 'LIKE', '%' . $text . '%')
                        ->orWhere('patronymic', 'LIKE', '%' . $text . '%');
                }

            });
        }

        return $item;
    }

    public function destroy($id)
    {
        User::find($id)->delete();
    }

    public function render()
    {
        $this->users = User::query();
        $this->users = $this->applySearch($this->users);
        return view('livewire.users.users-controller',[
            'users' => $this->users->paginate(),
        ])->layout('layouts.admin');
    }
}
