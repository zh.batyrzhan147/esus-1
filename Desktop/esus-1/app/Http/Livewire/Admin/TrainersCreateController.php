<?php

namespace App\Http\Livewire\Admin;

use App\Models\Role;
use App\Models\User;
use Illuminate\Support\Facades\Gate;
use Livewire\Component;
use Symfony\Component\HttpFoundation\Response;
use function abort_if;
use function bcrypt;
use function redirect;
use function view;

class TrainersCreateController extends Component
{
    public $iin;
    public $trainer;
    public $new = null;

    use \App\Traits\ValidateMessages;
    protected $rules = [
        'iin' => 'required|digits:12',
        'trainer.iin' => 'digits:12|required|unique:users,iin',
        'trainer.name' => 'string|required',
        'trainer.last_name' => 'string|required',
        'trainer.email' => 'email|required|unique:users,email',
        'trainer.affiliate' => 'nullable',
    ];

    public function mount()
    {
        abort_if(Gate::denies(\App\Models\Permission::PERMISSION_SUPERADMIN), Response::HTTP_FORBIDDEN, '403 Ошибка доступа ');
    }

    public function search()
    {
        $this->validate([
            'iin' => 'required|digits:12'
        ]);
        $trainer = User::where('iin',$this->iin)->where('iin', '!=', 'admin')->first();
        if ($trainer) {
            return redirect()->route('admin.trainers.edit',['id' => $trainer->id])->with('user_for_trainer_found', 'Пользователь найден, нажмите сохранить для добавления.');
//            $this->trainer = $trainer;
//            $this->new = false;
        } else {
            $this->trainer = new User();
            $this->trainer->iin = $this->iin;
            $this->new = true;
        }
    }

    public function save()
    {
        $this->validate();
        $this->trainer->password = bcrypt(random_bytes(10));
        $this->trainer->save();
        $this->trainer->roles()->syncWithoutDetaching(Role::ROLE_TRAINER);
        $this->redirectRoute('admin.trainers.edit',['id' => $this->trainer->id]);
    }
    public function render()
    {
        return view('livewire.trainers.trainers-create-controller')->layout('layouts.admin');
    }
}
