<?php

namespace App\Http\Livewire\Admin;

use App\Models\Institutes;
use App\Models\Locations;
use Illuminate\Support\Facades\Gate;
use Livewire\Component;
use Livewire\WithPagination;
use Symfony\Component\HttpFoundation\Response;
use function abort_if;
use function view;

class InstitutesController extends Component
{
    use WithPagination;

    public $type;
    public $paginationCount = 10;

    public function  queryString() {
      return [
          'paginationCount' => [ 'except' => 10]
            ];
    }

    public $obl;
    public $oblItem;
    public $raion;
    public $raionItem;
    public $city;
    public $cityItem;

    public function updatedObl($value)
    {
        $this->oblItem = Locations::where('type',Locations::LOCATION_OBLAST)->where('id',$value)->first();
        $this->raionItem = null;
        $this->raion = null;
        $this->cityItem = null;
        $this->city = null;
        $this->applyFilter();
    }

    public function updatedRaion($value)
    {
        $this->raionItem = Locations::where('type',Locations::LOCATION_RAYON)->where('id',$value)->first();;
        $this->cityItem = null;
        $this->city = null;
        $this->applyFilter();
    }

    public function updatedCity($value)
    {
        $this->cityItem = Locations::where('type',Locations::LOCATION_AUL)->where('id',$value)->first();
        $this->applyFilter();
    }

    public function updatedType()
    {
        $this->applyFilter();
    }


    public function applyFilter()
    {
        $this->resetPage();
    }


    public function mount()
    {
        abort_if(Gate::denies(\App\Models\Permission::PERMISSION_SUPERADMIN), Response::HTTP_FORBIDDEN, '403 Ошибка доступа ');
    }

    public function render()
    {
        $intitutes = Institutes::query();
        $intitutes = $this->applySearch($intitutes);
        $oblasti = Locations::where('type',Locations::LOCATION_OBLAST)->get();
        return view('livewire.institutes.institutes-controller',
        [
            'institutes' => $intitutes->paginate($this->paginationCount),
            'oblasti' => $oblasti,
        ])->layout('layouts.admin');
    }

    private function applySearch(\Illuminate\Database\Eloquent\Builder $item)
    {
        if ($this->type) {
            $item = $item->where('type', $this->type);
        }
        if ($this->oblItem) {
            $item = $item->where('oblast', $this->oblItem->id);
        }
        if ($this->raionItem) {
            $item = $item->where('rayon', $this->raionItem->id);
        }
        if ($this->cityItem) {
            $item = $item->where('city', $this->cityItem->id);
        }
        $item = $item->with('cityItem')->with('rayonItem')->with('oblastItem');
        return $item;
    }
}
