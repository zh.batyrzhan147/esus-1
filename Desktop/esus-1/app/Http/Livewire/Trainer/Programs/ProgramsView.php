<?php

namespace App\Http\Livewire\Trainer\Programs;

use App\Models\Courses;
use Livewire\Component;

class ProgramsView extends Component
{
    public $program;
    public function mount($id = null)
    {
        if ($id)
        {
            $this->program = Courses::where('id', $id)->first();
        }
    }
    public function render()
    {
        return view('livewire.trainer.programs.programsView');
    }
}
