<?php

namespace App\Http\Livewire\Trainer\Groups;

use App\Exports\StatementsExport;
use App\Models\Courses;
use App\Models\ExcludedUsers;
use App\Models\Groups;
use App\Models\Orders;
use App\Models\Statements;
use App\Models\User;
use App\Traits\ValidateMessages;
use Illuminate\Support\Facades\Notification;
use Livewire\Component;
use Maatwebsite\Excel\Facades\Excel;

class GroupsView extends Component
{
    public $slug;
    public $group;
    public $statements;
    protected $listeners = ['setStatementStatus', 'setStatementAllStatus'];
    public $showUserDetails = false;
    public $userModal;
    public $userStatus;
    public $typeModal;
    public $message;
    public $showmessageform = false;
    public $zoompopup = false;
    public $zoomLink;

    public function setStatementStatus($groupId, $userId, $status)
    {
        Statements::updateOrCreate(
            [
                'group' => $groupId,
                'user' => $userId,
                'available_at' => date('Y-m-d', strtotime('now')),
            ],
            ['status' => $status]
        );
        $this->statements[$userId] = $status;
    }

    public function setStatementAllStatus($status)
    {
        foreach ($this->group->users as $user) {
            Statements::updateOrCreate(
                [
                    'group' => $this->group->id,
                    'user' => $user->id,
                    'available_at' => date('Y-m-d', strtotime('now')),
                ],
                ['status' => $status]
            );
            $this->statements[$user->id] = $status;
        }
    }



    public function notify($id)
    {
        $user = User::where('id', $id)->first();

        Notification::send($user, new \App\Notifications\Statements('heellooo'));
    }

    public function accept($id)
    {
        $this->group->users_sb()->detach($id);
        $this->group->users()->detach($id);
        $this->group->users()->attach($id);
        $user = User::where('id', $id)->first();
        $user->last_group_slug = str_pad(($this->group->language ?? 0), 2, '0', STR_PAD_LEFT).'-'.($this->group->slug ?? '');
        $user->save();
        Notification::send($user, new \App\Notifications\GroupAcceptNotification($this->group));
        $this->showUserDetails = false;
    }

    public function payment($id)
    {
        $user = User::where('id', $id)->first();

        $this->group->users_sb()->detach($id);
        $this->group->users()->detach($id);
        $this->group->users_sb()->attach($id, ['status' => Groups::GROUPS_USER_PAYMENT]);

        $orders = Orders::whereUserId($id)->whereCanceled(false)->whereHasMorph('target', Groups::class, function ($query) {
            $query->whereId($this->group->id);
        });

        if ($orders->count() < 1) {
            $order = new Orders();
            $order->total = $this->group->coursesItem->price;
            $order->user_id = $id;
            $order->target()->associate($this->group);
            $order->save();

            Notification::send($user, new \App\Notifications\GroupPaymentNotification($this->group, $order));
        }
    }

    public function showmessage()
    {
        $this->showmessageform = true;
    }
    use ValidateMessages;
    public function rework($id)
    {
        $this->validate([
            'message' => 'required|string'
        ]);
        if ($this->group->status == Groups::GROUP_STATUS_FORMED) {
            $this->group->status = Groups::GROUP_STATUS_PLANNED;
            $this->group->save();
        }
        $this->group->users_sb()->detach($id);
        $this->group->users()->detach($id);
        $this->group->users_sb()->attach($id, ['status' => Groups::GROUPS_USER_REWORK]);
        $user = User::where('id', $id)->first();
        Notification::send($user, new \App\Notifications\ProfileDeclineNotification($this->message,$this->group));
        $this->showUserDetails = false;
        $this->showmessageform = false;
    }

    public function showUserModal(User $user, $type)
    {
        $this->showUserDetails = true;
        $this->userModal = $user;
        $this->typeModal = ($type == 1);
    }

    public function mount($slug = null)
    {
        if ($slug) {
            $this->slug = $slug;
        } else {
            abort(404);
        }
    }

    public function changeGroupStatus($status)
    {
        if ($status == Groups::GROUP_STATUS_PLANNED) {
            if ($this->group->users()->count() != $this->group->count_all) {
                $this->emit('showMessage','У Вас не верное количество слушателей!');
            } else {
                $this->setOAuth2Group();
                $this->group->status = Groups::GROUP_STATUS_FORMED;
                $this->group->save();
            }
        } elseif ($status == Groups::GROUP_STATUS_FORMED) {
            $this->group->users_sb()->detach();
            foreach ($this->group->users as $user) {
                $user->last_group_slug = str_pad(($this->group->language ?? 0), 2, '0', STR_PAD_LEFT).'-'.($this->group->slug ?? '');
                $user->save();
            }
            $this->group->status = Groups::GROUP_STATUS_INPROGRESS;
            $this->group->save();
        } elseif ($status == Groups::GROUP_STATUS_INPROGRESS) {
            if ($this->group->coursesItem->grading) {
                $this->group->status = Groups::GROUP_STATUS_FINISHED_TESTING;
            } else {
                $this->group->status = Groups::GROUP_STATUS_FINISHED;
            }
            $this->group->save();
        } elseif ($status == Groups::GROUP_STATUS_FINISHED) {
        }

    }

    public function setOAuth2Group()
    {
        auth()->user()->trainer_oauth_group = $this->getOAuth2Group();
        auth()->user()->save();
    }

    public function getOAuth2Group()
    {
        return 'T-'.str_pad(($this->group->language ?? 0), 2, '0', STR_PAD_LEFT).'-'.$this->group->slug;
    }

    public function recommendToCert($id)
    {
        if (array_key_exists($id,$this->statements)){
            $this->group->users_sb()->detach($id);
        } else {
            $this->group->users_sb()->detach($id);
//        $this->group->users()->detach($id);
            $this->group->users_sb()->attach($id, ['status' => Groups::GROUPS_USER_CERT]);
        }
    }

    public function getAllStatements()
    {
        return Excel::download(new StatementsExport($this->group), $this->group->slug.'.xlsx');
    }

    public function editZoom()
    {
        $this->zoompopup = true;
    }

    public function saveZoomLink()
    {
        $this->validate([
            'zoomLink' => 'required|string'
        ]);
        $this->group->zoom_link = $this->zoomLink;
        $this->group->save();
        $this->zoompopup = false;
    }

    public function removeZoomLink()
    {
        $this->group->zoom_link = null;
        $this->group->save();
        $this->zoompopup = false;
    }

    public function excludeUser($id)
    {
        if ($this->group->status == Groups::GROUP_STATUS_INPROGRESS) {
            $this->validate([
                'message' => 'required|string'
            ]);
            $this->group->users_sb()->detach($id);
            $this->group->users()->detach($id);
            $excude = new ExcludedUsers([
                'user' => $id,
                'group' => $this->group->id,
                'message' => $this->message,
                'excluded_by' => auth()->user()->id,
            ]);
            $excude->save();
//            $this->group->users_sb()->attach($id, ['status' => Groups::GROUPS_USER_REWORK]);
//            $this->group->status = Groups::GROUP_STATUS_PLANNED;
//            $this->group->save();
//            $user = User::where('id', $id)->first();
//            Notification::send($user, new \App\Notifications\Statements($this->message));
        }
            $this->showUserDetails = false;
            $this->showmessageform = false;
    }
    public function render()
    {
        $this->group = Groups::where('slug', $this->slug)->first();
        if ($this->group->trainer != auth()->user()->id) abort(403);
        if (!$this->group) abort(404);
        if ($this->group->status == Groups::GROUP_STATUS_INPROGRESS)
            $this->statements = Statements::where('group', $this->group->id)->where('available_at', date('Y-m-d', strtotime('now')))->get()->pluck('status', 'user')->toArray();
        if ($this->group->status == Groups::GROUP_STATUS_FINISHED or $this->group->status == Groups::GROUP_STATUS_FINISHED_TESTING) {
            $this->statements = $this->group->users_sb->pluck('pivot.status', 'id')->toArray();
        }

        $statuses = $this->group->users_sb->pluck('pivot.status')->toArray();

        if (count($statuses) < 1) {
            $statuses = $this->group->users->pluck('pivot.status')->toArray();
        }

        $this->userStatus = end($statuses);

        return view('livewire.trainer.groups.groupsView')->layout('layouts.user');
    }
}
