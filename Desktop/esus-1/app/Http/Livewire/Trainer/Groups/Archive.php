<?php

namespace App\Http\Livewire\Trainer\Groups;

use App\Models\Groups;
use Illuminate\Support\Facades\Auth;
use Livewire\Component;

class Archive extends Component
{
    public $groups;

    public function render()
    {
        $this->groups = Auth::user()->groups()
            ->whereIn('status',[Groups::GROUP_STATUS_FINISHED,Groups::GROUP_STATUS_FINISHED_TESTING,Groups::GROUP_STATUS_ARCHIVE,Groups::GROUP_STATUS_CERTIFICATED])
            ->orderBy('start_time', 'ASC')
            ->with('coursesItem')
            ->get();
        return view('livewire.trainer.groups.archive')->layout('layouts.user');
    }
}
