<?php

namespace App\Http\Livewire\Trainer\Groups;

use App\Models\Groups;
use Illuminate\Support\Facades\Auth;
use Livewire\Component;

class Active extends Component
{
    public $groups;

    public function render()
    {
        $this->groups = Auth::user()->groups()
            ->whereIn('status',[Groups::GROUP_STATUS_PLANNED,Groups::GROUP_STATUS_FORMED,Groups::GROUP_STATUS_INPROGRESS])
            ->orderBy('start_time', 'ASC')
            ->with('coursesItem')
            ->get();
        return view('livewire.trainer.groups.active')->layout('layouts.user');
    }
}
