<?php

namespace App\Http\Livewire\Trainer;

use App\Models\Groups;
use Illuminate\Support\Facades\Auth;
use Livewire\Component;

class TrainerProfile extends Component
{
    public $groups;

    public function mount()
    {
        $this->groups = Auth::user()->groups;
    }

    public function render()
    {
        return view('livewire.trainer.trainer-profile')->layout('layouts.user');
    }
}
