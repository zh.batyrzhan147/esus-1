<?php

namespace App\Http\Livewire;

use Illuminate\Support\Facades\Cookie;
use Livewire\Component;

class SetLocale extends Component
{
    public function changeLocaleInRoute(\Illuminate\Routing\Route $route, $newLocale)
    {
        Cookie::queue('locale', $newLocale);
        app()->setLocale($newLocale);
        return redirect(request()->header('Referer'));
    }


    public function render()
    {
        return view('livewire.set-locale');
    }
}
