<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Gate;

class UpdateAffiliateRequest extends FormRequest
{
    use \App\Traits\ValidateMessages;
    public function rules()
    {
        return [
            'title' => [
                'required', 'string',
            ],
            'director' => [
                'nullable', 'string',
            ],
            'address' => [
                'nullable', 'string',
            ],
            'phone' => [
                'nullable', 'string',
            ],
            'email' => [
                'nullable', 'string',
            ]
        ];
    }

    public function authorize()
    {
        return Gate::allows(\App\Models\Permission::PERMISSION_SUPERADMIN);
    }
}
