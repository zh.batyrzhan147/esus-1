<?php

namespace App\Http\Requests;

use App\Rules\mod8;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Gate;

class NewCoursesRequest extends FormRequest
{
    use \App\Traits\ValidateMessages;
    public function rules()
    {
        return [
            'kz_title' => [
                'required', 'string',
            ],
            'ru_title' => [
                'required', 'string',
            ],
            'slug' => [
                'required', 'string',
                'unique:courses,slug',
            ],
            'type' => [
                'integer'
            ],
            'grading' => [

            ],
            'course_length' => [
                'required','integer', new mod8()
            ],
            'developer' => [

            ],
            'file_1_listener' => [],
            'file_2_listener' => [],
            'file_1_teacher' => [],
            'file_2_teacher' => [],
            'webinar_count' => [
                'nullable','integer'
            ],
            'webinar_length' => [
                'integer', 'nullable', new mod8()
            ],
            'sdo_category' => [],
            'sdo_link' => [],
            'one_drive_link' => [],
            'auto_generation' => [
                'required', 'boolean'
            ]
        ];
    }

    public function authorize()
    {
        return Gate::allows(\App\Models\Permission::PERMISSION_SUPERADMIN);
    }
}
