<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Gate;

class UpdateNationalityRequest extends FormRequest
{
    use \App\Traits\ValidateMessages;
    public function rules()
    {
        return [
            'kz_title' => [
                'required', 'string',
            ],
            'ru_title' => [
                'required', 'string',
            ]
        ];
    }

    public function authorize()
    {
        return Gate::allows(\App\Models\Permission::PERMISSION_SUPERADMIN);
    }
}
