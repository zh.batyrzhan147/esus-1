<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Gate;

class UpdateProgramsRequest extends FormRequest
{
    use \App\Traits\ValidateMessages;
    public function rules()
    {
        return [
            'title' => [
                'string',
                'required',
            ],
            'slug' => [
                'string',
                'required',
            ],
            'start' => [
                'date',
                'nullable'
            ],
            'end' => [
                'date',
                'nullable'
            ],
            'year' => [
                'integer',
                'required',
            ],
            'courses.*' => [
                'integer',
            ],
            'courses'   => [
                'required',
                'array',
            ],
        ];
    }

    public function authorize()
    {
        return Gate::allows(\App\Models\Permission::PERMISSION_SUPERADMIN);
    }
}
