<?php

namespace App\Actions\Fortify;
use Laravel\Fortify\Contracts\RegisterResponse as RegisterResponseContract;

class OwnRegisterResponse implements RegisterResponseContract
{

    public function toResponse($request)
    {

        return redirect()->route('login')->with('registered',true);
    }

}

