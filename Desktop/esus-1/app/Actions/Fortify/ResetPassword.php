<?php

namespace App\Actions\Fortify;

use App\Models\User;
use Illuminate\Contracts\Auth\PasswordBroker;
use Illuminate\Contracts\Support\Responsable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Password;
use Laravel\Fortify\Contracts\FailedPasswordResetLinkRequestResponse;
use Laravel\Fortify\Contracts\SuccessfulPasswordResetLinkRequestResponse;
use Laravel\Fortify\Fortify;

class ResetPassword extends Controller
{

    public function mask($str, $first, $last) {
        $len = strlen($str);
        $toShow = $first + $last;
        return substr($str, 0, $len <= $toShow ? 0 : $first).str_repeat("*", $len - ($len <= $toShow ? 0 : $toShow)).substr($str, $len - $last, $len <= $toShow ? 0 : $last);
    }

    public function mask_email($email) {
        $mail_parts = explode("@", $email);
        $domain_parts = explode('.', $mail_parts[1]);

        $mail_parts[0] = $this->mask($mail_parts[0], 2, 1); // show first 2 letters and last 1 letter
        $domain_parts[0] = $this->mask($domain_parts[0], 2, 1); // same here
        $mail_parts[1] = implode('.', $domain_parts);

        return implode("@", $mail_parts);
    }


    /**
     * Send a reset link to the given user.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Contracts\Support\Responsable
     */
    public function store(Request $request): Responsable
    {
        $request->validate([
            'iin' => 'required|numeric|digits:12',
//            Fortify::email() => 'required|email'
        ]);

        $user = User::where('iin',$request->only('iin'))->first();

        if (!$user) {
//            if (!$user->email === $request->email) {
//                return app(FailedPasswordResetLinkRequestResponse::class, ['status' => Password::INVALID_USER]);
//            }
//        } else {
            return app(FailedPasswordResetLinkRequestResponse::class, ['status' => Password::INVALID_USER]);
        } else {

        }
        // We will send the password reset link to this user. Once we have attempted
        // to send the link, we will examine the response then see the message we
        // need to show to the user. Finally, we'll send out a proper response.
//        $status = $this->broker()->sendResetLink(
////            $request->only(Fortify::email())
//            ['email' => $user->email]
//        );
//        $status = str_replace(':email',$user->email,$status);
//        dd($status);
        $user->resetPassword();
        $status = Password::RESET_LINK_SENT;
        $email = $this->mask_email($user->email);
        return $status == Password::RESET_LINK_SENT
            ? app(SuccessfulPasswordResetLinkRequestResponse::class, ['status' => 'Новый пароль отправлен на почту '.$email])
            : app(FailedPasswordResetLinkRequestResponse::class, ['status' => $status]);
    }

    /**
     * Get the broker to be used during password reset.
     *
     * @return \Illuminate\Contracts\Auth\PasswordBroker
     */
    protected function broker(): PasswordBroker
    {
        return Password::broker(config('fortify.passwords'));
    }

}
