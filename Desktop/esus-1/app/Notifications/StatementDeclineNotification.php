<?php

namespace App\Notifications;

use App\Models\Statements;
use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class StatementDeclineNotification extends Notification
{
    use Queueable;

    private $title = 'Уважительная причина по пропуску отклонена';
    private $message;
    private $statements;
    private $user;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($message, Statements $statements, User $user)
    {
        $this->message = $message;
        $this->statements = $statements;
        $this->user = $user;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database'];
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        $text = '';
        $text = $text.'<div class="w-1/2 flex flex-col"><span class="w-full flex text-user-gray text-sm mb-2">Группа:</span><div class="w-full flex font-semibold text-main-black text-base">'. $this->statements->groupItem->slug.'</div></div>';
        $text = $text.'<div class="w-1/2 flex flex-col"><span class="w-full flex text-user-gray text-sm mb-2">Дата:</span><div class="w-full flex font-semibold text-main-black text-base">'. date('d.m.Y',strtotime($this->statements->available_at)).'</div></div>';
        $text = $text.'<div class="w-full text-lg lg:text-xl pt-4">Инициатор: '. $this->user->fio.'</div>';
        if ($this->message) {
            $text = $text . '<div class="w-full bg-gray-bg rounded-lg p-4 flex flex-col trainer-message relative"><div class="w-full flex text-user-gray text-sm mb-1">Сообщение от инициатора:</div><div class="w-full flex font-normal text-main-black text-base">' . $this->message . '</div></div>';
        }
        return [
            'title' => $this->title,
            'message' => $text,
        ];
    }
}
