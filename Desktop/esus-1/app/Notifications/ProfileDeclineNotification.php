<?php

namespace App\Notifications;

use App\Models\Groups;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class ProfileDeclineNotification extends Notification
{
    use Queueable;

    private $title = 'Данные вашего профиля не согласованы';
    private $message;
    private $group;
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($message, Groups $group)
    {
        $this->message = $message;
        $this->group = $group;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database'];
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        $text = '<div class="w-full text-lg lg:text-xl font-bold">'. $this->group->coursesItem->ru_title.'</div>';
        $text = $text.'<div class="w-1/2 flex flex-col"><span class="w-full flex text-user-gray text-sm mb-2">Группа:</span><div class="w-full flex font-semibold text-main-black text-base">'. $this->group->slug.'</div></div>';
        $text = $text.'<div class="w-1/2 flex flex-col"><span class="w-full flex text-user-gray text-sm mb-2">Тренер:</span><div class="w-full flex font-semibold text-main-black text-base">'. $this->group->trainerItem->fio.'</div></div>';
        $text = $text.'<div class="w-full bg-gray-bg rounded-lg p-4 flex flex-col trainer-message relative"><div class="w-full flex text-user-gray text-sm mb-1">Сообщение от тренера:</div><div class="w-full flex font-normal text-main-black text-base">'. $this->message.'</div></div>';
        return [
            'title' => $this->title,
            'message' => $text,
        ];
    }
}
