<?php

namespace App\Notifications;

use App\Models\Groups;
use App\Models\Orders;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class GroupPaymentNotification extends Notification
{
    use Queueable;

    private $title = 'Оплатите доступ';
    private $message;
    private $group;
    private $order;


    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(Groups $group, Orders $order)
    {
        $this->group = $group;
        $this->order = $order;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database'];
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        $this->message = '<div class="w-full text-lg lg:text-xl font-bold">'. $this->group->coursesItem->ru_title.'</div>';
        $this->message = $this->message.'<div class="w-1/2 flex flex-col"><span class="w-full flex text-user-gray text-sm mb-2">Группа:</span><div class="w-full flex font-semibold text-main-black text-base">'. $this->group->slug.'</div></div>';
        $this->message = $this->message.'<div class="w-1/2 flex flex-col"><span class="w-full flex text-user-gray text-sm mb-2">Тренер:</span><div class="w-full flex font-semibold text-main-black text-base">'. $this->group->trainerItem->fio.'</div></div>';
        return [
            'type' => 'payment',
            'order' => $this->order->id,
            'title' => $this->title,
            'message' => $this->message,
            'group' => $this->group->id,
            'course' => $this->group->coursesItem->id
        ];
    }
}
