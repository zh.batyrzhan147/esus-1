<?php

namespace App\Notifications;

use App\Models\Groups;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class GroupEndNotification extends Notification
{
    use Queueable;

    private $title = 'Курс закончен';
    private $group;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(Groups $group)
    {
        $this->group = $group;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database'];
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        $text = '<span>Курс: '. $this->group->coursesItem->ru_title.'</span>';
        $text = $text.'<span>Группа: '. $this->group->slug.'</span>';
        return [
            'title' => $this->title,
            'message' => $text,
        ];
    }
}
