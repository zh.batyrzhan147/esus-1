<?php

namespace App\Notifications;

use App\Models\Groups;
use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class ExcludedUserNotification extends Notification
{
    use Queueable;

    private $title = 'Вас исключили из группы';
    private $message;
    private $group;
    private $user;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($message,Groups $group, User $user)
    {
        $this->message = $message;
        $this->group = $group;
        $this->user = $user;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database'];
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        $text = '<span>Курс: '. $this->group->coursesItem->ru_title.'</span>';
        $text = $text.'<span>Группа: '. $this->group->slug.'</span>';
        $text = $text.'<span>Инициатор: '. $this->user->fio.'</span>';
        $text = $text.'<span>Сообщение от инициатора: '. $this->message.'</span>';
        return [
            'title' => $this->title,
            'message' => $text,
        ];
    }
}
