<?php

namespace App\Notifications;

use App\Models\Groups;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class GroupAcceptNotification extends Notification
{
    use Queueable;

    private $title = 'Вы зачислены на курс';
    private $message;
    private $group;


    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(Groups $group)
    {
        $this->group = $group;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database'];
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        $this->message = '<div class="w-full text-lg lg:text-xl font-bold">'. $this->group->coursesItem->ru_title.'</div>';
        $this->message = $this->message.'<div class="w-1/2 flex flex-col"><span class="w-full flex text-user-gray text-sm mb-2">Группа:</span><div class="w-full flex font-semibold text-main-black text-base">'. $this->group->slug.'</div></div>';
        $this->message = $this->message.'<div class="w-1/2 flex flex-col"><span class="w-full flex text-user-gray text-sm mb-2">Тренер:</span><div class="w-full flex font-semibold text-main-black text-base">'. $this->group->trainerItem->fio.'</div></div>';
        return [
            'title' => $this->title,
            'message' => $this->message,
        ];
    }
}
