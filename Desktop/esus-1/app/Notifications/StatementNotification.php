<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class StatementNotification extends Notification
{
    use Queueable;

    private $title = 'Вам проставлен пропуск, укажите причину и по необходимости приложите документы';
    private $statements;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(\App\Models\Statements $statements)
    {
        $this->statements = $statements;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database'];
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        $text = '';
        $text = $text.'<div class="w-1/2 flex flex-col"><span class="w-full flex text-user-gray text-sm mb-2">Группа: </span><div class="w-full flex font-semibold text-main-black text-base"><a href="'.route('user.group.view',['slug' => $this->statements->groupItem->slug]).'">'.$this->statements->groupItem->slug.'</a></div></div>';
        $text = $text.'<div class="w-1/2 flex flex-col"><span class="w-full flex text-user-gray text-sm mb-2">Дата:</span><div class="w-full flex font-semibold text-main-black text-base">'. date('d.m.Y',strtotime($this->statements->available_at)).'</div></div>';
        return [
            'title' => $this->title,
            'message' => $text,
        ];
    }
}
