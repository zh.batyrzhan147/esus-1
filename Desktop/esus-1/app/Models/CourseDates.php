<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CourseDates extends Model
{
    use HasFactory;

    public $fillable = [
        'start',
        'end',
        'courses',
        'year',
    ];
}
