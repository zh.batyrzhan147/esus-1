<?php

namespace App\Models;

use GeneaLabs\LaravelModelCaching\Traits\Cachable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class Locations extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;
    use HasFactory;

    //use Cachable;
    //public $cacheCooldownSeconds = 30;


    const LOCATION_OBLAST = 1;
    const LOCATION_RAYON = 2;
    const LOCATION_AUL = 4;

    public static function listTypes()
    {
        return [
            self::LOCATION_OBLAST    => 'Область',
            self::LOCATION_RAYON => 'Район',
            self::LOCATION_AUL  => 'Населенный пункт'
        ];
    }


    protected $fillable = [
        'ru_title',
        'kz_title',
        'type',
        'parent',
    ];

    public function childrens()
    {
        return $this->hasMany(Locations::class, 'parent');
    }

    public function parentItem()
    {
        return $this->belongsTo(Locations::class, 'parent');
    }
}
