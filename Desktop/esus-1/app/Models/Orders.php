<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\MorphTo;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use OwenIt\Auditing\Contracts\Auditable;

class Orders extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;
    use HasFactory;

    /**
     * @var array
     */
    protected $casts = [
        'payed' => 'boolean',
        'canceled' => 'boolean',
        'properties' => 'array',
    ];

    protected $fillable = [
        'id',
        'code',
        'total',
        'currency',
        'notes',
        'comment',
        'payed',
        'canceled',
        'properties',
        'reason_canceled',
        'date_payed',
        'date_canceled',
        'user_id',
        'target_type',
        'target_id',
        'created_at',
        'updated_at',
        'external_id',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphTo
     */
    public function target(): MorphTo
    {
        return $this->morphTo();
    }

    /**
     * @return bool
     */
    public function isPayed(): bool
    {
        return $this->payed;
    }

    /**
     * @return bool
     */
    public function isCanceled(): bool
    {
        return $this->canceled;
    }

    /**
     * @return bool
     */
    public function isTargetGroups(): bool
    {
        return $this->target_type === Groups::class;
    }


    public function getUserWithId($user_id)
    {
        $user = $this->user()->firstWhere('id', $user_id);

        return implode(' ', [$user->last_name ?? " ", $user->name ?? " ", $user->patronimic ?? " "]);
    }

    public function getOblastWithId($user_id)
    {
        $oblastId = $this->user()->firstWhere('id', $user_id)->oblast;
        $location = Locations::find($oblastId);
        return (app()->getLocale()=='kz') ? $location->kz_title ?? "Empty" : $location->ru_title ?? "Empty";
    }


    public function getCourseNameWithTargetId($target_id)
    {
        $coursesId = Groups::firstWhere('id', $target_id)->courses;
        $langId = Groups::firstWhere('id', $target_id)->language;
        $course =  Courses::query()->firstWhere('id', $coursesId);
        // return ($langId == 1) ? $course->kz_title : $course->ru_title ?? "EMPTY";
        return (app()->getLocale() == 'kz') ? $course->kz_title : $course->ru_title ?? "EMPTY";
    }

    public function getAffilateWithId($target_id)
    {
        $affId = Groups::firstWhere('id', $target_id ?? " ")->affiliate;
        return Affiliates::find($affId)->title ?? " ";
    }

    public function getTrainerWithId($target_id)
    {

        $trainerId = Groups::firstWhere('id', $target_id)->trainer;
        $trainer = User::query()->find($trainerId);
        return implode(' ', [$trainer->last_name ?? " ", $trainer->name ?? " ", $trainer->patronymic ?? " "]);
    }

    public function getSlugWithId($target_id)
    {
        return Groups::firstWhere('id', $target_id)->slug ?? " ";
    }
}
