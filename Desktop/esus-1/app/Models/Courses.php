<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;
use OwenIt\Auditing\Contracts\Auditable;
use phpDocumentor\Reflection\Types\Null_;
use stdClass;

class Courses extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;
    use HasFactory;

    const COURSES_OFFLINE = 1;
    const COURSES_MIXED = 2;
    const COURSES_ONLINE = 3;

    const COURSES_GRADING_YES = true;
    const COURSES_GRADING_NO = false;

    public static function listGrading()
    {
        return [
            self::COURSES_GRADING_YES    => 'С оценкой',
            self::COURSES_GRADING_NO => 'Без оценки',
        ];
    }

    public static function listTypes()
    {
        return [
            self::COURSES_ONLINE    => 'Онлайн',
            self::COURSES_OFFLINE => 'Оффлайн',
            self::COURSES_MIXED  => 'Смешанный'
        ];
    }

    protected $fillable = [
        'price',
        'kz_title',
        'ru_title',
        'slug',
        'type',
        'grading',
        'course_length',
        'practice_length',
//        'developer',
        'file_1_listener',
        'file_2_listener',
        'file_1_teacher',
        'file_2_teacher',
        'webinar_count',
        'webinar_length',
        'sdo_category',
        'sdo_link',
        'sdo_link_ru',
        'sdo_link_en',
        'one_drive_link',
        'auto_generation',
        'offer_agreement',
        'kz_short_title',
        'ru_short_title',
        'en_short_title',
    ];

    protected $attributes = [
        'auto_generation' => true,
        'type' => self::COURSES_ONLINE,
        'grading' => self::COURSES_GRADING_YES,
        'course_length' => 0,
        'practice_length' => 0,
    ];


    public function codevelopers()
    {
        return $this->belongsToMany(User::class, 'courses_users', 'course_id', 'user_id');
    }

    public function jobcategory()
    {
        return $this->belongsToMany(JobNames::class, 'job_names_users', 'course_id', 'job_name_id');
    }

    public function years()
    {
        return $this->belongsToMany( Years::class,'courses_years', 'course_id', 'year_id');
    }

    public function gettypeStringAttribute()
    {
        return $this->listTypes()[$this->type];
    }

    public function getgradingStringAttribute()
    {
        return $this->listGrading()[$this->grading];
    }

    public function trainers()
    {
        return $this->belongsToMany(User::class,'courses_users','course_id', 'user_id');
    }

    public function customers()
    {
        return $this->belongsToMany(User::class,'courses_users','course_id', 'user_id');
    }

    public function groups()
    {
        return $this->hasMany(Groups::class,'courses');
    }

    public function courseDates()
    {
        return $this->hasMany(CourseDates::class,'courses');
    }

    public function getFileUrl($item)
    {
        return Storage::disk('public')->url($item);
    }

    public function certificateTemplate()
    {
        return $this->hasOne(CertificatesTemplates::class, 'course');
    }
}
