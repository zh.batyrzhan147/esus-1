<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Programs extends Model
{
    use HasFactory;

    protected $fillable = [
        'title',
        'slug',
        'start',
        'end',
        'year'
    ];


    public function courses()
    {
        return $this->belongsToMany(Courses::class, 'programs_courses', 'program_id', 'course_id');
    }
}
