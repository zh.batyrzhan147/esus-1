<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Statements extends Model
{
    use HasFactory;

    const STATEMENTS_ATTEND = 1;
    const STATEMENTS_ONLINE = 2;
    const STATEMENTS_MISSING = 3;
    const STATEMENTS_SICK = 4;
    const STATEMENTS_MISSING_SENT = 5;
    const STATEMENTS_MISSING_DECLINE = 6;

    public static function listTypes()
    {
        return [
            self::STATEMENTS_ATTEND => 'Присутствовал лично',
            self::STATEMENTS_ONLINE => 'Присутствовал удаленно',
            self::STATEMENTS_MISSING => 'Отсутствовал неуважительно',
            self::STATEMENTS_MISSING_SENT => 'Отправлены документы',
            self::STATEMENTS_MISSING_DECLINE => 'Отсутствовал неуважительно, Отказано',
            self::STATEMENTS_SICK => 'Отсутствовал уважительно'
        ];
    }

    public static function listColorTypes()
    {
        return [
            self::STATEMENTS_ATTEND => 'bg-green-600',
            self::STATEMENTS_ONLINE => 'bg-blue-600',
            self::STATEMENTS_MISSING => 'bg-red-600',
            self::STATEMENTS_MISSING_SENT => 'bg-red-600',
            self::STATEMENTS_MISSING_DECLINE => 'bg-red-600',
            self::STATEMENTS_SICK => 'bg-teal-400',
            '' => 'bg-gray-200',
        ];
    }

    public static function listShortTypes()
    {
        return [
            self::STATEMENTS_ATTEND => 'ПЛ',
            self::STATEMENTS_ONLINE => 'ПУ',
            self::STATEMENTS_MISSING => 'ОН',
            self::STATEMENTS_MISSING_SENT => 'ОН',
            self::STATEMENTS_MISSING_DECLINE => 'ОН',
            self::STATEMENTS_SICK => 'ОУ',
            '' => '-',
        ];
    }


    protected $fillable = [
        'user',
        'group',
        'available_at',
        'status',
    ];

    public function userItem()
    {
        return $this->belongsTo(User::class, 'user');
    }

    public function groupItem()
    {
        return $this->belongsTo(Groups::class, 'group');
    }

    public function getStatusNameAttribute()
    {
        return $this->listTypes()[$this->status];
    }

    public function getStatusColorAttribute()
    {
        return $this->listColorTypes()[$this->status];
    }
}
