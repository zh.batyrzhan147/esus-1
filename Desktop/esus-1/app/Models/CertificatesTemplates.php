<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class CertificatesTemplates extends Model
{
    use HasFactory;

    const CERTIFICATES_TEMPLATES_TYPE_CERTIFICATE = 0;
    const CERTIFICATES_TEMPLATES_TYPE_REFERENCE = 1;

    public static function listTypes()
    {
        return [
            self::CERTIFICATES_TEMPLATES_TYPE_CERTIFICATE => 'Сертификат',
            self::CERTIFICATES_TEMPLATES_TYPE_REFERENCE => 'Справка об обучении',
        ];
    }


    protected $fillable = [
        'active',
        'title',
        'background',
        'year',
        'course',
        'type',
    ];

    protected $attributes = [
        'active' => true,
    ];

    public function courseItem()
    {
        return $this->belongsTo(Courses::class, 'course','id');
    }

    public function getbackgroundUrlAttribute()
    {

        if (filter_var($this->background, FILTER_VALIDATE_URL))
        {
            return $this->background;
        } else {
            return $this->background
                ? Storage::disk('certificate-templates')->url($this->background)
                : null;
        }

    }
}
