<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Subjects extends Model
{
    use HasFactory;

    protected $fillable = [
        'title',
        'fromUser',
        'toUser',
        'files',
        'request',
        'statement',
    ];

    public function fromUserItem()
    {
        return $this->belongsTo(User::class, 'fromUser');
    }

    public function toUserItem()
    {
        return $this->belongsTo(User::class, 'toUser');
    }
    public function statementItem()
    {
        return $this->belongsTo(Statements::class, 'statement');
    }
}
