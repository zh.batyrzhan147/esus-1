<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AuditReports extends Model
{
    use HasFactory;

    const AUDIT_REPORT_STATUS_NEW = 0;
    const AUDIT_REPORT_STATUS_PROCESSING = 1;
    const AUDIT_REPORT_STATUS_FINISHED = 2;
    const AUDIT_REPORT_STATUS_ERROR = 3;

    public static function listStatuses()
    {
        return [
            self::AUDIT_REPORT_STATUS_NEW => 'Новый',
            self::AUDIT_REPORT_STATUS_PROCESSING => 'В обработке',
            self::AUDIT_REPORT_STATUS_FINISHED => 'Завершено',
            self::AUDIT_REPORT_STATUS_ERROR => 'Ошибка',

        ];
    }

    const AUDIT_REPORT_TYPE_COURSES = 0;
    const AUDIT_REPORT_TYPE_GROUPS = 1;
    const AUDIT_REPORT_TYPE_LISTENERS = 2;

    public static function listTypes()
    {
        return [
            self::AUDIT_REPORT_TYPE_COURSES => 'Аудит курсов',
            self::AUDIT_REPORT_TYPE_GROUPS => 'Аудит групп',
            self::AUDIT_REPORT_TYPE_LISTENERS => 'Аудит слушателей',
        ];
    }

    protected $fillable = [
        'file',
        'user',
        'year',
        'data',
        'status',
        'affiliate',
    ];

    public static function boot() {

        parent::boot();

        static::created(function($item) {
            if ($item->type  == self::AUDIT_REPORT_TYPE_COURSES) {
                \App\Jobs\AuditGrants::dispatch($item)->onQueue('no-limit');
            }

            if ($item->type  == self::AUDIT_REPORT_TYPE_GROUPS) {
                \App\Jobs\AuditGroups::dispatch($item)->onQueue('no-limit');
            }

            if ($item->type  == self::AUDIT_REPORT_TYPE_LISTENERS) {
                \App\Jobs\AuditListeners::dispatch($item)->onQueue('no-limit');
            }
        });
    }


    public function userItem()
    {
        return $this->belongsTo(User::class,'user');
    }


}
