<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class Certificates extends Model
{
    use HasFactory;

    const CERTIFICATES_TYPE_CERTIFICATE = 0;
    const CERTIFICATES_TYPE_REFERENCE = 1;

    public static function listTypes()
    {
        return [
            self::CERTIFICATES_TYPE_CERTIFICATE => 'Сертификат',
            self::CERTIFICATES_TYPE_REFERENCE => 'Справка об обучении',
        ];
    }

    protected $fillable = [
        'pdf',
        'fio',
        'group_title',
        'group_id',
        'course_title',
        'course_id',
        'year',
        'description',
        'url',
        'views_count',
        'user',
        'template',
        'uuid',
        'type',
        'cert_day'
    ];

    public function users()
    {
        return $this->belongsTo(User::class, 'user');
    }

    public function templateItem()
    {
        return $this->belongsTo(CertificatesTemplates::class, 'template');
    }

    public function groupItem()
    {
        return $this->belongsTo(Groups::class, 'group_id');
    }


    public function getPdfCopyUrl()
    {
        return Storage::disk('public')->url(date('Y/m/d',strtotime($this->created_at)).'/'.$this->uuid.'.pdf');
    }

    public static function boot() {

        parent::boot();

        static::created(function($item) {
            if ($item->type == self::CERTIFICATES_TYPE_CERTIFICATE) {
                \App\Jobs\GenerateCertificate::dispatch($item->id)->onQueue('generate');
            }
            if ($item->type == self::CERTIFICATES_TYPE_REFERENCE) {
                \App\Jobs\GenerateReference::dispatch($item->id)->onQueue('generate');
            }

        });
    }

}
