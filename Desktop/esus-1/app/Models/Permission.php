<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class Permission extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;
    const PERMISSION_SUPERADMIN = 1;
    const PERMISSION_ADMIN = 2;
    const PERMISSION_TRAINER = 3;
    const PERMISSION_USER = 4;
    const PERMISSION_AFFILIATE = 5;
    const PERMISSION_OOP = 6;
    const PERMISSION_OMR = 7;
    const PERMISSION_OOUP = 8;
    const PERMISSION_ACCOUNTANT = 9;

    public static function listPermissions()
    {
        return [
            self::PERMISSION_SUPERADMIN    => self::PERMISSION_SUPERADMIN,
            self::PERMISSION_ADMIN => self::PERMISSION_ADMIN,
            self::PERMISSION_TRAINER  => self::PERMISSION_TRAINER,
//            self::PERMISSION_USER => self::PERMISSION_USER,
            self::PERMISSION_AFFILIATE => self::PERMISSION_AFFILIATE,
            self::PERMISSION_OOP => self::PERMISSION_OOP,
            self::PERMISSION_OMR => self::PERMISSION_OMR,
            self::PERMISSION_OOUP => self::PERMISSION_OOUP,
            self::PERMISSION_ACCOUNTANT => self::PERMISSION_ACCOUNTANT,
        ];
    }

    use HasFactory;

    protected $fillable = [
        'title',
    ];

    public function roles()
    {
        return $this->belongsToMany(Role::class);
    }
}
