<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Notification;
use Illuminate\Support\Facades\Storage;

class Reports extends Model
{
    use HasFactory;

    const REPORT_STATUS_NEW = 0;
    const REPORT_STATUS_PROCESSING = 1;
    const REPORT_STATUS_FINISHED = 2;
    const REPORT_STATUS_ERROR = 3;

    const REPORT_TYPE_LISTENER_BY_COURSES = 1;
    const REPORT_TYPE_PRILOZHENIE_2 = 2;
    const REPORT_TYPE_FORMA_CPI = 3;
    const REPORT_TYPE_PROGRAM_FILIAL_YEAR = 4;
    const REPORT_TYPE_PRILOZHENIE_1 = 5;
    const REPORT_TYPE_COURSES_RESULTS = 6;
    const REPORT_TYPE_TRAINERS_PUBS = 7;


    public static function listTypes()
    {
        return [
            self::REPORT_TYPE_PRILOZHENIE_1 => 'Приложение 1',
            self::REPORT_TYPE_PRILOZHENIE_2 => 'Приложение 2 групп',
            self::REPORT_TYPE_FORMA_CPI => 'Форма ЦПИ',
            self::REPORT_TYPE_LISTENER_BY_COURSES => 'Отчет слушателей по программам',
            self::REPORT_TYPE_PROGRAM_FILIAL_YEAR => 'Отчет по програм-филиалам-годам',
            self::REPORT_TYPE_COURSES_RESULTS => 'Результаты курсов',
            self::REPORT_TYPE_TRAINERS_PUBS => 'Информация о публикациях тренеров',

        ];
    }

    public static function listStatuses()
    {
        return [
            self::REPORT_STATUS_NEW => 'Новый',
            self::REPORT_STATUS_PROCESSING => 'В обработке',
            self::REPORT_STATUS_FINISHED => 'Завершено',
            self::REPORT_STATUS_ERROR => 'Ошибка',
        ];
    }

    protected $fillable = [
        'file',
        'user',
        'type',
        'status',
        'starttime',
        'endtime',
        'affiliate',
        'year',
        'course',
        'language',
    ];

    protected $attributes = [
        'status' => self::REPORT_STATUS_NEW,
    ];

    public function getFileUrl($item)
    {
        return Storage::disk('public')->url($item);
    }

    public function courseItem()
    {
        return $this->belongsTo(Courses::class,'course');
    }
    public function languageItem()
    {
        return $this->belongsTo(Languages::class,'language');
    }

    public static function exportItem($item)
    {
        if ($item->type == self::REPORT_TYPE_LISTENER_BY_COURSES) \App\Jobs\Reports\ListenersByCourses::dispatch($item)->onQueue('no-limit');
        if ($item->type == self::REPORT_TYPE_PRILOZHENIE_2) \App\Jobs\Reports\PrilozhenieTwo::dispatch($item)->onQueue('no-limit');
        if ($item->type == self::REPORT_TYPE_FORMA_CPI) \App\Jobs\Reports\FormaCPI::dispatch($item)->onQueue('no-limit');
        if ($item->type == self::REPORT_TYPE_PROGRAM_FILIAL_YEAR) \App\Jobs\Reports\ProgramFilialYear::dispatch($item)->onQueue('no-limit');
        if ($item->type == self::REPORT_TYPE_PRILOZHENIE_1) \App\Jobs\Reports\PrilozhenieOne::dispatch($item)->onQueue('no-limit');
        if ($item->type == self::REPORT_TYPE_COURSES_RESULTS) \App\Jobs\Reports\CoursesResults::dispatch($item)->onQueue('no-limit');
        if ($item->type == self::REPORT_TYPE_TRAINERS_PUBS) \App\Jobs\Reports\TrainersPubs::dispatch($item)->onQueue('no-limit');
    }

    public static function boot() {

        parent::boot();

        static::updating(function($item) {
            if ($item->status != $item->getOriginal('status')) {
               if ($item->status == self::REPORT_STATUS_NEW) {
                   self::exportItem($item);
               }
            }
        });
        static::created(function($item) {
            self::exportItem($item);
        });
    }

}
