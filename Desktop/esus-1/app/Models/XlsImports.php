<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class XlsImports extends Model
{
    use HasFactory;

    const IMPORTS_STATUS_CREATED = 0;
    const IMPORTS_STATUS_STARTED = 1;
    const IMPORTS_STATUS_FINISHED = 2;
    const IMPORTS_STATUS_ERROR = -1;


    const IMPORTS_MODEL_LOCATION = 'locations';

    public static function listModels()
    {
        return [
            self::IMPORTS_MODEL_LOCATION => 'Локации'
        ];
    }
    public static function listStatuses()
    {
        return [
            self::IMPORTS_STATUS_ERROR => 'Ошибка',
            self::IMPORTS_STATUS_CREATED => 'Создано',
            self::IMPORTS_STATUS_STARTED => 'В процессе',
            self::IMPORTS_STATUS_FINISHED => 'Завершено',
        ];
    }


    protected $fillable = [
        'model',
        'file',
        'status',
        'message',
    ];

    public static function boot() {

        parent::boot();

        static::created(function($item) {
            \App\Jobs\XlsImportJob::dispatch($item->id)->onQueue('no-limit');
        });
    }
}
