<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AttractedCats extends Model
{
    use HasFactory;

    protected $fillable = [
        'kz_title',
        'ru_title',
    ];
}
