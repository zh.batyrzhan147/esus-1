<?php

namespace App\Models;

use App\Jobs\QueuedPasswordResetJob;
use App\Jobs\QueuedVerifyEmailJob;
use App\Jobs\SendPasswordJob;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Fortify\TwoFactorAuthenticatable;
use Laravel\Jetstream\HasProfilePhoto;
use Laravel\Sanctum\HasApiTokens;
use OwenIt\Auditing\Contracts\Auditable;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Notification;
use Carbon\Carbon;

class User extends Authenticatable implements Auditable
{

    use \OwenIt\Auditing\Auditable;
    const USER_SUPERADMIN = 'admin';

    use HasApiTokens;
    use HasFactory;
    use HasProfilePhoto;
    use Notifiable;
    use TwoFactorAuthenticatable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'iin',
        'name',
        'name_lat',
        'last_name',
        'last_name_lat',
        'patronymic',
        'phone',
        'email',
        'password',
        'auto_generation',
        'email_verified_at',
        'stazh',
        'attracted',
        'gorono',
        'attractedcat',
        'hours_count',
        'teach_subject_id',
        'last_group_slug',
        'trainer_awards',
        'trainer_theses',
        'trainer_certificates',
        'trainer_oauth_group',
    ];

    protected $attributes = [
        'attracted' => false,
        'hours_count' => 0,
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
        'two_factor_recovery_codes',
        'two_factor_secret',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = [
        'profile_photo_url',
        'fio',
    ];

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
//        self::created(function (User $user) {
//            if (!$user->roles()->get()->contains(3)) {
//                $user->roles()->attach(3);
//            }
//        });
    }

    public function languages()
    {
        return $this->belongsToMany(Languages::class, 'users_languages', 'user_id', 'language_id');
    }

    public function roles()
    {
        return $this->belongsToMany(Role::class);
    }

    public function orders()
    {
        return $this->hasMany(Orders::class, 'user_id');
    }

    public function courses()
    {
        return $this->belongsToMany(Courses::class,'courses_users','user_id', 'course_id');
    }

    public function nationalityItem()
    {
        return $this->belongsTo(Nationalities::class,'nationality');
    }

    public function genderItem()
    {
        return $this->belongsTo(Genders::class, 'gender');
    }

    public function oblastItem()
    {
        return $this->belongsTo(Locations::class,'oblast');
    }

    public function raionItem()
    {
        return $this->belongsTo(Locations::class, 'raion');
    }

    public function gorodItem()
    {
        return $this->belongsTo(Locations::class, 'gorod');
    }

    public function affiliateItem()
    {
        return $this->belongsTo(Affiliates::class, 'affiliate');
    }

    public function academicCategoryItem()
    {
        return $this->belongsTo(AcademicCategories::class, 'academic_category');
    }
    public function educationItem()
    {
        return $this->belongsTo(Educations::class, 'education');
    }
    public function academicDegreeItem()
    {
        return $this->belongsTo(AcademicDegrees::class, 'academic_degree');
    }



    public function instituteItem()
    {
        return $this->belongsTo(Institutes::class, 'institute');
    }

    public function jobnameItem()
    {
        return $this->belongsTo(JobNames::class, 'jobname');
    }

    public function attractedcatItem()
    {
        return $this->belongsTo(AttractedCats::class, 'attractedcat');
    }

    public function certificates()
    {
        return $this->hasMany(Certificates::class, 'user');
    }


    public function groups()
    {
        return $this->hasMany(Groups::class,'trainer');
    }

    public function listenerGroups()
    {
        return $this->belongsToMany(Groups::class,'groups_users','user_id','group_id');
    }


    public function getfioAttribute()
    {
        return ($this->last_name ?? '') . ' ' . ($this->name ?? '') . ' ' .($this->patronymic ?? '');
    }

    public function sendEmailVerificationNotification()
    {
        //dispactches the job to the queue passing it this User object
        QueuedVerifyEmailJob::dispatch($this)->onQueue('send-email');
    }

    public function sendPasswordResetNotification($token)
    {
        //dispactches the job to the queue passing it this User object
        QueuedPasswordResetJob::dispatch($this,$token)->onQueue('send-email');
    }

    public function teachSubjectItem()
    {
        return $this->belongsTo(TeachSubjects::class,'teach_subject_id');
    }

    public function isFull()
    {
        if ($this->iin == 'admin') return true;
        if (Carbon::now()->diffInMinutes($this->created_at) < 5) return true;

        $result = true;
        if ($this->iin == null) { $result = false; }
        if ($this->name == null) { $result = false; }
        if ($this->last_name == null) { $result = false; }
        if ($this->patronymic == null) { $result = false; }
        if ($this->name_lat == null) { $result = false; }
        if ($this->last_name_lat == null) { $result = false; }
        if ($this->gender == null) { $result = false; }
        if ($this->nationality == null) { $result = false; }
        if ($this->phone == null) { $result = false; }
        if ($this->email == null) { $result = false; }
        if ($this->oblast == null) { $result = false; }
        if ($this->raion == null) { $result = false; }
        if ($this->gorod == null) { $result = false; }
        if ($this->institute == null) { $result = false; }
        if ($this->jobname == null) { $result = false; }
        if ($this->academic_category == null) { $result = false; }
        if ($this->education == null) { $result = false; }
        if ($this->academic_degree == null) { $result = false; }
        if ($this->teach_subject_id == null) { $result = false; }
        if ($this->stazh == null) { $result = false; }

        return $result;
    }


    private function generatePassword()
    {
        $random = str_shuffle('1234567890');
        $symbol = str_shuffle('!@#+*-');
        $oneLetter = str_shuffle('bcdfghjklqrstuvwxyz');
        $password = "Cpm". substr($random, 0, 6)  . substr($oneLetter, 0, 1) . substr($symbol, 0, 1);
        return $password;
    }

    public function resetPassword($new = false)
    {
        $password = $this->generatePassword();
        $this->password = Hash::make($password);
        $this->save();
        SendPasswordJob::dispatch($this, $password, $new)->onQueue('send-email');
    }

    public static function boot()
    {

        parent::boot();

        static::created(function ($item) {
            $item->resetPassword(true);
        });

    }


}
