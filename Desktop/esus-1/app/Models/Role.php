<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class Role extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;
    use HasFactory;
    const ROLE_SUPERADMIN = 1;
    const ROLE_ADMIN = 2;
    const ROLE_USER = 3;
    const ROLE_TRAINER = 4;
    const ROLE_AFFILIATE = 5;
    const ROLE_OOP = 6;
    const ROLE_OMR = 7;
    const ROLE_OOUP = 8;
    const ROLE_ACCOUNTANT = 9;


    public static function listRoles()
    {
        return [
            self::ROLE_SUPERADMIN => 'Супер администратор',
            self::ROLE_ADMIN    => 'Администратор',
//            self::ROLE_USER => 'Пользователь',
            self::ROLE_TRAINER  => 'Тренер',
            self::ROLE_AFFILIATE => 'Сотрудник филлиала',
            self::ROLE_OOP => 'Сотрудник ООП',
            self::ROLE_OMR => 'Сотрудник ОМР',
            self::ROLE_OOUP => 'Сотрудник ООУП',
            self::ROLE_ACCOUNTANT => 'Сотрудник Бухгалтерии',
        ];
    }

    public static function badgesColors()
    {
        return [
            self::ROLE_SUPERADMIN => 'bg-red-100 hover:bg-red-200 text-red-800 text-sm font-medium mr-2 px-2.5 py-0.5 rounded dark:bg-red-200 dark:text-red-900',
            self::ROLE_ADMIN    => 'bg-blue-100 hover:bg-blue-200 text-blue-800 text-sm font-medium mr-2 px-2.5 py-0.5 rounded dark:bg-blue-200 dark:text-blue-800',
            self::ROLE_TRAINER  => 'bg-green-100 hover:bg-green-200 text-green-800 text-sm font-medium mr-2 px-2.5 py-0.5 rounded dark:bg-green-200 dark:text-green-900',
            self::ROLE_AFFILIATE => 'bg-yellow-100 hover:bg-yellow-200 text-yellow-800 text-sm font-medium mr-2 px-2.5 py-0.5 rounded dark:bg-yellow-200 dark:text-yellow-900',
            self::ROLE_OOP => 'bg-indigo-100 hover:bg-indigo-200 text-indigo-800 text-sm font-medium mr-2 px-2.5 py-0.5 rounded dark:bg-indigo-200 dark:text-indigo-900',
            self::ROLE_OMR => 'bg-purple-100 hover:bg-purple-200 text-purple-800 text-sm font-medium mr-2 px-2.5 py-0.5 rounded dark:bg-purple-200 dark:text-purple-900',
            self::ROLE_OOUP => 'bg-pink-100 hover:bg-pink-200 text-pink-800 text-sm font-medium mr-2 px-2.5 py-0.5 rounded dark:bg-pink-200 dark:text-pink-900',
            self::ROLE_ACCOUNTANT => 'bg-blue-100 hover:bg-blue-200 text-blue-800 text-sm font-medium mr-2 px-2.5 py-0.5 rounded dark:bg-blue-200 dark:text-blue-800',
        ];
    }

    protected $fillable = [
        'title'
    ];

    public function users()
    {
        return $this->belongsToMany(User::class);
    }

    public function permissions()
    {
        return $this->belongsToMany(Permission::class);
    }
}
