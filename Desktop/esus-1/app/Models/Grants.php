<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Arr;
use OwenIt\Auditing\Contracts\Auditable;

class Grants extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;
    use HasFactory;

    protected $fillable = [
        'year',
        'count_all',
        'courses',
        'counts',
        'affiliate'
    ];

    public function getCountsArrayAttribute()
    {
        if ($this->counts && !is_array($this->counts)) {
            return json_decode($this->counts, true);
        }
        return $this->counts;
    }


    public function affiliateItem()
    {
        return $this->belongsTo(Affiliates::class, 'affiliate');
    }

    public function coursesItem()
    {
        return $this->belongsTo(Courses::class, 'courses');
    }


}
