<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;
use OwenIt\Auditing\Contracts\Auditable;

class Groups extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;
    use HasFactory;

    /**
     * @var array
     */
    protected $casts = [
        'paid' => 'boolean',
    ];

    const GROUPS_USER_REQUEST = 1;
    const GROUPS_USER_REWORK = 2;
    const GROUPS_USER_CERT = 3;
    const GROUPS_USER_PAYMENT = 4;

    const GROUP_STATUS_ARCHIVE = -1;
    const GROUP_STATUS_PLANNED = 1;
    const GROUP_STATUS_FORMED = 2;
    const GROUP_STATUS_INPROGRESS = 3;
    const GROUP_STATUS_FINISHED = 4;
    const GROUP_STATUS_FINISHED_TESTING = 6;
    const GROUP_STATUS_CERTIFICATED = 5;

    public static function listUserStatuses()
    {
        return [
            self::GROUPS_USER_REQUEST    => 'Заявка',
            self::GROUPS_USER_REWORK   => 'На доработке',
            self::GROUPS_USER_CERT   => '',
            self::GROUPS_USER_PAYMENT   => 'На оплату',
        ];
    }

    public static function listGroupStatuses()
    {
        return [
            self::GROUP_STATUS_PLANNED   => 'Запланирован, идет набор',
            self::GROUP_STATUS_FORMED   => 'Сформировано',
            self::GROUP_STATUS_INPROGRESS   => 'Идет обучение',
            self::GROUP_STATUS_FINISHED   => 'Завершено',
            self::GROUP_STATUS_FINISHED_TESTING => 'На оценивании',
            self::GROUP_STATUS_CERTIFICATED   => 'Сертификат выдан',
            self::GROUP_STATUS_ARCHIVE    => 'В архиве',
        ];
    }

    protected $fillable = [
        'slug',
        'paid',
        'language',
        'affiliate',
        'year',
        'offer_agreement',
        'count_all',
        'courses',
        'trainer',
        'start_time',
        'end_time',
        'type',
        'active',
        'staged',
        'hash',
        'zoom_link',
        'status',
        'practice_start',
        'practice_end',
        'cert_day',
        'change_statements',
    ];

    public function languageItem()
    {
        return $this->belongsTo(Languages::class, 'language');
    }

    public function certificates()
    {
        return $this->hasMany(Certificates::class, 'group_id')->where('type', '=',Certificates::CERTIFICATES_TYPE_CERTIFICATE);
    }

    public function users()
    {
        return $this->belongsToMany(User::class, 'groups_users','group_id','user_id');
    }

    public function users_sb()
    {
        return $this->belongsToMany(User::class, 'groups_users_sb','group_id','user_id')->withPivot('status');
    }

    public function trainerItem()
    {
        return $this->belongsTo(User::class, 'trainer');
    }

    public function coursesItem()
    {
        return $this->belongsTo(Courses::class, 'courses');
    }

    public function affiliateItem()
    {
        return $this->belongsTo(Affiliates::class, 'affiliate');
    }

    public function offer_agreementUrl()
    {

        if (filter_var($this->offer_agreement, FILTER_VALIDATE_URL))
        {
            return $this->offer_agreement;
        } else {
            return $this->offer_agreement
                ? Storage::disk('offer_agreements')->url($this->offer_agreement)
                : null;
        }

    }

    public function checkLangTitle($group){
        return ($group->language == 1) ?  $group->coursesItem->kz_title : $group->coursesItem->ru_title;
    }



    public function getJoinLinkAttribute()
    {
        return route('join-group', ['uuid' => $this->hash]);;
    }

    public function getFileUrl($item)
    {
        return Storage::disk('public')->url($item);
    }

    public function setAttribute($key, $value)
    {
        if ($key !== 'paid') {
            $value = empty($value) ? null : $value;
        }

        if ($key === 'change_statements') {
            $value = empty($value) ? false : $value;
        }

        return parent::setAttribute($key, $value);
    }



    public static function boot() {

        parent::boot();

        static::created(function($item) {
            $item->trainerItem->hours_count = ($item->trainerItem->hours_count ?? 0) + ($item->coursesItem->course_length ?? 0);
            $item->cert_day = $item->end_time;
            $item->trainerItem->save();
        });

        static::updating(function ($item) {
            if (!$item->cert_day) {
                if ($item->end_time) {
                    $item->cert_day = $item->end_time;
                }
            }
            if ($item->trainer != $item->getOriginal('trainer')) {
                $old = User::where('id',$item->getOriginal('trainer'))->first();
                $old->hours_count = ($old->hours_count ?? 0) - ($item->coursesItem->course_length ?? 0);
                $old->save();
                $item->trainerItem->hours_count = ($item->trainerItem->hours_count ?? 0) + ($item->coursesItem->course_length ?? 0);
                $item->trainerItem->save();
            }

            if ($item->status != $item->getOriginal('status')) {
                if (($item->status == self::GROUP_STATUS_FINISHED) or ($item->status == self::GROUP_STATUS_FINISHED_TESTING)) {
                    \App\Jobs\CreateReferenceOnGroupFinished::dispatch($item)->onQueue('generate');
                }
            }
            if($item->offer_agreement == null) {
                $offer = null;
                if ($item->coursesItem) $offer = $item->coursesItem->offer_agreement;
                if ($offer) $item->offer_agreement = $offer;
            }
        });
        static::deleting(function ($item) {
            $item->trainerItem->hours_count = ($item->trainerItem->hours_count ?? 0) - ($item->coursesItem->course_length ?? 0);
            $item->trainerItem->save();
        });

    }

}
