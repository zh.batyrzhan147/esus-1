<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Notification;
use Illuminate\Support\Facades\Storage;

class StatementsMessages extends Model
{
    use HasFactory;

    const STATEMENTSMESSAGE_STATUS_NEW = 0;
    const STATEMENTSMESSAGE_STATUS_DECLINE = 1;
    const STATEMENTSMESSAGE_STATUS_ACCEPT = 2;
    const STATEMENTSMESSAGE_STATUS_UPDATED = 3;


    public static function listStatuses()
    {
        return [
            self::STATEMENTSMESSAGE_STATUS_NEW => 'Новый',
            self::STATEMENTSMESSAGE_STATUS_ACCEPT => 'Одобрено',
            self::STATEMENTSMESSAGE_STATUS_DECLINE => 'Отклонено',
            self::STATEMENTSMESSAGE_STATUS_UPDATED => 'Обновлено',
        ];
    }



    protected $fillable = [
        'user',
        'group',
        'affiliate',
        'statement',
        'description',
        'file',
        'status',
        'changedby',
        'message'
    ];

    protected $attributes = [
        'status' => self::STATEMENTSMESSAGE_STATUS_NEW,
    ];

    public function userItem()
    {
        return $this->belongsTo(User::class,'user');
    }

    public function groupItem()
    {
        return $this->belongsTo(Groups::class,'group');
    }

    public function affiliateItem()
    {
        return $this->belongsTo(Affiliates::class,'affiliate');
    }

    public function statementItem()
    {
        return $this->belongsTo(Statements::class,'statement');
    }

    public function changedbyItem()
    {
        return $this->belongsTo(User::class,'changedby');
    }

    public function getFileUrl($item)
    {
        return Storage::disk('public')->url($item);
    }

    public static function boot() {

        parent::boot();

        static::updating(function($item) {
            if ($item->status != $item->getOriginal('status')) {
                if ($item->status == self::STATEMENTSMESSAGE_STATUS_ACCEPT) {
                    Notification::send($item->userItem, new \App\Notifications\StatementAcceptNotification($item->message,$item->statementItem,$item->changedbyItem));
                }
                if ($item->status == self::STATEMENTSMESSAGE_STATUS_DECLINE) {
                    Notification::send($item->userItem, new \App\Notifications\StatementDeclineNotification($item->message,$item->statementItem,$item->changedbyItem));
                }
            }
        });
    }

}
