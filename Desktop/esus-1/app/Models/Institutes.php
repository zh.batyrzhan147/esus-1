<?php

namespace App\Models;

use GeneaLabs\LaravelModelCaching\Traits\Cachable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class Institutes extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;
    use HasFactory;

    //use Cachable;
    //public $cacheCooldownSeconds = 30;

    const INSTITUTE_TYPE_NOTYPE = 0;
    const INSTITUTE_TYPE_MAGNIT = 1;
    const INSTITUTE_TYPE_VEDUSH = 2;

    public static function listMagnetic()
    {
        return [
            self::INSTITUTE_TYPE_NOTYPE => 'Не выбрано',
            self::INSTITUTE_TYPE_MAGNIT => 'Магнитная школа',
            self::INSTITUTE_TYPE_VEDUSH => 'Ведущая школа'
        ];
    }


    public static function listTypes()
    {
        return InstitutesTypes::all()->pluck('title','id')->toArray();
    }

    public function getTypeNameAttribute()
    {
        return $this->listTypes()[$this->type];
    }

    protected $fillable = [
        'id',
        'title',
        'kz_title',
        'ru_title',
        'type',
        'city',
        'rayon',
        'oblast',
        'address',
        'type_org',
        'type_school',
        'forms_id',
    ];

    protected $attributes = [
        'type_org' => 0,
    ];

    public function oblastItem()
    {
        return $this->belongsTo(Locations::class, 'oblast');
    }

    public function rayonItem()
    {
        return $this->belongsTo(Locations::class, 'rayon');
    }

    public function cityItem()
    {
        return $this->belongsTo(Locations::class, 'city');
    }

    public function formsItem()
    {
        return $this->belongsTo(InstituteForms::class,'forms_id');
    }
    public function getLocationAttribute()
    {
        $location = '';

        if (isset($this->oblastItem)) {
            $location = $location . $this->oblastItem->kz_title;
        }

        if (isset($this->rayonItem)) {

            if (empty($location)) {
                $location = $this->rayonItem->kz_title;
            } else {
                $location = $location . ' / ' . $this->rayonItem->kz_title;
            }
        }

        if (isset($this->cityItem)) {
            if (empty($location)) {
                $location = $this->cityItem->kz_title;
            } else {
                $location = $location . ' / ' . $this->cityItem->kz_title;
            }
        }

        return $location;
    }

}
