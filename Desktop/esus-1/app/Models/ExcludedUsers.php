<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Notification;

class ExcludedUsers extends Model
{
    use HasFactory;

    protected $fillable = [
        'user',
        'group',
        'message',
        'excluded_by',
    ];

    public static function boot() {

        parent::boot();

        static::created(function($item) {
            Notification::send($item->userItem, new \App\Notifications\ExcludedUserNotification($item->message,$item->groupItem, $item->excludedByItem));
        });
    }


    public function userItem()
    {
        return $this->belongsTo(User::class,'user');
    }

    public function groupItem()
    {
        return $this->belongsTo(Groups::class,'group');
    }

    public function excludedByItem()
    {
        return $this->belongsTo(User::class,'excluded_by');
    }
}
