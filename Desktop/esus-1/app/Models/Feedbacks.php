<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Feedbacks extends Model
{
    use HasFactory;

    protected $fillable = [
        'stars',
        'description',
        'year',
        'trainer',
        'listener',
        'course',
        'group',
    ];

    public function yearItem()
    {
        return $this->belongsTo(Years::class,'year');
    }

    public function trainerItem()
    {
        return $this->belongsTo(User::class,'trainer');
    }

    public function listenerItem()
    {
        return $this->belongsTo(User::class, 'listener');
    }

    public function courseItem()
    {
        return $this->belongsTo(Courses::class, 'course');
    }

    public function groupItem()
    {
        return $this->belongsTo(Groups::class, 'group');
    }
}
