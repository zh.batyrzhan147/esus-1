<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Weekends extends Model
{
    use HasFactory;

    const WEEKENDS_HOLIDAY = 1;
    const WEEKENDS_WORKING = 2;

    public static function listTypes()
    {
        return [
            self::WEEKENDS_HOLIDAY  => 'Выходной день',
            self::WEEKENDS_WORKING  => 'Рабочий день',
        ];
    }
    public function getTypeStringAttribute()
    {
        return $this->listTypes()[$this->type];
    }

    protected $fillable = [
        'day',
        'type'
    ];
}
