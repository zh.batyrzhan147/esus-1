<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class InstituteForms extends Model
{
    use HasFactory;
    protected $fillable = ['title'];
}
