<?php

namespace App\Models;

use GeneaLabs\LaravelModelCaching\Traits\Cachable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class Affiliates extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;
    use HasFactory;

    //use Cachable;
    //public $cacheCooldownSeconds = 30;

    protected $fillable = [
        'title',
        'director',
        'address',
        'phone',
        'email',
        'directorId'
    ];

    public function directorIdItem()
    {
        return $this->belongsTo(User::class, 'directorId');
    }
}
