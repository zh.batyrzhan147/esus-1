<?php

namespace App\Listeners\Order;

use App\Events\Order\Payed;
use App\Models\Groups;
use Illuminate\Support\Facades\Notification;

class GrantAccess
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  Payed  $event
     * @return void
     */
    public function handle(Payed $event)
    {
        $order = $event->order;

        if ($order->isTargetGroups()) {
            $group = $order->target;

            $group->users_sb()->detach($order->user);
            $group->users()->detach($order->user);
            $group->users()->attach($order->user);

            $order->user->last_group_slug = str_pad(($this->group->language ?? 0), 2, '0', STR_PAD_LEFT).'-'.($this->group->slug ?? '');
            $order->user->save();

            Notification::send($order->user, new \App\Notifications\GroupAcceptNotification($group));
        }
    }
}
