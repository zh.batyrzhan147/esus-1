<?php

namespace App\Console\Commands;

use App\Models\Statements;
use App\Models\StatementsMessages;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Notification;

class StatementsAnalyze extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'statements:analyze';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        Statements::where('status', Statements::STATEMENTS_MISSING)
            ->where('available_at', date('Y-m-d', strtotime('yesterday')))
            ->chunk(100, function ($items) {
                foreach ($items as $item) {
                    Notification::send($item->userItem, new \App\Notifications\StatementNotification($item));
                }
            });
        return 0;
    }
}
