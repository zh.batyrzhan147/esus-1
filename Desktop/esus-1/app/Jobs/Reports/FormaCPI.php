<?php

namespace App\Jobs\Reports;

use App\Models\Reports;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Maatwebsite\Excel\Facades\Excel;

class FormaCPI implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    private $starttime;
    private $endtime;
    private $year;
    private $courses;
    private $language;
    private $report;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Reports $report)
    {
        $this->report = $report;
        $this->starttime = $report->starttime;
        $this->endtime = $report->endtime;
        $this->year = $report->year;
        $this->courses = $report->course;
        $this->language = $report->language;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $reportItem = $this->report;
        try {
            $reportItem->status = Reports::REPORT_STATUS_PROCESSING;
            $reportItem->save();
            $file = Excel::store(new \App\Exports\FormaCPIMain($this->starttime, $this->endtime, $this->year, $this->courses, $this->language), date('Y/m/d/') . 'report_' . $this->report->id . '.xlsx', 'public');
            if ($file) {
                $reportItem->status = Reports::REPORT_STATUS_FINISHED;
                $reportItem->file = date('Y/m/d/') . 'report_' . $this->report->id . '.xlsx';
            }
            $reportItem->save();
        } catch (\Exception $e) {
            $reportItem->data = $e;
            $reportItem->status = Reports::REPORT_STATUS_ERROR;
            $reportItem->save();
        }
    }
}
