<?php

namespace App\Jobs\Reports;

use App\Models\Reports;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Maatwebsite\Excel\Facades\Excel;

class CoursesResults implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public function __construct(public Reports $report){}

    public function handle()
    {
        $reportItem = $this->report;
        try {
            $reportItem->status = Reports::REPORT_STATUS_PROCESSING;
            $reportItem->save();
            $file = Excel::store(new \App\Exports\CoursesResults($this->report->starttime, $this->report->endtime, $this->report->year, $this->report->courses, $this->report->language, $this->report->affiliate), date('Y/m/d/') . 'report_' . $this->report->id . '.xlsx', 'public');
            if ($file) {
                $reportItem->status = Reports::REPORT_STATUS_FINISHED;
                $reportItem->file = date('Y/m/d/') . 'report_' . $this->report->id . '.xlsx';
            }
            $reportItem->save();
        } catch (\Exception $e) {
            $reportItem->data = $e;
            $reportItem->status = Reports::REPORT_STATUS_ERROR;
            $reportItem->save();
        }
    }


}
