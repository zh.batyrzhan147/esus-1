<?php

namespace App\Jobs\Reports;

use App\Models\Reports;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Maatwebsite\Excel\Facades\Excel;

class ProgramFilialYear implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $report;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Reports $report)
    {
        $this->report = $report;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $reportItem = $this->report;
        try {
            $reportItem->status = Reports::REPORT_STATUS_PROCESSING;
            $reportItem->save();
            $file = Excel::store(new \App\Exports\ProgramFilialYear($reportItem->course), date('Y/m/d/') . 'report_' . $this->report->id . '.xlsx', 'public');
            if ($file) {
                $reportItem->status = Reports::REPORT_STATUS_FINISHED;
                $reportItem->file = date('Y/m/d/') . 'report_' . $this->report->id . '.xlsx';
            }
            $reportItem->save();
        } catch (\Exception $e) {
            $reportItem->data = $e;
            $reportItem->status = Reports::REPORT_STATUS_ERROR;
            $reportItem->save();
        }

    }
}
