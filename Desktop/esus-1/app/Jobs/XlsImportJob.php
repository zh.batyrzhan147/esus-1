<?php

namespace App\Jobs;

use App\Imports\LocationsImport;
use App\Models\XlsImports;
use Exception;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Maatwebsite\Excel\Facades\Excel;

class XlsImportJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $timeout = 0;

    protected $xlsimport;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($xlsimport)
    {
        $item = XlsImports::where('id',$xlsimport)->first();
        if (!$item) return 0;
        $this->xlsimport = $item;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $_startTime = microtime(true);
        $this->xlsimport->status = XlsImports::IMPORTS_STATUS_STARTED;
        $this->xlsimport->save();
        Excel::import(new LocationsImport(), $this->xlsimport->file, 'public');

        $this->xlsimport->status = XlsImports::IMPORTS_STATUS_FINISHED;
        $this->xlsimport->message = 'Finished: '.(microtime(true)-$_startTime);
        $this->xlsimport->save();

    }


    public function failed(Exception $exception)
    {
        $this->xlsimport->status = XlsImports::IMPORTS_STATUS_ERROR;
        $this->xlsimport->message = $exception;
        $this->xlsimport->save();
    }

}
