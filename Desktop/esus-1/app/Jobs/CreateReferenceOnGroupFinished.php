<?php

namespace App\Jobs;

use App\Models\Certificates;
use App\Models\Groups;
use Hashids\Hashids;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Ramsey\Uuid\Uuid;

class CreateReferenceOnGroupFinished implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $group;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Groups $group)
    {
        $this->group = $group;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        foreach ($this->group->users as $user) {
            $uuid4 = Uuid::uuid4();
            $hashids = new Hashids($uuid4->toString());
            $uuid = $hashids->encode(1,2,3,4).date('y');

            if ($this->group->coursesItem->certificateTemplate()->where('type',\App\Models\CertificatesTemplates::CERTIFICATES_TEMPLATES_TYPE_REFERENCE)->first()) {
                $cert = Certificates::where('user', $user->id)->where('group_id', $this->group->id)->where('type',Certificates::CERTIFICATES_TYPE_REFERENCE)->first();
                if (!$cert) {
                    $c = new Certificates([
                            'fio' => $user->fio,
                            'group_title' => $this->group->slug,
                            'group_id' => $this->group->id,
                            'course_title' => $this->group->coursesItem->kz_title,
                            'course_id' => $this->group->coursesItem->id,
                            'year' => $this->group->year,
                            'description' => '',
                            'user' => $user->id,
                            'template' => $this->group->coursesItem->certificateTemplate()->where('type', \App\Models\CertificatesTemplates::CERTIFICATES_TEMPLATES_TYPE_REFERENCE)->first()->id,
                            'uuid' => $uuid,
                            'type' => Certificates::CERTIFICATES_TYPE_REFERENCE,
                            'cert_day' => $this->group->end_time ?? null,
                        ]
                    );
                    $c->save();
                }
            }
        }
    }
}
