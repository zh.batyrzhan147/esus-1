<?php

namespace App\Jobs;

use App\Mail\PasswordMail;
use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Mail;

class SendPasswordJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private User $user;
    private ?string $password;
    private bool $new;


    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(User $user, $password = null, $new = false)
    {
        $this->user = $user;
        $this->password = $password;
        $this->new = $new;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $email = new PasswordMail($this->user->iin, $this->password,$this->new);
        Mail::to($this->user->email)->send($email);

    }
}
