<?php

namespace App\Jobs;

use App\Models\Certificates;
use App\Models\Groups;
use Barryvdh\Snappy\Facades\SnappyPdf;
use Hashids\Hashids;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Http\File;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Barryvdh\DomPDF\Facade\Pdf;
use Illuminate\Support\Facades\Storage;
use Ramsey\Uuid\Uuid;

class GenerateReference implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $certificate;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($id)
    {
        $this->certificate = Certificates::where('id',$id)->first();
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $qrpath = route('verify-certificate',['uuid' => $this->certificate->uuid]);
        $group = Groups::where('id',$this->certificate->group_id)->first();
        $data = [
            'title' => $this->certificate->fio,
            'date' => date('d.m.Y',strtotime($group->start_time ?? 'now')) .'-'. date('d.m.Y',strtotime($group->end_time ?? 'now')),
            'background' => $this->certificate->templateItem->backgroundUrl,
            'url' => $qrpath,
            'uuid' => $this->certificate->uuid,
        ];


        $file = SnappyPdf::loadView('certificates.information', $data)->setPaper('a4','landscape');
        $path = storage_path('app/public').date('/Y/m/d',strtotime($this->certificate->created_at));
        Storage::makeDirectory('public'.date('/Y/m/d',strtotime($this->certificate->created_at)));
        $uuid4 = Uuid::uuid4();
        $file_name = $uuid4 . '.' . 'pdf' ;
        $file->save($path . '/' . $file_name);

        $file_url = Storage::disk('public')->url(date('Y/m/d',strtotime($this->certificate->created_at)).'/'.$file_name);
        $this->certificate->pdf = $file_url;
        $this->certificate->url = $qrpath;
        $this->certificate->save();

    }
}
