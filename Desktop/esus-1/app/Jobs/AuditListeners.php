<?php

namespace App\Jobs;

use App\Models\Affiliates;
use App\Models\AuditReports;
use App\Models\Grants;
use App\Models\Groups;
use App\Models\Languages;
use App\Models\Years;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class AuditListeners implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $auditReport;
    private $affiliate;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(AuditReports $auditReport)
    {
        $this->auditReport = $auditReport;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        try {
            if ($this->auditReport->type != AuditReports::AUDIT_REPORT_TYPE_LISTENERS) return;
            $this->auditReport->status = AuditReports::AUDIT_REPORT_STATUS_PROCESSING;
            $this->auditReport->save();
            $report = [];
//        $reportID = 0;
            $year = $this->auditReport->year;
            $affiliates = Affiliates::query();
            if ($this->auditReport->affiliate) {
                $this->affiliate = $this->auditReport->affiliate;
                $affiliates = $affiliates->where('id', $this->affiliate);
            }
            $affiliates = $affiliates->get();
            $courses = Years::where('id', $year)->first()->courses;
            $languages = Languages::all();
            $grantAll = 0;
            foreach ($courses as $cours) {
                $totalgrants = 0;
                foreach ($affiliates as $affiliate) {
//                    $grants = Grants::query();
//                    $grants = $grants->where('year', $year)->where('affiliate', $affiliate->id)->where('courses', $cours->id)->first();
//                    $totalgrants = $totalgrants + ($grants->count_all ?? 0);
                    $sum = 0;
                    foreach ($languages as $language) {
//                    $item = json_decode(($grants->counts ?? '[]'), true)[$language->slug] ?? 0;
                        $groups = Groups::where('year', $year)->where('affiliate', $affiliate->id)->where('courses', $cours->id)->where('language', $language->id)
                            ->whereIn('status', [\App\Models\Groups::GROUP_STATUS_FINISHED, \App\Models\Groups::GROUP_STATUS_CERTIFICATED, \App\Models\Groups::GROUP_STATUS_FINISHED_TESTING, \App\Models\Groups::GROUP_STATUS_ARCHIVE, \App\Models\Groups::GROUP_STATUS_INPROGRESS, \App\Models\Groups::GROUP_STATUS_FORMED])
                            ->with('users')
                            ->get();
                        foreach ($groups as $group) {
                            $fact = $group->users()->count();
                            if ($group->count_all != $fact) {
                                $report['groups']['items'][$cours->id]['courses']['id'] = $cours->id;
                                $report['groups']['items'][$cours->id]['courses']['title'] = $cours->ru_title;
                                $report['groups']['items'][$cours->id]['items'][$affiliate->id]['affiliate']['id'] = $affiliate->id;
                                $report['groups']['items'][$cours->id]['items'][$affiliate->id]['affiliate']['title'] = $affiliate->title;
//                                $report['groups']['items'][$cours->id]['items'][$affiliate->id]['count_all'] = ($grants->count_all ?? 0);
                                $report['groups']['items'][$cours->id]['items'][$affiliate->id]['items'][$language->id]['language']['id'] = $language->id;
                                $report['groups']['items'][$cours->id]['items'][$affiliate->id]['items'][$language->id]['language']['title'] = $language->title;
                                $report['groups']['items'][$cours->id]['items'][$affiliate->id]['items'][$language->id]['groups'][$group->id]['slug'] = $group->slug;
                                $report['groups']['items'][$cours->id]['items'][$affiliate->id]['items'][$language->id]['groups'][$group->id]['planed'] = $group->count_all;
                                $report['groups']['items'][$cours->id]['items'][$affiliate->id]['items'][$language->id]['groups'][$group->id]['fact'] = $fact;
                            }
                        }
//                    if ($item != $groups) {
//                        $report['groups']['items'][$cours->id]['items'][$affiliate->id]['items'][$language->id]['error'] = true;
//                    } else {
//                        $report['groups']['items'][$cours->id]['items'][$affiliate->id]['items'][$language->id]['error'] = false;
//                    }
//                        $report['groups']['items'][$cours->id]['courses']['id'] = $cours->id;
//                        $report['groups']['items'][$cours->id]['courses']['title'] = $cours->ru_title;
//                        $report['groups']['items'][$cours->id]['items'][$affiliate->id]['affiliate']['id'] = $affiliate->id;
//                        $report['groups']['items'][$cours->id]['items'][$affiliate->id]['affiliate']['title'] = $affiliate->title;
//                        $report['groups']['items'][$cours->id]['items'][$affiliate->id]['count_all'] = ($grants->count_all ?? 0);
//                        $report['groups']['items'][$cours->id]['items'][$affiliate->id]['items'][$language->id]['language']['id'] = $language->id;
//                        $report['groups']['items'][$cours->id]['items'][$affiliate->id]['items'][$language->id]['language']['title'] = $language->title;
//                        $report['groups']['items'][$cours->id]['items'][$affiliate->id]['items'][$language->id]['groups'] = $groups;
//                        $report['groups']['items'][$cours->id]['items'][$affiliate->id]['items'][$language->id]['counts'] = $item;
//                        $report['groups']['items'][$cours->id]['items'][$affiliate->id]['items'][$language->id]['groupscount'] = $groupsCount;
////                    }
                    }
//                    if ($sum != $grants->count_all) {
//                        $report['grants']['items'][$cours->id]['courses']['id'] = $cours->id;
//                        $report['grants']['items'][$cours->id]['courses']['title'] = $cours->ru_title;
//                        $report['grants']['items'][$cours->id]['items'][$affiliate->id]['affiliate']['id'] = $affiliate->id;
//                        $report['grants']['items'][$cours->id]['items'][$affiliate->id]['affiliate']['title'] = $affiliate->title;
//                        $report['grants']['items'][$cours->id]['items'][$affiliate->id]['count_all'] = $grants->count_all;
//                        $report['grants']['items'][$cours->id]['items'][$affiliate->id]['counts'] = json_decode($grants->counts,true);
//                    }
//                } else {
//                    $report['grants']['items'][$cours->id]['courses']['id'] = $cours->id;
//                    $report['grants']['items'][$cours->id]['courses']['title'] = $cours->ru_title;
//                    $report['grants']['items'][$cours->id]['items'][$affiliate->id]['affiliate']['id'] = $affiliate->id;
//                    $report['grants']['items'][$cours->id]['items'][$affiliate->id]['affiliate']['title'] = $affiliate->title;
//                    $report['grants']['items'][$cours->id]['items'][$affiliate->id]['count_all'] = 0;
//                    $report['grants']['items'][$cours->id]['items'][$affiliate->id]['counts'] = 0;
//                }
//
////                $groups = Groups::where('affiliate', $affiliate->id)
////                    ->where('courses', $cours->id)
////                    ->where('year', $year)
////                    ->whereIn('status',[\App\Models\Groups::GROUP_STATUS_FORMED,\App\Models\Groups::GROUP_STATUS_INPROGRESS,\App\Models\Groups::GROUP_STATUS_FINISHED,\App\Models\Groups::GROUP_STATUS_FINISHED_TESTING,\App\Models\Groups::GROUP_STATUS_CERTIFICATED,\App\Models\Groups::GROUP_STATUS_ARCHIVE])
////                    ->get();
////
////                foreach ($groups as $group) {
////                    $sum = $group->users()->count();
////                    if ($group->count != $sum) {
////
////                    }
//                }
//            }
//            $grantTotal = Grants::query();
//            $grantTotal = $grantTotal->where('year', $year)->where('affiliate', null)->where('courses', $cours->id)->first();
//            if ($grantTotal) {
//                $grantAll = $grantAll + $grantTotal->count_all;
//                if ($totalgrants != $grantTotal->count_all) {
//                    $report['grants']['items'][$cours->id]['courses']['id'] = $cours->id;
//                    $report['grants']['items'][$cours->id]['courses']['title'] = $cours->ru_title;
//                    $report['grants']['items'][$cours->id]['total']['count'] = $grantTotal->count_all;
//                    $report['grants']['items'][$cours->id]['total']['sum'] = $totalgrants;
//                }
//            }
//        }
                }
            }

            $this->auditReport->data = $report;
            $this->auditReport->status = AuditReports::AUDIT_REPORT_STATUS_FINISHED;
            $this->auditReport->save();
        } catch (\Exception $exception) {
            $this->auditReport->data = $exception;
            $this->auditReport->status = AuditReports::AUDIT_REPORT_STATUS_ERROR;
            $this->auditReport->save();
        }
    }
}
