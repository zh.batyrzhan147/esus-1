<?php

namespace App\Imports;

use App\Models\InstituteForms;
use App\Models\Institutes;
use App\Models\InstitutesTypes;
use App\Models\Locations;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithBatchInserts;
use Maatwebsite\Excel\Concerns\WithChunkReading;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class LocationsImport implements ToModel, WithChunkReading, WithHeadingRow
{

    function substrwords($text, $maxchar, $end='...') {
        if (strlen($text) > $maxchar || $text == '') {
            $words = preg_split('/\s/', $text);
            $output = '';
            $i      = 0;
            while (1) {
                $length = strlen($output)+strlen($words[$i]);
                if ($length > $maxchar) {
                    break;
                }
                else {
                    $output .= " " . $words[$i];
                    ++$i;
                }
            }
            $output .= $end;
        }
        else {
            $output = $text;
        }
        return $output;
    }


    /**
    * @param Collection $collection
    */
    public function model(array $location)
    {
//        foreach ($collection as $location)
//        {

            $obl = Locations::where('ru_title',$location['oblast'])->where('type',Locations::LOCATION_OBLAST)->first();
            if (!$obl) {
                if (empty($location['oblast'])) return;
                $obl = new Locations([
                    'type' => Locations::LOCATION_OBLAST,
                    'ru_title' => $this->substrwords($location['oblast'],255),
                    'kz_title' => $this->substrwords($location['oblast'],255),
                ]);
                $obl->save();
            }

            $rayon = Locations::where('ru_title',$location['raion'])->where('parent',$obl->id)->where('type',Locations::LOCATION_RAYON)->first();
            if (!$rayon) {
                if (empty($location['raion'])) return;
                $rayon = new Locations([
                    'type' => Locations::LOCATION_RAYON,
                    'parent' => $obl->id,
                    'ru_title' => $this->substrwords($location['raion'],255),
                    'kz_title' => $this->substrwords($location['raion'],255),
                ]);
                $rayon->save();
            }

            $aul = Locations::where('ru_title',$location['naselennyi_punkt'])->where('parent',$rayon->id)->where('type',Locations::LOCATION_AUL)->first();
            if (!$aul) {
                if (empty($location['naselennyi_punkt'])) return;
                $aul = new Locations([
                    'type' => Locations::LOCATION_AUL,
                    'parent' => $rayon->id,
                    'ru_title' => $this->substrwords($location['naselennyi_punkt'],255),
                    'kz_title' => $this->substrwords($location['naselennyi_punkt'],255),
                ]);
                $aul->save();
            }

            $insttype = InstitutesTypes::where('title',$location['tip_organizacii_obrazovaniya'])->first();
            if (!$insttype) {
                if (empty($location['tip_organizacii_obrazovaniya'])) return;
                $insttype = new InstitutesTypes([
                    'title' => $this->substrwords($location['tip_organizacii_obrazovaniya'],255),
                ]);
                $insttype->save();
            }

            $formstype = InstituteForms::where('title',$location['forma_sobstvennosti'])->first();
            if (!$formstype) {
                if (empty($location['forma_sobstvennosti'])) return;
                $formstype = new InstituteForms([
                    'title' => $this->substrwords($location['forma_sobstvennosti'],255),
                ]);
                $formstype->save();
            }

        if (empty($location['id'])) return;
        Institutes::updateOrCreate(
            [
                'id' => $location['id'],
            ],
            [
                'id' => $location['id'],
                'title' => $this->substrwords($location['naimenovanie_organizacii'],255),
                'kz_title' => $this->substrwords($location['polnoe_naimenovanie_na_kazaxskom'],255),
                'ru_title' => $this->substrwords($location['polnoe_naimenovanie_na_russkom'],255),
                'type'  => $insttype->id,
                'city' => $aul->id,
                'rayon' => $rayon->id,
                'oblast' => $obl->id,
                'forms_id' => $formstype->id,
            ]);

//            $insitute = Institutes::find($location['id']);
//            if (!$insitute) {
//
//
//            }

//        }

    }

    public function chunkSize(): int
    {
        return 100;
    }
}
