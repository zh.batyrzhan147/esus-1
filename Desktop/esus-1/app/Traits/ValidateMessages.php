<?php

namespace App\Traits;

trait ValidateMessages
{
    public function messages()
    {
        return [
            'required' => 'Поле обязательно к заполнению',
            'boolean' => 'Поле должно быть логическим типом',
            'string' => 'Поле должно состоять из текста',
            'unique' => 'Значение должно быть уникальным',
            'digits' => 'Должно содержать :digits цифр',
            'file' => 'Выберите файл',
            'integer' => 'Значение должно быть целым числом',
            'max' => 'Файл не должен привышать :max кБ',
            'regex' => 'Формат заполнения не верный'
        ];
    }

    public function updated($name, $value)
    {
        if ( $value == '' ) data_set($this, $name, null);
    }
}
