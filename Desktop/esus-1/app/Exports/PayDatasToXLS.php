<?php

namespace App\Exports;


use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\WithCustomCsvSettings;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Events\AfterSheet;

class PayDatasToXLS implements FromArray, WithHeadings, WithCustomCsvSettings, WithEvents
{
    use Exportable;

    protected $table = [];

    public function __construct($table = [])
    {
        $this->table = $table;
    }


    public function array(): array
    {

        $datas = [];

        foreach ($this->table['orders'] as $key => $item) {
            $datas[$key]['ID'] = $item['pay_id'];
            $datas[$key]['ФИО'] = $item['fio'];
            $datas[$key]['Область'] = $item['oblast'];
            $datas[$key]['Филиал'] = $item['filial'];
            $datas[$key]['Программа'] = $item['course'];
            $datas[$key]['Тренер'] = $item['trainer'];
            $datas[$key]['Шифр'] = $item['slug'];
            $datas[$key]['Дата оплаты'] = $item['date_payed'];
            $datas[$key]['Сумма'] = $item['total'];
        }
        return $datas;
    }

    public function headings(): array
    {
        return [
            'ID',
            'ФИО',
            'Область',
            'Филиал',
            'Программа',
            'Тренер',
            'Шифр',
            'Дата оплаты',
            'Сумма',
        ];
    }

    public function registerEvents(): array
    {
        return [
            AfterSheet::class => function (AfterSheet $event) {
                $event->sheet->getStyle('A1:I1')->applyFromArray(
                    [
                        'font' => [
                            'bold' => true
                        ]
                    ]
                );
            }
        ];
    }

    public function getCsvSettings(): array
    {
        return [
            'delimiter' => ';',
            'use_bom' => true,
            'output_encoding' => 'UTF-8',
            'enclosure' => '',
        ];
    }


}
