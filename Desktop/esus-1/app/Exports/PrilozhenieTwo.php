<?php

namespace App\Exports;

use App\Models\Courses;
use App\Models\Groups;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;
use Maatwebsite\Excel\Concerns\WithTitle;

class PrilozhenieTwo implements FromView,ShouldAutoSize,WithTitle
{
//    private $starttime;
//    private $endtime;
//    private $year;
//    private $courses;
//    private $language;
    private $table;
    private $lang;

//    public function __construct($starttime = null, $endtime = null, $year = null, $courses = null, $language = null,$t = '')
//    {
//        $this->starttime = $starttime;
//        $this->endtime = $endtime;
//        $this->year = $year;
//        $this->courses = $courses;
//        $this->language = $language;
//        $this->t = $t;
//    }
    public function __construct($table = [], $lang = '')
    {
        $this->table = $table;
        $this->lang = $lang;
    }

    public function view(): View
    {
//        $table = [];
//        $groups = Groups::query();
//        $groups = $this->applySearch($groups)->get();
//
//        foreach ($groups as $group) {
//            $table[$group->id]['courses'] = $group->coursesItem->ru_title;
//            $table[$group->id]['period'] = date('d.m.Y', strtotime($group->start_time)) . '-' . date('d.m.Y', strtotime($group->end_time));
//            $table[$group->id]['format'] = Courses::listTypes()[$group->coursesItem->type];
//            $table[$group->id]['affiliate'] = $group->affiliateItem->title;
//            $i = 1;
//            foreach ($group->users as $user) {
//                $table[$group->id]['users'][$user->id]['number'] = $i;
//                $i++;
//                $table[$group->id]['users'][$user->id]['fio'] = $user->fio;
//                $table[$group->id]['users'][$user->id]['working'] = $user->instituteItem->title ?? 'Не указано';
//                $table[$group->id]['users'][$user->id]['jobname'] = $user->jobnameItem->ru_title ?? 'Не указано';
//                $table[$group->id]['users'][$user->id]['language'] = $group->languageItem->title;
//                $table[$group->id]['users'][$user->id]['trainer'] = $group->trainerItem->fio;
//                $table[$group->id]['users'][$user->id]['group'] = $group->slug;
//            }
//
//        }
        return view('exports.PrilozhenieTwo'.$this->lang, [
            'table' => $this->table,
        ]);
    }

//    private function applySearch(\Illuminate\Database\Eloquent\Builder $item)
//    {
//        $item = $item->where(function ($q) {
//            $q->where('status', Groups::GROUP_STATUS_FORMED)
//                ->orWhere('status', Groups::GROUP_STATUS_INPROGRESS)
//                ->orWhere('status', Groups::GROUP_STATUS_FINISHED)
//                ->orWhere('status', Groups::GROUP_STATUS_FINISHED_TESTING)
//                ->orWhere('status', Groups::GROUP_STATUS_ARCHIVE);
//        });
//        $item = $item->where('courses', $this->courses);
//        $item = $item->where('language', $this->language);
//        $item = $item->where(function ($q) {
//            $q->where('start_time', '>=', $this->starttime)
//                ->orWhere('end_time', '>=', $this->starttime);
//        });
//        $item = $item->Where(function ($q) {
//            $q->where('start_time', '<=', $this->endtime)
//                ->orWhere('end_time', '<=', $this->endtime);
//        });
//        $item = $item->with('users');
//
//        $item = $item->orderBy('start_time', 'ASC')->orderBy('affiliate', 'ASC');
//        return $item;
//    }

    public function title(): string
    {
        return $this->lang;
    }
}
