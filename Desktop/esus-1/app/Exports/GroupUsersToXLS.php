<?php

namespace App\Exports;

use App\Models\Groups;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\WithCustomCsvSettings;
use Maatwebsite\Excel\Concerns\WithHeadings;

class GroupUsersToXLS implements FromArray,WithHeadings,WithCustomCsvSettings
{
    use Exportable;

    protected $group;

    public function __construct(Groups $group)
    {
        $this->group = $group;
    }


    public function array(): array
    {

        $users = [];
        $i = 0;
        foreach ($this->group->users as $key=>$item) {
            $i++;
            $users[$key]['ID'] = $i;
            $users[$key]['ФИО'] = $item->fio;
            $users[$key]['Место работы'] = $item->instituteItem->ru_title ?? '';
            $users[$key]['Область'] = $item->oblastItem->ru_title ?? '';
            $users[$key]['Район'] = $item->raionItem->ru_title ?? '';
            $users[$key]['Город'] = $item->gorodItem->ru_title ?? '';
            $users[$key]['Телефон'] = $item->phone;
            $users[$key]['Почта'] = $item->email;
        }
        return $users;
    }

    public function headings(): array
    {
        return [
            'ID',
            'ФИО',
            'Место работы',
            'Область',
            'Район',
            'Город',
            'Телефон',
            'Почта',
        ];

    }

    public function getCsvSettings(): array
    {
        return [
            'delimiter' => ';',
            'use_bom' => true,
            'output_encoding' => 'UTF-8',
            'enclosure' => '',
        ];
    }
}
