<?php

namespace App\Exports;

use App\Models\Affiliates;
use App\Models\Languages;
use App\Models\Role;
use App\Models\User;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithDefaultStyles;
use Maatwebsite\Excel\Concerns\WithStyles;
use PhpOffice\PhpSpreadsheet\Style\Style;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;

class TrainersPubs implements FromView,ShouldAutoSize,WithDefaultStyles,WithStyles

{
    public function __construct(public ?Affiliates $affiliate = null, public ?Languages $languages = null){}

    public function view(): View
    {
        $table = [];
        $trainers = User::query();
        $trainers = $this->applySearch($trainers)->get();
        $afffiliates = [];
        if ($this->affiliate) {
            $afffiliates[] = $this->affiliate;
        } else {
            $afffiliates = Affiliates::all();
        }
        foreach ($trainers as $trainer) {
            $table[$trainer->affiliate][$trainer->id]['fio'] = $trainer->fio;
            $table[$trainer->affiliate][$trainer->id]['jobnameItem'] = $trainer->jobnameItem->kz_title;
            $table[$trainer->affiliate][$trainer->id]['teach_subject_id'] = $trainer->teachSubjectItem->kz_title;
            $table[$trainer->affiliate][$trainer->id]['trainer_awards'] = $trainer->trainer_awards;
            $table[$trainer->affiliate][$trainer->id]['trainer_theses'] = $trainer->trainer_theses;
            $table[$trainer->affiliate][$trainer->id]['trainer_certificates'] = $trainer->trainer_certificates;
        }
        return view('exports.TrainersPubs', [
            'table' => $table,
            'affiliates' => $afffiliates,
        ]);
    }

    public function defaultStyles(Style $defaultStyle)
    {
        return [
            'borders' => [
                'allBorders' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['argb' => '000000'],
                ],
            ],
            'font' => [
                'name' => 'Times New Roman'
            ],
        ];

    }

    public function styles(Worksheet $sheet)
    {

    }

    private function applySearch(\Illuminate\Database\Eloquent\Builder $item)
    {
        $item = $item->whereHas('roles', function ($q) {
            $q->where('id', Role::ROLE_TRAINER);
        });

        if ($this->affiliate) {
            $item = $item->where('affiliate', $this->affiliate->id);
        }

        if ($this->languages) {
            $lang = $this->languages->id;
            $item = $item->whereHas('languages',function ($q) use ($lang) {$q->where('id',$lang);});
        }

        $item->orderBy('affiliate', 'ASC');

        return $item;
    }

}
