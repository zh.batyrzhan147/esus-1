<?php

namespace App\Exports;

use App\Http\Livewire\Weekend;
use App\Models\Groups;
use App\Models\Statements;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class StatementsExport implements FromView
{

    protected $group;

    public function __construct(Groups $group)
    {
        $this->group = $group;
    }

    public function view(): View
    {
        $dates = [];
        $firstDate = new \DateTime($this->group->start_time);
        $endDate = new \DateTime($this->group->end_time);

        while (strtotime($firstDate->format('Y-m-d')) <= strtotime($this->group->end_time)) {
//            $this->weeks[$f->format('Y-m-d')] = $f->format('d.m').'-'.$f->modify('next sunday')->format('d.m');
            if (!Weekend::isWeekend($firstDate->format('Y-m-d'))) {
                $dates[] = $firstDate->format('Y-m-d');
            }
            $firstDate->modify('+1 day');
        }

        $data = [];
        $statements = Statements::where('group',$this->group->id)->whereIn('available_at',$dates)->with('userItem')->get();

        foreach ($statements as $statement)
        {
            // $data[$statement->user]['user'] = $statement->userItem->name;
            // $data[$statement->user]['user'] = $statement->userItem->last_name;
          //  $data[$statement->user]['items'][$statement->available_at] = $statement->status;
        }

        return view('exports.statementsExport', [
            'dates' => $dates,
            'group' => $this->group,
            'data' => $data,
        ]);

    }
}
