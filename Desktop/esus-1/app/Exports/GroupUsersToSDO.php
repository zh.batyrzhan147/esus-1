<?php

namespace App\Exports;

use App\Models\Groups;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\WithCustomCsvSettings;
use Maatwebsite\Excel\Concerns\WithHeadings;

class GroupUsersToSDO implements FromArray,WithHeadings,WithCustomCsvSettings
{
    use Exportable;

    protected $group;

    public function __construct(Groups $group)
    {
        $this->group = $group;
    }


    public function array(): array
    {
//        'iin',
//        'name',
//        'last_name',
//        'patronymic',
//        'phone',
//        'email',
//        'password',
//        'auto_generation',
//        'email_verified_at',
//        'stazh',
//        'attracted',
//        'gorono',
//        'attractedcat',
//        'hours_count',
        $users = [];
        foreach ($this->group->users as $key=>$item) {
            $users[$key]['firstname'] = $item->name;
            $users[$key]['lastname'] = $item->last_name;
            $users[$key]['middlename'] = $item->patronymic;
            $users[$key]['city'] = $item->oblastItem->ru_title ?? '';
            $users[$key]['idnumber'] = $item->iin;
            $users[$key]['email'] = $item->email;
            $users[$key]['username'] = $item->email;
            $users[$key]['course1'] = ($this->group->languageItem->slug == 'kz' ? $this->group->coursesItem->kz_short_title : ($this->group->languageItem->slug == 'ru' ? $this->group->coursesItem->ru_short_title : $this->group->coursesItem->en_short_title));
            $users[$key]['group1'] = $this->group->slug;
            $users[$key]['password'] = 'Cpm'.substr(strrev($item->iin),0,6).'+';
        }
        return $users;
    }

    public function headings(): array
    {
        return [
            'firstname',
            'lastname',
            'middlename',
            'city',
            'idnumber',
            'email',
            'username',
            'course1',
            'group1',
            'password',

        ];

    }

    public function getCsvSettings(): array
    {
        return [
            'delimiter' => ';',
            'use_bom' => true,
            'output_encoding' => 'UTF-8',
            'enclosure' => '',
        ];
    }
}
