<?php

namespace App\Exports;

use App\Models\Courses;
use App\Models\Groups;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class PrilozhenieOne implements FromView,ShouldAutoSize
{


    public function __construct(public $starttime = null, public $endtime = null, public $year = null, public $courses = null, public $language = null, public $affiliate = null)
    {
    }

    public function view(): View
    {
        return view('exports.PrilozhenieOne',[
            'table' => $this->getTable(),
        ]);
    }

    public function getTable()
    {
        $table = [];
        $groups = Groups::query();
        $groups = $this->applySearch($groups)->get();

        foreach ($groups as $group) {
            $table['affiliates'][$group->affiliate] = $group->affiliate;
            $table['title'][$group->affiliate] = $group->affiliateItem->title ?? '';
            $table[$group->affiliate][$group->id]['courses'] = $group->coursesItem->ru_title ?? '';
            $table[$group->affiliate][$group->id]['courseskz'] = $group->coursesItem->kz_title ?? '';
            $table[$group->affiliate][$group->id]['period'] = date('d.m.Y', strtotime($group->start_time)) . '-' . date('d.m.Y', strtotime($group->end_time));
            $table[$group->affiliate][$group->id]['slug'] = $group->slug ?? '';
            $table[$group->affiliate][$group->id]['count'] = $group->users()->count();
            $table[$group->affiliate][$group->id]['language'] = $group->languageItem->title ?? '';
            $table[$group->affiliate][$group->id]['trainer_fio'] = $group->trainerItem->fio ?? '';
            $table[$group->affiliate][$group->id]['trainer_work'] = $group->trainerItem->instituteItem->title ?? '';
            $table[$group->affiliate][$group->id]['trainer_jobname'] = $group->trainerItem->jobnameItem->ru_title ?? '';
//            $table[$group->affiliate][$group->id]['trainer_teachSubject'] = $group->trainerItem->teachSubjectItem->ru_title ?? '';
            $table[$group->affiliate][$group->id]['trainer_attractedcat'] = $group->trainerItem->attractedcatItem->ru_title ?? '';
            $table[$group->affiliate][$group->id]['course_hours'] = $group->coursesItem->course_length ?? '';

//            foreach ($group->users()->orderBy('last_name','ASC')->get() as $user) {
//                $table[$group->id]['users'][$user->id]['number'] = $i;
//                $i++;
//                $table[$group->id]['users'][$user->id]['fio'] = $user->fio;
//                $table[$group->id]['users'][$user->id]['working'] = $user->instituteItem->title ?? 'Не указано';
//                $table[$group->id]['users'][$user->id]['jobname'] = $user->jobnameItem->ru_title ?? 'Не указано';
//                $table[$group->id]['users'][$user->id]['jobnamekz'] = $user->jobnameItem->kz_title ?? 'Не указано';
//                $table[$group->id]['users'][$user->id]['language'] = $group->languageItem->title;
//                $table[$group->id]['users'][$user->id]['languagekz'] = ($group->languageItem->slug == 'kz') ? 'Қазақша' : (($group->languageItem->slug == 'ru') ? 'Орысша' : (($group->languageItem->slug == 'en') ? 'Ағылшын' : ''));
//                $table[$group->id]['users'][$user->id]['trainer'] = $group->trainerItem->fio;
//                $table[$group->id]['users'][$user->id]['group'] = $group->slug;
//            }

        }
        return $table;
    }

    private function applySearch(\Illuminate\Database\Eloquent\Builder $item)
    {
        $item = $item->where(function ($q) {
            $q->where('status', Groups::GROUP_STATUS_FORMED)
                ->orWhere('status', Groups::GROUP_STATUS_INPROGRESS)
                ->orWhere('status', Groups::GROUP_STATUS_FINISHED)
                ->orWhere('status', Groups::GROUP_STATUS_FINISHED_TESTING)
                ->orWhere('status', Groups::GROUP_STATUS_ARCHIVE);
        });
        $item = $item->where('courses', $this->courses);
        $item = $item->where('language', $this->language);
        $item = $item->where(function ($q) {
            $q->where('start_time', '>=', $this->starttime)
                ->orWhere('end_time', '>=', $this->starttime);
        });
        $item = $item->Where(function ($q) {
            $q->where('start_time', '<=', $this->endtime)
                ->orWhere('end_time', '<=', $this->endtime);
        });
        $item = $item->with('users','trainerItem');
        if ($this->affiliate) $item = $item->where('affiliate',$this->affiliate);

        $item = $item->orderBy('start_time', 'ASC')->orderBy('affiliate', 'ASC');
        return $item;
    }

}
