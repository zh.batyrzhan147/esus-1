<?php

namespace App\Exports;

use App\Models\Groups;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithDefaultStyles;
use Maatwebsite\Excel\Concerns\WithStyles;
use PhpOffice\PhpSpreadsheet\Style\Style;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;


class CoursesResults implements FromView,ShouldAutoSize,WithDefaultStyles,WithStyles
{

    /**
     * @param $starttime
     * @param $endtime
     * @param $year
     * @param $courses
     * @param $language
     */
    public function __construct(public $starttime, public $endtime, public $year, public $courses, public $language, public $affiliate)
    {
    }


    /**
     * @return View
     */
    public function view(): View
    {
        return view('exports.CoursesResults',[
            'table' => $this->getTable(),
        ]);
    }

    public function getTable(): array
    {
        $table = [];
        $groups = Groups::query();
        $groups = $this->applySearch($groups)->get();
        foreach ($groups as $group) {
            $table[$group->id]['slug'] = $group->slug;
            $table[$group->id]['trainer'] = $group->trainerItem->fio;
            $table[$group->id]['affiliate'] = $group->affiliateItem->title;
            $table[$group->id]['course'] = $group->coursesItem->kz_title;
            $table[$group->id]['language'] = $group->languageItem->title;
            $table[$group->id]['year'] = $group->year;
            $users = count($group->users);
            $userpassed = count($group->certificates);
            $notpassed = ($users > $userpassed) ? $users - $userpassed : 0;
            $percentage = ($userpassed * 100) / ( $users > 0 ? $users : 1);

            $table[$group->id]['passed'] = $userpassed;
            $table[$group->id]['not_passed'] = $notpassed;
            $table[$group->id]['total'] = $users;
            $table[$group->id]['percentage'] = intval($percentage);
        }
        return $table;
    }

    private function applySearch(\Illuminate\Database\Eloquent\Builder $item)
    {
        $item = $item->where(function ($q) {
            $q->where('status',Groups::GROUP_STATUS_ARCHIVE)
                ->orWhere('status', Groups::GROUP_STATUS_CERTIFICATED)
            ;
        });

        if ($this->courses)  $item = $item->where('courses', $this->courses);
        if ($this->language) $item = $item->where('language', $this->language);
        if ($this->starttime)
            $item = $item->where(function ($q) {
                $q->where('start_time', '>=', $this->starttime)
                    ->orWhere('end_time', '>=', $this->starttime);
            });
        if ($this->endtime)
            $item = $item->where(function ($q) {
                $q->where('start_time', '<=', $this->endtime)
                    ->orWhere('end_time', '<=', $this->endtime);
            });
        $item = $item->with('users','trainerItem', 'certificates','affiliateItem','coursesItem','languageItem');
        if ($this->affiliate) $item = $item->where('affiliate',$this->affiliate);
        $item = $item->orderBy('start_time', 'DESC')->orderBy('affiliate', 'DESC');
        return $item;
    }

    public function defaultStyles(Style $defaultStyle)
    {
        return [
            'font' => [
                'name' => 'Times New Roman'
            ],
        ];

    }

    public function styles(Worksheet $sheet)
    {
        $sheet->getStyle('A1')->getAlignment()->setWrapText(true);
    }
}
