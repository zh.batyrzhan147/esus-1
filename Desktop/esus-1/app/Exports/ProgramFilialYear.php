<?php

namespace App\Exports;

use App\Models\Affiliates;
use App\Models\Courses;
use App\Models\Groups;
use App\Models\Years;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithDefaultStyles;
use Maatwebsite\Excel\Concerns\WithStyles;
use Maatwebsite\Excel\Concerns\WithTitle;
use PhpOffice\PhpSpreadsheet\Reader\Xml\Style\Font;
use PhpOffice\PhpSpreadsheet\Style\Fill;
use PhpOffice\PhpSpreadsheet\Style\Style;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use function Illuminate\Events\queueable;

class ProgramFilialYear implements FromView,ShouldAutoSize,WithDefaultStyles,WithStyles
{
    public $courseItem;

    public function __construct($courseId)
    {
        $this->courseItem = Courses::find($courseId);
    }

    public function view(): View
    {

        $table = [];

        $course = $this->courseItem;
        $years = Years::all();
        $affiliates = Affiliates::all();

        foreach ($years as $year) {
            foreach ($affiliates as $affiliate) {
                $count = 0;
                Groups::where('affiliate',$affiliate->id)->where('year', $year->year)->where('courses',$course->id)
                    ->with('users')
                    ->chunk(200, function ($items) use (&$count) {
                        foreach ($items as $item) {
                            $count = $count + $item->users()->count();
                        }
                    });
                $table[$affiliate->id][$year->year]['count'] = $count;
                $table[$affiliate->id][$year->year]['title'] = $year->year;
                $table[$affiliate->id]['title'] = $affiliate->title;
            }
        }
        $tableTotal = 0;
        foreach ($years as $year) {
            foreach ($affiliates as $affiliate) {
                $tableTotal = $tableTotal + $table[$affiliate->id][$year->year]['count'];
                $table['total'][0][$affiliate->id] = ($table['total'][0][$affiliate->id] ?? 0) + $table[$affiliate->id][$year->year]['count'];
                $table['total'][$year->id][0] = ($table['total'][$year->id][0] ?? 0) + $table[$affiliate->id][$year->year]['count'];
            }
        }
        $table['total'][0][0] = $tableTotal;
        return view('exports.ProgramFilialYear', [
            'table' => $table,
            'course' => $course,
            'affiliates' => $affiliates,
            'years' => $years,
        ]);
    }


    public function defaultStyles(Style $defaultStyle)
    {
        return [
            'font' => [
                'name' => 'Times New Roman'
            ],
        ];

    }

    public function styles(Worksheet $sheet)
    {
        $sheet->getStyle('A1')->getAlignment()->setWrapText(true);
    }
}
